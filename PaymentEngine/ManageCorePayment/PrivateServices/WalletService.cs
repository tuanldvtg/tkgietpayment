﻿using ManageCorePayment.DAL;
using ManageCorePayment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManageCorePayment.PrivateServices
{
    public class WalletService
    {
        public List<Wallet> find(int merchantID)
        {
            List<Wallet> items = new List<Wallet>();
            using (var db = new GamePaymentContext())
            {
                var query = from w in db.Wallet
                            join mw in db.Merchant_Wallet on w.id equals mw.wallet_id
                            where mw.merchant_id == merchantID
                            select new
                            {
                                w.id,
                                w.name,
                                w.coin_id
                            };
                query.ToList().ForEach(ele =>
                {
                    items.Add(new Wallet()
                    {
                        id = ele.id,
                        name = ele.name,
                        coin_id = ele.coin_id
                    });
                });
            }

            return items;
        }
    }
}