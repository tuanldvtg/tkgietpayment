﻿using ManageCorePayment.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManageCorePayment.PrivateServices
{
    public class AuthMiddleware
    {
        public static void checkRole(AuthenticationHelper AuthHlp, List<SessionKeyMode> allows)
        {
            if (!AuthHlp.isLogined())
            {
                throw new Exception();
            }
            if (!allows.Contains(AuthHlp.getRole()))
            {
                throw new Exception();
            }
        }

        public static void checkRole(AuthenticationHelper AuthHlp, SessionKeyMode allow)
        {
            if (!AuthHlp.isLogined())
            {
                throw new Exception();
            }
            if (allow != AuthHlp.getRole())
            {
                throw new Exception();
            }
        }
        public static Boolean checkRoleBoolen(AuthenticationHelper AuthHlp, SessionKeyMode allow)
        {
            if (!AuthHlp.isLogined())
            {
                return false;
            }
            if (allow != AuthHlp.getRole())
            {
                return false;
            }
            return true;
        }
    }
}