﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Exceptions;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ManageCorePayment.PrivateServices
{
    public class UserPrivateServices
    {


        public List<Users> authentication(String username, String password)
        {
            using (var db = new GamePaymentContext())
            {

                ServiceBase serviceBase = new ServiceBase();
                username = serviceBase.convertStringToSql(username);

                if (!serviceBase.isCheckRegexQuery(username))
                {
                    throw new AccountNotfoundException();
                }

                var authusername = db.User.FirstOrDefault(u => u.username == username);
                if (authusername == null)
                {
                    throw new AccountNotfoundException();
                }
                var password_md5 = CryptoHelper.MD5Hash(password, authusername.id.ToString());
                var list_user = (from w in db.User
                                 where (w.username == username && w.password == password_md5)
                                 select w).ToList();
                if (list_user.Count() == 0)
                {
                    throw new AccountNotfoundException();
                }

                if (!list_user[0].status_active.Trim().Equals("active"))
                {
                    throw new AccountNotfoundException();
                }
                return list_user;
            }
        }
        public List<Users> authenticationByMerchant(String username, String password, int merchant_id)
        {
            using (var db = new GamePaymentContext())
            {
                ServiceBase serviceBase = new ServiceBase();
                username = serviceBase.convertStringToSql(username);

                if (!serviceBase.isCheckRegexQuery(username))
                {
                    throw new AccountNotfoundException();
                }
                var authusername = db.User.SingleOrDefault(u => u.username == username);
                if (authusername == null)
                {
                    throw new AccountNotfoundException();
                }
                var password_md5 = CryptoHelper.MD5Hash(password, authusername.id.ToString());
                var list_user = (from w in db.User
                                 where (w.username == username && w.password == password_md5 && w.merchant_id == merchant_id)
                                 select w).ToList();
                if (list_user.Count() == 0)
                {
                    throw new AccountNotfoundException();
                }
                return list_user;
            }
        }

        public void changePassword(int agencyID, String oldPassword, String newPassword)
        {
            using (var db = new GamePaymentContext())
            {
                oldPassword = CryptoHelper.MD5Hash(oldPassword, agencyID.ToString());
                var userModel = db.User.FirstOrDefault(u => u.id == agencyID && u.password == oldPassword);
                if (userModel == null)
                {
                    throw new AccountNotfoundException();
                }
                if (userModel.status_active.Trim().Equals("deactive"))
                {
                    throw new AccountDeactiveException();
                }
                // Update new pw
                var hashedNewPassword = CryptoHelper.MD5Hash(newPassword, agencyID.ToString());
                userModel.password = hashedNewPassword;
                db.SaveChanges();
            }
        }

        
        /**
         * Get Agency wallets
         */
        internal List<ManagerWallet> getWallets(int agencyID)
        {
            List<ManagerWallet> wallets = new List<ManagerWallet>();
            try
            { 
                using (var db = new GamePaymentContext())
                {
                    var walletsQuery = from w in db.ManagerWallet
                                  where w.manager_id == agencyID
                                  select w;
                    wallets = walletsQuery.ToList();
                }
                return wallets;
            }
            catch (Exception )
            {
                return wallets;
            }
        }
    }
}