﻿using ManageCorePayment.DAL;
using ManageCorePayment.Exceptions;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManageCorePayment.PrivateServices
{
    public class ManagerPrivateServices
    {
        public void changePassword(int managerID, String oldPassword, String newPassword)
        {

            var pwsHash = CryptoHelper.MD5Hash(oldPassword, managerID.ToString());
            var pwsNewHash = CryptoHelper.MD5Hash(newPassword, managerID.ToString());

            using (var db = new GamePaymentContext())
            {

                var record = db.Admin.Where(e => (e.id == managerID && e.password == pwsHash)).FirstOrDefault();
                if (record != null)
                {
                    record.password = pwsNewHash;
                    db.SaveChanges();
                    return;
                }
                throw new AccountNotfoundException();
            }
        }

        internal void createFundAgency(int agencyID, double money, int walletID,string content,double money_in, int paymentgateID)
        {
            using (var db = new GamePaymentContext())
            {
                db.AdminMoneyTransferCommit.Add(new AdminMoneyTransferCommit()
                {
                    type = 1,
                    agency_id = agencyID,
                    wallet_id = walletID,
                    money = money,
                    approved = 0,
                    create_time = DateTime.Now,
                    approve_time = DateTime.Now,
                    money_in = money_in,
                    paymentgate_id = paymentgateID,
                    note = content
                });
                db.SaveChanges();
            }
        }

        internal void createWallet(string title, int agencyID)
        {
            using (var db = new GamePaymentContext())
            {
                db.ManagerWallet.Add(new ManagerWallet()
                {
                    title = title,
                    manager_id = agencyID
                });
                db.SaveChanges();
            }
        }
    }
}