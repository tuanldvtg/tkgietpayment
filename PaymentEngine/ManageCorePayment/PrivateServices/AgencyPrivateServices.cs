﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Exceptions;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ManageCorePayment.PrivateServices
{
    public class AgencyPrivateServices
    {
        public Agency authentication(String username, String password)
        {
            using (var db = new GamePaymentContext())
            {

                ServiceBase serviceBase = new ServiceBase();
                username = serviceBase.convertStringToSql(username);

                if (!serviceBase.isCheckRegexQuery(username))
                {
                    throw new AccountNotfoundException();
                }

                var authusername = db.Agency.FirstOrDefault(u => u.username == username);
                if (authusername == null)
                {
                    throw new AccountNotfoundException();
                }

                var password_md5 = CryptoHelper.MD5Hash(password, authusername.id.ToString());
                var model_Agency = (from w in db.Agency
                                 where (w.username == username && w.password == password_md5)
                                 select w).FirstOrDefault();
                if (model_Agency == null)
                {
                    throw new AccountNotfoundException();
                }
                if (!model_Agency.status_active.Trim().Equals("active"))
                {
                    throw new AccountDeactiveException();
                }
                return model_Agency;
            }
        } 

        public void changePassword(int agencyID, String oldPassword, String newPassword)
        {
            using (var db = new GamePaymentContext())
            {
                oldPassword = CryptoHelper.MD5Hash(oldPassword, agencyID.ToString());
                var userModel = db.Agency.FirstOrDefault(u => u.id == agencyID && u.password == oldPassword);
                if (userModel == null)
                {
                    throw new AccountNotfoundException();
                }
                if (userModel.status_active.Equals("deactive"))
                {
                    throw new AccountDeactiveException();
                }
                // Update new pw
                var hashedNewPassword = CryptoHelper.MD5Hash(newPassword, agencyID.ToString());
                userModel.password = hashedNewPassword;
                db.SaveChanges();
            }
        }
        public void changePasswordUser(int UserID, String oldPassword, String newPassword)
        {
            using (var db = new GamePaymentContext())
            {
                oldPassword = CryptoHelper.MD5Hash(oldPassword, UserID.ToString());
                var userModel = db.User.FirstOrDefault(u => u.id == UserID && u.password == oldPassword);
                if (userModel == null)
                {
                    throw new AccountNotfoundException();
                }
                if (userModel.status_active.Equals("deactive"))
                {
                    throw new AccountDeactiveException();
                }
                // Update new pw
                var hashedNewPassword = CryptoHelper.MD5Hash(newPassword, UserID.ToString());
                userModel.password = hashedNewPassword;
                db.SaveChanges();
            }
        }

        /**
         * Get Agency wallets
         */
        internal List<Wallet> getWallets(int agencyID)
        {
            using (var db = new GamePaymentContext())
            {
                List<Wallet> wallets = new List<Wallet>();
                ServiceBase serviceBase = new ServiceBase();
                SqlDataReader reader;
                string query_agency = "";
                var agency = db.Agency.FirstOrDefault(a => a.id == agencyID);
                if (agency.subid == 0)
                {
                    query_agency = agencyID.ToString();
                }
                else
                {
                    query_agency = agency.subid.ToString();
                }
                //reader = serviceBase.query("SELECT id,name FROM Wallet WHERE id IN(SELECT Merchant_wallet.wallet_id FROM Agency_Merchant INNER JOIN Merchant_wallet ON Agency_Merchant.merchant_id = Merchant_wallet.merchant_id WHERE Agency_Merchant.agency_id = " + query_agency + ")");
                string query = "SELECT id,name FROM Wallet WHERE id IN(SELECT Merchant_wallet.wallet_id FROM Agency_Merchant INNER JOIN Merchant_wallet ON Agency_Merchant.merchant_id = Merchant_wallet.merchant_id WHERE Agency_Merchant.agency_id = @query_agency)";
                SqlCommand command = new SqlCommand(query, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@query_agency", query_agency);
                reader = serviceBase.querySqlParam(command);
                while (reader.Read())
                {
                    Wallet wallet = new Wallet();
                    wallet.id = Int32Ext.toInt(reader["id"].ToString());
                    wallet.name = reader["name"].ToString();
                    wallets.Add(wallet);
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();
                return wallets;
            }
        }
    }
}