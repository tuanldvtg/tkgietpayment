﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace ManageCorePayment.Helper
{
    public class HashCash
    {
        private Random rnd = new Random();
        protected string HashCurrent;
        private static HashCash instance;
        public static HashCash Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new HashCash();
                }
                return instance;
            }
        }
        public bool Verify(string header,string countZeroStr, string token)
        {
            // We assume the bits that are going to be 0 are going to be between 10 and 99.
            try
            {
                SHA1CryptoServiceProvider sha = new SHA1CryptoServiceProvider();
                byte[] hash = sha.ComputeHash(Encoding.UTF8.GetBytes(header));
                var result = string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
                Debug.WriteLine("Hashcahs --- Verify --- result: " + result);
                string[] substrings = header.Split(':');
                int countZero = countZeroStr.Length;
                if (result.Substring(0, countZero) == countZeroStr)
                {
                    var hashRand = Convert.ToBase64String(Encoding.UTF8.GetBytes(token));
                    if (substrings[4].Equals(hashRand))
                    {
                        return true;
                    }
                }
                return false;
            }catch(Exception )
            {
                return false;
            }
        }
        //public bool VerifyMiniGame(string HashCashVerify)
        //{
        //    int zbits = -1;
        //    try
        //    {
        //        zbits = int.Parse(HashCashVerify.Substring(2, 2));
        //    }
        //    catch (Exception )
        //    {
        //        return false;
        //    }
        //    if (zbits < -1) return false;
        //    int bytesToCheck = zbits / 8;
        //    int remainderBitsToCheck = zbits % 8;
        //    byte[] zArray = Enumerable.Repeat((byte)0x00, bytesToCheck).ToArray();
        //    byte remainderMask = (byte)(0xFF << (8 - remainderBitsToCheck));
        //    SHA1CryptoServiceProvider sha = new SHA1CryptoServiceProvider();
        //    byte[] hash = sha.ComputeHash(Encoding.UTF8.GetBytes(HashCashVerify));
        //    var result = string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
        //    String[] substrings = HashCashVerify.Split(':');
        //    if (result.Substring(0, 3) == "000" || result.Substring(0, 3) == "111")
        //    {
        //        var hashRand = Convert.ToBase64String(Encoding.UTF8.GetBytes(HashCurrent));
        //        if (substrings[4].Equals(hashRand))
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}
        
    }
}
