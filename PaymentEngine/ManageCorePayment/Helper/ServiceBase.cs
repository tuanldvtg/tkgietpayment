using ManageCorePayment.Helper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace CorePayment.Base
{
    public class ServiceBase
    {
        public Boolean isCheckRegexQuery(string stringQuery)
        {
            Regex regex = new Regex("([a-zA-Z0-9](_|-| )[a-zA-Z0-9])*");
            return regex.IsMatch(stringQuery);
            
        }
        public Boolean validatePassword(string stringQuery)
        {
            if (stringQuery.Contains(" "))
            {
                return false;
            }
            if (stringQuery.Length > 5)
            {
                return true;
            }
            return false;

        }
        public string convertStringToSql(string stringQuery)
        {
            stringQuery = stringQuery.Replace("\u0000", "");
            stringQuery = stringQuery.Replace("\0", "");
            return stringQuery;
        }

        public SqlDataReader query(string stringQuery)
        {

            stringQuery = this.convertStringToSql(stringQuery);
            var con = DatabaseService.Instance.getConnection();
            SqlCommand cmd = new SqlCommand();

            SqlDataReader reader;
            cmd.CommandText = stringQuery;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;
            Console.WriteLine("Query: " + stringQuery);
            try
            {
                con.Open();
            }
            catch (Exception )
            {

            }
            reader = cmd.ExecuteReader();
            return reader;
        }
        public SqlDataReader querySqlParam(SqlCommand command)
        {
            command.CommandText = this.convertStringToSql(command.CommandText);
            
            DatabaseService.Instance.getConnection().Open();
            SqlDataReader reader = command.ExecuteReader();
            return reader;
        }
        public void querySqlParamInsert(SqlCommand command)
        {
            DatabaseService.Instance.getConnection().Open();
            command.ExecuteNonQuery(); 
        }

    }
}
