﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ManageCorePayment.Models;

namespace ManageCorePayment.Helper
{

    public enum SessionKeyMode {Agency = 1111, Admin = 2222,Nothing = 3333,Accounting=5555,Users = 6666}

    public class AuthenticationHelper
    {
        private HttpSessionStateBase session;

        private String sessionKeyLogined = "islogined";
        private String sessionKeyMode = "agency";
        private String sessionKeySub = "0";
        private String sessionMerchantId = "merchant_id";
        private String sessionTitle = "title";
        private String sessionMerchantIdList = "list_merchant_id";

        public AuthenticationHelper(HttpSessionStateBase session)
        {
            this.session = session;
        }

        public bool isLogined()
        {
            if(session[this.sessionKeyLogined] == null)
            {
                return false;
            }
            return true;
        }

        public int ID()
        {
            try
            {
                return Int32.Parse(this.session[this.sessionKeyLogined].ToString());
            }
            catch (Exception ) {
                throw new Exception("Session is expired");
            }
            
        }

        internal void assign(Agency agency)
        {
            session[this.sessionKeyLogined] = agency.id;
            session[this.sessionKeyMode] = SessionKeyMode.Agency;
            session[this.sessionTitle] = agency.title;
            session[this.sessionKeySub] = agency.subid;
        }
        internal void assignUser(Users users)
        {
            session[this.sessionKeyLogined] = users.id;
            session[this.sessionKeyMode] = SessionKeyMode.Users;
            session[this.sessionMerchantId] = users.merchant_id;
            session[this.sessionTitle] = users.username.Trim();
        }


        internal void assignMerchantListID(string merchant_id)
        {
            session[this.sessionMerchantIdList] = merchant_id;
        }
        public string getMerchantListIDD()
        {
            try
            {
                return this.session[this.sessionMerchantIdList].ToString();
            }
            catch (Exception )
            {
                throw new Exception("Session is expired");
            }
        }

        internal void assign(Admin agency)
        {
            session["UserID"] = agency.id;
            session[this.sessionKeyLogined] = agency.id;
            session[this.sessionTitle] = agency.name;
            if (agency.role_id == 1) { 
                session[this.sessionKeyMode] = SessionKeyMode.Admin;
            }else
            {
                session[this.sessionKeyMode] = SessionKeyMode.Accounting;
                session[this.sessionKeySub] = agency.subid;
            }


        }
        public string getMerchantID()
        {
            try
            {
                return this.session[this.sessionMerchantId].ToString();
            }
            catch (Exception )
            {
                throw new Exception("Session is expired");
            }
        }
        public string getTitle()
        {
            try
            {
                return this.session[this.sessionTitle].ToString();
            }
            catch (Exception )
            {
                throw new Exception("Session is expired");
            }
        }
        public string getSub()
        {
            try
            {
                return this.session[this.sessionKeySub].ToString();
            }
            catch (Exception )
            {
                throw new Exception("Session is expired");
            }
        }

        public SessionKeyMode getRole()
        {
            try
            {
                string roleString = this.session[this.sessionKeyMode].ToString();
                if (roleString == SessionKeyMode.Admin.ToString())
                {
                    return SessionKeyMode.Admin;
                }

                if (roleString == SessionKeyMode.Accounting.ToString())
                {
                    return SessionKeyMode.Accounting;
                }

                if (roleString == SessionKeyMode.Agency.ToString())
                {
                    return SessionKeyMode.Agency;
                }


                if (roleString == SessionKeyMode.Users.ToString())
                {
                    return SessionKeyMode.Users;
                }

                return SessionKeyMode.Nothing;
            }
            catch (Exception ) {
                throw new Exception("Session is expired");
            }

        }
        
        public void LogOut()
        {
            session[this.sessionKeyLogined] = null;
        }
    }
}