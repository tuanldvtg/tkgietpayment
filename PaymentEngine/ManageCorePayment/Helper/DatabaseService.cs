﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace ManageCorePayment.Helper
{
   public class DatabaseService
    {

            private static DatabaseService instance;

            private DatabaseService() { }
            public static DatabaseService Instance
            {
                get
                {
                    if (instance == null)
                    {
                        instance = new DatabaseService();
                    }
                    return instance;
                }
            }
            SqlConnection conn;
            internal void connect(string serverAddress)
            {
            string connetionString = serverAddress;
                conn = new SqlConnection(connetionString);
            }

            public SqlConnection getConnection()
            {
                return conn;
            }
    }
}