﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Exceptions
{
    public class AccountNotfoundException:Exception
    {
    }

    public class AccountActiveException : Exception
    {
    }

    public class AccountDeactiveException : Exception
    {

    }
}