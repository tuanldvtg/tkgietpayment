﻿using ManageCorePayment.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ManageCorePayment
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
                string cs = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
                DatabaseService.Instance.connect(cs);
                routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

                routes.MapRoute(
                    name: "Default",
                    url: "{controller}/{action}/{id}",
                    defaults: new
                    {
                        controller = "User",
                        action = "Index",
                        id = UrlParameter.Optional
                    }
                );

           
        }
    }
}
