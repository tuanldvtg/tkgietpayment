﻿using ManageCorePayment.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ManageCorePayment
{
    public class MvcApplication : System.Web.HttpApplication
    {
        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs
            Exception ex = Server.GetLastError();
            HttpContextBase context = new HttpContextWrapper(HttpContext.Current);
            RouteData rd = RouteTable.Routes.GetRouteData(context);
            string controllerName = rd.GetRequiredString("controller");
            // Log the exception and notify system operators
            using (var db = new GamePaymentContext())
            {
                db.Error_Log_App.Add(new Models.Error_Log_App()
                {
                    content_error = ex.Message,
                    controller = controllerName,
                    time_create = DateTime.Now
                });
                db.SaveChanges();
            }
            Server.ClearError();
            Server.Transfer("Default5s.aspx");
        }
        protected void Application_Start()
        {
                AreaRegistration.RegisterAllAreas();
                GlobalConfiguration.Configure(WebApiConfig.Register);
                FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
                RouteConfig.RegisterRoutes(RouteTable.Routes);
                BundleConfig.RegisterBundles(BundleTable.Bundles);

               
        }
    }
}
