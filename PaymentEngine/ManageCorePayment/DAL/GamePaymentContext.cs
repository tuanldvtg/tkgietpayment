﻿using ManageCorePayment.Models;
using ManageCorePayment.Models.Alego;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ManageCorePayment.DAL
{
    public class GamePaymentContext: DbContext
    {
        public GamePaymentContext() : base("conString")
        {
            Database.SetInitializer<GamePaymentContext>(null);
        }
        public DbSet<ActivityLog> ActivityLog { get; set; }
        public DbSet<PaymentGate> PaymentGate { get; set; }
        public DbSet<Merchant> Merchant { get; set; }
        public DbSet<MerchantUserResponse> MerchantUserResponse { get; set; }
        public DbSet<Campaign> Campaign { get; set; }
        public DbSet<Giftcode> Giftcode { get; set; }
        public DbSet<Merchant_wallet> Merchant_Wallet { get; set; }
        public DbSet<Coin> Coin { get; set; }
        public DbSet<Wallet> Wallet { set; get; }
        public DbSet<CashIORate> CashIORate { set; get; }
        public DbSet<Log_Merchant_To_Agency> Log_Merchant_To_Agency { set; get; }
        public DbSet<ManagerWallet> ManagerWallet { set; get; }
        public DbSet<Users> User { set; get; }
        public DbSet<Config> Config { set; get; }
        public DbSet<Transactions> Transactions { set; get; }
        public DbSet<Transactions_Log> Transactions_Log { set; get; }
        public DbSet<Transactions_LogVT> Transactions_LogVT { set; get; }
        public DbSet<Transactions_Log_OnePay> Transactions_Log_OnePay { set; get; }
        public DbSet<Transactions_Log_Response> Transactions_Log_Response { set; get; }
        public DbSet<Transactions_Log_ResponseVT> Transactions_Log_ResponseVT { set; get; }
        public DbSet<Transactions_Log_Response_OnePay> Transactions_Log_Response_OnePay { set; get; }
        public DbSet<Admin> Admin { set; get; }
        public DbSet<AdminMoneyTransferCommit> AdminMoneyTransferCommit { set; get; }
        public DbSet<OTP_user> OTP_user { set; get; }
        public DbSet<Agency> Agency { get; set; }
        public DbSet<Agency_social> Agency_social { get; set; }
        public DbSet<Agency_Merchant> Agency_Merchant { get; set; }
        public DbSet<AgencyMoneyTransferCommit> AgencyMoneyTransferCommit { set; get; }
        public DbSet<Rwithdrawal_Agency> Rwithdrawal_Agency { set; get; }
        public DbSet<Rwithdrawal_User> Rwithdrawal_User { set; get; }
        public DbSet<Accounting_ref_paymentgate> Accounting_ref_paymentgate { set; get; }
        public DbSet<History_ip> History_ip { get; set; }
        public DbSet<Error_Log_App> Error_Log_App { set; get; }
        public DbSet<CardBuyAlegoResponse> CardBuyAlegoResponse { set; get; }
    }
}