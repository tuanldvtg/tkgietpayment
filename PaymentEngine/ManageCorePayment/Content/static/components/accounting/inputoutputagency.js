﻿InputoutputAgency = {};

InputoutputAgency.usert_statis_id = 1;
InputoutputAgency.getListInputOut = function (id) {
    InputoutputAgency.usert_statis_id = id;
    InputoutputAgency.getListInputOut();
}

getMaxValue = function (arr) {
    return Math.max.apply(Math, arr);
}
formatMonney = function (num) {
    var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    return n;
}
formatMonney2 = function (num) {
    num = parseFloat(num);
    var n = '';

    var ti = Math.floor(num / 1000000000);
    if (ti > 0) {
        n += ti + "t";
    } else {
        ti = 0;
    }
    var tr = Math.floor((num - (ti * 1000000000)) / 1000000);
    if (tr > 0) {
        n += tr + "tr";
    } else {
        tr = 0;
    }

    var ngan = Math.floor((num - (ti * 1000000000) - (tr * 1000000)) / 1000);
    if (ngan > 0) {
        n += ngan + "n";
    } else {
        ngan = 0;
    }

    var dong = (num - (ti * 1000000000) - (tr * 1000000) - (ngan * 1000));
    if (dong > 0) {
        n += dong;
    } else {
        dong = 0;
    }

    // var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    return n;
}

InputoutputAgency.getListInputOut = function () {
    InputoutputAgency.usert_statis_id = $('#agency_id').val();

    var start_time = $('input[name="daterangepicker_start"]').val();
    var end_time = $('input[name="daterangepicker_end"]').val();
    $('#show-getinputoutput').modal('show');
    var type = $('#type').val();
    if (type == "day") {
        urlpost = "/InputoutputAgency/GetListInputOut";
    } else {
        urlpost = "/InputoutputAgency/GetListInputOutMonth";
    }
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: urlpost,
        dataType: "json",
        data: {
            id: InputoutputAgency.usert_statis_id,
            start_time: start_time,
            end_time: end_time,
        },
        success: function (json) {
            if (json.error) {
                ns_master.toastr(json.msg, "error");
                return;
            }
            $.fancybox.hideActivity();

            var data = json.history;
            var data_out = json.history_out;
            dataDailySalesChart = {
                labels: [],
                series: [[]]
            };
            dataDailySalesChart2 = {
                labels: [],
                series: [[]]
            };

            for (i = 0; i < data.length; i++) {
                var val = data[i];
                dataDailySalesChart.labels.push(val.date);
                dataDailySalesChart.series[0].push(val.value);
            }
            for (i = 0; i < data_out.length; i++) {
                var val = data_out[i];
                dataDailySalesChart2.labels.push(val.date);
                dataDailySalesChart2.series[0].push(val.value);
            }

            //series_2 = [];
            //for (i = 0; i < data_out.length; i++) {
            //    series_2.push(data_out[i].value);
            //}


            $('.inputmoney b').html(json.balance);
            $('.outputmoney b').html(json.balance_out);
            InputoutputAgency.getDataDetailTransaction(InputoutputAgency.usert_statis_id);

            var domChart = '#dailySalesChart';

            var maxValue = getMaxValue(dataDailySalesChart.series[0])

            optionsDailySalesChart = {
                lineSmooth: Chartist.Interpolation.cardinal({
                    tension: 0
                }),
                low: 0,
                high: maxValue + 50,
                chartPadding: {
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0
                },
            }

            var dailySalesChart = new Chartist.Line(domChart, dataDailySalesChart, optionsDailySalesChart);

            md.startAnimationForLineChart(dailySalesChart);
            setTimeout(function () {
                $('#dailySalesChart .ct-label.ct-vertical.ct-start').each(function () {
                    var count = $(this).html();
                    $(this).html(formatMonney2(count));

                });
                $('#dailySalesChart .ct-point').each(function () {
                    var count = $(this).attr('ct:value');
                    $(this).attr('title', formatMonney(count));
                    $(this).tooltip();
                });
            }, 500);
            //CHART 2
            var domChart2 = '#dailySalesChart2';

            var maxValue = getMaxValue(dataDailySalesChart2.series[0]);
            optionsDailySalesChart2 = {
                lineSmooth: Chartist.Interpolation.cardinal({
                    tension: 0
                }),
                low: 0,
                high: maxValue + 50,
                chartPadding: {
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0
                },
            }
            var dailySalesChart2 = new Chartist.Line(domChart2, dataDailySalesChart2, optionsDailySalesChart2);
            
            md.startAnimationForLineChart(dailySalesChart2);
            setTimeout(function () {
                $('#dailySalesChart2 .ct-label.ct-vertical.ct-start').each(function () {
                    var count = $(this).html();
                    $(this).html(formatMonney2(count));

                });
                $('#dailySalesChart2 .ct-point').each(function () {
                    var count = $(this).attr('ct:value');
                    $(this).attr('title', formatMonney(count));
                    $(this).tooltip();
                });
            }, 500);

            //line
            //Chart.defaults.global.defaultFontColor = '#333';
            //var ctxL = document.getElementById("lineChart").getContext('2d');
            //var myLineChart = new Chart(ctxL, {
            //    type: 'line',
            //    data: {
            //        labels: dataDailySalesChart.labels,
            //        datasets: [
            //            {
            //                label: "Nạp",
            //                backgroundColor: "rgba(255,99,132,0.2)",
            //                fillColor: "rgba(255,99,132,0.2)",
            //                strokeColor: "rgba(255,99,132,0.2)",
            //                pointColor: "rgba(255,99,132,0.2)",
            //                pointStrokeColor: "#fff",
            //                pointHighlightFill: "#fff",
            //                pointHighlightStroke: "rgba(255,99,132,0.2)",
            //                data: dataDailySalesChart.series[0]
            //            }
            //        ]
            //    },
            //    options: {
            //        responsive: true
            //    }
            //});
            //var ctxL = document.getElementById("lineChart2").getContext('2d');
            //var myLineChart = new Chart(ctxL, {
            //    type: 'line',
            //    data: {
            //        labels: dataDailySalesChart.labels,
            //        datasets: [
            //            {
            //                label: "Rút",
            //                backgroundColor: "rgba(54,162,235,0.2)",
            //                fillColor: "rgba(151,187,205,0.2)",
            //                strokeColor: "rgba(151,187,205,1)",
            //                pointColor: "rgba(151,187,205,1)",
            //                pointStrokeColor: "#fff",
            //                pointHighlightFill: "#fff",
            //                pointHighlightStroke: "rgba(151,187,205,1)",
            //                data: series_2
            //            }
            //        ]
            //    },
            //    options: {
            //        responsive: true
            //    }
            //});
        }
    });

}

InputoutputAgency.getDataDetailTransaction = function (id) {
    var page_start = $('#datatable-history .pagination .paginate_button.current > a').html();
    //var lengh = $('select[name="datatable-labelManagement_length"]').val();
    var page = (page_start - 1) * 10;
    if (!page) {
        page = 0;
    }

    var start_time = $('input[name="daterangepicker_start"]').val();
    var end_time = $('input[name="daterangepicker_end"]').val();
    $('#datatable-history-detail').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "displayStart": page,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

        },
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "id", "value": $('#agency_id').val() },
                { "name": "start_time", "value": start_time },
                { "name": "end_time", "value": end_time }
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/InputoutputAgency/getDataDetailTransaction",
        "aaSorting": [[0, "desc"]]
    });
}


$(function () {
    $('#daterange-btn').daterangepicker(
        {
            ranges: {
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(6, 'days'),
            endDate: moment()
        },
        function (start, end) {
            $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        }
    );
    InputoutputAgency.getListInputOut();
});