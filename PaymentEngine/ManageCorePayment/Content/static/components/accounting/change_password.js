﻿define({

    data: {
        toast: {
            error: false,
            msg: ""
        },
        present: {
            title: "Tạo mới cổng",
            btn_title: "Tạo mới cổng"
        },
        items: [],
        form: {
            oldpass: "",
            password: "",
            repass:""
        },
        wallets: [],
        socials:[]
    },

    // Methods
    methods: {
        doUpdate: function () {
            if (!this.form.password) {
                this.toast = {
                    error: true,
                    msg: "Vui lòng  nhập mật khẩu"
                }
                return;
            }
            if (this.form.password != this.form.repass) {
                this.toast = {
                    error: true,
                    msg : "Mật khẩu nhập lại phải giống nhau"
                }
                return;
            }
            comp = this;
            $.post('/AccountingAPI/ChangePassword', this.form, function (json) {
                // Show if error
                if (json.error) {
                    comp.toast = json;
                } else {
                    comp.toast = {
                        success: true,
                        msg: "Cập nhật thông tin thành công"
                    }
                }
            }, 'json');
            
        },
        
    },
    // Autoload
    created: function () {

    }
});