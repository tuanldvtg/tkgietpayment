﻿define({
    data: {
        toast: {
            error: false,
            msg: ""
        },
        wallets: [],
        transaction_history: [],
        wallets_id : 0
    },
    // Methods
    methods: {
        findataTabel: function () {
            users_dashboard.showTransactionHistory(users_dashboard.wallets_id,"find");
        },
        showTransactionHistory: function (walletid, change) {
            users_dashboard.wallets_id = walletid;
            $('#content-history-wallet').show();
            var position = $('#content-history-wallet').position();
            $(window).scrollTop(position.top);
            var page_start = $('.paginate_button.current').html();
            //var lengh = $('select[name="datatable-labelManagement_length"]').val();
            var page = (page_start - 1) * 10;
            if (!page) {
                page = 0;
            }
            if (change) {
                page = 0;
            }
            $('#user-merchant').dataTable({
                dom: '<"top"i>rt<"bottom"flp><"clear">',
                tableTools: {
                    "aButtons": []
                },
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "bLengthChange": true,
                "bFilter": false,
                'displayStart': page,
                "bSort": true,
                "bInfo": false,
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    nRow.setAttribute('id', aData.RowOrder);

                },
                "fnServerParams": function (aoData) {
                    aoData.push(
                        {
                            "name": "walletid", "value": walletid
                        },
                        {
                            "name": "tag_type", "value": $('#form-merchant').val()
                        }
                    );
                },
                "sServerMethod": "POST",
                "sAjaxSource": "/UsersCreatePayAPI/History",
                "aaSorting": [[0, "desc"]]
            });
            return;


            comp = this;
            //console.log("Get merchants");
            $.fancybox.showActivity();
            $.post('/UsersCreatePayAPI/History', {
                "walletid": walletid
            }, function (json) {
                $.fancybox.hideActivity();
                // Show if error
                if (json.error) {
                    comp.toast = json;
                } else {
                    comp.transaction_history = json.items;
                }
            }, 'json');

        },
        clickChangeTag: function (id, walletid) {
            $.fancybox.showActivity();
            $.post('/UsersCreatePayAPI/changeTag', {
                "id": id
            }, function (json) {
                $.fancybox.hideActivity();
                // Show if error
                if (json.error) {
                    ns_master.toastr(json.msg, "error");
                } else {
                    ns_master.toastr("Thực hiện thành công", "success");
                    users_dashboard.showTransactionHistory(walletid);
                }
            }, 'json');

        },
        clickChangeTagRemove: function (id, walletid) {
            $.fancybox.showActivity();
            $.post('/UsersCreatePayAPI/clickChangeTagRemove', {
                "id": id
            }, function (json) {
                $.fancybox.hideActivity();
                // Show if error
                if (json.error) {
                    ns_master.toastr(json.msg, "error");
                } else {
                    ns_master.toastr("Thực hiện thành công", "success");
                    users_dashboard.showTransactionHistory(walletid);
                }
            }, 'json');

        }

    },

    
    // Created
    created: function () {

        getMaxValue = function (arr) {
            return Math.max.apply(Math, arr);
        }
        formatMonney2 = function (num) {
            num = parseFloat(num);
            var n = '';

            var ti = Math.floor(num / 1000000000);
            if (ti > 0) {
                n += ti + "t";
            } else {
                ti = 0;
            }
            var tr = Math.floor((num - (ti * 1000000000)) / 1000000);
            if (tr > 0) {
                n += tr + "tr";
            } else {
                tr = 0;
            }

            var ngan = Math.floor((num - (ti * 1000000000) - (tr * 1000000) ) / 1000);
            if (ngan > 0) {
                n += ngan + "n";
            } else {
                ngan = 0;
            }

            var dong = (num - (ti * 1000000000) - (tr * 1000000) - (ngan * 1000) );
            if (dong > 0) {
                n += dong;
            } else {
                dong = 0;
            }

           // var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            return n;
        }

        formatMonney = function (num) {
  
            var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            return n;
        }
        createChart = function (wallet) {
            setTimeout(function () {
                var domChart = ['#dailySalesChart_', wallet.id].join('');
                var data = wallet.history.reverse();

                dataDailySalesChart = {
                    labels: [],
                    series: [[]]
                };

                for (i = 0; i < data.length; i++) {
                    var val = data[i];
                    dataDailySalesChart.labels.push(val.date);
                    dataDailySalesChart.series[0].push(val.value);
                }

                var maxValue = getMaxValue(dataDailySalesChart.series[0])

                optionsDailySalesChart = {
                    lineSmooth: Chartist.Interpolation.cardinal({
                        tension: 0
                    }),
                    low: 0,
                    high: maxValue + 50,
                    chartPadding: {
                        top: 0,
                        right: 0,
                        bottom: 0,
                        left: 0
                    },
                }

                var dailySalesChart = new Chartist.Line(domChart, dataDailySalesChart, optionsDailySalesChart);

                md.startAnimationForLineChart(dailySalesChart);
                setTimeout(function () {
                    $('#dailySalesChart_' + wallet.id+' .ct-label.ct-vertical.ct-start').each(function () {
                        var count = $(this).html();
                        $(this).html(formatMonney2(count));

                    });
                    $('#dailySalesChart_' + wallet.id + ' .ct-point').each(function () {
                        var count = $(this).attr('ct:value');
                        $(this).attr('title', formatMonney2(count));
                        $(this).tooltip();
                    });
                }, 500);
            }, 1000);
        }


        comp = this;
        //console.log("Get merchants");
        $.fancybox.showActivity();
        $.post('/UsersAccountAPI/Wallets', this.form, function (json) {
            $.fancybox.hideActivity();
            // Show if error
            if (json.error) {
                comp.toast = json;
            } else {
                comp.wallets = json.items;
                for (i = 0; i < json.items.length; i++) {
                    var ele = json.items[i];
                    createChart(ele);
                }
            }
        }, 'json');
    }
});