﻿define({
    data: {
        form: {
            money: "0",
            id: -1
        },
        balance: 0,
        balance_out:0
    },

    // Methods
    methods: {
        formatMonney : function (num) {
            var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            return n;
        },
        onCoinPress: function () {
            var price = rwithdrawuser.form.money;
            var a_pay = price.split(',').join('');
            rwithdrawuser.form.money = rwithdrawuser.formatMonney(a_pay);
        },

        confirmTransactions: function () {
            if (!rwithdrawuser.form.code) {
                ns_master.toastr("Vui lòng điền mã giao dịch", "error");
                return;
            }
            $.ajax({
                type: 'POST',
                url: "/rwithdrawuserUserAPI/getTransactions",
                dataType: "json",
                data: {
                    code: rwithdrawuser.form.code,
                    agencyid: withdrawal_user.agencyid
                },
                success: function (data) {
                    $.fancybox.hideActivity();
                    if (data.error) {
                        ns_master.toastr(data.msg, "error");
                        rwithdrawuser.form.id = 0;
                    } else {
                        rwithdrawuser.form = data.item;
                        rwithdrawuser.form.value = rwithdrawuser.formatMonney(rwithdrawuser.form.value);
                        rwithdrawuser.form.value_out = rwithdrawuser.formatMonney(rwithdrawuser.form.value_out);
                    }
                }
            });
        },

        confirmSend: function () {
            if (rwithdrawuser.form.id == 0 || rwithdrawuser.form.id == -1) {
                ns_master.toastr("Thông tin giao dịch chưa được xác định", "error");
                return;
            }
            $('#show-edit').modal("show");
            $('#create-rwithdrawuser').modal("hide");
        },

        send: function () {
            if (rwithdrawuser.form.id == 0 || rwithdrawuser.form.id == -1) {
                ns_master.toastr("Thông tin giao dịch chưa được xác định", "error");
                return;
            }
            $.fancybox.showActivity();
            $.ajax({
                type: 'POST',
                url: "/rwithdrawuserUserAPI/confirmRwithdrawuser",
                dataType: "json",
                data: {
                    id: rwithdrawuser.form.id,
                    password: $('.txt-password').val().trim() 
                },
                success: function (data) {
                    $.fancybox.hideActivity();
                    if (data.error) {
                        ns_master.toastr(data.msg, "error");
                    } else {
                        ns_master.toastr("Thực hiện thành công", "success");
                        $('#show-edit').modal("hide");
                       
                        withdrawal_user.dataTable();
                    }
                    
                }
            });

        }

    },

});