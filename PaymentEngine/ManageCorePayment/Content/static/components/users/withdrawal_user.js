﻿withdrawal_user = {};
withdrawal_user.agencyid = 0;
withdrawal_user.showCreate = function (agencyid) {
    $('#create-rwithdrawuser').modal("show");
    withdrawal_user.agencyid = agencyid;
};

withdrawal_user.dataTable = function (change) {
    var page_start = $('.paginate_button.current').html();
    //var lengh = $('select[name="datatable-labelManagement_length"]').val();
    var page = (page_start - 1) * 10;
    if (!page) {
        page = 0;
    }
    if (change) {
        page = 0;
    }
    var start_time = $('input[name="daterangepicker_start"]').val();
    var end_time = $('input[name="daterangepicker_end"]').val();
    $('#user-merchant').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "displayStart": page,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

        },
        "fnServerParams": function (aoData) { 
            aoData.push(
                { "name": "filter", "value": $('#filter').val() },
                { "name": "search_note", "value": $('#search_note').val() }
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/rwithdrawuserUserAPI/listdata",
        "aaSorting": [[0, "desc"]]
    });
};
withdrawal_user.dataTableCode = function (change) {
    var page_start = $('.paginate_button.current').html();
    //var lengh = $('select[name="datatable-labelManagement_length"]').val();
    var page = (page_start - 1) * 10;
    if (!page) {
        page = 0;
    }
    if (change) {
        page = 0;
    }
    var start_time = $('input[name="daterangepicker_start"]').val();
    var end_time = $('input[name="daterangepicker_end"]').val();
    $('#user-merchant-code').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "displayStart": page,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

        },
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "filter", "value": $('#filter').val() },
                { "name": "search_note", "value": $('#search_note').val() }
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/rwithdrawuserUserAPI/listdataCode",
        "aaSorting": [[0, "desc"]]
    });
};

$(function () {
    withdrawal_user.dataTable();
    withdrawal_user.dataTableCode();
});

