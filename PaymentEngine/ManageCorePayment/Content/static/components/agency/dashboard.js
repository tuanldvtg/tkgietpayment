﻿define({
    data: {
        toast: {
            error: false,
            msg: ""
        },
        wallets: [],
        transaction_history: [],
        wallets_id : 0
    },
    // Methods
    methods: {
        findataTabel: function () {
            agency_dashboard.showTransactionHistory(agency_dashboard.wallets_id,"find");
        },
       
        showTransactionHistory: function (walletid, change) {
            agency_dashboard.wallets_id = walletid;
            $('#content-history-wallet').show();
            var position = $('#content-history-wallet').position();
            $(window).scrollTop(position.top);
            var page_start = $('.paginate_button.current').html();
            //var lengh = $('select[name="datatable-labelManagement_length"]').val();
            var page = (page_start - 1) * 10;
            if (!page) {
                page = 0;
            }
            if (change) {
                page = 0;
            }
            console.log("walletid: " + walletid + " | " + $('#form-merchant').val());
            $('#user-merchant').dataTable({
                dom: '<"top"i>rt<"bottom"flp><"clear">',
                tableTools: {
                    "aButtons": []
                },
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "bLengthChange": true,
                "bFilter": false,
                'displayStart': page,
                "bSort": true,
                "bInfo": false,
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    nRow.setAttribute('id', aData.RowOrder);

                },
                "fnServerParams": function (aoData) {
                    aoData.push(
                        {
                            "name": "walletid", "value": walletid
                        },
                        {
                            "name": "tag_type", "value": $('#form-merchant').val()
                        }
                    );
                },
                "sServerMethod": "POST",
                "sAjaxSource": "/AgencyCreatePayAPI/History",
                "aaSorting": [[0, "desc"]]
            });
            return;


            comp = this;
            //console.log("Get merchants");
            $.fancybox.showActivity();
            $.post('/AgencyCreatePayAPI/History', {
                "walletid": walletid
            }, function (json) {
                $.fancybox.hideActivity();
                // Show if error
                if (json.error) {
                    comp.toast = json;
                } else {
                    comp.transaction_history = json.items;
                    $('#analys_transaction').hide();
                }
            }, 'json');

        },

        analysTran: function (change) {
            var page_start = $('.pagination .paginate_button.current > a').html();
            //var lengh = $('select[name="datatable-labelManagement_length"]').val();
            var page = (page_start - 1) * 10;
            if (!page) {
                page = 0;
            }
            if (change) {
                page = 0;
            }
            var start_time = $('input[name="daterangepicker_start"]').val();
            var end_time = $('input[name="daterangepicker_end"]').val();
            //$('#show-getinputoutput').modal('show');
            naprut = $('#form-merchant1').val();
            if (type == "") {
                naprut = 0;
            }
            //$.fancybox.showActivity();
            $('#lst_transaction').dataTable({
                dom: '<"top"i>rt<"bottom"flp><"clear">',
                tableTools: {
                    "aButtons": []
                },
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "bLengthChange": true,
                "bFilter": false,
                'displayStart': page,
                "bSort": true,
                "bInfo": false,
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    nRow.setAttribute('id', aData.RowOrder);
                },
                "fnServerParams": function (aoData) {
                    aoData.push(
                        {
                            "name": "naprut", "value": naprut
                        },
                        {
                            "name": "start_time", "value": start_time
                        },
                        {
                            "name": "end_time", "value": end_time
                        }
                    );
                },
                "sServerMethod": "POST",
                "sAjaxSource": "/AgencyCreatePayAPI/HistoryTransaction",
                "aaSorting": [[0, "desc"]]
            });
        },

        clickChangeTag: function (id, walletid) {
            $.fancybox.showActivity();
            $.post('/AgencyCreatePayAPI/changeTag', {
                "id": id
            }, function (json) {
                $.fancybox.hideActivity();
                // Show if error
                if (json.error) {
                    ns_master.toastr(json.msg, "error");
                } else {
                    ns_master.toastr("Thực hiện thành công", "success");
                    agency_dashboard.showTransactionHistory(walletid);
                }
            }, 'json');

        },
        clickChangeTagRemove: function (id, walletid) {
            $.fancybox.showActivity();
            $.post('/AgencyCreatePayAPI/clickChangeTagRemove', {
                "id": id
            }, function (json) {
                $.fancybox.hideActivity();
                // Show if error
                if (json.error) {
                    ns_master.toastr(json.msg, "error");
                } else {
                    ns_master.toastr("Thực hiện thành công", "success");
                    agency_dashboard.showTransactionHistory(walletid);
                }
            }, 'json');

        },
        onloadFunction : function () {
            //console.log("Get merchants");
            var start_time = $('input[name="daterangepicker_start"]').val();
            var end_time = $('input[name="daterangepicker_end"]').val();
            $('#show-getinputoutput').modal('show');
            //var type = $('#type').val();
            //if (type == "day") {
            //    urlpost = "/AgencyAccountAPI/Wallets";
            //} else {
            //    urlpost = "/AgencyAccountAPI/WalletsMonth";
            //}
            $.fancybox.showActivity();
            $.post("/AgencyAccountAPI/Wallets", {
                start_time: start_time,
                end_time: end_time
            }, function (json) {
                $.fancybox.hideActivity();
                // Show if error
                if (json.error) {
                    ns_master.toastr(json.msg, "error");
                } else {
                    agency_dashboard.wallets = json.items;
                    for (i = 0; i < json.items.length; i++) {
                        var ele = json.items[i];
                        //createChart(ele);
                    }
                }
            }, 'json');
        },
        loadDataTransction: function () {
            //console.log("Get merchants");
            var start_time = $('input[name="daterangepicker_start"]').val();
            var end_time = $('input[name="daterangepicker_end"]').val();
            //$('#show-getinputoutput').modal('show');
            //var type = $('#type').val();
            //if (type == "day") {
            //    urlpost = "/AgencyAccountAPI/WalletsNew";
            //} else {
            //    urlpost = "/AgencyAccountAPI/WalletsMonthNew";
            //}
            console.log("start_time: " + start_time + " end_time: " + end_time + " type: " + type);
            $.fancybox.showActivity();
            $.post("/AgencyAccountAPI/WalletsNew", {
                start_time: start_time,
                end_time: end_time
            }, function (json) {
                $.fancybox.hideActivity();
                // Show if error
                if (json.error) {
                    ns_master.toastr(json.msg, "error");
                } else {
                    ns_master.toastr(json.msg, "success");
                    $('#tongnap').html("Tổng nạp: " + formatMonney(json.tongNap));
                    $('#tongrut').html("Tổng rút: " + formatMonney(json.tongRut));
                    $('#analys_transaction').show();
                    $('#content-history-wallet').hide();
                }
            }, 'json');
        }
    },

    
    // Created
    created: function () {

        getMaxValue = function (arr) {
            return Math.max.apply(Math, arr);
        }
        formatMonney = function (num) {
            var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            return n;
        }
        formatMonney2 = function (num) {
            num = parseFloat(num);
            var n = '';

            var ti = Math.floor(num / 1000000000);
            if (ti > 0) {
                n += ti + "t";
            } else {
                ti = 0;
            }
            var tr = Math.floor((num - (ti * 1000000000)) / 1000000);
            if (tr > 0) {
                n += tr + "tr";
            } else {
                tr = 0;
            }

            var ngan = Math.floor((num - (ti * 1000000000) - (tr * 1000000)) / 1000);
            if (ngan > 0) {
                n += ngan + "n";
            } else {
                ngan = 0;
            }

            var dong = (num - (ti * 1000000000) - (tr * 1000000) - (ngan * 1000));
            if (dong > 0) {
                n += dong;
            } else {
                dong = 0;
            }

            // var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            return n;
        }
        //createChart = function (wallet) {
        //    setTimeout(function () {
        //        var domChart = ['#dailySalesChart_', wallet.id].join('');
        //        var data = wallet.history;

        //        dataDailySalesChart = {
        //            labels: [],
        //            series: [[]]
        //        };

        //        for (i = 0; i < data.length; i++) {
        //            var val = data[i];
        //            dataDailySalesChart.labels.push(val.date);
        //            dataDailySalesChart.series[0].push(val.value);
        //        }

        //        var maxValue = getMaxValue(dataDailySalesChart.series[0])

        //        optionsDailySalesChart = {
        //            lineSmooth: Chartist.Interpolation.cardinal({
        //                tension: 0
        //            }),
        //            low: 0,
        //            high: maxValue + 50,
        //            chartPadding: {
        //                top: 0,
        //                right: 0,
        //                bottom: 0,
        //                left: 0
        //            },
        //        }

        //        var dailySalesChart = new Chartist.Line(domChart, dataDailySalesChart, optionsDailySalesChart);

        //        md.startAnimationForLineChart(dailySalesChart);
        //        setTimeout(function () {
        //            $('#dailySalesChart_' + wallet.id + ' .ct-label.ct-vertical.ct-start').each(function () {
        //                var count = $(this).html();
        //                $(this).html(formatMonney2(count));

        //            });
        //            $('#dailySalesChart_' + wallet.id + ' .ct-point').each(function () {
        //                var count = $(this).attr('ct:value');
        //                $(this).attr('title', formatMonney(count));
        //                $(this).tooltip();
        //            });
        //        }, 500);
        //    }, 1000);
        //}

    
        comp = this;
        //console.log("Get merchants");
        var start_time = $('input[name="daterangepicker_start"]').val();
        var end_time = $('input[name="daterangepicker_end"]').val();
        $('#show-getinputoutput').modal('show');
        //var type = $('#type').val();
        //if (type == "day") {
        //    urlpost = "/AgencyAccountAPI/Wallets";
        //} else {
        //    urlpost = "/AgencyAccountAPI/WalletsMonth";
        //}

        $.fancybox.showActivity();
        $.post("/AgencyAccountAPI/Wallets", {
            start_time: start_time,
            end_time: end_time
        }, function (json) {
            $.fancybox.hideActivity();
            // Show if error
            if (json.error) {
                comp.toast = json;
            } else {
                comp.wallets = json.items;
                for (i = 0; i < json.items.length; i++) {
                    var ele = json.items[i];
                    //createChart(ele);
                }
                //datatable();
            }
        }, 'json');
       
    }
});