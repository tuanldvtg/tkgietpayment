﻿define({
    data: {
        loading:true,
        toast: {
            error: false,
            msg: ""
        },
        items: [{ id: "", "merchant": {"id":0,"title":""},"title":"","status":""}],
        form: {
            kw : ""
        },
    },

    // Methods
    methods: {

        showAdd: function () {
            create_payment_gate.id = "";
            create_payment_gate.present = {
                title: "Thêm mới cổng",
                btn_title: "Thêm mới"
            };
            $('#vueAppPaymentGateInfo').modal('show');
        },

        doEdit: function (id) {
            $.post('/AgencyPaymentGateAPI/Detail', {
                id:id
            }, function (json) {
                // Show if error
                if (json.error) {
                    payment_gates.toast = json;
                } else {

                    create_payment_gate.form = json.item;
                    create_payment_gate.wallets = json.wallets;

                    create_payment_gate.items = [];
                    var rateIO = json.rateIO;
                    rateIO.forEach(function (item) {
                        s_cash_in = create_payment_gate.formatMonney(item.cash_in);

                        s_cash_out = create_payment_gate.formatMonney(item.cash_out);

                        create_payment_gate.items.push({
                            "cash_in": s_cash_in,
                            "cash_out": s_cash_out
                        });
                        create_payment_gate.items_post.push({
                            "cash_in": item.cash_in,
                            "cash_out": item.cash_out
                        });
                    });

                    create_payment_gate.sms = json.sms;
                    create_payment_gate.present = {
                        title: json.item.title,
                        btn_title: "Cập nhật"
                    };
                    create_payment_gate.id = id;
                    $('#vueAppPaymentGateInfo').modal('show');
                }
            }, 'json');
        },

        doUpdate: function () {
            //comp = this;
            $.post('/AgencyPaymentGateAPI/ListAll', this.form, function (json) {
                // Show if error
                if (json.error) {
                    payment_gates.toast = json;
                } else {
                    payment_gates.items = json.items;
                    payment_gates.loading = false;
                    //comp.$forceUpdate();
                }
            }, 'json');
        },

        doLock: function (item) {
            $.post('/AgencyPaymentGateAPI/Active', {
                id: item.id,
                active:false
            }, function (json) {
                // Show if error
                if (json.error) {
                    alert(json.msg);
                } else {
                    item.status = "unactive";
                }
            }, 'json');

        },

        doUnLock: function (item) {
            $.post('/AgencyPaymentGateAPI/Active', {
                id: item.id,
                active: true
            }, function (json) {
                // Show if error
                if (json.error) {
                    alert(json.msg);
                } else {
                    item.status = "active";
                }
            }, 'json');
        }
    },

    // Autoload
    created: function () {
        this.doUpdate();
    }
});