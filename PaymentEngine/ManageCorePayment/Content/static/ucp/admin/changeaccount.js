﻿account = {};
account.savePassword = function () {
    var password_old = $('.password-old').val();
    var password = $('.password').val();
    var password_re = $('.password-re').val();
    if (!password_old) {
        ns_master.toastr("Vui lòng nhập mật khẩu cũ.", "error");
        return;
    }
    if (!password) {
        ns_master.toastr("Vui lòng nhập mật khẩu mới.", "error");
        return;
    }

    if (password != password_re) {
        ns_master.toastr("Nhập lại mật khẩu không đúng.", "error");
        return;
    }

    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Account/editPasswordAccount",
        dataType: "json",
        data: {
            password: password,
            password_old: password_old
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
              
                ns_master.toastr("Thực hiện thành công", "success");
            }
            
        }
    });

}