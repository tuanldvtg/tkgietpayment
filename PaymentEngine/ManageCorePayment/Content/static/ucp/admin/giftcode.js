﻿giftcode = {};
giftcode.dataTable = function (change) {
    var page_start = $('.pagination .paginate_button.current > a').html();
    //var lengh = $('select[name="datatable-labelManagement_length"]').val();
    var page = (page_start - 1) * 10;
    if (!page) {
        page = 0;
    }
    if (change) {
        page = 0;
    }
    $('#datatable').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

        },
        "fnServerParams": function (aoData) {
            aoData.push(
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/Giftcode/Listdata",
        "aaSorting": [[0, "desc"]]
    });
}

giftcode.deleteCampaign = function (id) {
    $('#modal-confirm').modal("show");
    $('#action-confirm').attr('onclick', 'giftcode.actionDelete(' + id + ')');
}

giftcode.actionDelete = function (id) {
    $.post('/Giftcode/deleteCampaign', {
        id: id
    }, function (json) {
        // Show if error
        if (json.error) {
            ns_master.toastr(json.msg, "error");
        } else {
            ns_master.toastr(json.msg, "success");
            giftcode.dataTable();
        }
    }, 'json');
};

giftcode.deleteGiftcode = function (id) {
    $('#modal-confirm').modal("show");
    $('#action-confirm').attr('onclick', 'giftcode.actionDeleteGiftcode(' + id + ')');
}

giftcode.actionDeleteGiftcode = function (id) {
    $.post('/Giftcode/deleteGiftcode', {
        id: id
    }, function (json) {
        // Show if error
        if (json.error) {
            ns_master.toastr(json.msg, "error");
        } else {
            ns_master.toastr(json.msg, "success");
            giftcode.dataTable();
        }
    }, 'json');
};

giftcode.isClick = 0;
giftcode.showGiftcode = function (id) {
    $('#lo').modal("show");
    giftcode.isClick = id;
    $('#lodatatable').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

        },
        "fnServerParams": function (aoData) {
            aoData.push(
                 { "name": "id", "value": giftcode.isClick }
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/Giftcode/ListdataGiftcode",
        "aaSorting": [[0, "desc"]]
    });
}

$(function () {
    create_payment_gate = new Vue({
        el: '#vueAppGiftcodeInfo',
            data: {
                toast: {
                    error: false,
                    msg: ""
                },
                present: {
                    title: "Tạo mới chiến dịch",
                    btn_title: "Tạo mới chiến dịch"
                },
                items: [],
                form: {
                    merchant: 0,
                    name:"",
                    start_time: "",
                    end_time: ""
                },
                wallets: [],
                merchants: [],
            },
            // Methods
            methods: {
                formatMonney: function (num) {

                    var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                    return n;
                },
                // Btn add rule click
                addRule: function () {
                    this.items.push({
                        "cash_in": this.item.cash_in,
                        "cash_out": this.item.cash_out
                    });
                    this.item.cash_in = "";
                    this.item.cash_out = "";
                },
                onCoinPressIn: function () {
                    s_money = this.item.cash_in;
                    var a_money = s_money.split(",");
                    s_money = a_money.join("");

                    this.item.cash_in = this.formatMonney(s_money);
                },
                onCoinPressOut: function () {
                    s_money = this.item.cash_out;
                    var a_money = s_money.split(",");
                    s_money = a_money.join("");

                    this.item.cash_out = this.formatMonney(s_money);
                },

                // Remove rule
                removeValueCoin: function (index) {
                    console.log(index);
                    this.items.splice(index, 1);
                },

                // On merchant select
                onMerchantSelected: function () {
                    comp = this;
                    $.post("/AgencyPaymentGateAPI/WalletByMerchant", {
                        merchant_id: this.form.merchant
                    }, function (json) {
                        if (!json.error) {
                            comp.wallets = json.items;
                        }
                    }, 'json');
                },

                // Submit - Do submit new wallet to system
                onSubmit: function () {
                    if ($('#check_sms').is(':checked')) {
                        sms = "1";
                    } else {
                        sms = "0";
                    }
                    var endpoint = (this.id) ? "/Giftcode/Update" : "/Giftcode/Create";
                    if (this.form.code == "") {
                        ns_master.toastr("Vui lòng điền mã code", "error");
                        return;
                    }
                    if (this.form.name == "") {
                        ns_master.toastr("Vui lòng điền tên", "error");
                        return;
                    }
                    if (this.items.length == 0) {
                        ns_master.toastr("Vui lòng điền config tỉ giá", "error");
                        return;
                    }
                    create_payment_gate.items_post = [];
                    var check_in_out = false;
                    this.items.forEach(function (item) {
                        var cash_in = item.cash_in;
                        var a_cash_in = cash_in.split(",");
                        s_cash_in = a_cash_in.join("");


                        var cash_out = item.cash_out;
                        var a_cash_out = cash_out.split(",");
                        s_cash_out = a_cash_out.join("");


                        if (parseFloat(s_cash_in) < parseFloat(s_cash_out)) {
                            check_in_out = true;
                        }

                        create_payment_gate.items_post.push({
                            "cash_in": s_cash_in,
                            "cash_out": s_cash_out
                        });
                    });
                    var s_money = create_payment_gate.form.fee;
                    console.log(s_money)
                    var a_money = s_money.split(",");
                    s_money = a_money.join(""); 
                    $.post(endpoint, {
                        name: this.form.name,
                        sms: sms,
                        iorate: this.items_post,
                        type: this.form.type,
                        id: this.id,
                    }, function (json) {
                        if (json.error) {
                            alert(json.msg);
                        } else {
                            // Close dialog
                            $('#vueAppGiftcodeInfo').modal('hide');
                            giftcode.dataTable();
                        }
                    }, 'json');
                }
            },

            // Created
            created: function () {
                comp = this;
                //console.log("Get merchants");
                $.post('/AgencyPaymentGateAPI/Merchants', this.form, function (json) {
                    // Show if error
                    if (json.error) {
                        comp.toast = json;
                    } else {
                        comp.merchants = json.items;
                    }
                }, 'json');
                // Gete all manager wallet manager_wallets
                $.post('/Giftcode/WalletsIn', this.form, function (json) {
                    // Show if error
                    if (json.error) {
                        comp.toast = json;
                    } else {
                        comp.manager_wallets = json.items;
                    }
                }, 'json');
            }
    });

    giftcode.dataTable();

});