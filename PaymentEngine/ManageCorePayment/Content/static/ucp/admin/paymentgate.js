﻿paymentgate = {};
paymentgate.dataTable = function (change) {
    var page_start = $('.pagination .paginate_button.current > a').html();
    //var lengh = $('select[name="datatable-labelManagement_length"]').val();
    var page = (page_start - 1) * 10;
    if (!page) {
        page = 0;
    }
    if (change) {
        page = 0;
    }

    $('#datatable').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

        },
        "fnServerParams": function (aoData) {
            aoData.push(
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/Paymentgate/Listdata",
        "aaSorting": [[0, "desc"]]
    });
}

paymentgate.showEditActive = function (id, e) {
    $('#edit-active').modal("show");

    var avtice = $(e).attr('data-active');
    $('#edit-active type[type="radio"]').prop('checked', false);
    $('#' + avtice).prop('checked', true);
    $('#edit-active #action-confirm').attr("onclick", "paymentgate.actionEditActive(" + id + ")");
    $('#edit-active .modal-title').html("Sửa");
}

paymentgate.actionEditActive = function (id) {
    var status = "active";
    if ($('#active').is(":checked")) {
        status = "active";
    } else {
        status = "deactive";
    }
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Paymentgate/postActive",
        dataType: "json",
        data: {
            id: id,
            status: status,
            note: $('.note').val()
        },
        success: function (data) {
            $.fancybox.hideActivity();
            paymentgate.dataTable();
            $('#edit-active').modal("hide");
        }
    });
}




paymentgate.showEdit = function (id) {
    $.post('/Paymentgate/Detail', {
        id: id
    }, function (json) {
        // Show if error
        if (json.error) {
            payment_gates.toast = json;
        } else {

            create_payment_gate.form = json.item;
            create_payment_gate.wallets = json.wallets;

            create_payment_gate.items = [];
            var rateIO = json.rateIO;
            rateIO.forEach(function (item) {
                s_cash_in = create_payment_gate.formatMonney(item.cash_in);

                s_cash_out = create_payment_gate.formatMonney(item.cash_out);

                create_payment_gate.items.push({
                    "cash_in": s_cash_in,
                    "cash_out": s_cash_out
                });
            });



            create_payment_gate.present = {
                title: json.item.title,
                btn_title: "Cập nhật"
            };
            create_payment_gate.id = id;
            $('#vueAppPaymentGateInfo').modal('show');
        }
    }, 'json');
}

paymentgate.showDelete = function(id){
    $('#modal-confirm').modal("show");
    $('#action-confirm').attr('onclick', 'paymentgate.actionDelete(' + id + ')');
}

paymentgate.actionDelete = function (id) {
    $.post('/Paymentgate/Delete', {
        id: id
    }, function (json) {
        // Show if error
        if (json.error) {
            ns_master.toastr(json.msg, "error");
        } else {
            paymentgate.dataTable();
        }
    }, 'json');
};

paymentgate.loadHistoryByTime = function () {
    
     var start_time = $('input[name="daterangepicker_start"]').val();
     var end_time = $('input[name="daterangepicker_end"]').val();
     paymentgate.loadStaticMoney(paymentgate.isClick, start_time, end_time);
     $('#datatable-history').dataTable({
         dom: '<"top"i>rt<"bottom"flp><"clear">',
         tableTools: {
             "aButtons": []
         },
         "bProcessing": true,
         "bServerSide": true,
         "bDestroy": true,
         "bLengthChange": true,
         "bFilter": false,
         "bSort": true,
         "bInfo": false,
         "fnRowCallback": function (nRow, aData, iDisplayIndex) {
             nRow.setAttribute('id', aData.RowOrder);
         },
         "fnServerParams": function (aoData) {
             aoData.push(
                 { "name": "id", "value": paymentgate.isClick },
                 { "name": "start_time", "value": start_time },
                 { "name": "end_time", "value": end_time },
                 { "name": "merchant_id", "value": $('#fillter_merchant_id').val() }
             );
         },
         "sServerMethod": "POST",
         "sAjaxSource": "/Paymentgate/ListdataHistory",
         "aaSorting": [[0, "desc"]]
     });
 };
 paymentgate.isClick = 0;


paymentgate.loadStaticMoney = function (id, start_time, end_time) {
    $.post('/Paymentgate/loadStaticMoney', {
        id: id,
        start_time: start_time,
        end_time: end_time,
        merchant_id: $('#fillter_merchant_id').val()
    }, function (json) {
        // Show if error
        if (json.error) {
            ns_master.toastr(json.msg, "error");
        } else {
            $('.info-box-number').html(json.value);
        }
    }, 'json')
}

paymentgate.historyTransactions = function (id) {
    
    $('#history').modal("show");
    paymentgate.isClick = id;
    var start_time = $('input[name="daterangepicker_start"]').val();
    var end_time = $('input[name="daterangepicker_end"]').val();
    paymentgate.loadStaticMoney(id, start_time, end_time);
    $('#datatable-history').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

        },
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "id", "value": id },
                { "name": "start_time", "value": start_time },
                { "name": "end_time", "value": end_time },
                { "name": "merchant_id", "value": $('#fillter_merchant_id').val() }
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/Paymentgate/ListdataHistory",
        "aaSorting": [[0, "desc"]]
    });
}


paymentgate.showAdd = function(){
    $('#vueAppPaymentGateInfo').modal("show");
    create_payment_gate.id = 0;
    create_payment_gate.form = {
        merchant: 0,
        wallet_in: 0,
        type: 0,
        fee:0
    };
    create_payment_gate.items = [];
}

paymentgate.addValueCoin = function () {
    var ob_item = { cash_in: itemapp.item.cash_in, cash_out: itemapp.item.cash_out };
    itemapp.items.push(ob_item);
    itemapp.item = {
        cash_in: 0,
        cash_out: 0
    };
}

paymentgate.removeValueCoin = function (index) {
    //if (!confirm("Xác nhận")) {
        //return;
   // }
    itemapp.items.splice(index, 1);
}

paymentgate.add = function () {
    var code = $('input[name="code"]').val();
    if (!code) {
        ns_master.toastr("Vui lòng điền mã cổng thanh toán", "error");
        $('input[name="code"]').focus();
        return;
    }
    var title = $('input[name="title"]').val();
    if (!title) {
        ns_master.toastr("Vui lòng điền tên cổng thanh toán", "error");
        $('input[name="title"]').focus();
        return;
    }

    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Paymentgate/add",
        dataType: "json",
        data: {
            title: title,
            code : code,
            items: itemapp.items,
            coin_in: $('#wallet_in_id').val(),
            coin_out: $('#wallet_out_id').val()
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                paymentgate.dataTable();
                $('#show-edit').modal("hide");
                ns_master.toastr("Thực hiện thành công", "success");
            }
        }
    });
}

$(function () {
    create_payment_gate = new Vue({
        el: '#vueAppPaymentGateInfo',
            data: {
                toast: {
                    error: false,
                    msg: ""
                },
                present: {
                    title: "Tạo mới cổng",
                    btn_title: "Tạo mới cổng"
                },
                items: [],
                form: {
                    merchant: 0,
                    wallet_in: 0,
                    fee: "0"
                },
                wallets: [],
                merchants: [],
                manager_wallets: [],
                items_post : []
            },
            // Methods
            methods: {
                formatMonney: function (num) {

                    var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                    return n;
                },
                // Btn add rule click
                addRule: function () {
                    this.items.push({
                        "cash_in": this.item.cash_in,
                        "cash_out": this.item.cash_out
                    });
                    this.item.cash_in = "";
                    this.item.cash_out = "";
                },
                onCoinPressFees: function () {
                    var s_money = create_payment_gate.form.fee;
                    console.log(s_money)
                    var a_money = s_money.split(",");
                    s_money = a_money.join("");
                    create_payment_gate.form.fee = this.formatMonney(s_money); 
                },
                onCoinPressIn: function () {
                    s_money = this.item.cash_in;
                    var a_money = s_money.split(",");
                    s_money = a_money.join("");

                    this.item.cash_in = this.formatMonney(s_money);
                },
                onCoinPressOut: function () {
                    s_money = this.item.cash_out;
                    var a_money = s_money.split(",");
                    s_money = a_money.join("");

                    this.item.cash_out = this.formatMonney(s_money);
                },

                // Remove rule
                removeValueCoin: function (index) {
                    console.log(index);
                    this.items.splice(index, 1);
                },

                // On merchant select
                onMerchantSelected: function () {
                    comp = this;
                    $.post("/AgencyPaymentGateAPI/WalletByMerchant", {
                        merchant_id: this.form.merchant
                    }, function (json) {
                        if (!json.error) {
                            comp.wallets = json.items;
                        }
                    }, 'json');
                },

                // Submit - Do submit new wallet to system
                onSubmit: function () {
                    if ($('#check_sms').is(':checked')) {
                        sms = "1";
                    } else {
                        sms = "0";
                    }
                    var endpoint = (this.id) ? "/Paymentgate/Update" : "/Paymentgate/Create";
                    if (this.form.code == "") {
                        ns_master.toastr("Vui lòng điền mã code", "error");
                        return;
                    }
                    if (this.form.name == "") {
                        ns_master.toastr("Vui lòng điền tên", "error");
                        return;
                    }
                    if (this.items.length == 0) {
                        ns_master.toastr("Vui lòng điền config tỉ giá", "error");
                        return;
                    }
                    create_payment_gate.items_post = [];
                    var check_in_out = false;
                    this.items.forEach(function (item) {
                        var cash_in = item.cash_in;
                        var a_cash_in = cash_in.split(",");
                        s_cash_in = a_cash_in.join("");


                        var cash_out = item.cash_out;
                        var a_cash_out = cash_out.split(",");
                        s_cash_out = a_cash_out.join("");


                        if (parseFloat(s_cash_in) < parseFloat(s_cash_out)) {
                            check_in_out = true;
                        }

                        create_payment_gate.items_post.push({
                            "cash_in": s_cash_in,
                            "cash_out": s_cash_out
                        });
                    });
                    var s_money = create_payment_gate.form.fee;
                    console.log(s_money)
                    var a_money = s_money.split(",");
                    s_money = a_money.join(""); 
                    $.post(endpoint, {
                        code: this.form.code,
                        name: this.form.name,
                        sms: sms,
                        iorate: this.items_post,
                        type: this.form.type,
                        id: this.id,
                        fee: s_money,
                    }, function (json) {
                        if (json.error) {
                            alert(json.msg);
                        } else {
                            // Close dialog
                            $('#vueAppPaymentGateInfo').modal('hide');
                            paymentgate.dataTable();
                        }
                    }, 'json');
                }
            },

            // Created
            created: function () {
                comp = this;
                //console.log("Get merchants");
                $.post('/AgencyPaymentGateAPI/Merchants', this.form, function (json) {
                    // Show if error
                    if (json.error) {
                        comp.toast = json;
                    } else {
                        comp.merchants = json.items;
                    }
                }, 'json');
                // Gete all manager wallet manager_wallets
                $.post('/Paymentgate/WalletsIn', this.form, function (json) {
                    // Show if error
                    if (json.error) {
                        comp.toast = json;
                    } else {
                        comp.manager_wallets = json.items;
                    }
                }, 'json');
            }
    });

    paymentgate.dataTable();

    $('#daterange-btn').daterangepicker(
        {
            ranges: {
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(6, 'days'),
            endDate: moment()
        },
        function (start, end) {
            $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        }
    );
    
});