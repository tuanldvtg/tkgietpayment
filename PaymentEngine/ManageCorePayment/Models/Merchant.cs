﻿using CorePayment.Base;
using ManageCorePayment.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    [Table("Merchant")]
    public class Merchant
    {
        public int id { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string status_active { get; set; }
        public string secret_key { get; set; }
        public Merchant()
        {

        }
        public Merchant(int id)
        {
            this.id = id;
        }
        public Merchant(int id,string name ,string secret_key)
        {
            this.id = id;
            this.name = name;
            this.secret_key = secret_key;
        }



    }
}