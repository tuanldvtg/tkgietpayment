﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    [Table("ManagerWallet")]
    public class ManagerWallet
    {
        [Key]
        public int id { get; set; }
        public string title { get; set; }
        public int manager_id { get; set; }
    }
}