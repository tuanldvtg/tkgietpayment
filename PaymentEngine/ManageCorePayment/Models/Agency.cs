﻿using CorePayment.Base;
using ManageCorePayment.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    [Table("Agency")]
    public class Agency
    {
        public int id { get; set; }
        public int subid { get; set; }
        public string title { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string phone { get; set; }
        [NotMapped]
        public string phone_new { get; set; }
        [NotMapped]
        public string email_new { get; set; }
        [NotMapped]
        public List<Agency_social> list_social; 

        public string email { get; set; }
        public string status_active { get; set; }
        public string location { get; set; }
        public int orderId { get; set; }
        public int is_appear { get; set; }
        public Agency()
        {

        }

    }
}