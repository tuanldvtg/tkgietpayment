﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    [Table("AdminMoneyTransferCommit")]
    public class AdminMoneyTransferCommit
    {
        public int id { get; set; }
        public int type { get; set; }
        public int agency_id { get; set; }
        public int wallet_id { get; set; }
        public int paymentgate_id { get; set; }
        public double money { get; set; }
        public double money_in { get; set; }
        public int approved { get; set; }
        public int approved_userid { get; set; }
        public DateTime create_time { get; set; }
        public DateTime approve_time { get; set; }
        public string note { get; set; }
        [NotMapped]
        public string create_time_string { get { return this.create_time.ToString("HH:mm:ss dd/MMM/yyyy"); } }
        [NotMapped]
        public string approve_time_string { get { return this.approve_time.ToString("HH:mm:ss dd/MMM/yyyy"); } }
        public string code { get; set; }
        

    }
}