﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    [Table("OTP_user")]
    public class OTP_user
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public string otp { get; set; }
        public DateTime time_creat { get; set; }
        public int status { get; set; }
        public int type { get; set; }
    }
}