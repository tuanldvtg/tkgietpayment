﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    [Table("Transactions_LogVT")]
    public class Transactions_LogVT
    {
        [Key]
        public int id { get; set; }
        public int user_id { get; set; }
        public string requestId { get; set; }
        public string url { get; set; }
        public string cardCode { get; set; }
        public string cardSerial { get; set; }
        public int status { get; set; }
        public float amout { get; set; }
        public DateTime time { get; set; }

    }
}