﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageCorePayment.Models
{
    [Table("ActivityLog")]
    public class ActivityLog
    {
        public int id { set; get; }
        public int accountid { set; get; }
        public int userid { set; get; }
        public string type { set; get; }
        public string note { set; get; } 
        public string type_action { set; get; }
    }
}
