﻿using CorePayment.Base;
using ManageCorePayment.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    [Table("Agency_social")]
    public class Agency_social
    {
        public int id { get; set; }
        public int agency_id { get; set; }
        public string title { get; set; }
        public string link { get; set; }
        public int view_user { get; set; }
    }
}