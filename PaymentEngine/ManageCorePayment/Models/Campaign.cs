﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    
    [Table("Campaign")]
    public class Campaign
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime start_time { get; set; }
        public DateTime end_time { get; set; }
        public int merchant_id { get; set; }
    }
}