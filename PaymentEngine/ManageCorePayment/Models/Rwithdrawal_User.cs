﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    [Table("Rwithdrawal_User")]
    public class Rwithdrawal_User
    {
        public int id { get; set; }
        public int agency_id { get; set; }
        public int user_id { get; set; }
        public int wallet_id { get; set; }
        public int paymentgate_id { get; set; }
        public int approved { get; set; }
        public int accounting_id { get; set; }
        public double value { get; set; }
        public string note { get; set; }
        public string code { get; set; }
        public double value_out { get; set; }
        public DateTime? create_time { get; set; }
        public DateTime? approved_time { get; set; }
        public DateTime? confirm_time { get; set; }
        public string tag { get; set; }//để làm rut tiền thẻ cào
        public int status { get; set; } //status=0: chưa đc nạp, 1: đã nạp, 2: bị ketoan reject
    }
}