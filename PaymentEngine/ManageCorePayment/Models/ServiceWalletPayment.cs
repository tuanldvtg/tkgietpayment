﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageCorePayment.Models
{
    public class ServiceWalletPayment
    {
        public int id;
        public string wallet_name;
        public List<PaymentGate> items = new List<PaymentGate>();
    }
}
