﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageCorePayment.Models
{
    [Table("History_ip")]
   public class History_ip
    {
        [Key]
        public int id { get; set; }
        public int count { get; set; }
        public string ip { get; set; }
        public DateTime time_create { get; set; }
    }
}
