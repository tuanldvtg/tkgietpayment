﻿using CorePayment.Base;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using System;
using System.Data.SqlClient;


namespace ManageCorePayment.Models
{
    public class Merchant_WalletUser
    {
        public int id;
        public int merchant_id;
        public int wallet_id;
        public string wallet_name;

        public Merchant_WalletUser()
        {
            this.merchant_id = 0;
            this.wallet_id = 0;
            this.wallet_name = "";
        }
        public Merchant_WalletUser(int merchant_id, int wallet_id,string wallet_name)
        {
            this.merchant_id = merchant_id;
            this.wallet_id = wallet_id;
            this.wallet_name = wallet_name;
        }

        public string getNameCoin()
        {
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            Wallet wallet = new Wallet();
            reader = serviceBase.query("SELECT TOP 1 * FROM Wallet WHERE id=" + wallet_id);
            
            while (reader.Read())
            {
                wallet.id = wallet_id;
                wallet.name = reader["name"].ToString();
                wallet.coin_id = Int32.Parse(reader["coin_id"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();


            reader = serviceBase.query("SELECT TOP 1 * FROM Coin WHERE id=" + wallet.coin_id);

            Coin coin = new Coin();
            while (reader.Read())
            {
                coin.id = wallet_id;
                coin.name = reader["name"].ToString();
            }
            DatabaseService.Instance.getConnection().Close();

            return coin.name;
        }

        public double getBalance(Users user)
        {
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            double balance = 0;
            reader = serviceBase.query("SELECT TOP 1 * FROM Transactions WHERE wallet_id=" + wallet_id + " AND merchant_id="+ merchant_id +" AND user_id ="+user.id + " ORDER BY id DESC");
            while (reader.Read())
            {
                balance =double.Parse(reader["balance"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            return balance;

        }
    }

   
}
