﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    [Table("Transactions_Log_OnePay")]
    public class Transactions_Log_OnePay
    {
        [Key]
        public int id { get; set; }
        public int user_id { get; set; }
        public string sid { get; set; }
        public int status { get; set; }
        public double amout { get; set; }
        public DateTime time { get; set; }

    }
}