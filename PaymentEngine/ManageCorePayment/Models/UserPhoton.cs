﻿using ExitGames.Client.Photon;
using ExitGames.Client.Photon.LoadBalancing;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web;

namespace ManageCorePayment.Models
{
    public class UserPhoton : LoadBalancingClient
    {
        public Users user { get; set; }
        public string msg { get; set; }
        public string title { get; set; }
        public int uidRecieve { get; set; }
        public string LobbyNameBaCay { set; get; }

        const byte OP_UPDATE_COIN_USER = 26;
        #region Fields
        public readonly Thread updateThread;
        public Action OnUpdate { get; set; }
        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DemoClient"/> class.
        /// </summary>
        public UserPhoton(ConnectionProtocol Protocol) : base(Protocol)
        {
            this.loadBalancingPeer.DebugOut = DebugLevel.INFO;
            this.loadBalancingPeer.TrafficStatsEnabled = true;
            this.updateThread = new Thread(this.UpdateLoop);
            this.updateThread.IsBackground = true;
            this.updateThread.Start();
            this.MasterServerAddress = ConfigurationManager.AppSettings["ipPhoton"];
            this.LobbyNameBaCay ="2";
            AppId = "LoadBalancing";
            AppVersion = "1.0";
            AutoJoinLobby = true;

        }
        #endregion
        #region Send data & update
        /// <summary>Endless loop to be run by background thread (ends when app exits).</summary>
        public void UpdateLoop()
        {
            while (true)
            {
                this.Update();
                Thread.Sleep(500);
            }
        }
        public override bool Connect()
        {
            var Hashtable = new Hashtable()
            {
                {"username", user.username},
                {"password", user.password},
                {"merchant_id", user.merchant_id}
            };
            AuthValues = new AuthenticationValues();
            AuthValues.UserId = user.id.ToString();
            AuthValues.SetAuthPostData(Newtonsoft.Json.JsonConvert.SerializeObject(Hashtable));
            return base.Connect();
        }
        public override void OnOperationResponse(OperationResponse operationResponse)
        {
            base.OnOperationResponse(operationResponse);
            switch (operationResponse.OperationCode)
            {
                case OperationCode.JoinLobby:
                    HandleNotifyUpdateCoin();
                    break;
                case OP_UPDATE_COIN_USER:
                    this.OpLeaveLobby();
                    this.Disconnect();
                    break;
            }
        }

        public void HandleNotifyUpdateCoin()
        {
            if (IsConnected)
            {
                Dictionary<byte, object> notiUpdateCoin = new Dictionary<byte, object>();
                notiUpdateCoin.Add(0, uidRecieve);
                notiUpdateCoin.Add(1, msg);
                notiUpdateCoin.Add(2, title);
                loadBalancingPeer.OpCustom(OP_UPDATE_COIN_USER, notiUpdateCoin, true);
            }
        }

        /// <summary>
        /// Update must be called by a gameloop (a single thread), so it can handle
        /// automatic movement and networking.
        /// </summary>
        public virtual void Update()
        {
            Service();
        }
        #endregion
    }
}