﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageCorePayment.Models
{
    [Table("Error_Log_App")]
   public class Error_Log_App
    {
        [Key]
        public int id { get; set; }
        public string controller { get; set; }
        public string content_error { get; set; }
        public DateTime time_create { get; set; }
    }
}
