﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    [Table("AgencyMoneyTransferCommit")]
    public class AgencyMoneyTransferCommit
    {
        public int id { get; set; }
        public int agency_id { get; set; }
        public int user_id { get; set; }
        public int payment_gate { get; set; }
        public int wallet_id { get; set; }
        public double money { get; set; }
        public int approved { get; set; }
        public DateTime create_time { get; set; }
        public DateTime approve_time { get; set; }
        public String token { get; set; }
    }
}