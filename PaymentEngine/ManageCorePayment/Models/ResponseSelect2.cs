﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    public class ResponseSelect2
    {
        public int id;
        public string username;
        public string phone;
        public string email;
    }
}