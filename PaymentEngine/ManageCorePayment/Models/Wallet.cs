﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageCorePayment.Models
{
    [Table("Wallet")]
   public class Wallet
    {
        public int id { get; set; }
        public string name { get; set; }
        public int coin_id { get; set; }
    }
}
