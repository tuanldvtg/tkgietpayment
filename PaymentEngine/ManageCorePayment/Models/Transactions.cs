﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    [Table("Transactions")]
    public class Transactions
    {
        [Key]
        public int id { get; set; }
        public int user_id { get; set; }
        public int merchant_id { get; set; }
        public int payment_gate { get; set; }
        public int wallet_id { get; set; }
        public double balance { get; set; }
        public string type { get; set; }
        public double value { get; set; }
        public double value_out { get; set; }
        public double profit { get; set; }
        public int transactions_type { get; set; }
        public int user_send { get; set; }
        public string tag { get; set; }
        public string message { get; set; }
        
        public DateTime time_creat { get; set; }

    }
}