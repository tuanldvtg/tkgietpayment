﻿using CorePayment.Base;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;


namespace ManageCorePayment.Models
{
    [Table("Agency_Merchant")]
    public class Agency_Merchant
    {
        [Key]
        public int id { get; set; }
        public int agency_id { get; set; }
        public int merchant_id { get; set; }
    }

   
}
