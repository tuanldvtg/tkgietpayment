﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageCorePayment.Models
{

    public class PostOnePay
    {
        public string merchant = ConfigurationManager.AppSettings["merchantOnePay"];
        public string reference;
        public string customer;
        public string amount;
        public string currency;
        public string datetime;
        public string securityCode = ConfigurationManager.AppSettings["securityCodeOnePay"];
        public string clientIP;
        public string key;
        public string urlpost = ConfigurationManager.AppSettings["domainOnePay"];
        public string domainCallBack = ConfigurationManager.AppSettings["domainCallBack"];
    }
}
