﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    [Table("CashIORate")]
    public class CashIORate
    {
        [Key]
        public int id {set;get;}
        public double cash_in { set; get; }
        public double cash_out { set; get; }
        public int payment_gate_id { set;get;}
    }
}