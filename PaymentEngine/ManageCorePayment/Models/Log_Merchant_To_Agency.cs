﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    [Table("Log_Merchant_To_Agency")]
    public class Log_Merchant_To_Agency
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public DateTime time { get; set; }
        public int agency_id { get; set; }
        public string message { get; set; }
        public double value { get; set; }
    }
}