﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    [Table("Accounting_ref_paymentgate")]
    public class Accounting_ref_paymentgate
    {
        public int id { set; get; }
        public int accounting_id { set; get; }
        public int paymentgate_id { set; get; }
    }
}