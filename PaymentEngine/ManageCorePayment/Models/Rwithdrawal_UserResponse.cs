﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    public class Rwithdrawal_UserResponse
    {
        public int id { get; set; }
        public int agency_id { get; set; }
        public int user_id { get; set; }
        public int wallet_id { get; set; }
        public int paymentgate_id { get; set; }
        public int approved { get; set; }
        public int accounting_id { get; set; }
        public double value { get; set; }
        public string note { get; set; }
        public string code { get; set; }
        public string paymentgate_name { get; set; }
        public double value_out { get; set; }
        public string create_time { get; set; }
        public string approved_time { get; set; }
        public string wallet_name { get; set; }
        public string agency_name { get; set; }
    }
}