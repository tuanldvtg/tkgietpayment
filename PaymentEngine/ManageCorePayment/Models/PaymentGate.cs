﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageCorePayment.Models
{
    [Table("PaymentGate")]
    public class PaymentGate
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public int wallet_in { get; set; }
        public int wallet_out { get; set; }
        public string status_active { get; set; }
        public int merchant_id { get; set; }
        public int copyid { get; set; }
        public int sms { get; set; }
        public int fee { get; set; }
        public int type { get; set; }
        [NotMapped]
        public string wallet_name { get; set; }
        [NotMapped]
        public string wallet_name_in { get; set; }
        public int agency_id { get; set; }
       
        public PaymentGate()
        {

        }
    }
}
