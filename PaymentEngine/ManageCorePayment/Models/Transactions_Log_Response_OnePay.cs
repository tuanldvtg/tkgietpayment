﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    [Table("Transactions_Log_Response_OnePay")]
    public class Transactions_Log_Response_OnePay
    {
        [Key]
        public int id { get; set; }
        public string param { get; set; }
        public string s_param { get; set; }
        public DateTime time { get; set; }
    }
}