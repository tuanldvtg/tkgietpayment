﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models.Alego
{

    [Table("CardBuyAlegoResponse")]
    public class CardBuyAlegoResponse
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public DateTime time_creat { get; set; }
        public DateTime confirm_time { get; set; }
        public DateTime approved_time { get; set; }
        public int status { get; set; }
        public string type { get; set; }
        public double card_value { get; set; }
        public double value_out { get; set; }
        public string card_code { get; set; }
        public string card_serial { get; set; }
        public int isSeen { get; set; }

    }
}