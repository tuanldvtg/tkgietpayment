﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingCard.Models
{
    public class CardBuyRequest
    {
        public string ProductCode { get; set; }
        public string RefNumber { get; set; }
        public string Telco { get; set; }
        public string Type { get; set; }
        public string CustMobile { get; set; }
        public string CustIP { get; set; }
        public string CardPrice { get; set; }
        public int CardQuantity { get; set; }
    }

    public class RequestToALeGo
    {
        public string Fnc { get; set; }
        public string Ver { get; set; }
        public string AgentID { get; set; }
        public string AccID { get; set; }
        public string data { get; set; }
        public string EncData { get; set; }
        public string Checksum { get; set; }

    }
}
