﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingCard.Models
{
    public class CardBuy
    {
        public string ErrorMessage { get; set; }
        public string OrderId { get; set; }
        public ENSPType CardType { get; set; }
        public long CardValue { get; set; }
        public string CardCode { get; set; }
        public string CardSerial { get; set; }
        public string DateExpress { get; set; }
    }
}
