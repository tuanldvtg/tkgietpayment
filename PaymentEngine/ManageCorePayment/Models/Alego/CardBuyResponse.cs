﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingCard.Models
{
    public class CardBuyResponse
    {
        public string Fnc { get; set; }
        public string Ver { get; set; }
        public string AgentID { get; set; }
        public string AccID { get; set; }
        public string RespCode { get; set; }
        public string EncData { get; set; }
        public string Description { get; set; }
        public string Checksum { get; set; }
    }
}
