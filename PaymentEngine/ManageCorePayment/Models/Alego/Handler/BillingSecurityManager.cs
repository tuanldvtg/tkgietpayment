﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BillingCard
{
    public class BillingSecurityManager
    {
        public static string MD5(string encData)
        {
            var inputStr = string.Format("{0}{1}{2}{3}{4}{5}", BillingConfig.Function, BillingConfig.Ver, BillingConfig.AgentId, BillingConfig.AccId, encData, BillingConfig.KeyMD5);
            return CreateMD5(inputStr);
        }

        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("x2"));
                }
                return sb.ToString();
            }
        }
        public static string TripleDESEncrypt(string key, string data)
        {
            //data = data.Trim();
            byte[] bytes = Encoding.UTF8.GetBytes(key);
            string text = BitConverter.ToString(new MD5CryptoServiceProvider().ComputeHash(bytes)).Replace("-", "").ToLower();
            byte[] bytes2 = Encoding.UTF8.GetBytes(text.Substring(0, 24));
            TripleDES tripleDES = TripleDES.Create();
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Key = bytes2;
            tripleDES.GenerateIV();
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, tripleDES.CreateEncryptor(), CryptoStreamMode.Write);
            cryptoStream.Write(Encoding.UTF8.GetBytes(data), 0, Encoding.UTF8.GetByteCount(data));
            cryptoStream.FlushFinalBlock();
            byte[] array = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String(array, 0, array.GetLength(0)).Trim();
        }

        public static string TripleDESDecrypt(string key, string data)
        {
            string result;
            try
            {
                byte[] bytes = Encoding.ASCII.GetBytes(key);
                string text = BitConverter.ToString(new MD5CryptoServiceProvider().ComputeHash(bytes)).Replace("-", "").ToLower();
                byte[] bytes2 = Encoding.ASCII.GetBytes(text.Substring(0, 24));
                TripleDES tripleDES = TripleDES.Create();
                tripleDES.Mode = CipherMode.ECB;
                tripleDES.Key = bytes2;
                byte[] array = Convert.FromBase64String(data);
                MemoryStream stream = new MemoryStream(array, 0, array.Length);
                ICryptoTransform transform = tripleDES.CreateDecryptor();
                CryptoStream stream2 = new CryptoStream(stream, transform, CryptoStreamMode.Read);
                StreamReader streamReader = new StreamReader(stream2);
                result = streamReader.ReadToEnd();
            }
            catch
            {
                result = string.Empty;
            }
            return result;
        }
    }
}
