﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CorePayment.Base;
using ManageCorePayment.Models;

namespace CorePayment.Response
{
    public class TransactionResponse
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public int merchant_id { get; set; }
        public int payment_gate { get; set; }
        public int wallet_id { get; set; }
        public double balance { get; set; }
        public string type { get; set; }
        public double value { get; set; }
        public double value_out { get; set; }
        public double profit { get; set; }
        public int transactions_type { get; set; }
        public int user_send { get; set; }
        public string tag { get; set; }
        public string message { get; set; }

        public string time_creat { get; set; }
        public string wallet_name { get; set; }
    }
}
