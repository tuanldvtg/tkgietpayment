﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    public class PaymentGateCashIO
    {
        public int id = 0;
        public string name;
        public double cash_in;
        public double cash_out;
        public int sms;
    }
}