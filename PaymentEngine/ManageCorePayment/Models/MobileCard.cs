﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageCorePayment.Models
{
    [Table("MobileCard")]
    public class MobileCard
    {
        public int id { get; set; }
        public int provider_id { get; set; }
        public int value { get; set; }
    }
}
