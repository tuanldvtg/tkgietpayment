﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    [Table("Rwithdrawal_Agency")]
    public class Rwithdrawal_Agency
    {
        public int id { get; set; }
        public int agency_id { get; set; }
        public int wallet_id { get; set; }
        public int paymentgate_id { get; set; }
        public int approved { get; set; }
        public int accounting_id { get; set; }
        public double value { get; set; }
        public string note { get; set; }
        public double value_out { get; set; }
        public DateTime create_time { get; set; }
        public string approved_time { get; set; }
    }
}