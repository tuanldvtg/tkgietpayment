﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace ManageCorePayment.Models
{
    public enum EnumUserModelActiveStatus { active, deactive };

    [Table("Users")]
    public class Users
    {
        public int id { get; set; }
        public int verify { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string username { get; set; }
        public string phone { get; set; }
        public string password { get; set; }
        public int merchant_id { get; set; }
        public int verify_phone { get; set; }
        public string status_active { get; set; }
        public string code_sms { get; set; }
        public string code { get; set; }
        public DateTime? time_send_otp { get; set; }
        public EnumUserModelActiveStatus isActive = EnumUserModelActiveStatus.active;


        public String MD5Pass()
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(password));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
    }

   
}