﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    [Table("Admin")]
    public class Admin
    {
        public int id { set; get; }
        public int subid { set; get; }
        public string username { set; get; }
        public string email { set; get; }
        public string name { set; get; }
        public string password { set; get; }
        public string phone { set; get; }
        [NotMapped]
        public string phone_new { set; get; }
        [NotMapped]
        public string email_new { set; get; }
        public int role_id { set; get; }

    }
}