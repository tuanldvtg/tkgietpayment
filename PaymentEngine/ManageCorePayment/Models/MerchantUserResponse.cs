﻿using CorePayment.Base;
using ManageCorePayment.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    public class MerchantUserResponse
    {
        public int id { get; set; }
        public string title { get; set; }
    }
}