﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageCorePayment.Models
{
    public class Provider
    {
        public int id { get; set; }
        public string name { get; set; }
        public string shortline { get; set; }
        public int type_card { get; set; }
        public int type { get; set; }
    }
}
