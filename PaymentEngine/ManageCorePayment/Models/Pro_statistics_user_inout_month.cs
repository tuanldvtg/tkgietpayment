﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageCorePayment.Models
{
    public class Pro_statistics_user_inout_month
    {
        public int year { set; get; }
        public int month { set; get; }
        public double balance_in { set; get; }
        public double balance_out { set; get; }

    }
}