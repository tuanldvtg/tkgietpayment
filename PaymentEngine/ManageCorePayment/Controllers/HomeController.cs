﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Exceptions;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;


namespace ManageCorePayment.Controllers
{
    public class HomeController : Controller
    {
        private bool checkPermission(string module)
        {
            var authHelper = new AuthenticationHelper(Session);
            if (authHelper.isLogined())
            {
                if (authHelper.getRole() == SessionKeyMode.Accounting)
                {
                    ViewBag.isAccounting = true;
                }
                if (authHelper.getRole() == SessionKeyMode.Admin)
                {
                    ViewBag.isAdmin = true;
                }
                return true;
            }
            return false;
        }

        public ActionResult Index()
        {

            var authHelper = new AuthenticationHelper(Session);
            if (authHelper.isLogined()) {
                // Check accounting or not ?
                if (authHelper.getRole() == SessionKeyMode.Accounting)
                {
                    return RedirectToAction("Index", "Accounting");
                }
                if (authHelper.getRole() == SessionKeyMode.Admin)
                {
                    return View();
                }
                
            }
            return RedirectToAction("/Login");
        }

        public ActionResult Account()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
                
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
            if (!this.checkPermission("users")) return RedirectToAction("/Login");
            // Authentication Passed
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader = serviceBase.query("SELECT id,title,secret_key FROM Merchant ORDER BY id DESC");
            var list_merchant = new List<Merchant>();
            while (reader.Read())
            {
                Merchant merchant = new Merchant();
                merchant.id = Int32.Parse(reader["id"].ToString());
                merchant.name = reader["title"].ToString();
                merchant.secret_key = reader["secret_key"].ToString();
                list_merchant.Add(merchant);
            }
            DatabaseService.Instance.getConnection().Close();
            ViewBag.list = list_merchant;
            
            return View("ChangePass");
        }

        public ActionResult Users()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
                
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }

            // Authentication Passed

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader = serviceBase.query("SELECT id,title,secret_key FROM Merchant ORDER BY id DESC");
            var list_merchant = new List<Merchant>();
            while (reader.Read())
            {
                Merchant merchant = new Merchant();
                merchant.id = Int32.Parse(reader["id"].ToString());
                merchant.name = reader["title"].ToString();
                merchant.secret_key = reader["secret_key"].ToString();
                list_merchant.Add(merchant); 
            }

            ViewBag.list = list_merchant;
            
            DatabaseService.Instance.getConnection().Close();
            return View();
        }


        public ActionResult Merchant()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
                
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
            // Authentication Passed

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader = serviceBase.query("SELECT id,name FROM Wallet ORDER BY id DESC");
            var list_wallet = new List<Wallet>();
            while (reader.Read())
            {
                Wallet wallet = new Wallet();
                wallet.id = Int32.Parse(reader["id"].ToString());
                wallet.name = reader["name"].ToString();
                list_wallet.Add(wallet);
            }
            DatabaseService.Instance.getConnection().Close();
            ViewBag.list = list_wallet;

            reader = serviceBase.query("SELECT id,name FROM PaymentGate WHERE copyid = 0 AND agency_id = 0 AND type IN(0,1,2) ORDER BY id DESC");
            var list_paymentgate = new List<PaymentGate>();
            while (reader.Read())
            {
                PaymentGate paymentGate = new PaymentGate();
                paymentGate.id = Int32.Parse(reader["id"].ToString());
                paymentGate.name = reader["name"].ToString();
                list_paymentgate.Add(paymentGate);
            }
            DatabaseService.Instance.getConnection().Close();
            ViewBag.list_paymentgate = list_paymentgate;
            ViewBag.Message = "Merchant";

            return View();
        }
        public ActionResult Store()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
                
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            reader = serviceBase.query("SELECT id,name FROM PaymentGate WHERE copyid = 0 AND agency_id = 0 AND type = 3 ORDER BY id DESC");
            var list_paymentgate = new List<PaymentGate>();
            while (reader.Read())
            {
                PaymentGate paymentGate = new PaymentGate();
                paymentGate.id = Int32.Parse(reader["id"].ToString());
                paymentGate.name = reader["name"].ToString();
                list_paymentgate.Add(paymentGate);
            }
            DatabaseService.Instance.getConnection().Close();
            ViewBag.list_paymentgate = list_paymentgate;


            reader = serviceBase.query("SELECT * FROM Merchant ORDER BY id DESC");
            var list_merchant = new List<Merchant>();
            while (reader.Read())
            {
                Merchant merchant = new Merchant();
                merchant.id = Int32.Parse(reader["id"].ToString());
                merchant.name = reader["title"].ToString();
                merchant.secret_key = reader["secret_key"].ToString();
                list_merchant.Add(merchant);
            }
            DatabaseService.Instance.getConnection().Close();
            ViewBag.list_merchant = list_merchant;

            ViewBag.Message = "Store";

            return View();
        }

        public ActionResult Giftcode()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);

            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            //---lấy chiến dịch

            var query = "SELECT id,name,start_time,end_time FROM Campaign ORDER BY id DESC";
            SqlCommand command = new SqlCommand(query, DatabaseService.Instance.getConnection());
            reader = serviceBase.querySqlParam(command);

            var list_campaign = new List<Campaign>();

            while (reader.Read())
            {
                Campaign wallet = new Campaign();
                wallet.id = Int32.Parse(reader["id"].ToString());
                wallet.name = reader["name"].ToString();
                wallet.start_time = DateTime.Parse(reader["start_time"].ToString());
                wallet.end_time = DateTime.Parse(reader["end_time"].ToString());
                list_campaign.Add(wallet);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            ViewBag.list_campaign = list_campaign;


            //---lấy merchant
            reader = serviceBase.query("SELECT id,name,title FROM Merchant ORDER BY id DESC");
            var list_merchant = new List<Merchant>();
            while (reader.Read())
            {
                Merchant merchant = new Merchant();
                merchant.id = Int32.Parse(reader["id"].ToString());
                merchant.title = reader["title"].ToString();
                list_merchant.Add(merchant);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            ViewBag.list_merchant = list_merchant;


            ViewBag.Message = "Giftcode";
            return View();
        }

        public ActionResult Paymentgate()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
                
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
            // Authentication Passed

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            reader = serviceBase.query("SELECT id,name FROM Wallet ORDER BY id DESC");
            var list_coin = new List<Wallet>();
            while (reader.Read())
            {
                Wallet wallet = new Wallet();
                wallet.id = Int32.Parse(reader["id"].ToString());
                wallet.name = reader["name"].ToString();
                list_coin.Add(wallet);
            }
            DatabaseService.Instance.getConnection().Close();
            ViewBag.list = list_coin;


            reader = serviceBase.query("SELECT id,title,secret_key FROM Merchant ORDER BY id DESC");
            var list_merchant = new List<Merchant>();
            while (reader.Read())
            {
                Merchant merchant = new Merchant();
                merchant.id = Int32.Parse(reader["id"].ToString());
                merchant.name = reader["title"].ToString();
                merchant.secret_key = reader["secret_key"].ToString();
                list_merchant.Add(merchant);
            }
            DatabaseService.Instance.getConnection().Close();
            ViewBag.list_merchant = list_merchant;

            ViewBag.Message = "Paymentgate";

            return View();
        }
        public ActionResult Coin()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
                
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }


            ViewBag.Message = "Coin";
            return View();
        }
        public ActionResult Wallet()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
            // Authentication Passed

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader = serviceBase.query("SELECT id,name FROM Coin ORDER BY id DESC");
            var list_coin = new List<Coin>();
            while (reader.Read())
            {
                Coin coin = new Coin();
                coin.id = Int32.Parse(reader["id"].ToString());
                coin.name = reader["name"].ToString();
                list_coin.Add(coin);
            }
            DatabaseService.Instance.getConnection().Close();
            ViewBag.list = list_coin;
            ViewBag.Message = "Wallet";

            return View();
        }
        public ActionResult Transations()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
                
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
            // Authentication Passed

            ViewBag.Message = "Transations";
            return View();
        }
        public ActionResult Accounting()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
                
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
            // Authentication Passed
            return View("~/Views/Accounting/Transactions");
        }

        public ActionResult Googleapple()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);

            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
            // Authentication Passed
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader = serviceBase.query("SELECT TOP 1 id,client_id,client_secret,refresh_token FROM Config ORDER BY id DESC");
            Config config = new Config();
            while (reader.Read())
            {

                config.id = Int32.Parse(reader["id"].ToString());
                config.client_id = reader["client_id"].ToString();
                config.client_secret = reader["client_secret"].ToString();
                config.refresh_token = reader["refresh_token"].ToString();
            }
            DatabaseService.Instance.getConnection().Close();
            ViewBag.config = config;
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Logout()
        {
            (new AuthenticationHelper(Session)).LogOut();
            return Redirect("/Home/Login");
        }

        public ActionResult LogoutAgency()
        {
            (new AuthenticationHelper(Session)).LogOut();
            return Redirect("/Agency/Login");
        }
        public ActionResult LogoutUsers()
        {
            (new AuthenticationHelper(Session)).LogOut();
            return Redirect("/User/Login");
        }

        [HttpPost]
        public ActionResult LogoutUserAjax()
        {
            (new AuthenticationHelper(Session)).LogOut();
            return Json(new
            {
                error = false
            });
        }


        public string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        public ActionResult Profit()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
                ServiceBase serviceBase = new ServiceBase();
                SqlDataReader reader = serviceBase.query("SELECT id,title,secret_key FROM Merchant ORDER BY id DESC");
                var list_merchant = new List<Merchant>();
                while (reader.Read())
                {
                    Merchant merchant = new Merchant();
                    merchant.id = Int32.Parse(reader["id"].ToString());
                    merchant.name = reader["title"].ToString();
                    merchant.secret_key = reader["secret_key"].ToString();
                    list_merchant.Add(merchant);
                }
                DatabaseService.Instance.getConnection().Close();
                ViewBag.list = list_merchant;
                
                return View();
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }

        }


       

        [HttpPost]
        public ActionResult PostLogin(String username,String password)
        {
            if (username == null || password == null)
            {
                ViewData["Message"] = "Account không đúng";
                return RedirectToAction("Index", "Home");
            }


            ServiceBase serviceBase = new ServiceBase();
            username = serviceBase.convertStringToSql(username);
            
            if (!serviceBase.isCheckRegexQuery(username))
            {
                ViewData["Message"] = "Account không đúng";
                return RedirectToAction("Index", "Home");
            }

            try
            {
                var authHelper = new AuthenticationHelper(Session);
               
                // Get all managet wallets
                using (var db = new GamePaymentContext())
                {
                    var authusername = from w in db.Admin
                               where (w.username == username)
                               select w;
                    if (authusername.Count() == 0)
                    {
                        throw new AccountNotfoundException();
                    }
                    var passwordHashed =CryptoHelper.MD5Hash(password, authusername.FirstOrDefault().id.ToString());
                    var auth = from w in db.Admin
                                  where (w.username == username && w.password == passwordHashed)
                                  select w;
                    if (auth.Count() == 1)
                    {
                        // Foud it
                        var rec = auth.FirstOrDefault();
                        // You are admin !
                        var manager = new Admin()
                        {
                            id = rec.id,
                            username = rec.username,
                            role_id = rec.role_id,
                            subid = rec.subid,
                            name = rec.name
                        };
                        authHelper.assign(manager);
                    }
                    else
                    {
                        throw new AccountNotfoundException();
                    }
                    // Check accounting or not ?
                    if(authHelper.getRole() == SessionKeyMode.Accounting)
                    {
                        return RedirectToAction("Index", "Accounting");
                    }
                    if (authHelper.getRole() == SessionKeyMode.Admin)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            catch (AccountNotfoundException )
            {
                ViewData["Message"] = "Account không đúng";
            }
            //catch (Exception)
            //{
            //    ViewData["Message"] = "Có lỗi không xác định!";
            //}
            return View("Login");
        }
    }
}