﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers.AgencyAPI
{
    public class RwithdrawagencyAccountingAPIController : Controller
    {


        public ActionResult listdata(string filter, string search_note, String s_seach, String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength)
        {
            using (var db = new GamePaymentContext())
            {
                var accountingID = (new AuthenticationHelper(Session).ID());
                string[] a_sort = new string[] { "id", "agency_id", "wallet_id", "value", "value_out", "approved", "create_time", "approved_time", "note", "id" };
                ServiceBase serviceBase = new ServiceBase();
                string queryString = "SELECT" +
                    " id" +
                    " ,(SELECT TOP 1 name FROM PaymentGate WHERE id = Rwithdrawal_Agency.paymentgate_id ) as 'paymentgate_id'" +
                    " ,approved" +
                    " ,create_time" +
                    " ,(SELECT TOP 1 title FROM ManagerWallet WHERE id = Rwithdrawal_Agency.wallet_id ) as 'wallet_id'" +
                    " ,(SELECT TOP 1 title FROM Agency WHERE id = Rwithdrawal_Agency.agency_id) as 'agency_id'" +
                    " ,value" +
                    " ,value_out" +
                    " ,approved_time" +
                    " ,note" +
                    " FROM Rwithdrawal_Agency WHERE agency_id IS NOT NULL ";

                string queryCount = "SELECT COUNT(id) as 'count' FROM Rwithdrawal_Agency WHERE agency_id IS NOT NULL ";
                if (!filter.Trim().Equals("all"))
                {
                    queryString += " AND approved = " + filter.Trim() + " ";
                    queryCount += " AND approved = " + filter.Trim() + " ";
                }
                if (!search_note.Equals(""))
                {
                    queryString += " AND note LIKE '%" + search_note + "%' ";
                    queryCount += " AND note LIKE '%" + search_note + "%' ";
                }

                string subid = (new AuthenticationHelper(Session).getSub());
                if (!subid.Equals("0"))
                {
                    var list_paymengate = from c in db.Accounting_ref_paymentgate
                                          where c.accounting_id == accountingID
                                          select new
                                          {
                                              c.paymentgate_id
                                          };
                    int count = list_paymengate.ToList().Count();
                    string[] a_id = new string[count];
                    int i = 0;
                    list_paymengate.ToList().ForEach(ent =>
                    {
                        a_id[i] = ent.paymentgate_id.ToString();
                        i++;
                    });
                    queryString += " AND paymentgate_id IN(" + string.Join(",", a_id) + ") ";
                    queryCount += " AND paymentgate_id IN(" + string.Join(",", a_id) + ") ";
                }

                queryString += " ORDER BY " + a_sort[Int32Ext.toInt(iSortCol_0)] + " " + sSortDir_0 + " OFFSET " + iDisplayStart + " ROWS FETCH NEXT " + iDisplayLength + " ROWS ONLY";
                SqlDataReader reader = serviceBase.query(queryString);

                List<String[]> users = new List<String[]>();
                while (reader.Read())
                {
                    string[] s_user = new string[10];
                    s_user[0] = reader["id"].ToString().Trim();
                    s_user[1] = reader["agency_id"].ToString().Trim();
                    s_user[2] = reader["wallet_id"].ToString().Trim();
                    s_user[3] = Double.Parse(reader["value"].ToString().Trim()).ToString("N0", CultureInfo.InvariantCulture);
                    s_user[4] = Double.Parse(reader["value_out"].ToString().Trim()).ToString("N0", CultureInfo.InvariantCulture);
                    switch (reader["approved"].ToString().Trim())
                    {
                        case "0":
                            s_user[5] = "";
                            break;
                        case "1":
                            s_user[5] = "<span class=\"label label-success\">Đã hoàn thành</span>";
                            break;
                        case "2":
                            s_user[5] = "<span class=\"label label-danger\">Đã hủy</span>";
                            break;
                    }
                    s_user[6] = reader["create_time"].ToString().Trim();
                    s_user[7] = reader["approved_time"].ToString().Trim();
                    s_user[8] = reader["note"].ToString().Trim();
                    if (reader["approved"].ToString().Trim().Equals("0"))
                    {
                        s_user[9] = "<div class=\"btn-group\">" +
                            "<button data-toggle=\"dropdown\" class=\"btn btn-success dropdown-toggle\" type=\"button\" aria-expanded=\"false\">" +
                              "Chọn &nbsp; &nbsp;" +
                              "<span class=\"caret\"></span>" +
                              "<span class=\"sr-only\">Toggle Dropdown</span>" +
                          "</button>" +
                          "<ul role = \"menu\" class=\"dropdown-menu\">" +
                              "<li><a onclick=withdrawal_agency.agreeConfirm(" + reader["Id"].ToString().Trim() + ") ><i class=\"fa fa-lock\"></i> Duyệt</a></li>" +
                              "<li><a onclick=withdrawal_agency.cancelConfirm(" + reader["Id"].ToString().Trim() + ") ><i class=\"fa fa-unlock\"></i> Không duyệt</a></li>" +
                          "</ul>" +
                      "</div>";
                    }
                    else
                    {
                        s_user[9] = "";
                    }

                    users.Add(s_user);
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                int row = 0;
                reader = serviceBase.query(queryCount);
                while (reader.Read())
                {
                    row = Int32Ext.toInt(reader["count"].ToString());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                return Json(new
                {
                    aaData = users,
                    iTotalRecords = row,
                    iTotalDisplayRecords = row,
                    display = iDisplayLength
                });
            }
        }
        public ActionResult postActive(string id, string status, string code)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = false,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            var accountingID = (new AuthenticationHelper(Session).ID());
            var idID = Int32Ext.toInt(id);
            var statusApp = Int32Ext.toInt(status);
            if (statusApp == 1)
            {
                code = code.Trim();
                using (var db = new GamePaymentContext())
                {
                    var record_otp = db.OTP_user.Where(e => (e.otp == code && e.user_id == accountingID && e.type == 1)).FirstOrDefault();
                    if (record_otp == null)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Mã OTP không đúng"
                        });
                    }
                    var time_creat = record_otp.time_creat;
                    DateTime dateTime = DateTime.Now;
                    int timeMsSinceNow = (int)dateTime.TimeOfDay.TotalMilliseconds;
                    int timeMsSinceOTP = (int)time_creat.TimeOfDay.TotalMilliseconds;
                    if ((timeMsSinceOTP - timeMsSinceNow) / 300 > 5)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Mã OTP đã hết hạn"
                        });
                    }
                    //if (!code.Trim().Equals("123456"))
                    //{
                    //    return Json(new
                    //    {
                    //        error = true,
                    //        msg = "Mã OTP không đúng"
                    //    });
                    //}
                    var wi = db.Rwithdrawal_Agency.FirstOrDefault(e => e.id == idID);
                    if (wi == null)
                    {
                        throw new Exception("Giao dịch không tồn tại");
                    }

                    var query = db.Transactions
    .OrderByDescending(p => p.id)
    .Where(e => (e.user_id == wi.agency_id && e.transactions_type == 2 && e.wallet_id == wi.wallet_id))
    .FirstOrDefault();
                    double agencyMoney = 0;
                    if (query != null)
                    {
                        agencyMoney = query.balance;
                    }
                    if (agencyMoney < wi.value)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Số tiền trong ví không đủ."
                        });
                    }


                    if (wi != null)
                    {
                        wi.approved = statusApp;
                        wi.accounting_id = accountingID;
                        wi.approved_time = DateTime.Now.ToString();
                        db.SaveChanges();
                    }

                    double moneyAgencyMinus = agencyMoney - wi.value;

                    //Update vào ví
                    db.Transactions.Add(new Models.Transactions()
                    {
                        user_id = wi.agency_id,
                        merchant_id = 0,
                        payment_gate = wi.paymentgate_id,
                        wallet_id = wi.wallet_id,
                        balance = moneyAgencyMinus,
                        type = "minus",
                        value = wi.value,
                        transactions_type = 2,
                        user_send = 0,
                        tag = "ruttien",
                        message = wi.note,
                        time_creat = DateTime.Now,
                        value_out = wi.value_out
                    });
                    db.SaveChanges();
                    //Update ví rút agency
                    var query_agency = db.Transactions
    .OrderByDescending(p => p.id)
    .Where(e => (e.user_id == wi.agency_id && e.wallet_id == 0 && e.type == "minus" && e.transactions_type == 2))
    .FirstOrDefault();
                    double agencyMoney_agency = 0;
                    if (query_agency != null)
                    {
                        agencyMoney_agency = query_agency.balance;
                    }

                    agencyMoney_agency = agencyMoney_agency + wi.value_out;
                    db.Transactions.Add(new Models.Transactions()
                    {
                        user_id = wi.agency_id,
                        merchant_id = 0,
                        payment_gate = wi.paymentgate_id,
                        wallet_id = 0,
                        balance = agencyMoney_agency,
                        type = "minus",
                        value = wi.value,
                        transactions_type = 2,
                        user_send = 0,
                        tag = "ruttien",
                        message = wi.note,
                        time_creat = DateTime.Now,
                        value_out = wi.value_out
                    });
                    db.SaveChanges();


                    //Update ví rút admin
                    var query_admin = db.Transactions
    .OrderByDescending(p => p.id)
    .Where(e => (e.user_id == 0 && e.wallet_id == 0 && e.type == "minus"))
    .FirstOrDefault();
                    double agencyMoney_admin = 0;
                    if (query_admin != null)
                    {
                        agencyMoney_admin = query_admin.balance;
                    }

                    agencyMoney_admin = agencyMoney_admin + wi.value_out;
                    db.Transactions.Add(new Models.Transactions()
                    {
                        user_id = 0,
                        merchant_id = 0,
                        payment_gate = wi.paymentgate_id,
                        wallet_id = 0,
                        balance = agencyMoney_admin,
                        type = "minus",
                        value = wi.value,
                        transactions_type = 2,
                        user_send = wi.agency_id,
                        tag = "ruttien",
                        message = wi.note,
                        time_creat = DateTime.Now,
                        value_out = wi.value_out
                    });
                    db.SaveChanges();

                    return Json(new
                    {
                        error = false
                    });
                }
            }
            else
            {
                using (var db = new GamePaymentContext())
                {
                    var wi = db.Rwithdrawal_Agency.FirstOrDefault(e => e.id == idID);
                    if (wi != null)
                    {
                        wi.approved = statusApp;
                        wi.accounting_id = accountingID;
                        wi.approved_time = DateTime.Now.ToString();
                        db.SaveChanges();
                    }
                    return Json(new
                    {
                        error = false
                    });
                }
            }
        }

    }
}