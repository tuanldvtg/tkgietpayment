﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace ManageCorePayment.Controllers
{
    public class InputoutputAgencyController : Controller
    {
        public DateTime[] GetMonthBetween(DateTime startDate, DateTime endDate)
        {
            if (endDate > DateTime.Now)
            {
                endDate = DateTime.Now;
            }

            List<DateTime> allDates = new List<DateTime>();
            for (DateTime date = startDate; date <= endDate; date = date.AddMonths(1))
                allDates.Add(date);
            return allDates.ToArray();
        }

        private DateTime[] GetDatesBetween(DateTime startDate, DateTime endDate)
        {
            if (endDate > DateTime.Now)
            {
                endDate = DateTime.Now;
            }
            List<DateTime> allDates = new List<DateTime>();
            for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                allDates.Add(date);
            return allDates.ToArray();
        }

        [HttpPost]
        public ActionResult getDataDetailTransaction(string id, String s_seach, String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength, string start_time, string end_time, string merchant_id)
        {
            string whereagency = "";
            if (!id.Trim().Equals("all"))
            {
                whereagency = " user_id = '" + id + "'  AND ";
            }
            using (var db = new GamePaymentContext())
            {
                string[] a_start_time_new = start_time.Split('/');
                string start_time_new = a_start_time_new[2] + "-" + a_start_time_new[0] + "-" + a_start_time_new[1] + " 00:00:00.000";

                string[] a_end_time_new = end_time.Split('/');
                string end_time_new = a_end_time_new[2] + "-" + a_end_time_new[0] + "-" + a_end_time_new[1];

                string[] a_sort = new string[] { "id", "time_creat", "payment_gate","user_id", "value", "value_out", "tag", "message" };
                ServiceBase serviceBase = new ServiceBase();
                string queryString = "SELECT" +
        " id" +
        " ,time_creat" +
        " ,value" +
        " ,type" +
        " ,(SELECT TOP 1 name FROM PaymentGate WHERE id = Transactions.payment_gate) as 'payment_gate'" +
        " ,user_send" +
        " ,transactions_type" +
        " ,value_out " +
        " ,tag" +
         ",(SELECT TOP 1 title FROM Agency WHERE id = Transactions.user_id ) as 'user_id'" +
         " ,message" +
        " FROM Transactions WHERE "+ whereagency + " transactions_type = 2 AND wallet_id= 0 AND user_send = 0 AND time_creat >= '" + start_time_new + "' AND time_creat <= '" + end_time_new + " 23:59:59.999' ";
                queryString += " ORDER BY " + a_sort[Int32Ext.toInt(iSortCol_0)] + " " + sSortDir_0 + " OFFSET " + iDisplayStart + " ROWS FETCH NEXT " + iDisplayLength + " ROWS ONLY";
                string queryCount = "SELECT COUNT(id) as 'count' FROM Transactions WHERE " + whereagency + " transactions_type = 2 AND wallet_id= 0 AND user_send = 0 AND time_creat >= '" + start_time_new + "' AND time_creat <= '" + end_time_new + " 23:59:59.999' ";
                SqlDataReader reader = serviceBase.query(queryString);

                List<String[]> users = new List<String[]>();

                while (reader.Read())
                {
                    string[] s_user = new string[8];
                    s_user[0] = reader["id"].ToString().Trim();
                    s_user[1] = reader["time_creat"].ToString().Trim();
                    s_user[2] = reader["payment_gate"].ToString();


                    s_user[3] = reader["user_id"].ToString();

                    if (reader["type"].ToString().Trim().Equals("plus"))
                    {
                        s_user[4] = "<a style=\"color:#4CB052\" >+" + Double.Parse(reader["value"].ToString()).ToString("N", CultureInfo.InvariantCulture) + "</a>";
                        s_user[5] = "<a style=\"color:#4CB052\" >+" + Double.Parse(reader["value_out"].ToString()).ToString("N", CultureInfo.InvariantCulture) + "</a>";
                    }
                    else
                    {
                        s_user[4] = "<a style=\"color:red\" >-" + Double.Parse(reader["value"].ToString()).ToString("N", CultureInfo.InvariantCulture) + "</a>";
                        s_user[5] = "<a style=\"color:red\" >-" + Double.Parse(reader["value_out"].ToString()).ToString("N", CultureInfo.InvariantCulture) + "</a>";
                    }

                    s_user[6] = reader["tag"].ToString();
                    s_user[7] = reader["message"].ToString();
                    users.Add(s_user);
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();
                int row = 0;
                reader = serviceBase.query(queryCount);
                while (reader.Read())
                {
                    row = Int32Ext.toInt(reader["count"].ToString());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();
                return Json(new
                {
                    aaData = users,
                    iTotalRecords = row,
                    iTotalDisplayRecords = row,
                    display = iDisplayLength
                });
            }
        }

        [HttpPost]
        
        public ActionResult GetListInputOutMonth(string id, string start_time, string end_time)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            int userID = Int32Ext.toInt(id);

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string[] a_start_time_new = start_time.Split('/');
            string start_time_new = a_start_time_new[2] + "-" + a_start_time_new[0] + "-" + a_start_time_new[1] + " 00:00:00.000";

            string[] a_end_time_new = end_time.Split('/');
            string end_time_new = a_end_time_new[2] + "-" + a_end_time_new[0] + "-" + a_end_time_new[1] + " 23:59:59.999";
            DateTime[] dates = this.GetMonthBetween(DateTime.Parse(start_time_new), DateTime.Parse(end_time_new));
            if (dates.Length > 24)
            {
                return Json(new
                {
                    error = true,
                    msg = "Số ngày chọn quá lớn không thể thống kê."
                });
            }

            using (var db = new GamePaymentContext())
            {
                //Tìm giao dịch nạp rút trong khoảng thời gian
                var time_startParameter = new SqlParameter("@timestart", start_time_new);
                var time_endParameter = new SqlParameter("@timeend", end_time_new);
                var usersend_idParameter = new SqlParameter("@user_send", userID);
                var result = db.Database
                    .SqlQuery<Pro_statistics_user_inout_month>("Pro_statistics_agency_inout_month @user_send , @timestart , @timeend", usersend_idParameter, time_startParameter, time_endParameter).ToList();

                //Tính nạp rút tất cả thời gian thống kê dựa trên kết quả nhận được
                Pro_statistics_user_inout[] list_date_query = new Pro_statistics_user_inout[dates.Length];
                for (int i = 0; i < list_date_query.Length; i++)
                {
                    bool check_is = false;

                    result.ForEach(ent =>
                    {
                        
                        if (dates[i].Year == ent.year && dates[i].Month == ent.month)
                        {
                            check_is = true;
                            Pro_statistics_user_inout pro_statistics = new Pro_statistics_user_inout();
                            pro_statistics.date = dates[i];
                            if (ent.balance_in != 0)
                            {
                                pro_statistics.balance_in = ent.balance_in;
                            }
                            else
                            {
                                if (i == 0)
                                {
                                    string queryStringBalance = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 2 AND type ='plus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                                    reader = serviceBase.query(queryStringBalance);
                                    double balance = 0;
                                    while (reader.Read())
                                    {
                                        balance = Double.Parse(reader["balance"].ToString());
                                    }
                                    reader.Close();
                                    DatabaseService.Instance.getConnection().Close();
                                    pro_statistics.balance_in = balance;
                                }
                                else
                                {
                                    pro_statistics.balance_in = list_date_query[i - 1].balance_in;
                                }

                            }
                            if (ent.balance_out != 0)
                            {
                                pro_statistics.balance_out = ent.balance_out;
                            }
                            else
                            {
                                if (i == 0)
                                {
                                    string queryStringBalanceOut = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 2 AND type ='minus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                                    reader = serviceBase.query(queryStringBalanceOut);
                                    double balance_out = 0;
                                    while (reader.Read())
                                    {
                                        balance_out = Double.Parse(reader["balance"].ToString());
                                    }
                                    reader.Close();
                                    DatabaseService.Instance.getConnection().Close();

                                    pro_statistics.balance_out = balance_out;
                                }
                                else
                                {
                                    pro_statistics.balance_out = list_date_query[i - 1].balance_out;
                                }

                            }
                            list_date_query[i] = pro_statistics;
                        }
                    });

                    if (!check_is)
                    {
                        if (i > 0)
                        {
                            Pro_statistics_user_inout pro_statistics = new Pro_statistics_user_inout();
                            pro_statistics.date = dates[i];
                            pro_statistics.balance_in = list_date_query[i - 1].balance_in;
                            pro_statistics.balance_out = list_date_query[i - 1].balance_out;
                            list_date_query[i] = pro_statistics;

                        }
                        else
                        {
                            string queryStringBalance = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 2 AND type ='plus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                            reader = serviceBase.query(queryStringBalance);
                            double balance = 0;
                            while (reader.Read())
                            {
                                balance = Double.Parse(reader["balance"].ToString());
                            }
                            reader.Close();
                            DatabaseService.Instance.getConnection().Close();

                            string queryStringBalanceOut = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 2 AND type ='minus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                            reader = serviceBase.query(queryStringBalanceOut);
                            double balance_out = 0;
                            while (reader.Read())
                            {
                                balance_out = Double.Parse(reader["balance"].ToString());
                            }
                            reader.Close();
                            DatabaseService.Instance.getConnection().Close();


                            Pro_statistics_user_inout pro_statistics = new Pro_statistics_user_inout();
                            pro_statistics.date = dates[i];
                            pro_statistics.balance_in = balance;
                            pro_statistics.balance_out = balance_out;
                            list_date_query[i] = pro_statistics;
                        }

                    }
                }
                var list = list_date_query;
                var historyStruct = new[] { new {
                            date = "",
                            value = 0.1
                        }}.ToList();
                historyStruct.Clear();



                var historyStructOut = new[] { new {
                            date = "",
                            value = 0.1
                        }}.ToList();
                historyStructOut.Clear();


                //Lấy danh sách ngày chọn thống kê trả về 2 mảng
                for (int i = 0; i < list_date_query.Length; i++)
                {

                    historyStruct.Add(new
                    {
                        date = list_date_query[i].date.ToString("M/yyyy"),
                        value = list_date_query[i].balance_in,
                    });
                    historyStructOut.Add(new
                    {
                        date = list_date_query[i].date.ToString("M/yyyy"),
                        value = list_date_query[i].balance_out,
                    });
                }
                string queryStringBalancefirt = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 2 AND type ='plus' AND time_creat <= '" + start_time_new + "' ORDER BY id DESC";
                reader = serviceBase.query(queryStringBalancefirt);
                double balance_first = 0;
                while (reader.Read())
                {
                    balance_first = Double.Parse(reader["balance"].ToString());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();


                string queryStringBalancefirtMinus = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 2 AND type ='minus' AND time_creat <= '" + start_time_new + "' ORDER BY id DESC";
                reader = serviceBase.query(queryStringBalancefirtMinus);
                double balance_first_minus = 0;
                while (reader.Read())
                {
                    balance_first_minus = Double.Parse(reader["balance"].ToString());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                var total_nowbalance = historyStruct.Last().value - balance_first;
                var total_nowbalance_out = historyStructOut.Last().value - balance_first_minus;
                return Json(new
                {
                    error = false,
                    balance = Double.Parse(total_nowbalance.ToString()).ToString("N0", CultureInfo.InvariantCulture),
                    balance_out = Double.Parse(total_nowbalance_out.ToString()).ToString("N0", CultureInfo.InvariantCulture),
                    history = historyStruct,
                    history_out = historyStructOut
                });
            }

        }
        [HttpPost]
        public ActionResult GetListInputOut(string id,string start_time,string end_time)
        {
            int userID = Int32Ext.toInt(id);

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string[] a_start_time_new = start_time.Split('/');
            string start_time_new = a_start_time_new[2] + "-" + a_start_time_new[0] + "-" + a_start_time_new[1] + " 00:00:00.000";

            string[] a_end_time_new = end_time.Split('/');
            string end_time_new = a_end_time_new[2] + "-" + a_end_time_new[0] + "-" + a_end_time_new[1] + " 23:59:59.999";
            DateTime[] dates = this.GetDatesBetween(DateTime.Parse(start_time_new), DateTime.Parse(end_time_new));
            if (dates.Length > 31)
            {
                return Json(new
                {
                    error = true,
                    msg = "Số ngày chọn quá lớn không thể thống kê."
                });
            }

            using (var db = new GamePaymentContext())
            {
                //Tìm giao dịch nạp rút trong khoảng thời gian

                var time_startParameter = new SqlParameter("@timestart", start_time_new);
                var time_endParameter = new SqlParameter("@timeend", end_time_new);
                var usersend_idParameter = new SqlParameter("@user_send", userID);

                var result = db.Database
                    .SqlQuery<Pro_statistics_user_inout>("Pro_statistics_agency_inout @user_send ,  @timestart , @timeend", usersend_idParameter, time_startParameter, time_endParameter).ToList();

                //Tính nạp rút tất cả thời gian thống kê dựa trên kết quả nhận được
                Pro_statistics_user_inout[] list_date_query = new Pro_statistics_user_inout[dates.Length];
                for (int i = 0; i < list_date_query.Length; i++)
                {
                    bool check_is = false;

                    result.ForEach(ent =>
                    {
                        if (dates[i] == ent.date)
                        {
                            check_is = true;
                            Pro_statistics_user_inout pro_statistics = new Pro_statistics_user_inout();
                            if (ent.balance_in != 0)
                            {
                                pro_statistics.date = dates[i];
                                pro_statistics.balance_in = ent.balance_in;
                            }
                            else
                            {
                                if (i == 0)
                                {
                                    string queryStringBalance = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 2 AND type ='plus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                                    reader = serviceBase.query(queryStringBalance);
                                    double balance = 0;
                                    while (reader.Read())
                                    {
                                        balance = Double.Parse(reader["balance"].ToString());
                                    }
                                    reader.Close();
                                    DatabaseService.Instance.getConnection().Close();
                                    pro_statistics.balance_in = balance;
                                }
                                else
                                {
                                    pro_statistics.date = dates[i];
                                    pro_statistics.balance_in = list_date_query[i - 1].balance_in;
                                }

                            }
                            if (ent.balance_out != 0)
                            {
                                pro_statistics.balance_out = ent.balance_out;
                            }
                            else
                            {
                                if (i == 0)
                                {
                                    string queryStringBalanceOut = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 2 AND type ='minus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                                    reader = serviceBase.query(queryStringBalanceOut);
                                    double balance_out = 0;
                                    while (reader.Read())
                                    {
                                        balance_out = Double.Parse(reader["balance"].ToString());
                                    }
                                    reader.Close();
                                    DatabaseService.Instance.getConnection().Close();
                                    pro_statistics.date = dates[i];
                                    pro_statistics.balance_out = balance_out;
                                }
                                else
                                {
                                    pro_statistics.date = dates[i];
                                    pro_statistics.balance_out = list_date_query[i - 1].balance_out;
                                }

                            }
                            list_date_query[i] = pro_statistics;
                        }
                    });

                    if (!check_is)
                    {
                        if (i>0)
                        {
                            Pro_statistics_user_inout pro_statistics = new Pro_statistics_user_inout();
                            pro_statistics.date = dates[i];
                            pro_statistics.balance_in = list_date_query[i - 1].balance_in;
                            pro_statistics.balance_out = list_date_query[i - 1].balance_out;
                            list_date_query[i] = pro_statistics;

                        }
                        else
                        {
                            string queryStringBalance = "SELECT TOP 1 balance FROM Transactions WHERE user_id = "+ userID + " AND wallet_id = 0 AND transactions_type = 2 AND type ='plus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                            reader = serviceBase.query(queryStringBalance);
                            double balance = 0;
                            while (reader.Read())
                            {
                                balance = Double.Parse(reader["balance"].ToString());
                            }
                            reader.Close();
                            DatabaseService.Instance.getConnection().Close();

                            string queryStringBalanceOut = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 2 AND type ='minus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                            reader = serviceBase.query(queryStringBalanceOut);
                            double balance_out = 0;
                            while (reader.Read())
                            {
                                balance_out = Double.Parse(reader["balance"].ToString());
                            }
                            reader.Close();
                            DatabaseService.Instance.getConnection().Close();


                            Pro_statistics_user_inout pro_statistics = new Pro_statistics_user_inout();
                            pro_statistics.date = dates[i];
                            pro_statistics.balance_in = balance;
                            pro_statistics.balance_out = balance_out;
                            list_date_query[i] = pro_statistics;
                        }
                        
                    }
                }
                var list = list_date_query;
                var historyStruct = new[] { new {
                            date = "",
                            value = 0.1
                        }}.ToList();
                historyStruct.Clear();



                var historyStructOut = new[] { new {
                            date = "",
                            value = 0.1
                        }}.ToList();
                historyStructOut.Clear();
                

                //Lấy danh sách ngày chọn thống kê trả về 2 mảng
                for (int i = 0; i < list_date_query.Length; i++)
                {

                    historyStruct.Add(new
                    {
                        date = list_date_query[i].date.ToString("d/M"),
                        value = list_date_query[i].balance_in,
                    });
                    historyStructOut.Add(new
                    {
                        date = list_date_query[i].date.ToString("d/M"),
                        value = list_date_query[i].balance_out,
                    });
                }

                string queryStringBalancefirt = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 2 AND type ='plus' AND time_creat <= '" + start_time_new + "' ORDER BY id DESC";
                reader = serviceBase.query(queryStringBalancefirt);
                double balance_first = 0;
                while (reader.Read())
                {
                    balance_first = Double.Parse(reader["balance"].ToString());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();


                string queryStringBalancefirtMinus = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 2 AND type ='minus' AND time_creat <= '" + start_time_new + "' ORDER BY id DESC";
                reader = serviceBase.query(queryStringBalancefirtMinus);
                double balance_first_minus = 0;
                while (reader.Read())
                {
                    balance_first_minus = Double.Parse(reader["balance"].ToString());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();


                var total_nowbalance = historyStruct.Last().value - balance_first;
                var total_nowbalance_out = historyStructOut.Last().value - balance_first_minus;
                return Json(new
                {
                    error = false,
                    balance = Double.Parse(total_nowbalance.ToString()).ToString("N0", CultureInfo.InvariantCulture),
                    balance_out = Double.Parse(total_nowbalance_out.ToString()).ToString("N0", CultureInfo.InvariantCulture),
                    history = historyStruct,
                    history_out = historyStructOut
                });
            }

        }
        [HttpPost]
        public ActionResult GetListInputOutAll(string id, string start_time, string end_time)
        {
            int userID = Int32Ext.toInt(id);

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string[] a_start_time_new = start_time.Split('/');
            string start_time_new = a_start_time_new[2] + "-" + a_start_time_new[0] + "-" + a_start_time_new[1] + " 00:00:00.000";

            string[] a_end_time_new = end_time.Split('/');
            string end_time_new = a_end_time_new[2] + "-" + a_end_time_new[0] + "-" + a_end_time_new[1] + " 23:59:59.999";
            DateTime[] dates = this.GetDatesBetween(DateTime.Parse(start_time_new), DateTime.Parse(end_time_new));
            if (dates.Length > 31)
            {
                return Json(new
                {
                    error = true,
                    msg = "Số ngày chọn quá lớn không thể thống kê."
                });
            }

            using (var db = new GamePaymentContext())
            {
                //Tìm giao dịch nạp rút trong khoảng thời gian

                var time_startParameter = new SqlParameter("@timestart", start_time_new);
                var time_endParameter = new SqlParameter("@timeend", end_time_new);
                var usersend_idParameter = new SqlParameter("@user_send", userID);

                var result = db.Database
                    .SqlQuery<Pro_statistics_user_inout>("Pro_statistics_agency_inout @user_send ,  @timestart , @timeend", usersend_idParameter, time_startParameter, time_endParameter).ToList();

                //Tính nạp rút tất cả thời gian thống kê dựa trên kết quả nhận được
                Pro_statistics_user_inout[] list_date_query = new Pro_statistics_user_inout[dates.Length];
                for (int i = 0; i < list_date_query.Length; i++)
                {
                    bool check_is = false;

                    result.ForEach(ent =>
                    {
                        if (dates[i] == ent.date)
                        {
                            check_is = true;
                            Pro_statistics_user_inout pro_statistics = new Pro_statistics_user_inout();
                            pro_statistics.date = ent.date;
                            if (ent.balance_in != 0)
                            {
                                pro_statistics.balance_in = ent.balance_in;
                            }
                            else
                            {
                                pro_statistics.balance_in = list_date_query[i - 1].balance_in;
                            }
                            if (ent.balance_out != 0)
                            {
                                pro_statistics.balance_out = ent.balance_out;
                            }
                            else
                            {
                                pro_statistics.balance_out = list_date_query[i - 1].balance_out;
                            }
                            list_date_query[i] = pro_statistics;
                        }
                    });

                    if (!check_is)
                    {
                        if (i > 0)
                        {
                            Pro_statistics_user_inout pro_statistics = new Pro_statistics_user_inout();
                            pro_statistics.date = dates[i];
                            pro_statistics.balance_in = list_date_query[i - 1].balance_in;
                            pro_statistics.balance_out = list_date_query[i - 1].balance_out;
                            list_date_query[i] = pro_statistics;

                        }
                        else
                        {
                            string queryStringBalance = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 2 AND type ='plus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                            reader = serviceBase.query(queryStringBalance);
                            double balance = 0;
                            while (reader.Read())
                            {
                                balance = Double.Parse(reader["balance"].ToString());
                            }
                            reader.Close();
                            DatabaseService.Instance.getConnection().Close();

                            string queryStringBalanceOut = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 2 AND type ='minus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                            reader = serviceBase.query(queryStringBalanceOut);
                            double balance_out = 0;
                            while (reader.Read())
                            {
                                balance_out = Double.Parse(reader["balance"].ToString());
                            }
                            reader.Close();
                            DatabaseService.Instance.getConnection().Close();


                            Pro_statistics_user_inout pro_statistics = new Pro_statistics_user_inout();
                            pro_statistics.date = dates[i];
                            pro_statistics.balance_in = balance;
                            pro_statistics.balance_out = balance_out;
                            list_date_query[i] = pro_statistics;
                        }

                    }
                }
                var list = list_date_query;
                var historyStruct = new[] { new {
                            date = "",
                            value = 0.1
                        }}.ToList();
                historyStruct.Clear();



                var historyStructOut = new[] { new {
                            date = "",
                            value = 0.1
                        }}.ToList();
                historyStructOut.Clear();


                //Lấy danh sách ngày chọn thống kê trả về 2 mảng
                for (int i = 0; i < list_date_query.Length; i++)
                {

                    historyStruct.Add(new
                    {
                        date = list_date_query[i].date.ToString("d/M"),
                        value = list_date_query[i].balance_in,
                    });
                    historyStructOut.Add(new
                    {
                        date = list_date_query[i].date.ToString("d/M"),
                        value = list_date_query[i].balance_out,
                    });
                }
                var total_nowbalance = historyStruct.Last().value - historyStruct.First().value;
                var total_nowbalance_out = historyStructOut.Last().value - historyStructOut.First().value;
                return Json(new
                {
                    error = false,
                    balance = Double.Parse(total_nowbalance.ToString()).ToString("N0", CultureInfo.InvariantCulture),
                    balance_out = Double.Parse(total_nowbalance_out.ToString()).ToString("N0", CultureInfo.InvariantCulture),
                    history = historyStruct,
                    history_out = historyStructOut
                });
            }

        }
    }
}