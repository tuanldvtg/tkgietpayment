﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers
{
    public class ManageAccountingController : Controller
    {
        // GET: Coin
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Listdata(String s_seach, String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength)
        {
            string[] a_sort = new string[] { "id", "name", "username", "phone", "email","id" };
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT" +
                " id" +
                " ,name" +
                " ,username" +
                " ,phone" +
                " ,email" +
                " FROM Admin WHERE role_id = 2 AND subid != 0 ";

            string queryCount = "SELECT COUNT(id) as 'count' FROM Admin WHERE role_id = 2  AND subid != 0 ";
            
            queryString += " ORDER BY " + a_sort[Int32Ext.toInt(iSortCol_0)] + " " + sSortDir_0 + " OFFSET " + iDisplayStart + " ROWS FETCH NEXT " + iDisplayLength + " ROWS ONLY";
            SqlDataReader reader = serviceBase.query(queryString);

            List<String[]> users = new List<String[]>();
            while (reader.Read())
            {
                string[] s_user = new string[6];
                s_user[0] = reader["id"].ToString();
                s_user[1] = reader["name"].ToString();
                s_user[2] = reader["username"].ToString();

                string phone_new = reader["phone"].ToString();
                var chars = phone_new.ToCharArray();
                string email = reader["email"].ToString().Trim();

                string phone_response = "";
                for (int ctr = 0; ctr < chars.Length; ctr++)
                {
                    if (ctr < chars.Length - 4)
                    {
                        phone_response += "*";
                    }
                    else
                    {
                        phone_response += chars[ctr];
                    }
                }
                s_user[3] = phone_response;

                string email_response = "";
                for (int ctr = 0; ctr < email.Length; ctr++)
                {
                    if (ctr > 3)
                    {
                        email_response += "*";
                    }
                    else
                    {
                        email_response += email[ctr];
                    }
                }
                s_user[4] = email_response;

                s_user[5] = "<button onclick=\"manage_accounting.showEdit('" + reader["id"].ToString() + "')\" type=\"button\" class=\"btn btn-default btn-xs\">Sửa</button><button onclick=\"manage_accounting.showDelete('" + reader["id"].ToString() + "')\"  type=\"button\" class=\"btn btn-danger btn-xs\">Xóa</button>";
                users.Add(s_user);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            int row = 0;
            reader = serviceBase.query(queryCount);
            while (reader.Read())
            {
                row = Int32Ext.toInt(reader["count"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength
            });
        }
        public string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
        [HttpPost]
        public ActionResult add(string title,string username,string password,string email,string phone,string[] paymentgate_id)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
             password = MD5Hash(password);
            var accountingID = (new AuthenticationHelper(Session).ID());
            using (var db = new GamePaymentContext())
            {

                db.Admin.Add(new Models.Admin()
                {
                    name = title,
                    role_id = 2,
                    subid = accountingID,
                    username = username,
                    password = password,
                    email = email,
                    phone = phone
                });
                db.SaveChanges();
                var ele_admin = db.Admin.SingleOrDefault(o => (o.username == username && o.password == password));

                ele_admin.password = CryptoHelper.MD5Hash(password, ele_admin.id.ToString());
                db.SaveChanges();
                foreach (string value in paymentgate_id)
                {
                    db.Accounting_ref_paymentgate.Add(new Models.Accounting_ref_paymentgate()
                    {
                        accounting_id = ele_admin.id,
                        paymentgate_id = Int32Ext.toInt(value.Trim())
                    });
                    db.SaveChanges();
                }
            }
            return Json(new
            {
                error = false
            });
        }

        public Boolean check_delete(int id)
        {
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT TOP 1 * FROM Wallet WHERE coin_id = '" + id + "'";

            SqlDataReader reader = serviceBase.query(queryString);

            int count = 0;
            while (reader.Read())
            {
                count++;
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            if (count > 0)
            {
                return false;
            }
            return true;
        }


        [HttpPost]
        public ActionResult edit(string title,int id,string username,string phone,string email,string password,string[] paymentgate_id)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            using (var db = new GamePaymentContext())
            {
                ServiceBase serviceBase = new ServiceBase();
                SqlDataReader reader;

                string queryStringt = "UPDATE Admin SET name=N'" + title + "',username='" + username + "',phone='" + phone + "',email='" + email + "' " +
                    "WHERE id = " + id;
                reader = serviceBase.query(queryStringt);
                reader.Close();
                DatabaseService.Instance.getConnection().Close();
                if (!password.Trim().Equals(""))
                {
                    string queryStringUpdatePass = "UPDATE Admin SET password='" + CryptoHelper.MD5Hash(password, id.ToString()) + "' " +
                    "WHERE id = " + id;
                    reader = serviceBase.query(queryStringUpdatePass);
                    reader.Close();
                    DatabaseService.Instance.getConnection().Close();
                }

                //Insert nếu chưa có
                foreach (string value in paymentgate_id)
                {
                    int id_paymentgate = Int32Ext.toInt(value.Trim());
                    var ele_admin = db.Accounting_ref_paymentgate.SingleOrDefault(o => (o.accounting_id == id && o.paymentgate_id == id_paymentgate));
                    if (ele_admin == null)
                    {
                        db.Accounting_ref_paymentgate.Add(new Models.Accounting_ref_paymentgate()
                        {
                            accounting_id = id,
                            paymentgate_id = Int32Ext.toInt(value.Trim())
                        });
                        db.SaveChanges();
                    }
                }

                string deletenotin = "DELETE Accounting_ref_paymentgate WHERE accounting_id = "+id+ " AND paymentgate_id NOT IN ("+ string.Join(",", paymentgate_id) + ")";
                reader = serviceBase.query(deletenotin);
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                return Json(new
                {
                    error = false
                });
            }

        }
        [HttpPost]
        public ActionResult delete(int id)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            string queryStringInsertLog = "DELETE FROM Admin WHERE id = " + id;
            SqlDataReader reader = serviceBase.query(queryStringInsertLog);
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false
            });
            
        }

        [HttpPost]
        public ActionResult detail(int id)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT * FROM Admin WHERE id = " + id;

            SqlDataReader reader = serviceBase.query(queryString);

            Admin admin = new Admin();
            while (reader.Read())
            {
                admin.id = Int32Ext.toInt(reader["id"].ToString());
                admin.name = reader["name"].ToString().Trim();
                admin.username = reader["username"].ToString().Trim();
                admin.phone = reader["phone"].ToString().Trim();

                string phone_new = admin.phone;
                var chars = phone_new.ToCharArray();
                string email = reader["email"].ToString().Trim();

                string phone_response = "";
                for (int ctr = 0; ctr < chars.Length; ctr++)
                {
                    if (ctr < chars.Length - 4)
                    {
                        phone_response += "*";
                    }
                    else
                    {
                        phone_response += chars[ctr];
                    }
                }
                admin.phone_new = phone_response;


                string email_response = "";
                for (int ctr = 0; ctr < email.Length; ctr++)
                {
                    if (ctr > 3)
                    {
                        email_response += "*";
                    }
                    else
                    {
                        email_response += email[ctr];
                    }
                }
                admin.email_new = email_response;


                admin.email = reader["email"].ToString().Trim();
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            string queryStringRef = "SELECT * FROM Accounting_ref_paymentgate WHERE accounting_id = " + id;
            reader = serviceBase.query(queryStringRef);
            List<Accounting_ref_paymentgate> list_id = new List<Accounting_ref_paymentgate>();
            while (reader.Read())
            {
                Accounting_ref_paymentgate accounting_ref_paymentgate = new Accounting_ref_paymentgate();
                accounting_ref_paymentgate.id = Int32Ext.toInt(reader["id"].ToString().Trim());
                accounting_ref_paymentgate.accounting_id = Int32Ext.toInt(reader["accounting_id"].ToString().Trim());
                accounting_ref_paymentgate.paymentgate_id = Int32Ext.toInt(reader["paymentgate_id"].ToString().Trim());
                list_id.Add(accounting_ref_paymentgate);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false,
                item = admin,
                items = list_id
            });

        }
    }
}