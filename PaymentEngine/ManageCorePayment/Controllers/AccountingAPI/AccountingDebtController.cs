﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;


namespace ManageCorePayment.Controllers.AccountingAPI
{
    public class AccountingDebtController : Controller
    {
        [HttpPost]
        public ActionResult ListRequest(string filter, String s_seach, String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength, String merchant_id)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            string[] a_sort = new string[] { "agency_id", "wallet_id", "money", "agency_id" };
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT " +
                "ISNULL(SUM(money_in),0) as 'money',agency_id," +
                "(SELECT TOP 1 title FROM Agency WHERE id = AdminMoneyTransferCommit.agency_id) as 'title' " +
                "FROM AdminMoneyTransferCommit WHERE tag_type = '1' GROUP BY agency_id HAVING ISNULL(SUM(money_in),0) > 0 "; 
            
            string queryCount = "SELECT count(id) AS 'count' FROM AdminMoneyTransferCommit WHERE tag_type = '1' GROUP BY agency_id HAVING ISNULL(SUM(money_in),0) > 0";


            queryString += " ORDER BY " + a_sort[Int32Ext.toInt(iSortCol_0)] + " " + sSortDir_0 + " OFFSET " + iDisplayStart + " ROWS FETCH NEXT " + iDisplayLength + " ROWS ONLY";
            SqlDataReader reader = serviceBase.query(queryString);

            List<String[]> users = new List<String[]>();
            while (reader.Read())
            {
                string[] s_user = new string[3];
                s_user[0] = reader["title"].ToString().Trim();
                s_user[1] = Double.Parse(reader["money"].ToString().Trim()).ToString("N0", CultureInfo.InvariantCulture); ;
                s_user[2] = "<a onclick=\"debt_agency.showDetail('"+ reader["agency_id"].ToString().Trim() + "')\">Danh sách</a>";
                users.Add(s_user);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            int row = 0;
            reader = serviceBase.query(queryCount);
            while (reader.Read())
            {
                row = Int32Ext.toInt(reader["count"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength
            });
        }

        [HttpPost]
        public ActionResult ListDetail(string agency_id, String s_seach, String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            string[] a_sort = new string[] { "id", "create_time", "paymentgate_id", "money_in", "money", "tag_type","note" };
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT " +
                "id," +
                "create_time," +
                "(SELECT TOP 1 name FROM PaymentGate WHERE id = AdminMoneyTransferCommit.paymentgate_id) as 'paymentgate_title', " +
                "money_in, " +
                "money, " +
                "tag_type, " +
                "note " +
                "FROM AdminMoneyTransferCommit WHERE tag_type = '1' AND agency_id = " + agency_id + " ";

            string queryCount = "SELECT count(id) AS 'count' FROM AdminMoneyTransferCommit WHERE tag_type = '1' AND agency_id = " + agency_id;


            queryString += " ORDER BY " + a_sort[Int32Ext.toInt(iSortCol_0)] + " " + sSortDir_0 + " OFFSET " + iDisplayStart + " ROWS FETCH NEXT " + iDisplayLength + " ROWS ONLY";
            SqlDataReader reader = serviceBase.query(queryString);

            List<String[]> users = new List<String[]>();
            while (reader.Read())
            {
                string[] s_user = new string[7];
                s_user[0] = reader["id"].ToString().Trim();
                s_user[1] = reader["create_time"].ToString().Trim();
                s_user[2] = reader["paymentgate_title"].ToString().Trim();
                s_user[3] = Double.Parse(reader["money_in"].ToString().Trim()).ToString("N0", CultureInfo.InvariantCulture);
                s_user[4] = Double.Parse(reader["money"].ToString().Trim()).ToString("N1", CultureInfo.InvariantCulture);
                
                s_user[5] = reader["note"].ToString().Trim();
                s_user[6] = "<a onclick=\"debt_agency.clickChangeTagRemove('"+ reader["id"].ToString().Trim() + "')\"><i class=\"fa fa-tag\"></i>Bỏ tag nợ</a>";

                users.Add(s_user);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            int row = 0;
            reader = serviceBase.query(queryCount);
            while (reader.Read())
            {
                row = Int32Ext.toInt(reader["count"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength
            });
        }
    }
}