﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Exceptions;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;


namespace ManageCorePayment.Controllers.AccountingAPI
{
    public class AccountingAPIController : Controller
    {
        private Random random = new Random();
        public string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public ActionResult sendSMSRwith(string id)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            int e_id = Int32Ext.toInt(id);
                var accountingID = (new AuthenticationHelper(Session).ID());

                using (var db = new GamePaymentContext())
                {
                    //Xóa những lịch sử cache cũ
                    var query = from c in db.OTP_user
                                where c.user_id == accountingID && c.type == 1
                                select new
                                {
                                    c.id,
                                    c.time_creat,
                                    c.user_id,
                                    c.type,
                                    c.otp,
                                    c.status
                                };

                    query.ToList().ForEach(ent =>
                    {
                        var otp_user = db.OTP_user.FirstOrDefault(o => (o.id == ent.id));
                        db.OTP_user.Remove(otp_user);
                        db.SaveChanges();
                    });


                    string code = this.RandomString(6);


                        DateTime time = DateTime.Now;
                        // Use current time
                        db.OTP_user.Add(new Models.OTP_user()
                        {
                            user_id = accountingID,
                            type = 1,
                            otp = code,
                            time_creat = time,
                            status = 0
                        });
                        db.SaveChanges();
                        var record_admin = db.Admin.Where(e => e.id == accountingID).FirstOrDefault();
                        if (record_admin != null)
                        {
                            string phone = record_admin.phone.Trim().Substring(1, record_admin.phone.Trim().Length - 1);
                            SendSMSHelper sendSMSHelper = new SendSMSHelper();
                            string result = sendSMSHelper.send("+84" + phone, code);

                            string phone_new = record_admin.phone.Trim();
                            var chars = phone_new.ToCharArray();

                            string phone_response = "";
                            for (int ctr = 0; ctr < chars.Length; ctr++)
                            {
                                if (ctr < chars.Length - 4)
                                {
                                    phone_response += "*";
                                }
                                else
                                {
                                    phone_response += chars[ctr];
                                }
                            }

                            return Json(new
                            {
                                error = false,
                                phone = phone_response
                            });
                        }
                        else
                        {
                            return Json(new
                            {
                                error = true,
                                msg = "Kế toán chưa có số điện thoại."
                            });
                        }

                }
        }
        public ActionResult ChangePassword(string oldpass, string password)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            try
            {
                // Check auth type
                var auth = new AuthenticationHelper(Session);
                if (!auth.isLogined())
                {
                    throw new Exception("Hết hạn phiên đăng nhập !");
                }
                if (auth.getRole() == SessionKeyMode.Agency)
                {
                    (new AgencyPrivateServices()).changePassword(auth.ID(), oldpass, password);
                    return Json(new
                    {
                        error = false,
                        msg = "Cập nhật hoàn tất !"
                    });
                }


                if (auth.getRole() == SessionKeyMode.Admin || auth.getRole() == SessionKeyMode.Accounting)
                {
                    (new ManagerPrivateServices()).changePassword(auth.ID(), oldpass, password);
                    return Json(new
                    {
                        error = false,
                        msg = "Thay đổi mật khẩu hoàn tất !"
                    });
                }

                return Json(new
                {
                    error = true,
                    msg = "Unknow role"
                });

            }
            catch (AccountNotfoundException)
            {
                return Json(new
                {
                    error = true,
                    msg = "Mật khẩu của bạn không đúng, vui lòng thử lại!"
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }
        public ActionResult sendSMS(string id)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            try
            {
                int e_id = Int32Ext.toInt(id);
                var accountingID = (new AuthenticationHelper(Session).ID());

                using (var db = new GamePaymentContext())
                {
                    //Xóa những lịch sử cache cũ
                    var query = from c in db.OTP_user
                                where c.user_id == accountingID && c.type == 1
                                select new
                                {
                                    c.id,
                                    c.time_creat,
                                    c.user_id,
                                    c.type,
                                    c.otp,
                                    c.status
                                };

                    query.ToList().ForEach(ent =>
                    {
                        var otp_user = db.OTP_user.FirstOrDefault(o => (o.id == ent.id));
                        db.OTP_user.Remove(otp_user);
                        db.SaveChanges();
                    });


                    string code = this.RandomString(6);
                    var record = db.AdminMoneyTransferCommit.Where(e => e.id == e_id).FirstOrDefault();
                    if (record != null)
                    {
                        if (record.approved != 0)
                        {
                            throw new Exception("Yêu cầu đã được xử lý trước kia");
                        }
                        // Found record
                        record.code = code;
                        db.SaveChanges();



                        DateTime time = DateTime.Now;
                        // Use current time
                        db.OTP_user.Add(new Models.OTP_user()
                        {
                            user_id = accountingID,
                            type = 1,
                            otp = code,
                            time_creat = time,
                            status = 0
                        });
                        db.SaveChanges();
                    var record_admin = db.Admin.Where(e => e.id == accountingID).FirstOrDefault();
                    if (record_admin != null)
                    {
                        string phone = record_admin.phone.Trim().Substring(1, record_admin.phone.Trim().Length - 1);
                        SendSMSHelper sendSMSHelper = new SendSMSHelper();
                        string result = sendSMSHelper.send("+84"+ phone,code);

                            string phone_new = record_admin.phone.Trim();
                            var chars = phone_new.ToCharArray();

                            string phone_response = "";
                            for (int ctr = 0; ctr < chars.Length; ctr++)
                            {
                                if (ctr < chars.Length-4)
                                {
                                    phone_response += "*";
                                }
                                else
                                {
                                    phone_response += chars[ctr];
                                }
                            }

                            return Json(new
                        {
                            error = false,
                            phone = phone_response
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Kế toán chưa có số điện thoại."
                        });
                    }


                }
                    throw new Exception("Không tìm thấy giao dịch");
                }
        }
            catch (Exception )
            {
                return Json(new
                {
                    error = true,
                    msg = "Send OTP thất bại"
                });
            }

        }
        public ActionResult ResponseRequestNone(string id, string status)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            int recid = Int32Ext.toInt(id);
            int intStatus = Int32Ext.toInt(status);
            var accountingID = (new AuthenticationHelper(Session).ID());
            using (var db = new GamePaymentContext())
            {

                var record = db.AdminMoneyTransferCommit.Where(e => e.id == recid).FirstOrDefault();
                if (record != null)
                {
                    if (record.approved != 0)
                    {
                        throw new Exception("Yêu cầu đã được xử lý trước kia");
                    }
                    // Found record
                    record.approved = intStatus;
                    record.approve_time = DateTime.Now;
                    db.SaveChanges();
                    return Json(new
                    {
                        error = false
                    });
                }
                throw new Exception("Không tìm thấy giao dịch");
            }
        }
            public ActionResult ResponseRequest(string id, string status,string code)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            try
            {
                int recid = Int32Ext.toInt(id);
                int intStatus = Int32Ext.toInt(status);
                var accountingID = (new AuthenticationHelper(Session).ID());
                using (var db = new GamePaymentContext())
                {
                    var record_otp = db.OTP_user.Where(e => (e.otp == code && e.user_id == accountingID && e.type == 1)).FirstOrDefault();
                    if (record_otp == null)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Mã OTP không đúng"
                        });
                    }
                    var time_creat = record_otp.time_creat;
                    DateTime dateTime = DateTime.Now;
                    long timeMsSinceNow = new DateTimeOffset(dateTime).ToUnixTimeMilliseconds();
                    long timeMsSinceOTP = new DateTimeOffset(time_creat).ToUnixTimeMilliseconds();
                    if ((timeMsSinceNow - timeMsSinceOTP) / 1000 / 60 > 5)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Mã OTP đã hết hạn"
                        });
                    }
                    //if (!code.Trim().Equals("123456"))
                    //{
                    //    return Json(new
                    //    {
                    //        error = true,
                    //        msg = "Mã OTP không đúng"
                    //    });
                    //}
                    var record = db.AdminMoneyTransferCommit.Where(e => e.id == recid).FirstOrDefault();
                    if(record != null)
                    {
                        if(record.approved != 0)
                        {
                            throw new Exception("Yêu cầu đã được xử lý trước kia");
                        }
                        // Found record
                        record.approved = intStatus;
                        record.approve_time = DateTime.Now;
                        record.approved_userid = accountingID;
                        db.SaveChanges();
                        if(intStatus == 1) {
                            //CONG TIEN CHO AGENCRY
                            // Neu la accept giao dich - thi nhan tien cua admin vao angecy
                            // Get ra so tien hien tai cua agency

                            double agencyNowCash = 0;
                            var agencyCash = db.Transactions
                                .OrderByDescending(e => e.id)
                                .Where(e => (e.user_id == record.agency_id && e.wallet_id == record.wallet_id && e.transactions_type == 2))
                                .FirstOrDefault();
                            if(agencyCash != null)
                            {
                                agencyNowCash = agencyCash.balance;
                            }

                            // OK Create transaction for client
                            db.Transactions.Add(new Models.Transactions()
                            {
                                user_id = record.agency_id,
                                merchant_id = 0,
                                payment_gate = record.paymentgate_id,
                                wallet_id = record.wallet_id,
                                balance = agencyNowCash + record.money,
                                type = "plus",
                                value = record.money,
                                transactions_type = 2,
                                user_send = 0,
                                tag = "",
                                message = "Admin nap tien cho agency",
                                time_creat = DateTime.Now
                            });
                            db.SaveChanges();
                            //CONG TIEN NAP CHO ADMIN
                           double adminNowCash = 0;
                          var nowCash =  db.Transactions
                                .OrderByDescending(e => e.id)
                                .Where(e => (e.user_id == 0 && e.wallet_id == 0 && e.type=="plus"))
                                .FirstOrDefault();
                            if (nowCash != null)
                            {
                                adminNowCash = nowCash.balance;
                            }

                            db.Transactions.Add(new Models.Transactions()
                            {
                                user_id = 0,
                                merchant_id = 0,
                                payment_gate = record.paymentgate_id,
                                wallet_id = 0,
                                balance = adminNowCash + record.money_in,
                                type = "plus",
                                value = record.money_in,
                                transactions_type = 2,
                                user_send = record.agency_id,
                                tag = "naptien",
                                message = "Agency nạp tiền",
                                time_creat = DateTime.Now,
                                value_out = record.money
                            });
                            db.SaveChanges();
                            //CONG TIEN NAP CHO AGENCY
                            double adminNowCash_agency = 0;
                            var nowCash_agency = db.Transactions
                                  .OrderByDescending(e => e.id)
                                  .Where(e => (e.user_id == record.agency_id && e.wallet_id == 0 && e.transactions_type == 2 && e.type == "plus"))
                                  .FirstOrDefault();
                            if (nowCash_agency != null)
                            {
                                adminNowCash_agency = nowCash_agency.balance;
                            }
                            
                            db.Transactions.Add(new Models.Transactions()
                            {
                                user_id = record.agency_id,
                                merchant_id = 0,
                                payment_gate = record.paymentgate_id,
                                wallet_id = 0,
                                balance = adminNowCash_agency + record.money_in,
                                type = "plus",
                                value = record.money_in,
                                transactions_type = 2,
                                user_send = 0,
                                tag = "naptien",
                                message = "Agency nạp tiền",
                                time_creat = DateTime.Now,
                                value_out = record.money
                            });
                            db.SaveChanges();
                        }
                        return Json(new
                        {
                            error = false
                        });
                    }
                    throw new Exception("Không tìm thấy giao dịch");
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.ToString()
                });
            }
        }
        [HttpPost]
        public ActionResult changeTag(string id)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "UPDATE AdminMoneyTransferCommit SET tag_type = 1 WHERE id = '" + id + "'";
            SqlDataReader reader = serviceBase.query(queryString);
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false
            });
        }
        [HttpPost]
        public ActionResult clickChangeTagRemove(string id)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "UPDATE AdminMoneyTransferCommit SET tag_type = '' WHERE id = '" + id + "'";
            SqlDataReader reader = serviceBase.query(queryString);
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false
            });
        }
        [HttpPost]
        public ActionResult ListRequest(string filter, String s_seach, String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength, String merchant_id)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            using (var db = new GamePaymentContext())
            {
                int accountingID = (new AuthenticationHelper(Session).ID());
                string[] a_sort = new string[] { "Id", "agency_id", "wallet_id", "money", "approved", "create_time", "approve_time", "tag_type", "note", "id" };
                ServiceBase serviceBase = new ServiceBase();
                string queryString = "SELECT" +
                    " AdminMoneyTransferCommit.Id" +
                    " ,(SELECT TOP 1 title FROM Agency WHERE id = AdminMoneyTransferCommit.agency_id ) as 'agency_title'" +
                    " ,(SELECT TOP 1 name FROM Wallet WHERE id = AdminMoneyTransferCommit.wallet_id ) as 'wallet_title'" +
                    " ,(SELECT TOP 1 name FROM PaymentGate WHERE id = AdminMoneyTransferCommit.paymentgate_id) as 'paymentgate_id'" +
                    " ,money" +
                    " ,approved" +
                    " ,create_time" +
                    " ,approve_time" +
                    " ,tag_type" +
                    " ,note" +
                    " FROM AdminMoneyTransferCommit WHERE paymentgate_id IS NOT NULL ";

                string queryCount = "SELECT COUNT(id) as 'count' FROM AdminMoneyTransferCommit  WHERE paymentgate_id IS NOT NULL ";
                if (filter.Equals("none"))
                {
                    queryString += " AND approved = 0 ";
                    queryCount += " AND approved = 0 ";
                }
                if (filter.Equals("passed"))
                {
                    queryString += " AND approved = 1 ";
                    queryCount += " AND approved = 1 ";
                }
                string subid = (new AuthenticationHelper(Session).getSub());
                if (!subid.Equals("0"))
                {
                    var list_paymengate = from c in db.Accounting_ref_paymentgate
                                          where c.accounting_id == accountingID
                                          select new
                                          {
                                              c.paymentgate_id
                                          };
                    int count = list_paymengate.ToList().Count();
                    string[] a_id = new string[count];
                    int i = 0;
                    list_paymengate.ToList().ForEach(ent =>
                    {
                        a_id[i] = ent.paymentgate_id.ToString();
                        i++;
                    });
                    queryString += " AND paymentgate_id IN("+ string.Join(",", a_id) + ") ";
                    queryCount += " AND paymentgate_id IN(" + string.Join(",", a_id) + ") ";
                }

                queryString += " ORDER BY " + a_sort[Int32Ext.toInt(iSortCol_0)] + " " + sSortDir_0 + " OFFSET " + iDisplayStart + " ROWS FETCH NEXT " + iDisplayLength + " ROWS ONLY";
                SqlDataReader reader = serviceBase.query(queryString);

                List<String[]> users = new List<String[]>();
                while (reader.Read())
                {
                    string[] s_user = new string[10];
                    s_user[0] = reader["Id"].ToString().Trim();
                    s_user[1] = reader["agency_title"].ToString().Trim();
                    s_user[2] = reader["wallet_title"].ToString().Trim();
                    s_user[3] = Double.Parse(reader["money"].ToString().Trim()).ToString("N", CultureInfo.InvariantCulture);
                    if (reader["approved"].ToString().Trim().Equals("0"))
                    {
                        s_user[4] = "";
                    }
                    if (reader["approved"].ToString().Trim().Equals("1"))
                    {
                        s_user[4] = "<span class=\"label label-success\">Đã duyệt</span>";
                    }
                    if (reader["approved"].ToString().Trim().Equals("2"))
                    {
                        s_user[4] = "<span class=\"label label-danger\">Đã hủy</span>";
                    }
                    s_user[5] = reader["create_time"].ToString().Trim();

                    s_user[6] = reader["approve_time"].ToString().Trim();

                    if (reader["tag_type"].ToString().Trim().Equals("1"))
                    {
                        s_user[7] = " <a onclick=\"verify_payment.clickChangeTagRemove('" + reader["id"].ToString().Trim() + "')\" ><i class=\"fa fa-tag\"></i>Bỏ tag nợ</a> <span class=\"label label-danger\">Đang nợ</span>";
                    }
                    else
                    {
                        s_user[7] = " <a onclick=\"verify_payment.clickChangeTag('" + reader["id"].ToString().Trim() + "')\" ><i class=\"fa fa-tag\"></i>Gắn tag nợ</a>";
                    }
                    s_user[8] = reader["note"].ToString().Trim();
                    if (reader["approved"].ToString().Trim().Equals("0"))
                    {
                        s_user[9] = "<div class=\"btn-group\">" +
                                            "<button data-toggle=\"dropdown\" class=\"btn btn-success dropdown-toggle\" type=\"button\" aria-expanded=\"false\">" +
                                              "Chọn &nbsp; &nbsp;" +
                                              "<span class=\"caret\"></span>" +
                                              "<span class=\"sr-only\">Toggle Dropdown</span>" +
                                          "</button>" +
                                          "<ul role = \"menu\" class=\"dropdown-menu\">" +
                                              "<li><a onclick=verify_payment.doViewOTP(" + reader["Id"].ToString().Trim() + ",'1') ><i class=\"fa fa-lock\"></i> Duyệt</a></li>" +
                                              "<li><a onclick=verify_payment.doVerifyNone(" + reader["Id"].ToString().Trim() + ",'2') ><i class=\"fa fa-unlock\"></i> Không duyệt</a></li>" +
                                          "</ul>" +
                                      "</div>";
                    }
                    else
                    {
                        s_user[9] = "";
                    }

                    users.Add(s_user);
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                int row = 0;
                reader = serviceBase.query(queryCount);
                while (reader.Read())
                {
                    row = Int32Ext.toInt(reader["count"].ToString().Trim());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                return Json(new
                {
                    aaData = users,
                    iTotalRecords = row,
                    iTotalDisplayRecords = row,
                    display = iDisplayLength
                });
            }
        }
    }
}