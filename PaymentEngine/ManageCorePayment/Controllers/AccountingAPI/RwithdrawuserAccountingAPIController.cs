﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers.userAPI
{
    public class RwithdrawuserAccountingAPIController : Controller
    {

        public ActionResult listdata(string filter, string search_note, String s_seach, String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength)
        {
            using (var db = new GamePaymentContext())
            {
                var accountingID = (new AuthenticationHelper(Session).ID());
                string[] a_sort = new string[] { "id", "user_id", "wallet_id", "value", "value_out", "approved", "create_time", "approved_time", "code", "username", "note", "id" };
                ServiceBase serviceBase = new ServiceBase();
                string queryString = "SELECT" +
                    " id" +
                    " ,paymentgate_id" +
                    " ,approved" +
                    " ,create_time" +
                    " ,(SELECT TOP 1 name FROM Wallet WHERE id = Rwithdrawal_User.wallet_id ) as 'wallet_id'" +
                    " ,(SELECT TOP 1 title FROM Agency WHERE id = Rwithdrawal_User.agency_id) as 'agency_id'" +
                    " ,value" +
                    " ,value_out" +
                    " ,approved_time" +
                    " ,note" +
                    " ,code" +
                    " ,(SELECT TOP 1 username FROM Users WHERE id = Rwithdrawal_User.user_id ) as 'username'" +
                    " FROM Rwithdrawal_User WHERE user_id IS NOT NULL AND approved != 0 ";

                string queryCount = "SELECT COUNT(id) as 'count' FROM Rwithdrawal_User WHERE user_id IS NOT NULL AND approved != 0 ";
                if (!filter.Trim().Equals("all"))
                {
                    queryString += " AND approved = " + filter.Trim() + " ";
                    queryCount += " AND approved = " + filter.Trim() + " ";
                }
                if (!search_note.Equals(""))
                {
                    queryString += " AND note LIKE '%" + search_note + "%' ";
                    queryCount += " AND note LIKE '%" + search_note + "%' ";
                }
                string subid = (new AuthenticationHelper(Session).getSub());
                if (!subid.Equals("0"))
                {
                    var list_paymengate = from c in db.Accounting_ref_paymentgate
                                          where c.accounting_id == accountingID
                                          select new
                                          {
                                              c.paymentgate_id
                                          };
                    int count = list_paymengate.ToList().Count();
                    string[] a_id = new string[count];
                    int i = 0;
                    list_paymengate.ToList().ForEach(ent =>
                    {
                        a_id[i] = ent.paymentgate_id.ToString();
                        i++;
                    });
                    queryString += " AND paymentgate_id IN(" + string.Join(",", a_id) + ") ";
                    queryCount += " AND paymentgate_id IN(" + string.Join(",", a_id) + ") ";
                }

                queryString += " ORDER BY " + a_sort[Int32Ext.toInt(iSortCol_0)] + " " + sSortDir_0 + " OFFSET " + iDisplayStart + " ROWS FETCH NEXT " + iDisplayLength + " ROWS ONLY";
                SqlDataReader reader = serviceBase.query(queryString);

                List<String[]> users = new List<String[]>();
                while (reader.Read())
                {
                    string[] s_user = new string[12];
                    s_user[0] = reader["id"].ToString().Trim();
                    s_user[1] = reader["agency_id"].ToString().Trim();
                    s_user[2] = reader["wallet_id"].ToString().Trim();
                    s_user[3] = Double.Parse(reader["value"].ToString().Trim()).ToString("N0", CultureInfo.InvariantCulture);
                    s_user[4] = Double.Parse(reader["value_out"].ToString().Trim()).ToString("N0", CultureInfo.InvariantCulture);
                    switch (reader["approved"].ToString().Trim())
                    {
                        case "2":
                            s_user[5] = "<span class=\"label label-danger\">Đã hủy</span>";
                            break;
                        case "3":
                            s_user[5] = "<span class=\"label label-success\">Đã duyệt</span>";
                            break;
                        default:
                            s_user[5] = "";
                            break;
                    }
                    s_user[6] = reader["create_time"].ToString().Trim();
                    s_user[7] = reader["approved_time"].ToString().Trim();
                    s_user[8] = "<b>" + reader["code"].ToString().Trim() + "</b>";
                    s_user[9] = reader["username"].ToString().Trim();
                    s_user[10] = reader["note"].ToString().Trim();
                    if (reader["approved"].ToString().Trim().Equals("1"))
                    {
                        s_user[11] = "<div class=\"btn-group\">" +
                            "<button data-toggle=\"dropdown\" class=\"btn btn-success dropdown-toggle\" type=\"button\" aria-expanded=\"false\">" +
                              "Chọn &nbsp; &nbsp;" +
                              "<span class=\"caret\"></span>" +
                              "<span class=\"sr-only\">Toggle Dropdown</span>" +
                          "</button>" +
                          "<ul role = \"menu\" class=\"dropdown-menu\">" +
                              "<li><a onclick=withdrawal_user.agreeConfirm(" + reader["Id"].ToString().Trim() + ") ><i class=\"fa fa-lock\"></i> Duyệt</a></li>" +
                              "<li><a onclick=withdrawal_user.cancelConfirm(" + reader["Id"].ToString().Trim() + ") ><i class=\"fa fa-unlock\"></i> Không duyệt</a></li>" +
                          "</ul>" +
                      "</div>";
                    }
                    else
                    {
                        s_user[11] = "";
                    }

                    users.Add(s_user);
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                int row = 0;
                reader = serviceBase.query(queryCount);
                while (reader.Read())
                {
                    row = Int32Ext.toInt(reader["count"].ToString());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                return Json(new
                {
                    aaData = users,
                    iTotalRecords = row,
                    iTotalDisplayRecords = row,
                    display = iDisplayLength
                });
            }
        }
        public ActionResult postActive(string id, string status, string code)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = false,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            var accountingID = (new AuthenticationHelper(Session).ID());
            var idID = Int32Ext.toInt(id);
            var statusApp = Int32Ext.toInt(status);
            if (statusApp == 3)
            {
                code = code.Trim();
                using (var db = new GamePaymentContext())
                {
                    var record_otp = db.OTP_user.Where(e => (e.otp == code && e.user_id == accountingID && e.type == 1)).FirstOrDefault();
                    if (record_otp == null)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Mã OTP không đúng"
                        });
                    }
                    var time_creat = record_otp.time_creat;
                    DateTime dateTime = DateTime.Now;
                    int timeMsSinceNow = (int)dateTime.TimeOfDay.TotalMilliseconds;
                    int timeMsSinceOTP = (int)time_creat.TimeOfDay.TotalMilliseconds;
                    if ((timeMsSinceOTP - timeMsSinceNow) / 300 > 5)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Mã OTP đã hết hạn"
                        });
                    }
                    //if (!code.Trim().Equals("123456"))
                    //{
                    //    return Json(new
                    //    {
                    //        error = true,
                    //        msg = "Mã OTP không đúng"
                    //    });
                    //}
                    var wi = db.Rwithdrawal_User.FirstOrDefault(e => e.id == idID);
                    if (wi == null)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Giao dịch không tồn tại"
                        });
                    }



                    var user_item = db.User.FirstOrDefault(u => u.id == wi.user_id);


                    //                var query = db.Transactions
                    //.OrderByDescending(p => p.id)
                    //.Where(e => (e.user_id == wi.user_id && e.transactions_type == 0 && e.wallet_id == wi.wallet_id))
                    //.FirstOrDefault();
                    //                double userMoney = 0;
                    //                if (query != null)
                    //                {
                    //                    userMoney = query.balance;
                    //                }
                    //                if (userMoney < wi.value)
                    //                {
                    //                    return Json(new
                    //                    {
                    //                        error = true,
                    //                        msg = "Số tiền trong ví không đủ."
                    //                    });
                    //                }

                    if (wi != null)
                    {
                        wi.approved = statusApp;
                        wi.accounting_id = accountingID;
                        wi.approved_time = DateTime.Now;
                        db.SaveChanges();
                    }


                    //double moneyuserMinus = userMoney - wi.value;


                    ////Update vào ví
                    //db.Transactions.Add(new Models.Transactions()
                    //{
                    //    user_id = wi.user_id,
                    //    merchant_id = user_item.merchant_id,
                    //    payment_gate = wi.paymentgate_id,
                    //    wallet_id = wi.wallet_id,
                    //    balance = moneyuserMinus,
                    //    type = "minus",
                    //    value = wi.value,
                    //    transactions_type = 0,
                    //    user_send = 0,
                    //    tag = "ruttien",
                    //    message = wi.note,
                    //    time_creat = DateTime.Now,
                    //    value_out = wi.value_out
                    //});
                    //db.SaveChanges();



                    //Lấy merchant user

                    var merchant_item = db.Merchant.FirstOrDefault(m => m.id == user_item.merchant_id);
                    var user_merchant = db.User.FirstOrDefault(u => u.username == merchant_item.username);
                    //Lấy số dư ví merchant giao dịch
                    var query_merchant = db.Transactions
    .OrderByDescending(p => p.id)
    .Where(e => (e.user_id == user_merchant.id && e.transactions_type == 0 && e.wallet_id == wi.wallet_id))
    .FirstOrDefault();
                    //Trừ tiền merchant tạm giữ khi giao dịch
                    var balance_merchant = query_merchant.balance - wi.value;
                    db.Transactions.Add(new Models.Transactions()
                    {
                        user_id = user_merchant.id,
                        merchant_id = wi.id,
                        payment_gate = 0,
                        wallet_id = wi.wallet_id,
                        balance = balance_merchant,
                        type = "minus",
                        value = wi.value,
                        transactions_type = 0,
                        user_send = 0,
                        tag = "ruttien",
                        message = "Trừ tiền tạm giữ " + user_item.username + " rút tiền",
                        time_creat = DateTime.Now,
                        value_out = wi.value_out
                    });
                    db.SaveChanges();
                    //Lưu log Merchant gửi cho Agency
                    db.Log_Merchant_To_Agency.Add(new Models.Log_Merchant_To_Agency()
                    {
                        user_id = user_merchant.id,
                        time = DateTime.Now,
                        agency_id = wi.agency_id,
                        value = wi.value,
                        message = "Khi " + user_item.username + " rút tiền thành công"
                    });
                    db.SaveChanges();
                    //Cộng tiền cho Agency
                    var queryAgency = db.Transactions
    .OrderByDescending(p => p.id)
    .Where(e => (e.user_id == wi.agency_id && e.transactions_type == 2 && e.wallet_id == wi.wallet_id))
    .FirstOrDefault();
                    double agencyMoney = 0;
                    if (queryAgency != null)
                    {
                        agencyMoney = queryAgency.balance;
                    }
                    agencyMoney = agencyMoney + wi.value;
                    db.Transactions.Add(new Models.Transactions()
                    {
                        user_id = wi.agency_id,
                        merchant_id = user_item.merchant_id,
                        payment_gate = wi.paymentgate_id,
                        wallet_id = wi.wallet_id,
                        balance = agencyMoney,
                        type = "plus",
                        value = wi.value,
                        transactions_type = 2,
                        user_send = wi.user_id,
                        tag = "phe",
                        message = "Cộng tiền User:" + user_item.username + " rút tiền",
                        time_creat = DateTime.Now,
                        value_out = wi.value_out
                    });
                    db.SaveChanges();

                    //Update ví rút user
                    var query_user = db.Transactions
    .OrderByDescending(p => p.id)
    .Where(e => (e.user_id == wi.user_id && e.wallet_id == 0 && e.type == "minus" && e.transactions_type == 0))
    .FirstOrDefault();
                    double userMoney_user = 0;
                    if (query_user != null)
                    {
                        userMoney_user = query_user.balance;
                    }

                    userMoney_user = userMoney_user + wi.value_out;
                    db.Transactions.Add(new Models.Transactions()
                    {
                        user_id = wi.user_id,
                        merchant_id = 0,
                        payment_gate = wi.paymentgate_id,
                        wallet_id = 0,
                        balance = userMoney_user,
                        type = "minus",
                        value = wi.value,
                        transactions_type = 0,
                        user_send = 0,
                        tag = "ruttien",
                        message = wi.note,
                        time_creat = DateTime.Now,
                        value_out = wi.value_out
                    });
                    db.SaveChanges();


                    //Update ví rút admin
                    var query_admin = db.Transactions
    .OrderByDescending(p => p.id)
    .Where(e => (e.user_id == 0 && e.wallet_id == 0 && e.type == "minus" && e.transactions_type == 0))
    .FirstOrDefault();
                    double userMoney_admin = 0;
                    if (query_admin != null)
                    {
                        userMoney_admin = query_admin.balance;
                    }

                    userMoney_admin = userMoney_admin + wi.value_out;
                    db.Transactions.Add(new Models.Transactions()
                    {
                        user_id = 0,
                        merchant_id = 0,
                        payment_gate = wi.paymentgate_id,
                        wallet_id = 0,
                        balance = userMoney_admin,
                        type = "minus",
                        value = wi.value,
                        transactions_type = 0,
                        user_send = wi.user_id,
                        tag = "ruttien",
                        message = wi.note,
                        time_creat = DateTime.Now,
                        value_out = wi.value_out
                    });
                    db.SaveChanges();


                    return Json(new
                    {
                        error = false
                    });
                }
            }
            else
            {
                using (var db = new GamePaymentContext())
                {
                    var wi = db.Rwithdrawal_User.FirstOrDefault(e => e.id == idID);
                    if (wi == null)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Giao dịch không tồn tại"
                        });
                    }


                    var user_item = db.User.FirstOrDefault(u => u.id == wi.user_id);
                    if (wi != null)
                    {
                        wi.approved = statusApp;
                        wi.accounting_id = accountingID;
                        wi.approved_time = DateTime.Now;
                        db.SaveChanges();
                    }
                    var merchant_item = db.Merchant.FirstOrDefault(m => m.id == user_item.merchant_id);
                    var user_merchant = db.User.FirstOrDefault(u => u.username == merchant_item.username);
                    //Lấy số dư ví merchant giao dịch
                    var query_merchant = db.Transactions
    .OrderByDescending(p => p.id)
    .Where(e => (e.user_id == user_merchant.id && e.transactions_type == 0 && e.wallet_id == wi.wallet_id))
    .FirstOrDefault();
                    //Trừ tiền merchant tạm giữ khi giao dịch
                    var balance_merchant = query_merchant.balance - wi.value;
                    db.Transactions.Add(new Models.Transactions()
                    {
                        user_id = user_merchant.id,
                        merchant_id = wi.id,
                        payment_gate = 0,
                        wallet_id = wi.wallet_id,
                        balance = balance_merchant,
                        type = "minus",
                        value = wi.value,
                        transactions_type = 0,
                        user_send = 0,
                        tag = "ruttien",
                        message = "Trừ tiền tạm giữ " + user_item.username + " rút tiền",
                        time_creat = DateTime.Now,
                        value_out = wi.value_out
                    });
                    db.SaveChanges();
                    //Cộng tiền tạm giữ lại cho user
                    var query_user_r = db.Transactions
  .OrderByDescending(p => p.id)
  .Where(e => (e.user_id == user_item.id && e.transactions_type == 0 && e.wallet_id == wi.wallet_id))
  .FirstOrDefault();
                    //Trừ tiền merchant tạm giữ khi giao dịch
                    var balance_user_r = query_user_r.balance + wi.value;
                    db.Transactions.Add(new Models.Transactions()
                    {
                        user_id = user_item.id,
                        merchant_id = wi.id,
                        payment_gate = 0,
                        wallet_id = wi.wallet_id,
                        balance = balance_user_r,
                        type = "plus",
                        value = wi.value,
                        transactions_type = 0,
                        user_send = user_merchant.id,
                        tag = "ruttien",
                        message = "Công tiền tạm giữ ở merchant",
                        time_creat = DateTime.Now,
                        value_out = wi.value_out
                    });
                    db.SaveChanges();

                    return Json(new
                    {
                        error = false
                    });
                }
            }
        }
    }
}