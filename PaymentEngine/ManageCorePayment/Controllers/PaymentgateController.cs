﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers
{
    public class PaymentgateController : Controller
    {
        // GET: Paymentgate
        public ActionResult Index()
        {
            return View();
        }

        static string ConvertStringArrayToStringJoin(string[] array)
        {
            // Use string Join to concatenate the string elements.
            string result = string.Join(".", array);
            return result;
        }

        public ActionResult loadStaticMoney(string id, string start_time, string end_time, string merchant_id)
        {
            using (var db = new GamePaymentContext())
            {
                int idPaymentGate = Int32.Parse(id);
                var record_otp = from c in db.PaymentGate
                                 where c.copyid == idPaymentGate
                                 select new
                                 {
                                     c.id
                                 };
                StringBuilder builder = new StringBuilder();
                string[] a_id = new string[record_otp.ToList().Count + 1];
                int i = 0;
                record_otp.ToList().ForEach(ent =>
                {
                    a_id[i] = ent.id.ToString();
                    i++;
                });

                a_id[i] = idPaymentGate.ToString();


                string s_id = string.Join(",", a_id);


                string[] a_start_time_new = start_time.Split('/');
                string start_time_new = a_start_time_new[2] + "-" + a_start_time_new[0] + "-" + a_start_time_new[1] + " 00:00:00";

                string[] a_end_time_new = end_time.Split('/');
                string end_time_new = a_end_time_new[2] + "-" + a_end_time_new[0] + "-" + a_end_time_new[1];

                ServiceBase serviceBase = new ServiceBase();

                var record_paymentGate = db.PaymentGate.Where(e => e.id == idPaymentGate).FirstOrDefault();
                //Nếu là cổng đặc biết nạp thẻ hoặc rút tiền.
                string type_query = "AND type ='plus' AND user_id = 0";
                if (record_paymentGate.type == 0 || record_paymentGate.type == 2 || record_paymentGate.type == 4)
                {
                    type_query = "AND type='minus'";
                }
                string queryMerchant = "";
                if (!merchant_id.Trim().Equals("all"))
                {
                    queryMerchant += " AND merchant_id = " + merchant_id.Trim() + " ";
                }
                string query_wallet = "";
                switch (record_paymentGate.type)
                {
                    case 0:
                        query_wallet = "AND wallet_id != 0";

                        break;
                    case 1:
                        query_wallet = "AND wallet_id = 0";

                        break;
                    case 2:
                        query_wallet = "AND wallet_id = 0";

                        break;
                    case 3:
                        query_wallet = "AND wallet_id = 0";
                        break;
                    case 4:
                        query_wallet = "AND wallet_id = 0";
                        break;
                }
                string queryString = "SELECT ISNULL(SUM(value), 0) as 'value' FROM Transactions WHERE payment_gate IN(" + s_id + ") " + type_query + queryMerchant + " " + query_wallet + " AND time_creat >= '" + start_time_new + "' AND time_creat <= '" + end_time_new + " 23:59:59' ";
                SqlDataReader reader = serviceBase.query(queryString);
                double value = 0;
                while (reader.Read())
                {
                    value = Double.Parse(reader["value"].ToString().Trim());
                }
                DatabaseService.Instance.getConnection().Close();
                return Json(new
                {
                    error = false,
                    value = value.ToString("N", CultureInfo.InvariantCulture)
                });
            }


        }

        public ActionResult ListdataHistory(string id, String s_seach, String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength, string start_time, string end_time, string merchant_id)
        {
            using (var db = new GamePaymentContext())
            {
                int idPaymentGate = Int32.Parse(id);
                var record_otp = from c in db.PaymentGate
                                 where c.copyid == idPaymentGate
                                 select new
                                 {
                                     c.id
                                 };
                StringBuilder builder = new StringBuilder();
                string[] a_id = new string[record_otp.ToList().Count + 1];
                int i = 0;
                record_otp.ToList().ForEach(ent =>
                {
                    a_id[i] = ent.id.ToString();
                    i++;
                });

                a_id[i] = idPaymentGate.ToString();


                string s_id = string.Join(",", a_id);


                string[] a_start_time_new = start_time.Split('/');
                string start_time_new = a_start_time_new[2] + "-" + a_start_time_new[0] + "-" + a_start_time_new[1] + " 00:00:00.000";

                string[] a_end_time_new = end_time.Split('/');
                string end_time_new = a_end_time_new[2] + "-" + a_end_time_new[0] + "-" + a_end_time_new[1];

                string[] a_sort = new string[] { "id", "time_creat", "user_id", "value", "user_send", "value_out", "tag", "message" };
                ServiceBase serviceBase = new ServiceBase();
                
                var wi = db.PaymentGate.SingleOrDefault(e => e.id == idPaymentGate);
                string queryUser = "";

                var record_paymentGate = db.PaymentGate.Where(e => e.id == idPaymentGate).FirstOrDefault();
                //Nếu là cổng đặc biết nạp thẻ hoặc rút tiền.
                string type_query = "";
                string query_user_send = "";
                string query_wallet = "";
                switch (record_paymentGate.type)
                {
                    case 0:
                        //---check hardcode giftcode ở đây
                        var s = record_paymentGate.code.Replace(" ","");
                        if (s.Equals("GIFTCODE")|| s.Equals("NAPTHE"))
                        {
                            type_query = "AND type='plus'";
                            query_user_send = "(SELECT TOP 1 username FROM Users WHERE id = Transactions.user_send) as 'user_out' ";
                            query_wallet = "AND wallet_id != 0";

                            if (wi.agency_id != 0)
                            {
                                queryUser += "(SELECT TOP 1 title FROM Agency WHERE id = Transactions.user_id) as 'user_in' ";
                            }
                            else
                            {
                                queryUser += "(SELECT TOP 1 username FROM Users WHERE id = Transactions.user_id) as 'user_in' ";
                            }
                            break;
                        }
                        //---
                        type_query = "AND type='minus'";
                        query_user_send = "(SELECT TOP 1 username FROM Users WHERE id = Transactions.user_send) as 'user_out' ";
                        query_wallet = "AND wallet_id != 0";
                        if (wi.agency_id != 0)
                        {
                            queryUser += "(SELECT TOP 1 title FROM Agency WHERE id = Transactions.user_id) as 'user_in' ";
                        }
                        else
                        {
                            queryUser += "(SELECT TOP 1 username FROM Users WHERE id = Transactions.user_id) as 'user_in' ";
                        }
                        break;
                    case 1:
                        type_query = "AND type ='plus' AND user_id = 0";
                        query_user_send = "(SELECT TOP 1 username FROM Users WHERE id = Transactions.user_send) as 'user_out' ";
                        query_wallet = "AND wallet_id = 0";
                        if (wi.agency_id != 0)
                        {
                            queryUser += "(SELECT TOP 1 title FROM Agency WHERE id = Transactions.user_send) as 'user_in' ";
                        }
                        else
                        {
                            queryUser += "(SELECT TOP 1 username FROM Users WHERE id = Transactions.user_send) as 'user_in' ";
                        }
                        break;
                    case 2:
                        type_query = "AND type ='minus' AND user_id = 0";
                        query_user_send = "(SELECT TOP 1 username FROM Users WHERE id = Transactions.user_send) as 'user_out' ";
                        query_wallet = "AND wallet_id = 0";
                        if (wi.agency_id != 0)
                        {
                            queryUser += "(SELECT TOP 1 title FROM Agency WHERE id = Transactions.user_send) as 'user_in' ";
                        }
                        else
                        {
                            queryUser += "(SELECT TOP 1 username FROM Users WHERE id = Transactions.user_send) as 'user_in' ";
                        }
                        break;
                    case 3:
                        type_query = "AND type ='plus' AND user_id = 0";
                        query_user_send = "(SELECT TOP 1 title FROM Agency WHERE id = Transactions.user_send) as 'user_out' ";
                        query_wallet = "AND wallet_id = 0";
                        if (wi.agency_id != 0)
                        {
                            queryUser += "(SELECT TOP 1 title FROM Agency WHERE id = Transactions.user_send) as 'user_in' ";
                        }
                        else
                        {
                            queryUser += "(SELECT TOP 1 username FROM Users WHERE id = Transactions.user_send) as 'user_in' ";
                        }
                        break;
                    case 4:
                        type_query = "AND type ='minus' AND user_id = 0";
                        query_user_send = "(SELECT TOP 1 title FROM Agency WHERE id = Transactions.user_send) as 'user_out' ";
                        query_wallet = "AND wallet_id = 0";
                        if (wi.agency_id != 0)
                        {
                            queryUser += "(SELECT TOP 1 title FROM Agency WHERE id = Transactions.user_send) as 'user_in' ";
                        }
                        else
                        {
                            queryUser += "(SELECT TOP 1 username FROM Users WHERE id = Transactions.user_send) as 'user_in' ";
                        }
                        break;

                }
                string queryMerchant = "";
                if (!merchant_id.Trim().Equals("all"))
                {
                    queryMerchant += " AND merchant_id = " + merchant_id.Trim() + " ";
                }
                string queryString = "SELECT" +
                    " id" +
                    " ,time_creat" +
                    " ," + queryUser +
                    " ," + query_user_send +
                    " ,value" +
                    " ,value_out " +
                    " ,tag" +
                     " ,message" +
                     " ,user_send" +
                     " ,transactions_type" +
                    " FROM Transactions WHERE payment_gate IN(" + s_id + ") "+ type_query + queryMerchant + " "+ query_wallet + " AND time_creat >= '" + start_time_new + "' AND time_creat <= '" + end_time_new + " 23:59:59.999' ";

                string queryCount = "SELECT COUNT(id) as 'count' FROM Transactions WHERE payment_gate IN(" + s_id + ") " + type_query + queryMerchant + " " + query_wallet + " AND time_creat >= '" + start_time_new + "' AND time_creat <= '" + end_time_new + " 23:59:59.999' ";

                queryString += " ORDER BY " + a_sort[Int32.Parse(iSortCol_0)] + " " + sSortDir_0 + " OFFSET " + iDisplayStart + " ROWS FETCH NEXT " + iDisplayLength + " ROWS ONLY";
                SqlDataReader reader = serviceBase.query(queryString);
                List<String[]> users = new List<String[]>();

                while (reader.Read())
                {
                    string[] s_user = new string[8];
                    s_user[0] = reader["id"].ToString().Trim();
                    s_user[1] = reader["time_creat"].ToString().Trim();
                    s_user[2] = reader["user_in"].ToString();
                    s_user[3] =Double.Parse(reader["value"].ToString()).ToString("N", CultureInfo.InvariantCulture);
                    if (reader["transactions_type"].ToString().Trim().Equals("2"))
                    {
                        var user_send = Int32Ext.toInt(reader["user_send"].ToString().Trim());
                        if (reader["tag"].ToString().Trim().Equals("Agency nap tien agency con"))
                        {
                            
                            var item_agency = db.Agency.SingleOrDefault(e => e.id == user_send);
                            if (item_agency != null)
                            {
                                s_user[4] = item_agency.title;

                            }
                            else
                            {
                                s_user[4] = "";
                            }

                        }
                        else
                        {
                            var itemuser = db.User.SingleOrDefault(e => e.id == user_send);
                            if (itemuser != null)
                            {
                                s_user[4] = itemuser.username;
                            }
                            else
                            {
                                s_user[4] ="";
                            }
                            
                        }
                    }
                    else
                    {
                        s_user[4] = reader["user_out"].ToString();
                    }
                   
                    s_user[5] = Double.Parse(reader["value_out"].ToString()).ToString("N", CultureInfo.InvariantCulture);
                    s_user[6] = reader["tag"].ToString();
                    s_user[7] = reader["message"].ToString();
                    users.Add(s_user);
                }
                DatabaseService.Instance.getConnection().Close();
                int row = 0;
                reader = serviceBase.query(queryCount);
                while (reader.Read())
                {
                    row = Int32.Parse(reader["count"].ToString());
                }
                DatabaseService.Instance.getConnection().Close();

                return Json(new
                {
                    aaData = users,
                    iTotalRecords = row,
                    iTotalDisplayRecords = row,
                    display = iDisplayLength
                });
            }
        }


        [HttpPost]
        public ActionResult Listdata(String s_seach, String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength)
        {
            using (var db = new GamePaymentContext())
            {
                DateTime datetime = DateTime.Now;
                string[] a_sort = new string[] { "id", "code", "name", "id", "sms", "type", "type", "status_active", "id" };
                ServiceBase serviceBase = new ServiceBase();
                string queryString = "SELECT" +
                    " id" +
                    " ,code" +
                    " ,name" +
                    " ,status_active" +
                    " ,sms" +
                    " ,type" +
                    " ,agency_id" +
                    " FROM PaymentGate WHERE copyid = 0 ";

                string queryCount = "SELECT COUNT(id) as 'count' FROM PaymentGate WHERE copyid = 0 ";

                queryString += " ORDER BY " + a_sort[Int32.Parse(iSortCol_0)] + " " + sSortDir_0 + " OFFSET " + iDisplayStart + " ROWS FETCH NEXT " + iDisplayLength + " ROWS ONLY";
                SqlDataReader reader = serviceBase.query(queryString);

                List<String[]> users = new List<String[]>();

                while (reader.Read())
                {
                    string s_sms = "";
                    if (reader["sms"].ToString().Trim().Equals("1"))
                    {
                        s_sms = "<span class=\"label label-primary\">Sử dụng</span>";
                    }
                    string[] s_user = new string[9];
                    s_user[0] = reader["id"].ToString().Trim();
                    s_user[1] = reader["code"].ToString().Trim();
                    s_user[2] = reader["name"].ToString().Trim();
                    s_user[3] = "<button onclick=\"paymentgate.historyTransactions('" + reader["id"].ToString() + "')\" type=\"button\" class=\"btn btn-default btn-xs btn-history \">Lịch sử</button>";
                    s_user[4] = s_sms;
                    if (!reader["agency_id"].ToString().Trim().Equals("0"))
                    {
                        int agency_id = Int32.Parse(reader["agency_id"].ToString().Trim());
                        s_user[5] = "Agency tạo";
                        var agency_item = db.Agency.Where(u => (u.id == agency_id)).FirstOrDefault();
                        s_user[6] = agency_item.title;
                    }
                    else
                    {
                        switch (reader["type"].ToString().Trim())
                        {
                            case "0":
                                s_user[5] = "Giao dịch trong game";
                                s_user[6] = "Admin";
                                break;
                            case "1":
                                s_user[5] = "Nạp tiền cho User";
                                s_user[6] = "Admin";
                                break;
                            case "2":
                                s_user[5] = "Rút tiền của User";
                                s_user[6] = "Kế toán";
                                break;
                            case "3":
                                s_user[5] = "Nạp tiền cho Agency";
                                s_user[6] = "Admin";
                                break;
                            case "4":
                                s_user[5] = "Rút tiền cho Agency";
                                s_user[6] = "Kế toán";
                                break;
                            case "5":
                                s_user[5] = "Agency nạp tiền cho user";
                                s_user[6] = "Admin";
                                break;
                            case "6":
                                s_user[5] = "Rút tiền user thẻ cào";
                                s_user[6] = "Kế toán";
                                break;
                        }

                    }

                    s_user[7] = "<a data-active=\"" + reader["status_active"].ToString() + "\" onclick=\"paymentgate.showEditActive(" + reader["id"].ToString() + ",this)\">" + reader["status_active"].ToString() + "</a>";
                    if (reader["agency_id"].ToString().Trim().Equals("0") && !reader["type"].ToString().Trim().Equals("2") && !reader["type"].ToString().Trim().Equals("4"))
                    {
                        s_user[8] = "<button onclick=\"paymentgate.showEdit('" + reader["id"].ToString() + "')\" type=\"button\" class=\"btn btn-default btn-xs\">Sửa</button><button onclick=\"paymentgate.showDelete('" + reader["id"].ToString() + "')\"  type=\"button\" class=\"btn btn-danger btn-xs\">Xóa</button>";
                    }
                    else
                    {
                        s_user[8] = "";
                    }

                    users.Add(s_user);
                }
                DatabaseService.Instance.getConnection().Close();

                int row = 0;
                reader = serviceBase.query(queryCount);
                while (reader.Read())
                {
                    row = Int32.Parse(reader["count"].ToString());
                }
                DatabaseService.Instance.getConnection().Close();

                return Json(new
                {
                    aaData = users,
                    iTotalRecords = row,
                    iTotalDisplayRecords = row,
                    display = iDisplayLength
                });
            }
        }
        //Edit user active/deactive
        [HttpPost]
        public ActionResult postActive(int id, string status, string note)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            //Update user
            string queryStringUpdateUser = "UPDATE PaymentGate SET status_active='" + status + "' WHERE id = '" + id + "'";
            reader = serviceBase.query(queryStringUpdateUser);
            DatabaseService.Instance.getConnection().Close();
            //Insert Log
            string queryStringInsertLog = "INSERT INTO ActivityLog(accountid,userid,type,note,type_action) VALUES ('" + Session["UserID"] + "','" + id + "','" + status + "',N'" + note + "','paymentgate')";
            reader = serviceBase.query(queryStringInsertLog);
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false
            });
        }
        [HttpPost]
        public ActionResult add(string title, string code, int coin_in, int coin_out, CashIORate[] items, int fee)
        {
            PaymentGate paymentGate = new PaymentGate();
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string quertFindCode = "SELECT TOP 1 * FROM PaymentGate WHERE code ='" + code + "'";
            reader = serviceBase.query(quertFindCode);
            while (reader.Read())
            {
                paymentGate.id = Int32.Parse(reader["id"].ToString());
                paymentGate.name = reader["name"].ToString();
            }
            DatabaseService.Instance.getConnection().Close();
            if (paymentGate.id != 0)
            {
                return Json(new
                {
                    error = true,
                    msg = "Mã code đã bị trùng."
                });
            }
            string queryStringInsertLog = "INSERT INTO PaymentGate(code,name,coin_in,coin_out,status_active,fee) VALUES (N'" + code + "',N'" + title + "','" + coin_in + "','" + coin_out + "','active'," + fee + ")";
            reader = serviceBase.query(queryStringInsertLog);
            DatabaseService.Instance.getConnection().Close();
            //Lấy id insert
            string quertFindCodeId = "SELECT TOP 1 * FROM PaymentGate WHERE code ='" + code + "'";
            reader = serviceBase.query(quertFindCodeId);
            while (reader.Read())
            {
                paymentGate.id = Int32.Parse(reader["id"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();

            //Insert giá trị in out
            string queryStringInsertCashIORate = "";
            //foreach (var item in items)
            //{
            //    queryStringInsertCashIORate = "INSERT INTO CashIORate(payment_gate_id,cash_in,cash_out) VALUES ('" + paymentGate.id + "','" + item.cash_in + "','" + item.cash_out + "')";
            //    reader = serviceBase.query(queryStringInsertCashIORate);
            //    DatabaseService.Instance.getConnection().Close();
            //}
            return Json(new
            {
                error = false,
                stringquery = queryStringInsertCashIORate
            });
        }

        [HttpPost]
        public ActionResult WalletsIn()
        {
            try
            {
                var agencyID = (new AuthenticationHelper(Session).ID());
                using (var db = new GamePaymentContext())
                {
                    var query = from e in db.Wallet
                                select new
                                {
                                    e.id,
                                    e.name
                                };
                    return Json(new
                    {
                        error = false,
                        items = query.ToList()
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.ToString()
                });
            }
        }
        [HttpPost]
        public ActionResult Create(string name, string code, List<CashIORate> iorate, string sms, string type, int fee)
        {
            try
            {
                var adminID = (new AuthenticationHelper(Session).ID());

                using (var context = new GamePaymentContext())
                {
                    var inst = new PaymentGate
                    {
                        name = name,
                        code = code,
                        status_active = "active",
                        sms = Int32Ext.toInt(sms),
                        type = Int32Ext.toInt(type),
                        agency_id = 0,
                        fee = fee
                    };
                    context.PaymentGate.Add(inst);
                    context.SaveChanges();

                    // Filter cashIO Rate key pair is not null element
                    iorate.Where(e =>
                    {
                        return (e.cash_in != 0 && e.cash_out != 0);
                    });

                    // Set gateID for cashIO rate 
                    iorate.ForEach(ele =>
                    {
                        ele.payment_gate_id = inst.id;
                    });
                    context.CashIORate.AddRange(iorate);
                    context.SaveChanges();
                }

                return Json(new
                {
                    error = false,
                    msg = ""
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }
        [HttpPost]
        public ActionResult Detail(string id)
        {
            var queryid = Int32Ext.toInt(id);
            var adminID = (new AuthenticationHelper(Session).ID());
            try
            {

                using (var db = new GamePaymentContext())
                {
                    var ent = (from g in db.PaymentGate
                               where (g.id == queryid)
                               select new
                               {
                                   g.id,
                                   g.sms,
                                   g.type,
                                   g.fee,
                                   g.name,
                                   g.code,
                                   g.status_active,
                                   merchant = g.merchant_id,
                                   g.wallet_in,
                                   wallet = g.wallet_out
                               }).First();
                    var wallets = (new WalletService()).find(ent.merchant);

                    var cashIORates = from cio in db.CashIORate
                                      where cio.payment_gate_id == queryid
                                      select new
                                      {
                                          cio.id,
                                          cio.cash_in,
                                          cio.cash_out
                                      };
                    return Json(new
                    {
                        error = false,
                        item = ent,
                        wallets = wallets,
                        rateIO = cashIORates.ToList()
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }

        public Boolean check_delete_paymentgate(int id)
        {
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT TOP 1 * FROM Transactions WHERE payment_gate = '" + id + "'";

            SqlDataReader reader = serviceBase.query(queryString);

            int count = 0;
            while (reader.Read())
            {
                count++;
            }
            DatabaseService.Instance.getConnection().Close();
            if (count > 0)
            {
                return false;
            }
            return true;
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            int paymentGateID = Int32.Parse(id.ToString());
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            if (!check_delete_paymentgate(paymentGateID))
            {
                return Json(new
                {
                    error = true,
                    msg = "Cổng thanh toán đã giao dịch không thể xóa."
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string queryStringUpdateUser = "DELETE FROM PaymentGate WHERE id = '" + id + "'";
            reader = serviceBase.query(queryStringUpdateUser);
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false
            });
        }

        [HttpPost]
        public ActionResult Update(string id, string name, string code, string merchantid, string wallet, string wallet_in, List<CashIORate> iorate, string sms, string purchase, string type, string fee)
        {
            try
            {
                var agencyID = (new AuthenticationHelper(Session).ID());
                var gateid = Int32Ext.toInt(id);
                var walletIn = Int32Ext.toInt(wallet_in);
                var walletTo = Int32Ext.toInt(wallet);
                var merchant = Int32Ext.toInt(merchantid);
                var idfee = Int32Ext.toInt(fee);
                var typePaymentGate = Int32Ext.toInt(type);
                using (var db = new GamePaymentContext())
                {

                    var inst = db.PaymentGate.FirstOrDefault(g => (g.id == gateid));
                    if (inst.agency_id != 0)
                    {
                        throw new Exception("Cổng thanh toán thuộc Agency");
                    }
                    if (inst != null)
                    {
                        inst.code = code;
                        inst.name = name;
                        inst.fee = idfee;
                        inst.sms = Int32Ext.toInt(sms);
                        inst.wallet_out = inst.wallet_out;
                        inst.wallet_in = inst.wallet_in;
                        inst.type = Int32Ext.toInt(type);
                        inst.agency_id = 0;
                        db.SaveChanges();
                        // Check and update io rate
                        if (iorate != null && iorate.Count > 0)
                        {
                            // Filter cashIO Rate key pair is not null element
                            iorate.Where(e =>
                            {
                                return (e.cash_in != 0 && e.cash_out != 0);
                            });

                            //Xóa rate io không tồn tại
                            var query = from c in db.CashIORate
                                        where c.payment_gate_id == gateid
                                        select new
                                        {
                                            c.id,
                                            c.payment_gate_id,
                                            c.cash_out,
                                            c.cash_in,
                                        };

                            query.ToList().ForEach(ent =>
                            {

                                if (!iorate.Exists(cash => (cash.id == ent.id)))
                                {
                                    var cashio = db.CashIORate.FirstOrDefault(cash => (cash.id == ent.id));
                                    db.CashIORate.Remove(cashio);
                                    db.SaveChanges();
                                }
                            });

                            // Set gateID for cashIO rate 
                            iorate.ForEach(ele =>
                            {
                                if (ele.id == 0)
                                {
                                    ele.payment_gate_id = gateid;
                                    db.CashIORate.Add(ele);
                                }
                                else
                                {
                                    // Check for update
                                    var iorRec = db.CashIORate.FirstOrDefault(g => g.id == ele.id);
                                    if (iorRec != null)
                                    {
                                        iorRec.cash_in = ele.cash_in;
                                        iorRec.cash_out = ele.cash_out;
                                    }
                                }
                            });
                        }
                        db.SaveChanges();
                    }

                    var record_otp = from c in db.PaymentGate
                                     where c.copyid == gateid
                                     select new
                                     {
                                         c.id,
                                         c.code,
                                         c.name,
                                         c.sms,
                                         c.merchant_id,
                                         c.wallet_out,
                                         c.wallet_in,
                                         c.agency_id
                                     };
                    record_otp.ToList().ForEach(entp =>
                    {
                        if (entp != null)
                        {
                            var inst_child = db.PaymentGate.FirstOrDefault(g => (g.id == entp.id));
                            inst_child.code = code;
                            inst_child.name = name;
                            inst_child.sms = Int32Ext.toInt(sms);
                            inst_child.merchant_id = entp.merchant_id;
                            inst_child.wallet_out = entp.merchant_id;
                            inst_child.wallet_in = entp.wallet_in;
                            inst_child.agency_id = 0;
                            db.SaveChanges();
                            // Check and update io rate
                            if (iorate != null && iorate.Count > 0)
                            {
                                // Filter cashIO Rate key pair is not null element
                                iorate.Where(e =>
                                {
                                    return (e.cash_in != 0 && e.cash_out != 0);
                                });
                                //Xóa rate io không tồn tại
                                var query = from c in db.CashIORate
                                            where c.payment_gate_id == entp.id
                                            select new
                                            {
                                                c.id,
                                                c.payment_gate_id,
                                                c.cash_out,
                                                c.cash_in,
                                            };

                                query.ToList().ForEach(ent =>
                                {
                                    var cashio = db.CashIORate.FirstOrDefault(cash => (cash.id == ent.id));
                                    db.CashIORate.Remove(cashio);
                                    db.SaveChanges();
                                });

                                // Set gateID for cashIO rate 
                                iorate.ForEach(ele =>
                                {
                                    ele.payment_gate_id = entp.id;
                                    db.CashIORate.Add(ele);
                                });
                            }
                            db.SaveChanges();
                        }
                    });


                }

                return Json(new
                {
                    error = false,
                    msg = ""
                });

            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }



        [HttpPost]
        public ActionResult profit(string start_time, string end_time, string merchant_id)
        {
            string[] a_start_time_new = start_time.Split('/');
            string start_time_new = a_start_time_new[2] + "-" + a_start_time_new[0] + "-" + a_start_time_new[1] + " 00:00:00";

            string[] a_end_time_new = end_time.Split('/');
            string end_time_new = a_end_time_new[2] + "-" + a_end_time_new[0] + "-" + a_end_time_new[1];

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            string queryString = "";
            if (!merchant_id.Trim().Equals("all"))
            {
                queryString = "SELECT TOP 10 id,name,(SELECT ISNULL(SUM(profit),0) FROM Transactions WHERE " +
    "payment_gate = PaymentGate.id " +
    "AND time_creat >= '" + start_time_new + "' AND time_creat <= '" + end_time_new + " 23:59:59' ) as 'value'," +
    "(SELECT TOP 1 name FROM wallet WHERE id = PaymentGate.wallet_in) as 'wallet_name' " +
    "FROM PaymentGate WHERE (agency_id = 0 OR agency_id IS NULL) AND merchant_id ='" + merchant_id.Trim() + "'  AND wallet_in = wallet_out AND wallet_in != 0 ";
            }
            else
            {
                queryString = "SELECT TOP 10 id,name,(SELECT ISNULL(SUM(profit),0) FROM Transactions WHERE " +
"(payment_gate = PaymentGate.id OR payment_gate IN (SELECT id FROM PaymentGate WHERE copyid=PaymentGate.id ) ) " +
"AND time_creat >= '" + start_time_new + "' AND time_creat <= '" + end_time_new + " 23:59:59' ) as 'value'," +
"(SELECT TOP 1 name FROM wallet WHERE id = PaymentGate.wallet_in) as 'wallet_name' " +
"FROM PaymentGate WHERE (agency_id = 0 OR agency_id IS NULL) AND copyid = 0 AND wallet_in = wallet_out AND wallet_in != 0 ";
            }


            reader = serviceBase.query(queryString);
            List<ResponseProfit> l_responseProfit = new List<ResponseProfit>();
            while (reader.Read())
            {
                ResponseProfit responseProfit = new ResponseProfit();
                responseProfit.id = Int32.Parse(reader["id"].ToString().Trim());
                responseProfit.name = reader["name"].ToString().Trim();
                responseProfit.wallet_name = reader["wallet_name"].ToString().Trim();
                responseProfit.value = Double.Parse(reader["value"].ToString().Trim()).ToString("N", CultureInfo.InvariantCulture);
                l_responseProfit.Add(responseProfit);
            }
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false,
                items = l_responseProfit
            });
        }
        [HttpPost]
        public ActionResult ListdataProfit(String paymentgate_id, string merchant_id, string start_time, string end_time, String s_seach, String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength)
        {
            string[] a_start_time_new = start_time.Split('/');
            string start_time_new = a_start_time_new[2] + "-" + a_start_time_new[0] + "-" + a_start_time_new[1] + " 00:00:00.000";

            string[] a_end_time_new = end_time.Split('/');
            string end_time_new = a_end_time_new[2] + "-" + a_end_time_new[0] + "-" + a_end_time_new[1];

            DateTime datetime = DateTime.Now;
            string[] a_sort = new string[] { "id", "time_creat", "payment_gate", "wallet_id", "profit", "user_id", "user_send", "tag", "message" };
            ServiceBase serviceBase = new ServiceBase();
            string query_merchant_id = "";
            if (!merchant_id.Trim().Equals("all"))
            {
                query_merchant_id += " AND merchant_id = '" + merchant_id.Trim() + "' ";
            }
            string queryString = "SELECT " +
            "id " +
            ",time_creat " +
            ",(SELECT TOP 1 name FROM PaymentGate WHERE id = Transactions.payment_gate) as 'payment_gate_name' " +
            ",(SELECT TOP 1 name FROM wallet WHERE id = Transactions.wallet_id) as 'wallet_name' " +
            ",profit " +
            ",(SELECT TOP 1 username FROM Users WHERE id = Transactions.user_id ) as 'username' " +
            ",(SELECT TOP 1 username FROM Users WHERE id = Transactions.user_send ) as 'username_send' " +
            ",tag " +
            ",message " +
            "FROM Transactions WHERE type = 'minus' " + query_merchant_id + " AND (transactions_type =0 OR transactions_type IS NULL) AND profit > 0 AND time_creat >= '" + start_time_new + "' AND time_creat <= '" + end_time_new + " 23:59:59.999' ";

            string queryCount = "SELECT COUNT(id) as 'count' FROM Transactions  WHERE type = 'minus' " + query_merchant_id + " AND (transactions_type =0 OR transactions_type IS NULL) AND profit > 0 AND time_creat >= '" + start_time_new + "' AND time_creat <= '" + end_time_new + " 23:59:59.999' ";

            queryString += " ORDER BY " + a_sort[Int32.Parse(iSortCol_0)] + " " + sSortDir_0 + " OFFSET " + iDisplayStart + " ROWS FETCH NEXT " + iDisplayLength + " ROWS ONLY";
            SqlDataReader reader = serviceBase.query(queryString);

            List<String[]> users = new List<String[]>();

            while (reader.Read())
            {
                string[] s_user = new string[9];
                s_user[0] = reader["id"].ToString().Trim();
                s_user[1] = reader["time_creat"].ToString().Trim();
                s_user[2] = reader["payment_gate_name"].ToString().Trim();
                s_user[3] = reader["wallet_name"].ToString().Trim();
                s_user[4] = Double.Parse(reader["profit"].ToString().Trim()).ToString("N", CultureInfo.InvariantCulture);
                s_user[5] = reader["username"].ToString().Trim();
                s_user[6] = reader["username_send"].ToString().Trim();
                s_user[7] = reader["tag"].ToString().Trim();
                s_user[8] = reader["message"].ToString().Trim();

                users.Add(s_user);
            }
            DatabaseService.Instance.getConnection().Close();

            int row = 0;
            reader = serviceBase.query(queryCount);
            while (reader.Read())
            {
                row = Int32.Parse(reader["count"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength
            });
        }

    }
}