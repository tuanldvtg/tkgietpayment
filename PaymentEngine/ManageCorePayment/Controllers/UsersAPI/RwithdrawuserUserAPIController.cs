﻿using BillingCard;
using BillingCard.Models;
using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.Models.Alego;
using ManageCorePayment.PrivateServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers.AgencyAPI
{
    public class RwithdrawuserUserAPIController : Controller
    {

        //Lấy số tiền trong ví
        public double getBalace(int user_id, int wallet_in, int merchant_id)
        {
            double balance = 0;
            ServiceBase serviceBase = new ServiceBase();
            string query = "SELECT TOP 1 balance FROM Transactions WHERE wallet_id =@wallet_in " +
                "AND user_id=@user_id AND (transactions_type = 0 OR transactions_type IS NULL) AND merchant_id='" + merchant_id + "' ORDER BY id DESC";
            SqlCommand command = new SqlCommand(query, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@wallet_in", wallet_in);
            command.Parameters.AddWithValue("@user_id", user_id);
            command.Parameters.AddWithValue("@merchant_id", merchant_id);
            SqlDataReader reader = serviceBase.querySqlParam(command);
            while (reader.Read())
            {
                balance = double.Parse(reader["balance"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return balance;
        }


        private bool updateBalaceWalletUserNoAccounting(int idID)
        {
            using (var db = new GamePaymentContext())
            {

                var wi = db.Rwithdrawal_User.FirstOrDefault(e => e.id == idID);
                if (wi == null)
                {
                    return false;
                }
                var query = db.Transactions
.OrderByDescending(p => p.id)
.Where(e => (e.user_id == wi.user_id && e.transactions_type == 0 && e.wallet_id == wi.wallet_id))
.FirstOrDefault();
                double userMoney = 0;
                if (query != null)
                {
                    userMoney = query.balance;
                }
                if (userMoney < wi.value)
                {
                    return false;
                }

                if (wi != null)
                {
                    wi.approved = 3;
                    wi.accounting_id = 0;
                    wi.approved_time = DateTime.Now;
                    db.SaveChanges();
                }
                //Lấy user giao dịch
                var user_item = db.User.FirstOrDefault(u => u.id == wi.user_id);

                double moneyuserMinus = userMoney - wi.value;


                //Trừ tiền merchant tạm giữ
                var auth = new AuthenticationHelper(Session);
                var merchant_id = Int32Ext.toInt(auth.getMerchantID());
                var merchant_item = db.Merchant.FirstOrDefault(m => m.id == merchant_id);
                var user_merchant = db.User.FirstOrDefault(u => u.username == merchant_item.username);
                var query_merchant = db.Transactions
.OrderByDescending(p => p.id)
.Where(e => (e.user_id == user_merchant.id && e.transactions_type == 0 && e.wallet_id == wi.wallet_id))
.FirstOrDefault();
                double userMoney_merchant = 0;
                if (query_merchant != null)
                {
                    userMoney_merchant = query_merchant.balance;
                }
                userMoney_merchant = userMoney_merchant - wi.value;
                db.Transactions.Add(new Models.Transactions()
                {
                    user_id = user_merchant.id,
                    merchant_id = merchant_id,
                    payment_gate = 0,
                    wallet_id = wi.wallet_id,
                    balance = userMoney_merchant,
                    type = "minus",
                    value = wi.value,
                    transactions_type = 0,
                    user_send = 0,
                    tag = "ruttien",
                    message = "Trả tiền tạm giữ của user:" + user_item.username + " cho merchant ",
                    time_creat = DateTime.Now,
                    value_out = wi.value
                });
                db.SaveChanges();
                //Lưu log Merchant gửi cho Agency
                db.Log_Merchant_To_Agency.Add(new Models.Log_Merchant_To_Agency()
                {
                    user_id = user_merchant.id,
                    time = DateTime.Now,
                    agency_id = wi.agency_id,
                    value = wi.value,
                    message = "Khi " + user_item.username + " rút tiền thành công"
                });
                db.SaveChanges();
                //Cộng tiền cho agency
                var query_agency = db.Transactions
.OrderByDescending(p => p.id)
.Where(e => (e.user_id == wi.agency_id && e.wallet_id == wi.wallet_id && e.transactions_type == 2))
.FirstOrDefault();
                double userMoney_agency = 0;
                if (query_agency != null)
                {
                    userMoney_agency = query_agency.balance;
                }
                userMoney_agency = userMoney_agency + wi.value;
                db.Transactions.Add(new Models.Transactions()
                {
                    user_id = wi.agency_id,
                    merchant_id = 0,
                    payment_gate = wi.paymentgate_id,
                    wallet_id = wi.wallet_id,
                    balance = userMoney_agency,
                    type = "plus",
                    value = wi.value,
                    transactions_type = 2,
                    user_send = wi.user_id,
                    tag = "ruttien",
                    message = "Cộng tiền user:" + user_item.username + " rút tiền ",
                    time_creat = DateTime.Now,
                    value_out = wi.value
                });
                db.SaveChanges();


                //Update ví rút user
                var query_user = db.Transactions
.OrderByDescending(p => p.id)
.Where(e => (e.user_id == wi.user_id && e.wallet_id == 0 && e.type == "minus" && e.transactions_type == 0))
.FirstOrDefault();
                double userMoney_user = 0;
                if (query_user != null)
                {
                    userMoney_user = query_user.balance;
                }

                userMoney_user = userMoney_user + wi.value_out;
                db.Transactions.Add(new Models.Transactions()
                {
                    user_id = wi.user_id,
                    merchant_id = 0,
                    payment_gate = wi.paymentgate_id,
                    wallet_id = 0,
                    balance = userMoney_user,
                    type = "minus",
                    value = wi.value,
                    transactions_type = 0,
                    user_send = 0,
                    tag = "ruttien",
                    message = wi.note,
                    time_creat = DateTime.Now,
                    value_out = wi.value_out
                });
                db.SaveChanges();


                //Update ví rút admin
                var query_admin = db.Transactions
.OrderByDescending(p => p.id)
.Where(e => (e.user_id == 0 && e.wallet_id == 0 && e.type == "minus"))
.FirstOrDefault();
                double userMoney_admin = 0;
                if (query_admin != null)
                {
                    userMoney_admin = query_admin.balance;
                }

                userMoney_admin = userMoney_admin + wi.value_out;
                db.Transactions.Add(new Models.Transactions()
                {
                    user_id = 0,
                    merchant_id = 0,
                    payment_gate = wi.paymentgate_id,
                    wallet_id = 0,
                    balance = userMoney_admin,
                    type = "minus",
                    value = wi.value,
                    transactions_type = 0,
                    user_send = wi.user_id,
                    tag = "ruttien",
                    message = wi.note,
                    time_creat = DateTime.Now,
                    value_out = wi.value_out
                });
                db.SaveChanges();
                return true;
            }
        }

        private bool updateBalaceWalletUser(int idID)
        {
            using (var db = new GamePaymentContext())
            {
                var wi = db.Rwithdrawal_User.FirstOrDefault(e => e.id == idID);
                if (wi == null)
                {
                    return false;
                }
                var query = db.Transactions
.OrderByDescending(p => p.id)
.Where(e => (e.user_id == wi.user_id && e.transactions_type == 0 && e.wallet_id == wi.wallet_id))
.FirstOrDefault();
                double userMoney = 0;
                if (query != null)
                {
                    userMoney = query.balance;
                }
                if (userMoney < wi.value)
                {
                    return false;
                }

                //if (wi != null)
                //{
                //    wi.approved = 3;
                //    wi.accounting_id = 0;
                //    wi.approved_time = DateTime.Now.ToString();
                //    db.SaveChanges();
                //}

                double moneyuserMinus = userMoney - wi.value;

                //Trừ vào ví user
                db.Transactions.Add(new Models.Transactions()
                {
                    user_id = wi.user_id,
                    merchant_id = 0,
                    payment_gate = wi.paymentgate_id,
                    wallet_id = wi.wallet_id,
                    balance = moneyuserMinus,
                    type = "minus",
                    value = wi.value,
                    transactions_type = 0,
                    user_send = 0,
                    tag = "ruttien",
                    message = "Bán coin cho agency",
                    time_creat = DateTime.Now,
                    value_out = wi.value_out
                });
                db.SaveChanges();
                //Cộng tiền merchant tạm giữ
                var auth = new AuthenticationHelper(Session);
                var merchant_id = Int32Ext.toInt(auth.getMerchantID());
                var merchant_item = db.Merchant.FirstOrDefault(m => m.id == merchant_id);
                var user_merchant = db.User.FirstOrDefault(u => u.username == merchant_item.username);
                var query_merchant = db.Transactions
.OrderByDescending(p => p.id)
.Where(e => (e.user_id == user_merchant.id && e.transactions_type == 0 && e.wallet_id == wi.wallet_id))
.FirstOrDefault();
                double userMoney_merchant = 0;
                if (query_merchant != null)
                {
                    userMoney_merchant = query_merchant.balance;
                }
                userMoney_merchant = userMoney_merchant + wi.value;
                db.Transactions.Add(new Models.Transactions()
                {
                    user_id = user_merchant.id,
                    merchant_id = 0,
                    payment_gate = wi.paymentgate_id,
                    wallet_id = wi.wallet_id,
                    balance = userMoney_merchant,
                    type = "plus",
                    value = wi.value,
                    transactions_type = 0,
                    user_send = 0,
                    tag = "ruttien",
                    message = "Tạm giữ coin bán cho agency ",
                    time_creat = DateTime.Now,
                    value_out = wi.value
                });
                db.SaveChanges();

                //Update ví rút user
                //                var query_user = db.Transactions
                //.OrderByDescending(p => p.id)
                //.Where(e => (e.user_id == wi.user_id && e.wallet_id == 0 && e.type == "minus" && e.transactions_type == 0))
                //.FirstOrDefault();
                //                double userMoney_user = 0;
                //                if (query_user != null)
                //                {
                //                    userMoney_user = query_user.balance;
                //                }

                //                userMoney_user = userMoney_user + wi.value_out;
                //                db.Transactions.Add(new Models.Transactions()
                //                {
                //                    user_id = wi.user_id,
                //                    merchant_id = 0,
                //                    payment_gate = wi.paymentgate_id,
                //                    wallet_id = 0,
                //                    balance = userMoney_user,
                //                    type = "minus",
                //                    value = wi.value,
                //                    transactions_type = 0,
                //                    user_send = 0,
                //                    tag = "ruttien",
                //                    message = wi.note,
                //                    time_creat = DateTime.Now,
                //                    value_out = wi.value_out
                //                });
                //                db.SaveChanges();


                //                //Update ví rút admin
                //                var query_admin = db.Transactions
                //.OrderByDescending(p => p.id)
                //.Where(e => (e.user_id == 0 && e.wallet_id == 0 && e.type == "minus"))
                //.FirstOrDefault();
                //                double userMoney_admin = 0;
                //                if (query_admin != null)
                //                {
                //                    userMoney_admin = query_admin.balance;
                //                }

                //                userMoney_admin = userMoney_admin + wi.value_out;
                //                db.Transactions.Add(new Models.Transactions()
                //                {
                //                    user_id = 0,
                //                    merchant_id = 0,
                //                    payment_gate = wi.paymentgate_id,
                //                    wallet_id = 0,
                //                    balance = userMoney_admin,
                //                    type = "minus",
                //                    value = wi.value,
                //                    transactions_type = 0,
                //                    user_send = wi.user_id,
                //                    tag = "ruttien",
                //                    message = wi.note,
                //                    time_creat = DateTime.Now,
                //                    value_out = wi.value_out
                //                });
                //                db.SaveChanges();
                return true;
            }
        }

        [HttpPost]
        public ActionResult confirmRwithdrawuser(string id, string password)
        {
            if (id == null || password == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }

            var auth = new AuthenticationHelper(Session);
            var usersID = auth.ID();
            var merchant_id = Int32.Parse(auth.getMerchantID());

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            SqlCommand command;
            
            using (var db = new GamePaymentContext())
            {
                string queryCheckPassword = "SELECT TOP 1 id,password FROM Users WHERE id =@usersID AND password =@password ";
                command = new SqlCommand(queryCheckPassword, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@usersID", usersID);
                string pass_check = CryptoHelper.MD5Hash(password, usersID.ToString());
                command.Parameters.AddWithValue("@password", pass_check);
                reader = serviceBase.querySqlParam(command);
                int countcheck = 0;
                while (reader.Read())
                {
                    countcheck++;
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();
                if (countcheck == 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Mật khẩu không đúng."
                    });
                }


                string queryString = "SELECT TOP 1 " +
                    "id," +
                     "code," +
                    "create_time," +
                    "agency_id," +
                    "value," +
                    "wallet_id," +
                    "(SELECT TOP 1 name FROM Wallet WHERE id = Rwithdrawal_User.wallet_id ) as 'wallet_name'," +
                    "(SELECT TOP 1 title FROM Agency WHERE id = Rwithdrawal_User.agency_id ) as 'agency_name'" +
                    " FROM Rwithdrawal_User WHERE id =@id";
                command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@id", id);
                reader = serviceBase.querySqlParam(command);
                Rwithdrawal_UserResponse rwithdrawal = new Rwithdrawal_UserResponse();
                int count = 0;
                while (reader.Read())
                {
                    count++;
                    rwithdrawal.id = Int32.Parse(reader["id"].ToString().Trim());
                    rwithdrawal.create_time = reader["create_time"].ToString().Trim();
                    rwithdrawal.wallet_id = Int32.Parse(reader["wallet_id"].ToString().Trim());
                    rwithdrawal.wallet_name = reader["wallet_name"].ToString().Trim();
                    rwithdrawal.code = reader["code"].ToString().Trim();
                    rwithdrawal.agency_id = Int32.Parse(reader["agency_id"].ToString().Trim());
                    rwithdrawal.agency_name = reader["agency_name"].ToString().Trim();
                    rwithdrawal.value = Double.Parse(reader["value"].ToString().Trim());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();
                if (count == 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Mã code không đúng"
                    });
                }
                ////---chống rút tiền
                //---tien`nap
                double ballanceNap = 0;
                try
                {
                    ballanceNap = db.Transactions.Where(e => (e.user_id == usersID && e.wallet_id == 11 && e.type == "plus" && (e.payment_gate == 2066 || e.tag == "Agency+"))).Sum(e => e.balance);
                }
                catch (Exception) { }
                if (ballanceNap == 0)
                {
                    DatabaseService.Instance.getConnection().Close();
                    return Json(new
                    {
                        error = true,
                        msg = "Bạn chưa nạp tiền, nên không được rút tiền."
                    });
                }

                DatabaseService.Instance.getConnection().Close();

                //---tien` rut'
                double ballanceRut = 0;
                try
                {
                    ballanceRut = db.Transactions.Where(e => (e.user_id == usersID && e.wallet_id == 11 && e.type == "minus" && (e.payment_gate == 2061 || e.payment_gate == 2072 || e.payment_gate == 2082))).Sum(e => e.balance);
                }
                catch (Exception) { }
                //---tien` cho merchant
                var ballanceMerchant = db.Transactions.Where(e => (e.user_send == usersID && e.user_id == 155 && e.wallet_id == 11 && e.type == "minus" && e.payment_gate == 1054)).Sum(e => e.balance);
                if (ballanceMerchant == 0)
                {
                    DatabaseService.Instance.getConnection().Close();
                    return Json(new
                    {
                        error = true,
                        msg = "Bạn chưa chơi game, nên không được rút tiền."
                    });
                }
                DatabaseService.Instance.getConnection().Close();
                //---tien` merchant chuyen lai user
                var ballanceFromMerchant = db.Transactions.Where(e => (e.user_send == 155 && e.user_id == usersID && e.wallet_id == 11 && e.type == "plus" && e.payment_gate == 1055)).Sum(e => e.balance);
                if (ballanceFromMerchant == 0)
                {
                    DatabaseService.Instance.getConnection().Close();
                    return Json(new
                    {
                        error = true,
                        msg = "Bạn chưa chơi game, nên không được rút tiền."
                    });
                }
                DatabaseService.Instance.getConnection().Close();
                //---tien` userToUser
                double balanceUToU = 0;
                try
                {
                    balanceUToU = db.Transactions.Where(e => (e.user_id == usersID && e.user_send != 155 && e.wallet_id == 11 && e.payment_gate == 1058)).Sum(e => e.balance);
                }
                catch (Exception) { }
                //---tinh' toan' chong'r rut' tien`
                var sysConf = (float)ballanceMerchant / (balanceUToU + ballanceNap + ballanceFromMerchant);
                if (sysConf < 0.1)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Bạn chưa đủ điều kiện để rút tiền. Hãy liên hệ với chăm sóc khách hàng để được giải quyết!"
                    });
                }
                ////---
                
                //Kiêm tra thời gian rút gần nhất
                string queryStringCheckRwithdrawal = "SELECT TOP 1 " +
        "id,confirm_time" +
        " FROM Rwithdrawal_User WHERE user_id =" + usersID + " ORDER BY confirm_time DESC";
                reader = serviceBase.query(queryStringCheckRwithdrawal);
                DateTime dateTime = DateTime.Now;
                DateTime time_creat = dateTime;
                int count_r = 0;
                while (reader.Read())
                {
                    count_r++;
                    time_creat = DateTime.Parse(reader["confirm_time"].ToString().Trim());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                long timeMsSinceNow = new DateTimeOffset(dateTime).ToUnixTimeMilliseconds();
                long timeMsSinceOTP = new DateTimeOffset(time_creat).ToUnixTimeMilliseconds();
                long count_min = (timeMsSinceNow - timeMsSinceOTP) / 1000 / 60;
                if (count_r > 0)
                {
                    if (count_min < 5)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Xin vui lòng chờ trong giây lát! (Mỗi giao dịch cách nhau 5 phút. Thời gian còn lại của bạn là: " + (5 - count_min) + " phút)"
                        });
                    }
                }

                //Kiểm tra thời gian nạp
                string queryStringCheck = "SELECT TOP 1 " +
    "id,time_creat" +
    " FROM Transactions WHERE user_id =0 AND user_send = " + usersID + " AND wallet_id = 0 AND type = 'plus' ORDER BY time_creat DESC";
                reader = serviceBase.query(queryStringCheck);
                int count_i = 0;
                while (reader.Read())
                {
                    count_i++;
                    var time_cr = reader["time_creat"].ToString().Trim();
                    time_creat = DateTime.Parse(reader["time_creat"].ToString().Trim());
                }
                DatabaseService.Instance.getConnection().Close();

                long timeMsSinceNowIn = new DateTimeOffset(dateTime).ToUnixTimeMilliseconds();
                long timeMsSinceOTPIn = new DateTimeOffset(time_creat).ToUnixTimeMilliseconds();
                long count_min_in = (timeMsSinceNowIn - timeMsSinceOTPIn) / 1000 / 60;
                if (count_i > 0)
                {
                    if (count_min_in < 5)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Xin vui lòng chờ trong giây lát! (Mỗi giao dịch cách nhau 5 phút. Thời gian còn lại của bạn là: " + (5 - count_min_in) + " phút)"
                        });
                    }
                }
                //Tìm cổng thanh toán phù hợp
                string queryPaymentGate = "SELECT TOP 1 " +
                    "PaymentGate.id as 'id'," +
                    "PaymentGate.sms as 'sms'," +
                    "PaymentGate.name as 'name'," +
                    "CashIORate.cash_in as 'cash_in'," +
                    "CashIORate.cash_out as 'cash_out' " +
                    "FROM PaymentGate INNER JOIN CashIORate ON PaymentGate.id = CashIORate.payment_gate_id WHERE PaymentGate.merchant_id = " + merchant_id + " AND PaymentGate.type = 2 AND CashIORate.cash_in > " + rwithdrawal.value + " ";
                reader = serviceBase.query(queryPaymentGate);
                PaymentGateCashIO paymentGateCashIO = new PaymentGateCashIO();
                while (reader.Read())
                {
                    paymentGateCashIO.id = Int32.Parse(reader["id"].ToString().Trim());
                    paymentGateCashIO.sms = Int32.Parse(reader["sms"].ToString().Trim());
                    paymentGateCashIO.cash_in = Double.Parse(reader["cash_in"].ToString().Trim());
                    paymentGateCashIO.cash_out = Double.Parse(reader["cash_out"].ToString().Trim());
                    paymentGateCashIO.name = reader["name"].ToString().Trim();
                }
                DatabaseService.Instance.getConnection().Close();

                if (paymentGateCashIO.id == 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Merchant không có cổng thanh toán nào phù hợp giao dịch."
                    });
                }
                double plus_money_profit = rwithdrawal.value / paymentGateCashIO.cash_in * paymentGateCashIO.cash_out;
                plus_money_profit = Math.Round(plus_money_profit, 2, MidpointRounding.ToEven);
                rwithdrawal.value_out = plus_money_profit;
                //Kiểm tra xem tiền trong ví có đủ
                double balance = this.getBalace(usersID, rwithdrawal.wallet_id, merchant_id);
                if (balance < rwithdrawal.value)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Số tiền trong ví của bạn không đủ để thực hiện giao dịch này."
                    });
                }

                DateTime time = DateTime.Now;
                // Use current time
                string format = "yyyy-MM-dd HH:mm:ss";
                serviceBase.query("UPDATE Rwithdrawal_User SET user_id = " + usersID + ",approved = 1,paymentgate_id=" + paymentGateCashIO.id + ",value_out=" + plus_money_profit + ",confirm_time='" + time.ToString(format) + "' WHERE id ='" + id.Trim() + "'");
                DatabaseService.Instance.getConnection().Close();

                //Trừ số tiền ví của user vào merchant
                int iD = Int32.Parse(id.Trim());
                bool is_update = this.updateBalaceWalletUser(iD);
                if (!is_update)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Xảy ra sự cố update ví"
                    });
                }

                if (paymentGateCashIO.sms != 1)
                {
                    bool is_update_accounting = updateBalaceWalletUserNoAccounting(iD);
                    return Json(new
                    {
                        error = false
                    });
                }
                else
                {
                    return Json(new
                    {
                        error = false
                    });
                }
            }
        }

        private string render_code()
        {
            using (var db = new GamePaymentContext())
            {
                //CryptoHelper cryptoHelper = new CryptoHelper();
                var code = CryptoHelper.RandomString(6);
                var querycode = db.Rwithdrawal_User.Where(c => (c.code == code && c.user_id == 0)).FirstOrDefault();
                if (querycode != null)
                {
                    return this.render_code();
                }
                return code;
            }
        }

        //---bunbun nap the dienthoai
        [HttpPost]
        public ActionResult confirmDetailMobileCard(int mobileId, string pass)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }

            var auth = new AuthenticationHelper(Session);
            var usersID = auth.ID();
            var merchant_id = Int32.Parse(auth.getMerchantID());

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            SqlCommand command;
            //---xac thuc tai khoan dung pass
            string queryCheckPassword = "SELECT id FROM Users WHERE password = @password";
            command = new SqlCommand(queryCheckPassword, DatabaseService.Instance.getConnection());
            string password = CryptoHelper.MD5Hash(pass, usersID.ToString());
            command.Parameters.AddWithValue("@password", password);
            reader = serviceBase.querySqlParam(command);
            if (!reader.HasRows)
            {
                DatabaseService.Instance.getConnection().Close();
                return Json(new
                {
                    error = true,
                    msg = "Mật khẩu không đúng."
                });
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            CardBuyAlegoResponse cardMobile = new CardBuyAlegoResponse();
            string queryDetail = "SELECT id,card_code,card_serial,card_value,type,isSeen FROM CardBuyAlegoResponse WHERE id = @mobileId";
            command = new SqlCommand(queryDetail, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@mobileId", mobileId);
            reader = serviceBase.querySqlParam(command);
            while (reader.Read())
            {
                cardMobile.id = Int32.Parse(reader["id"].ToString());
                cardMobile.card_code = reader["card_code"].ToString();
                cardMobile.card_serial = reader["card_serial"].ToString();
                cardMobile.card_value = Convert.ToDouble(reader["card_value"].ToString());
                cardMobile.type = reader["type"].ToString();
                cardMobile.isSeen = Convert.ToInt32(reader["isSeen"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            string queryUpdate = "UPDATE CardBuyAlegoResponse SET isSeen=1 WHERE id =@mobileId";
            command = new SqlCommand(queryUpdate, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@mobileId", mobileId);
            reader = serviceBase.querySqlParam(command);
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                error = false,
                item = cardMobile,
                msg = "Chi tiết thẻ."
            });

        }

        private string postMethod(string url, string parsedContent)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);

            var data = Encoding.ASCII.GetBytes(parsedContent);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            return responseString;
        }
        //Query lấy cổng thanh toán
        private PaymentGate getPaymentGate(string paymentgate_code, int merchant_id)
        {
            PaymentGate paymentGateService = new PaymentGate();
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            SqlCommand command;
            string queryCheckPassword = "SELECT TOP 1 id,name,code,wallet_in,wallet_out,status_active,sms FROM PaymentGate WHERE code = @paymentgate_code AND merchant_id = @merchant_id ORDER BY id DESC";
            command = new SqlCommand(queryCheckPassword, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@paymentgate_code", paymentgate_code);
            command.Parameters.AddWithValue("@merchant_id", merchant_id);
            reader = serviceBase.querySqlParam(command);
            int count_app = 0;
            while (reader.Read())
            {
                count_app++;
                paymentGateService.id = Int32.Parse(reader["id"].ToString());
                paymentGateService.name = reader["name"].ToString();
                paymentGateService.code = reader["code"].ToString();
                paymentGateService.wallet_in = Int32.Parse(reader["wallet_in"].ToString());
                paymentGateService.wallet_out = Int32.Parse(reader["wallet_out"].ToString());
                paymentGateService.status_active = reader["status_active"].ToString();
                paymentGateService.sms = Int32.Parse(reader["sms"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return paymentGateService;
        }

        //Kiểm tra tham số trừ tiền có đúng config
        private CashIORate checkValueConfig(PaymentGate paymentGateService, double value)
        {
            CashIORate cashIORate = new CashIORate();
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            SqlCommand command;
            string query = "SELECT TOP 1 id,payment_gate_id,cash_out,cash_in FROM CashIORate WHERE payment_gate_id =@paymentGateService AND cash_in >=@value ORDER BY cash_in ASC ";
            command = new SqlCommand(query, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@paymentGateService", paymentGateService.id);
            command.Parameters.AddWithValue("@value", value);
            reader = serviceBase.querySqlParam(command);
            while (reader.Read())
            {
                cashIORate.payment_gate_id = Int32.Parse(reader["payment_gate_id"].ToString());
                cashIORate.cash_out = Int32.Parse(reader["cash_out"].ToString());
                cashIORate.cash_in = Int32.Parse(reader["cash_in"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return cashIORate;
        }
        [HttpPost]
        public ActionResult callURLmobileCard(string typeCard, string cardValue, string password)
        {
            if (typeCard == null || cardValue == null || password ==  null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }


            using (var db = new GamePaymentContext())
            {
                var auth = new AuthenticationHelper(Session);
                var usersID = auth.ID();
                var merchant_id = Int32.Parse(auth.getMerchantID());

                ServiceBase serviceBase = new ServiceBase();
                SqlDataReader reader;
                SqlCommand command;
                string password_md5 = CryptoHelper.MD5Hash(password, usersID.ToString());
                //---xac thuc tai khoan dung pass
                string queryCheckPassword = "SELECT id FROM Users WHERE id =@usersID AND password = @password_md5";
                //reader = serviceBase.query(queryCheckPassword);
                command = new SqlCommand(queryCheckPassword, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@usersID", usersID);
                command.Parameters.AddWithValue("@password_md5", password_md5);
                reader = serviceBase.querySqlParam(command);
                if (!reader.HasRows)
                {
                    reader.Close();
                    DatabaseService.Instance.getConnection().Close();
                    return Json(new
                    {
                        error = true,
                        msg = "Mật khẩu không đúng."
                    });
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();
                
                ////---chống rút tiền
                //---tien`nap
                double ballanceNap = 0;

                try
                {
                    ballanceNap = db.Transactions.Where(e => (e.user_id == usersID && e.wallet_id == 11 && e.type == "plus" && (e.payment_gate == 2066 || e.tag == "Agency+"))).Sum(e => e.balance);
                }
                catch (Exception) { }
                if (ballanceNap == 0)
                {
                    DatabaseService.Instance.getConnection().Close();
                    return Json(new
                    {
                        error = true,
                        msg = "Bạn chưa nạp tiền, nên không được rút tiền."
                    });
                }
                DatabaseService.Instance.getConnection().Close();

                //---tien` rut'
                double ballanceRut = 0;
                try
                {
                    ballanceRut = db.Transactions.Where(e => (e.user_id == usersID && e.wallet_id == 11 && e.type == "minus" && (e.payment_gate == 2061 || e.payment_gate == 2072 || e.payment_gate == 2082))).Sum(e => e.balance);
                }
                catch (Exception) { }
                //---tien` cho merchant
                var ballanceMerchant = db.Transactions.Where(e => (e.user_send == usersID && e.user_id == 155 && e.wallet_id == 11 && e.type == "minus" && e.payment_gate == 1054)).Sum(e => e.balance);
                if (ballanceMerchant == 0)
                {
                    DatabaseService.Instance.getConnection().Close();
                    return Json(new
                    {
                        error = true,
                        msg = "Bạn chưa chơi game, nên không được rút tiền."
                    });
                }
                DatabaseService.Instance.getConnection().Close();
                //---tien` merchant chuyen lai user
                var ballanceFromMerchant = db.Transactions.Where(e => (e.user_send == 155 && e.user_id == usersID && e.wallet_id == 11 && e.type == "plus" && e.payment_gate == 1055)).Sum(e => e.balance);
                if (ballanceFromMerchant == 0)
                {
                    DatabaseService.Instance.getConnection().Close();
                    return Json(new
                    {
                        error = true,
                        msg = "Bạn chưa chơi game, nên không được rút tiền."
                    });
                }
                DatabaseService.Instance.getConnection().Close();
                //---tien` userToUser
                double balanceUToU = 0;
                try
                {
                    balanceUToU = db.Transactions.Where(e => (e.user_id == usersID && e.user_send != 155 && e.wallet_id == 11 && e.payment_gate == 1058)).Sum(e => e.balance);
                }
                catch (Exception) { }
                //---tinh' toan' chong'r rut' tien`
                var sysConf = (float)ballanceMerchant / (balanceUToU + ballanceNap + ballanceFromMerchant);
                if (sysConf < 0.1)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Bạn chưa đủ điều kiện để rút tiền. Hãy liên hệ với chăm sóc khách hàng để được giải quyết!"
                    });
                }
                ////---
                
                //---kiểm tra tiền còn đủ ko
                var intCardVal = Convert.ToDouble(cardValue);
                long ubalance = 0;
                string queryUserBalane = "SELECT TOP 1 balance FROM Transactions WHERE user_id =@usersID ORDER BY id DESC";
                command = new SqlCommand(queryUserBalane, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@usersID", usersID);
                reader = serviceBase.querySqlParam(command);
                if (!reader.HasRows)
                {
                    reader.Close();
                    DatabaseService.Instance.getConnection().Close();
                    return Json(new
                    {
                        error = true,
                        msg = "Bạn chưa có tiền, nên không được rút tiền."
                    });
                }
                while (reader.Read())
                {
                    ubalance = (long)Convert.ToDouble(reader["balance"].ToString().Trim());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();
                PaymentGate paymentGateService = this.getPaymentGate("THECAO", merchant_id);
                if (paymentGateService.id == 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Cổng thanh toán không tồn tại."
                    }, JsonRequestBehavior.AllowGet);

                }


                if (!paymentGateService.status_active.Trim().Equals("active"))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Cổng thanh toán đang bị khóa ."
                    }, JsonRequestBehavior.AllowGet);

                }

                //Kiểm tra tham số trừ tiền có đúng config
                CashIORate cashIORate;
                try
                {
                    cashIORate = this.checkValueConfig(paymentGateService, intCardVal);
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Kiểm tra config lỗi: " + ex.Message
                    }, JsonRequestBehavior.AllowGet);

                }

                if (cashIORate.cash_in == 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Số tiền vượt quá config được chuyển "
                    }, JsonRequestBehavior.AllowGet);

                }

                if (ubalance < (intCardVal / cashIORate.cash_in * cashIORate.cash_out))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Bạn không đủ coin để rút được tiền mệnh giá này."
                    });
                }
                //Kiêm tra thời gian rút gần nhất
                string queryStringCheckRwithdrawal = "SELECT TOP 1 " +
        "id,confirm_time" +
        " FROM CardBuyAlegoResponse WHERE user_id =@usersID ORDER BY confirm_time DESC";
                command = new SqlCommand(queryStringCheckRwithdrawal, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@usersID", usersID);
                reader = serviceBase.querySqlParam(command);
                DateTime dateTime = DateTime.Now;
                DateTime time_creat = dateTime;
                int count_r = 0;
                while (reader.Read())
                {
                    count_r++;
                    time_creat = DateTime.Parse(reader["confirm_time"].ToString().Trim());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                long timeMsSinceNow = new DateTimeOffset(dateTime).ToUnixTimeMilliseconds();
                long timeMsSinceOTP = new DateTimeOffset(time_creat).ToUnixTimeMilliseconds();
                long count_min = (timeMsSinceNow - timeMsSinceOTP) / 1000 / 60;
                if (count_r > 0)
                {
                    if (count_min < 5)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Xin vui lòng chờ trong giây lát!(Mỗi giao dịch cách nhau 5 phút.Thời gian còn lại của bạn là: " + (5-count_min) + " phút)"
                        });
                    }
                }


                //Kiểm tra thời gian nạp
                string queryStringCheck = "SELECT TOP 1 " +
    "id,time_creat" +
    " FROM Transactions WHERE user_id =0 AND user_send =@usersID AND wallet_id = 0 AND type = 'plus' ORDER BY time_creat DESC";
                command = new SqlCommand(queryStringCheck, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@usersID", usersID);
                reader = serviceBase.querySqlParam(command);
                int count_i = 0;
                while (reader.Read())
                {
                    count_i++;
                    var time_cr = reader["time_creat"].ToString().Trim();
                    time_creat = DateTime.Parse(reader["time_creat"].ToString().Trim());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                long timeMsSinceNowIn = new DateTimeOffset(dateTime).ToUnixTimeMilliseconds();
                long timeMsSinceOTPIn = new DateTimeOffset(time_creat).ToUnixTimeMilliseconds();
                long count_min_in = (timeMsSinceNowIn - timeMsSinceOTPIn) / 1000 / 60;
                if (count_i > 0)
                {
                    if (count_min_in < 5)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Xin vui lòng chờ trong giây lát!(Mỗi giao dịch cách nhau 5 phút.Thời gian còn lại của bạn là: " +(5 - count_min_in) + " phút)"
                        });
                    }
                }
                //---gọi API sang ben Alego
                int responseStatus;
                CardBuy response = BillingTelcoCardManager.GetOneCardOnline((ENSPType)Convert.ToInt32(typeCard), Convert.ToInt32(cardValue), out responseStatus);

                //---lay the thanh cong
                if (responseStatus > 0)
                {
                    double plus_money_profit = response.CardValue / cashIORate.cash_in * cashIORate.cash_out;
                    plus_money_profit = Math.Round(plus_money_profit, 2, MidpointRounding.ToEven);

                    //Kiểm tra xem tiền trong ví có đủ
                    double balance = this.getBalace(usersID, 11, merchant_id);
                    if (balance < plus_money_profit)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Số tiền trong ví của bạn không đủ để thực hiện giao dịch này."
                        });
                    }

                    //Trừ số tiền ví của user vào merchant
                    var query = db.Transactions.OrderByDescending(p => p.id).Where(e => (e.user_id == usersID && e.wallet_id == 11)).FirstOrDefault();
                    double userMoney = 0;
                    if (query != null)
                    {
                        userMoney = query.balance;
                    }

                    double moneyuserMinus = userMoney - plus_money_profit;
                    //Trừ vào ví user
                    db.Transactions.Add(new Models.Transactions()
                    {
                        user_id = usersID,
                        merchant_id = 0,
                        payment_gate = paymentGateService.id,
                        wallet_id = 11,
                        balance = moneyuserMinus,
                        type = "minus",
                        value = intCardVal,
                        transactions_type = 0,
                        user_send = 0,
                        tag = "ruttien",
                        message = "Bán coin cho agency",
                        time_creat = DateTime.Now,
                        value_out = plus_money_profit
                    });
                    db.SaveChanges();
                    //Cộng tiền merchant tạm giữ
                    var merchant_item = db.Merchant.FirstOrDefault(m => m.id == merchant_id);
                    var user_merchant = db.User.FirstOrDefault(u => u.username == merchant_item.username);
                    var query_merchant = db.Transactions
    .OrderByDescending(p => p.id)
    .Where(e => (e.user_id == user_merchant.id && e.transactions_type == 0 && e.wallet_id == 0))
    .FirstOrDefault();
                    double userMoney_merchant = 0;
                    if (query_merchant != null)
                    {
                        userMoney_merchant = query_merchant.balance;
                    }
                    userMoney_merchant = userMoney_merchant + plus_money_profit;
                    db.Transactions.Add(new Models.Transactions()
                    {
                        user_id = user_merchant.id,
                        merchant_id = 0,
                        payment_gate = paymentGateService.id,
                        wallet_id = 0,
                        balance = userMoney_merchant,
                        type = "plus",
                        value = intCardVal,
                        transactions_type = 0,
                        user_send = usersID,
                        tag = "ruttien",
                        message = "Tạm giữ coin bán cho agency ",
                        time_creat = DateTime.Now,
                        value_out = plus_money_profit
                    });
                    db.SaveChanges();

                    //---luu the
                    var strTypeCard = "";
                    var intTypeCard = (int)response.CardType;
                    switch (intTypeCard)
                    {
                        case 1:
                            strTypeCard = "Viettel";
                            break;
                        case 2:
                            strTypeCard = "Vinaphone";
                            break;
                        case 3:
                            strTypeCard = "Mobifone";
                            break;
                    }
                    DateTime time = DateTime.Now;
                    db.CardBuyAlegoResponse.Add(new CardBuyAlegoResponse()
                    {
                        card_code = response.CardCode,
                        card_serial = response.CardSerial,
                        card_value = (int)response.CardValue,
                        type = strTypeCard,
                        time_creat = time,
                        status = 0,
                        user_id = usersID,
                        value_out = (float)plus_money_profit,
                        approved_time = time,
                        confirm_time = time,
                        isSeen=0
                    });
                    db.SaveChanges();

                    return Json(new
                    {
                        error = false,
                        msg = "Bạn đã nhận được thẻ cào, vào mục thẻ cào để lấy thẻ!"
                    });
                }
                return Json(new
                {
                    error = true,
                    msg = "Chưa nhận được thẻ!"
                });
            }
        }
        //---


        [HttpPost]
        public ActionResult getTransactions(string code, string agencyid)
        {
            if (code == null || agencyid == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var auth = new AuthenticationHelper(Session);
            var usersID = auth.ID();
            var merchant_id = Int32.Parse(auth.getMerchantID());

            ServiceBase serviceBase = new ServiceBase();

            string queryString = "SELECT TOP 1 " +
                "id," +
                 "code," +
                "create_time," +
                "agency_id," +
                "value," +
                "wallet_id," +
                "(SELECT TOP 1 name FROM Wallet WHERE id = Rwithdrawal_User.wallet_id ) as 'wallet_name'," +
                "(SELECT TOP 1 title FROM Agency WHERE id = Rwithdrawal_User.agency_id ) as 'agency_name'" +
                " FROM Rwithdrawal_User WHERE code =@code AND user_id = 0 AND agency_id = @agencyid ";
            SqlDataReader reader;
            SqlCommand command;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@code", code);
            command.Parameters.AddWithValue("@agencyid", agencyid);
            reader = serviceBase.querySqlParam(command);

            Rwithdrawal_UserResponse rwithdrawal = new Rwithdrawal_UserResponse();
            int count = 0;
            while (reader.Read())
            {
                count++;
                rwithdrawal.id = Int32.Parse(reader["id"].ToString().Trim());
                rwithdrawal.create_time = reader["create_time"].ToString().Trim();
                rwithdrawal.wallet_id = Int32.Parse(reader["wallet_id"].ToString().Trim());
                rwithdrawal.wallet_name = reader["wallet_name"].ToString().Trim();
                rwithdrawal.code = reader["code"].ToString().Trim();
                rwithdrawal.agency_id = Int32.Parse(reader["agency_id"].ToString().Trim());
                rwithdrawal.agency_name = reader["agency_name"].ToString().Trim();
                rwithdrawal.value = Double.Parse(reader["value"].ToString().Trim());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            if (count == 0)
            {
                return Json(new
                {
                    error = true,
                    msg = "Mã code không đúng"
                });
            }
            //responseQuery = this.query("UPDATE Rwithdrawal_User SET user_id = "+ user.id + ",approved = 2 WHERE code ='" + code.Trim() + "'");
            //Tìm cổng thanh toán phù hợp
            string queryPaymentGate = "SELECT TOP 1 " +
                "PaymentGate.id as 'id'," +
                "PaymentGate.name as 'name'," +
                "CashIORate.cash_in as 'cash_in'," +
                "CashIORate.cash_out as 'cash_out' " +
                "FROM PaymentGate INNER JOIN CashIORate ON PaymentGate.id = CashIORate.payment_gate_id WHERE PaymentGate.merchant_id = " + merchant_id + " AND PaymentGate.type = 2 AND CashIORate.cash_in > " + rwithdrawal.value + " ";
            reader = serviceBase.query(queryPaymentGate);
            PaymentGateCashIO paymentGateCashIO = new PaymentGateCashIO();
            while (reader.Read())
            {
                paymentGateCashIO.id = Int32.Parse(reader["id"].ToString().Trim());
                paymentGateCashIO.cash_in = Double.Parse(reader["cash_in"].ToString().Trim());
                paymentGateCashIO.cash_out = Double.Parse(reader["cash_out"].ToString().Trim());
                paymentGateCashIO.name = reader["name"].ToString().Trim();
            }
            DatabaseService.Instance.getConnection().Close();

            if (paymentGateCashIO.id == 0)
            {
                return Json(new
                {
                    error = true,
                    msg = "Merchant không có cổng thanh toán nào phù hợp giao dịch."
                });
            }
            double plus_money_profit = rwithdrawal.value / paymentGateCashIO.cash_in * paymentGateCashIO.cash_out;
            plus_money_profit = Math.Round(plus_money_profit, 2, MidpointRounding.ToEven);
            rwithdrawal.value_out = plus_money_profit;
            rwithdrawal.paymentgate_name = paymentGateCashIO.name;
            return Json(new
            {
                error = false,
                item = rwithdrawal
            });
        }
        [HttpPost]
        public ActionResult listdata(string filter, string search_note, int iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength)
        {
            if (filter == null || search_note == null || iSortCol_0 == null || sSortDir_0 == null || iDisplayStart == null || iDisplayLength == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var auth = new AuthenticationHelper(Session);

            var userID = auth.ID();
            string[] a_sort = new string[] { "id", "agency_id", "wallet_id", "value", "approved", "create_time", "approved_time", "code", "username", "note", "id" };
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT" +
                " id" +
                " ,paymentgate_id" +
                " ,approved" +
                " ,create_time" +
                " ,(SELECT TOP 1 name FROM Wallet WHERE id = Rwithdrawal_User.wallet_id ) as 'wallet_id'" +
                " ,(SELECT TOP 1 title FROM Agency WHERE id = Rwithdrawal_User.agency_id) as 'agency_id'" +
                " ,value" +
                " ,value_out" +
                " ,approved_time" +
                " ,note" +
                " ,code" +
                " ,(SELECT TOP 1 username FROM Users WHERE id = Rwithdrawal_User.user_id ) as 'username'" +
                " FROM Rwithdrawal_User WHERE user_id =@userID ";

            string queryCount = "SELECT COUNT(id) as 'count' FROM Rwithdrawal_User WHERE user_id = " + userID + " ";
            if (!filter.Trim().Equals("all"))
            {
                queryString += " AND approved =@filter ";
                queryCount += " AND approved =@filter ";
            }
            if (!search_note.Equals(""))
            {
                queryString += " AND note LIKE Concat('%',@search_note,'%') ";
                queryCount += " AND note LIKE Concat('%',@search_note,'%') ";
            }


            if (iSortCol_0 > 10)
            {
                iSortCol_0 = 0;
            }

            if (sSortDir_0.Equals("desc"))
            {
                sSortDir_0 = "DESC";
            }
            else
            {
                sSortDir_0 = "ASC";
            }
            queryString += " ORDER BY " + a_sort[iSortCol_0] + " " + sSortDir_0 + " OFFSET @iDisplayStart ROWS FETCH NEXT @iDisplayLength ROWS ONLY";
            // SqlDataReader reader = serviceBase.query(queryString);
            SqlCommand command;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@userID", userID);
            command.Parameters.AddWithValue("@filter", filter);
            command.Parameters.AddWithValue("@search_note", search_note);
            command.Parameters.AddWithValue("@iDisplayStart", Int32Ext.toInt(iDisplayStart));
            command.Parameters.AddWithValue("@iDisplayLength", Int32Ext.toInt(iDisplayLength));
            SqlDataReader reader = serviceBase.querySqlParam(command);

            List<String[]> users = new List<String[]>();
            while (reader.Read())
            {
                string[] s_user = new string[10];
                s_user[0] = reader["id"].ToString().Trim();
                s_user[1] = reader["agency_id"].ToString().Trim();
                s_user[2] = reader["wallet_id"].ToString().Trim();
                s_user[3] = Double.Parse(reader["value"].ToString().Trim()).ToString("N0", CultureInfo.InvariantCulture);
                switch (reader["approved"].ToString().Trim())
                {
                    case "0":
                        s_user[4] = "<span class=\"label label-info\">Chờ user xác nhận</span>";
                        break;
                    case "1":
                        s_user[4] = "<span class=\"label label-warning\">Chờ kế toán xác nhận</span>";
                        break;
                    case "2":
                        s_user[4] = "<span class=\"label label-danger\">Đã hủy</span>";
                        break;
                    case "3":
                        s_user[4] = "<span class=\"label label-success\">Đã hoàn thành</span>";
                        break;
                }
                s_user[5] = reader["create_time"].ToString().Trim();
                s_user[6] = reader["approved_time"].ToString().Trim();
                s_user[7] = "<b>" + reader["code"].ToString().Trim() + "</b>";
                s_user[8] = reader["username"].ToString().Trim();
                s_user[9] = reader["note"].ToString().Trim();

                users.Add(s_user);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            int row = 0;
            command = new SqlCommand(queryCount, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@userID", userID);
            command.Parameters.AddWithValue("@filter", filter);
            command.Parameters.AddWithValue("@search_note", search_note);
             reader = serviceBase.querySqlParam(command);
            while (reader.Read())
            {
                row = Int32.Parse(reader["count"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength
            });
        }

        [HttpPost]
        public ActionResult listdataAgencyy()
        {
            string[] a_sort = new string[] { "id", "title", "phone", "link", "location", "id" };
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT" +
                " id" +
                " ,title" +
                " ,status_active" +
                " ,phone" +
                " ,location" +
                " ,(SELECT link FROM Agency_social WHERE subid = Agency.id ) as 'link'" +
                " FROM Agency WHERE subid = 0 AND status_active='active' AND is_appear=1 ";

            string queryCount = "SELECT COUNT(id) as 'count' FROM Agency WHERE subid = 0";

            queryString += " ORDER BY orderId ASC OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY";
            SqlDataReader reader = serviceBase.query(queryString);
            
            List<String[]> users = new List<String[]>();
            while (reader.Read())
            {
                string[] s_user = new string[10];
                s_user[0] = reader["id"].ToString().Trim();
                s_user[1] = reader["title"].ToString().Trim();
                s_user[2] = reader["phone"].ToString().Trim();
                var link = reader["link"].ToString();
                s_user[3] = "<button href=\"" + link + "\" type=\"button\" class=\"btn btn-success btn_facebook\"><i class=\"fa fa-facebook - f \" aria-hidden=\"true\"></i></button>";
                s_user[4] = reader["location"].ToString().Trim();
                s_user[5] = "<button onclick=\"openPopup(" + Int32Ext.toInt(reader["id"].ToString()) + ")\" type=\"button\" class=\"btn btn-success \"><i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>GIAO DỊCH</button>";

                users.Add(s_user);
            }
            DatabaseService.Instance.getConnection().Close();

            int row = 0;
            reader = serviceBase.query(queryCount);
            while (reader.Read())
            {
                row = Int32.Parse(reader["count"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = 10
            });
        }

        [HttpPost]
        public ActionResult listdataMobilecard(String s_seach, String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength)
        {
            var auth = new AuthenticationHelper(Session);

            var userID = auth.ID();
            string[] a_sort = new string[] { "id", "type", "card_code", "card_serial", "card_value", "confirm_time", "isSeen", "id" };
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT" +
                " id" +
                " ,card_code" +
                " ,card_serial" +
                " ,card_value" +
                " ,type,confirm_time,isSeen" +
                " FROM CardBuyAlegoResponse WHERE user_id = @userID";

            string queryCount = "SELECT COUNT(id) as 'count' FROM CardBuyAlegoResponse WHERE user_id =@userID";
            if (sSortDir_0.Equals("desc"))
            {
                sSortDir_0 = "DESC";
            }
            else
            {
                sSortDir_0 = "ASC";
            }
            queryString += " ORDER BY "+ a_sort[Int32Ext.toInt(iSortCol_0)] + " "+ sSortDir_0 + " OFFSET @iDisplayStart ROWS FETCH NEXT @iDisplayLength ROWS ONLY";
            // SqlDataReader reader = serviceBase.query(queryString);
            SqlCommand command;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@userID", userID);
            command.Parameters.AddWithValue("@iDisplayStart",Int32Ext.toInt(iDisplayStart));
            command.Parameters.AddWithValue("@iDisplayLength", Int32Ext.toInt(iDisplayLength));
            SqlDataReader reader = serviceBase.querySqlParam(command);

            List<String[]> users = new List<String[]>();
            while (reader.Read())
            {
                string[] s_user = new string[8];
                s_user[0] = reader["id"].ToString().Trim();
                s_user[1] = reader["type"].ToString().Trim();
                s_user[2] = reader["card_code"].ToString().Trim();
                s_user[3] = reader["card_serial"].ToString();
                s_user[4] = reader["card_value"].ToString().Trim();
                s_user[5] = reader["confirm_time"].ToString().Trim();
                s_user[6] = Convert.ToInt32(reader["isSeen"].ToString().Trim())==1? "<i class=\"fa fa-eye\"></i>" : "";
                s_user[7] = "<button type=\"button\" onclick=\"showPass(" + Int32Ext.toInt(reader["id"].ToString()) + ")\" class=\"btn btn-primary\">Thao tác</button>";

                users.Add(s_user);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            int row = 0;
            command = new SqlCommand(queryCount, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@userID", userID);
            reader = serviceBase.querySqlParam(command);
            while (reader.Read())
            {
                row = Int32.Parse(reader["count"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength
            });
        }

        [HttpPost]
        public ActionResult listdataCode(int iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength)
        {
            if (iSortCol_0 == null || sSortDir_0 == null || iDisplayStart == null || iDisplayLength == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            using (var db = new GamePaymentContext())
            {


                var auth = new AuthenticationHelper(Session);

                var userID = auth.ID();

                var userMD = db.User.FirstOrDefault(e => e.id == userID);

                var list_agency = from a in db.Agency
                                  join a_m in db.Agency_Merchant on a.id equals a_m.agency_id
                                  where a_m.merchant_id == userMD.merchant_id && a.status_active == "active"
                                  select new
                                  {
                                      id = a.id
                                  };





                int count = list_agency.ToList().Count();
                List<String> s = new List<string>();
                int i_index = 0;
                list_agency.ToList().ForEach(ent =>
                {
                    // s[i_index] = ent.id.ToString();
                    s.Add(ent.id.ToString());
                    i_index++;
                    var list_agency_child = from a in db.Agency
                                            where a.subid == ent.id
                                            select new
                                            {
                                                id = a.id
                                            };
                    if (list_agency_child != null)
                    {
                        list_agency_child.ToList().ForEach(entchild =>
                        {
                            s.Add(entchild.id.ToString());
                            i_index++;
                        });
                    }
                });
                string s_agency_query = "0";
                if (count > 0)
                {
                    s_agency_query = String.Join(",", s);
                }

                string[] a_sort = new string[] { "id", "agency_id", "wallet_id", "value", "create_time", "code", "note", "id" };
                ServiceBase serviceBase = new ServiceBase();
                string queryString = "SELECT" +
                    " id" +
                    " ,paymentgate_id" +
                    " ,create_time" +
                    " ,(SELECT TOP 1 name FROM Wallet WHERE id = Rwithdrawal_User.wallet_id ) as 'wallet_id'" +
                    " ,(SELECT TOP 1 title FROM Agency WHERE id = Rwithdrawal_User.agency_id) as 'agency_id'" +
                    " ,value" +
                    " ,value_out" +
                    " ,note" +
                    " ,code" +
                    " FROM Rwithdrawal_User WHERE user_id = 0 AND agency_id IN("+ s_agency_query + ") ";

                string queryCount = "SELECT COUNT(id) as 'count' FROM Rwithdrawal_User WHERE user_id = 0 AND agency_id IN("+ s_agency_query + ") ";

                if (sSortDir_0.Equals("desc"))
                {
                    sSortDir_0 = "DESC";
                }
                else
                {
                    sSortDir_0 = "ASC";
                }
                if (iSortCol_0 > 7)
                {
                    iSortCol_0 = 0;
                }
                queryString += " ORDER BY " + a_sort[iSortCol_0] + " " + sSortDir_0 + " OFFSET @iDisplayStart ROWS FETCH NEXT @iDisplayLength ROWS ONLY";
                // SqlDataReader reader = serviceBase.query(queryString);
                SqlCommand command;
                command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@iDisplayStart", Int32Ext.toInt(iDisplayStart));
                command.Parameters.AddWithValue("@iDisplayLength", Int32Ext.toInt(iDisplayLength));
                SqlDataReader reader = serviceBase.querySqlParam(command);

                List<String[]> users = new List<String[]>();
                while (reader.Read())
                {
                    string[] s_user = new string[7];
                    s_user[0] = reader["id"].ToString().Trim();
                    s_user[1] = reader["agency_id"].ToString().Trim();
                    s_user[2] = reader["wallet_id"].ToString().Trim();
                    s_user[3] = Double.Parse(reader["value"].ToString().Trim()).ToString("N0", CultureInfo.InvariantCulture);

                    s_user[4] = reader["create_time"].ToString().Trim();
                    s_user[5] = "<b>" + reader["code"].ToString().Trim() + "</b>";
                    s_user[6] = reader["note"].ToString().Trim();
                    users.Add(s_user);
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                int row = 0;
                command = new SqlCommand(queryCount, DatabaseService.Instance.getConnection());
                reader = serviceBase.querySqlParam(command);
                while (reader.Read())
                {
                    row = Int32.Parse(reader["count"].ToString());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                return Json(new
                {
                    aaData = users,
                    iTotalRecords = row,
                    iTotalDisplayRecords = row,
                    display = iDisplayLength
                });

            }
        }
    }
}