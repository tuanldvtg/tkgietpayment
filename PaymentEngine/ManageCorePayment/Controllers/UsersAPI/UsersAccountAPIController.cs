﻿using ManageCorePayment.DAL;
using ManageCorePayment.Exceptions;
using ManageCorePayment.Helper;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers.AgencyAPI
{ 
    public class UsersAccountAPIController : Controller
    {

        /**
         * Return all agency wallets
         **/
        [HttpPost]
        public ActionResult Wallets()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var walletResults = new[] { new {
                    id = 0,
                    name = "",
                    balance = "0.00",
                    history = new[]
                    {
                        new
                        {
                             date = "",
                            value = 0.1
                        }
                    }.ToList()
                }}.ToList();
            walletResults.Clear(); 

            try
            {
                var auth = new AuthenticationHelper(Session);
                var usersID = auth.ID();
                var merchant_id = Int32.Parse(auth.getMerchantID());

                // Get all managet wallets
                using (var db = new GamePaymentContext())
                {

                    var wallets = from w in db.Wallet
                                  join jw in db.Merchant_Wallet on w.id equals jw.wallet_id
                                  where jw.merchant_id == merchant_id orderby w.id ascending
                                  select w;
                    wallets.ToList().ForEach(w =>
                    {
                        var historyStruct = new[] { new {
                            date = "",
                            value = 0.1
                        }}.ToList();
                        historyStruct.Clear();



                        string datenow = DateTime.Now.ToString("yyyy-MM-dd") + " 23:59:59.000";
                        DateTime dt = DateTime.Parse(datenow);
                        List<DateTime> list_date = new List<DateTime>();
                        //list_date.Add(dt.Date.ToShortDateString());
                        list_date.Add(dt);
                        for (int i = 0; i < 6; i++)
                        {
                            dt = dt.AddDays(-1);
                            list_date.Add(dt);
                        }

                        foreach (DateTime element in list_date)
                        {

                            var item_tran = db.Transactions
                                                    .OrderByDescending(p => p.id)
                                                    .Where(e => (e.user_id == usersID && e.transactions_type == 0 && e.wallet_id == w.id && e.time_creat <= element))
                                                    .FirstOrDefault();
                            double nowbalance = 0;
                            if (item_tran != null)
                            {
                                 nowbalance = item_tran.balance;
                            }
                            historyStruct.Add(new
                            {
                                date = element.ToString("d/M"),
                                value = nowbalance
                            });
                        }
                        
                        //var balanceRecord = db.Transactions
                        //                        .OrderByDescending(p => p.id)
                        //                        .Where(e => (e.user_id == usersID && e.merchant_id == 0 && e.wallet_id == w.id && e.time_creat < ))
                        //                        .Take(10);

                        //double nowbalance = 0;

                        //if (balanceRecord.Count() > 0)
                        //{
                        //    var firsrec = balanceRecord.First();
                        //    nowbalance = firsrec.balance;
                        //}

                        //balanceRecord.ToList().ForEach(ent =>
                        //{
                        //    historyStruct.Add(new
                        //    {
                        //        date = ent.time_creat.ToString("d/M"),
                        //        value = ent.balance
                        //    });
                        //});

                        /*
                        var history = db.Transactions.OrderByDescending(p => p.id).

                        if (balanceRecord != null)
                        {
                            nowbalance = balanceRecord.balance;
                        } */
                        double total_nowbalance = 0;
                        var item_tran_total = db.Transactions
                                     .OrderByDescending(p => p.id)
                                     .Where(e => (e.user_id == usersID && e.transactions_type == 0 && e.wallet_id == w.id))
                                     .FirstOrDefault();
                        if (item_tran_total != null)
                        {
                            total_nowbalance = item_tran_total.balance;
                        }
                        walletResults.Add(new
                        {
                            id = w.id,
                            name = w.name,
                            balance = Double.Parse(total_nowbalance.ToString()).ToString("N", CultureInfo.InvariantCulture),
                            history = historyStruct
                        });

                    });
                }

                return Json(new
                {
                    error = false,
                    items = walletResults
                });
            }
            catch (AccountNotfoundException )
            {
                return Json(new
                {
                    error = true,
                    msg = "Mật khẩu của bạn không đúng, vui lòng thử lại!"
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }

        [HttpPost]
        public ActionResult selectMerchant(string id)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            AuthenticationHelper authenticationHelper = new AuthenticationHelper(Session);
            string username = authenticationHelper.getTitle();
            int idMerchant = Int32.Parse(id);
            using (var db = new GamePaymentContext())
            {
                var wi = db.User.FirstOrDefault(u => (u.username == username && u.merchant_id == idMerchant));
                authenticationHelper.assignUser(wi);
            }
            return Json(new
            {
                error = false
            });
        }

        [HttpPost]
        public ActionResult ChangePassword(string oldpass, string password)
        {
            if (oldpass == null || password == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            try
            {
                // Check auth type
                var auth = new AuthenticationHelper(Session);
                if (!auth.isLogined())
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Hết hạn phiên đăng nhập !"
                    });
                }
                if (auth.getRole() == SessionKeyMode.Users)
                {
                    (new AgencyPrivateServices()).changePasswordUser(auth.ID(), oldpass, password);
                    return Json(new
                    {
                        error = false,
                        msg = "Thay đổi mật khẩu hoàn tất !"
                    });
                }


                return Json(new
                {
                    error = true,
                    msg = "Unknow role"
                });

            }
            catch (AccountNotfoundException )
            {
                return Json(new
                {
                    error = true,
                    msg = "Mật khẩu của bạn không đúng, vui lòng thử lại!"
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }
    }
}
