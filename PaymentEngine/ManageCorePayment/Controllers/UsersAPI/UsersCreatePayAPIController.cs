﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.Models.PaymentVT;
using ManageCorePayment.PrivateServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ManageCorePayment.Controllers.AgencyAPI
{
    public class UsersCreatePayAPIController : Controller
    {
        int app_id;
        private string _postURLLogin(string url, string parsedContent)
        {
            ServicePointManager.ServerCertificateValidationCallback =
    delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
        return true;
    };
            var baseAddress = url;

            var http = (HttpWebRequest)WebRequest.Create(new Uri(baseAddress));
            http.Accept = "application/json";
            //http.ContentType = "application/json";
            http.ContentType = "application/x-www-form-urlencoded";

            http.Method = "GET";
            ASCIIEncoding encoding = new ASCIIEncoding();
            Byte[] bytes = encoding.GetBytes(parsedContent);

            Stream newStream = http.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();

            var response = http.GetResponse();

            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();
            return content;
        }
        
        private string postMethod(string url, string parsedContent)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);

            var postData = "thing1=hello";
            postData += "&thing2=world";
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            return responseString;
        }

        private string getMethod(string url, string parsedContent)
        {
            using (var client = new WebClient())
            {
                var responseString = client.DownloadString(url + "?data=" + parsedContent);
                return responseString;
            }
        }

        private string get_accessToken()
        {
            string client_id = "487719171387-isfcshdgpfbhsurd0dbp5lbcn8rrj2qc.apps.googleusercontent.com";
            string client_secret = "LJpLH0e-XzagtEdCQnGk9xfT";
            string refresh_token = "1/JVGsbd3YUnbqLbQK-v9NwVEc_fCSLTRmbo4U8PsdZGg";

            var request = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");

            string postData = string.Format("grant_type=refresh_token&client_id={0}&client_secret={1}&refresh_token={2}", client_id, client_secret, refresh_token);

            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;


            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            Accesstoken accesstoken = new Accesstoken();
            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                accesstoken = JsonConvert.DeserializeObject<Accesstoken>(responseString);
            }
            catch (Exception)
            {
                return "";
            }

            return accesstoken.access_token;

        }
        //Query lấy cổng thanh toán
        private PaymentGate getPaymentGate(string paymentgate_code, GamePaymentContext db)
        {

            var paymentGateService = db.PaymentGate.FirstOrDefault(e => e.code == paymentgate_code);
            return paymentGateService;


        }
        //Kiểm tra tham số trừ tiền có đúng config
        private CashIORate checkValueConfig(PaymentGate paymentGateService, double value)
        {
            CashIORate cashIORate = new CashIORate();
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            string query = "SELECT TOP 1 payment_gate_id,cash_out,cash_in FROM CashIORate WHERE payment_gate_id =@payment_gate_id AND cash_in >= @value ORDER BY cash_in ASC ";
            SqlCommand command = new SqlCommand(query, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@payment_gate_id", paymentGateService.id);
            command.Parameters.AddWithValue("@value", value);
            reader = serviceBase.querySqlParam(command);
            while (reader.Read())
            {
                cashIORate.payment_gate_id = Int32.Parse(reader["payment_gate_id"].ToString());
                cashIORate.cash_out = Int32.Parse(reader["cash_out"].ToString());
                cashIORate.cash_in = Int32.Parse(reader["cash_in"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return cashIORate;
        }



        [HttpPost]
        public ActionResult insertGiftcode(string code)
        {
            if (code == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Bạn chưa nhập giftcode"
                });
            }
            using (var db = new GamePaymentContext())
            {
                int userID = 0;
                try
                {
                    userID = (new AuthenticationHelper(Session).ID());
                }
                catch (Exception)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Vui lòng đăng nhập lại"
                    });
                }
                if (code.Equals(""))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Bạn chưa nhập giftcode"
                    });
                }

                var userDB = db.User.FirstOrDefault(e => e.id == userID);
                this.app_id = userDB.merchant_id;

                ServiceBase serviceBase = new ServiceBase();
                SqlDataReader reader;
                Campaign campaign = new Campaign();
                //  Giftcode giftcode = new Giftcode();
                //---lấy giftcodecreate
                code = code.Trim();



                var giftcode = (from w in db.Giftcode
                                where (w.code == code)
                                select new
                                {
                                    w.id,
                                    w.value,
                                    w.user_id,
                                    w.campaign_id,
                                    w.is_active
                                }).FirstOrDefault();
                if (giftcode == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Sai mã giftcode"

                    });
                }
                //---kiểm tra tiền giftcode có âm không
                if (giftcode.value < 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Không nạp được giftcode âm."
                    });
                }
                //---kiểm tra user đã nạp thẻ nào chưa


                if (giftcode.user_id == userDB.id)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Bạn đã sử dụng 1 giftcode trong chiến dịch này, không thể sử dụng thêm nữa!!!"
                    });
                }


                string queryStringUpdateUser = "SELECT TOP 1 id,name,start_time,end_time,merchant_id FROM Campaign WHERE id =@code";
                SqlCommand command;
                command = new SqlCommand(queryStringUpdateUser, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@code", giftcode.campaign_id);
                reader = serviceBase.querySqlParam(command);

                Campaign campain = new Campaign();
                while (reader.Read())
                {
                    campaign.id = Int32Ext.toInt(reader["id"].ToString());
                    campaign.name = reader["name"].ToString();
                    campaign.start_time = DateTime.Parse(reader["start_time"].ToString());
                    campaign.end_time = DateTime.Parse(reader["end_time"].ToString());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                if (campaign == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Chưa chạy chiến dịch này"
                    });
                }
                //Kiểm tra thẻ thành công
                //DateTime d1, d2;
                if (/*(DateTime.TryParse(campain.start_time, out d1) &&*/
                    DateTime.Now < campain.start_time || /*(DateTime.TryParse(campaign.end_time, out d2) &&*/
                    DateTime.Now > campaign.end_time)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Chưa chạy chiến dịch này"
                    });
                }
                else
                {
                    if (giftcode.is_active == 1)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Giftcode này đã được sử dụng"
                        });
                    }
                }


                this.app_id = userDB.merchant_id;
                //Kiểm tra thẻ thành công

                //Kieemr tra cong thanh toan
                PaymentGate paymentGateService = this.getPaymentGate("GIFTCODE", db);
                if (paymentGateService.id == 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Cổng thanh toán không tồn tại."
                    }, JsonRequestBehavior.AllowGet);
                }


                if (!paymentGateService.status_active.Trim().Equals("active"))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Cổng thanh toán đang bị khóa ."
                    }, JsonRequestBehavior.AllowGet);
                }

                //Kiểm tra tham số trừ tiền có đúng config
                CashIORate cashIORate;
                try
                {
                    cashIORate = this.checkValueConfig(paymentGateService, giftcode.value);
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Kiểm tra config lỗi: " + ex.Message
                    }, JsonRequestBehavior.AllowGet);
                }

                if (cashIORate.cash_in == 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Số tiền vượt quá config được chuyển "
                    }, JsonRequestBehavior.AllowGet);
                }

                //---lay merchant
                var merchant_item = db.Merchant.FirstOrDefault(m => (m.id == this.app_id));
                var user_merchant = db.User.FirstOrDefault(u => (u.username == merchant_item.username));

                // Update tiên cho merchant
                double balance = this.getBalace(user_merchant.id, paymentGateService.wallet_in);
                double profit = 0;
                double plus_money_profit = 0;
                plus_money_profit = double.Parse(giftcode.value.ToString()) / cashIORate.cash_in * cashIORate.cash_out;
                plus_money_profit = Math.Round(plus_money_profit, 2, MidpointRounding.ToEven);

                balance = balance - plus_money_profit;
                DateTime time = DateTime.Now;
                // Use current time
                string format = "yyyy-MM-dd HH:mm:ss";
                reader = serviceBase.query("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) " +
                    "VALUES (" + user_merchant.id + "," + this.app_id + "," + paymentGateService.id + ",0," + balance + ",'minus','" + time.ToString(format) + "'," + giftcode.value + "," + userDB.id + ",N'giftcode',N'User nạp giftcode'," + profit + "," + plus_money_profit + ")");
                DatabaseService.Instance.getConnection().Close();

                //Update tiền cho user
                double balance_user = this.getBalace(userDB.id, paymentGateService.wallet_out);
                balance_user = balance_user + plus_money_profit;

                reader = serviceBase.query("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) " +
        "VALUES (" + userDB.id + "," + this.app_id + "," + paymentGateService.id + ",0," + balance_user + ",'plus','" + time.ToString(format) + "'," + giftcode.value + "," + user_merchant.id + ",N'giftcode',N'User nạp giftcode'," + profit + "," + plus_money_profit + ")");

                DatabaseService.Instance.getConnection().Close();


                //Tính số tiền trong ví để cộng
                double balance_out = 0;
                try
                {
                    balance_out = this.getBalace(userDB.id, paymentGateService.wallet_out);
                }
                catch (Exception)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Lỗi lấy tổng tiền trong ví user để cộng"
                    }, JsonRequestBehavior.AllowGet);
                }

                balance_out = balance_out + plus_money_profit;
                reader = serviceBase.query("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message) VALUES (" +
        userDB.id + "," + this.app_id + "," + paymentGateService.id + "," + paymentGateService.wallet_out + "," + balance_out + ",'plus','" + time.ToString(format) + "'," + plus_money_profit + "," + user_merchant.id + ",N'giftcode',N'Nạp Giftcode')");
                DatabaseService.Instance.getConnection().Close();

                //---gạch thẻ giftcode 
                string query = "UPDATE Giftcode SET is_active = 1, user_id = " + userDB.id + " WHERE id=" + giftcode.id;
                reader = serviceBase.query(query);
                DatabaseService.Instance.getConnection().Close();

                //---connect photon
                var demoLB = new UserPhoton(ExitGames.Client.Photon.ConnectionProtocol.Tcp);
                var userDB1 = db.User.Where(e => e.id == 155).FirstOrDefault();
                demoLB.user = userDB1;
                demoLB.uidRecieve = userID;
                demoLB.title = "[Hệ thống] Nhận thư Giftcode";
                demoLB.msg = "Bạn nhận được " + (long)plus_money_profit + " Coin";
                demoLB.Connect();

                return Json(new
                {
                    error = false,
                    msg = "Nạp Giftcode thành công, bạn nhận được " + plus_money_profit + " Coin"
                });
            }

        }



        //Lấy số tiền trong ví
        private double getBalace(int user_id, int wallet_in)
        {
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            double balance = 0;
            string queryString = "SELECT TOP 1 balance FROM Transactions WHERE wallet_id =@wallet_id " +
                "AND user_id=@user_id AND (transactions_type = 0 OR transactions_type IS NULL) ORDER BY id DESC";
            SqlCommand command;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@user_id", user_id);
            command.Parameters.AddWithValue("@wallet_id", wallet_in);
            reader = serviceBase.querySqlParam(command);
            while (reader.Read())
            {
                balance = double.Parse(reader["balance"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return balance;
        }

        //Lấy số tiền trong ví nạp của admin
        private double getBalaceInput()
        {
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            double balance = 0;
            reader = serviceBase.query("SELECT TOP 1 balance FROM Transactions WHERE wallet_id =0 " +
                "AND user_id=0 AND type ='plus' ORDER BY id DESC");
            while (reader.Read())
            {
                balance = double.Parse(reader["balance"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            return balance;
        }
        //[HttpPost]
        //public ActionResult addCard(string code, string seri, string typecard, string den)
        //{
        //    using (var db = new GamePaymentContext())
        //    {
        //        int userID = 0;
        //        try
        //        {
        //            userID = (new AuthenticationHelper(Session).ID());
        //        }
        //        catch (Exception)
        //        {
        //            return Json(new
        //            {
        //                error = true,
        //                msg = "Vui lòng đăng nhập lại"
        //            });
        //        }

        //        var userDB = db.User.FirstOrDefault(e => e.id == userID);
        //        this.app_id = userDB.merchant_id;

        //        string username = ConfigurationManager.AppSettings["usernameAPIcard1Pay"];
        //        string password = ConfigurationManager.AppSettings["passwordAPIcard1Pay"];

        //        string parsedContent = @"{
        //        'username': 'tuongca',
        //        'password': 'banbia'
        //            }";
        //        string result_login = this._postURLLogin(ConfigurationManager.AppSettings["urlAPIcard1Pay"] + "/user/login", parsedContent);

        //        ParamAPILoginCard json_login = JsonConvert.DeserializeObject<ParamAPILoginCard>(result_login);
        //        if (!json_login.code.Equals("200"))
        //        {
        //            return Json(new
        //            {
        //                error = true,
        //                msg = json_login.message,
        //                code = json_login.code
        //            });
        //        }
        //        string parsedContentCard = "{ \"telcoId\":\"" + typecard + "\",\"denId\":\"" + den + "\",\"code\":\"" + code + "\",\"serial\":\"" + seri + "\",\"scratchCallbackUrl\":\"" + ConfigurationManager.AppSettings["domainCallBack"] + "/UsersCreatePayAPI/callbackURLInput\" }";
        //        string result_card = this._postURLLogin(ConfigurationManager.AppSettings["urlAPIcard1Pay"] + "/v2/cp/card?auth=" + json_login.data.sid, parsedContentCard);
        //        ResponseAddCard json_card = JsonConvert.DeserializeObject<ResponseAddCard>(result_card);
        //        if (!json_card.code.Equals("200"))
        //        {
        //            return Json(new
        //            {
        //                error = true,
        //                msg = "Mã code hoặc seri thẻ không tồn tại",
        //                code = json_card.code,
        //                url = ConfigurationManager.AppSettings["domainCallBack"]
        //            });
        //        }

        //        //WebClient n = new WebClient();
        //        //string url_get_card = ConfigurationManager.AppSettings["urlAPIcard1Pay"] + @"/cp/card/" + json_card.data.id + "?auth=" + json_login.data.sid;
        //        //var json = n.DownloadString(url_get_card);
        //        //string valueOriginal = Convert.ToString(json);
        //        //ResponseGetCard responseCard = JsonConvert.DeserializeObject<ResponseGetCard>(valueOriginal);
        //        //if (!responseCard.code.Equals("200"))
        //        //{
        //        //    return Json(new
        //        //    {
        //        //        error = true,
        //        //        msg = "ID thẻ không tồn tại",
        //        //        code = responseCard.code
        //        //    });
        //        //}
        //        //string sid = System.Uri.UnescapeDataString(json_card.data.id);
        //        string sid = seri;
        //        db.Transactions_Log.Add(new Models.Transactions_Log()
        //        {
        //            user_id = userID,
        //            time = DateTime.Now,
        //            sid = sid,
        //            amout = 0.00,
        //            status = 0,
        //            url = ConfigurationManager.AppSettings["domainCallBack"] + "/UsersCreatePayAPI/callbackURLInput"
        //        });
        //        db.SaveChanges();

        //        return Json(new
        //        {
        //            error = false,
        //            msg = "Yêu cầu gửi thẻ dã được gửi. Xin vui lòng đợi hệ thống xử lý",
        //            domain = parsedContentCard
        //        });
        //    }

        //}

        //---bunbun card vietel 
        [HttpPost]
        public ActionResult addCard(string cardNumber, string serialNumber, int cardValue, string provider)
        {
            try
            {
                if (cardNumber == null || serialNumber == null || cardValue == null || provider == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Tham số null"
                    });
                }
                using (var db = new GamePaymentContext())
                {
                    int userID = 0;
                    try
                    {
                        userID = (new AuthenticationHelper(Session).ID());
                        AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
                    }
                    catch (Exception)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Vui lòng đăng nhập lại"
                        });
                    }

                    var userDB = db.User.FirstOrDefault(e => e.id == userID);
                    this.app_id = userDB.merchant_id;

                    string urlCreateCard = ConfigurationManager.AppSettings["urlCreateCard"];
                    string gameCode = ConfigurationManager.AppSettings["gameCode"];
                    string nccCode = ConfigurationManager.AppSettings["nccCode"];
                    string accessKey = ConfigurationManager.AppSettings["accessKey"];
                    string sercretKey = ConfigurationManager.AppSettings["sercretKey"];

                    string requestId = CryptoHelper.randomToken();

                    string text = requestId + "|" + nccCode + "|" + gameCode + "|" + userDB.username + "|" + cardNumber + "|" + serialNumber + "|" + cardValue + "|" + provider + "|1|" + accessKey;
                    //string plantext = CryptoHelper.Base64Encode(text);
                    var signature = CryptoHelper.HmacSha256Digest(text, sercretKey);

                    text += "|" + signature;
                    var data = CryptoHelper.Base64Encode(text);

                    //string postCreate = this._postURLLogin(urlCreateCard, data);
                    string getCreate = this.getMethod(urlCreateCard, data);

                    ResponseCardVT jsonCreateCard = JsonConvert.DeserializeObject<ResponseCardVT>(getCreate);
                    if (jsonCreateCard.IsError.Equals("true"))
                    {
                        return Json(new
                        {
                            error = true,
                            msg = jsonCreateCard.Message,
                            code = jsonCreateCard.ErrorCode
                        });
                    }

                    string sid = requestId;
                    ServiceBase serviceBase = new ServiceBase();
                    string query = "INSERT INTO Transactions_LogVT(user_id,time,requestId,amout,status,url,cardCode,cardSerial) " + "VALUES (@userID, convert(datetime,'" + DateTime.Now + "'),@sid,0,0,'" + ConfigurationManager.AppSettings["domainCallBack"] + "/UsersCreatePayAPI/callbackURLInput',@cardNumber,@serialNumber)";

                    SqlCommand command;
                    command = new SqlCommand(query, DatabaseService.Instance.getConnection());
                    command.Parameters.AddWithValue("@userID", userID);
                    command.Parameters.AddWithValue("@sid", sid);
                    command.Parameters.AddWithValue("@cardNumber", cardNumber);
                    command.Parameters.AddWithValue("@serialNumber", serialNumber);
                    serviceBase.querySqlParamInsert(command);
                    DatabaseService.Instance.getConnection().Close();

                    return Json(new
                    {
                        error = false,
                        msg = "Yêu cầu gửi thẻ dã được gửi. Xin vui lòng đợi hệ thống xử lý"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("loi: " + e.Message);
                return Json(new
                {
                    error = true,
                    msg = "Lỗi nạp thẻ: " + e.Message
                });
            }
        }


        private readonly static string reservedCharacters = "!*'();:@&=+$,/?%#[]";

        private string urlEncode(string value)
        {
            if (String.IsNullOrEmpty(value))
                return String.Empty;

            var sb = new StringBuilder();

            foreach (char @char in value)
            {
                if (reservedCharacters.IndexOf(@char) == -1)
                    sb.Append(@char);
                else
                    sb.AppendFormat("%{0:X2}", (int)@char);
            }
            return sb.ToString();
        }

        private string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "128.199.171.7";
        }
        //[HttpPost]
        //public ActionResult postOnepay(string amout, string bank)
        //{
        //    return Json(new
        //    {
        //        error = true,
        //        msg = "Chua dung."
        //    });
        //    using (var db = new GamePaymentContext())
        //    {
        //        int userID = 0;
        //        try
        //        {
        //            userID = (new AuthenticationHelper(Session).ID());
        //            AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
        //        }
        //        catch (Exception)
        //        {
        //            return Json(new
        //            {
        //                error = true,
        //                msg = "Vui lòng đăng nhập lại"
        //            });
        //        }

        //        DateTime dateTime = DateTime.UtcNow.ToLocalTime().ToUniversalTime().AddHours(8);
        //        int timeMsSinceMidnight = (int)dateTime.TimeOfDay.TotalMilliseconds;

        //        string format = "yyyy-MM-dd hh:mm:sstt";
        //        string format_re = "yyyy-MM-dd HH:mm:ss";
        //        PostOnePay postOnePay = new PostOnePay();
        //        postOnePay.reference = userID.ToString() + timeMsSinceMidnight.ToString();
        //        postOnePay.currency = "VND";
        //        postOnePay.customer = userID.ToString();
        //        postOnePay.amount = amout;
        //        postOnePay.datetime = dateTime.ToString(format);

        //        string rep_time = dateTime.ToString(format_re);
        //        rep_time = rep_time.Replace("-", "");
        //        rep_time = rep_time.Replace(":", "");
        //        rep_time = rep_time.Replace(" ", "");

        //        postOnePay.clientIP = this.GetLocalIPAddress();
        //        string md5 = String.Format("{0}{1}{2}{3}{4}{5}{6}{7}", postOnePay.merchant, postOnePay.reference, postOnePay.customer, postOnePay.amount, postOnePay.currency, rep_time, postOnePay.securityCode, postOnePay.clientIP);
        //        string md5_base = CryptoHelper.Md5Base(md5);
        //        postOnePay.key = md5_base.ToUpper();


        //        db.Transactions_Log_OnePay.Add(new Models.Transactions_Log_OnePay()
        //        {
        //            user_id = userID,
        //            time = DateTime.Now,
        //            sid = postOnePay.reference,
        //            amout = Double.Parse(amout.Trim()),
        //            status = 0
        //        });
        //        db.SaveChanges();

        //        return Json(new
        //        {
        //            error = false,
        //            item = postOnePay
        //        });
        //    }
        //}


        //[HttpPost]
        //public ActionResult callbackURLInputBank(string Merchant, string Reference, string Currency, string Amount, string Language, string Customer, string Datetime, string StatementDate, string Note, string Key, string Status, string ID)
        //{
        //    return Json(new
        //    {
        //        error = true,
        //        msg = "Chua dung."
        //    });
        //    using (var db = new GamePaymentContext())
        //    {
        //        string documentContents;
        //        using (Stream receiveStream = Request.InputStream)
        //        {
        //            using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
        //            {
        //                documentContents = readStream.ReadToEnd();
        //            }
        //        }

        //        db.Transactions_Log_Response_OnePay.Add(new Models.Transactions_Log_Response_OnePay()
        //        {
        //            time = DateTime.Now,
        //            param = "Merchant: " + Merchant + " / Reference: " + Reference + " / Currency: " + Currency + " / Amount : " + Amount + " / Language: " + Language
        //            + " / Customer: " + Customer + " / Datetime: " + Datetime + " / StatementDate: " + StatementDate + " / Note: " + Note + " / Key: " + Key + " / Status: " + Status + " / ID" + ID
        //        });
        //        db.SaveChanges();
        //        var tran_log = db.Transactions_Log_OnePay.Where(t => (t.sid == Reference)).FirstOrDefault();
        //        if (tran_log == null)
        //        {
        //            return Json(new
        //            {
        //                error = true,
        //                msg = "ID không tồn tại."
        //            });
        //        }
        
                    //this.app_id = userDB.merchant_id;
                    ////Kiểm tra thẻ thành công
                    
                    ////Kieemr tra cong thanh toan
                    //PaymentGate paymentGateService = this.getPaymentGate("NAPONEPAY",db);
                    //if (paymentGateService.id == 0)
                    //{
                    //    return Json(new
                    //    {
                    //        error = true,
                    //        msg = "Cổng thanh toán không tồn tại."
                    //    }, JsonRequestBehavior.AllowGet);
                    //}

        //            double a_tran_log = tran_log.amout;
        //            double price = a_tran_log;
        //            int i_status = Int32.Parse(Status.Trim());

        //            tran_log.amout = a_tran_log;
        //            tran_log.status = i_status;
        //            db.SaveChanges();

        //            this.app_id = userDB.merchant_id;
        //            //Kiểm tra thẻ thành công

        //            //Kieemr tra cong thanh toan
        //            PaymentGate paymentGateService = this.getPaymentGate("NAPONEPAY",db);
        //            if (paymentGateService.id == 0)
        //            {
        //                return Json(new
        //                {
        //                    error = true,
        //                    msg = "Cổng thanh toán không tồn tại."
        //                }, JsonRequestBehavior.AllowGet);
        //            }

        //            if (!paymentGateService.status_active.Trim().Equals("active"))
        //            {
        //                return Json(new
        //                {
        //                    error = true,
        //                    msg = "Cổng thanh toán đang bị khóa ."
        //                }, JsonRequestBehavior.AllowGet);
        //            }

        //            //Kiểm tra tham số trừ tiền có đúng config
        //            CashIORate cashIORate;
        //            try
        //            {
        //                cashIORate = this.checkValueConfig(paymentGateService, price);
        //            }
        //            catch (Exception ex)
        //            {
        //                return Json(new
        //                {
        //                    error = true,
        //                    msg = "Kiểm tra config lỗi: " + ex.Message
        //                }, JsonRequestBehavior.AllowGet);
        //            }

        //            if (cashIORate.cash_in == 0)
        //            {
        //                return Json(new
        //                {
        //                    error = true,
        //                    msg = "Số tiền vượt quá config được chuyển "
        //                }, JsonRequestBehavior.AllowGet);
        //            }

        //            ServiceBase serviceBase = new ServiceBase();
        //            SqlDataReader reader;

        //            // Update tiên cho admin
        //            double balance = this.getBalaceInput();
        //            double profit = 0;
        //            double plus_money_profit = 0;
        //            plus_money_profit = double.Parse(price.ToString()) / cashIORate.cash_in * cashIORate.cash_out;
        //            plus_money_profit = Math.Round(plus_money_profit, 2, MidpointRounding.ToEven);


        //            balance = balance + price;
        //            DateTime time = DateTime.Now;
        
                    //DatabaseService.Instance.getConnection().Close();
                    


        //            DatabaseService.Instance.getConnection().Close();

        //            //Update tiền cho user
        //            double balance_user = this.getBalace(userDB.id, 0);
        //            balance_user = balance_user + price;

        //            reader = serviceBase.query("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) " +
        //    "VALUES (" + userDB.id + "," + this.app_id + "," + paymentGateService.id + ",0," + balance_user + ",'plus','" + time.ToString(format) + "'," + price + ",0,N'naptien',N'User nạp tiền'," + profit + "," + plus_money_profit + ")");

        //            DatabaseService.Instance.getConnection().Close();

        //            //Tính số tiền trong ví để cộng
        //            double balance_out = 0;
        //            try
        //            {
        //                balance_out = this.getBalace(userDB.id, paymentGateService.wallet_out);
        //            }
        //            catch (Exception)
        //            {
        //                return Json(new
        //                {
        //                    error = true,
        //                    msg = "Lỗi lấy tổng tiền trong ví user để cộng"
        //                }, JsonRequestBehavior.AllowGet);
        //            }

        //            balance_out = balance_out + plus_money_profit;
        //            reader = serviceBase.query("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message) VALUES (" +
        //    userDB.id + "," + this.app_id + "," + paymentGateService.id + "," + paymentGateService.wallet_out + "," + balance_out + ",'plus','" + time.ToString(format) + "'," + plus_money_profit + ",0,N'naptien',N'Nạp tiền')");
        //            DatabaseService.Instance.getConnection().Close();

        //            return Json(new
        //            {
        //                error = false,
        //                status = Status
        //            });
        //        }
        //        else
        //        {
        //            return Json(new
        //            {
        //                error = false,
        //                status = Status
        //            });
        //        }


        //    }

        //}

        
        //public void ProcessRequest(HttpContext httpContext)
        //{
        //    HttpResponse response = httpContext.Response;
        //    response.Clear();
        //    response.BufferOutput = true;
        //    response.StatusCode = HttpStatusCode.OK;
        //    response.Write("Hello");
        //    response.ContentType = "text/xml";
        //    response.End();
        //}

        //---callback card VT
        [HttpGet]
        public string callbackURLInput(string data)
        {
            string documentContents;
            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }

            using (var db = new GamePaymentContext())
            {
                string stringDecrypt = CryptoHelper.Base64Decode(data);
                string[] str = stringDecrypt.Split('|');
                var errorCode = str[3];

                var requestId = str[0];
                var cardValue = str[2];

                string sid = requestId;
                if (sid == null)
                {
                    return "04|Truy cập bị từ chối";
                }

                //---
                ServiceBase serviceBase = new ServiceBase();
                //SqlDataReader reader;

                Transactions_LogVT tran_log = new Transactions_LogVT();

                tran_log = db.Transactions_LogVT.FirstOrDefault(t => t.requestId == sid);

                //---add DB SQL
                var param = "ID: " + sid + " / CODE: " + tran_log.cardCode + " / Seri: " + tran_log.cardSerial + " / Amount : " + cardValue + " / STATUS: " + errorCode;
                db.Transactions_Log_ResponseVT.Add(new Models.Transactions_Log_ResponseVT()
                {
                    time = DateTime.Now,
                    param = param
                });
                db.SaveChanges();
                if (!errorCode.Equals("00"))
                {
                    var userDB1 = db.User.Where(e => e.id == 155).FirstOrDefault();
                    //---connect photon
                    var demoLB1 = new UserPhoton(ExitGames.Client.Photon.ConnectionProtocol.Tcp);

                    demoLB1.user = userDB1;
                    demoLB1.uidRecieve = tran_log.user_id;
                    demoLB1.title = "[Hệ thống] Nhận thư Nạp thẻ";
                    demoLB1.msg = "Thông tin thẻ không đúng hoặc đã được sử dụng";
                    demoLB1.Connect();

                    return "04|Truy cập bị từ chối";
                }
                //---
                if (tran_log == null)
                {
                    return "03|Thông tin thẻ không đúng hoặc đã được sử dụng";
                }


                if (tran_log.status == 1)
                {
                    return "03|Thông tin thẻ không đúng hoặc đã được sử dụng";
                }


                //if (status.Equals("1"))
                //{
                var userDB = db.User.FirstOrDefault(e => e.id == tran_log.user_id);

                float a_tran_log = float.Parse(cardValue.Trim());
                double price = a_tran_log;
                int i_status = 1;
                var transactions_LogVT = db.Transactions_LogVT.FirstOrDefault(t => t.requestId == sid);
                transactions_LogVT.status = i_status;
                transactions_LogVT.amout = a_tran_log;
                db.SaveChanges();
                //tran_log.amout = a_tran_log;
                //tran_log.status = i_status;
                //db.SaveChanges();

                this.app_id = userDB.merchant_id;
                //Kiểm tra thẻ thành công

                //Kieemr tra cong thanh toan
                PaymentGate paymentGateService = this.getPaymentGate("NAPTHE", db);
                if (paymentGateService.id == 0)
                {
                    //return Json(new
                    //{
                    //    error = true,
                    //    msg = "Cổng thanh toán không tồn tại."
                    //}, JsonRequestBehavior.AllowGet);
                    return "04|Truy cập bị từ chối";

                }


                if (!paymentGateService.status_active.Trim().Equals("active"))
                {
                    //return Json(new
                    //{
                    //    error = true,
                    //    msg = "Cổng thanh toán đang bị khóa ."
                    //}, JsonRequestBehavior.AllowGet);
                    return "04|Truy cập bị từ chối";

                }

                //Kiểm tra tham số trừ tiền có đúng config
                CashIORate cashIORate;
                try
                {
                    cashIORate = this.checkValueConfig(paymentGateService, price);
                }
                catch (Exception)
                {
                    //return Json(new
                    //{
                    //    error = true,
                    //    msg = "Kiểm tra config lỗi: " + ex.Message
                    //}, JsonRequestBehavior.AllowGet);
                    return "04|Truy cập bị từ chối";

                }

                if (cashIORate.cash_in == 0)
                {
                    //return Json(new
                    //{
                    //    error = true,
                    //    msg = "Số tiền vượt quá config được chuyển "
                    //}, JsonRequestBehavior.AllowGet);
                    return "04|Truy cập bị từ chối";

                }

                // Update tiên cho admin
                double balance = this.getBalaceInput();
                double profit = 0;
                double plus_money_profit = 0;
                plus_money_profit = double.Parse(price.ToString()) / cashIORate.cash_in * cashIORate.cash_out;
                plus_money_profit = Math.Round(plus_money_profit, 2, MidpointRounding.ToEven);


                balance = balance + price;
                DateTime time = DateTime.Now;
                // Use current time
                //string format = "yyyy-MM-dd HH:mm:ss";


                db.Transactions.Add(new Models.Transactions()
                {
                    user_id = 0,
                    merchant_id = this.app_id,
                    payment_gate = paymentGateService.id,
                    wallet_id = 0,
                    balance = balance,
                    type = "plus",
                    value = price,
                    transactions_type = 0,
                    user_send = userDB.id,
                    tag = "naptien",
                    message = "User nạp tiền",
                    time_creat = DateTime.Now,
                    profit = profit,
                    value_out = plus_money_profit
                });
                db.SaveChanges();

                //Update tiền cho user
                double balance_user = this.getBalace(userDB.id, 0);
                balance_user = balance_user + price;

                db.Transactions.Add(new Models.Transactions()
                {
                    user_id = userDB.id,
                    merchant_id = this.app_id,
                    payment_gate = paymentGateService.id,
                    wallet_id = 0,
                    balance = balance_user,
                    type = "plus",
                    value = price,
                    transactions_type = 0,
                    user_send = 0,
                    tag = "naptien",
                    message = "User nạp tiền",
                    time_creat = DateTime.Now,
                    profit = profit,
                    value_out = plus_money_profit
                });
                db.SaveChanges();

                //Tính số tiền trong ví để cộng
                double balance_out = 0;
                try
                {
                    balance_out = this.getBalace(userDB.id, paymentGateService.wallet_out);
                }
                catch (Exception)
                {
                    //return Json(new
                    //{
                    //    error = true,
                    //    msg = "Lỗi lấy tổng tiền trong ví user để cộng"
                    //}, JsonRequestBehavior.AllowGet);
                    string rtnString7 = "04|Truy cập bị từ chối";
                    Response.Write(rtnString7);
                    Response.End();
                }

                balance_out = balance_out + plus_money_profit;
                db.Transactions.Add(new Models.Transactions()
                {
                    user_id = userDB.id,
                    merchant_id = this.app_id,
                    payment_gate = paymentGateService.id,
                    wallet_id = paymentGateService.wallet_out,
                    balance = balance_out,
                    type = "plus",
                    value = price,
                    transactions_type = 0,
                    user_send = 0,
                    tag = "naptien",
                    message = "User nạp tiền",
                    time_creat = DateTime.Now,
                    profit = profit,
                    value_out = plus_money_profit
                });
                db.SaveChanges();


                //---connect photon
                var demoLB = new UserPhoton(ExitGames.Client.Photon.ConnectionProtocol.Tcp);
                var userDB2 = db.User.Where(e => e.id == 155).FirstOrDefault();
                demoLB.user = userDB2;
                demoLB.uidRecieve = tran_log.user_id;
                demoLB.title = "[Hệ thống] Nhận thư Nhận thư Nạp thẻ";
                demoLB.msg = "Bạn đã nhận được " + (long)plus_money_profit + " Coin";
                demoLB.Connect();

                return "00|Thành công";

            }
        }

        
        [HttpPost]
        public ActionResult History(string walletid, String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength, string tag_type)
        {
            if (walletid == null || iSortCol_0 == null || sSortDir_0== null || iDisplayStart ==  null || iDisplayLength == null || tag_type == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }

            using (var db = new GamePaymentContext())
            {
                var userID = (new AuthenticationHelper(Session).ID());
                string[] a_sort = new string[] { "Transactions.time_creat", "wallet_name", "Transactions.type", "Transactions.value", "Transactions.balance", "user_name", "Transactions.tag", "Transactions.message" };
                ServiceBase serviceBase = new ServiceBase();
                string queryString = "SELECT" +
                    " Transactions.id" +
                    " ,Transactions.time_creat" +
                    " ,(SELECT TOP 1 name FROM Wallet WHERE id = Transactions.wallet_id) as 'wallet_name'" +
                    " ,Transactions.type" +
                    " ,Transactions.value" +
                    " ,Transactions.balance" +
                    " ,Transactions.tag" +
                    " ,Transactions.message" +
                    " ,Transactions.tag_type" +
                     " ,Transactions.user_send as 'user_send'" +
                    " ,(SELECT TOP 1 title FROM Agency WHERE id = Transactions.user_send) as 'user_name'" +
                    " FROM Transactions WHERE Transactions.user_id =@userID AND transactions_type = 0 ";

                string queryCount = "SELECT COUNT(id) as 'count' FROM Transactions WHERE user_id =@userID AND transactions_type = 0 ";
                if (!walletid.Equals("all"))
                {
                    queryString += " AND Transactions.wallet_id = @walletid";
                    queryCount += " AND wallet_id = @walletid";
                }

                if (!tag_type.Trim().Equals(""))
                {
                    if (tag_type.Equals("1"))
                    {
                        queryString += " AND Transactions.tag_type = '1'";
                        queryCount += " AND tag_type = '1'";
                    }
                    if (tag_type.Equals("2"))
                    {
                        queryString += " AND Transactions.tag_type = '0'";
                        queryCount += " AND (tag_type = '0' OR tag_type IS NULL) ";
                    }


                }

                int int_iSortCol_0 = Int32Ext.toInt(iSortCol_0);
                if (int_iSortCol_0 > 7)
                {
                    int_iSortCol_0 = 0;
                }
                if (sSortDir_0.Equals("desc"))
                {
                    sSortDir_0 = "DESC";
                }
                else
                {
                    sSortDir_0 = "ASC";
                }
                queryString += " ORDER BY " + a_sort[int_iSortCol_0] + " " + sSortDir_0 + " OFFSET @iDisplayStart ROWS FETCH NEXT @iDisplayLength ROWS ONLY";
                // SqlDataReader reader = serviceBase.query(queryString);
                SqlCommand command;
                command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@userID", userID);
                command.Parameters.AddWithValue("@walletid", walletid);
                command.Parameters.AddWithValue("@iDisplayStart", Int32Ext.toInt(iDisplayStart));
                command.Parameters.AddWithValue("@iDisplayLength", Int32Ext.toInt(iDisplayLength));
                SqlDataReader reader = serviceBase.querySqlParam(command);

                List<String[]> users = new List<String[]>();
                while (reader.Read())
                {
                    string[] s_user = new string[8];
                    s_user[0] = reader["time_creat"].ToString().Trim();
                    s_user[1] = reader["wallet_name"].ToString().Trim();
                    string s_type = "";
                    if (reader["type"].ToString().Trim().Equals("plus"))
                    {
                        s_user[2] = "<a style=\"color:#4CB052\" >Cộng tiền</a>";
                        s_type = "+";
                        s_user[3] = "<a style=\"color:#4CB052\" >" + s_type + Double.Parse(reader["value"].ToString().Trim()).ToString("N", CultureInfo.InvariantCulture) + "</a>";
                    }
                    else
                    {
                        s_user[2] = "<a style=\"color:red\" >Trừ tiền</a>";
                        s_type = "-";
                        s_user[3] = "<a style=\"color:red\" >" + s_type + Double.Parse(reader["value"].ToString().Trim()).ToString("N", CultureInfo.InvariantCulture) + "</a>";
                    }


                    s_user[4] = Double.Parse(reader["balance"].ToString().Trim()).ToString("N", CultureInfo.InvariantCulture);

                    if (reader["user_name"].ToString().Trim().Equals(""))
                    {
                        if (reader["user_send"].ToString().Trim() != null)
                        {
                            try
                            {
                                int user_send_user = Int32.Parse(reader["user_send"].ToString().Trim());
                                var user_endMD = db.User.FirstOrDefault(e => (e.id == user_send_user));
                                s_user[5] = user_endMD.username;
                            }
                            catch (Exception)
                            {
                                s_user[5] = "";
                            }

                        }

                    }
                    else
                    {
                        s_user[5] = reader["user_name"].ToString().Trim();
                    }
                    s_user[6] = reader["tag"].ToString().Trim();

                    s_user[7] = reader["message"].ToString().Trim();
                    users.Add(s_user);
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                int row = 0;
                command = new SqlCommand(queryCount, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@userID", userID);
                command.Parameters.AddWithValue("@walletid", walletid);
                reader = serviceBase.querySqlParam(command);
                while (reader.Read())
                {
                    row = Int32.Parse(reader["count"].ToString());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                return Json(new
                {
                    aaData = users,
                    iTotalRecords = row,
                    iTotalDisplayRecords = row,
                    display = iDisplayLength
                });
            }
        }



        public ActionResult sendSMS(int gateid, int cash, string s_user, string content)
        {
            if (gateid == null || cash == null || s_user == null || content == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            
            var userID = (new AuthenticationHelper(Session).ID());
          

            // Calculate money

            double cashIn = cash;
            // Gate
            if (cashIn <= 0)
            {
                return Json(new
                {
                    error = true,
                    msg = "Số tiền không được <= 0"
                });
            }

            var gateID = gateid;
            using (var db = new GamePaymentContext())
            {
                var wi = db.PaymentGate.FirstOrDefault(e => e.id == gateID);
                if (wi == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Cổng thanh toán không tồn tại"
                    });
                }
                if (wi.wallet_in == 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Cổng thanh toán chưa được chọn loại ví vào"
                    });
                }

                if (wi.wallet_out == 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Cổng thanh toán chưa được chọn loại ví ra"
                    });
                }

                var query_cashInOut = db.CashIORate.OrderBy(c => c.cash_in).Where(c => (c.payment_gate_id == gateID && c.cash_in >= cashIn)).FirstOrDefault();
                if (query_cashInOut == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Số tiền vượt quá config"
                    });
                }

                // Kiem tra so du tai khoan và ballance cua user xem du tien khong
                var query = db.Transactions
                        .OrderByDescending(p => p.id)
                        .Where(e => (e.user_id == userID && e.transactions_type == 0 && e.merchant_id == wi.merchant_id && e.wallet_id == wi.wallet_in))
                        .FirstOrDefault();
                double agencyMoney = 0;
                if (query != null)
                {
                    agencyMoney = query.balance;
                }
                if (agencyMoney < cashIn)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Bạn không đủ tiền để giao dịch"
                    });
                }
                //Xóa những lịch sử cache otp cũ
                var record_otp = from c in db.OTP_user
                                 where c.user_id == userID && c.type == 0
                                 select new
                                 {
                                     c.id,
                                     c.time_creat,
                                     c.user_id,
                                     c.type,
                                     c.otp,
                                     c.status
                                 };
                record_otp.ToList().ForEach(ent =>
                {
                    var otp_user = db.OTP_user.FirstOrDefault(o => (o.id == ent.id));
                    db.OTP_user.Remove(otp_user);
                    db.SaveChanges();
                });



                //Insert cache giao dịch
                var nowtime = DateTime.Now;
                //CryptoHelper cryptoHelper = new CryptoHelper();
                var token = CryptoHelper.RandomString(6);

                if (wi.sms != 0)
                {
                    //Nếu cần SMS
                    DateTime time = DateTime.Now;
                    db.OTP_user.Add(new Models.OTP_user()
                    {
                        user_id = userID,
                        type = 0,
                        otp = token,
                        time_creat = time,
                        status = 0
                    });
                    db.SaveChanges();

                    var record_agency = db.User.Where(e => e.id == userID).FirstOrDefault();
                    if (record_agency.verify_phone != 0)
                    {
                        string phone = record_agency.phone.Trim().Substring(1, record_agency.phone.Trim().Length - 1);
                        SendSMSHelper sendSMSHelper = new SendSMSHelper();
                        string result = sendSMSHelper.send("+84" + phone, token);
                        return Json(new
                        {
                            error = false,
                            result = result,
                            sms = 1
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Bạn chưa verify số điện thoại.",
                            sms = 1
                        });
                    }
                }
                else
                {
                    //Nếu không cần SMS
                    string[] separators = { "," };
                    string[] words = s_user.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    double moneyAgencyMinus = agencyMoney;
                    foreach (var word in words)
                    {
                        //Lấy số tiền ra
                        moneyAgencyMinus -= cashIn;
                        if (moneyAgencyMinus < 0)
                        {
                            return Json(new
                            {
                                error = true,
                                msg = "Bạn không đủ tiền để giao dịch"
                            });
                        }
                        int id_word = Int32Ext.toInt(word.ToString());
                        if (id_word == 0)
                        {
                            continue;
                        }
                        // Lay ra so tien hien tai cua user & cong tien cho user
                        var query_money_user = db.Transactions
                            .OrderByDescending(p => p.id)
                            .Where(e => (e.user_id == id_word && e.merchant_id == wi.merchant_id && e.transactions_type == 0 && wi.wallet_out == e.wallet_id))
                            .FirstOrDefault();
                        double money = 0;
                        if (query_money_user != null)
                        {
                            money = query_money_user.balance;
                        }

                        double cashOut = cashIn / query_cashInOut.cash_in * query_cashInOut.cash_out;
                        double moneySet = money + cashOut;


                        db.Transactions.Add(new Models.Transactions()
                        {
                            user_id = userID,
                            merchant_id = wi.merchant_id,
                            payment_gate = gateID,
                            wallet_id = wi.wallet_in,
                            balance = moneyAgencyMinus,
                            type = "minus",
                            value = cashIn,
                            transactions_type = 0,
                            user_send = id_word,
                            tag = "chuyentien",
                            message = content,
                            time_creat = DateTime.Now,
                            value_out = cashOut
                        });
                        db.SaveChanges();

                        var merchant_item = db.Merchant.FirstOrDefault(m => (m.id == wi.merchant_id));
                        var user_merchant = db.User.FirstOrDefault(u => (u.username == merchant_item.username));
                        //Trừ tiền phí cố định
                        if (wi.fee != 0)
                        {
                            moneyAgencyMinus = moneyAgencyMinus - wi.fee;
                            db.Transactions.Add(new Models.Transactions()
                            {
                                user_id = userID,
                                merchant_id = wi.merchant_id,
                                payment_gate = gateID,
                                wallet_id = wi.wallet_in,
                                balance = moneyAgencyMinus,
                                type = "minus",
                                value = wi.fee,
                                transactions_type = 0,
                                user_send = user_merchant.id,
                                tag = "phicodinh",
                                message = "Trừ phí cố định qua cổng thanh toán",
                                time_creat = DateTime.Now,
                                value_out = wi.fee
                            });
                            db.SaveChanges();
                        }

                        // Bonus cash
                        db.Transactions.Add(new Models.Transactions()
                        {
                            user_id = Int32.Parse(word),
                            merchant_id = wi.merchant_id,
                            payment_gate = gateID,
                            wallet_id = wi.wallet_out,
                            balance = moneySet,
                            type = "plus",
                            value = cashOut,
                            transactions_type = 0,
                            user_send = userID,
                            tag = "chuyentien",
                            message = content,
                            time_creat = DateTime.Now,
                            value_out = cashIn
                        });
                        db.SaveChanges();


                        //Cổng tiền phế nêu cùng ví
                        double profit = cashIn - cashOut;
                        if (wi.wallet_in == wi.wallet_out && (profit > 0))
                        {

                            var query_money_user_merchant = db.Transactions
        .OrderByDescending(p => p.id)
        .Where(e => (e.user_id == user_merchant.id && e.merchant_id == wi.merchant_id && e.transactions_type == 0 && e.wallet_id == wi.wallet_out))
        .FirstOrDefault();
                            double money_merchant = 0;
                            money_merchant = query_money_user_merchant.balance + profit;
                            db.Transactions.Add(new Models.Transactions()
                            {
                                user_id = user_merchant.id,
                                merchant_id = wi.merchant_id,
                                payment_gate = gateID,
                                wallet_id = wi.wallet_out,
                                balance = money_merchant,
                                type = "plus",
                                value = profit,
                                transactions_type = 0,
                                user_send = userID,
                                tag = "phe",
                                message = "Phế nhận được",
                                time_creat = DateTime.Now,
                                value_out = profit
                            });
                            db.SaveChanges();
                        }
                        if (wi.fee != 0)
                        {
                            var query_money_user_merchant_phi = db.Transactions
      .OrderByDescending(p => p.id)
      .Where(e => (e.user_id == user_merchant.id && e.transactions_type == 0 && e.wallet_id == wi.wallet_out))
      .FirstOrDefault();
                            double money_merchant_phi = 0;
                            money_merchant_phi = query_money_user_merchant_phi.balance + wi.fee;
                            db.Transactions.Add(new Models.Transactions()
                            {
                                user_id = user_merchant.id,
                                merchant_id = wi.merchant_id,
                                payment_gate = gateID,
                                wallet_id = wi.wallet_out,
                                balance = money_merchant_phi,
                                type = "plus",
                                value = wi.fee,
                                transactions_type = 0,
                                user_send = userID,
                                tag = "phecodinh",
                                message = "Phế cố định nhận được",
                                time_creat = DateTime.Now,
                                value_out = wi.fee
                            });
                            db.SaveChanges();
                        }
                    }

                    //---connect photon
                    var demoLB = new UserPhoton(ExitGames.Client.Photon.ConnectionProtocol.Tcp);

                    var userDB = db.User.Where(e => e.id == 155).FirstOrDefault();
                    demoLB.user = userDB;
                    demoLB.uidRecieve = userID;
                    demoLB.title = "[Hệ thống] Nhận thư Hệ thống";
                    demoLB.msg = "Bạn nhận được " + Convert.ToInt32(cash) + "Coin";

                    demoLB.Connect();

                    return Json(new
                    {
                        error = false,
                        msg = "",
                        sms = 0
                    });
                }

            }

        }

        [HttpPost]
        public ActionResult TransferMoney(int gateid,int totalIn, int cash,string code, string s_user, string content)
        {
            if (gateid == null || cash == null || s_user == null || content == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var agencyID = (new AuthenticationHelper(Session).ID());


            double cashIn = cash;
            // Gate
            if (cashIn <= 0)
            {
                return Json(new
                {
                    error = true,
                    msg = "Số tiền không được <= 0"
                });
            }

            var gateID = gateid;
            using (var db = new GamePaymentContext())
            {
                ////---chống rút tiền
                //---tien`nap
                double ballanceNap = 0;

                try
                {
                    ballanceNap = db.Transactions.Where(e => (e.user_id == agencyID && e.wallet_id == 11 && e.type == "plus" && (e.payment_gate == 2066 || e.tag == "Agency+"))).Sum(e => e.balance);
                }
                catch (Exception) { }
                if (ballanceNap == 0)
                {
                    DatabaseService.Instance.getConnection().Close();
                    return Json(new
                    {
                        error = true,
                        msg = "Bạn chưa nạp tiền, nên không được rút tiền."
                    });
                }
                DatabaseService.Instance.getConnection().Close();

                //---tien` rut'
                double ballanceRut = 0;
                try
                {
                    ballanceRut = db.Transactions.Where(e => (e.user_id == agencyID && e.wallet_id == 11 && e.type == "minus" && (e.payment_gate == 2061 || e.payment_gate == 2072 || e.payment_gate == 2082))).Sum(e => e.balance);
                }
                catch (Exception) { }
                //---tien` cho merchant
                var ballanceMerchant = db.Transactions.Where(e => (e.user_send == agencyID && e.user_id == 155 && e.wallet_id == 11 && e.type == "minus" && e.payment_gate == 1054)).Sum(e => e.balance);
                if (ballanceMerchant == 0)
                {
                    DatabaseService.Instance.getConnection().Close();
                    return Json(new
                    {
                        error = true,
                        msg = "Bạn chưa chơi game, nên không được rút tiền."
                    });
                }
                DatabaseService.Instance.getConnection().Close();
                //---tien` merchant chuyen lai user
                var ballanceFromMerchant = db.Transactions.Where(e => (e.user_send == 155 && e.user_id == agencyID && e.wallet_id == 11 && e.type == "plus" && e.payment_gate == 1055)).Sum(e => e.balance);
                if (ballanceFromMerchant == 0)
                {
                    DatabaseService.Instance.getConnection().Close();
                    return Json(new
                    {
                        error = true,
                        msg = "Bạn chưa chơi game, nên không được rút tiền."
                    });
                }
                DatabaseService.Instance.getConnection().Close();
                //---tien` userToUser
                double balanceUToU = 0;
                try
                {
                    balanceUToU = db.Transactions.Where(e => (e.user_id == agencyID && e.user_send != 155 && e.wallet_id == 11 && e.payment_gate == 1058)).Sum(e => e.balance);
                }
                catch (Exception) { }
                //---tinh' toan' chong'r rut' tien`
                var sysConf = (float)ballanceMerchant / (balanceUToU + ballanceNap + ballanceFromMerchant);
                if (sysConf < 0.1)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Bạn chưa đủ điều kiện để rút tiền. Hãy liên hệ với chăm sóc khách hàng để được giải quyết!"
                    });
                }
                ////---
                
                var record_otp = db.OTP_user.Where(e => (e.otp == code && e.user_id == agencyID && e.type == 0)).FirstOrDefault();
                if (record_otp == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Mã OTP không đúng"
                    });
                }
                //Kiem tra otp quá 5 phút
                var time_creat = record_otp.time_creat;
                DateTime dateTime = DateTime.Now;
                int timeMsSinceNow = (int)dateTime.TimeOfDay.TotalMilliseconds;
                int timeMsSinceOTP = (int)time_creat.TimeOfDay.TotalMilliseconds;
                if ((timeMsSinceOTP - timeMsSinceNow) / 300 > 5)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Mã OTP đã hết hạn"
                    });
                }
                //if (!code.Trim().Equals("123456"))
                //{
                //    return Json(new
                //    {
                //        error = true,
                //        msg = "Mã OTP không đúng"
                //    });
                //}
                var wi = db.PaymentGate.FirstOrDefault(e => e.id == gateID);
                if (wi == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Cổng thanh toán không tồn tại"
                    });
                }
                if (wi.wallet_in == 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Cổng thanh toán chưa được chọn loại ví vào"
                    });
                }

                if (wi.wallet_out == 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Cổng thanh toán chưa được chọn loại ví ra"
                    });
                }

                var query_cashInOut = db.CashIORate.OrderBy(c => c.cash_in).Where(c => (c.payment_gate_id == gateID && c.cash_in >= cashIn)).FirstOrDefault();
                if (query_cashInOut == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Số tiền vượt quá config"
                    });
                }


                // Kiem tra so du tai khoan và ballance cua agency xem du tien khong
                var query = db.Transactions
                        .OrderByDescending(p => p.id)
                        .Where(e => (e.user_id == agencyID && e.transactions_type == 0 && e.wallet_id == wi.wallet_in))
                        .FirstOrDefault();
                double agencyMoney = 0;
                if (query != null)
                {
                    agencyMoney = query.balance;
                }
                if (agencyMoney < (totalIn + wi.fee))
                {
                    // hey, you are not enought money
                    throw new Exception("Ban khong du tien de giao dich");
                }
                string[] separators = { "," };
                string[] words = s_user.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                double moneyAgencyMinus = agencyMoney;
                foreach (var word in words)
                {
                    //Lấy số tiền ra
                    moneyAgencyMinus = moneyAgencyMinus - cashIn;
                    if (moneyAgencyMinus < 0)
                    {
                        throw new Exception("Ban khong du tien de giao dich");
                    } 
                    int id_word = Int32Ext.toInt(word.ToString());
                    if (id_word == 0)
                    {
                        continue;
                    }
                    // Lay ra so tien hien tai cua user & cong tien cho user
                    var query_money_user = db.Transactions
                        .OrderByDescending(p => p.id)
                        .Where(e => (e.user_id == id_word && e.transactions_type == 0 && wi.wallet_out == e.wallet_id))
                        .FirstOrDefault();
                    double money = 0;
                    if (query_money_user != null)
                    {
                        money = query_money_user.balance;
                    }

                    double cashOut = cashIn / query_cashInOut.cash_in * query_cashInOut.cash_out;
                    double moneySet = money + cashOut;

                    db.Transactions.Add(new Models.Transactions()
                    {
                        user_id = agencyID,
                        merchant_id = wi.merchant_id,
                        payment_gate = gateID,
                        wallet_id = wi.wallet_in,
                        balance = moneyAgencyMinus,
                        type = "minus",
                        value = cashIn,
                        transactions_type = 0,
                        user_send = id_word,
                        tag = "chuyentien",
                        message = content,
                        time_creat = DateTime.Now,
                        value_out = cashOut
                    });
                    db.SaveChanges();

                    var merchant_item = db.Merchant.FirstOrDefault(m => (m.id == wi.merchant_id));
                    var user_merchant = db.User.FirstOrDefault(u => (u.username == merchant_item.username));
                    //Trừ tiền phí cố định
                    if (wi.fee != 0)
                    {
                        moneyAgencyMinus = moneyAgencyMinus - wi.fee;
                        db.Transactions.Add(new Models.Transactions()
                        {
                            user_id = agencyID,
                            merchant_id = wi.merchant_id,
                            payment_gate = gateID,
                            wallet_id = wi.wallet_in,
                            balance = moneyAgencyMinus,
                            type = "minus",
                            value = wi.fee,
                            transactions_type = 0,
                            user_send = user_merchant.id,
                            tag = "phicodinh",
                            message = "Trừ phí cố định qua cổng thanh toán",
                            time_creat = DateTime.Now,
                            value_out = wi.fee
                        });
                        db.SaveChanges();
                    }


                    // Bonus cash
                    db.Transactions.Add(new Models.Transactions()
                    {
                        user_id = id_word,
                        merchant_id = wi.merchant_id,
                        payment_gate = gateID,
                        wallet_id = wi.wallet_out,
                        balance = moneySet,
                        type = "plus",
                        value = cashOut,
                        transactions_type = 0,
                        user_send = agencyID,
                        tag = "chuyentien",
                        message = content,
                        time_creat = DateTime.Now,
                        value_out = cashIn
                    });
                    db.SaveChanges();

                    //Cổng tiền phế nêu cùng ví
                    double profit = cashIn - cashOut;
                    if (wi.wallet_in == wi.wallet_out && (profit > 0))
                    {

                        var query_money_user_merchant = db.Transactions
    .OrderByDescending(p => p.id)
    .Where(e => (e.user_id == user_merchant.id && e.transactions_type == 0 && e.wallet_id == wi.wallet_out))
    .FirstOrDefault();
                        double money_merchant = 0;
                        money_merchant = query_money_user_merchant.balance + profit;
                        db.Transactions.Add(new Models.Transactions()
                        {
                            user_id = user_merchant.id,
                            merchant_id = wi.merchant_id,
                            payment_gate = gateID,
                            wallet_id = wi.wallet_out,
                            balance = money_merchant,
                            type = "plus",
                            value = profit,
                            transactions_type = 0,
                            user_send = agencyID,
                            tag = "phe",
                            message = "Phế nhận được",
                            time_creat = DateTime.Now,
                            value_out = profit
                        });
                        db.SaveChanges();
                    }
                    if (wi.fee != 0)
                    {
                        var query_money_user_merchant_phi = db.Transactions
    .OrderByDescending(p => p.id)
    .Where(e => (e.user_id == user_merchant.id && e.transactions_type == 0 && e.wallet_id == wi.wallet_out))
    .FirstOrDefault();
                        double money_merchant_phi = 0;
                        money_merchant_phi = query_money_user_merchant_phi.balance + wi.fee;
                        db.Transactions.Add(new Models.Transactions()
                        {
                            user_id = user_merchant.id,
                            merchant_id = wi.merchant_id,
                            payment_gate = gateID,
                            wallet_id = wi.wallet_out,
                            balance = money_merchant_phi,
                            type = "plus",
                            value = wi.fee,
                            transactions_type = 0,
                            user_send = agencyID,
                            tag = "phecodinh",
                            message = "Phế cố định nhận được",
                            time_creat = DateTime.Now,
                            value_out = wi.fee
                        });
                        db.SaveChanges();
                    }
                }

                return Json(new
                {
                    error = false,
                    msg = ""
                });
            }
        }

        [HttpPost]
        public ActionResult GetRateIO(string gateid)
        {
            if (gateid == null)
            {
                return Json(new
                {
                    error = true,
                    gateid = "Tham số gateid null"
                });

            }
            if (gateid == null)
            {
                return Json(new
                {
                    error = true,
                    gateid = "Cổng thanh toán không tồn tại"
                });
            }
            try
            {
                var agencyID = (new AuthenticationHelper(Session).ID());
                var gateID = Int32Ext.toInt(gateid);
                using (var db = new GamePaymentContext())
                {
                    var query = from g in db.CashIORate
                                where (g.payment_gate_id == gateID)
                                select new
                                {
                                    g.id,
                                    g.cash_in,
                                    g.cash_out,
                                };

                    // Kiem tra so du tai khoan và ballance cua agency xem du tien khong

                    var wi = db.PaymentGate.FirstOrDefault(e => e.id == gateID);
                    if (wi == null)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Cổng thanh toán không tồn tại"
                        });
                    }


                    var query_transactions = db.Transactions
                            .OrderByDescending(p => p.id)
                            .Where(e => (e.user_id == agencyID && e.transactions_type == 0 && e.wallet_id == wi.wallet_in))
                            .FirstOrDefault();
                    double agencyMoney = 0;
                    if (query_transactions != null)
                    {
                        agencyMoney = query_transactions.balance;
                    }

                    var wallet = db.Wallet.FirstOrDefault(e => e.id == wi.wallet_in);
                    if (wallet == null)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Ví không tồn tại"
                        });
                    }

                    return Json(new
                    {
                        error = false,
                        items = query.ToList(),
                        money = agencyMoney,
                        wallet_name = wallet.name + ": "
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }

        [HttpPost]
        public ActionResult GetPaymentGates()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            try
            {
                var auth = new AuthenticationHelper(Session);
                var usersID = auth.ID();
                var merchant_id = Int32Ext.toInt(auth.getMerchantID());


                using (var db = new GamePaymentContext())
                {
                    var query = from g in db.PaymentGate
                                where (g.merchant_id == merchant_id && g.status_active == "active" && g.code == "USERTTOUSER")
                                select new
                                {
                                    g.id,
                                    g.name,
                                };
                    return Json(new
                    {
                        error = false,
                        items = query.ToList()
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }

        [HttpPost]
        public ActionResult GetPaymentGateThecao()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            try
            {
                var auth = new AuthenticationHelper(Session);
                var usersID = auth.ID();
                var merchant_id = Int32Ext.toInt(auth.getMerchantID());


                using (var db = new GamePaymentContext())
                {
                    var query = from g in db.PaymentGate
                                where (g.merchant_id == merchant_id && g.status_active == "active" && g.code == "THECAO")
                                select new
                                {
                                    g.id,
                                    g.name,
                                };
                    return Json(new
                    {
                        error = false,
                        items = query.ToList()
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }

        [HttpPost]
        public ActionResult SearchUsersSelect2(String search)
        {
            if (search == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số search không tồn tại"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
            }
            catch (Exception)
            { 
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var auth = new AuthenticationHelper(Session);
            var usersID = auth.ID();
            var merchant_id = Int32Ext.toInt(auth.getMerchantID());
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT TOP 5" +
    " Users.id" +
    " ,Users.phone" +
    " ,Users.username" +
    " ,Users.email" +
    " ,Users.status_active" +
    " ,Users.verify" +
    " FROM Users WHERE Users.id != 0 AND id != @user_id AND merchant_id =@merchant_id AND status_active = 'active' AND verify = 1 AND username != '' AND (Users.username LIKE Concat('%',@search,'%') OR Users.email  LIKE Concat('%',@search,'%') OR Users.phone LIKE Concat('%',@search,'%')) ORDER BY Users.id DESC ";

            SqlCommand command;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@user_id", usersID);
            command.Parameters.AddWithValue("@merchant_id", merchant_id);
            command.Parameters.AddWithValue("@search", search);
            SqlDataReader reader = serviceBase.querySqlParam(command);
            List<ResponseSelect2> a_response = new List<ResponseSelect2>();
            while (reader.Read())
            {
                ResponseSelect2 responseSelect2 = new ResponseSelect2();
                responseSelect2.id = Int32Ext.toInt(reader["id"].ToString().Trim());
                responseSelect2.username = reader["username"].ToString().Trim();
                a_response.Add(responseSelect2);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                results = a_response
            });
        }
    }
}