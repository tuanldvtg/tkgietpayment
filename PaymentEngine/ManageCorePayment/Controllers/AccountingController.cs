﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers
{
    public class AccountingController : Controller
    {

        public ActionResult Login()
        {
            return View();
        }

        // GET: Accounting
        public ActionResult Index()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
                string subid = (new AuthenticationHelper(Session).getSub());
                ViewBag.subid = subid;
                return View("Transactions");
            }
            catch (Exception) {
                return Redirect("/Home/Login");
            }
        }

        public ActionResult Account()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
                string subid = (new AuthenticationHelper(Session).getSub());
                ViewBag.subid = subid;
                using (var db = new GamePaymentContext())
                {
                    var accountingID = (new AuthenticationHelper(Session).ID());
                    var wi = db.Admin.FirstOrDefault(e => e.id == accountingID);
                    string phone_new = wi.phone.Trim();
                    var chars = phone_new.ToCharArray();

                    string phone_response = "";
                    for (int ctr = 0; ctr < chars.Length; ctr++)
                    {
                        if (ctr < chars.Length - 4)
                        {
                            phone_response += "*";
                        }
                        else
                        {
                            phone_response += chars[ctr];
                        }
                    }

                    ViewBag.phone = phone_response;
                    return View("Account");
                }
                    
            }
            catch (Exception) {
                return Redirect("/Home/Login");
            }
        }

        public ActionResult inputoutputagencty()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
                string subid = (new AuthenticationHelper(Session).getSub());
                ViewBag.subid = subid;
                using (var db = new GamePaymentContext())
                {
                    ServiceBase serviceBase = new ServiceBase();
                    SqlDataReader reader = serviceBase.query("SELECT id,title FROM Agency WHERE status_active = 'active' ORDER BY id DESC");
                    var list_merchant = new List<Agency>();
                    while (reader.Read())
                    {
                        Agency merchant = new Agency();
                        merchant.id = Int32.Parse(reader["id"].ToString());
                        merchant.title = reader["title"].ToString();
                        list_merchant.Add(merchant);
                    }
                    reader.Close();
                    DatabaseService.Instance.getConnection().Close();
                    ViewBag.list = list_merchant;
                    return View("Inputoutputagency");
                }
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
        }
        public ActionResult Debt()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
                string subid = (new AuthenticationHelper(Session).getSub());
                if (!subid.Equals("0"))
                {
                    return Redirect("/Home/Login");
                }
                ViewBag.subid = subid;
                using (var db = new GamePaymentContext())
                {
                    ServiceBase serviceBase = new ServiceBase();
                    SqlDataReader reader = serviceBase.query("SELECT id,title FROM Agency WHERE status_active = 'active' ORDER BY id DESC");
                    var list_merchant = new List<Agency>();
                    while (reader.Read())
                    {
                        Agency merchant = new Agency();
                        merchant.id = Int32.Parse(reader["id"].ToString());
                        merchant.title = reader["title"].ToString();
                        list_merchant.Add(merchant);
                    }
                    reader.Close();
                    DatabaseService.Instance.getConnection().Close();
                    ViewBag.list = list_merchant;
                    return View("Debt");
                }
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
        }
        public ActionResult Paymentgate()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
            // Authentication Passed
            string subid = (new AuthenticationHelper(Session).getSub());
            ViewBag.subid = subid;
            if (!subid.Equals("0"))
            {
                return Redirect("/Home/Login");
            }

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            reader = serviceBase.query("SELECT id,name FROM Wallet ORDER BY id DESC");
            var list_coin = new List<Wallet>();
            while (reader.Read())
            {
                Wallet wallet = new Wallet();
                wallet.id = Int32.Parse(reader["id"].ToString());
                wallet.name = reader["name"].ToString();
                list_coin.Add(wallet);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            ViewBag.list = list_coin;


            reader = serviceBase.query("SELECT id,name,secret_key FROM Merchant ORDER BY id DESC");
            var list_merchant = new List<Merchant>();
            while (reader.Read())
            {
                Merchant merchant = new Merchant();
                merchant.id = Int32.Parse(reader["id"].ToString());
                merchant.name = reader["name"].ToString();
                merchant.secret_key = reader["secret_key"].ToString();
                list_merchant.Add(merchant);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            ViewBag.list_merchant = list_merchant;

            ViewBag.Message = "Paymentgate";

            return View();
        }

        public ActionResult inputoutsystem()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
                string subid = (new AuthenticationHelper(Session).getSub());
                ViewBag.subid = subid;
                if (!subid.Equals("0"))
                {
                    return Redirect("/Home/Login");
                }
                return View("Inoutoutputsystem");
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
        }
        public ActionResult transaction_agency()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
                string subid = (new AuthenticationHelper(Session).getSub());
                ViewBag.subid = subid;

                return View("transaction_agency");
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
        }
        public ActionResult transaction_user()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
                string subid = (new AuthenticationHelper(Session).getSub());
                ViewBag.subid = subid;
                return View("transaction_agency");
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
        }

        public ActionResult manageaccounting()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
                string subid = (new AuthenticationHelper(Session).getSub());
                ViewBag.subid = subid;
                if (!subid.Equals("0"))
                {
                    return Redirect("/Home/Login");
                }
                ServiceBase serviceBase = new ServiceBase();
                SqlDataReader reader = serviceBase.query("SELECT id,name FROM PaymentGate WHERE status_active = 'active' AND (type = 2 OR type = 3 OR type = 4) ORDER BY id DESC");
                var list_paymentgate2 = new List<PaymentGate>();
                while (reader.Read())
                { 
                    PaymentGate paymentGate = new PaymentGate();
                    paymentGate.id = Int32.Parse(reader["id"].ToString());
                    paymentGate.name = reader["name"].ToString();
                    list_paymentgate2.Add(paymentGate);
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();
                ViewBag.list_paymentgate2 = list_paymentgate2;
                return View("Manageaccounting");
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
        }

        public ActionResult Rwithdrawaagency()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
                string subid = (new AuthenticationHelper(Session).getSub());
                ViewBag.subid = subid;
                return View("Rwithdrawaagency");
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
        }
        public ActionResult Rwithdrawauser()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Accounting);
                string subid = (new AuthenticationHelper(Session).getSub());
                ViewBag.subid = subid;
                return View("Rwithdrawauser");
            }
            catch (Exception)
            {
                return Redirect("/Home/Login");
            }
        }
    }
}