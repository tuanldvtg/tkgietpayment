﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Exceptions;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers
{
    public class UserController : Controller
    {

        private bool checkPermission()
        {
            var isLogined = new AuthenticationHelper(Session).isLogined();
            return isLogined;
        }

        // GET: User
        public ActionResult Index()
        {
            try
            {
                using (var db = new GamePaymentContext())
                {
                    AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
                    ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();
                    AuthenticationHelper auth = new AuthenticationHelper(Session);
                    int id_merchant_login = Int32.Parse(auth.getMerchantID());
                    var merchant_login = db.Merchant.FirstOrDefault(e => e.id == id_merchant_login);
                    ViewBag.merchant_title = merchant_login.name;

                    return RedirectToAction("Agencyy", "User");
                }
            }
            catch (Exception)
            {
                return Redirect("/User/Login");
            }

        }

        [HttpGet]
        public ActionResult Dashboard()
        {
            try
            {

                using (var db = new GamePaymentContext())
                {
                    AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
                    ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();

                    AuthenticationHelper auth = new AuthenticationHelper(Session);
                    var list_id_user = auth.getMerchantListIDD();

                    ServiceBase serviceBase = new ServiceBase();
                    SqlDataReader reader = serviceBase.query("SELECT id,name,title FROM Merchant WHERE id IN(" + list_id_user + ") ORDER BY id DESC");
                    var list_merchant = new List<Merchant>();
                    while (reader.Read())
                    {
                        Merchant merchant = new Merchant();
                        merchant.id = Int32.Parse(reader["id"].ToString());
                        merchant.name = reader["title"].ToString();
                        if (reader["id"].ToString().Trim().Equals(auth.getMerchantID()))
                        {
                            merchant.status_active = "active";
                        }
                        else
                        {
                            merchant.status_active = "deactive";
                        }
                        list_merchant.Add(merchant);
                    }
                    DatabaseService.Instance.getConnection().Close();
                    ViewBag.list_merchant = list_merchant;

                    int id_merchant_login = Int32.Parse(auth.getMerchantID());
                    var merchant_login = db.Merchant.FirstOrDefault(e => e.id == id_merchant_login);
                    ViewBag.merchant_title = merchant_login.name;
                    return View();
                }
            }
            catch (Exception)
            {
                return Redirect("/User/Login");
            }
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        static string ConvertStringArrayToString(string[] array)
        {
            // Concatenate all the elements into a StringBuilder.
            StringBuilder builder = new StringBuilder();
            foreach (string value in array)
            {
                builder.Append(value);
                builder.Append('.');
            }
            return builder.ToString();
        }
        [HttpPost]
        public ActionResult Login(String username, String password)
        {
            if (username == null || password == null)
            {
                ViewData["Message"] = "username không tồn tại hoặc password không đúng";
                return View();
            }

            try
            {
                var loginResult = new UserPrivateServices().authentication(username, password);
                AuthenticationHelper auth = new AuthenticationHelper(Session);
                auth.assignUser(loginResult[0]);

                string[] arr_id_merchant = new string[loginResult.Count];
                for (var i = 0; i < loginResult.Count; i++)
                {
                    arr_id_merchant[i] = loginResult[i].merchant_id.ToString();
                }
                string merchant_ids = String.Join(",", arr_id_merchant);
                auth.assignMerchantListID(merchant_ids);
                return RedirectToAction("Agencyy", "User");
            }
            catch (AccountNotfoundException)
            {
                ViewData["Message"] = "Account,password không đúng hoặc đã bị admin khóa";
            }
            catch (Exception)
            {
                ViewData["Message"] = "Có lỗi không xác định!";
            }
            return View();


        }

        private string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "128.199.171.7";
        }
        [HttpGet]
        public ActionResult Input()
        {
            try
            {
                using (var db = new GamePaymentContext())
                {
                    AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
                    ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();
                    var agencyID = (new AuthenticationHelper(Session).ID());
                    var userModel = db.User.FirstOrDefault(e => e.id == agencyID);

                    var list_agency = from a in db.Agency
                                      join a_m in db.Agency_Merchant on a.id equals a_m.agency_id
                                      where a_m.merchant_id == userModel.merchant_id && a.is_appear==1
                                      select new
                                      {
                                          id = a.id,
                                          title = a.title,
                                          email = a.email,
                                          phone = a.phone
                                      };
                    List<Agency> list_social_all = new List<Agency>();
                    list_agency.ToList().ForEach(ent =>
                    {
                        Agency ag = new Agency();
                        var list_social = db.Agency_social.Where(s => (s.agency_id == ent.id && s.view_user == 1)).ToList();
                        ag.list_social = list_social;
                        ag.id = ent.id;
                        ag.title = ent.title;
                        ag.email = ent.email;
                        ag.phone = ent.phone;
                        list_social_all.Add(ag);
                    });
                    ViewBag.list_agency = list_social_all;

                    DateTime dateTime = DateTime.UtcNow.ToLocalTime().ToUniversalTime().AddHours(8);
                    int timeMsSinceMidnight = (int)dateTime.TimeOfDay.TotalMilliseconds;


                    string format = "yyyy-MM-dd hh:mm:sstt";
                    string format_re = "yyyy-MM-dd HH:mm:ss";

                    PostOnePay postOnePay = new PostOnePay();
                    postOnePay.reference = timeMsSinceMidnight.ToString();
                    postOnePay.currency = "VND";
                    postOnePay.customer = agencyID.ToString();
                    postOnePay.amount = "0.00";
                    postOnePay.datetime = dateTime.ToString(format);

                    string rep_time = dateTime.ToString(format_re);
                    rep_time = rep_time.Replace("-", "");
                    rep_time = rep_time.Replace(":", "");
                    rep_time = rep_time.Replace(" ", "");

                    postOnePay.clientIP = this.GetLocalIPAddress();
                    string md5 = String.Format("{0}{1}{2}{3}{4}{5}{6}{7}", postOnePay.merchant, postOnePay.reference, postOnePay.customer, postOnePay.amount, postOnePay.currency, rep_time, postOnePay.securityCode, postOnePay.clientIP);
                    string md5_base = CryptoHelper.Md5Base(md5);
                    postOnePay.key = md5_base.ToUpper();
                    ViewBag.postOnePay = postOnePay;


                    AuthenticationHelper auth = new AuthenticationHelper(Session);
                    int id_merchant_login = Int32.Parse(auth.getMerchantID());
                    var merchant_login = db.Merchant.FirstOrDefault(e => e.id == id_merchant_login);
                    ViewBag.merchant_title = merchant_login.name;

                    return View();
                }
            }
            catch (Exception)
            {
                return Redirect("/User/Login");
            }
        }
        [HttpGet]
        public ActionResult Rwithdrawaagency()
        {
            try
            {
                using (var db = new GamePaymentContext())
                {
                    AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
                    ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();
                    var agencyID = (new AuthenticationHelper(Session).ID());
                    var userModel = db.User.FirstOrDefault(e => e.id == agencyID);

                    var list_agency = from a in db.Agency
                                      join a_m in db.Agency_Merchant on a.id equals a_m.agency_id
                                      where a_m.merchant_id == userModel.merchant_id && a.is_appear == 1
                                      select new
                                      {
                                          id = a.id,
                                          title = a.title,
                                          email = a.email,
                                          phone = a.phone
                                      };
                    List<Agency> list_social_all = new List<Agency>();

                    list_agency.ToList().ForEach(ent =>
                    {
                        Agency ag = new Agency();
                        var list_social = db.Agency_social.Where(s => (s.agency_id == ent.id && s.view_user == 1)).ToList();
                        ag.list_social = list_social;
                        ag.id = ent.id;
                        ag.title = ent.title;
                        ag.email = ent.email;
                        ag.phone = ent.phone;
                        list_social_all.Add(ag);


                    });

                    ViewBag.list_agency = list_social_all;

                    AuthenticationHelper auth = new AuthenticationHelper(Session);
                    int id_merchant_login = Int32.Parse(auth.getMerchantID());
                    var merchant_login = db.Merchant.FirstOrDefault(e => e.id == id_merchant_login);
                    ViewBag.merchant_title = merchant_login.name;
                    return View();
                }
            }
            catch (Exception)
            {
                return Redirect("/User/Login");
            }
        }

        [HttpGet]
        public ActionResult Giftcode()
        {
            try
            {
                using (var db = new GamePaymentContext())
                {
                    AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
                    ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();

                    AuthenticationHelper auth = new AuthenticationHelper(Session);
                    var list_id_user = auth.getMerchantListIDD();

                    ServiceBase serviceBase = new ServiceBase();
                    SqlDataReader reader;

                    //---lấy chiến dịch
                    var query = "SELECT id,name,start_time,end_time FROM Campaign ORDER BY id DESC";
                    SqlCommand command = new SqlCommand(query, DatabaseService.Instance.getConnection());
                    reader = serviceBase.querySqlParam(command);
                    var list_campaign = new List<Campaign>();
                    while (reader.Read())
                    {
                        Campaign wallet = new Campaign();
                        wallet.id = Int32.Parse(reader["id"].ToString());
                        wallet.name = reader["name"].ToString();
                        wallet.start_time = DateTime.Parse(reader["start_time"].ToString());
                        wallet.end_time = DateTime.Parse(reader["end_time"].ToString());
                        list_campaign.Add(wallet);
                    }
                    DatabaseService.Instance.getConnection().Close();
                    ViewBag.list_campaign = list_campaign;
                    
                    //---lấy merchant
                    reader = serviceBase.query("SELECT id,name,title FROM Merchant ORDER BY id DESC");
                    var list_merchant = new List<Merchant>();
                    while (reader.Read())
                    {
                        Merchant merchant = new Merchant();
                        merchant.id = Int32.Parse(reader["id"].ToString());
                        merchant.title = reader["title"].ToString();
                        list_merchant.Add(merchant);
                    }
                    DatabaseService.Instance.getConnection().Close();
                    ViewBag.list_merchant = list_merchant;

                    return View("GiftcodeUser");
                }
            }
            catch (Exception)
            {
                return Redirect("/User/Login");
            }

        }

        [HttpGet]
        public ActionResult Agencyy()
        {
            try
            {
                using (var db = new GamePaymentContext())
                {
                    AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
                    ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();
                    var agencyID = (new AuthenticationHelper(Session).ID());
                    var userModel = db.User.FirstOrDefault(e => e.id == agencyID);

                    var list_agency = from a in db.Agency
                                      join a_m in db.Agency_Merchant on a.id equals a_m.agency_id
                                      where a_m.merchant_id == userModel.merchant_id && a.is_appear == 1
                                      orderby a.orderId ascending
                                      select new
                                      {
                                          id = a.id,
                                          title = a.title,
                                          email = a.email,
                                          phone = a.phone,
                                          location = a.location
                                      };
                    List<Agency> list_social_all = new List<Agency>();

                    list_agency.ToList().ForEach(ent =>
                    {
                        Agency ag = new Agency();
                        var list_social = db.Agency_social.Where(s => (s.agency_id == ent.id && s.view_user == 1)).ToList();
                        ag.list_social = list_social;
                        ag.id = ent.id;
                        ag.title = ent.title;
                        ag.email = ent.email;
                        ag.phone = ent.phone;
                        ag.location = ent.location;
                        list_social_all.Add(ag);


                        //var list_agency_subid = from a in db.Agency
                        //                        where a.subid == ent.id
                        //                        select new
                        //                        {
                        //                            id = a.id,
                        //                            title = a.title,
                        //                            email = a.email,
                        //                            phone = a.phone,
                        //                            location = a.location
                        //                        };
                        //list_agency_subid.ToList().ForEach(ent_sub =>
                        //{
                        //    Agency ag_sub = new Agency();
                        //    var list_social_sub = db.Agency_social.Where(s => (s.agency_id == ent_sub.id && s.view_user == 1)).ToList();
                        //    ag_sub.list_social = list_social_sub;
                        //    ag_sub.id = ent_sub.id;
                        //    ag_sub.title = ent_sub.title;
                        //    ag_sub.email = ent_sub.email;
                        //    ag_sub.phone = ent_sub.phone;
                        //    ag_sub.location = ent_sub.location;
                        //    list_social_all.Add(ag_sub);
                        //});

                    });

                    ViewBag.list_agency = list_social_all;

                    AuthenticationHelper auth = new AuthenticationHelper(Session);
                    int id_merchant_login = Int32.Parse(auth.getMerchantID());
                    var merchant_login = db.Merchant.FirstOrDefault(e => e.id == id_merchant_login);
                    ViewBag.merchant_title = merchant_login.name;
                    return View();
                }
            }
            catch (Exception)
            {
                return Redirect("/User/Login");
            }
        }

        [HttpGet]
        public ActionResult MobileCard()
        {
            try
            {
                using (var db = new GamePaymentContext())
                {
                    AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
                    ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();
                    var agencyID = (new AuthenticationHelper(Session).ID());
                    var userModel = db.User.FirstOrDefault(e => e.id == agencyID);
                    AuthenticationHelper auth = new AuthenticationHelper(Session);
                    int id_merchant_login = Int32.Parse(auth.getMerchantID());
                    var merchant_login = db.Merchant.FirstOrDefault(e => e.id == id_merchant_login);
                    ViewBag.merchant_title = merchant_login.name;
                    return View();
                }
            }
            catch (Exception)
            {
                return Redirect("/User/Login");
            }
        }

        [HttpGet]
        public ActionResult Transactions()
        {
            try
            {
                using (var db = new GamePaymentContext())
                {
                    AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
                    ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();
                    AuthenticationHelper auth = new AuthenticationHelper(Session);
                    int id_merchant_login = Int32.Parse(auth.getMerchantID());
                    var merchant_login = db.Merchant.FirstOrDefault(e => e.id == id_merchant_login);
                    ViewBag.merchant_title = merchant_login.name;
                    return View();
                }
            }
            catch (Exception)
            {
                return Redirect("/User/Login");
            }
        }

        [HttpGet]
        public ActionResult Account()
        {
            try
            {

                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
                ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();
                using (var db = new GamePaymentContext())
                {
                    var userID = (new AuthenticationHelper(Session).ID());
                    var wi = db.User.FirstOrDefault(e => e.id == userID);

                    ViewBag.phone = wi.phone;
                    AuthenticationHelper auth = new AuthenticationHelper(Session);
                    int id_merchant_login = Int32.Parse(auth.getMerchantID());
                    var merchant_login = db.Merchant.FirstOrDefault(e => e.id == id_merchant_login);
                    ViewBag.merchant_title = merchant_login.name;
                    return View("Account");
                }

            }
            catch (Exception)
            {
                return Redirect("/User/Login");
            }
        }


        [HttpGet]
        public ActionResult SelectMerchant()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Users);
                ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();
                using (var db = new GamePaymentContext())
                {
                    AuthenticationHelper auth = new AuthenticationHelper(Session);
                    var list_id_user = auth.getMerchantListIDD();

                    ServiceBase serviceBase = new ServiceBase();
                    SqlDataReader reader = serviceBase.query("SELECT id,name FROM Merchant WHERE id IN(" + list_id_user + ") ORDER BY id DESC");
                    var list_merchant = new List<Merchant>();
                    while (reader.Read())
                    {
                        Merchant merchant = new Merchant();
                        merchant.id = Int32.Parse(reader["id"].ToString());
                        merchant.name = reader["name"].ToString();
                        if (reader["id"].ToString().Trim().Equals(auth.getMerchantID()))
                        {
                            merchant.status_active = "active";
                        }
                        else
                        {
                            merchant.status_active = "deactive";
                        }
                        list_merchant.Add(merchant);
                    }
                    DatabaseService.Instance.getConnection().Close();
                    ViewBag.list_merchant = list_merchant;

                    return View();
                }

            }
            catch (Exception)
            {
                return Redirect("/User/Login");
            }
        }

    }
}