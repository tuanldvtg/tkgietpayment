﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers
{
    public class MerchantController : Controller
    {
        // GET: Merchant
        public ActionResult Index()
        {
            return View();
        }

        //Danh sách user theo datatable
        [HttpPost]
        public ActionResult Listdata(String s_seach, int iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            string[] a_sort = new string[] { "id", "name", "secret_key", "username", "email", "id", "id", "id", "id"};
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT" +
                " id" +
                " ,name" +
                " ,secret_key" +
                " ,username" +
                " ,email" +
                " ,status_active"+
                " FROM Merchant ";

            string queryCount = "SELECT COUNT(id) as 'count' FROM Merchant";

            queryString += " ORDER BY " + a_sort[iSortCol_0] + " " + sSortDir_0 + " OFFSET " + iDisplayStart + " ROWS FETCH NEXT " + iDisplayLength + " ROWS ONLY";
            SqlDataReader reader = serviceBase.query(queryString);

            List<String[]> users = new List<String[]>();
            while (reader.Read())
            {
                string[] s_user = new string[9];
                s_user[0] = reader["id"].ToString();
                s_user[1] = reader["name"].ToString();
                s_user[2] = reader["secret_key"].ToString();
                s_user[3] = reader["username"].ToString();
                s_user[4] = reader["email"].ToString();
                s_user[5] = "<a onclick=\"merchant.showEditType(" + reader["id"].ToString() + ")\" >Danh sách</a>";
                s_user[6] = "<a data-active=\"" + reader["status_active"].ToString() + "\" onclick=\"merchant.showEditActive(" + reader["id"].ToString() + ",this)\">" + reader["status_active"].ToString() + "</a>";

                s_user[7] = "<button onclick=\"merchant.showEdit('" + reader["id"].ToString() + "')\" type=\"button\" class=\"btn btn-default btn-xs\">Sửa</button><button onclick=\"merchant.showDelete('" + reader["id"].ToString() + "')\"  type=\"button\" class=\"btn btn-danger btn-xs\">Xóa</button>";
   
                users.Add(s_user);
            }
            DatabaseService.Instance.getConnection().Close();

            int row = 0;
            reader = serviceBase.query(queryCount);
            while (reader.Read())
            {
                row = Int32.Parse(reader["count"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength
            });
        }
        //Load get List Payment
        [HttpPost]
        public ActionResult loadPaymentGate(string id)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string queryStringListPaymentGate = "SELECT id,name,code,status_active,(SELECT TOP 1 name FROM Wallet WHERE id = PaymentGate.wallet_in) as 'wallet_in',(SELECT TOP 1 name FROM Wallet WHERE id = PaymentGate.wallet_out) as 'wallet_out' FROM PaymentGate WHERE merchant_id ='" + id+"' AND agency_id = 0 ORDER BY id DESC";
            reader = serviceBase.query(queryStringListPaymentGate);
            List<PaymentGate> a_wallet = new List<PaymentGate>();
            while (reader.Read())
            {
                PaymentGate serviceWalletPayment = new PaymentGate();
                serviceWalletPayment.id =Int32.Parse(reader["id"].ToString().Trim());
                serviceWalletPayment.wallet_name = reader["wallet_out"].ToString().Trim();
                serviceWalletPayment.wallet_name_in = reader["wallet_in"].ToString().Trim();
                serviceWalletPayment.code = reader["code"].ToString().Trim();
                serviceWalletPayment.name = reader["name"].ToString().Trim();
                serviceWalletPayment.status_active = reader["status_active"].ToString().Trim();
                a_wallet.Add(serviceWalletPayment);
            }
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                error = false,
                items = a_wallet
            });
        }


        [HttpPost]
        public ActionResult loadPaymentGate2(string id)
        {
            return Json(new
            {
                error = false
            });
        }
        //Load Wallet
        [HttpPost]
        public ActionResult loadWallet(int id)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string queryString = "SELECT wallet_id,(SELECT name FROM Wallet WHERE id = Merchant_wallet.wallet_id) as 'name' FROM Merchant_wallet WHERE merchant_id ='" + id + "' ";
            reader = serviceBase.query(queryString);
            List<string> a_wallet_id = new List<string>();
            List<string[]> a_wallet_ob = new List<string[]>();
            while (reader.Read())
            {
                string[] s = new string[2];
                s[0] = reader["wallet_id"].ToString();
                s[1] = reader["name"].ToString();
                a_wallet_id.Add(reader["wallet_id"].ToString());
                a_wallet_ob.Add(s);
            }
            DatabaseService.Instance.getConnection().Close();
            string s_id = "0";
            if (a_wallet_id.Count > 0)
            {
                 s_id = string.Join(",", a_wallet_id);
            }
            
            string queryPaymentGate = "SELECT * FROM PaymentGate WHERE (wallet_in IN ("+ s_id + ") OR wallet_in = 0) AND (wallet_out IN (" + s_id + ") OR wallet_out = 0) AND (agency_id = 0 OR agency_id IS NULL) AND copyid = 0 AND type IN (0,1,2) ORDER BY id DESC";
            List<PaymentGate> list_payment = new List<PaymentGate>();
            reader = serviceBase.query(queryPaymentGate);
            while (reader.Read())
            {
                PaymentGate paymentGate = new PaymentGate();
                paymentGate.id = Int32.Parse(reader["id"].ToString().Trim());
                paymentGate.code = reader["code"].ToString().Trim();
                paymentGate.name = reader["name"].ToString().Trim();
                list_payment.Add(paymentGate);
            }
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false,
                items = a_wallet_id,
                items_object = a_wallet_ob,
                list_paymentGate = list_payment
            });
        }

        //Update Wallet
        [HttpPost]
        public ActionResult updateWallet(int id,string wallet_ids)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            //Update user
            string queryStringUpdateUser = "DELETE FROM Merchant_wallet WHERE merchant_id = '" + id + "'";
            reader = serviceBase.query(queryStringUpdateUser);
            DatabaseService.Instance.getConnection().Close();

            string[] strArrayOne = new string[] { "" };
            //somewhere in your code
            strArrayOne = wallet_ids.Split(',');
            foreach (string element in strArrayOne)
            {
                string queryStringInsertLog = "INSERT INTO Merchant_wallet(merchant_id,wallet_id) VALUES ('" + id + "','" + element + "')";
                reader = serviceBase.query(queryStringInsertLog);
                DatabaseService.Instance.getConnection().Close();
            }

            return Json(new
            {
                error = false
            });
        }

        [HttpPost]
        public ActionResult addPaymentGate(string wallet_out, string wallet_in, string merchant_id, string paymentgate_id)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }

            int walletOut = 0;
            int walletIn = 0;

            if (!wallet_out.Trim().Equals(""))
            {
                 walletOut = Int32.Parse(wallet_out);
            }

            if (!wallet_in.Trim().Equals(""))
            {
                walletIn = Int32.Parse(wallet_in);
            }
          
            
            int merchantId = Int32.Parse(merchant_id);
            int paymentgateID = Int32.Parse(paymentgate_id);

            using (var db = new GamePaymentContext())
            {
                

                var paymentgate =  db.PaymentGate.FirstOrDefault(x => x.id == paymentgateID);

                var paymentgatecheck = db.PaymentGate.FirstOrDefault(x => (x.code == paymentgate.code && x.merchant_id == merchantId));
                if (paymentgatecheck != null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Cổng thanh toán này đã được tạo trong merchant."
                    });
                }

                
                if (paymentgate.merchant_id ==0)
                {
                    paymentgate.merchant_id = merchantId;
                    paymentgate.wallet_in = walletIn;
                    paymentgate.wallet_out = walletOut;
                    db.SaveChanges();
                }
                else
                {
                    var inst = new PaymentGate
                    {
                        name = paymentgate.name.Trim(),
                        code = paymentgate.code.Trim(),
                        wallet_in = paymentgate.wallet_in,
                        wallet_out = paymentgate.wallet_out,
                        merchant_id = merchantId,
                        status_active = "active",
                        copyid = paymentgate.id,
                        agency_id = 0,
                        sms = paymentgate.sms
                    };
                    db.PaymentGate.Add(inst);
                    db.SaveChanges();

                    //Sao chép config
                    var query = from c in db.CashIORate
                                where c.payment_gate_id == paymentgateID
                                select new
                                {
                                    c.id,
                                    c.payment_gate_id,
                                    c.cash_out,
                                    c.cash_in,
                                };

                    query.ToList().ForEach(ent =>
                    {
                        var cashIORate = new CashIORate
                        {
                            payment_gate_id = inst.id,
                            cash_out = ent.cash_out,
                            cash_in = ent.cash_in
                        };
                        db.CashIORate.Add(cashIORate);
                        db.SaveChanges();
                    });

                }
            }
            return Json(new
            {
                error = false
            });
        }

        private Boolean check_delete_merchant(int id)
        {
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT TOP 2 id FROM Users WHERE merchant_id = '" + id + "'";

            SqlDataReader reader = serviceBase.query(queryString);

            int count = 0;
            while (reader.Read())
            {
                count++;
            }
            DatabaseService.Instance.getConnection().Close();
            if (count > 1)
            {
                return false;
            }
            return true;
        }


        private Boolean check_delete_paymentgate(int id)
        {
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT TOP 1 * FROM Transactions WHERE payment_gate = '" + id + "'";

            SqlDataReader reader = serviceBase.query(queryString);

            int count = 0;
            while (reader.Read())
            {
                count++;
            }
            DatabaseService.Instance.getConnection().Close();
            if (count > 0)
            {
                return false;
            }
            return true;
        }

        [HttpPost]
        public ActionResult removePaymentGate(int id)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            if (!check_delete_paymentgate(id))
            {
                return Json(new
                {
                    error = true,
                    msg = "Cổng thanh toán đã giao dịch không thể xóa."
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string queryStringUpdateUser = "DELETE FROM PaymentGate WHERE id = '" + id + "'";
            reader = serviceBase.query(queryStringUpdateUser);
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false
            });
        }
        [HttpPost]
        public ActionResult updateStatusPaymentGate(int id,string status_active)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string queryStringUpdateUser = "UPDATE PaymentGate SET status_active = '"+ status_active + "' WHERE id = '" + id + "'";
            reader = serviceBase.query(queryStringUpdateUser);
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                error = false
            });
        }

        //Edit user active/deactive
        [HttpPost]
        public ActionResult postActive(int id, string status, string note)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            //Update user
            string queryStringUpdateUser = "UPDATE Merchant SET status_active='" + status + "' WHERE id = '" + id + "'";
            reader = serviceBase.query(queryStringUpdateUser);
            DatabaseService.Instance.getConnection().Close();
            //Insert Log
            string queryStringInsertLog = "INSERT INTO ActivityLog(accountid,userid,type,note,type_action) VALUES ('" + Session["UserID"] + "','" + id + "','" + status + "',N'" + note + "','merchant')";
            reader = serviceBase.query(queryStringInsertLog);
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false
            });
        }


        [HttpPost]
        public ActionResult loadPaymentGateConfig(int id)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string queryGatePayment = "SELECT * FROM PaymentGate WHERE id = '" + id + "'";
            reader = serviceBase.query(queryGatePayment);
            string name_payment_gate = "";
            while (reader.Read())
            {
                name_payment_gate = reader["name"].ToString().Trim();
            }
            DatabaseService.Instance.getConnection().Close();

            string queryStringListPaymentGate = "SELECT * FROM CashIORate WHERE payment_gate_id = '"+ id + "'";
            reader = serviceBase.query(queryStringListPaymentGate);
            List<CashIORate> list_cash = new List<CashIORate>();
            while (reader.Read()) {
                CashIORate cashIORate = new CashIORate();
                cashIORate.id = Int32.Parse(reader["id"].ToString().Trim());
                cashIORate.cash_in = Double.Parse(reader["cash_in"].ToString().Trim());
                cashIORate.cash_out = Double.Parse(reader["cash_out"].ToString().Trim());
                list_cash.Add(cashIORate);
            }
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false,
                items = list_cash,
                name  = name_payment_gate
            });
        }

        [HttpPost]
        public ActionResult addConfigPaymentGate(int cash_in, int cash_out,int payment_gate_id)
        {
            if (Session["UserID"] == null)
            {
                return Json(new 
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string queryStringInsertLog = "INSERT INTO CashIORate(payment_gate_id,cash_in,cash_out) " +
            "VALUES ('" + payment_gate_id + "','" + cash_in + "','" + cash_out + "')";
            reader = serviceBase.query(queryStringInsertLog);
            DatabaseService.Instance.getConnection().Close();


            string queryStringListPaymentGate = "SELECT TOP 1 " +
                    "* FROM CashIORate WHERE payment_gate_id ='" + payment_gate_id + "' ORDER BY id DESC";
            reader = serviceBase.query(queryStringListPaymentGate);

            CashIORate cashIORate = new CashIORate();
            while (reader.Read())
            {
                cashIORate.id = Int32.Parse(reader["id"].ToString());
                cashIORate.cash_in = Int32.Parse(reader["cash_in"].ToString());
                cashIORate.cash_out = Int32.Parse(reader["cash_out"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false,
                item = cashIORate
            });
        }

        [HttpPost]
        public ActionResult removeConfigPaymentGate(int id)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string queryStringUpdateUser = "DELETE FROM CashIORate WHERE id = '" + id + "'";
            reader = serviceBase.query(queryStringUpdateUser);
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                error = false
            });
        }

        [HttpPost]
        public ActionResult loadWalletPaymentGate(string id)
        {
            int id_new = Int32Ext.toInt(id);


            using (var db = new GamePaymentContext())
            {
                var paymentGate = db.PaymentGate
                        .OrderByDescending(p => p.id)
                        .Where(e => (e.id == id_new))
                        .FirstOrDefault();
                return Json(new
                {
                    error = false,
                    wallet_in = paymentGate.wallet_in,
                    wallet_out = paymentGate.wallet_out,
                    type = paymentGate.type
                });

            }


        }


        public bool validateUsername(string username)
        {
            Regex regex = new Regex("^[a-z0-9_-]{3,24}$");
            return regex.IsMatch(username);
        }

        public bool validateEmail(string email)
        {
            Regex regex = new Regex("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
            return regex.IsMatch(email);
        }

        public bool validatePassword(string password)
        {
            if (password.Length > 5)
            {
                return true;
            }
            return false;
        }

        public bool validatePhone(string phone)
        {
            if (phone.Length > 11 || phone.Length < 10)
            {
                return false;
            }
            Regex regex = new Regex(@"^-*[0-9,\.?\-?\(?\)?\ ]+$");
            return regex.IsMatch(phone);
        }

        [HttpPost]
        public ActionResult add(string title,string name,string secret_key,string phone,string password,string email,string username,string wallet_ids)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            if (!this.validateUsername(username))
            {
                return Json(new
                {
                    error = true,
                    msg = "Username không hợp lệ (Không chứa ký tự đặc biểt > 3, < 24 ký tự)."
                });
            }
            if (!this.validatePassword(password))
            {
                return Json(new
                {
                    error = true,
                    msg = "Password không hợp lệ ( > 6 ký tự)."
                });
            }
            if (!this.validatePhone(phone))
            {
                return Json(new
                {
                    error = true,
                    msg = "Điện thoại không đúng định dạng."
                });
            }
            if (!this.validateEmail(email))
            {
                return Json(new
                {
                    error = true,
                    msg = "Email không đúng định dạng."
                });
            }
        

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string queryCheckMerchant = "SELECT TOP 1 id FROM Merchant WHERE username = '" + username + "'";
            reader = serviceBase.query(queryCheckMerchant);
            int count_mertchant = 0;
            while (reader.Read())
            {
                count_mertchant++;
            }
            DatabaseService.Instance.getConnection().Close();
            if (count_mertchant > 0)
            {
                return Json(new
                {
                    error = true,
                    msg = "Không thể đặt username này."
                });
            }


            using (var db = new GamePaymentContext())
            {
                var inst = new Merchant
                {
                    name = name,
                    title = title,
                    secret_key = secret_key,
                    username = username,
                    phone = phone,
                    password = MD5Hash(password),
                    email = email,
                    status_active = "active"
                };
                db.Merchant.Add(inst);
                db.SaveChanges();
                var user_new = new Users
                {
                    username = username,
                    merchant_id = inst.id,
                    phone = phone,
                    password = MD5Hash(password),
                    email = email,
                    isActive = EnumUserModelActiveStatus.active,
                    verify = 1,
                    status_active = "active"
                };
                db.User.Add(user_new);
                db.SaveChanges();

                
                string[] strArrayOne = new string[] { "" };
                strArrayOne = wallet_ids.Split(',');
                foreach (string element in strArrayOne)
                {
                    string queryInsertWallet = "INSERT INTO Merchant_wallet(merchant_id,wallet_id) VALUES("+ inst.id + ","+ element + ") ";
                    reader = serviceBase.query(queryInsertWallet);
                    DatabaseService.Instance.getConnection().Close();
                }
            }

            return Json(new
            {
                error = false
            });
        }
        [HttpPost]
        public ActionResult edit(string title, string name, string secret_key, string phone, string password, string email,string username,int id, string wallet_ids)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            if (!this.validateUsername(username))
            {
                return Json(new
                {
                    error = true,
                    msg = "Username không hợp lệ (Không chứa ký tự đặc biểt > 3, < 24 ký tự)."
                });
            }
            if (!password.Trim().Equals(""))
            {
                if (!this.validatePassword(password))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Password không hợp lệ ( > 6 ký tự)."
                    });
                }
            }

            if (!this.validatePhone(phone))
            {
                return Json(new
                {
                    error = true,
                    msg = "Điện thoại không đúng định dạng."
                });
            }
            if (!this.validateEmail(email))
            {
                return Json(new
                {
                    error = true,
                    msg = "Email không đúng định dạng."
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            string queryStringt = "UPDATE Merchant SET title='"+ title + "',name='" + name + "',secret_key='" + secret_key + "',username ='"+ username + "',phone='" + phone + "',email='" + email + "' " +
                "WHERE id = "+ id;
            reader = serviceBase.query(queryStringt);
            DatabaseService.Instance.getConnection().Close();

            if (!password.Trim().Equals(""))
            {
                string queryStringUpdatePass = "UPDATE Merchant SET password='" + MD5Hash(password) + "' " +
                "WHERE id = " + id;
                reader = serviceBase.query(queryStringUpdatePass);
                DatabaseService.Instance.getConnection().Close();
            }

            //Update user
            string queryStringUpdateUser = "DELETE FROM Merchant_wallet WHERE merchant_id = '" + id + "'";
            reader = serviceBase.query(queryStringUpdateUser);
            DatabaseService.Instance.getConnection().Close();

            string[] strArrayOne = new string[] { "" };
            //somewhere in your code
            strArrayOne = wallet_ids.Split(',');
            foreach (string element in strArrayOne)
            {
                string queryStringInsertLog = "INSERT INTO Merchant_wallet(merchant_id,wallet_id) VALUES ('" + id + "','" + element + "')";
                reader = serviceBase.query(queryStringInsertLog);
                DatabaseService.Instance.getConnection().Close();
            }


            return Json(new
            {
                error = false
            });
        }
        [HttpPost]
        public ActionResult delete(int id)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            if (!check_delete_merchant(id))
            {
                return Json(new
                {
                    error = true,
                    msg = "Merchant đã có user không thể xóa."
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            string queryStringInsertLog = "DELETE FROM Merchant WHERE id = " + id;
            SqlDataReader reader = serviceBase.query(queryStringInsertLog);
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false
            });

        }

        [HttpPost]
        public ActionResult detail(int id)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT * FROM Merchant WHERE id = "+ id;

            SqlDataReader reader = serviceBase.query(queryString);

            Merchant merchant = new Merchant();
            while (reader.Read())
            {
                merchant.id =Int32.Parse(reader["id"].ToString());
                merchant.name = reader["name"].ToString().Trim();
                merchant.secret_key = reader["secret_key"].ToString().Trim();
                merchant.title = reader["title"].ToString().Trim();
                merchant.username = reader["username"].ToString().Trim();
                merchant.phone = reader["phone"].ToString().Trim();
                merchant.email = reader["email"].ToString().Trim();
                merchant.status_active = reader["status_active"].ToString().Trim();
            }
            DatabaseService.Instance.getConnection().Close();

            queryString = "SELECT wallet_id,(SELECT name FROM Wallet WHERE id = Merchant_wallet.wallet_id) as 'name' FROM Merchant_wallet WHERE merchant_id ='" + id + "' ";
            reader = serviceBase.query(queryString);
            List<string> a_wallet_id = new List<string>();
            while (reader.Read())
            {
                string[] s = new string[2];
                s[0] = reader["wallet_id"].ToString();
                s[1] = reader["name"].ToString();
                a_wallet_id.Add(reader["wallet_id"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                error = false,
                item = merchant,
                wallet_id = a_wallet_id
            });
        }

        public string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
    }
}