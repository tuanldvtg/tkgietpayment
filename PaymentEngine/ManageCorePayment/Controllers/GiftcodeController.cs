﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers
{
    public class GiftcodeController : Controller
    {
        // GET: Giftcode
        public ActionResult Index()
        {
            return View();
        }

        static string ConvertStringArrayToStringJoin(string[] array)
        {
            // Use string Join to concatenate the string elements.
            string result = string.Join(".", array);
            return result;
        }
        [HttpPost]
        public ActionResult WalletsIn()
        {
            try
            {
                var acountingID = (new AuthenticationHelper(Session).ID());
                using (var db = new GamePaymentContext())
                {
                    var query = from e in db.Wallet
                                select new
                                {
                                    e.id,
                                    e.name
                                };
                    return Json(new
                    {
                        error = false,
                        items = query.ToList()
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.ToString()
                });
            }
        }

        [HttpPost]
        public ActionResult Listdata()
        {
           
            using (var db = new GamePaymentContext())
            {
                DateTime datetime = DateTime.Now;
                string[] a_sort = new string[] { "id", "name", "start_time", "end_time", "merchant_id", "" };
                ServiceBase serviceBase = new ServiceBase();
                string queryString = "SELECT" +
                    " id" +
                    " ,name" +
                    " ,start_time" +
                    " ,end_time" +
                    " ,merchant_id" +
                    " FROM Campaign";

                string queryCount = "SELECT COUNT(id) as 'count' FROM Campaign";

                //queryString += " ORDER BY " + a_sort[Int32.Parse(iSortCol_0)] + " " + sSortDir_0 + " OFFSET " + iDisplayStart + " ROWS FETCH NEXT " + iDisplayLength + " ROWS ONLY";
                SqlDataReader reader = serviceBase.query(queryString);

                List<String[]> campaigns = new List<String[]>();

                while (reader.Read())
                {
                    string[] campaign = new string[7];
                    campaign[0] = reader["id"].ToString().Trim();
                    campaign[1] = reader["name"].ToString().Trim();
                    campaign[2] = reader["start_time"].ToString().Trim();
                    campaign[3] = reader["end_time"].ToString().Trim();
                    campaign[4] = reader["merchant_id"].ToString().Trim();
                    campaign[5] = "<button onclick=\"giftcode.showGiftcode(" + Int32Ext.toInt(reader["id"].ToString()) + ")\" type=\"button\" class=\"btn btn-success \">Chi tiết</button><button onclick=\"giftcode.deleteCampaign('" + Int32Ext.toInt(reader["id"].ToString()) + "')\" type=\"button\" class=\"btn btn-danger \">Xóa chiển dịch</button>";

                    campaigns.Add(campaign);
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                int row = 0;

                reader = serviceBase.query(queryCount);
                while (reader.Read())
                {
                    row = Int32.Parse(reader["count"].ToString());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                return Json(new
                {
                    aaData = campaigns,
                    iTotalRecords = row,
                    iTotalDisplayRecords = row,
                    display = 10
                });
            }
        }


        [HttpPost]
        public ActionResult deleteCampaign(int id)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            if (id == 0)
            {
                return Json(new
                {
                    error = true,
                    msg = "Id không tồn tại"
                });
            }
            using (var db = new GamePaymentContext())
            {
                var giftcode = db.Giftcode.FirstOrDefault(c => c.campaign_id == id);
                if (giftcode != null)
                {
                    db.Giftcode.Remove(giftcode);
                }
               
                var campain = db.Campaign.FirstOrDefault(c => c.id == id);
                if (campain != null)
                {
                    db.Campaign.Remove(campain);
                }
                
                return Json(new
                {
                    error = false,
                    msg = "Xóa chiến dịch thành công"
                });

            }
     
        }

        [HttpPost]
        public ActionResult deleteGiftcode(int id)
        {
            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            if (id == 0)
            {
                return Json(new
                {
                    error = true,
                    msg = "Id không tồn tại"
                });
            }
            using (var db = new GamePaymentContext())
            {
                var giftcode = db.Giftcode.FirstOrDefault(c => c.id == id);
                db.Giftcode.Remove(giftcode);
                return Json(new
                {
                    error = false,
                    msg = "Thực hiện thành công"
                });
            }
        }


        [HttpPost]
        public ActionResult ListdataGiftcode(int id)
        {
            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            if (id == 0)
            {
                return Json(new
                {
                    error = true,
                    msg = "Id không tồn tại"
                });
            }
            using (var db = new GamePaymentContext())
            {
                DateTime datetime = DateTime.Now;
                string[] a_sort = new string[] { "id", "code", "type_giftcode", "value", "is_active", "create_time", "approved_time", "end_time", "user_id" };
                ServiceBase serviceBase = new ServiceBase();
                string queryString = "SELECT" +
                    " id" +
                    " ,code" +
                    " ,type_giftcode" +
                    " ,value" +
                    " ,is_active" +
                    " ,create_time" +
                    " ,approved_time" +
                    " ,expired_time" +
                    " ,user_id" +
                    ",(SELECT top 1 username FROM Users WHERE id = Giftcode.user_id) as 'username'" +
                    " FROM Giftcode WHERE campaign_id=@id";

                string queryCount = "SELECT COUNT(id) as 'count' FROM Giftcode WHERE campaign_id=@id";
                //queryString += " ORDER BY " + a_sort[Int32.Parse(iSortCol_0)] + " " + sSortDir_0 + " OFFSET " + iDisplayStart + " ROWS FETCH NEXT " + iDisplayLength + " ROWS ONLY";
                SqlCommand command;
                command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = serviceBase.querySqlParam(command);
                
                List<String[]> campaigns = new List<String[]>();

                while (reader.Read())
                {
                    string[] campaign = new string[10];
                    campaign[0] = reader["id"].ToString().Trim();
                    campaign[1] = reader["code"].ToString().Trim();
                    campaign[2] = reader["type_giftcode"].ToString().Trim();
                    campaign[3] = reader["value"].ToString();
                    campaign[4] = reader["is_active"].ToString();
                    campaign[5] = reader["create_time"].ToString();
                    campaign[6] = reader["approved_time"].ToString();
                    campaign[7] = reader["expired_time"].ToString();
                    //var idUser = reader["user_id"].ToString();
                    //Users user = new Users();
                    //var reader1 = serviceBase.query("SELECT username FROM Users WHERE id=" + idUser + "");
                    //while (reader1.Read())
                    //{
                    //    user.username = reader1["username"].ToString().Trim();
                    //}
                    campaign[8] = /*user.username; //*/reader["username"].ToString();
                    campaign[9] = "<button onclick=\"giftcode.deleteGiftcode('" + Int32Ext.toInt(reader["id"].ToString()) + "')\" type=\"button\" class=\"btn btn-danger \">Xóa</button>";
                    campaigns.Add(campaign);
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                int row = 0;
                command = new SqlCommand(queryCount, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@id", id);
                reader = serviceBase.querySqlParam(command);
                while (reader.Read())
                {
                    row = Int32.Parse(reader["count"].ToString());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();

                return Json(new
                {
                    aaData = campaigns,
                    iTotalRecords = row,
                    iTotalDisplayRecords = row,
                    display = 10
                });
            }
        }

        [HttpPost]
        public ActionResult addCampaign(string name, string start_time, string end_time, int merchant_id)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            using (var db = new GamePaymentContext())
            {
                var campaign = db.Campaign.FirstOrDefault(c => c.name == name);
                if (campaign != null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Trùng tên chiến dịch"
                    });
                }
                db.Campaign.Add(new Campaign()
                {
                    name = name,
                    start_time = DateTime.Parse(start_time),
                    end_time = DateTime.Parse(end_time),
                    merchant_id = merchant_id
                });
                db.SaveChanges();
            }

            return Json(new
            {
                error = false,
                msg = "Đã thêm chiến dịch thành công."
            });
        }

        private bool validateUsername(string username)
        {
            Regex regex = new Regex("^[a-z0-9_-]{3,24}$");
            return regex.IsMatch(username);
        }


        private bool validatePassword(string password)
        {
            if (password.Length > 5)
            {
                return true;
            }
            return false;
        }

        //[HttpPost]
        //public ActionResult addBot(string username, string password)
        //{

        //    Users campaign = new Users();
        //    ServiceBase serviceBase = new ServiceBase();
        //    SqlDataReader reader;
        //    if (username == null)
        //    {
        //        return Json(new
        //        {
        //            error = true,
        //            code = 1,
        //            msg = "Username không hợp lệ (Không chứa ký tự đặc biểt > 3, < 24 ký tự)"
        //        });
        //    }
        //    if (password == null)
        //    {
        //        return Json(new
        //        {
        //            error = false,
        //            code = 2,
        //            msg = "Password không hợp lệ ( > 6 ký tự)"
        //        });
        //    }
        //    if (!this.validateUsername(username))
        //    {
        //        return Json(new
        //        {
        //            error = true,
        //            code = 1,
        //            msg = "Username không hợp lệ (Không chứa ký tự đặc biểt > 3, < 24 ký tự)"
        //        });
        //    }

        //    if (!validatePassword(password))
        //    {
        //        return Json(new
        //        {
        //            error = false,
        //            code = 2,
        //            msg = "Password không hợp lệ ( > 6 ký tự)"
        //        });
        //    }


        //    //Kiểm tra username đã đăng ký chưa
        //    reader = serviceBase.query("SELECT TOP 1 id FROM Users WHERE username = '" + username + "' AND merchant_id = '" + 4 + "' ");

        //    int count_user = 0;
        //    while (reader.Read())
        //    {
        //        count_user++;
        //    }
        //    DatabaseService.Instance.getConnection().Close();
        //    if (count_user > 0)
        //    {
        //        //User đã tồn tại
        //        return Json(new
        //        {
        //            error = false,
        //            code = 4,
        //            msg = "Username đã được đăng ký"
        //        });
        //    }

        //    reader = serviceBase.query("INSERT INTO Users (username,password,merchant_id,status_active,verify,verify_phone) " +
        //        "VALUES('" + username + "','" + password + "',4,'active',1,1");
        //    DatabaseService.Instance.getConnection().Close();

        //    //Lấy info
        //    reader = serviceBase.query("SELECT TOP 1 * FROM Users WHERE username='" + username + "' AND merchant_id = 4");

        //    Users users = new Users();
        //    while (reader.Read())
        //    {
        //        users.id = Int32.Parse(reader["id"].ToString());
        //        users.username = reader["username"].ToString();
        //        if (reader["status_active"].ToString() == "active")
        //        {
        //            users.isActive = EnumUserModelActiveStatus.active;
        //        }
        //        else
        //        {
        //            users.isActive = EnumUserModelActiveStatus.deactive;
        //        }
        //        users.verify = Int32.Parse(reader["verify"].ToString());
        //    }
        //    DatabaseService.Instance.getConnection().Close();

        //    reader = serviceBase.query("UPDATE Users SET password='" + CryptoHelper.MD5Hash(password, users.id.ToString()) + "' WHERE id = '" + users.id + "' AND merchant_id =4");
        //    DatabaseService.Instance.getConnection().Close();

        //    string token_gen = TokenHelper.GenerateToken(users.id.ToString());
        //    return Json(new
        //    {
        //        error = false,
        //        code = 0,
        //        token = token_gen,
        //        user = users,
        //        msg = "Tạo bot thành công!!!"
        //    });
        //}

        [HttpPost]
        public ActionResult addGiftcode(int amountGC, int campaign_id, int value)
        {
            using (var db = new GamePaymentContext())
            {
                if (Session["UserID"] == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Vui lòng đăng nhập lại."
                    });
                }
                if (value < 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Tiền giftcode không được âm."
                    });
                }

                var accountingID = (new AuthenticationHelper(Session).ID());
                Giftcode giftcode = new Giftcode();
                ServiceBase serviceBase = new ServiceBase();
                SqlDataReader reader;


                var campaign = db.Campaign.FirstOrDefault(c => c.id == campaign_id);

                //---gen giftcode
                int vouchersToGenerate = amountGC;
                int lengthOfVoucher = 10;


                List<string> generatedVouchers = new List<string>();
                char[] keys = "ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890".ToCharArray();
                //string format = "yyyy-MM-dd HH:mm:ss";
                var time = DateTime.Now;
                while (generatedVouchers.Count < vouchersToGenerate)
                {
                    var voucher = CryptoHelper.GenerateVoucher(keys, lengthOfVoucher);
                    if (!generatedVouchers.Contains(voucher))
                    {
                        try
                        {

                            //---add DB SQL
                            //string queryStringInsertLog = "INSERT INTO Giftcode(campaign_id, id_creator, code, type_giftcode, value, is_active, create_time, expired_time, user_id) VALUES (" + campaign_id + "," + accountingID + ",'" + voucher + "'," + 0 + "," + value + "," + 0 + ", '"+ time.ToString(format) + "', '" + campaign.end_time.ToString(format)+"'," + 0 + ")";

                            //reader = serviceBase.query(queryStringInsertLog);
                            //DatabaseService.Instance.getConnection().Close();

                            string query = "INSERT INTO Giftcode(campaign_id, id_creator, code, type_giftcode, value, is_active, create_time, expired_time, user_id) VALUES (@campaign_id,@accountingID,@voucher,0,@value,0,@time,@endtime,0)";
                            SqlCommand command = new SqlCommand(query, DatabaseService.Instance.getConnection());
                            command.Parameters.AddWithValue("@campaign_id", campaign_id);
                            command.Parameters.AddWithValue("@accountingID", accountingID);
                            command.Parameters.AddWithValue("@voucher", voucher);
                            command.Parameters.AddWithValue("@value", value);
                            command.Parameters.AddWithValue("@time", time);
                            command.Parameters.AddWithValue("@endtime", campaign.end_time);
                            reader = serviceBase.querySqlParam(command);

                            reader.Close();
                            DatabaseService.Instance.getConnection().Close();
                        }
                        catch (Exception)
                        {
                            return Json(new
                            {
                                error = true,
                                msg = "Lỗi ko lưu được DB, "
                            });
                        }

                        generatedVouchers.Add(voucher);
                    }

                }
                DatabaseService.Instance.getConnection().Close();
                return Json(new
                {
                    error = false,
                    msg = "Đã tạo giftcode thành công."
                });
            }
        }
        
    }
}