﻿
using CorePayment.Base;
using CorePayment.Helper;
using CorePayment.Response;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers.APIService
{
    public class APIUserServiceController : Controller
    {

        private string render_code()
        {
            using (var db = new GamePaymentContext())
            {
                var code = CryptoHelper.RandomString(6);
                var querycode = db.User.Where(c => (c.code == code)).FirstOrDefault();
                if (querycode != null)
                {
                    return this.render_code();
                }
                return code;
            }
        }

        private bool validateUsername(string username)
        {
            if (username.Contains(" "))
            {
                return true;
            }
            Regex regex = new Regex("^[a-z0-9_-]{6,24}$");
            return regex.IsMatch(username);
        }

        private bool validateEmail(string email)
        {
            Regex regex = new Regex("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
            return regex.IsMatch(email);
        }

        private bool validatePassword(string password)
        {
            if(password.Contains(" "))
            { 

                return false;
            }
            if (password.Length > 5)
            {
                return true;
            }
            return false;
        }

        private bool validatePhone(string phone)
        {
            if (phone.Length > 11 || phone.Length < 10)
            {
                return false;
            }
            Regex regex = new Regex(@"^-*[0-9,\.?\-?\(?\)?\ ]+$");
            return regex.IsMatch(phone);
        }

        public ActionResult generateToken()
        {
            string token_gen_f = TokenHelper.GenerateToken("tokenlogin", 3);
            string ipAddress = this.GetIPAddress();
            var complex = 0;
            //Sửa lý log IP đăng ký        
            var h_ip = new History_ip();
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            try
            {
                var query = "SELECT count FROM History_ip WHERE ip=@ipaddress";
                SqlCommand command = new SqlCommand(query, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@ipaddress", ipAddress);
                reader = serviceBase.querySqlParam(command);
                if (!reader.HasRows)
                {
                    DatabaseService.Instance.getConnection().Close();
                    return Json(new
                    {
                        error = false,
                        token = token_gen_f,
                        comp = 1
                    });
                }
                while (reader.Read())
                {
                    h_ip.count = Convert.ToInt32(reader["count"].ToString());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();
                complex = h_ip.count + 1;

                return Json(new
                {
                    error = false,
                    token = token_gen_f,
                    comp = complex
                });
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = false,
                    token = token_gen_f,
                    comp = 2
                });
            }

        }

        public ActionResult login(string username, string password, string token)
        {
            string id = TokenHelper.ValidateToken(token);
            if (!id.Equals("tokenlogin"))
            {
                return Json(new
                {
                    error = true,
                    msg = "Token fail.",
                    code = -1
                });
            }

            if (username == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Username chưa được post lên",
                    code = 2
                });
            }
            if (password == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Username chưa được post lên",
                    code = 2
                });
            }
            using (var db = new GamePaymentContext())
            {
                List<Users> loginResult = new List<Users>();
                try
                {
                    loginResult = new UserPrivateServices().authentication(username.Trim(), password.Trim());
                    if (loginResult.Count == 0)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Tài khoản hoặc password không đúng.",
                            code = 1
                        });

                    }
                }
                catch (Exception)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Tài khoản hoặc password không đúng.",
                        code = 2
                    });

                }


                //Nếu tồn tại
                List<Merchant> list_merchant = new List<Merchant>();
                if (loginResult.Count == 1)
                {
                    Users user = new Users();
                    user = loginResult[0];
                    if (user.status_active.Equals("deactive"))
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Tài khoản đã bị khóa.",
                            code = 3
                        });
                    }
                    if (user.verify == 0)
                    {
                        string token_gen_f = TokenHelper.GenerateToken(loginResult[0].id.ToString());
                        return Json(new
                        {
                            error = true,
                            token = token_gen_f,
                            user = user,
                            msg = "Tài khoản chưa được verify.",
                            code = 4
                        });
                    }

                    Merchant merchant_item = db.Merchant.FirstOrDefault(e => e.id == user.merchant_id);
                    list_merchant.Add(merchant_item);
                }
                if (loginResult.Count > 1)
                {
                    foreach (Users element in loginResult)
                    {
                        Merchant merchant_item = db.Merchant.FirstOrDefault(e => e.id == element.merchant_id);
                        list_merchant.Add(merchant_item);
                    }
                }
                List<MerchantUserResponse> list_merchant_res = new List<MerchantUserResponse>();
                foreach (Merchant element in list_merchant)
                {
                    MerchantUserResponse merchantUserResponse = new MerchantUserResponse();
                    merchantUserResponse.id = element.id;
                    merchantUserResponse.title = element.name;
                    list_merchant_res.Add(merchantUserResponse);
                }
                string token_gen = TokenHelper.GenerateToken(loginResult[0].id.ToString());
                return Json(new
                {
                    error = false,
                    token = token_gen,
                    code = 0,
                    user = loginResult[0],
                    list_merchant = list_merchant_res,
                    ResultCode = 1,
                    UserId = loginResult[0].id
                });
            }
        }

        [HttpPost]
        public ActionResult loginPhoton()
        {
            string documentContents;
            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }
            if (documentContents == null)
            {
                return Json(new
                {
                    ResultCode = 2,
                    Data = new
                    {
                        error = true,
                        msg = "Chưa có dữ liệu",
                        code = 2,
                        documentContents = documentContents
                    }
                });
            }
            PostPhotonLogin obj = JsonConvert.DeserializeObject<PostPhotonLogin>(documentContents);

            int merchant_id_int = obj.merchant_id;
            string username = obj.username;
            string password = obj.password;
            //string token = obj.token;
            //string id = TokenHelper.ValidateToken(token);
            //if (id.Equals(""))
            //{
            //    return Json(new
            //    {
            //        error = true,
            //        msg = "Token fail.",
            //        code = -1
            //    });
            //}
            if (merchant_id_int == 0)
            {
                return Json(new
                {
                    ResultCode = 2,
                    Data = new
                    {
                        error = true,
                        msg = "merchant_id_int chưa được post lên",
                        code = 2,
                        documentContents = documentContents
                    }
                });
            }
            if (username == null)
            {
                return Json(new
                {
                    ResultCode = 2,
                    Data = new
                    {
                        error = true,
                        msg = "Username chưa được post lên",
                        code = 2,
                        documentContents = documentContents
                    }
                });

            }
            if (password == null)
            {
                return Json(new
                {
                    ResultCode = 2,
                    Data = new
                    {
                        error = true,
                        msg = "password chưa được post lên",
                        code = 2,
                        documentContents = documentContents
                    }
                });
            }
            using (var db = new GamePaymentContext())
            {
                List<Users> loginResult = new List<Users>();
                try
                {
                    loginResult = new UserPrivateServices().authenticationByMerchant(username.Trim(), password.Trim(), merchant_id_int);
                    if (loginResult.Count == 0)
                    {
                        return Json(new
                        {
                            ResultCode = 2,
                            Data = new
                            {
                                error = true,
                                code = 1,
                                msg = "Tài khoản hoặc mật khẩu không đúng."
                            }
                        });

                    }
                }
                catch (Exception)
                {
                    return Json(new
                    {
                        ResultCode = 2,
                        Data = new
                        {
                            error = true,
                            code = 1,
                            msg = "Tài khoản hoặc mật khẩu không đúng."
                        }
                    });
                }


                //Nếu tồn tại
                List<Merchant> list_merchant = new List<Merchant>();
                if (loginResult.Count == 1)
                {
                    Users user = new Users();
                    user = loginResult[0];
                    if (user.status_active.Equals("deactive"))
                    {
                        return Json(new
                        {
                            ResultCode = 2,
                            Data = new
                            {
                                error = true,
                                code = 3,
                                msg = "Tài khoản đang bị khóa."
                            }
                        });
                    }
                    if (user.verify == 0)
                    {
                        string token_gen_f = TokenHelper.GenerateToken(loginResult[0].id.ToString());
                        return Json(new
                        {
                            ResultCode = 2,
                            Data = new
                            {
                                error = true,
                                code = 4,
                                msg = "Tài khoản chưa được verify."
                            }
                        });
                    }

                    Merchant merchant_item = db.Merchant.FirstOrDefault(e => e.id == user.merchant_id);
                    list_merchant.Add(merchant_item);
                }
                if (loginResult.Count > 1)
                {
                    foreach (Users element in loginResult)
                    {
                        Merchant merchant_item = db.Merchant.FirstOrDefault(e => e.id == element.merchant_id);
                        list_merchant.Add(merchant_item);
                    }
                }
                List<MerchantUserResponse> list_merchant_res = new List<MerchantUserResponse>();
                foreach (Merchant element in list_merchant)
                {
                    MerchantUserResponse merchantUserResponse = new MerchantUserResponse();
                    merchantUserResponse.id = element.id;
                    merchantUserResponse.title = element.name;
                    list_merchant_res.Add(merchantUserResponse);
                }
                string token_gen = TokenHelper.GenerateToken(loginResult[0].id.ToString());

                return Json(new
                {
                    ResultCode = 1,
                    UserId = loginResult[0].id,
                    Data = new
                    {
                        error = false,
                        code = 0,
                        token = token_gen,
                        user = loginResult[0]

                    }
                });
            }
        }
        [HttpPost]
        public ActionResult getPaymentGate(string token, string code_gate)
        {
            if (code_gate == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số code_gate null."
                });
            }
            string id = TokenHelper.ValidateToken(token);
            if (id.Equals(""))
            {
                return Json(new
                {
                    error = true,
                    msg = "Token fail."
                });
            }


            using (var db = new GamePaymentContext())
            {
                PaymentGate paymentGateService = new PaymentGate();
                code_gate = code_gate.Trim();
                paymentGateService = db.PaymentGate.Where(e => e.code == code_gate).FirstOrDefault();
                var query = from g in db.CashIORate
                            where (g.payment_gate_id == paymentGateService.id)
                            select new
                            {
                                g.id,
                                g.cash_in,
                                g.cash_out,
                            };

                if (paymentGateService == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Cổng thanh toán không tồn tại."
                    });
                }
                return Json(new
                {
                    error = false,
                    item = paymentGateService,
                    cashIO = query.ToList()
                });
            }

        }
        private PaymentGate getPaymentGate(string paymentgate_code, GamePaymentContext db)
        {


            PaymentGate paymentGateService = new PaymentGate();
            paymentgate_code = paymentgate_code.Trim();
            paymentGateService = db.PaymentGate.Where(e => e.code == paymentgate_code).FirstOrDefault();
            return paymentGateService;

        }
        //[HttpPost]
        //public ActionResult getListTransactions(string token, string code_gate, string tag, int index)
        //{
        //    using (var db = new GamePaymentContext())
        //    {
        //        string idUser = TokenHelper.ValidateToken(token);
        //        if (idUser.Equals(""))
        //        {
        //            return Json(new
        //            {
        //                error = true,
        //                msg = "Token fail."
        //            });
        //        }
        //        int userID = Int32.Parse(idUser.ToString());
        //        var userDB = db.User.FirstOrDefault(e => e.id == userID);
        //        if (userDB.status_active.Trim().Equals("deactive"))
        //        {
        //            return Json(new
        //            {
        //                error = true,
        //                msg = "Tài khoản đã bị khóa."
        //            });
        //        }

        //        List<TransactionResponse> list_transactions = new List<TransactionResponse>();

        //        //Kiểm tra cổng thanh toán có tồn tại 
        //        PaymentGate paymentGateService = this.getPaymentGate(code_gate, db);
        //        if (paymentGateService.id == 0)
        //        {
        //            return Json(new
        //            {
        //                error = true,
        //                msg = "Cổng thanh toán không tồn tại"
        //            });
        //        }
        //        ServiceBase serviceBase = new ServiceBase();
        //        string queryString = "SELECT " +
        //        "id,user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,(SELECT name FROM Wallet WHERE id =Transactions.wallet_id) as 'wallet_name' " +
        //        "FROM Transactions WHERE " +
        //        "user_id='" + idUser + "' " +
        //        "AND payment_gate='" + paymentGateService.id + "' " +
        //        "AND tag = '" + tag + "' " +
        //        "AND (transactions_type = '0' OR transactions_type IS NULL) " +
        //        "AND merchant_id ='" + userDB.merchant_id + "' " +
        //        "ORDER BY id DESC OFFSET " + index + " ROWS FETCH NEXT 10 ROWS ONLY";
        //        SqlDataReader reader = serviceBase.query(queryString);
        //        while (reader.Read())
        //        {
        //            TransactionResponse transactions = new TransactionResponse();
        //            transactions.id = Int32.Parse(reader["id"].ToString());
        //            transactions.user_id = Int32.Parse(reader["user_id"].ToString());
        //            transactions.merchant_id = Int32.Parse(reader["merchant_id"].ToString());
        //            transactions.payment_gate = Int32.Parse(reader["payment_gate"].ToString());
        //            transactions.wallet_id = Int32.Parse(reader["wallet_id"].ToString());
        //            transactions.balance = Double.Parse(reader["balance"].ToString());
        //            transactions.type = reader["type"].ToString();
        //            transactions.time_creat = reader["time_creat"].ToString();
        //            transactions.value = Double.Parse(reader["value"].ToString());
        //            transactions.user_send = Int32.Parse(reader["user_send"].ToString());
        //            transactions.tag = reader["tag"].ToString();
        //            transactions.message = reader["message"].ToString();
        //            transactions.wallet_name = reader["wallet_name"].ToString();
        //            list_transactions.Add(transactions);

        //        }
        //        DatabaseService.Instance.getConnection().Close();
        //        return Json(new
        //        {
        //            error = false,
        //            transactions = list_transactions
        //        });

        //    }

        //}
        //[HttpPost]
        //public ActionResult getTransactionStatisticUser(string token, string code_gate, string tag)
        //{
        //    using (var db = new GamePaymentContext())
        //    {
        //        string idUser = TokenHelper.ValidateToken(token);
        //        if (idUser.Equals(""))
        //        {
        //            return Json(new
        //            {
        //                error = true,
        //                msg = "Token fail."
        //            });
        //        }
        //        int userID = Int32.Parse(idUser.ToString());
        //        var userDB = db.User.FirstOrDefault(e => e.id == userID);
        //        if (userDB.status_active.Trim().Equals("deactive"))
        //        {
        //            return Json(new
        //            {
        //                error = true,
        //                msg = "Tài khoản đã bị khóa."
        //            });
        //        }
        //        TransactionServiceResponse transactionServiceResponse = new TransactionServiceResponse();
        //        SqlDataReader reader;

        //        PaymentGate paymentGateService = new PaymentGate();
        //        //Kiểm tra cổng thanh toán có tồn tại
        //        try
        //        {
        //            paymentGateService = this.getPaymentGate(code_gate, db);

        //        }
        //        catch (Exception)
        //        {
        //            return Json(new
        //            {
        //                error = true,
        //                msg = " Get công thanh toán xảy ra lỗi"
        //            });

        //        }
        //        if (paymentGateService.id == 0)
        //        {
        //            return Json(new
        //            {
        //                error = true,
        //                msg = " Cổng thanh toán không tồn tại"
        //            });
        //        }
        //        ServiceBase serviceBase = new ServiceBase();

        //        //Lấy dữ liệu giao dịch
        //        reader = serviceBase.query("SELECT ISNULL(SUM(value),0) as 'value' FROM Transactions WHERE user_id='" + userDB.id + "' AND payment_gate='" + paymentGateService.id + "' AND tag = '" + tag + "' AND type = 'plus' AND merchant_id =" + userDB.merchant_id);

        //        while (reader.Read())
        //        {
        //            transactionServiceResponse.plus = Double.Parse(reader["value"].ToString());
        //        }
        //        DatabaseService.Instance.getConnection().Close();

        //        reader = serviceBase.query("SELECT ISNULL(SUM(value),0) as 'value' FROM Transactions WHERE user_id='" + userDB.id + "' AND payment_gate='" + paymentGateService.id + "' AND tag = '" + tag + "' AND type = 'minus' AND merchant_id =" + userDB.merchant_id);
        //        while (reader.Read())
        //        {
        //            transactionServiceResponse.minus = Double.Parse(reader["value"].ToString());
        //        }
        //        DatabaseService.Instance.getConnection().Close();
        //        return Json(new
        //        {
        //            error = false,
        //            minus = transactionServiceResponse.minus,
        //            plus = transactionServiceResponse.plus
        //        });
        //    }
        //}

        private Random random = new Random();
        public string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        //Send verify phone
        private void _send_sms_otp(string phone, string code)
        {
            SendSMSHelper sendSMSHelper = new SendSMSHelper();
            string new_phone = phone.Substring(1, phone.Length - 1);
            string result = sendSMSHelper.send("+84" + phone, code);
        }
        [HttpPost]
        public ActionResult updateProfile(string token, string username, string email, string phone)
        {
            using (var db = new GamePaymentContext())
            {
                phone = phone.Trim();
                email = email.Trim();
                username = username.Trim();
                token = token.Trim();
                string idUser = TokenHelper.ValidateToken(token);
                if (idUser.Equals(""))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Token fail."
                    });
                }
                int userID = Int32.Parse(idUser.ToString());
                var userDB = db.User.FirstOrDefault(e => e.id == userID);
                if (userDB.status_active.Trim().Equals("deactive"))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Tài khoản đã bị khóa."
                    });
                }

                if (!this.validatePhone(phone.Trim()))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Điện thoại không đúng định dạng."
                    });
                }
                ServiceBase serviceBase = new ServiceBase();
                //Kiểm tra username tồn tại
                var userDBischeck = db.User.FirstOrDefault(e => e.username == username && e.merchant_id == e.merchant_id);

                if (userDBischeck != null)
                {
                    if (userID != userDBischeck.id)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Username đã tồn tại."
                        });
                    }
                    

                }
                string code = this.RandomString(6);
                if (!phone.Equals(""))
                {
                    //Kiểm tra phone tồn tại
                    var userDBischeckPhone = db.User.FirstOrDefault(e => e.phone == phone && e.merchant_id == e.merchant_id);
                    if (userDBischeckPhone != null && userDBischeckPhone.id != userDB.id)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Số điện thoại đã được đăng ký."
                        });
                    }

                    //Send SMS
                    if (userDBischeckPhone != null)
                    {
                        this._send_sms_otp(phone, code);
                    }
                }
                userDB.username = username;
                userDB.phone = phone;
                userDB.code = code;
                userDB.email = email;
                db.SaveChanges();

                return Json(new
                {
                    error = false,
                    msg = "Cập nhật thành công."
                });
            }
        }
        [HttpPost]
        public ActionResult changePassword(string otp, string phone, string password)
        {
            using (var db = new GamePaymentContext())
            {
                //Kiểm tra otp

                var userDB = db.User.FirstOrDefault(e => (e.code_sms == otp && e.phone == phone));

                if (userDB == null)
                {
                    return Json(new
                    {
                        error = true,
                        code = 1,
                        msg = "Mã otp không đúng."
                    });
                }
                if (userDB.time_send_otp != null)
                {
                    var time_creat = userDB.time_send_otp;
                    DateTime dateTime = DateTime.Now;
                    long timeMsSinceNow = new DateTimeOffset(dateTime).ToUnixTimeMilliseconds();
                    long timeMsSinceOTP = new DateTimeOffset(time_creat.Value).ToUnixTimeMilliseconds();
                    if ((timeMsSinceOTP - timeMsSinceNow) / 1000 / 60 > 5)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Mã OTP đã hết hạn"
                        });
                    }

                    if (userDB.status_active.Trim().Equals("deactive"))
                    {
                        return Json(new
                        {
                            error = true,
                            code = 2,
                            msg = "Tài khoản đã bị khóa."
                        });
                    }
                }
                //Kiểm tra password cũ
                ServiceBase serviceBase = new ServiceBase();
                userDB.password = CryptoHelper.MD5Hash(password, userDB.id.ToString());
                db.SaveChanges();
                return Json(new
                {
                    error = false,
                    code = 0,
                    msg = "Cập nhật thành công."
                });
            }
        }
        [HttpPost]
        public ActionResult verifyPhone(string token, string code)
        {
            using (var db = new GamePaymentContext())
            {
                code = code.Trim();
                token = token.Trim();
                string idUser = TokenHelper.ValidateToken(token);
                if (idUser.Equals(""))
                {
                    return Json(new
                    {
                        error = true,
                        code = 1,
                        msg = "Token fail."
                    });
                }
                int userID = Int32.Parse(idUser.ToString());
                var userDB = db.User.FirstOrDefault(e => e.id == userID);
                if (userDB.status_active.Trim().Equals("deactive"))
                {
                    return Json(new
                    {
                        error = true,
                        code = 2,
                        msg = "Tài khoản đã bị khóa."
                    });
                }
                //Check mã OTP
                ServiceBase serviceBase = new ServiceBase();                
                if (userDB.code_sms != code)
                {
                    return Json(new
                    {
                        error = true,
                        code = 3,
                        msg = "Mã OTP không đúng."
                    });
                }

                //Update verify phone
                userDB.verify_phone = 1;
                userDB.verify = 1;
                db.SaveChanges();
                return Json(new
                {
                    error = false,
                    code = 0,
                    user = userDB,
                    msg = "Cập nhật thành công."
                });
            }
        }
        [HttpPost]
        public ActionResult sendToSMSOTP(string token)
        {
            using (var db = new GamePaymentContext())
            {
                string idUser = TokenHelper.ValidateToken(token);
                if (idUser.Equals(""))
                {
                    return Json(new
                    {
                        error = true,
                        code = 1,
                        msg = "Token fail."
                    });
                }
                int userID = Int32.Parse(idUser.ToString());
                var userDB = db.User.FirstOrDefault(e => e.id == userID);
                if (userDB == null)
                {
                    return Json(new
                    {
                        error = true,
                        code = 5,
                        msg = "User không tồn tại."
                    });
                }
                if (userDB.status_active.Trim().Equals("deactive"))
                {
                    return Json(new
                    {
                        error = true,
                        code = 2,
                        msg = "Tài khoản đã bị khóa."
                    });
                }
                //Kiểm tra đủ điều kiện gửi
                ServiceBase serviceBase = new ServiceBase();
                SqlDataReader reader;
                reader = serviceBase.query("SELECT TOP 1 id,time_send_otp,count_send_otp FROM Users WHERE id = '" + userDB.id + "' ");

                var now = DateTime.Now;
                int count_user = 0;
                string datetime = now.Year + "-" + now.Month + "-" + now.Day;
                int count_send = 0;
                while (reader.Read())
                {
                    if (reader["time_send_otp"] != null)
                    {
                        if (!reader["time_send_otp"].ToString().Trim().Equals(""))
                        {
                            datetime = reader["time_send_otp"].ToString();
                        }
                    }
                    if (reader["count_send_otp"] != null)
                    {
                        if (!reader["count_send_otp"].ToString().Trim().Equals(""))

                        {
                            count_send = Int32.Parse(reader["count_send_otp"].ToString());
                        }
                    }

                    count_user++;
                }
                DatabaseService.Instance.getConnection().Close();
                if (count_user == 0)
                {
                    return Json(new
                    {
                        error = true,
                        code = 3,
                        msg = "User không tồn tại."
                    });
                }
                string code = this.render_code();

                DateTime myDate = DateTime.Parse(datetime);
                string[] date_time = datetime.Split('-');
                if (myDate.Year == now.Year && myDate.Month == now.Month && myDate.Day == now.Day)
                {
                    if (count_send >= 2)
                    {
                        return Json(new
                        {
                            error = true,
                            code = 4,
                            msg = "Một ngày chỉ được gửi mã OTP tối đa 2 lần."
                        });
                    }
                    count_send++;
                }
                else
                {
                    count_send = 1;
                }

                string format = "yyyy-MM-dd";
                reader = serviceBase.query("UPDATE Users SET time_send_otp='" + now.ToString(format) + "',count_send_otp='" + count_send + "',code_sms='" + code + "' WHERE id ='" + userDB.id + "' ");
                DatabaseService.Instance.getConnection().Close();

                string emailOrPhone = userDB.phone.Substring(1, userDB.phone.Length - 1);
                SendSMSHelper sendSMSHelper = new SendSMSHelper();
                string result = sendSMSHelper.send("+84" + emailOrPhone, code);
                return Json(new
                {
                    error = false,
                    code = 0,
                    msg = "Gửi mã OTP thành công"
                });
            }
        }
        [HttpPost]
        public ActionResult sendToSMSOTPResetPassword(string phone, string merchant_id)
        {
            using (var db = new GamePaymentContext())
            {
                int merchantid = Int32Ext.toInt(merchant_id);
                var userDB = db.User.FirstOrDefault(e => (e.phone == phone && e.merchant_id == merchantid));
                if (userDB.status_active.Trim().Equals("deactive"))
                {
                    return Json(new
                    {
                        error = true,
                        code = 2,
                        msg = "Tài khoản đã bị khóa."
                    });
                }
                //Kiểm tra đủ điều kiện gửi
                ServiceBase serviceBase = new ServiceBase();
                SqlDataReader reader;
                reader = serviceBase.query("SELECT TOP 1 id,time_send_otp,count_send_otp FROM Users WHERE id = '" + userDB.id + "' ");

                var now = DateTime.Now;
                int count_user = 0;
                string datetime = now.Year + "-" + now.Month + "-" + now.Day;
                int count_send = 0;
                while (reader.Read())
                {
                    if (reader["time_send_otp"] != null)
                    {
                        if (!reader["time_send_otp"].ToString().Trim().Equals(""))
                        {
                            datetime = reader["time_send_otp"].ToString();
                        }
                    }

                    if (reader["count_send_otp"] != null)
                    {
                        if (!reader["count_send_otp"].ToString().Trim().Equals(""))

                        {
                            count_send = Int32.Parse(reader["count_send_otp"].ToString());
                        }
                    }

                    count_user++;
                }
                DatabaseService.Instance.getConnection().Close();
                if (count_user == 0)
                {
                    return Json(new
                    {
                        error = true,
                        code = 3,
                        msg = "User không tồn tại."
                    });
                }
                string code = this.render_code();

                DateTime myDate = DateTime.Parse(datetime);
                string[] date_time = datetime.Split('-');
                if (myDate.Year == now.Year && myDate.Month == now.Month && myDate.Day == now.Day)
                {
                    if (count_send >= 2)
                    {
                        return Json(new
                        {
                            error = true,
                            code = 4,
                            msg = "Một ngày chỉ được gửi mã OTP tối đa 2 lần."
                        });
                    }
                    count_send++;
                }
                else
                {
                    count_send = 1;
                }

                string format = "yyyy-MM-dd";
                reader = serviceBase.query("UPDATE Users SET time_send_otp='" + now.ToString(format) + "',count_send_otp='" + count_send + "',code_sms='" + code + "' WHERE id ='" + userDB.id + "' ");
                DatabaseService.Instance.getConnection().Close();

                string emailOrPhone = userDB.phone.Substring(1, userDB.phone.Length - 1);
                SendSMSHelper sendSMSHelper = new SendSMSHelper();
                string result = sendSMSHelper.send("+84" + emailOrPhone, code);
                return Json(new
                {
                    error = false,
                    code = 0,
                    msg = "Gửi mã OTP thành công"
                });
            }
        }
        private IRestResponse SendMailMessage(string email, string name, string code)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api",
                                            "key-e63b4cada39f62f60d9a6758226c7576");
            RestRequest request = new RestRequest();
            request.AddParameter("domain", "mail.newsoft.vn", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "XÁC NHẬN ĐĂNG KÝ <mailgun@mail.newsoft.vn>");
            request.AddParameter("to", email);
            request.AddParameter("subject", "Xác nhận đăng ký");
            EmailService emailService = new EmailService(email, name, code);
            request.AddParameter("html", emailService.email_registry);
            request.Method = Method.POST;
            return client.Execute(request);
        }
        private string GetIPAddress()
        {
            string ipAddressString = Request.UserHostAddress;

            if (ipAddressString == null)
                return null;

            IPAddress ipAddress;
            IPAddress.TryParse(ipAddressString, out ipAddress);

            // If we got an IPV6 address, then we need to ask the network for the IPV4 address 
            // This usually only happens when the browser is on the same machine as the server.
            if (ipAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
            {
                ipAddress = System.Net.Dns.GetHostEntry(ipAddress).AddressList
                    .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
            }

            return ipAddress.ToString();
        }
        [HttpPost]
        public ActionResult registry(String username, String password, String emailOrPhone, String merchant_id, string token, string header)
        {

            if (username == null)
            {
                return Json(new
                {
                    error = true,
                    code = 1,
                    msg = "Username không hợp lệ (Không chứa ký tự đặc biểt > 3, < 24 ký tự)"
                });
            }
            if (password == null)
            {
                return Json(new
                {
                    error = true,
                    code = 2,
                    msg = "Password không hợp lệ ( > 6 ký tự)"
                });
            }
            if (merchant_id == null)
            {
                return Json(new
                {
                    error = true,
                    code = 2,
                    msg = "merchant_id chưa được post lên"
                });
            }
            int merchantid = Int32Ext.toInt(merchant_id);
            string id = TokenHelper.ValidateToken(token);
            if (!id.Equals("tokenlogin"))
            {
                return Json(new
                {
                    error = true,
                    msg = "Token fail.",
                    code = -1
                });
            }
            String ipAddress = this.GetIPAddress();

            using (var db = new GamePaymentContext())
            {
                //Sửa lý log IP đăng ký
                var h_ip = db.History_ip.FirstOrDefault(h => h.ip == ipAddress);

                if (h_ip == null)
                {
                    db.History_ip.Add(new Models.History_ip()
                    {
                        ip = ipAddress,
                        time_create = DateTime.Now,
                        count = 1
                    });
                    db.SaveChanges();
                }
                else
                {
                    DateTime dateTime = DateTime.Now;
                    DateTime time_creat = h_ip.time_create;
                    //long timeMsSinceNow = new DateTimeOffset(dateTime).ToUnixTimeMilliseconds();
                    //long timeMsSinceOTP = new DateTimeOffset(time_creat).ToUnixTimeMilliseconds();
                    //long count_min = (timeMsSinceNow - timeMsSinceOTP) / 1000 / 60;
                    string strCountZero = new String('0', h_ip.count); // -----
                    if (!HashCash.Instance.Verify(header, strCountZero, token))
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Chưa đăng kí được, sai dữ liệu",
                            code = 1
                        });
                    }
                    if (time_creat.Day == DateTime.Now.Day&& time_creat.Month == DateTime.Now.Month && time_creat.Year == DateTime.Now.Year)
                    {
                        //Check số lần
                        if (h_ip.count >= 5)
                        {
                            return Json(new
                            {
                                error = true,
                                msg = "Bạn đã đăng ký vượt quá 5 lần, Vui lòng đợi sau 24h để đăng ký tiếp.",
                                code = 8
                            });
                        }
                        //h_ip.count = h_ip.count+1;
                        //h_ip.time_create = DateTime.Now;
                        db.SaveChanges();
                    }
                    else
                    {
                        h_ip.time_create = DateTime.Now;
                        h_ip.count = 1;
                        db.SaveChanges();
                    }

                    
                }

                if (!this.validateUsername(username))
                {
                    return Json(new
                    {
                        error = true,
                        code = 1,
                        msg = "Username không hợp lệ (Không chứa ký tự đặc biểt > 5, < 24 ký tự)"
                    });
                }
                //---check username khac agency
                if (this.validateUsername_Agency(username))
                {
                    return Json(new
                    {
                        error = true,
                        code = 1,
                        msg = "Username không hợp lệ (Username đã tồn tại)"
                    });
                }


                if (!validatePassword(password))
                {
                    return Json(new
                    {
                        error = true,
                        code = 2,
                        msg = "Password không hợp lệ ( > 6 ký tự)"
                    });
                }

                Boolean is_email = this.validateEmail(emailOrPhone);
                Boolean is_phone = this.validatePhone(emailOrPhone);

                if (!is_phone && !is_email)
                {
                    return Json(new
                    {
                        error = true,
                        code = 3,
                        msg = "Email or Phone không hợp lệ"
                    });
                }
                //Kiểm tra username đã đăng ký chưa
                ServiceBase serviceBase = new ServiceBase();
                SqlDataReader reader;
                reader = serviceBase.query("SELECT TOP 1 id FROM Users WHERE username = '" + username + "' AND merchant_id = '" + merchantid + "' ");

                int count_user = 0;
                while (reader.Read())
                {
                    count_user++;
                }
                DatabaseService.Instance.getConnection().Close();
                if (count_user > 0)
                {
                    //User đã tồn tại
                    return Json(new
                    {
                        error = true,
                        code = 4,
                        msg = "Username đã được đăng ký"
                    });
                }
                string code = "";
                if (!is_phone)
                {
                    code = this.render_code();
                    reader = serviceBase.query("SELECT TOP 1 id FROM Users WHERE  (email = '" + emailOrPhone.Trim() + "' AND merchant_id = '" + merchantid + "') ");
                    int count_user_email = 0;
                    while (reader.Read())
                    {
                        count_user_email++;
                    }
                    DatabaseService.Instance.getConnection().Close();
                    if (count_user_email > 0)
                    {
                        //User đã tồn tại
                        return Json(new
                        {
                            error = true,
                            code = 5,
                            msg = "Email đã được đăng ký"
                        });
                    }

                    reader = serviceBase.query("INSERT INTO Users (username,password,email,merchant_id,status_active,code,verify) " +
                        "VALUES('" + username + "','" + password + "','" + emailOrPhone.Trim() + "'," + merchantid + ",'active','" + code + "',0)");
                    DatabaseService.Instance.getConnection().Close();
                    IRestResponse response_sendmail = this.SendMailMessage(emailOrPhone, username, code);

                    if (response_sendmail.StatusCode != HttpStatusCode.OK)
                    {
                        return Json(new
                        {
                            error = true,
                            code = 6,
                            msg = "Send code email lỗi"
                        });
                    }
                }
                else
                {
                    code = this.RandomString(6);
                    reader = serviceBase.query("SELECT TOP 1 id FROM Users WHERE  (phone = '" + emailOrPhone.Trim() + "' AND merchant_id = '" + merchantid + "') ");
                    int count_user_email = 0;
                    while (reader.Read())
                    {
                        count_user_email++;
                    }
                    DatabaseService.Instance.getConnection().Close();
                    if (count_user_email > 0)
                    {
                        //User đã tồn tại
                        return Json(new
                        {
                            error = true,
                            code = 7,
                            msg = "Số điện thoại đã được đăng ký",

                        });
                    }
                    DateTime time = DateTime.Now;
                    // Use current time
                    string format = "yyyy-MM-dd";
                    reader = serviceBase.query("INSERT INTO Users (username,password,phone,merchant_id,status_active,code_sms,verify,time_send_otp,count_send_otp) " +
                        "VALUES('" + username + "','" + password + "','" + emailOrPhone.Trim() + "'," + merchantid + ",'active','" + code + "',0,'" + time.ToString(format) + "',1)");
                    DatabaseService.Instance.getConnection().Close();

                    emailOrPhone = emailOrPhone.Substring(1, emailOrPhone.Length - 1);
                    SendSMSHelper sendSMSHelper = new SendSMSHelper();
                    string result = sendSMSHelper.send("+84" + emailOrPhone, code);
                }


                //Lấy info
                reader = serviceBase.query("SELECT TOP 1 * FROM Users WHERE (email='" + emailOrPhone + "' OR phone = '0" + emailOrPhone + "') AND merchant_id = '" + merchantid + "'");

                Users users = new Users();
                while (reader.Read())
                {
                    users.id = Int32.Parse(reader["id"].ToString());
                    users.username = reader["username"].ToString();
                    users.email = reader["email"].ToString();
                    if (reader["status_active"].ToString() == "active")
                    {
                        users.isActive = EnumUserModelActiveStatus.active;
                    }
                    else
                    {
                        users.isActive = EnumUserModelActiveStatus.deactive;
                    }
                    users.verify = Int32.Parse(reader["verify"].ToString());
                    users.phone = reader["phone"].ToString().Trim();
                }
                DatabaseService.Instance.getConnection().Close();

                reader = serviceBase.query("UPDATE Users SET password='" + CryptoHelper.MD5Hash(password, users.id.ToString()) + "' WHERE id = '" + users.id + "' AND merchant_id ='" + merchantid + "'");
                DatabaseService.Instance.getConnection().Close();

                string token_gen = TokenHelper.GenerateToken(users.id.ToString());
                h_ip.count = 1;
                db.SaveChanges();
                return Json(new
                {
                    error = false,
                    code = 0,
                    token = token_gen,
                    user = users,
                    msg = "Đăng ký thành công"
                });
            }
        }

        private bool validateUsername_Agency(string username)
        {
            using (var db = new GamePaymentContext())
            {
                var agency = db.Agency.FirstOrDefault(e => e.username == username);
                if (agency != null)
                {
                    return true;
                }
                return false;
            }

        }
        [HttpPost]
        public ActionResult setUserDeactive(string token)
        {
            using (var db = new GamePaymentContext())
            {
                token = token.Trim();
                string idUser = TokenHelper.ValidateToken(token);
                if (idUser.Equals(""))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Token fail."
                    });
                }
                int userID = Int32.Parse(idUser.ToString());
                var userDB = db.User.FirstOrDefault(e => e.id == userID);
                userDB.verify = 0;
                db.SaveChanges();
                return Json(new
                {
                    error = false,
                    user = userDB,
                    msg = "Cập nhật thành công."
                });
            }
        }
        [HttpPost]
        public ActionResult setUserActive(string code, string token)
        {
            using (var db = new GamePaymentContext())
            {
                token = token.Trim();
                code = code.Trim();
                string idUser = TokenHelper.ValidateToken(token);
                if (idUser.Equals(""))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Token fail."
                    });
                }
                int userID = Int32.Parse(idUser.ToString());
                var userDB = db.User.FirstOrDefault(e => e.id == userID);

                if (!userDB.code.Trim().Equals(code))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Mã code không chính xác."
                    });
                }

                userDB.verify = 1;
                db.SaveChanges();
                return Json(new
                {
                    error = false,
                    user = userDB,
                    msg = "Cập nhật thành công."
                });
            }
        }
        [HttpPost]
        public ActionResult getInfo(string token)
        {
            using (var db = new GamePaymentContext())
            {
                token = token.Trim();
                string idUser = TokenHelper.ValidateToken(token);
                if (idUser.Equals(""))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Token fail."
                    });
                }
                int userID = Int32.Parse(idUser.ToString());
                var userDB = db.User.FirstOrDefault(e => e.id == userID);
                return Json(new
                {
                    error = false,
                    user = userDB
                });
            }
        }
        [HttpPost]
        public ActionResult loginFacebook()
        {
            string documentContents;
            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }
            if (documentContents == null)
            {
                return Json(new
                {
                    ResultCode = 2,
                    Data = new
                    {
                        error = true,
                        msg = "Chưa có dữ liệu",
                        code = 2,
                        documentContents = documentContents
                    }
                });
            }
            ParamLoginFacebook obj = JsonConvert.DeserializeObject<ParamLoginFacebook>(documentContents);
            string token = obj.token;
            int merchant_idLogin = obj.merchant_id;
            if (merchant_idLogin == 0)
            {
                return Json(new
                {
                    ResultCode = 2,
                    Data = new
                    {
                        error = true,
                        msg = "Chưa có merchant_id",
                        code = 2,
                        documentContents = documentContents
                    }
                });
            }
            ResponseCheckToken responseOrder;
            try
            {
                WebClient n = new WebClient();
                var json = n.DownloadString("https://graph.facebook.com/me?access_token=" + token);
                string valueOriginal = Convert.ToString(json);
                responseOrder = JsonConvert.DeserializeObject<ResponseCheckToken>(valueOriginal);
            }
            catch (Exception)
            {
                return Json(new
                {
                    ResultCode = 2,
                    Data = new
                    {
                        error = true,
                        code = 1,
                        msg = "Token error."
                    }
                });
            }
            if (responseOrder.id.Equals(""))
            {
                return Json(new
                {
                    ResultCode = 2,
                    Data = new
                    {
                        error = true,
                        code = 1,
                        msg = "Token error."
                    }
                });
            }
            string facebook_id = responseOrder.id;


            //Kiểm tra Đã có facebook_id chưa
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            reader = serviceBase.query("SELECT TOP 1 * FROM Users WHERE facebook='" + facebook_id + "' AND merchant_id ='" + merchant_idLogin + "'");
            int count_user = 0;
            while (reader.Read())
            {
                count_user++;
            }
            DatabaseService.Instance.getConnection().Close();
            //Kiểm tra email có không
            int count_user_email = 0;

            if (count_user == 0 && count_user_email == 0)
            {
                reader = serviceBase.query("INSERT INTO Users (username,password,email,merchant_id,status_active,code,verify,facebook) " +
                    "VALUES('','',''," + merchant_idLogin + ",'active','',1,'" + facebook_id + "')");
                DatabaseService.Instance.getConnection().Close();
                ;
            }
            if (count_user > 0 && count_user_email == 0)
            {
                reader = serviceBase.query("UPDATE Users SET email='' WHERE facebook='" + facebook_id + "' AND merchant_id ='" + merchant_idLogin + "' ");
                DatabaseService.Instance.getConnection().Close();
            }
            if (count_user_email > 0 && count_user == 0)
            {
                reader = serviceBase.query("UPDATE Users SET facebook='" + facebook_id + "' WHERE email='' AND merchant_id ='" + merchant_idLogin + "' ");
                DatabaseService.Instance.getConnection().Close();
            }

            //Lấy info
            reader = serviceBase.query("SELECT TOP 1 * FROM Users WHERE facebook='" + facebook_id + "' AND merchant_id ='" + merchant_idLogin + "' ");
            Users users = new Users();
            while (reader.Read())
            {
                users.id = Int32.Parse(reader["id"].ToString());
                users.username = reader["username"].ToString();
                users.email = reader["email"].ToString();
                if (reader["status_active"].ToString() == "active")
                {
                    users.isActive = EnumUserModelActiveStatus.active;
                }
                else
                {
                    users.isActive = EnumUserModelActiveStatus.deactive;
                }
                users.verify = Int32.Parse(reader["verify"].ToString());
                users.merchant_id = Int32.Parse(reader["merchant_id"].ToString());
                users.verify_phone = Int32.Parse(reader["verify_phone"].ToString());
                users.phone = reader["phone"].ToString().Trim();
            }
            DatabaseService.Instance.getConnection().Close();
            string token_gen = TokenHelper.GenerateToken(users.id.ToString());
            return Json(new
            {
                ResultCode = 1,
                UserId = users.id,
                Data = new
                {
                    error = false,
                    code = 0,
                    user = users,
                }
            });
        }
        [HttpPost]
        public ActionResult loginGmail(string gmail_id, string email, string merchant_id)
        {
            //Kiểm tra Đã có gmail_id chưa
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            reader = serviceBase.query("SELECT TOP 1 * FROM Users WHERE gmail_id='" + gmail_id + "' AND merchant_id ='" + merchant_id + "'");
            int count_user = 0;
            while (reader.Read())
            {
                count_user++;
            }
            DatabaseService.Instance.getConnection().Close();
            //Kiểm tra email có không
            int count_user_email = 0;

            reader = serviceBase.query("SELECT TOP 1 * FROM Users WHERE email='" + email + "' AND merchant_id ='" + merchant_id + "'");

            while (reader.Read())
            {
                count_user_email++;
            }
            DatabaseService.Instance.getConnection().Close();

            if (count_user == 0 && count_user_email == 0)
            {
                reader = serviceBase.query("INSERT INTO Users (username,password,email,merchant_id,status_active,code,verify,gmail_id) " +
                    "VALUES('','','" + email + "'," + merchant_id + ",'active','',1,'" + gmail_id + "')");

                DatabaseService.Instance.getConnection().Close();
            }
            if (count_user > 0 && count_user_email == 0)
            {
                reader = serviceBase.query("UPDATE Users SET email='" + email + "' WHERE gmail_id='" + gmail_id + "' AND merchant_id ='" + merchant_id + "' ");

                DatabaseService.Instance.getConnection().Close();
            }
            if (count_user_email > 0 && count_user == 0)
            {
                reader = serviceBase.query("UPDATE Users SET gmail_id='" + gmail_id + "' WHERE email='" + email + "' AND merchant_id ='" + merchant_id + "' ");

                DatabaseService.Instance.getConnection().Close();
            }

            //Lấy info
            reader = serviceBase.query("SELECT TOP 1 * FROM Users WHERE gmail_id='" + gmail_id + "' AND merchant_id ='" + merchant_id + "' ");

            Users users = new Users();
            while (reader.Read())
            {
                users.id = Int32.Parse(reader["id"].ToString());
                users.username = reader["username"].ToString();
                users.email = reader["email"].ToString();
                if (reader["status_active"].ToString() == "active")
                {
                    users.isActive = EnumUserModelActiveStatus.active;
                }
                else
                {
                    users.isActive = EnumUserModelActiveStatus.deactive;
                }
                users.verify = Int32.Parse(reader["verify"].ToString());
                users.verify_phone = Int32.Parse(reader["verify_phone"].ToString());
                users.phone = reader["phone"].ToString().Trim();
            }
            DatabaseService.Instance.getConnection().Close();
            string token_gen = TokenHelper.GenerateToken(users.id.ToString());
            return Json(new
            {
                error = false,
                token = token_gen,
                user = users
            });
        }
        [HttpPost]
        public ActionResult getWallet(string token)
        {
            using (var db = new GamePaymentContext())
            {
                token = token.Trim();
                string idUser = TokenHelper.ValidateToken(token);
                if (idUser.Equals(""))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Token fail."
                    });
                }
                int userID = Int32.Parse(idUser.ToString());
                var userDB = db.User.FirstOrDefault(e => e.id == userID);
                if (userDB.status_active.Trim().Equals("deactive"))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Tài khoản đã bị khóa."
                    });
                }
                ServiceBase serviceBase = new ServiceBase();
                SqlDataReader reader;
                reader = serviceBase.query("SELECT " +
                "TOP 20 " +
                "Merchant_Wallet.wallet_id," +
                "Wallet.name," +
                "Merchant_Wallet.merchant_id " +
                "FROM Merchant_Wallet INNER JOIN Wallet ON Merchant_Wallet.wallet_id = Wallet.id WHERE Merchant_Wallet.merchant_id =" + userDB.merchant_id + " ORDER BY Merchant_Wallet.wallet_id ASC");
                //Xem query có lỗi ko

                //Lấy danh sách Merchant_coin
                List<Merchant_WalletUser> list_mer = new List<Merchant_WalletUser>();
                while (reader.Read())
                {
                    Merchant_WalletUser merchant_wallet = new Merchant_WalletUser(userDB.merchant_id, Int32.Parse(reader["wallet_id"].ToString()), reader["name"].ToString());
                    list_mer.Add(merchant_wallet);
                }
                DatabaseService.Instance.getConnection().Close();

                //Danh sách ví
                List<WalletServiceUser> list_w = new List<WalletServiceUser>();
                Users users = new Users();
                foreach (var item in list_mer)
                {
                    //Lấy thông tin coin
                    //Lấy tiền dư trong ví lưu ở transaction
                    double balance = item.getBalance(userDB);
                    WalletServiceUser walletService = new WalletServiceUser();
                    walletService.id = item.wallet_id;
                    walletService.name = item.wallet_name;
                    walletService.balance = balance;
                    walletService.name_coin = item.getNameCoin();
                    list_w.Add(walletService);
                }
                return Json(new
                {
                    error = false,
                    userid = userDB.id,
                    wallets = list_w
                });
            }
        }
    }
}
