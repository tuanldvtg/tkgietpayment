﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers.APIService
{
    public class APIPaymentServiceController : Controller
    {
        private int app_id;
        private string get_accessToken()
        {
            string client_id = "487719171387-isfcshdgpfbhsurd0dbp5lbcn8rrj2qc.apps.googleusercontent.com";
            string client_secret = "LJpLH0e-XzagtEdCQnGk9xfT";
            string refresh_token = "1/JVGsbd3YUnbqLbQK-v9NwVEc_fCSLTRmbo4U8PsdZGg";

            var request = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");

            string postData = string.Format("grant_type=refresh_token&client_id={0}&client_secret={1}&refresh_token={2}", client_id, client_secret, refresh_token);

            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;


            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            Accesstoken accesstoken = new Accesstoken();
            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                accesstoken = JsonConvert.DeserializeObject<Accesstoken>(responseString);
            }
            catch (Exception)
            {
                return "";
            }

            return accesstoken.access_token;

        }
        private int getOrderAndroid(string purchaseToken, string packageName, string productID, string accesstoken)
        {
            try
            {
                WebClient n = new WebClient();
                var json = n.DownloadString("https://www.googleapis.com/androidpublisher/v2/applications/" + packageName + "/purchases/products/" + productID + "/tokens/" + purchaseToken + "?access_token=" + accesstoken);
                string valueOriginal = Convert.ToString(json);
                ResponseOrder responseOrder = JsonConvert.DeserializeObject<ResponseOrder>(valueOriginal);
                return responseOrder.purchaseState;
            }
            catch (Exception)
            {
                return 2;
            }

        }
        
        private double getPriceProduct(string packageName, string productID, string accesstoken)
        {
            try
            {
                WebClient n = new WebClient();
                var json = n.DownloadString("https://www.googleapis.com/androidpublisher/v2/applications/" + packageName + "/inappproducts/" + productID + "?access_token=" + accesstoken);
                string valueOriginal = Convert.ToString(json);
                ResponsePrice responseOrder = JsonConvert.DeserializeObject<ResponsePrice>(valueOriginal);
                return responseOrder.defaultPrice.priceMicros;
            }
            catch (Exception)
            {
                return 0;
            }

        }
        //Query lấy cổng thanh toán
        private PaymentGate getPaymentGate(string paymentgate_code)
        {
            using (var db = new GamePaymentContext())
            {
                var paymentGateService = db.PaymentGate.FirstOrDefault(g => g.code == paymentgate_code);
                return paymentGateService;
            }
        }
        //Kiểm tra tham số trừ tiền có đúng config
        private CashIORate checkValueConfig(PaymentGate paymentGateService, double value)
        {
            CashIORate cashIORate = new CashIORate();
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            string query = "SELECT TOP 1 payment_gate_id,cash_out,cash_in FROM CashIORate WHERE payment_gate_id =@payment_gate_id AND cash_in >= @value ORDER BY cash_in ASC ";
            SqlCommand command = new SqlCommand(query, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@payment_gate_id", paymentGateService.id);
            command.Parameters.AddWithValue("@value", value);
            reader = serviceBase.querySqlParam(command);
            while (reader.Read())
            {
                cashIORate.payment_gate_id = Int32.Parse(reader["payment_gate_id"].ToString());
                cashIORate.cash_out = Int32.Parse(reader["cash_out"].ToString());
                cashIORate.cash_in = Int32.Parse(reader["cash_in"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return cashIORate;
        }
        //Lấy số tiền trong ví
        private double getBalace(int user_id, int wallet_in)
        {
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            double balance = 0;
            string query = "SELECT TOP 1 balance FROM Transactions WHERE wallet_id =@wallet_in " +
                "AND user_id=@user_id AND (transactions_type = 0 OR transactions_type IS NULL) AND merchant_id='" + this.app_id + "' ORDER BY id DESC";
            SqlCommand command = new SqlCommand(query, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@user_id", user_id);
            command.Parameters.AddWithValue("@wallet_in", wallet_in);
            reader = serviceBase.querySqlParam(command);
            while (reader.Read())
            {
                balance = double.Parse(reader["balance"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return balance;
        }
        //Lấy số tiền trong ví
        private double getBalaceUser(int user_id, int wallet_in)
        {
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            double balance = 0;
            string query = "SELECT TOP 1 balance FROM Transactions WHERE wallet_id =@wallet_in " +
                "AND user_id=@user_id AND (transactions_type = 0 OR transactions_type IS NULL) AND type= 'plus' ORDER BY id DESC";
            SqlCommand command = new SqlCommand(query, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@user_id", user_id);
            command.Parameters.AddWithValue("@wallet_in", wallet_in);
            reader = serviceBase.querySqlParam(command);
            while (reader.Read())
            {
                balance = double.Parse(reader["balance"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return balance;
        }

        //Lấy số tiền trong ví nạp của admin
        private double getBalaceInput()
        {
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            double balance = 0;
            reader = serviceBase.query("SELECT TOP 1 balance FROM Transactions WHERE wallet_id =0 " +
                "AND user_id=0 AND type ='plus' ORDER BY id DESC");
            while (reader.Read())
            {
                balance = double.Parse(reader["balance"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return balance;
        }
        public ActionResult InAppPurchaseAndroid(string purchaseToken,string token, string packageName, string productID, string paymentgate_code)
        {
           
            using (var db = new GamePaymentContext())
            {
                token = token.Trim();
                string idUser = TokenHelper.ValidateToken(token);
                if (idUser.Equals(""))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Token fail."
                    });
                }
                int userID = Int32.Parse(idUser.ToString());
                var userDB = db.User.FirstOrDefault(e => e.id == userID);
                this.app_id = userDB.merchant_id;
                if (userDB.status_active.Trim().Equals("deactive"))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Tài khoản đã bị khóa."
                    });
                }
                ServiceBase serviceBase = new ServiceBase();
                SqlDataReader reader;

                //Lấy access token
                string access_token = this.get_accessToken();
                if (access_token.Equals(""))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Kiểm tra thông tin order lỗi."
                    });
                }
                //Kiểm tra đơn hàng thanh toán
                int state = this.getOrderAndroid(purchaseToken, packageName, productID, access_token);
                if (state != 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Đơn hàng chưa được thanh toán."
                    });
                }
                //Lấy giá tiền sản phẩm
                double price = this.getPriceProduct(packageName, productID, access_token);
                if (price == 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Lấy giá tiền nạp gặp lỗi."
                    });
                }
                price = price / 1000000;

                if (this.app_id == 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Chưa set app id."
                    });
                }


                //Kieemr tra cong thanh toan
                PaymentGate paymentGateService = this.getPaymentGate(paymentgate_code.Trim());
                if (paymentGateService.id == 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Cổng thanh toán không tồn tại."
                    });
                }


                if (!paymentGateService.status_active.Trim().Equals("active"))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Cổng thanh toán đang bị khóa ."
                    });
                }

                //Kiểm tra tham số trừ tiền có đúng config
                CashIORate cashIORate;
                try
                {
                    cashIORate = this.checkValueConfig(paymentGateService, price);
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Kiểm tra config lỗi: " + ex.Message
                    });
                }

                if (cashIORate.cash_in == 0)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Số tiền vượt quá config được chuyển "
                    });
                }


                //Update tiên cho admin
                double balance = this.getBalaceInput();
                double profit = 0;
                double plus_money_profit = 0;
                plus_money_profit = double.Parse(price.ToString()) / cashIORate.cash_in * cashIORate.cash_out;
                plus_money_profit = Math.Round(plus_money_profit, 2, MidpointRounding.ToEven);


                balance = balance + price;
                DateTime time = DateTime.Now;
                // Use current time
                string format = "yyyy-MM-dd HH:mm:ss";
                reader = serviceBase.query("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) " +
                    "VALUES (0," + this.app_id + "," + paymentGateService.id + ",0," + balance + ",'plus','" + time.ToString(format) + "'," + price + "," + userDB.id + ",N'naptien',N'User nạp tiền'," + profit + "," + plus_money_profit + ")");

                DatabaseService.Instance.getConnection().Close();

                //Update tiền cho user
                double balance_user = this.getBalace(userDB.id, 0);
                balance_user = balance_user + price;

                reader = serviceBase.query("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) " +
        "VALUES (" + userDB.id + "," + this.app_id + "," + paymentGateService.id + ",0," + balance_user + ",'plus','" + time.ToString(format) + "'," + price + ",0,N'naptien',N'User nạp tiền'," + profit + "," + plus_money_profit + ")");

                DatabaseService.Instance.getConnection().Close();


                //Tính số tiền trong ví để cộng
                double balance_out = 0;
                try
                {
                    balance_out = this.getBalace(userDB.id, paymentGateService.wallet_out);
                }
                catch (Exception )
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Lỗi lấy tổng tiền trong ví user để cộng"
                    });
                }

                balance_out = balance_out + plus_money_profit;
                reader = serviceBase.query("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message) VALUES (" +
        userDB.id + "," + this.app_id + "," + paymentGateService.id + "," + paymentGateService.wallet_out + "," + balance_out + ",'plus','" + time.ToString(format) + "'," + plus_money_profit + ",0,N'naptien',N'Nạp tiền')");
                DatabaseService.Instance.getConnection().Close();
                return Json(new
                {
                    error = false
                });
            }


            

        }
    }
}