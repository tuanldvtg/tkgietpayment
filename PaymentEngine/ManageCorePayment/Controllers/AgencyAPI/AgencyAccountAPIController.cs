﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Exceptions;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers.AgencyAPI
{ 
    public class AgencyAccountAPIController : Controller
    {
        public DateTime[] GetMonthBetween(DateTime startDate, DateTime endDate)
        {
            if (endDate > DateTime.Now)
            {
                endDate = DateTime.Now;
            }

            List<DateTime> allDates = new List<DateTime>();
            for (DateTime date = startDate; date <= endDate; date = date.AddMonths(1))
                allDates.Add(date);
            return allDates.ToArray();
        }

        private DateTime[] GetDatesBetween(DateTime startDate, DateTime endDate)
        {
            if (endDate > DateTime.Now)
            {
                endDate = DateTime.Now;
            }
            List<DateTime> allDates = new List<DateTime>();
            for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                allDates.Add(date);
            return allDates.ToArray();
        }

        /**
         * Return all agency wallets
         **/
        [HttpPost]
        public ActionResult WalletsNew(string start_time,string end_time)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }

            var agencyID = (new AuthenticationHelper(Session).ID());
            using (var db = new GamePaymentContext())
            {
                ServiceBase serviceBase = new ServiceBase();
                //SqlDataReader reader;

                DateTime startTime = DateTime.Parse(start_time);
                DateTime endTime = DateTime.Parse(end_time);
                
                var query_transactions_minus = db.Transactions
                                .OrderByDescending(p => p.id)
                                .Where(e => (e.user_id == agencyID && e.time_creat > startTime && e.time_creat <=endTime && e.transactions_type==2&&e.type=="minus")).ToArray();
                double agencyMoneyMinus = 0;
                if (query_transactions_minus != null)
                {
                    foreach (var i in query_transactions_minus)
                    {
                        agencyMoneyMinus = i.balance;
                    }
                }

                var query_transactions_plus = db.Transactions
                                .OrderByDescending(p => p.id)
                                .Where(e => (e.user_id == agencyID && e.time_creat > startTime && e.time_creat <= endTime && e.transactions_type == 2 && e.type == "plus")).ToArray();
                double agencyMoneyPlus = 0;
                if (query_transactions_plus != null)
                {
                    foreach (var i in query_transactions_plus)
                    {
                        agencyMoneyPlus = i.balance;
                    }
                }

                return Json(new
                {
                    error = false,
                    msg = "Lấy dữ liệu thành công!",
                    tongNap= agencyMoneyPlus,
                    tongRut = agencyMoneyMinus
                });
            }
            //catch (AccountNotfoundException )
            //{
            //return Json(new
            //{
            //    error = true,
            //    msg = "Mật khẩu của bạn không đúng, vui lòng thử lại!"
            //});
            //}
            //catch (Exception e)
            //{
            //    return Json(new
            //    {
            //        error = true,
            //        msg = e.Message.ToString()
            //    });
            //}
        }
        /**
        * Return all agency wallets
        **/
        [HttpPost]
        public ActionResult WalletsMonth(string start_time, string end_time)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var agencyID = (new AuthenticationHelper(Session).ID());

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string[] a_start_time_new = start_time.Split('/');
            string start_time_new = a_start_time_new[2] + "-" + a_start_time_new[0] + "-" + a_start_time_new[1] + " 00:00:00.000";

            string[] a_end_time_new = end_time.Split('/');
            string end_time_new = a_end_time_new[2] + "-" + a_end_time_new[0] + "-" + a_end_time_new[1] + " 23:59:59.999";
            DateTime[] dates = this.GetMonthBetween(DateTime.Parse(start_time_new), DateTime.Parse(end_time_new));
            if (dates.Length > 31)
            {
                return Json(new
                {
                    error = true,
                    msg = "Số ngày chọn quá lớn không thể thống kê."
                });
            }



            var walletResults = new[] { new {
                    id = 0,
                    name = "",
                    balance = "0.00",
                    history = new[]
                    {
                        new
                        {
                             date = "",
                            value = 0.1
                        }
                    }.ToList()
                }}.ToList();
            walletResults.Clear();

            try
            {
                // Get all managet wallets
                using (var db = new GamePaymentContext())
                {
                    var wallets = (new AgencyPrivateServices()).getWallets(agencyID);


                    wallets.ToList().ForEach(w =>
                    {
                        var historyStruct = new[] { new {
                            date = "",
                            value = 0.1
                        }}.ToList();
                        historyStruct.Clear();

                        //Tìm giao dịch nạp rút trong khoảng thời gian

                        var time_startParameter = new SqlParameter("@timestart", start_time_new);
                        var time_endParameter = new SqlParameter("@timeend", end_time_new);
                        var usersend_idParameter = new SqlParameter("@agency_id", agencyID);
                        var walletid_idParameter = new SqlParameter("@wallet_id", w.id);

                        var result = db.Database
                            .SqlQuery<Pro_statistics_agency_wallet_month>("Pro_statistics_agency_wallet_month @agency_id ,  @timestart , @timeend , @wallet_id", usersend_idParameter, time_startParameter, time_endParameter, walletid_idParameter).ToList();

                        //Tính nạp rút tất cả thời gian thống kê dựa trên kết quả nhận được
                        Pro_statistics_agency_wallet[] list_date_query = new Pro_statistics_agency_wallet[dates.Length];
                        for (int i = 0; i < list_date_query.Length; i++)
                        {
                            bool check_is = false;

                            result.ForEach(ent =>
                            {
                                if (dates[i].Year == ent.year && dates[i].Month == ent.month)
                                {
                                    check_is = true;
                                    Pro_statistics_agency_wallet pro_statistics = new Pro_statistics_agency_wallet();
                                    if (ent.balance != 0)
                                    {
                                        pro_statistics.date = dates[i];
                                        pro_statistics.balance = ent.balance;
                                    }
                                    else
                                    {
                                        if (i == 0)
                                        {
                                            string queryStringBalance = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + agencyID + " AND wallet_id =  " + w.id + " AND transactions_type = 2 AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                                            reader = serviceBase.query(queryStringBalance);
                                            double balance = 0;
                                            while (reader.Read())
                                            {
                                                balance = Double.Parse(reader["balance"].ToString());
                                            }
                                            reader.Close();
                                            DatabaseService.Instance.getConnection().Close();
                                            pro_statistics.balance = balance;
                                        }
                                        else
                                        {
                                            pro_statistics.date = dates[i];
                                            pro_statistics.balance = list_date_query[i - 1].balance;
                                        }
                                    }

                                    list_date_query[i] = pro_statistics;
                                }
                            });

                            if (!check_is)
                            {
                                if (i > 0)
                                {
                                    Pro_statistics_agency_wallet pro_statistics = new Pro_statistics_agency_wallet();
                                    pro_statistics.date = dates[i];
                                    pro_statistics.balance = list_date_query[i - 1].balance;
                                    list_date_query[i] = pro_statistics;
                                }
                                else
                                {
                                    string queryStringBalance = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + agencyID + " AND wallet_id = " + w.id + " AND transactions_type = 2 AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                                    reader = serviceBase.query(queryStringBalance);
                                    double balance = 0;
                                    while (reader.Read())
                                    {
                                        balance = Double.Parse(reader["balance"].ToString());
                                    }
                                    reader.Close();
                                    DatabaseService.Instance.getConnection().Close();
                                    
                                    Pro_statistics_agency_wallet pro_statistics = new Pro_statistics_agency_wallet();
                                    pro_statistics.date = dates[i];
                                    pro_statistics.balance = balance;

                                    list_date_query[i] = pro_statistics;
                                }

                            }
                        }
                        for (int i = 0; i < list_date_query.Length; i++)
                        {
                            historyStruct.Add(new
                            {
                                date = list_date_query[i].date.ToString("d/M"),
                                value = list_date_query[i].balance
                            });
                        }

                        var total_nowbalance = historyStruct.Last().value;

                        walletResults.Add(new
                        {
                            id = w.id,
                            name = w.name,
                            balance = Double.Parse(total_nowbalance.ToString()).ToString("N", CultureInfo.InvariantCulture),
                            history = historyStruct
                        });

                    });
                    return Json(new
                    {
                        error = false,
                        items = walletResults
                    });
                }


            }
            catch (AccountNotfoundException )
            {
                return Json(new
                {
                    error = true,
                    msg = "Mật khẩu của bạn không đúng, vui lòng thử lại!"
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }

        //---lich su giao dich
        /**
         * Return all agency wallets
         **/
        [HttpPost]
        public ActionResult Wallets(string start_time, string end_time)
        {
            if (start_time == null || end_time == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            DateTime dDate;

            if (!DateTime.TryParse(start_time, out dDate))
            {
                return Json(new
                {
                    error = true,
                    msg = "start_time không đúng định dạng"
                });
            }
            if (!DateTime.TryParse(end_time, out dDate))
            {
                return Json(new
                {
                    error = true,
                    msg = "end_time không đúng định dạng"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }

            var agencyID = (new AuthenticationHelper(Session).ID());

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string[] a_start_time_new = start_time.Split('/');
            string start_time_new = a_start_time_new[2] + "-" + a_start_time_new[0] + "-" + a_start_time_new[1] + " 00:00:00.000";

            string[] a_end_time_new = end_time.Split('/');
            string end_time_new = a_end_time_new[2] + "-" + a_end_time_new[0] + "-" + a_end_time_new[1] + " 23:59:59.999";
            DateTime[] dates = this.GetDatesBetween(DateTime.Parse(start_time_new), DateTime.Parse(end_time_new));
            if (dates.Length > 31)
            {
                return Json(new
                {
                    error = true,
                    msg = "Số ngày chọn quá lớn không thể thống kê."
                });
            }



            var walletResults = new[] { new {
                    id = 0,
                    name = "",
                    balance = "0.00",
                    history = new[]
                    {
                        new
                        {
                             date = "",
                            value = 0.1
                        }
                    }.ToList()
                }}.ToList();
            walletResults.Clear();

            try
            {
                // Get all managet wallets
                using (var db = new GamePaymentContext())
                {
                    var agencyLogin = db.Agency.FirstOrDefault(a => (a.id == agencyID));
                    var agencyLogin_id = agencyLogin.id;
                    if (agencyLogin.subid != 0)
                    {
                        agencyLogin_id = agencyLogin.subid;
                    }
                    var wallets = (new AgencyPrivateServices()).getWallets(agencyLogin_id);


                    wallets.ToList().ForEach(w =>
                    {
                        var historyStruct = new[] { new {
                            date = "",
                            value = 0.1
                        }}.ToList();
                        historyStruct.Clear();

                        //Tìm giao dịch nạp rút trong khoảng thời gian

                        var time_startParameter = new SqlParameter("@timestart", start_time_new);
                        var time_endParameter = new SqlParameter("@timeend", end_time_new);
                        var usersend_idParameter = new SqlParameter("@agency_id", agencyID);
                        var walletid_idParameter = new SqlParameter("@wallet_id", w.id);

                        var result = db.Database
                            .SqlQuery<Pro_statistics_agency_wallet>("Pro_statistics_agency_wallet @agency_id ,  @timestart , @timeend , @wallet_id", usersend_idParameter, time_startParameter, time_endParameter, walletid_idParameter).ToList();

                        //Tính nạp rút tất cả thời gian thống kê dựa trên kết quả nhận được
                        Pro_statistics_agency_wallet[] list_date_query = new Pro_statistics_agency_wallet[dates.Length];
                        for (int i = 0; i < list_date_query.Length; i++)
                        {
                            bool check_is = false;

                            result.ForEach(ent =>
                            {
                                if (dates[i] == ent.date)
                                {
                                    check_is = true;
                                    Pro_statistics_agency_wallet pro_statistics = new Pro_statistics_agency_wallet();
                                    if (ent.balance != 0)
                                    {
                                        pro_statistics.date = dates[i];
                                        pro_statistics.balance = ent.balance;
                                    }
                                    else
                                    {
                                        if (i == 0)
                                        {
                                            string queryStringBalance = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + agencyID + " AND wallet_id =  " + w.id + " AND transactions_type = 2 AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                                            reader = serviceBase.query(queryStringBalance);
                                            double balance = 0;
                                            while (reader.Read())
                                            {
                                                balance = Double.Parse(reader["balance"].ToString());
                                            }
                                            DatabaseService.Instance.getConnection().Close();
                                            pro_statistics.balance = balance;
                                        }
                                        else
                                        {
                                            pro_statistics.date = dates[i];
                                            pro_statistics.balance = list_date_query[i - 1].balance;
                                        }
                                    }

                                    list_date_query[i] = pro_statistics;
                                }
                            });

                            if (!check_is)
                            {
                                if (i > 0)
                                {
                                    Pro_statistics_agency_wallet pro_statistics = new Pro_statistics_agency_wallet();
                                    pro_statistics.date = dates[i];
                                    pro_statistics.balance = list_date_query[i - 1].balance;
                                    list_date_query[i] = pro_statistics;
                                }
                                else
                                {
                                    string queryStringBalance = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + agencyID + " AND wallet_id = " + w.id + " AND transactions_type = 2 AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                                    reader = serviceBase.query(queryStringBalance);
                                    double balance = 0;
                                    while (reader.Read())
                                    {
                                        balance = Double.Parse(reader["balance"].ToString());
                                    }
                                    reader.Close();
                                    DatabaseService.Instance.getConnection().Close();
                                    
                                    Pro_statistics_agency_wallet pro_statistics = new Pro_statistics_agency_wallet();
                                    pro_statistics.date = dates[i];
                                    pro_statistics.balance = balance;

                                    list_date_query[i] = pro_statistics;
                                }

                            }
                        }
                        for (int i = 0; i < list_date_query.Length; i++)
                        {
                            historyStruct.Add(new
                            {
                                date = list_date_query[i].date.ToString("d/M"),
                                value = list_date_query[i].balance
                            });
                        }

                        var total_nowbalance = historyStruct.Last().value;

                        walletResults.Add(new
                        {
                            id = w.id,
                            name = w.name,
                            balance = Double.Parse(total_nowbalance.ToString()).ToString("N", CultureInfo.InvariantCulture),
                            history = historyStruct
                        });

                    });
                    return Json(new
                    {
                        error = false,
                        items = walletResults
                    });
                }


            }
            catch (AccountNotfoundException)
            {
                return Json(new
                {
                    error = true,
                    msg = "Mật khẩu của bạn không đúng, vui lòng thử lại!"
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }
        /**
        * Return all agency wallets
        **/
        [HttpPost]
        public ActionResult WalletsMonthNew(string start_time, string end_time)
        {
            if (start_time == null || end_time == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            DateTime dDate;

            if (!DateTime.TryParse(start_time, out dDate))
            {
                return Json(new
                {
                    error = true,
                    msg = "start_time không đúng định dạng"
                });
            }
            if (!DateTime.TryParse(end_time, out dDate))
            {
                return Json(new
                {
                    error = true,
                    msg = "end_time không đúng định dạng"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var agencyID = (new AuthenticationHelper(Session).ID());

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string[] a_start_time_new = start_time.Split('/');
            string start_time_new = a_start_time_new[2] + "-" + a_start_time_new[0] + "-" + a_start_time_new[1] + " 00:00:00.000";

            string[] a_end_time_new = end_time.Split('/');
            string end_time_new = a_end_time_new[2] + "-" + a_end_time_new[0] + "-" + a_end_time_new[1] + " 23:59:59.999";
            DateTime[] dates = this.GetMonthBetween(DateTime.Parse(start_time_new), DateTime.Parse(end_time_new));
            if (dates.Length > 31)
            {
                return Json(new
                {
                    error = true,
                    msg = "Số ngày chọn quá lớn không thể thống kê."
                });
            }



            var walletResults = new[] { new {
                    id = 0,
                    name = "",
                    balance = "0.00",
                    history = new[]
                    {
                        new
                        {
                             date = "",
                            value = 0.1
                        }
                    }.ToList()
                }}.ToList();
            walletResults.Clear();

            try
            {
                // Get all managet wallets
                using (var db = new GamePaymentContext())
                {
                    var wallets = (new AgencyPrivateServices()).getWallets(agencyID);


                    wallets.ToList().ForEach(w =>
                    {
                        var historyStruct = new[] { new {
                            date = "",
                            value = 0.1
                        }}.ToList();
                        historyStruct.Clear();

                        //Tìm giao dịch nạp rút trong khoảng thời gian

                        var time_startParameter = new SqlParameter("@timestart", start_time_new);
                        var time_endParameter = new SqlParameter("@timeend", end_time_new);
                        var usersend_idParameter = new SqlParameter("@agency_id", agencyID);
                        var walletid_idParameter = new SqlParameter("@wallet_id", w.id);

                        var result = db.Database
                            .SqlQuery<Pro_statistics_agency_wallet_month>("Pro_statistics_agency_wallet_month @agency_id ,  @timestart , @timeend , @wallet_id", usersend_idParameter, time_startParameter, time_endParameter, walletid_idParameter).ToList();

                        //Tính nạp rút tất cả thời gian thống kê dựa trên kết quả nhận được
                        Pro_statistics_agency_wallet[] list_date_query = new Pro_statistics_agency_wallet[dates.Length];
                        for (int i = 0; i < list_date_query.Length; i++)
                        {
                            bool check_is = false;

                            result.ForEach(ent =>
                            {
                                if (dates[i].Year == ent.year && dates[i].Month == ent.month)
                                {
                                    check_is = true;
                                    Pro_statistics_agency_wallet pro_statistics = new Pro_statistics_agency_wallet();
                                    if (ent.balance != 0)
                                    {
                                        pro_statistics.date = dates[i];
                                        pro_statistics.balance = ent.balance;
                                    }
                                    else
                                    {
                                        if (i == 0)
                                        {
                                            string queryStringBalance = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + agencyID + " AND wallet_id =  " + w.id + " AND transactions_type = 2 AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                                            reader = serviceBase.query(queryStringBalance);
                                            double balance = 0;
                                            while (reader.Read())
                                            {
                                                balance = Double.Parse(reader["balance"].ToString());
                                            }
                                            reader.Close();
                                            DatabaseService.Instance.getConnection().Close();
                                            pro_statistics.balance = balance;
                                        }
                                        else
                                        {
                                            pro_statistics.date = dates[i];
                                            pro_statistics.balance = list_date_query[i - 1].balance;
                                        }
                                    }

                                    list_date_query[i] = pro_statistics;
                                }
                            });

                            if (!check_is)
                            {
                                if (i > 0)
                                {
                                    Pro_statistics_agency_wallet pro_statistics = new Pro_statistics_agency_wallet();
                                    pro_statistics.date = dates[i];
                                    pro_statistics.balance = list_date_query[i - 1].balance;
                                    list_date_query[i] = pro_statistics;
                                }
                                else
                                {
                                    string queryStringBalance = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + agencyID + " AND wallet_id = " + w.id + " AND transactions_type = 2 AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                                    reader = serviceBase.query(queryStringBalance);
                                    double balance = 0;
                                    while (reader.Read())
                                    {
                                        balance = Double.Parse(reader["balance"].ToString());
                                    }
                                    reader.Close();
                                    DatabaseService.Instance.getConnection().Close();
                                    
                                    Pro_statistics_agency_wallet pro_statistics = new Pro_statistics_agency_wallet();
                                    pro_statistics.date = dates[i];
                                    pro_statistics.balance = balance;

                                    list_date_query[i] = pro_statistics;
                                }

                            }
                        }
                        for (int i = 0; i < list_date_query.Length; i++)
                        {
                            historyStruct.Add(new
                            {
                                date = list_date_query[i].date.ToString("d/M"),
                                value = list_date_query[i].balance
                            });
                        }

                        var total_nowbalance = historyStruct.Last().value;

                        walletResults.Add(new
                        {
                            id = w.id,
                            name = w.name,
                            balance = Double.Parse(total_nowbalance.ToString()).ToString("N", CultureInfo.InvariantCulture),
                            history = historyStruct
                        });

                    });
                    return Json(new
                    {
                        error = false,
                        items = walletResults
                    });
                }


            }
            catch (AccountNotfoundException)
            {
                return Json(new
                {
                    error = true,
                    msg = "Mật khẩu của bạn không đúng, vui lòng thử lại!"
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }

        [HttpPost]
        public ActionResult getListSocial(Agency_social[] socials)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            // Check auth type
            var auth = new AuthenticationHelper(Session);
            if (!auth.isLogined())
            {
                return Json(new
                {
                    error = true,
                    msg = "Hết hạn phiên đăng nhập !"
                });
            }
            int agency_id = auth.ID();
            using (var db = new GamePaymentContext())
            {
                var query = db.Agency_social.Where(a => a.agency_id == agency_id).ToList();
                return Json(new
                {
                    error = false,
                    items = query
                });
            }


        }
        [HttpPost]
        public ActionResult doUpdateSocial(Agency_social[] socials)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            // Check auth type
            var auth = new AuthenticationHelper(Session);
            if (!auth.isLogined())
            {
                return Json(new
                {
                    error = true,
                    msg = "Hết hạn phiên đăng nhập !"
                });
            }
            int agency_id = auth.ID();
            using (var db = new GamePaymentContext())
            {
                if (socials == null)
                {
                    var query_list = db.Agency_social.Where(a => a.agency_id == agency_id).ToList();
                    query_list.ForEach(ent =>
                    {
                            db.Agency_social.Remove(ent);
                            db.SaveChanges();
                    });
                }
                foreach (Agency_social social in socials)
                {
                    if (social.id == 0)
                    {
                        social.agency_id = auth.ID();
                        db.Agency_social.Add(social);
                        db.SaveChanges();
                    }
                    else
                    {
                        var item_agency_social = db.Agency_social.Where(a => a.id == social.id).FirstOrDefault();
                        item_agency_social.title = social.title;
                        item_agency_social.link = social.link;
                        item_agency_social.view_user = social.view_user;
                        db.SaveChanges();
                    }
                }
                
                var query = db.Agency_social.Where(a => a.agency_id == agency_id).ToList();
                query.ForEach(ent =>
                 {
                     bool check = false;
                     foreach (Agency_social social in socials)
                     {
                         if (social.id == ent.id)
                         {
                             check = true;
                         }
                     }
                     if (!check)
                     {
                         db.Agency_social.Remove(ent);
                         db.SaveChanges();
                     }
                 });

            };

            return Json(new
            {
                error = false
            });
        }

        [HttpPost]
        public ActionResult ChangePassword(string oldpass, string password)
        {
            if (oldpass == null || password == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            try
            {
                // Check auth type
                var auth = new AuthenticationHelper(Session);
                if (!auth.isLogined())
                {
                    throw new Exception("Hết hạn phiên đăng nhập !");
                }
                if (auth.getRole() == SessionKeyMode.Agency)
                {
                    (new AgencyPrivateServices()).changePassword(auth.ID(), oldpass, password);
                    return Json(new
                    {
                        error = false,
                        msg = "Cập nhật hoàn tất !"
                    });
                }


                if (auth.getRole() == SessionKeyMode.Admin || auth.getRole() == SessionKeyMode.Accounting)
                {
                    (new ManagerPrivateServices()).changePassword(auth.ID(), oldpass, password);
                    return Json(new
                    {
                        error = false,
                        msg = "Thay đổi mật khẩu hoàn tất !"
                    });
                }

                return Json(new
                {
                    error = true,
                    msg = "Unknow role"
                });

            }
            catch (AccountNotfoundException )
            {
                return Json(new
                {
                    error = true,
                    msg = "Mật khẩu của bạn không đúng, vui lòng thử lại!"
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }
    }
}
