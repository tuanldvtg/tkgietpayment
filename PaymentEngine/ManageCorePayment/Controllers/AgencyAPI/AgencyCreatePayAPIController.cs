﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers.AgencyAPI
{
    public class AgencyCreatePayAPIController : Controller
    {
        
        public ActionResult changeTag(int id)
        {
            if (id == 0)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số id null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "UPDATE Transactions SET tag_type = 1 WHERE id =@id";
            SqlCommand command;
            SqlDataReader reader;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@id", id);
            reader = serviceBase.querySqlParam(command);
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false
            });
        }
        public ActionResult clickChangeTagRemove(int id)
        {
            if (id == 0)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số id null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "UPDATE Transactions SET tag_type = '' WHERE id =@id";
            SqlCommand command;
            SqlDataReader reader;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@id", id);
            reader = serviceBase.querySqlParam(command);
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false
            });
        }


        [HttpPost]
        public ActionResult History(string walletid, int iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength,string tag_type)
        {
            if (walletid == null || iSortCol_0 == null || sSortDir_0 == null || iDisplayStart== null || iDisplayLength == null || tag_type == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var agencyID = (new AuthenticationHelper(Session).ID());
            string[] a_sort = new string[] { "Transactions.time_creat", "wallet_name", "Transactions.type", "Transactions.value", "Transactions.balance","user_name","Transactions.tag", "Transactions.message" };
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT" +
                " Transactions.id" +
                " ,Transactions.time_creat" +
                " ,(SELECT TOP 1 name FROM Wallet WHERE id = Transactions.wallet_id) as 'wallet_name'" +
                " ,Transactions.type" +
                " ,Transactions.value" +
                " ,Transactions.balance" + 
                " ,Transactions.tag" +
                " ,Transactions.message" +
                " ,Transactions.tag_type" +
                 " ,Transactions.user_send" +
                " ,(SELECT TOP 1 username FROM Users WHERE id = Transactions.user_send) as 'user_name'" +
                " FROM Transactions WHERE Transactions.user_id =@agencyID AND transactions_type = 2 ";

            string queryCount = "SELECT COUNT(id) as 'count' FROM Transactions WHERE user_id =@agencyID AND transactions_type = 2 ";
            if (!walletid.Equals("all"))
            {
                queryString += " AND Transactions.wallet_id =@walletid";
                queryCount += " AND wallet_id = @walletid";
            }

            if (!tag_type.Trim().Equals(""))
            {
                if (tag_type.Equals("1"))
                {
                    queryString += " AND Transactions.tag_type = '1'";
                    queryCount += " AND tag_type = '1'";
                }
                if (tag_type.Equals("2"))
                {
                    queryString += " AND Transactions.tag_type = '0'";
                    queryCount += " AND (tag_type = '0' OR tag_type IS NULL) ";
                }


            }
            if (iSortCol_0 > 6)
            {
                iSortCol_0 = 0;
            }
            if (sSortDir_0.Equals("desc"))
            {
                sSortDir_0 = "DESC";
            }
            else
            {
                sSortDir_0 = "ASC";
            }
            queryString += " ORDER BY " + a_sort[iSortCol_0] + " " + sSortDir_0 + " OFFSET @iDisplayStart ROWS FETCH NEXT @iDisplayLength ROWS ONLY";
            SqlCommand command;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@walletid", walletid);
            command.Parameters.AddWithValue("@agencyID", agencyID);
            command.Parameters.AddWithValue("@iDisplayStart", Int32Ext.toInt(iDisplayStart));
            command.Parameters.AddWithValue("@iDisplayLength", Int32Ext.toInt(iDisplayLength));
            SqlDataReader reader = serviceBase.querySqlParam(command);

            List<String[]> users = new List<String[]>();
            while (reader.Read())
            {
                string[] s_user = new string[8];
                s_user[0] = reader["time_creat"].ToString().Trim();
                s_user[1] = reader["wallet_name"].ToString().Trim();
                string s_type = "";
                if (reader["type"].ToString().Trim().Equals("plus"))
                {
                    s_user[2] = "<a style=\"color:#4CB052\" >Cộng tiền</a>";
                    s_type = "+";
                    s_user[3] = "<a style=\"color:#4CB052\" >" + s_type + Double.Parse(reader["value"].ToString().Trim()).ToString("N", CultureInfo.InvariantCulture) + "</a>";
                }
                else
                {
                    s_user[2] = "<a style=\"color:red\" >Trừ tiền</a>";
                     s_type = "-";
                    s_user[3] = "<a style=\"color:red\" >" + s_type + Double.Parse(reader["value"].ToString().Trim()).ToString("N", CultureInfo.InvariantCulture) + "</a>";
                }

               
                s_user[4] =Double.Parse(reader["balance"].ToString().Trim()).ToString("N", CultureInfo.InvariantCulture);
                s_user[5] = reader["user_name"].ToString().Trim();

                if (!reader["type"].ToString().Trim().Equals("plus"))
                {
                    if (!(reader["user_send"].ToString().Trim().Equals("0")))
                    {
                        if (reader["tag_type"].ToString().Trim().Equals("1"))
                        {
                            s_user[6] = " <a onclick=\"agency_dashboard.clickChangeTagRemove('" + reader["id"].ToString().Trim() + "','" + walletid + "')\" ><i class=\"fa fa-tag\"></i>Bỏ tag nợ</a> <span class=\"label label-danger\">Đang nợ</span>";
                        }
                        else
                        {
                            s_user[6] = " <a onclick=\"agency_dashboard.clickChangeTag('" + reader["id"].ToString().Trim() + "','" + walletid + "')\" ><i class=\"fa fa-tag\"></i>Gắn tag nợ</a>";
                        }
                    }
                   
                }
                else
                {
                    if (reader["tag_type"].ToString().Trim().Equals("1"))
                    {
                        s_user[6] = "<span class=\"label label-danger\">Đang nợ</span>";
                    }
                }
               
                
                s_user[7] = reader["message"].ToString().Trim();
                users.Add(s_user);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            int row = 0;
            command = new SqlCommand(queryCount, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@walletid", walletid);
            command.Parameters.AddWithValue("@agencyID", agencyID);
            reader = serviceBase.querySqlParam(command);
            while (reader.Read())
            {
                row = Int32Ext.toInt(reader["count"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength,
                queryString = queryString
            });
        }

        [HttpPost]
        public ActionResult HistoryTransaction(int naprut, String s_seach, String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength, string start_time, string end_time)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var agencyID = (new AuthenticationHelper(Session).ID());
            string[] a_sort = new string[] { "Transactions.time_creat", "wallet_name", "Transactions.type", "Transactions.value", "Transactions.balance", "user_name", "Transactions.tag", "Transactions.message" };
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT" +
                " Transactions.id" +
                " ,Transactions.time_creat" +
                " ,(SELECT TOP 1 name FROM Wallet WHERE id = Transactions.wallet_id) as 'wallet_name'" +
                " ,Transactions.type" +
                " ,Transactions.value" +
                " ,Transactions.balance" +
                " ,Transactions.tag" +
                " ,Transactions.message" +
                " ,Transactions.tag_type" +
                 " ,Transactions.user_send" +
                " ,(SELECT TOP 1 username FROM Users WHERE id = Transactions.user_send) as 'user_name'" +
                " FROM Transactions WHERE Transactions.user_id = " + agencyID + " AND transactions_type = 2 ";




            string queryCount = "SELECT COUNT(id) as 'count' FROM Transactions WHERE user_id = " + agencyID + " AND transactions_type = 2";

            switch (naprut)
            {
                case 0:

                    break;
                case 1:
                    queryString += " AND type = 'plus' ";
                    break;
                case 2:
                    queryString += " AND type = 'minus' ";
                    break;
            }

            if (sSortDir_0.Equals("desc"))
            {
                sSortDir_0 = "DESC";
            }
            else
            {
                sSortDir_0 = "ASC";
            }
            queryString += " ORDER BY " + a_sort[Int32Ext.toInt(iSortCol_0)] + " " + sSortDir_0 + " OFFSET @iDisplayStart ROWS FETCH NEXT @iDisplayLength ROWS ONLY";
            // SqlDataReader reader = serviceBase.query(queryString);
            SqlCommand command;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@iDisplayStart", Int32Ext.toInt(iDisplayStart));
            command.Parameters.AddWithValue("@iDisplayLength", Int32Ext.toInt(iDisplayLength));
            SqlDataReader reader = serviceBase.querySqlParam(command);

            List<String[]> users = new List<String[]>();
            while (reader.Read())
            {
                string[] s_user = new string[8];
                s_user[0] = reader["time_creat"].ToString().Trim();
                s_user[1] = reader["wallet_name"].ToString().Trim();
                string s_type = "";
                if (reader["type"].ToString().Trim().Equals("plus"))
                {
                    s_user[2] = "<a style=\"color:#4CB052\" >Cộng tiền</a>";
                    s_type = "+";
                    s_user[3] = "<a style=\"color:#4CB052\" >" + s_type + Double.Parse(reader["value"].ToString().Trim()).ToString("N", CultureInfo.InvariantCulture) + "</a>";
                }
                else
                {
                    s_user[2] = "<a style=\"color:red\" >Trừ tiền</a>";
                    s_type = "-";
                    s_user[3] = "<a style=\"color:red\" >" + s_type + Double.Parse(reader["value"].ToString().Trim()).ToString("N", CultureInfo.InvariantCulture) + "</a>";
                }
                
                s_user[4] = Double.Parse(reader["balance"].ToString().Trim()).ToString("N", CultureInfo.InvariantCulture);
                s_user[5] = reader["user_name"].ToString().Trim();
                    s_user[6] = reader["tag"].ToString().Trim(); 

                //}


                s_user[7] = reader["message"].ToString().Trim();
                users.Add(s_user);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            int row = 0;
            reader = serviceBase.query(queryCount);
            while (reader.Read())
            {
                row = Int32Ext.toInt(reader["count"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength,
                queryString = queryString
            });
        }


        private bool _checkAgencyMerchant(GamePaymentContext db,int userid,int agencyID)
        {
            var agency = db.Agency.Where(u => u.id == agencyID).FirstOrDefault();
            int agency_id = 0;
            if (agency.subid != 0)
            {
                agency_id = agency.subid;
            }
            else
            {
                agency_id = agency.id;
            }
            var user = db.User.Where(u => u.id == userid).FirstOrDefault();
            var agency_merchant = db.Agency_Merchant.Where(u => (u.merchant_id == user.merchant_id && u.agency_id == agency_id)).FirstOrDefault();
            if (agency_merchant == null)
            {
                return false;
            }
            return true;
        }
        [HttpPost]
        public ActionResult sendSMS(int wallet_id, int cash, string s_user, string content)
        {
            if (wallet_id == 0 || cash == 0 || s_user == null || content == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var agencyID = (new AuthenticationHelper(Session).ID());
            string[] separators = { "," };
            string[] words = s_user.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            bool checkUserAGency = true;
                // Calculate money

            double cashIn = 0;

            double.TryParse(cash.ToString(), out cashIn);
            // Gate
            if (cashIn <= 0)
            {
                return Json(new
                {
                    error = true,
                    msg = "Số tiền không được <= 0."
                });
            }


            var wallet_idInt = wallet_id;
            using (var db = new GamePaymentContext())
            {
                foreach (var word in words)
                {
                    int id_user_param = Int32Ext.toInt(word);

                    if (!this._checkAgencyMerchant(db, id_user_param, agencyID))
                    {
                        checkUserAGency = false;
                    }
                }
                if (!checkUserAGency)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "User không thuộc game được cấp phép."
                    });
                }
                var wallet = db.Wallet.Where(w => w.id == wallet_idInt).FirstOrDefault();
                if (wallet == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Ví bạn chọn không tồn tại"
                    });
                }

                var wi = db.PaymentGate.FirstOrDefault(e => (e.agency_id == agencyID && e.wallet_in == wallet_idInt && e.wallet_out == wallet_idInt));
                if (wi == null)
                {
                    db.PaymentGate.Add(new Models.PaymentGate()
                    {
                        code = "",
                        name = "Agency chuyển coin " + wallet.name + " cho user ",
                        agency_id = agencyID,
                        wallet_in = wallet.id,
                        wallet_out = wallet.id,
                        status_active = "active",
                        sms = 0,
                        copyid = 0,
                        type = 0,
                        merchant_id = 0
                    });
                    db.SaveChanges();
                    wi = db.PaymentGate.FirstOrDefault(e => (e.agency_id == agencyID && e.wallet_in == wallet_idInt && e.wallet_out == wallet_idInt));
                }
                int gateID = wi.id;

                //var query_cashInOut = db.CashIORate.OrderBy(c => c.cash_in).Where(c => (c.payment_gate_id == gateID && c.cash_in >= cashIn)).FirstOrDefault();
                //if (query_cashInOut == null)
                //{
                //    return Json(new
                //    {
                //        error = true,
                //        msg = "Số tiền vượt quá config"
                //    });
                //}
                CashIORate query_cashInOut = new CashIORate();
                query_cashInOut.id = 0;
                query_cashInOut.cash_in = 100000000000;
                query_cashInOut.cash_out = 100000000000;
                // Kiem tra so du tai khoan và ballance cua agency xem du tien khong
                var query = db.Transactions
                        .OrderByDescending(p => p.id)
                        .Where(e => (e.user_id == agencyID && e.merchant_id == 0 && e.wallet_id == wi.wallet_in))
                        .FirstOrDefault();
                double agencyMoney = 0;
                if (query != null)
                {
                    agencyMoney = query.balance;
                }
                //Xóa những lịch sử cache otp cũ
                var record_otp = from c in db.OTP_user
                                 where c.user_id == agencyID && c.type == 2
                                 select new
                                 {
                                     c.id,
                                     c.time_creat,
                                     c.user_id,
                                     c.type,
                                     c.otp,
                                     c.status
                                 };
                record_otp.ToList().ForEach(ent =>
                {
                    var otp_user = db.OTP_user.FirstOrDefault(o => (o.id == ent.id));
                    db.OTP_user.Remove(otp_user);
                    db.SaveChanges();
                });
                //Xóa lịch sử insert transaction
                //var record_transaction = from c in db.AgencyMoneyTransferCommit
                //                         where c.agency_id == agencyID
                //                         select new
                //                         {
                //                             c.id
                //                         };
                //record_transaction.ToList().ForEach(ent =>
                //{
                //    var moneyTransferCommit = db.AgencyMoneyTransferCommit.FirstOrDefault(o => (o.id == ent.id));
                //    db.AgencyMoneyTransferCommit.Remove(moneyTransferCommit);
                //    db.SaveChanges();
                //});


                //Insert cache giao dịch
                var nowtime = DateTime.Now;
                //CryptoHelper cryptoHelper = new CryptoHelper();
                var token = CryptoHelper.RandomString(6);
                // Du tien, tao transaction cho user
                //db.AgencyMoneyTransferCommit.Add(new Models.AgencyMoneyTransferCommit()
                //{
                //    user_id = userID,
                //    agency_id = agencyID,
                //    wallet_id = wi.id,
                //    money = cashIn,
                //    approved = 0,
                //    create_time = nowtime,
                //    approve_time = nowtime,
                //    token = token,
                //    payment_gate = gateID
                //});
                //// Do send mail
                //db.SaveChanges();
                if (wi.sms != 0)
                {
                    //Nếu cần SMS
                    DateTime time = DateTime.Now;
                    db.OTP_user.Add(new Models.OTP_user()
                    {
                        user_id = agencyID,
                        type = 2,
                        otp = token,
                        time_creat = time,
                        status = 0
                    });
                    db.SaveChanges();

                    var record_agency = db.Agency.Where(e => e.id == agencyID).FirstOrDefault();
                    if (record_agency != null)
                    {
                        string phone = record_agency.phone.Trim().Substring(1, record_agency.phone.Trim().Length - 1);
                        SendSMSHelper sendSMSHelper = new SendSMSHelper();
                        string result = sendSMSHelper.send("+84" + phone, token);
                        return Json(new
                        {
                            error = false,
                            result = result,
                            sms = 1
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Agency chưa có số điện thoại.",
                            sms = 1
                        });
                    }
                }
                else
                {
                    //Nếu không cần SMS

                    double moneyAgencyMinus = agencyMoney;
                    foreach (var word in words)
                    {
                        int id_user = Int32Ext.toInt(word);
                        //Lấy số tiền ra
                        moneyAgencyMinus -= cashIn;
                        if (moneyAgencyMinus < 0)
                        {
                            return Json(new
                            {
                                error = true,
                                msg = "Bạn không đủ tiền để giao dịch."
                            });
                        }
                        // Lay ra so tien hien tai cua user & cong tien cho user
                        var query_money_user = db.Transactions
                            .OrderByDescending(p => p.id)
                            .Where(e => (e.user_id == id_user && e.transactions_type == 0 && e.wallet_id == wi.wallet_out))
                            .FirstOrDefault();
                        double money = 0;
                        if (query_money_user != null)
                        {
                            money = query_money_user.balance;
                        }

                        double cashOut = cashIn / query_cashInOut.cash_in * query_cashInOut.cash_out;
                        double moneySet = money + cashOut;


                        db.Transactions.Add(new Models.Transactions()
                        {
                            user_id = agencyID,
                            merchant_id = 0,
                            payment_gate = gateID,
                            wallet_id = wi.wallet_in,
                            balance = moneyAgencyMinus,
                            type = "minus",
                            value = cashIn,
                            transactions_type = 2,
                            user_send = Int32Ext.toInt(word),
                            tag = "Agency nap tien user",
                            message = content,
                            time_creat = DateTime.Now,
                            value_out = cashOut
                        });
                        db.SaveChanges();

                        double profit = cashIn - cashOut;

                        if (wi.wallet_in == wi.wallet_out && profit > 0)
                        {
                            moneyAgencyMinus = moneyAgencyMinus + profit;
                            db.Transactions.Add(new Models.Transactions()
                            {
                                user_id = agencyID,
                                merchant_id = 0,
                                payment_gate = gateID,
                                wallet_id = wi.wallet_in,
                                balance = moneyAgencyMinus,
                                type = "plus",
                                value = profit,
                                transactions_type = 2,
                                user_send = Int32Ext.toInt(word),
                                tag = "phe",
                                message = "Phế nhận được",
                                time_creat = DateTime.Now,
                                value_out = profit
                            });
                            db.SaveChanges();
                        }

                        // Bonus cash
                        int user_id_tran = Int32Ext.toInt(word);
                        var user_item = db.User.FirstOrDefault(u => u.id == user_id_tran);
                        db.Transactions.Add(new Models.Transactions()
                        {
                            user_id = user_item.id,
                            merchant_id = user_item.merchant_id,
                            payment_gate = gateID,
                            wallet_id = wi.wallet_out,
                            balance = moneySet,
                            type = "plus",
                            value = cashOut,
                            transactions_type = 0,
                            user_send = agencyID,
                            tag = "Agency+",
                            message = content,
                            time_creat = DateTime.Now
                        });
                        db.SaveChanges();
                    }

                    //---connect photon
                    var demoLB = new UserPhoton(ExitGames.Client.Photon.ConnectionProtocol.Tcp);

                    var userDB = db.User.Where(e => e.id == 155).FirstOrDefault();
                    demoLB.user = userDB;
                    demoLB.uidRecieve = userDB.id;
                    demoLB.title = "[Hệ thống] Nhận thư Agency cộng tiền";
                    demoLB.msg = "Bạn nhận được "+Convert.ToInt32(cash)+" Coin";
                    demoLB.Connect();
                    
                    return Json(new
                    {
                        error = false,
                        msg = "",
                        sms = 0
                    });
                }

            }

        }
        [HttpPost]
        public ActionResult sendSMSAgency(string wallet_id, string cash, string s_user, string content)
        {
            if (wallet_id == null || cash == null || s_user == null || content == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var agencyID = (new AuthenticationHelper(Session).ID());            
            // Calculate money
            
            double cashIn = 0;

            double.TryParse(cash, out cashIn);
            // Gate
            if (cashIn <= 0)
            {

                return Json(new
                {
                    error = true,
                    msg = "Số tiền không được <= 0"
                });
            }

            var wallet_idInt = Int32Ext.toInt(wallet_id);
            using (var db = new GamePaymentContext())
            {

                var wallet = db.Wallet.Where(w => w.id == wallet_idInt).FirstOrDefault();
                if (wallet == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Ví bạn chọn không tồn tại"
                    });
                }

                var wi = db.PaymentGate.FirstOrDefault(e => (e.agency_id == agencyID && e.wallet_in == wallet_idInt && e.wallet_out == wallet_idInt));
                if (wi == null)
                {
                    db.PaymentGate.Add(new Models.PaymentGate()
                    {
                        code = "",
                        name = "Agency chuyển coin " + wallet.name + " cho agency con ",
                        agency_id = agencyID,
                        wallet_in = wallet.id,
                        wallet_out = wallet.id,
                        status_active = "active",
                        sms = 0,
                        copyid = 0,
                        type = 0,
                        merchant_id = 0
                    });
                    db.SaveChanges();
                    wi = db.PaymentGate.FirstOrDefault(e => (e.agency_id == agencyID && e.wallet_in == wallet_idInt && e.wallet_out == wallet_idInt));
                }
                int gateID = wi.id;
                wi.sms = 0;
                //var query_cashInOut = db.CashIORate.OrderBy(c => c.cash_in).Where(c => (c.payment_gate_id == gateID && c.cash_in >= cashIn)).FirstOrDefault();
                //if (query_cashInOut == null)
                //{
                //    return Json(new
                //    {
                //        error = true,
                //        msg = "Số tiền vượt quá config"
                //    });
                //}
                CashIORate query_cashInOut = new CashIORate();
                query_cashInOut.id = 0;
                query_cashInOut.cash_in = 100000000000;
                query_cashInOut.cash_out = 100000000000;
                // Kiem tra so du tai khoan và ballance cua agency xem du tien khong
                var query = db.Transactions
                        .OrderByDescending(p => p.id)
                        .Where(e => (e.user_id == agencyID && e.merchant_id == 0 && e.wallet_id == wi.wallet_in))
                        .FirstOrDefault();
                double agencyMoney = 0;
                if (query != null)
                {
                    agencyMoney = query.balance;
                }
                if (agencyMoney < cashIn)
                {
                    // hey, you are not enought money
                    throw new Exception("Ban khong du tien de giao dich");
                }
                //Xóa những lịch sử cache otp cũ
                var record_otp = from c in db.OTP_user
                                 where c.user_id == agencyID && c.type == 2
                                 select new
                                 {
                                     c.id,
                                     c.time_creat,
                                     c.user_id,
                                     c.type,
                                     c.otp,
                                     c.status
                                 };
                record_otp.ToList().ForEach(ent =>
                {
                    var otp_user = db.OTP_user.FirstOrDefault(o => (o.id == ent.id));
                    db.OTP_user.Remove(otp_user);
                    db.SaveChanges();
                });
                //Xóa lịch sử insert transaction
                //var record_transaction = from c in db.AgencyMoneyTransferCommit
                //                         where c.agency_id == agencyID
                //                         select new
                //                         {
                //                             c.id
                //                         };
                //record_transaction.ToList().ForEach(ent =>
                //{
                //    var moneyTransferCommit = db.AgencyMoneyTransferCommit.FirstOrDefault(o => (o.id == ent.id));
                //    db.AgencyMoneyTransferCommit.Remove(moneyTransferCommit);
                //    db.SaveChanges();
                //});


                //Insert cache giao dịch
                var nowtime = DateTime.Now;
                //CryptoHelper cryptoHelper = new CryptoHelper();
                var token = CryptoHelper.RandomString(6);
                // Du tien, tao transaction cho user
                //db.AgencyMoneyTransferCommit.Add(new Models.AgencyMoneyTransferCommit()
                //{
                //    user_id = userID,
                //    agency_id = agencyID,
                //    wallet_id = wi.id,
                //    money = cashIn,
                //    approved = 0,
                //    create_time = nowtime,
                //    approve_time = nowtime,
                //    token = token,
                //    payment_gate = gateID
                //});
                //// Do send mail
                //db.SaveChanges();
                if (wi.sms != 0)
                {
                    //Nếu cần SMS
                    DateTime time = DateTime.Now;
                    db.OTP_user.Add(new Models.OTP_user()
                    {
                        user_id = agencyID,
                        type = 2,
                        otp = token,
                        time_creat = time,
                        status = 0
                    });
                    db.SaveChanges();

                    var record_agency = db.Agency.Where(e => e.id == agencyID).FirstOrDefault();
                    if (record_agency != null)
                    {
                        string phone = record_agency.phone.Trim().Substring(1, record_agency.phone.Trim().Length - 1);
                        SendSMSHelper sendSMSHelper = new SendSMSHelper();
                        string result = sendSMSHelper.send("+84" + phone, token);
                        return Json(new
                        {
                            error = false,
                            result = result,
                            sms = 1
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Agency chưa có số điện thoại.",
                            sms = 1
                        });
                    }
                }
                else
                {
                    //Nếu không cần SMS
                    string[] separators = { "," };
                    string[] words = s_user.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    double moneyAgencyMinus = agencyMoney;
                    foreach (var word in words)
                    {
                        int id_user = Int32Ext.toInt(word);
                        //Lấy số tiền ra
                        moneyAgencyMinus -= cashIn;

                        // Lay ra so tien hien tai cua user & cong tien cho user
                        var query_money_user = db.Transactions
                            .OrderByDescending(p => p.id)
                            .Where(e => (e.user_id == id_user && e.transactions_type == 2 &&  e.wallet_id == wi.wallet_out))
                            .FirstOrDefault();
                        double money = 0;
                        if (query_money_user != null)
                        {
                            money = query_money_user.balance;
                        }

                        double cashOut = cashIn / query_cashInOut.cash_in * query_cashInOut.cash_out;
                        double moneySet = money + cashOut;


                        db.Transactions.Add(new Models.Transactions()
                        {
                            user_id = agencyID,
                            merchant_id = 0,
                            payment_gate = gateID,
                            wallet_id = wi.wallet_in,
                            balance = moneyAgencyMinus,
                            type = "minus",
                            value = cashIn,
                            transactions_type = 2,
                            user_send = Int32Ext.toInt(word),
                            tag = "Agency nap tien agency con",
                            message = content,
                            time_creat = DateTime.Now,
                            value_out = cashOut
                        });
                        db.SaveChanges();

                        double profit = cashIn - cashOut;

                        if (wi.wallet_in == wi.wallet_out && profit > 0)
                        {
                            moneyAgencyMinus = moneyAgencyMinus + profit;
                            db.Transactions.Add(new Models.Transactions()
                            {
                                user_id = agencyID,
                                merchant_id = 0,
                                payment_gate = gateID,
                                wallet_id = wi.wallet_in,
                                balance = moneyAgencyMinus,
                                type = "plus",
                                value = profit,
                                transactions_type = 2,
                                user_send = Int32Ext.toInt(word),
                                tag = "phe",
                                message = "Phế nhận được",
                                time_creat = DateTime.Now,
                                value_out = profit
                            });
                            db.SaveChanges();
                        }

                        // Bonus cash
                        db.Transactions.Add(new Models.Transactions()
                        {
                            user_id = Int32Ext.toInt(word),
                            merchant_id = 0,
                            payment_gate = gateID,
                            wallet_id = wi.wallet_out,
                            balance = moneySet,
                            type = "plus",
                            value = cashOut,
                            transactions_type = 2,
                            user_send = agencyID,
                            tag = "Agency+",
                            message = content,
                            time_creat = DateTime.Now
                        });
                        db.SaveChanges();
                    }

                    return Json(new
                    {
                        error = false,
                        msg = "",
                        sms = 0
                    });
                }

            }

        }

        [HttpPost]
        public ActionResult TransferMoney(string wallet_id, string cash, string userid,string code,string total,string s_user,string content)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var agencyID = (new AuthenticationHelper(Session).ID());
            string[] separators = { "," };
            string[] words = s_user.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            var userID = Int32Ext.toInt(userid);

                // Calculate money
                double cashIn = 0;

                double.TryParse(cash, out cashIn);
            if (cashIn <= 0)
            {

                return Json(new
                {
                    error = true,
                    msg = "Số tiền không được <= 0"
                });
            }
            double totalIn = 0;

            double.TryParse(total, out totalIn);
            if (totalIn <= 0)
            {

                return Json(new
                {
                    error = true,
                    msg = "Số tiền không được <= 0"
                });
            }
            // Gate
            bool checkUserAGency = true;
            var wallet_idInt = Int32Ext.toInt(wallet_id);
                using (var db = new GamePaymentContext())
                {
                foreach (var word in words)
                {
                    int id_user_param = Int32Ext.toInt(word);

                    if (!this._checkAgencyMerchant(db, id_user_param, agencyID))
                    {
                        checkUserAGency = false;
                    }
                }
                if (!checkUserAGency)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "User không thuộc game được cấp phép."
                    });
                }
                var record_otp = db.OTP_user.Where(e => (e.otp == code && e.user_id == agencyID && e.type == 2)).FirstOrDefault();
                if (record_otp == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Mã OTP không đúng"
                    });
                }
                //Kiem tra otp quá 5 phút
                var time_creat = record_otp.time_creat;
                DateTime dateTime = DateTime.Now;
                long timeMsSinceNow = new DateTimeOffset(dateTime).ToUnixTimeMilliseconds();
                long timeMsSinceOTP = new DateTimeOffset(time_creat).ToUnixTimeMilliseconds();
                if ((timeMsSinceNow - timeMsSinceOTP) / 1000 / 60 > 5)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Mã OTP đã hết hạn"
                    });
                }
                //if (!code.Trim().Equals("123456"))
                //{
                //    return Json(new
                //    {
                //        error = true,
                //        msg = "Mã OTP không đúng"
                //    });
                //}
                var wallet = db.Wallet.Where(w => w.id == wallet_idInt).FirstOrDefault();
                if (wallet == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Ví bạn chọn không tồn tại"
                    });
                }

                var wi = db.PaymentGate.FirstOrDefault(e => (e.agency_id == agencyID && e.wallet_in == wallet_idInt && e.wallet_out == wallet_idInt));
                if (wi == null)
                {
                    db.PaymentGate.Add(new Models.PaymentGate()
                    {
                        code = "",
                        name = "Agency chuyển coin " + wallet.name + " cho user ",
                        agency_id = agencyID,
                        wallet_in = wallet.id,
                        wallet_out = wallet.id,
                        status_active = "active",
                        sms = 1,
                        copyid = 0,
                        type = 0,
                        merchant_id = 0
                    });
                    db.SaveChanges();
                    wi = db.PaymentGate.FirstOrDefault(e => (e.agency_id == agencyID && e.wallet_in == wallet_idInt && e.wallet_out == wallet_idInt));
                }
                int gateID = wi.id;

                //var query_cashInOut = db.CashIORate.OrderBy(c => c.cash_in).Where(c => (c.payment_gate_id == gateID && c.cash_in >= cashIn)).FirstOrDefault();
                //if (query_cashInOut == null)
                //{
                //    return Json(new
                //    {
                //        error = true,
                //        msg = "Số tiền vượt quá config"
                //    });
                //}
                CashIORate query_cashInOut = new CashIORate();
                query_cashInOut.id = 0;
                query_cashInOut.cash_in = 100000000000;
                query_cashInOut.cash_out = 100000000000;

                // Kiem tra so du tai khoan và ballance cua agency xem du tien khong
                var query = db.Transactions
                        .OrderByDescending(p => p.id)
                        .Where(e => (e.user_id == agencyID && e.merchant_id == 0 && e.transactions_type == 2 && e.wallet_id == wi.wallet_in))
                        .FirstOrDefault();
                double agencyMoney = 0;
                if (query != null)
                {
                    agencyMoney = query.balance;
                }
                if (agencyMoney < totalIn) 
                {
                    // hey, you are not enought money
                    throw new Exception("Ban khong du tien de giao dich");
                }
                double moneyAgencyMinus = agencyMoney;
                
                foreach (var word in words)
                {
                    //Lấy số tiền ra
                    moneyAgencyMinus = moneyAgencyMinus - cashIn;
                    int id_word = Int32Ext.toInt(word);

                    // Lay ra so tien hien tai cua user & cong tien cho user
                    var query_money_user = db.Transactions
                        .OrderByDescending(p => p.id)
                        .Where(e => (e.user_id == id_word && e.transactions_type == 0 && wi.wallet_out == e.wallet_id))
                        .FirstOrDefault();
                    double money = 0;
                    if (query_money_user != null)
                    {
                        money = query_money_user.balance;
                    }

                    double cashOut = cashIn / query_cashInOut.cash_in * query_cashInOut.cash_out;
                    double moneySet = money + cashOut;

                    db.Transactions.Add(new Models.Transactions()
                    {
                        user_id = agencyID,
                        merchant_id = 0,
                        payment_gate = gateID,
                        wallet_id = wi.wallet_in,
                        balance = moneyAgencyMinus,
                        type = "minus",
                        value = cashIn,
                        transactions_type = 2,
                        user_send = Int32Ext.toInt(word),
                        tag = "Agency nap tien user",
                        message = content,
                        time_creat = DateTime.Now,
                        value_out = cashOut
                    });
                    db.SaveChanges();

                    double profit = cashIn - cashOut;

                    if (wi.wallet_in == wi.wallet_out && profit > 0)
                    {
                        moneyAgencyMinus = moneyAgencyMinus + profit;
                        db.Transactions.Add(new Models.Transactions()
                        {
                            user_id = agencyID,
                            merchant_id = 0,
                            payment_gate = gateID,
                            wallet_id = wi.wallet_in,
                            balance = moneyAgencyMinus,
                            type = "plus",
                            value = profit,
                            transactions_type = 2,
                            user_send = Int32Ext.toInt(word),
                            tag = "phe",
                            message = "Phế nhận được",
                            time_creat = DateTime.Now,
                            value_out = profit
                        });
                        db.SaveChanges();
                    }


                    // Bonus cash
                    db.Transactions.Add(new Models.Transactions()
                    {
                        user_id = Int32Ext.toInt(word),
                        merchant_id = wi.merchant_id,
                        payment_gate = gateID,
                        wallet_id = wi.wallet_out,
                        balance = moneySet,
                        type = "plus",
                        value = cashOut,
                        transactions_type = 0,
                        user_send = agencyID,
                        tag = "Agency+",
                        message = content,
                        time_creat = DateTime.Now
                    });
                    db.SaveChanges();
                }
               
                return Json(new
                {
                    error = false,
                    msg = ""
                });
            }
        }

        [HttpPost]
        public ActionResult GetRateIO(string wallet_id)
        {
            if (wallet_id == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số wallet_id null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            try
            {
                var agencyID = (new AuthenticationHelper(Session).ID());
                using (var db = new GamePaymentContext())
                {

                    // Kiem tra so du tai khoan và ballance cua agency xem du tien khong
                    int int_wallet_id = Int32Ext.toInt(wallet_id);

                    var wallet = db.Wallet.FirstOrDefault(e => e.id == int_wallet_id);
                    if (wallet == null)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Ví không tồn tại"
                        });
                    }



                    var query_transactions = db.Transactions
                            .OrderByDescending(p => p.id)
                            .Where(e => (e.user_id == agencyID && e.transactions_type == 2 && e.wallet_id == int_wallet_id))
                            .FirstOrDefault();
                    double agencyMoney = 0;
                    if (query_transactions != null)
                    {
                        agencyMoney = query_transactions.balance;
                    }

                    List<CashIORate> cashIOrate = new List<CashIORate>();
                    CashIORate c = new CashIORate();
                    c.cash_in = 100000000000000;
                    c.cash_out =100000000000000;
                    c.id = 0;
                    cashIOrate.Add(c);

                    return Json(new
                    {
                        error = false,
                        items = cashIOrate,
                        money = agencyMoney,
                        wallet_name = wallet.name + ": "
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }

        [HttpPost]
        public ActionResult GetPaymentGates(string merchantid)
        {
            if (merchantid == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số merchantid null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            try
            {
                var agencyID = (new AuthenticationHelper(Session).ID());
                var merchantID = Int32Ext.toInt(merchantid);
                using (var db = new GamePaymentContext())
                {
                    var query = from w in db.Wallet
                                join mw in db.Merchant_Wallet on w.id equals mw.wallet_id
                                where mw.merchant_id == merchantID
                                select new
                                {
                                    w.id,
                                    w.name
                                };
                    return Json(new
                    {
                        error = false,
                        items = query.ToList()
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }

        [HttpPost]
        public ActionResult SearchUsersSelect2(String search)
        {
            if (search == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số search null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            using (var db = new GamePaymentContext())
            {
                ServiceBase serviceBase = new ServiceBase();
                var agencyID = (new AuthenticationHelper(Session).ID());
                var agency = db.Agency.Where(u => u.id == agencyID).FirstOrDefault();
                int agency_id = 0;
                if (agency.subid != 0)
                {
                    agency_id = agency.subid;
                }
                else
                {
                    agency_id = agency.id;
                }
                string queryString = "SELECT TOP 5" +
        " Users.id" +
        " ,Users.phone" +
        " ,Users.username" +
        " ,Users.email" +
        " ,Users.status_active" +
        " ,Users.verify" +
        " FROM Users WHERE Users.id != 0 AND status_active = 'active' AND verify = 1 AND username != '' AND merchant_id IN(SELECT merchant_id FROM Agency_Merchant WHERE agency_id =@agency_id ) AND (Users.username LIKE Concat('%',@search,'%') OR Users.email LIKE Concat('%',@search,'%') OR Users.phone LIKE Concat('%',@search,'%')) ORDER BY Users.id DESC ";
                SqlCommand command;
                command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@search", search);
                command.Parameters.AddWithValue("@agency_id", agency_id);
                SqlDataReader reader = serviceBase.querySqlParam(command);
                List<ResponseSelect2> a_response = new List<ResponseSelect2>();
                while (reader.Read())
                {
                    ResponseSelect2 responseSelect2 = new ResponseSelect2();
                    responseSelect2.id = Int32Ext.toInt(reader["id"].ToString().Trim());
                    responseSelect2.username = reader["username"].ToString().Trim();
                    a_response.Add(responseSelect2);
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();
                return Json(new
                {
                    results = a_response
                });
            }
        }

        [HttpPost]
        public ActionResult SearchUsers(String s_seach, int iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength, int merchant_id)
        {
            if (s_seach == null || iSortCol_0 == null || sSortDir_0 == null || iDisplayStart == null || iDisplayLength == null || merchant_id == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            string[] a_sort = new string[] { "Users.id", "Users.phone", "Users.username", "Users.email", "Users.email", "Users.status_active", "Users.verify"};
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT" +
                " Users.id" +
                " ,Users.phone" +
                " ,Users.username" +
                " ,Users.email" +
                " ,Users.status_active" +
                " ,Users.verify" +
                " FROM Users WHERE Users.id IS NOT NULL AND username != '' AND merchant_id = @merchant_id ";

            string queryCount = "SELECT COUNT(id) as 'count' FROM Users WHERE Users.id IS NOT NULL AND username != '' AND merchant_id = @merchant_id ";


            queryString += " AND Users.username =@s_seach ";
            queryCount += " AND Users.username =@s_seach ";
            if (sSortDir_0.Equals("desc"))
            {
                sSortDir_0 = "DESC";
            }
            else
            {
                sSortDir_0 = "ASC";
            }
            if (iSortCol_0 > 6)
            {
                iSortCol_0 = 0;
            }
            queryString += " ORDER BY " + a_sort[iSortCol_0] + " " + sSortDir_0 + " OFFSET @iDisplayStart ROWS FETCH NEXT @iDisplayLength ROWS ONLY";
            // SqlDataReader reader = serviceBase.query(queryString);
            SqlCommand command;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@s_seach", s_seach);
            command.Parameters.AddWithValue("@merchant_id", merchant_id);
            command.Parameters.AddWithValue("@iDisplayStart", Int32Ext.toInt(iDisplayStart));
            command.Parameters.AddWithValue("@iDisplayLength", Int32Ext.toInt(iDisplayLength));
            SqlDataReader reader = serviceBase.querySqlParam(command);

            List<String[]> users = new List<String[]>();
            while (reader.Read())
            {
                string verify = "";
                if (!reader["verify"].ToString().Trim().Equals("0") && reader["status_active"].ToString().Trim().Equals("active"))
                {
                    verify = "<span class=\"label label-primary\">Đã active</span>";
                }
                string[] s_user = new string[3];
                s_user[0] = reader["username"].ToString().Trim();
                s_user[1] = verify;
                s_user[2] = "<button type=\"button\" class=\"btn btn-success\" onclick=\"transaction.showCreateTransaction(" + reader["id"].ToString().Trim() + ",'" + reader["username"].ToString().Trim() + "')\">" +
                                           "<i class=\"fa fa-money\"></i>"+
                                        "&nbsp; Nạp tiền"+
                                    "</button>";
                users.Add(s_user);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            int row = 0;
            command = new SqlCommand(queryCount, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@s_seach", s_seach);
            command.Parameters.AddWithValue("@merchant_id", merchant_id);
            reader = serviceBase.querySqlParam(command);
            while (reader.Read())
            {
                row = Int32Ext.toInt(reader["count"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength
            });
        }
    }
}