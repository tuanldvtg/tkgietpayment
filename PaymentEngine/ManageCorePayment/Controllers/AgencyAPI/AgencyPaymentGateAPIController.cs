﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Exceptions;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers.AgencyAPI
{
    public class AgencyPaymentGateAPIController : Controller
    {


        [HttpPost]
        public ActionResult WalletsIn()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            try
            {
                var agencyID = (new AuthenticationHelper(Session).ID());
                using (var db = new GamePaymentContext())
                {
                    return Json(new
                    {
                        error = false,
                        items = (new AgencyPrivateServices()).getWallets(agencyID)
                });
                }
            }
            catch(Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.ToString()
                });
            }
        }

        /**
         * DO active or deactive port 
         */
        [HttpPost]
        public ActionResult Active(string id, bool active)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var agencyID = (new AuthenticationHelper(Session).ID());
            var gateid = Int32Ext.toInt(id);
            try
            {
                using (var db = new GamePaymentContext())
                {
                    var result = db.PaymentGate.FirstOrDefault(g =>(g.id == gateid && g.agency_id == agencyID));
                    if (result != null)
                    {
                        result.status_active = active ? "active" : "deactive";
                        db.SaveChanges();
                    }
                    return Json(new
                    {
                        error = false,
                        msg = ""
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }

        [HttpPost]
        public ActionResult WalletByMerchant(string merchant_id)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var merchantid = Int32Ext.toInt(merchant_id);
            var agencyID = (new AuthenticationHelper(Session).ID());

            try
            {
                var list = new[] { new {
                    id = "",
                    name = ""
                }}.ToList();
                list.Clear();

                using (var db = new GamePaymentContext())
                {
                    var query = from w in db.Wallet
                                join mw in db.Merchant_Wallet on w.id equals mw.wallet_id
                                where mw.merchant_id == merchantid
                                select new
                                {
                                    w.id,
                                    w.name
                                };

                    query.ToList().ForEach(ent =>
                    {
                        list.Add(new
                        {
                            id = ent.id.ToString().Trim(),
                            name = ent.name.ToString().Trim()
                        });
                    });

                    return Json(new
                    {
                        error = false,
                        items = query.ToList()
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }

        [HttpPost]
        public ActionResult Merchants()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            try
            {
                var agencyID = (new AuthenticationHelper(Session).ID());

                var list = new[] { new {
                    id = "",
                    title = ""
                }}.ToList();
                list.Clear();

                using (var db = new GamePaymentContext())
                {

                    var agencyLogin = db.Agency.FirstOrDefault(a => (a.id == agencyID));
                    if (agencyLogin.subid != 0)
                    {
                        agencyID = agencyLogin.subid;
                    }
                    
                    var query = from e in db.Merchant
                                join a2 in db.Agency_Merchant
                                on e.id equals a2.merchant_id
                                where a2.agency_id == agencyID
                                select new
                                {
                                    e.id,
                                    e.title
                                };

                    query.ToList().ForEach(ent =>
                    {
                        list.Add(new
                        {
                            id = ent.id.ToString(),
                            title = ent.title.ToString()
                        });
                    });

                    return Json(new
                    {
                        error = false,
                        items = list
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }

        [HttpPost]
        public ActionResult Update(string id, string name, string merchantid, string wallet, string wallet_in,string sms, List<CashIORate> iorate)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            try
            {
                var agencyID = (new AuthenticationHelper(Session).ID());
                var gateid = Int32Ext.toInt(id);
                var walletIn = Int32Ext.toInt(wallet_in);
                var walletTo = Int32Ext.toInt(wallet);
                var merchant = Int32Ext.toInt(merchantid);
                var smsPayment = Int32Ext.toInt(sms);
                using (var db = new GamePaymentContext())
                {
                    // Check permission for wallletIn
                    var wi = db.Wallet.FirstOrDefault(e => e.id == walletIn);

                    var inst = db.PaymentGate.FirstOrDefault(g => (g.id == gateid && g.agency_id == agencyID));
                    if (inst != null) { 
                        inst.name = name;
                        inst.merchant_id = merchant;
                        inst.wallet_out = walletTo;
                        inst.wallet_in = walletIn;
                        inst.sms = smsPayment;
                        db.SaveChanges();
                        // Check and update io rate
                        if(iorate!= null && iorate.Count > 0) { 
                            // Filter cashIO Rate key pair is not null element
                            iorate.Where(e =>
                            {
                                return (e.cash_in != 0 && e.cash_out != 0);
                            });
                            //Xóa rate io không tồn tại
                            var query = from c in db.CashIORate
                                        where c.payment_gate_id == gateid
                                        select new
                                        {
                                            c.id,
                                            c.payment_gate_id,
                                            c.cash_out,
                                            c.cash_in,
                                        };

                            query.ToList().ForEach(ent =>
                            {

                                if (!iorate.Exists(cash => (cash.id == ent.id)))
                                {
                                    var cashio = db.CashIORate.FirstOrDefault(cash => (cash.id == ent.id));
                                    db.CashIORate.Remove(cashio);
                                    db.SaveChanges();
                                }
                            });

                            // Set gateID for cashIO rate 
                            iorate.ForEach(ele =>
                            {
                                if(ele.id == 0)
                                {
                                    ele.payment_gate_id = gateid;
                                    db.CashIORate.Add(ele);
                                }else
                                {
                                    // Check for update
                                    var iorRec = db.CashIORate.FirstOrDefault(g => g.id == ele.id);
                                    if(iorRec != null)
                                    {
                                        iorRec.cash_in = ele.cash_in;
                                        iorRec.cash_out = ele.cash_out;
                                    }
                                }
                            });
                        }
                        db.SaveChanges();

                       
                    }
                }

                return Json(new
                {
                    error = false,
                    msg = ""
                });
            
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }

        [HttpPost]
        public ActionResult Create(string name, string merchantid, string wallet,string sms, List<CashIORate> iorate,string wallet_in)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            try
            {
                var agencyID = (new AuthenticationHelper(Session).ID());

                using (var context = new GamePaymentContext())
                {
                    var inst = new PaymentGate
                    {
                        name = name,
                        code = "",
                        wallet_in = Int32Ext.toInt(wallet_in),
                        wallet_out = Int32Ext.toInt(wallet),
                        merchant_id = Int32Ext.toInt(merchantid),
                        sms = Int32Ext.toInt(sms),
                    status_active = "active",
                        agency_id = agencyID
                    };
                    context.PaymentGate.Add(inst);
                    context.SaveChanges();

                    // Filter cashIO Rate key pair is not null element
                    iorate.Where(e =>
                    {
                        return (e.cash_in != 0 && e.cash_out != 0);
                    });

                    // Set gateID for cashIO rate 
                    iorate.ForEach(ele =>
                    {
                        ele.payment_gate_id = inst.id;
                    });
                    context.CashIORate.AddRange(iorate);
                    context.SaveChanges();
                }

                return Json(new
                {
                    error = false,
                    msg = ""
                });
            }
            catch(Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }



        [HttpPost]
        public ActionResult Detail(string id)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var queryid = Int32Ext.toInt(id);
            var agencyID = (new AuthenticationHelper(Session).ID());
            try
            {
                
                using (var db = new GamePaymentContext())
                {
                     var ent = (from g in db.PaymentGate
                     where (g.id == queryid && g.agency_id == agencyID)
                     select new
                     {
                         g.id,
                         g.name,
                         g.status_active,
                         merchant = g.merchant_id,
                         g.wallet_in,
                         wallet = g.wallet_out,
                         g.sms
                     }).First();
                    var wallets = (new WalletService()).find(ent.merchant);

                    var cashIORates = from cio in db.CashIORate
                                      where cio.payment_gate_id == queryid
                                      select new
                                      {
                                          cio.id,
                                          cio.cash_in,
                                          cio.cash_out
                                      };
                    
                    return Json(new
                    {
                        error = false,
                        item = ent,
                        wallets = wallets,
                        rateIO = cashIORates.ToList()
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }

        /**
         * List all payment gate by agency
         * */
        [HttpPost]
        public ActionResult ListAll(string kw)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            try
            {
                var agencyID = (new AuthenticationHelper(Session).ID());
                
                var list = new[] { new {
                    id = "",
                    status = "",
                    title = "",
                    merchant = new {
                        id = "",
                        title = ""
                    },
                    sms = ""
                }}.ToList();
                list.Clear();

                using (var db = new GamePaymentContext())
                {

                    (from g in db.PaymentGate
                     join m in db.Merchant
                     on g.merchant_id equals m.id
                     where g.agency_id == agencyID
                     select new
                     {
                         g.id,
                         g.name,
                         g.status_active,
                         merchant_id = m.id,
                         merchant_title = m.title,
                         g.sms
                     }).ToList().ForEach(ent =>
                     {
                         var sms_type = "";
                         if (ent.sms != 0)
                         {
                             sms_type = "<span class=\"label label-primary\">Sử dụng</span>";
                         }
                         list.Add(new
                         {
                             id = ent.id.ToString(),
                             status = ent.status_active,
                             title = ent.name,
                             merchant = new { id = ent.merchant_id.ToString(), title = ent.merchant_title.ToString() },
                             sms = sms_type
                         });
                     });

                    return Json(new
                    {
                        error = false,
                        items = list
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }
    }
}
