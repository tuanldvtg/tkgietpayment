﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers
{
    public class ManageagencyController : Controller
    {
        // GET: Coin
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Listdata(int iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength)
        {
            if (iSortCol_0 == null || sSortDir_0 == null || iDisplayStart ==  null || iDisplayLength == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var agencyID = (new AuthenticationHelper(Session).ID());
            string[] a_sort = new string[] { "id", "title", "username", "phone", "email","id","id" };
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT" +
                " id" +
                " ,title" +
                " ,username" +
                " ,phone" +
                " ,email" +
                " FROM Agency WHERE subid = "+agencyID;

            string queryCount = "SELECT COUNT(id) as 'count' FROM Agency WHERE subid != 0 ";

            if (sSortDir_0.Equals("desc"))
            {
                sSortDir_0 = "DESC";
            }
            else
            {
                sSortDir_0 = "ASC";
            }
            if (iSortCol_0 > 6)
            {
                iSortCol_0 = 0;
            }
            queryString += " ORDER BY " + a_sort[iSortCol_0] + " " + sSortDir_0 + " OFFSET @iDisplayStart ROWS FETCH NEXT @iDisplayLength ROWS ONLY";
            // SqlDataReader reader = serviceBase.query(queryString);
            SqlCommand command;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@iDisplayStart", Int32Ext.toInt(iDisplayStart));
            command.Parameters.AddWithValue("@iDisplayLength", Int32Ext.toInt(iDisplayLength));
            SqlDataReader reader = serviceBase.querySqlParam(command);

            List<String[]> users = new List<String[]>();
            while (reader.Read())
            {
                string[] s_user = new string[7];
                s_user[0] = reader["id"].ToString();
                s_user[1] = reader["title"].ToString();
                s_user[2] = reader["username"].ToString();

                string phone_new = reader["phone"].ToString();
                var chars = phone_new.ToCharArray();
                string email = reader["email"].ToString().Trim();

                string phone_response = "";
                for (int ctr = 0; ctr < chars.Length; ctr++)
                {
                    if (ctr < chars.Length - 4)
                    {
                        phone_response += "*";
                    }
                    else
                    {
                        phone_response += chars[ctr];
                    }
                }
                s_user[3] = phone_response;

                string email_response = "";
                for (int ctr = 0; ctr < email.Length; ctr++)
                {
                    if (ctr > 3)
                    {
                        email_response += "*";
                    }
                    else
                    {
                        email_response += email[ctr];
                    }
                }
                s_user[4] = email_response;
                s_user[5] = "<button onclick=\"transaction.showCreateTransaction("+ reader["id"].ToString() + ", '"+ reader["title"].ToString() + "')\" class=\"btn btn-block btn-success\" type=\"button\"><i class=\"fa fa-money\"></i>&nbsp; Nạp tiền</button>";
                s_user[6] = "<button onclick=\"manage_agency.showEdit('" + reader["id"].ToString() + "')\" type=\"button\" class=\"btn btn-default btn-xs\">Sửa</button><button onclick=\"manage_accounting.showDelete('" + reader["id"].ToString() + "')\"  type=\"button\" class=\"btn btn-danger btn-xs\">Xóa</button>";
                users.Add(s_user);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            int row = 0;
            reader = serviceBase.query(queryCount);
            while (reader.Read())
            {
                row = Int32Ext.toInt(reader["count"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength
            });
        }
        public string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
        [HttpPost]
        public ActionResult add(string title,string username,string password,string email,string phone)
        {
            if (title == null || username == null || password == null || email == null || phone == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            if (title.Equals(""))
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng nhập tên"
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            if (!serviceBase.isCheckRegexQuery(username))
            {
                return Json(new
                {
                    error = true,
                    msg = "username không đúng định dạng"
                });
            }
            if (serviceBase.validatePassword(password))
            {
                return Json(new
                {
                    error = true,
                    msg = "password phải >= 6 ký tự"
                }); 
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var agencyID = (new AuthenticationHelper(Session).ID());
            using (var db = new GamePaymentContext())
            {
                db.Agency.Add(new Models.Agency()
                {
                    title = title,
                    subid = agencyID,
                    username = username,
                    password = password,
                    email = email,
                    phone = phone,
                    status_active = "active",
                    is_appear = 1
                });
                db.SaveChanges();
                var ele_agency = db.Agency.FirstOrDefault(o => (o.username == username && o.password == password));
                ele_agency.password = CryptoHelper.MD5Hash(password,ele_agency.id.ToString());
                ele_agency.id = ele_agency.id;
                db.SaveChanges();
            }
            return Json(new
            {
                error = false
            });
        }

        public Boolean check_delete(int id)
        {
            using (var db = new GamePaymentContext())

            {
                var wallet = db.Wallet.FirstOrDefault(e => e.id == id);
                if (wallet == null)
                {
                    return true;
                }
                return false;
            }
  
        }
        

        [HttpPost]
        public ActionResult edit(string title,int id,string username,string phone,string email,string password)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var auth = new AuthenticationHelper(Session);
            if (!auth.isLogined())
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            using (var db = new GamePaymentContext())
            {
                ServiceBase serviceBase = new ServiceBase();
                var agency = db.Agency.FirstOrDefault(e => e.id == id);
                agency.title = title;
                agency.username = username;
                agency.phone = phone;
                agency.email = email;
               
                if (!password.Trim().Equals(""))
                {
                    agency.password = CryptoHelper.MD5Hash(password, id.ToString());

                }
                db.SaveChanges();
                return Json(new
                {
                    error = false
                });
            }

        }
        [HttpPost]
        public ActionResult delete(int id)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            using (var db = new GamePaymentContext())

            {
                var agency = db.Agency.FirstOrDefault(e => e.id == id);
                db.Agency.Remove(agency);
                return Json(new
                {
                    error = false
                });
            }
        }

        [HttpPost]
        public ActionResult detail(int id)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT id,title,username,phone,email FROM Agency WHERE id =@id";

            SqlCommand command;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@id", id);
            SqlDataReader reader = serviceBase.querySqlParam(command);
            
            Agency agency = new Agency();
            while (reader.Read())
            {
                agency.id = Int32Ext.toInt(reader["id"].ToString());
                agency.title = reader["title"].ToString().Trim();
                agency.username = reader["username"].ToString().Trim();
                agency.phone = reader["phone"].ToString().Trim();

                string phone_new = agency.phone;
                var chars = phone_new.ToCharArray();
                string email = reader["email"].ToString().Trim();

                string phone_response = "";
                for (int ctr = 0; ctr < chars.Length; ctr++)
                {
                    if (ctr < chars.Length - 4)
                    {
                        phone_response += "*";
                    }
                    else
                    {
                        phone_response += chars[ctr];
                    }
                }
                agency.phone_new = phone_response;
                string email_response = "";
                for (int ctr = 0; ctr < email.Length; ctr++)
                {
                    if (ctr > 3)
                    {
                        email_response += "*";
                    }
                    else
                    {
                        email_response += email[ctr];
                    }
                }
                agency.email_new = email_response;


                agency.email = reader["email"].ToString().Trim();
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            string queryStringRef = "SELECT id,accounting_id,paymentgate_id FROM Accounting_ref_paymentgate WHERE accounting_id = @id";
            command = new SqlCommand(queryStringRef, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@id", id);
            reader = serviceBase.querySqlParam(command);
            List<Accounting_ref_paymentgate> list_id = new List<Accounting_ref_paymentgate>();
            while (reader.Read())
            {
                Accounting_ref_paymentgate accounting_ref_paymentgate = new Accounting_ref_paymentgate();
                accounting_ref_paymentgate.id = Int32Ext.toInt(reader["id"].ToString().Trim());
                accounting_ref_paymentgate.accounting_id = Int32Ext.toInt(reader["accounting_id"].ToString().Trim());
                accounting_ref_paymentgate.paymentgate_id = Int32Ext.toInt(reader["paymentgate_id"].ToString().Trim());
                list_id.Add(accounting_ref_paymentgate);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false,
                item = agency,
                items = list_id
            });

        }
    }
}