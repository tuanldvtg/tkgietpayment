﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers.AgencyAPI
{
    public class RwithdrawagencyAPIController : Controller
    {

        public ActionResult loadPaymentGateId(int paymentGateid,int wallet_id)
        {
            if (paymentGateid == null || wallet_id == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            try
            {
                var agencyID = (new AuthenticationHelper(Session).ID());
                var gateID = paymentGateid;
                var walletID = wallet_id;
                using (var db = new GamePaymentContext())
                {
                    var query = from g in db.CashIORate
                                where (g.payment_gate_id == gateID)
                                select new
                                {
                                    g.id,
                                    g.cash_in,
                                    g.cash_out,
                                };

                    // Kiem tra so du tai khoan và ballance cua agency xem du tien khong

                    var wi = db.PaymentGate.FirstOrDefault(e => e.id == gateID);
                    if (wi == null)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Cổng thanh toán không tồn tại"
                        });
                    }

                    var query_transactions = db.Transactions
                            .OrderByDescending(p => p.id)
                            .Where(e => (e.user_id == agencyID && e.transactions_type == 2 && e.wallet_id == walletID))
                            .FirstOrDefault();
                    double agencyMoney = 0;
                    if (query_transactions != null)
                    {
                        agencyMoney = query_transactions.balance;
                    }

                    var wallet = db.Wallet.FirstOrDefault(e => e.id == walletID);
                    if (wallet == null)
                    {
                        return Json(new
                        {
                            error = true,
                            msg = "Ví không tồn tại"
                        });
                    }

                    return Json(new
                    {
                        error = false,
                        items = query.ToList(),
                        money = agencyMoney,
                        wallet_name = wallet.name + ": "
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }

        public ActionResult loadBalaceWalletid(int wallet_id)
        {
            if (wallet_id == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var agencyID = (new AuthenticationHelper(Session).ID());

            var walletID = wallet_id;

            using (var db = new GamePaymentContext())
            {
                // Kiem tra so du tai khoan và ballance cua agency xem du tien khong
                var query = db.Transactions
                        .OrderByDescending(p => p.id)
                        .Where(e => (e.user_id == agencyID && e.transactions_type == 2 && e.wallet_id == walletID))
                        .FirstOrDefault();
                double agencyMoney = 0;
                if (query != null)
                {
                    agencyMoney = query.balance;
                }
                return Json(new
                {
                    error = false,
                    balance = agencyMoney
                });
            }

        }

        public ActionResult addRwithdrawagency(int wallet_id, int money, string note,int paymengate_id)
        {
            if (wallet_id == null || money == null || note == null || paymengate_id == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var agencyID = (new AuthenticationHelper(Session).ID());
            var walletID = wallet_id;
            var money_value = money;
            if (money_value <= 0)
            {
                return Json(new
                {
                    error = true,
                    msg = "Số tiền không được phép <= 0"
                });
            }
            var paymengateID = paymengate_id;
            using (var db = new GamePaymentContext())
            {
                //Kiểm tra tiền trong ví
                var query = db.Transactions
        .OrderByDescending(p => p.id)
        .Where(e => (e.user_id == agencyID && e.transactions_type == 2 && e.wallet_id == walletID))
        .FirstOrDefault();
                double agencyMoney = 0;
                if (query != null)
                {
                    agencyMoney = query.balance;
                }
                if (agencyMoney < money_value)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Số tiền trong ví không đủ."
                    });
                }
                //Tính số tiền ra
                var query_cashInOut = db.CashIORate.OrderBy(c => c.cash_in).Where(c => (c.payment_gate_id == paymengateID && c.cash_in >= money_value)).FirstOrDefault();
                if (query_cashInOut == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Số tiền vượt quá config"
                    });
                }
                double cashOut = money_value / query_cashInOut.cash_in * query_cashInOut.cash_out;


                DateTime time = DateTime.Now;
                db.Rwithdrawal_Agency.Add(new Models.Rwithdrawal_Agency()
                {
                    agency_id = agencyID,
                    create_time = time,
                    wallet_id = walletID,
                    paymentgate_id = paymengateID,
                    approved = 0,
                    accounting_id = 0,
                    value = money_value,
                    note = note,
                    value_out = cashOut
                });
                db.SaveChanges();
                return Json(new
                {
                    error = false
                });
            }
        }


        public ActionResult listdata(string filter,string search_note, int iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength)
        {
            if (filter == null || search_note == null || iSortCol_0 == null || sSortDir_0 == null || iDisplayStart ==  null || iDisplayLength == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var agencyID = (new AuthenticationHelper(Session).ID());
            string[] a_sort = new string[] { "id", "agency_id", "wallet_id", "value", "approved", "create_time", "approved_time", "note", "id" };
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT" +
                " id" +
                " ,paymentgate_id" +
                " ,approved" +
                " ,create_time" +
                " ,(SELECT TOP 1 name FROM Wallet WHERE id = Rwithdrawal_Agency.wallet_id ) as 'wallet_id'" +
                " ,(SELECT TOP 1 title FROM Agency WHERE id = Rwithdrawal_Agency.agency_id) as 'agency_id'" +
                " ,value" +
                " ,value_out" +
                " ,approved_time" +
                " ,note" +
                " FROM Rwithdrawal_Agency WHERE agency_id =@agencyID ";

            string queryCount = "SELECT COUNT(id) as 'count' FROM Rwithdrawal_Agency WHERE agency_id =@agencyID ";
            if (filter.Equals("none"))
            {
                queryString += " AND approved = 0 ";
                queryCount += " AND approved = 0 ";
            }
            if (filter.Equals("passed"))
            {
                queryString += " AND approved = 1 ";
                queryCount += " AND approved = 1 ";
            }
            if (!search_note.Equals(""))
            {
                queryString += " AND note LIKE Concat('%',@search_note,'%') ";
                queryCount += " AND note LIKE Concat('%',@search_note,'%') "; 
            }

            if (sSortDir_0.Equals("desc"))
            {
                sSortDir_0 = "DESC";
            }
            else
            {
                sSortDir_0 = "ASC";
            }
            if (iSortCol_0 > 8)
            {
                iSortCol_0 = 0;
            }
            queryString += " ORDER BY " + a_sort[iSortCol_0] + " " + sSortDir_0 + " OFFSET @iDisplayStart ROWS FETCH NEXT @iDisplayLength ROWS ONLY";
            // SqlDataReader reader = serviceBase.query(queryString);
            SqlCommand command;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@search_note", search_note);
            command.Parameters.AddWithValue("@agencyID", agencyID);
            command.Parameters.AddWithValue("@iDisplayStart", Int32Ext.toInt(iDisplayStart));
            command.Parameters.AddWithValue("@iDisplayLength", Int32Ext.toInt(iDisplayLength));
            SqlDataReader reader = serviceBase.querySqlParam(command);

            List<String[]> users = new List<String[]>();
            while (reader.Read())
            {
                string[] s_user = new string[8];
                s_user[0] = reader["id"].ToString().Trim();
                s_user[1] = reader["agency_id"].ToString().Trim();
                s_user[2] = reader["wallet_id"].ToString().Trim();
                s_user[3] = Double.Parse(reader["value"].ToString().Trim()).ToString("N0", CultureInfo.InvariantCulture);
                if (reader["approved"].ToString().Trim().Equals("0"))
                {
                    s_user[4] = "";
                }
                switch (reader["approved"].ToString().Trim())
                {
                    case "0":
                        s_user[4] = "<span class=\"label label-info\">Chờ kế toán xác nhận</span>";
                        break;
                    case "1":
                        s_user[4] = "<span class=\"label label-warning\">Đã hoàn thành</span>";
                        break;
                    case "2":
                        s_user[4] = "<span class=\"label label-danger\">Đã hủy</span>";
                        break;
                }
                s_user[5] = reader["create_time"].ToString().Trim();
                s_user[6] = reader["approved_time"].ToString().Trim();
                s_user[7] = reader["note"].ToString().Trim();

                users.Add(s_user);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            int row = 0;
            command = new SqlCommand(queryCount, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@agencyID", agencyID);
            reader = serviceBase.querySqlParam(command);
            while (reader.Read())
            {
                row = Int32.Parse(reader["count"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength
            });
        }
    }
}