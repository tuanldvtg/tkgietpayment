﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers.AgencyAPI
{
    public class RwithdrawuserAPIController : Controller
    {

        private string render_code()
        {
            using (var db = new GamePaymentContext())
            {
                //CryptoHelper cryptoHelper = new CryptoHelper();
                var code = CryptoHelper.RandomString(6);
                var querycode = db.Rwithdrawal_User.Where(c => (c.code == code && c.user_id == 0)).FirstOrDefault();
                if (querycode != null)
                {
                    return this.render_code();
                }
                return code;
            }
        }
        
        public ActionResult addrwithdrawuser(string wallet_id, string money, string note)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            var agencyID = 0;
            try
            {
                agencyID = (new AuthenticationHelper(Session).ID());
            }
            catch(Exception )
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            
            var walletID = Int32Ext.toInt(wallet_id);
            var money_value = float.Parse(money.Trim());
            if (money_value <= 0)
            {
                return Json(new
                {
                    error = true,
                    msg = "Số tiền không được phép <= 0"
                });
            }
            using (var db = new GamePaymentContext())
            {
                DateTime time = DateTime.Now;

                string code = this.render_code();
                db.Rwithdrawal_User.Add(new Models.Rwithdrawal_User()
                {
                    agency_id = agencyID,
                    user_id = 0,
                    create_time = time,
                    wallet_id = walletID,
                    paymentgate_id = 0,
                    approved = 0,
                    accounting_id = 0,
                    value = money_value,
                    note = note,
                    value_out = 0,
                    code = code,
                    tag = "",
                    status = 0
                });
                db.SaveChanges();
                return Json(new
                {
                    error = false
                });
            }
        }
        [HttpPost]
        public ActionResult listdata(string filter,string search_note, String s_seach, String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength)
        {
            var auth = new AuthenticationHelper(Session);

            var agencyID = auth.ID();
            string[] a_sort = new string[] { "id" ,"agency_id", "wallet_id", "value", "approved", "create_time", "approved_time","code","username", "note", "id" };
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT" +
                " id" +
                " ,paymentgate_id" +
                " ,approved" +
                " ,create_time" +
                " ,(SELECT TOP 1 name FROM Wallet WHERE id = Rwithdrawal_User.wallet_id ) as 'wallet_id'" +
                " ,(SELECT TOP 1 title FROM Agency WHERE id = Rwithdrawal_User.agency_id) as 'agency_id'" +
                " ,value" +
                " ,value_out" +
                " ,approved_time" +
                " ,note" +
                " ,code" +
                " ,(SELECT TOP 1 username FROM Users WHERE id = Rwithdrawal_User.user_id ) as 'username'" +
                " FROM Rwithdrawal_User WHERE agency_id = " + agencyID + " ";

            string queryCount = "SELECT COUNT(id) as 'count' FROM Rwithdrawal_User WHERE agency_id = " + agencyID + " ";
            if (!filter.Trim().Equals("all"))
            {
                queryString += " AND approved =@filter";
                queryCount += " AND approved =@filter";
            }
            if (!search_note.Equals(""))
            {
                queryString += " AND note LIKE Concat('%',@search_note,'%') ";
                queryCount += " AND note LIKE Concat('%',@search_note,'%') "; 
            }

            if (sSortDir_0.Equals("desc"))
            {
                sSortDir_0 = "DESC";
            }
            else
            {
                sSortDir_0 = "ASC";
            }
            queryString += " ORDER BY " + a_sort[Int32Ext.toInt(iSortCol_0)] + " " + sSortDir_0 + " OFFSET @iDisplayStart ROWS FETCH NEXT @iDisplayLength ROWS ONLY";
            // SqlDataReader reader = serviceBase.query(queryString);
            SqlCommand command;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@filter", filter);
            command.Parameters.AddWithValue("@search_note", search_note);
            command.Parameters.AddWithValue("@iDisplayStart", Int32Ext.toInt(iDisplayStart));
            command.Parameters.AddWithValue("@iDisplayLength", Int32Ext.toInt(iDisplayLength));
            SqlDataReader reader = serviceBase.querySqlParam(command);

            List<String[]> users = new List<String[]>();
            while (reader.Read())
            {
                string[] s_user = new string[10];
                s_user[0] = reader["id"].ToString().Trim();
                s_user[1] = reader["agency_id"].ToString().Trim();
                s_user[2] = reader["wallet_id"].ToString().Trim();
                s_user[3] = Double.Parse(reader["value"].ToString().Trim()).ToString("N0", CultureInfo.InvariantCulture);
                switch (reader["approved"].ToString().Trim())
                {
                    case "0":
                        s_user[4] = "<span class=\"label label-info\">Chờ user xác nhận</span>";
                        break; 
                    case "1":
                        s_user[4] = "<span class=\"label label-warning\">Chờ kế toán xác nhận</span>";
                        break;
                    case "2":
                        s_user[4] = "<span class=\"label label-danger\">Đã hủy</span>";
                        break;
                    case "3":
                        s_user[4] = "<span class=\"label label-success\">Đã hoàn thành</span>";
                        break;
                }
                s_user[5] = reader["create_time"].ToString().Trim();
                s_user[6] = reader["approved_time"].ToString().Trim();
                s_user[7] ="<b>"+ reader["code"].ToString().Trim() +"</b>";
                s_user[8] = reader["username"].ToString().Trim();
                s_user[9] = reader["note"].ToString().Trim();

                users.Add(s_user);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            int row = 0;
            reader = serviceBase.query(queryCount);
            while (reader.Read())
            {
                row = Int32.Parse(reader["count"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength
            });
        }
    }
}