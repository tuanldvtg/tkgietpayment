﻿using CorePayment.Base;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers
{
    public class WalletController : Controller
    {
        // GET: wallet
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Listdata(String s_seach, String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength)
        {
            string[] a_sort = new string[] { "Wallet.id", "Wallet.name", "Coin.name", "Wallet.id" };
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT" +
                " Wallet.id" +
                " ,Wallet.name as 'name'" +
                " ,Coin.name as 'coin_name'" +
                " FROM Wallet INNER JOIN Coin ON Wallet.coin_id = Coin.id ";

            string queryCount = "SELECT COUNT(id) as 'count' FROM Wallet";

            queryString += " ORDER BY " + a_sort[Int32.Parse(iSortCol_0)] + " " + sSortDir_0 + " OFFSET " + iDisplayStart + " ROWS FETCH NEXT " + iDisplayLength + " ROWS ONLY";
            SqlDataReader reader = serviceBase.query(queryString);

            List<String[]> users = new List<String[]>();
            while (reader.Read())
            {
                string[] s_user = new string[4];
                s_user[0] = reader["id"].ToString();
                s_user[1] = reader["name"].ToString();
                s_user[2] = reader["coin_name"].ToString();
                s_user[3] = "<button onclick=\"wallet.showEdit('" + reader["id"].ToString() + "')\" type=\"button\" class=\"btn btn-default btn-xs\">Sửa</button><button onclick=\"wallet.showDelete('" + reader["id"].ToString() + "')\"  type=\"button\" class=\"btn btn-danger btn-xs\">Xóa</button>";
                users.Add(s_user);
            }
            DatabaseService.Instance.getConnection().Close();

            int row = 0;
            reader = serviceBase.query(queryCount);
            while (reader.Read())
            {
                row = Int32.Parse(reader["count"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength
            });
        }
        [HttpPost]
        public ActionResult add(string title,int coin_id)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            string queryStringInsertLog = "INSERT INTO Wallet(name,coin_id) VALUES (N'" + title + "','"+ coin_id + "')";
            reader = serviceBase.query(queryStringInsertLog);
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false
            });
        }
        [HttpPost]
        public ActionResult edit(string title, int id,int coin_id)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string queryString = "UPDATE Wallet SET name=N'" + title + "',coin_id='" + coin_id + "'" +
                " WHERE id = " + id;
            reader = serviceBase.query(queryString);
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                error = false
            });
        }
        [HttpPost]
        public ActionResult delete(int id)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }
            if (!check_delete(id))
            {
                return Json(new
                {
                    error = true,
                    msg = "Ví này đã tạo trong cổng thanh toán không thể xóa."
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            string queryStringInsertLog = "DELETE FROM Wallet WHERE id = " + id;
            SqlDataReader reader = serviceBase.query(queryStringInsertLog);
            DatabaseService.Instance.getConnection().Close();

            string queryString= "DELETE FROM Merchant_wallet WHERE wallet_id = " + id;
            reader = serviceBase.query(queryString);
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                error = false
            });

        }
        public Boolean check_delete(int id)
        {
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT TOP 1 id FROM PaymentGate WHERE wallet_in = '" + id + "' OR wallet_out = '" + id + "' ";

            SqlDataReader reader = serviceBase.query(queryString);

            int count = 0;
            while (reader.Read())
            {
                count++;
            }
            DatabaseService.Instance.getConnection().Close();
            if (count > 0)
            {
                return false;
            }
            return true;
        }


        [HttpPost]
        public ActionResult detail(int id)
        {
            if (Session["UserID"] == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại."
                });
            }

            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT * FROM Wallet WHERE id = " + id;

            SqlDataReader reader = serviceBase.query(queryString);

            Wallet wallet = new Wallet();
            while (reader.Read())
            {
                wallet.id = Int32.Parse(reader["id"].ToString());
                wallet.name = reader["name"].ToString().Trim();
                wallet.coin_id =Int32.Parse(reader["coin_id"].ToString().Trim());
            }
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false,
                item = wallet
            });
        }
    }
}