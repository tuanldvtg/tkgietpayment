﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Exceptions;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers
{
    public class AgencyController : Controller
    {
        private bool checkPermission()
        {
            var isLogined = new AuthenticationHelper(Session).isLogined();
            return isLogined;
        }

        // GET: Agency
        public ActionResult Index()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
                ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();
                string subid = (new AuthenticationHelper(Session).getSub());
                ViewBag.subid = subid;

                return RedirectToAction("Dashboard", "Agency");
            }
            catch (Exception)
            {
                return Redirect("/Agency/Login");
            }

        }

        [HttpGet]
        public ActionResult Dashboard()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
                ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();
                string subid = (new AuthenticationHelper(Session).getSub());
                ViewBag.subid = subid;
                return View();
            }
            catch (Exception)
            {
                return Redirect("/Agency/Login");
            }
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(String username, String password)
        {
            if (username == null || password == null)
            {
                ViewData["Message"] = "Username hoặc  mật khẩu không đúng.";
                return View("Login");
            }
            try
            {
                var loginResult = new AgencyPrivateServices().authentication(username, password);
                new AuthenticationHelper(Session).assign(loginResult);
                return RedirectToAction("Transactions", "Agency");
            }
            catch (AccountNotfoundException)
            {
                ViewData["Message"] = "Username hoặc  mật khẩu không đúng.";
            }
            catch (AccountDeactiveException)
            {
                ViewData["Message"] = "Tài khoản đã bị khóa.";
            }
            catch (Exception)
            {
                ViewData["Message"] = "Lỗi không xác định, xin vui lòng liên hệ quản trị.";
            }
            return View("Login");

        }

        [HttpGet]
        public ActionResult Ports()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
                ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();
                string subid = (new AuthenticationHelper(Session).getSub());
                ViewBag.subid = subid;
                return View();
            }
            catch (Exception)
            {
                return Redirect("/Agency/Login");
            }
        }
        [HttpGet]
        public ActionResult Manageagency()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
                ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();
                string subid = (new AuthenticationHelper(Session).getSub());
                ViewBag.subid = subid;
                return View();
            }
            catch (Exception)
            {
                return Redirect("/Agency/Login");
            }
        }

        [HttpGet]
        public ActionResult ListUserSelect2()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
                var agencyID = (new AuthenticationHelper(Session).ID());
                return Json(new
                {
                    error = false,
                    results = new[] {
    new {id = "1" , text = "X"},
    new {id = "2" , text = "X"}
                    }
                });
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    results = new[]
                    {
    new {id = "1" , text = "X"},
    new {id = "2" , text = "X"}
                    }
                });
            }
        }

        [HttpGet]
        public ActionResult Rwithdrawaagency()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
                var agencyID = (new AuthenticationHelper(Session).ID());
                ServiceBase serviceBase = new ServiceBase();
                SqlDataReader reader;
                var list_paymentgate = new AgencyPrivateServices().getWallets(agencyID);
                ViewBag.list_paymentgate = list_paymentgate;


                reader = serviceBase.query("SELECT id,name FROM PaymentGate WHERE type = 4 AND status_active = 'active' ORDER BY id DESC");
                List<PaymentGate> list_paymentgate2 = new List<PaymentGate>();
                while (reader.Read())
                {
                    PaymentGate paymentGate = new PaymentGate();
                    paymentGate.id = Int32.Parse(reader["id"].ToString());
                    paymentGate.name = reader["name"].ToString();
                    list_paymentgate2.Add(paymentGate);
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();
                ViewBag.list_paymentgate2 = list_paymentgate2;
                ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();
                string subid = (new AuthenticationHelper(Session).getSub());
                ViewBag.subid = subid;
                return View();
            }
            catch (Exception)
            {
                return Redirect("/Agency/Login");
            }
        }
        [HttpGet]
        public ActionResult Rwithdrawauser()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
                var agencyID = (new AuthenticationHelper(Session).ID());
                ServiceBase serviceBase = new ServiceBase();
                SqlDataReader reader = serviceBase.query("SELECT id,name FROM Wallet ORDER BY id DESC");
                var list_paymentgate = new List<Wallet>();
                while (reader.Read())
                {
                    Wallet paymentGate = new Wallet();
                    paymentGate.id = Int32.Parse(reader["id"].ToString());
                    paymentGate.name = reader["name"].ToString();
                    list_paymentgate.Add(paymentGate);
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();
                ViewBag.list_paymentgate = list_paymentgate;
                ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();
                string subid = (new AuthenticationHelper(Session).getSub());
                ViewBag.subid = subid;
                return View();
            }
            catch (Exception)
            {
                return Redirect("/Agency/Login");
            }
        }


        [HttpGet]
        public ActionResult Transactions()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
                ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();
                string subid = (new AuthenticationHelper(Session).getSub());
                ViewBag.subid = subid;
                return View();
            }
            catch (Exception)
            {
                return Redirect("/Agency/Login");
            }
        }

        [HttpGet]
        public ActionResult Account()
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Agency);
                ViewBag.title_login = new Helper.AuthenticationHelper(Session).getTitle();
                string subid = (new AuthenticationHelper(Session).getSub());
                ViewBag.subid = subid;
                using (var db = new GamePaymentContext())
                {
                    var agencyID = (new AuthenticationHelper(Session).ID());
                    var wi = db.Agency.FirstOrDefault(e => e.id == agencyID);
                    string phone_new = wi.phone.Trim();
                    var chars = phone_new.ToCharArray();

                    string phone_response = "";
                    for (int ctr = 0; ctr < chars.Length; ctr++)
                    {
                        if (ctr < chars.Length - 4)
                        {
                            phone_response += "*";
                        }
                        else
                        {
                            phone_response += chars[ctr];
                        }
                    }
                    ViewBag.phone = phone_response;
                    return View("Account");
                }

            }
            catch (Exception)
            {
                return Redirect("/Agency/Login");
            }
        }


        [HttpPost]
        public ActionResult CashIORate(string gateid)
        {
            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            try
            {
                var agencyID = (new AuthenticationHelper(Session).ID());
                var gateID = Int32Ext.toInt(gateid);
                using (var db = new GamePaymentContext())
                {
                    var query = from g in db.CashIORate
                                where (g.payment_gate_id == gateID)
                                select new
                                {
                                    g.id,
                                    g.cash_in,
                                    g.cash_out
                                };

                    return Json(new
                    {
                        error = false,
                        items = query.ToList()
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }

        [HttpPost]
        public ActionResult AdminTransferCash(string agencyid, string cash, string walletid, string content, string money_in, string paymentgate_id)
        {
            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            int agencyID = Int32Ext.toInt(agencyid);
            double intCash = Double.Parse(cash);
            double intMoney_out = Double.Parse(money_in);
            if (intMoney_out <= 0)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tiền giao dịch không được <= 0"
                });
            }
            int walletID = Int32Ext.toInt(walletid);
            int paymentgateID = Int32Ext.toInt(paymentgate_id);

            // Do process it
            (new ManagerPrivateServices()).createFundAgency(agencyID, intCash, walletID, content, intMoney_out, paymentgateID);
            return Json(new
            {
                error = false,
                msg = "Tạo yêu cầu thanh toán thành công, chờ duyệt bởi kế toán"
            });

        }

        [HttpPost]
        public ActionResult CreateWallet(string title, string agencyid)
        {
            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            // Check permission here
            try
            {
                int agencyID = Int32Ext.toInt(agencyid);
                // Do process it
                (new ManagerPrivateServices()).createWallet(title, agencyID);
                return Json(new
                {
                    error = false,
                    msg = "Tạo ví thành công"
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = true,
                    msg = e.Message.ToString()
                });
            }
        }


        //Danh sách user theo datatable
        [HttpPost]
        public ActionResult Listdata(String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength)
        {
            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            string[] a_sort = new string[] { "id", "title", "status_active", "location", "id", "id" };
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT" +
                " id" +
                " ,title" +
                " ,subid" +
                " ,username" +
                " ,password" +
                " ,location" +
                " ,status_active" +
                " FROM Agency WHERE subid =0 ";

            string queryCount = "SELECT COUNT(id) as 'count' FROM Agency WHERE subid =0 ";

            if (sSortDir_0.Equals("desc"))
            {
                sSortDir_0 = "DESC";
            }
            else
            {
                sSortDir_0 = "ASC";
            }
            queryString += " ORDER BY " + a_sort[Int32Ext.toInt(iSortCol_0)] + " " + sSortDir_0 + " OFFSET @iDisplayStart ROWS FETCH NEXT @iDisplayLength ROWS ONLY";
            // SqlDataReader reader = serviceBase.query(queryString);
            SqlCommand command;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@iDisplayStart", Int32Ext.toInt(iDisplayStart));
            command.Parameters.AddWithValue("@iDisplayLength", Int32Ext.toInt(iDisplayLength));
            SqlDataReader reader = serviceBase.querySqlParam(command);

            List<String[]> users = new List<String[]>();
            while (reader.Read())
            {
                string[] s_user = new string[7];
                s_user[0] = reader["id"].ToString();
                s_user[1] = reader["title"].ToString();
                s_user[2] = reader["subid"].ToString();
                s_user[3] = "<a data-active=\"" + reader["status_active"].ToString() + "\" onclick=\"agency.showEditActive(" + reader["id"].ToString() + ",this)\">" + reader["status_active"].ToString() + "</a>";
                s_user[4] = reader["location"].ToString();
                s_user[5] = "";
                s_user[6] = "<button onclick=\"agency.showEdit('" + reader["id"].ToString() + "')\" type=\"button\" class=\"btn btn-default btn-xs\">Sửa</button><button onclick=\"agency.showDelete('" + reader["id"].ToString() + "')\"  type=\"button\" class=\"btn btn-danger btn-xs\">Xóa</button>";
                users.Add(s_user);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            //List<String[]> users1 = new List<String[]>();
            users.ForEach(w =>
            {
                var subid = Convert.ToInt32(w[2]);
                if (subid != 0)
                {
                    var idx = users.IndexOf(w);
                    var queryStr = "SELECT title FROM Agency WHERE id=" + subid;
                    reader = serviceBase.query(queryStr);
                    while (reader.Read())
                    {
                        w[2] = reader["title"].ToString();
                    }
                    reader.Close();
                    DatabaseService.Instance.getConnection().Close();
                }
                else
                {
                    w[2] = "";
                }
                //users1.Add(w);
            });

            List<String[]> users_response = new List<String[]>();
            users.ForEach(w =>
            {
                w[5] = this.getStringWallets(w[0]);
                users_response.Add(w);
            });

            int row = 0;
            reader = serviceBase.query(queryCount);
            while (reader.Read())
            {
                row = Int32.Parse(reader["count"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users_response,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength
            });
        }

        private List<E> ShuffleList<E>(List<E> inputList)
        {
            List<E> randomList = new List<E>();

            Random r = new Random();
            int randomIndex = 0;
            while (inputList.Count > 0)
            {
                randomIndex = r.Next(0, inputList.Count); //Choose a random object in the list
                randomList.Add(inputList[randomIndex]); //add it to the new, random list
                inputList.RemoveAt(randomIndex); //remove to avoid duplicates
            }

            return randomList; //return the new random list
        }

        //Danh sách user theo datatable
        [HttpPost]
        public ActionResult shuffleAgency(String s_seach, String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength)
        {
            using (var db = new GamePaymentContext())
            {
                string[] a_sort = new string[] { "id", "title", "status_active", "location", "id", "id" };
                ServiceBase serviceBase = new ServiceBase();
                SqlDataReader reader;
                

                //shufle list agency
                string queryCount = "SELECT id,(SELECT COUNT(id) FROM Agency) as 'count' FROM Agency";
                int row = 0;
                List<int> lstId = new List<int>();
                reader = serviceBase.query(queryCount);
                while (reader.Read())
                {
                    row = Int32.Parse(reader["count"].ToString());
                    lstId.Add(Int32.Parse(reader["id"].ToString()));
                }
                DatabaseService.Instance.getConnection().Close();
                
                var list = Enumerable.Range(1, row).ToList();
                list = ShuffleList(list);
                for (int i = 0; i < list.Count; i++)
                {
                    var agency_item = db.Agency.FirstOrDefault(g => g.id == lstId[i]);
                    agency_item.orderId = list[i];
                    db.SaveChanges();
                    continue;
                }

                string queryString = "SELECT" +
                    " id" +
                    " ,title" +
                    " ,subid" +
                    " ,username" +
                    " ,password" +
                    " ,location" +
                    " ,status_active" +
                    " FROM Agency ORDER BY orderId ASC";

                reader = serviceBase.query(queryString);

                List<String[]> users = new List<String[]>();
                while (reader.Read())
                {
                    string[] s_user = new string[7];
                    s_user[0] = reader["id"].ToString();
                    s_user[1] = reader["title"].ToString();
                    s_user[2] = reader["subid"].ToString();
                    s_user[3] = "<a data-active=\"" + reader["status_active"].ToString() + "\" onclick=\"agency.showEditActive(" + reader["id"].ToString() + ",this)\">" + reader["status_active"].ToString() + "</a>";
                    s_user[4] = reader["location"].ToString();
                    s_user[5] = "";
                    s_user[6] = "<button onclick=\"agency.showEdit('" + reader["id"].ToString() + "')\" type=\"button\" class=\"btn btn-default btn-xs\">Sửa</button><button onclick=\"agency.showDelete('" + reader["id"].ToString() + "')\"  type=\"button\" class=\"btn btn-danger btn-xs\">Xóa</button>";
                    users.Add(s_user);
                }
                DatabaseService.Instance.getConnection().Close();

                users.ForEach(w =>
                {
                    var subid = Convert.ToInt32(w[2]);
                    if (subid != 0)
                    {
                        var idx = users.IndexOf(w);
                        var queryStr = "SELECT title FROM Agency WHERE id=" + subid;
                        reader = serviceBase.query(queryStr);
                        while (reader.Read())
                        {
                            w[2] = reader["title"].ToString();
                        }

                        DatabaseService.Instance.getConnection().Close();
                    }
                    else
                    {
                        w[2] = "";
                    }
                });

                List<String[]> users_response = new List<String[]>();
                users.ForEach(w =>
                {
                    w[5] = this.getStringWallets(w[0]);
                    users_response.Add(w);
                });


                return Json(new
                {
                    aaData = users_response,
                    iTotalRecords = row,
                    iTotalDisplayRecords = row,
                    display = iDisplayLength
                });
            }
               
        }

        private string getStringWallets(string stringAgencyID)
        {
            var agencyID = Int32Ext.toInt(stringAgencyID);
            var managerWallets = (new AgencyPrivateServices()).getWallets(agencyID);
            string html = "<ul>";

            managerWallets.ForEach(w =>
            {
                html += String.Format("<li>{0} <span class=\"btn btn-xs btn-success pull-right\" onclick=\"agency.showPayAgencyWalletForm({1},{2})\">Nạp tiền</span></li>", w.name, stringAgencyID, w.id);
            });
            html += "</ul>";
            return html;
        }

        //Edit user active/deactive
        [HttpPost]
        public ActionResult postActive(int id, string status, string note)
        {
            int userID = 0;
           
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            id = Int32Ext.toInt(id.ToString());
            using (var db = new GamePaymentContext())
            {
                var agency_item = db.Agency.FirstOrDefault(a => a.id == id);
                agency_item.status_active = status;
                db.SaveChanges();
                db.ActivityLog.Add(new ActivityLog()
                {
                    accountid = Int32Ext.toInt(Session["UserID"].ToString()),
                    userid = id,
                    type = status,
                    note = note,
                    type_action = "agency"
                });
                db.SaveChanges();
            }

            return Json(new
            {
                error = false
            });
        }
        public bool validateUsername(string username)
        {
            Regex regex = new Regex("^[a-z0-9_-]{3,24}$");
            return regex.IsMatch(username);
        }

        public bool validateEmail(string email)
        {
            Regex regex = new Regex("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
            return regex.IsMatch(email);
        }

        public bool validatePassword(string password)
        {
            if (password.Length > 5)
            {
                return true;
            }
            return false;
        }

        public bool validatePhone(string phone)
        {
            if (phone.Length > 11 || phone.Length < 10)
            {
                return false;
            }
            Regex regex = new Regex(@"^-*[0-9,\.?\-?\(?\)?\ ]+$");
            return regex.IsMatch(phone);
        }
        [HttpPost]
        public ActionResult add(string title, string username, string phone, string password, string email, string merchant_id, string location, int appear, int orderId)
        {
            int userID = 0;

            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }

            if (!this.validateUsername(username))
            {
                return Json(new
                {
                    error = true,
                    msg = "Username không hợp lệ (Không chứa ký tự đặc biểt > 3, < 24 ký tự)."
                });
            }
            if (!this.validatePassword(password))
            {
                return Json(new
                {
                    error = true,
                    msg = "Password không hợp lệ ( > 6 ký tự)."
                });
            }
            if (!this.validatePhone(phone))
            {
                return Json(new
                {
                    error = true,
                    msg = "Điện thoại không đúng định dạng."
                });
            }

            if (!this.validateEmail(email))
            {
                return Json(new
                {
                    error = true,
                    msg = "Email không đúng định dạng."
                });
            }


            using (var db = new GamePaymentContext())
            {
                var inst = new Agency
                {
                    title = title,
                    username = username,
                    phone = phone,
                    password = MD5Hash(password),
                    email = email,
                    status_active = "active",
                    location = location,
                    is_appear = appear
                };
                db.Agency.Add(inst);
                db.SaveChanges();
                int id_insert = inst.id;
                var agency_item = db.Agency.FirstOrDefault(e => e.username == username);
                agency_item.password = CryptoHelper.MD5Hash(password, agency_item.id.ToString());
                if (orderId == 0)
                {
                    agency_item.orderId = 1;
                }
                else
                {
                    agency_item.orderId = orderId;
                }
                db.SaveChanges();

                string[] strArrayOne = new string[] { "" };
                strArrayOne = merchant_id.Split(',');
                foreach (string element in strArrayOne)
                {
                    var agency_merchant = new Agency_Merchant
                    {
                        agency_id = inst.id,
                        merchant_id = Int32.Parse(element)
                    };
                    db.Agency_Merchant.Add(agency_merchant);
                    db.SaveChanges();
                }
            }


            return Json(new
            {
                error = false
            });
        }
        [HttpPost]
        public ActionResult edit(string title, string username, string phone, string password, string email, int id, string merchant_id, string location, int appear, int orderId)
        {
            int userID = 0;

            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
                if (!this.validateUsername(username))
            {
                return Json(new
                {
                    error = true,
                    msg = "Username không hợp lệ (Không chứa ký tự đặc biểt > 3, < 24 ký tự)."
                });
            }

            if (!this.validatePhone(phone))
            {
                return Json(new
                {
                    error = true,
                    msg = "Điện thoại không đúng định dạng."
                });
            }

            if (!this.validateEmail(email))
            {
                return Json(new
                {
                    error = true,
                    msg = "Email không đúng định dạng."
                });
            }
            var orderInsert = 0;
            if (orderId == 0)
            {
                orderInsert = 1;
            }
            else
            {
                orderInsert = orderId;
            }
            using (var db = new GamePaymentContext())
            {
                var agency_item = db.Agency.FirstOrDefault(g=>g.id == id);
                agency_item.title = title;
                agency_item.username = username;
                agency_item.phone = phone;
                agency_item.email = email;
                agency_item.location = location;
                agency_item.orderId = orderInsert;
                agency_item.is_appear = appear;
                if (!password.Trim().Equals(""))
                {
                    agency_item.password = CryptoHelper.MD5Hash(password, id.ToString());
                }
                db.SaveChanges();


                var list_agency_merchant = db.Agency_Merchant.Where(a => a.agency_id == id).ToList();
                list_agency_merchant.ForEach(ent =>
                {
                    var am = db.Agency_Merchant.FirstOrDefault(o => (o.id == ent.id));
                    db.Agency_Merchant.Remove(am);
                    db.SaveChanges();
                });

                string[] strArrayOne = new string[] { "" };
                //somewhere in your code
                strArrayOne = merchant_id.Split(',');
                foreach (string element in strArrayOne)
                {
                    int int_ele = Int32Ext.toInt(element);
                    db.Agency_Merchant.Add(new Agency_Merchant()
                    {
                        agency_id = id,
                        merchant_id = int_ele
                    });
                    db.SaveChanges();
                }
            }


            return Json(new
            {
                error = false
            });
        }

        [HttpPost]
        public ActionResult delete(int id)
        {
            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)

            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            using (var db = new GamePaymentContext())
            {
                var admintranfer = db.AdminMoneyTransferCommit.FirstOrDefault(a=> a.agency_id == id);
                if (admintranfer != null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Đã có giao dịch Agency này không thể xóa."
                    });
                }
                var agency_item = db.Agency.FirstOrDefault(a => a.id == id);
                db.Agency.Remove(agency_item);
            } 

            return Json(new
            {
                error = false
            });

        }

        public ActionResult deleteWalletAgcency(int id)
        {
            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            using (var db = new GamePaymentContext())

            {
                var admintranfer = db.AdminMoneyTransferCommit.FirstOrDefault(a => a.wallet_id == id);
                if (admintranfer != null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Đã có giao dịch Agency này không thể xóa."
                    });
                }
                var managerWallet = db.ManagerWallet.FirstOrDefault(a => a.id == id);
                db.ManagerWallet.Remove(managerWallet);
                return Json(new
                {
                    error = false
                });
            }

        }

        [HttpPost]
        public ActionResult detail(int id)
        {
            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            using (var db = new GamePaymentContext())
            {
                var agency_item = db.Agency.FirstOrDefault(g => g.id == id);
                string phone_new = agency_item.phone;
                var chars = phone_new.ToCharArray();
                string email = agency_item.email;

                string phone_response = "";
                for (int ctr = 0; ctr < chars.Length; ctr++)
                {
                    if (ctr < chars.Length - 4)
                    {
                        phone_response += "*";
                    }
                    else
                    {
                        phone_response += chars[ctr];
                    }
                }
                agency_item.phone_new = phone_response;


                string email_response = "";
                for (int ctr = 0; ctr < email.Length; ctr++)
                {
                    if (ctr > 3)
                    {
                        email_response += "*";
                    }
                    else
                    {
                        email_response += email[ctr];
                    }
                }

                var list_merchant = db.Agency_Merchant.Where(a => a.agency_id == id).ToList();
                List<string> a_merchant_id = new List<string>();
                list_merchant.ForEach(ent=> {
                    a_merchant_id.Add(ent.ToString());
                });

                return Json(new
                {
                    error = false,
                    item = agency_item,
                    merchant_id = a_merchant_id
                });
            }           
        }

        public string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
    }
}