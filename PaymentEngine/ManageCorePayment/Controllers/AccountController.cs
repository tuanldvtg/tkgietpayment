﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers
{
    public class AccountController : Controller
    {
        //Danh sách user theo datatable
        [HttpPost]
        public ActionResult Listdata(String s_seach, int iSortCol_0, string sSortDir_0, String iDisplayStart, String iDisplayLength, String merchant_id)
        {
            if (s_seach == null || iSortCol_0 == null || sSortDir_0 == null || iDisplayStart == null || iDisplayLength == null || merchant_id == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            string[] a_sort = new string[] { "Users.id", "Users.merchant_id", "Users.username", "Users.facebook", "Users.email", "Users.status_active", "Users.id", "Users.id", "Users.id" ,"Users.id" };
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT" +
                " Users.id" +
                " ,Users.merchant_id" +
                " ,Users.username" +
                " ,Users.facebook" +
                " ,Users.email" +
                " ,Users.status_active" +
                " ,Users.verify" +
                " ,Users.gmail_id" +
                " ,Merchant.name as 'name_mer'" +
                " FROM Users INNER JOIN Merchant ON Users.merchant_id = Merchant.id WHERE Users.id != 0";

            string queryCount = "SELECT COUNT(id) as 'count' FROM Users WHERE Users.id != 0";

            if (merchant_id != "all")
            {
                queryString += " AND Users.merchant_id =@merchant_id";
                queryCount += " AND Users.merchant_id =@merchant_id";
            }
            if (s_seach != "")
            {
                queryString += " AND Users.username LIKE Concat('%',@s_seach,'%') ";
                queryCount += " AND Users.username LIKE 'Concat('%',@s_seach,'%') ";
            }

            if (sSortDir_0.Equals("desc"))
            {
                sSortDir_0 = "DESC";
            }
            else
            {
                sSortDir_0 = "ASC";
            }
            if (iSortCol_0 > 9)
            {
                iSortCol_0 = 0;
            }
            queryString += " ORDER BY "+ a_sort[iSortCol_0] + " "+ sSortDir_0 + " OFFSET @iDisplayStart ROWS FETCH NEXT @iDisplayLength ROWS ONLY";
            SqlCommand command;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@merchant_id", merchant_id);
            command.Parameters.AddWithValue("@s_seach", s_seach);
            command.Parameters.AddWithValue("@iDisplayStart", Int32Ext.toInt(iDisplayStart));
            command.Parameters.AddWithValue("@iDisplayLength", Int32Ext.toInt(iDisplayLength));
            //command.Parameters.AddWithValue("@orderby", a_sort[iSortCol_0]);
            //command.Parameters.AddWithValue("@modecuong", sSortDir_0);

            SqlDataReader reader = serviceBase.querySqlParam(command);

            List<String[]> users = new List<String[]>();
            while (reader.Read())
            {
                string verify = "";
                if (!reader["verify"].ToString().Trim().Equals("0"))
                {
                    verify = "<span class=\"label label-primary\">Đã verify</span>";
                }
                string[] s_user = new string[8];

                s_user[0] = reader["id"].ToString();
                s_user[1] = reader["name_mer"].ToString();
                s_user[2] = reader["username"].ToString();
                s_user[3] = "<a data-active=\"" + reader["status_active"].ToString() + "\" onclick=\"account.showEditActive(" + reader["id"].ToString() + ",this)\">" + reader["status_active"].ToString() + "</a>";
                s_user[4] = "<button onclick=\"account.getListInputOut(" + reader["id"].ToString() + ",'"+ reader["username"].ToString() + "')\" type=\"button\" class=\"btn btn-default\">Thống kê</button>";
                s_user[5] = "<a onclick=\"account.getListWallet(" + reader["id"].ToString() + ")\">Chi tiết</a>";
                s_user[6] = "<a onclick=\"account.showAction(" + reader["id"].ToString() + ")\">Chi tiết</a>";
                s_user[7] = verify;
                users.Add(s_user);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            int row = 0;
            command = new SqlCommand(queryCount, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@merchant_id", merchant_id);
            command.Parameters.AddWithValue("@s_seach", s_seach);
            reader = serviceBase.querySqlParam(command);
            while (reader.Read())
            {
                row =Int32.Parse(reader["count"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength
            });
        }

        //Edit user active/deactive
        [HttpPost]
        public ActionResult postActive(int id, string status, string note)
        {
            if (id == 0 || status == null || note == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;
            //Update user

            string queryStringUpdateUser = "UPDATE Users SET status_active=@status WHERE id =@id";
            if (status == "deactive")
            {
                status = "deactive";
            }
            else
            {
                status = "active";
            }
            SqlCommand command;
            command = new SqlCommand(queryStringUpdateUser, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@status", status);
            command.Parameters.AddWithValue("@id", id);
            reader = serviceBase.querySqlParam(command);
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            //Insert Log
            string queryStringInsertLog = "INSERT INTO ActivityLog(accountid,userid,type,note,type_action) VALUES (@user_id,@id,@status,@note,'account')";
            command = new SqlCommand(queryStringInsertLog, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@user_id", Int32Ext.toInt(Session["UserID"].ToString()) );
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@status",status);
            command.Parameters.AddWithValue("@note", note);
            reader = serviceBase.querySqlParam(command);
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                error = false
            });
        }


        public ActionResult ListdataHistory( int iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength, int userid, int wallet_id)
        {
            if (iSortCol_0 == null || sSortDir_0 == null || iDisplayStart == null || iDisplayLength == null || userid == null || wallet_id == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            using (var db = new GamePaymentContext())
            {
                string[] a_sort = new string[] { "id", "time_creat", "payment_gate", "user_send", "value", "balance", "tag", "message" };
                ServiceBase serviceBase = new ServiceBase();
                string queryString = "SELECT" +
                    " id" +
                    " ,value" +
                    " ,time_creat" +
                    " ,(SELECT TOP 1 name FROM PaymentGate WHERE id = Transactions.payment_gate) as 'payment_gate'" +
                    " ,user_send" +
                    " ,transactions_type" +
                    " ,tag" +
                     " ,message" +
                    " ,type" +
                    " ,balance" +
                    " FROM Transactions WHERE user_id =@user_id AND wallet_id =@wallet_id";
                string queryCount = "SELECT COUNT(id) as 'count' " +
                    " FROM Transactions WHERE user_id =@user_id AND wallet_id =@wallet_id AND transactions_type = 0";
                if (sSortDir_0.Equals("desc"))
                {
                    sSortDir_0 = "DESC";
                }
                else
                {
                    sSortDir_0 = "ASC";
                }
                if (iSortCol_0 >7)
                {
                    iSortCol_0 = 0;
                }
                queryString += " ORDER BY " + a_sort[iSortCol_0] + " " + sSortDir_0 + " OFFSET @iDisplayStart ROWS FETCH NEXT @iDisplayLength ROWS ONLY";
                SqlDataReader reader;
                string sort = a_sort[iSortCol_0];
                string sortmode = sSortDir_0.Trim().ToString();
                SqlCommand command;
                command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@user_id", userid.ToString());
                command.Parameters.AddWithValue("@wallet_id", wallet_id);
                command.Parameters.AddWithValue("@sort", sort);
                command.Parameters.AddWithValue("@sortmode", sortmode);
                command.Parameters.AddWithValue("@iDisplayStart", Int32.Parse(iDisplayStart));
                command.Parameters.AddWithValue("@iDisplayLength", Int32.Parse(iDisplayLength));
                reader = serviceBase.querySqlParam(command);
                List<String[]> historys = new List<String[]>();
                while (reader.Read())
                {
                    string[] s_history = new string[8];
                    s_history[0] = reader["id"].ToString();
                    s_history[1] = reader["time_creat"].ToString();
                    s_history[2] = reader["payment_gate"].ToString();

                    s_history[3] = "";
                    
                    if (!reader["user_send"].ToString().Trim().Equals("0") && !reader["user_send"].ToString().Trim().Equals(null))
                    {
              
                            var user_send = Int32.Parse(reader["user_send"].ToString().Trim());
                            var user_item = db.User.Where(u => (u.id == user_send)).FirstOrDefault();
                            if (user_item != null)
                            {
                                s_history[3] = user_item.username;
                            }
                            else
                            {
                                var agency_item = db.Agency.Where(u => (u.id == user_send)).FirstOrDefault();
                                if (agency_item != null)
                                {
                                    s_history[3] = agency_item.title;
                                }
                            } 
                    }
                    string value = Double.Parse(reader["value"].ToString()).ToString("N", CultureInfo.InvariantCulture);
                    string type = reader["type"].ToString().Trim();
                    string type_com = "plus";
                    if (type.Equals(type_com))
                    {
                        s_history[4] = "<a style=\"color:#00c851\" >+" + value + "</a>";
                    }
                    else
                    {
                        s_history[4] = "<a style=\"color:red\">-" + value + "</a>";
                    }
                    s_history[5] = Double.Parse(reader["balance"].ToString()).ToString("N", CultureInfo.InvariantCulture);
                    s_history[6] = reader["tag"].ToString().Trim();
                    s_history[7] = reader["message"].ToString().Trim();
                    historys.Add(s_history);
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();
                int row = 0;
                command = new SqlCommand(queryCount, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@user_id", userid.ToString());
                command.Parameters.AddWithValue("@wallet_id", wallet_id);
                reader = serviceBase.querySqlParam(command);
                while (reader.Read())
                {
                    row = Int32.Parse(reader["count"].ToString());
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();
                return Json(new
                {
                    aaData = historys,
                    iTotalRecords = row,
                    iTotalDisplayRecords = row,
                    display = iDisplayLength
                });
            }
        }


        [HttpPost]
        public ActionResult editPasswordAccount(string password,string password_old)
        {

            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            if (password.Length < 6)
            {
                return Json(new
                {
                    error = true,
                    msg = "Mật khẩu không được <=6 ký tự."
                });
            }
            
            using (var db = new GamePaymentContext())

            {
                var admin = db.Admin.FirstOrDefault(u => u.username == "admin");
                if (!admin.password.Trim().Equals(CryptoHelper.MD5Hash(password_old, admin.id.ToString()).Trim()))
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Mật khẩu cũ không đúng."
                    });
                }
                admin.password = CryptoHelper.MD5Hash(password, admin.id.ToString()).Trim();
                db.SaveChanges();
                return Json(new
                {
                    error = false
                });
            }
        }

        //updatePassword

        [HttpPost]
        public ActionResult updatePassword(int id,string password)
        {
            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            if (password == null || password.Length < 6)
            {
                return Json(new
                {
                    error = true,
                    msg = "Mật khẩu không được <=6 ký tự."
                });
            }
            using (var db = new GamePaymentContext())
            {
                var user = db.User.FirstOrDefault(u => u.id == id);
                user.password = CryptoHelper.MD5Hash(password, id.ToString());
                db.SaveChanges();
            };

            return Json(new
            {
                error = false
            });
        }

        private DateTime[] GetDatesBetween(DateTime startDate, DateTime endDate)
        {
            if (endDate > DateTime.Now)
            {
                endDate = DateTime.Now;
            }
            List<DateTime> allDates = new List<DateTime>();
            for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                allDates.Add(date);
            return allDates.ToArray();
        }

        [HttpPost]
        public ActionResult getDataDetailTransaction(string id, String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength, string start_time, string end_time)
        {
            if (id == null || iSortCol_0 == null || sSortDir_0 == null || iDisplayStart == null || iDisplayLength == null || start_time == null || end_time == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            DateTime dDate;

            if (!DateTime.TryParse(start_time, out dDate))
            {
                return Json(new
                {
                    error = true,
                    msg = "start_time không đúng định dạng"
                });
            }
            if (!DateTime.TryParse(end_time, out dDate))
            {
                return Json(new
                {
                    error = true,
                    msg = "end_time không đúng định dạng"
                });
            }
            string[] a_start_time_new = start_time.Split('/');
            string start_time_new = a_start_time_new[2] + "-" + a_start_time_new[0] + "-" + a_start_time_new[1] + " 00:00:00.000";

            string[] a_end_time_new = end_time.Split('/');
            string end_time_new = a_end_time_new[2] + "-" + a_end_time_new[0] + "-" + a_end_time_new[1] + " 23:59:59.999";

            string[] a_sort = new string[] { "id", "time_creat", "payment_gate", "value", "value_out", "tag", "message" };
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT" +
    " id" +
    " ,time_creat" +
    " ,value" +
    " ,type" +
     " ,(SELECT TOP 1 name FROM PaymentGate WHERE id = Transactions.payment_gate) as 'payment_gate'" +
    " ,value_out " +
    " ,tag" +
     " ,message" +
    " FROM Transactions WHERE user_id =@user_id AND wallet_id= 0 AND transactions_type = 0 AND time_creat >= @start_time_new AND time_creat <=@end_time_new ";
            if (sSortDir_0.Equals("desc"))
            {
                sSortDir_0 = "DESC";
            }
            else
            {
                sSortDir_0 = "ASC";
            }
            queryString += " ORDER BY " + a_sort[Int32Ext.toInt(iSortCol_0)] + " " + sSortDir_0 + " OFFSET @iDisplayStart ROWS FETCH NEXT @iDisplayLength ROWS ONLY";
            // SqlDataReader reader = serviceBase.query(queryString);
            SqlCommand command;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@user_id", id);
            command.Parameters.AddWithValue("@start_time_new", start_time_new);
            command.Parameters.AddWithValue("@end_time_new", end_time_new);
            command.Parameters.AddWithValue("@iDisplayStart", Int32Ext.toInt(iDisplayStart));
            command.Parameters.AddWithValue("@iDisplayLength", Int32Ext.toInt(iDisplayLength));
            SqlDataReader reader = serviceBase.querySqlParam(command);

            string queryCount = "SELECT COUNT(id) as 'count' FROM Transactions WHERE user_id =@user_id AND transactions_type = 0 AND wallet_id= 0 AND time_creat >=@start_time_new AND time_creat <=@end_time_new ";

            List<String[]> users = new List<String[]>();

            while (reader.Read())
            {
                string[] s_user = new string[7];
                s_user[0] = reader["id"].ToString().Trim();
                s_user[1] = reader["time_creat"].ToString().Trim();
                s_user[2] = reader["payment_gate"].ToString();
                if (reader["type"].ToString().Trim().Equals("plus"))
                {
                    s_user[3] = "<a style=\"color:#4CB052\" >+" + Double.Parse(reader["value"].ToString()).ToString("N", CultureInfo.InvariantCulture) +"</a>";
                    s_user[4] = "<a style=\"color:#4CB052\" >+" + Double.Parse(reader["value_out"].ToString()).ToString("N", CultureInfo.InvariantCulture) + "</a>";
                }
                else
                {
                    s_user[3] = "<a style=\"color:red\" >-" + Double.Parse(reader["value"].ToString()).ToString("N", CultureInfo.InvariantCulture) + "</a>";
                    s_user[4] = "<a style=\"color:red\" >-" + Double.Parse(reader["value_out"].ToString()).ToString("N", CultureInfo.InvariantCulture) + "</a>";
                }

                s_user[5] = reader["tag"].ToString();
                s_user[6] = reader["message"].ToString();
                users.Add(s_user);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            int row = 0;
            command = new SqlCommand(queryCount, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@user_id", id);
            command.Parameters.AddWithValue("@start_time_new", start_time_new);
            command.Parameters.AddWithValue("@end_time_new", end_time_new);
            reader = serviceBase.querySqlParam(command);
            while (reader.Read())
            {
                row = Int32.Parse(reader["count"].ToString());
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength
            });
        }
        private DateTime[] GetMonthBetween(DateTime startDate, DateTime endDate)
        {
            if (endDate > DateTime.Now)
            {
                endDate = DateTime.Now; 
            }
            List<DateTime> allDates = new List<DateTime>();
            for (DateTime date = startDate; date <= endDate; date = date.AddMonths(1))
                allDates.Add(date);
            return allDates.ToArray();
        }
        [HttpPost]

        public ActionResult GetListInputOutMonth(string id, string start_time, string end_time)
        {
            if (id == null || start_time == null || end_time == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            DateTime dDate;

            if (!DateTime.TryParse(start_time, out dDate))
            {
                return Json(new
                {
                    error = true,
                    msg = "start_time không đúng định dạng"
                });
            }
            if (!DateTime.TryParse(end_time, out dDate))
            {
                return Json(new
                {
                    error = true,
                    msg = "end_time không đúng định dạng"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            int userID = Int32Ext.toInt(id);

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string[] a_start_time_new = start_time.Split('/');
            string start_time_new = a_start_time_new[2] + "-" + a_start_time_new[0] + "-" + a_start_time_new[1] + " 00:00:00.000";

            string[] a_end_time_new = end_time.Split('/');
            string end_time_new = a_end_time_new[2] + "-" + a_end_time_new[0] + "-" + a_end_time_new[1] + " 23:59:59.999";
            DateTime[] dates = this.GetMonthBetween(DateTime.Parse(start_time_new), DateTime.Parse(end_time_new));
            if (dates.Length > 24)
            {
                return Json(new
                {
                    error = true,
                    msg = "Số ngày chọn quá lớn không thể thống kê."
                });
            }

            using (var db = new GamePaymentContext())
            {
                //Tìm giao dịch nạp rút trong khoảng thời gian
                var time_startParameter = new SqlParameter("@timestart", start_time_new);
                var time_endParameter = new SqlParameter("@timeend", end_time_new);
                var usersend_idParameter = new SqlParameter("@user_send", userID);
                var result = db.Database
                    .SqlQuery<Pro_statistics_user_inout_month>("Pro_statistics_user_inout_month @user_send , @timestart , @timeend", usersend_idParameter, time_startParameter, time_endParameter).ToList();

                //Tính nạp rút tất cả thời gian thống kê dựa trên kết quả nhận được
                Pro_statistics_user_inout[] list_date_query = new Pro_statistics_user_inout[dates.Length];
                for (int i = 0; i < list_date_query.Length; i++)
                {
                    bool check_is = false;

                    result.ForEach(ent =>
                    {

                        if (dates[i].Year == ent.year && dates[i].Month == ent.month)
                        {
                            check_is = true;
                            Pro_statistics_user_inout pro_statistics = new Pro_statistics_user_inout();
                            pro_statistics.date = dates[i];
                            if (ent.balance_in != 0)
                            {
                                pro_statistics.balance_in = ent.balance_in;
                            }
                            else
                            {
                                if (i == 0)
                                {
                                    string queryStringBalance = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 0 AND type ='plus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                                    reader = serviceBase.query(queryStringBalance);
                                    double balance = 0;
                                    while (reader.Read())
                                    {
                                        balance = Double.Parse(reader["balance"].ToString());
                                    }
                                    reader.Close();
                                    DatabaseService.Instance.getConnection().Close();
                                    pro_statistics.balance_in = balance;
                                }
                                else
                                {
                                    pro_statistics.balance_in = list_date_query[i - 1].balance_in;
                                }

                            }
                            if (ent.balance_out != 0)
                            {
                                pro_statistics.balance_out = ent.balance_out;
                            }
                            else
                            {
                                if (i == 0)
                                {

                                    string queryStringBalanceOut = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 0 AND type ='minus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                                    reader = serviceBase.query(queryStringBalanceOut);
                                    double balance_out = 0;
                                    while (reader.Read())
                                    {
                                        balance_out = Double.Parse(reader["balance"].ToString());
                                    }
                                    reader.Close();
                                    DatabaseService.Instance.getConnection().Close();
                                    pro_statistics.balance_out = balance_out;
                                }
                                else
                                {
                                    pro_statistics.balance_out = list_date_query[i - 1].balance_out;
                                }

                            }
                            list_date_query[i] = pro_statistics;
                        }
                    });

                    if (!check_is)
                    {
                        if (i > 0)
                        {
                            Pro_statistics_user_inout pro_statistics = new Pro_statistics_user_inout();
                            pro_statistics.date = dates[i];
                            pro_statistics.balance_in = list_date_query[i - 1].balance_in;
                            pro_statistics.balance_out = list_date_query[i - 1].balance_out;
                            list_date_query[i] = pro_statistics;

                        }
                        else
                        {
                            string queryStringBalance = "SELECT TOP 1 balance FROM Transactions WHERE user_id = "+ userID + " AND wallet_id = 0 AND transactions_type = 0 AND type ='plus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                            reader = serviceBase.query(queryStringBalance);
                            double balance = 0;
                            while (reader.Read())
                            {
                                balance = Double.Parse(reader["balance"].ToString());
                            }
                            reader.Close();
                            DatabaseService.Instance.getConnection().Close();

                            string queryStringBalanceOut = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 0 AND type ='minus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                            reader = serviceBase.query(queryStringBalanceOut);
                            double balance_out = 0;
                            while (reader.Read())
                            {
                                balance_out = Double.Parse(reader["balance"].ToString());
                            }
                            reader.Close();
                            DatabaseService.Instance.getConnection().Close();


                            Pro_statistics_user_inout pro_statistics = new Pro_statistics_user_inout();
                            pro_statistics.date = dates[i];
                            pro_statistics.balance_in = balance;
                            pro_statistics.balance_out = balance_out;
                            list_date_query[i] = pro_statistics;
                        }

                    }
                }
                var list = list_date_query;
                var historyStruct = new[] { new {
                            date = "",
                            value = 0.1
                        }}.ToList();
                historyStruct.Clear();



                var historyStructOut = new[] { new {
                            date = "",
                            value = 0.1
                        }}.ToList();
                historyStructOut.Clear();


                //Lấy danh sách ngày chọn thống kê trả về 2 mảng
                for (int i = 0; i < list_date_query.Length; i++)
                {

                    historyStruct.Add(new
                    {
                        date = list_date_query[i].date.ToString("M/yyyy"),
                        value = list_date_query[i].balance_in,
                    });
                    historyStructOut.Add(new
                    {
                        date = list_date_query[i].date.ToString("M/yyyy"),
                        value = list_date_query[i].balance_out,
                    });
                }
                var total_nowbalance = historyStruct.Last().value - historyStruct.First().value;
                var total_nowbalance_out = historyStructOut.Last().value - historyStructOut.First().value;
                return Json(new
                {
                    error = false,
                    balance = Double.Parse(total_nowbalance.ToString()).ToString("N0", CultureInfo.InvariantCulture),
                    balance_out = Double.Parse(total_nowbalance_out.ToString()).ToString("N0", CultureInfo.InvariantCulture),
                    history = historyStruct,
                    history_out = historyStructOut
                });
            }

        }
        [HttpPost]
        public ActionResult saveAccountGoolge(string id,string client_id,string client_secret,string refresh_token)
        {
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            int configID = Int32.Parse(id.Trim());
            client_secret = client_secret.Trim();
            client_id = client_id.Trim();
            refresh_token = refresh_token.Trim();
            using (var db = new GamePaymentContext())
            {
                var config = db.Config.FirstOrDefault(e => e.id == configID);
                config.client_id = client_id;
                config.client_secret = client_secret;
                config.refresh_token = refresh_token;
                db.SaveChanges();
                return Json(new
                {
                    error = false,
                    msg = "Cập nhật thành công."
                });
            }
     
        }

        [HttpPost]
        public ActionResult GetListInputOut(string id,string start_time,string end_time)
        {
            if (id == null || start_time == null || end_time == null)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            DateTime dDate;

            if (!DateTime.TryParse(start_time, out dDate))
            {
                return Json(new
                {
                    error = true,
                    msg = "start_time không đúng định dạng"
                });
            }
            if (!DateTime.TryParse(end_time, out dDate))
            {
                return Json(new
                {
                    error = true,
                    msg = "end_time không đúng định dạng"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            int userID = Int32.Parse(id);

            ServiceBase serviceBase = new ServiceBase();
            SqlDataReader reader;

            string[] a_start_time_new = start_time.Split('/');
            string start_time_new = a_start_time_new[2] + "-" + a_start_time_new[0] + "-" + a_start_time_new[1] + " 00:00:00.000";

            string[] a_end_time_new = end_time.Split('/');
            string end_time_new = a_end_time_new[2] + "-" + a_end_time_new[0] + "-" + a_end_time_new[1] + " 23:59:59.999";
            DateTime[] dates = this.GetDatesBetween(DateTime.Parse(start_time_new), DateTime.Parse(end_time_new));
            if (dates.Length > 31)
            {
                return Json(new
                {
                    error = true,
                    msg = "Số ngày chọn quá lớn không thể thống kê."
                });
            }

            using (var db = new GamePaymentContext())
            {
                //Tìm giao dịch nạp rút trong khoảng thời gian
                var usersend_idParameter = new SqlParameter("@user_send", userID);
                var time_startParameter = new SqlParameter("@timestart", start_time_new);
                var time_endParameter = new SqlParameter("@timeend", end_time_new);

                var result = db.Database
                    .SqlQuery<Pro_statistics_user_inout>("Pro_statistics_user_inout @user_send , @timestart , @timeend", usersend_idParameter, time_startParameter, time_endParameter).ToList();

                //Tính nạp rút tất cả thời gian thống kê dựa trên kết quả nhận được
                Pro_statistics_user_inout[] list_date_query = new Pro_statistics_user_inout[dates.Length];
                for (int i = 0; i < list_date_query.Length; i++)
                {
                    bool check_is = false;

                    result.ForEach(ent =>
                    {
                        if (dates[i] == ent.date)
                        {
                            check_is = true;
                            Pro_statistics_user_inout pro_statistics = new Pro_statistics_user_inout();
                            pro_statistics.date = ent.date;
                            if (ent.balance_in != 0)
                            {
                                pro_statistics.balance_in = ent.balance_in;
                            }
                            else
                            {
                                if (i == 0)
                                {
                                    string queryStringBalance = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 0 AND type ='plus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                                    reader = serviceBase.query(queryStringBalance);
                                    double balance = 0;
                                    while (reader.Read())
                                    {
                                        balance = Double.Parse(reader["balance"].ToString());
                                    }
                                    reader.Close();
                                    DatabaseService.Instance.getConnection().Close();
                                    pro_statistics.balance_in = balance;
                                }
                                else
                                {
                                    pro_statistics.balance_in = list_date_query[i - 1].balance_in;
                                }

                            }
                            if (ent.balance_out != 0)
                            {
                                pro_statistics.balance_out = ent.balance_out;
                            }
                            else
                            {
                                if (i == 0)
                                {

                                    string queryStringBalanceOut = "SELECT TOP 1 balance FROM Transactions WHERE user_id = " + userID + " AND wallet_id = 0 AND transactions_type = 0 AND type ='minus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                                    reader = serviceBase.query(queryStringBalanceOut);
                                    double balance_out = 0;
                                    while (reader.Read())
                                    {
                                        balance_out = Double.Parse(reader["balance"].ToString());
                                    }
                                    reader.Close();
                                    DatabaseService.Instance.getConnection().Close();
                                    pro_statistics.balance_out = balance_out;
                                }
                                else
                                {
                                    pro_statistics.balance_out = list_date_query[i - 1].balance_out;
                                }

                            }
                            list_date_query[i] = pro_statistics;
                        }
                    });

                    if (!check_is)
                    {
                        if (i>0)
                        {
                            Pro_statistics_user_inout pro_statistics = new Pro_statistics_user_inout();
                            pro_statistics.date = dates[i];
                            pro_statistics.balance_in = list_date_query[i - 1].balance_in;
                            pro_statistics.balance_out = list_date_query[i - 1].balance_out;
                            list_date_query[i] = pro_statistics;

                        }
                        else
                        {
                            string queryStringBalance = "SELECT TOP 1 balance FROM Transactions WHERE user_id = '" + userID + "' AND wallet_id = 0 AND transactions_type = 0 AND type ='plus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                            reader = serviceBase.query(queryStringBalance);
                            double balance = 0;
                            while (reader.Read())
                            {
                                balance = Double.Parse(reader["balance"].ToString());
                            }
                            reader.Close();
                            DatabaseService.Instance.getConnection().Close();

                            string queryStringBalanceOut = "SELECT TOP 1 balance FROM Transactions WHERE user_id = '" + userID + "' AND wallet_id = 0 AND transactions_type = 0 AND type ='minus' AND time_creat <= '" + dates[i] + "' ORDER BY id DESC";
                            reader = serviceBase.query(queryStringBalanceOut);
                            double balance_out = 0;
                            while (reader.Read())
                            {
                                balance_out = Double.Parse(reader["balance"].ToString());
                            }
                            reader.Close();
                            DatabaseService.Instance.getConnection().Close();


                            Pro_statistics_user_inout pro_statistics = new Pro_statistics_user_inout();
                            pro_statistics.date = dates[i];
                            pro_statistics.balance_in = balance;
                            pro_statistics.balance_out = balance_out;
                            list_date_query[i] = pro_statistics;
                        }
                        
                    }
                }
                var list = list_date_query;
                var historyStruct = new[] { new {
                            date = "",
                            value = 0.1
                        }}.ToList();
                historyStruct.Clear();



                var historyStructOut = new[] { new {
                            date = "",
                            value = 0.1
                        }}.ToList();
                historyStructOut.Clear();
                

                //Lấy danh sách ngày chọn thống kê trả về 2 mảng
                for (int i = 0; i < list_date_query.Length; i++)
                {

                    historyStruct.Add(new
                    {
                        date = list_date_query[i].date.ToString("d/M"),
                        value = list_date_query[i].balance_in,
                    });
                    historyStructOut.Add(new
                    {
                        date = list_date_query[i].date.ToString("d/M"),
                        value = list_date_query[i].balance_out,
                    });
                }
                var total_nowbalance = historyStruct.Last().value - historyStruct.First().value;
                var total_nowbalance_out = historyStructOut.Last().value - historyStructOut.First().value;
                return Json(new
                {
                    error = false,
                    balance = Double.Parse(total_nowbalance.ToString()).ToString("N0", CultureInfo.InvariantCulture),
                    balance_out = Double.Parse(total_nowbalance_out.ToString()).ToString("N0", CultureInfo.InvariantCulture),
                    history = historyStruct,
                    history_out = historyStructOut
                });
            }

        }
        //Danh sách tiền trong ví của user
        [HttpPost]
        public ActionResult getListWallet(int id)
        {
            if (id == 0)
            {
                return Json(new
                {
                    error = true,
                    msg = "Tham số null"
                });
            }
            try
            {
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)

            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            using (var db = new GamePaymentContext())

            {
                ServiceBase serviceBase = new ServiceBase();
                SqlDataReader reader;
                var users = db.User.FirstOrDefault(u => u.id == id);

                string queryBalace = "SELECT Wallet.id,Wallet.name,ISNULL((SELECT TOP 1 balance as 'balance'  FROM Transactions WHERE user_id =@user_id AND wallet_id =Wallet.id AND transactions_type = 0 ORDER BY id DESC),0) as 'balance' " +
                    "FROM Wallet INNER JOIN Merchant_wallet ON Wallet.id = Merchant_wallet.wallet_id WHERE Merchant_wallet.merchant_id = @merchant_id";
                SqlCommand command;
                command = new SqlCommand(queryBalace, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@user_id", users.id);
                command.Parameters.AddWithValue("@merchant_id", users.merchant_id);
                reader = serviceBase.querySqlParam(command);
                List<string[]> a_items = new List<string[]>();
                while (reader.Read())
                {
                    string[] s_item = new string[4];
                    s_item[0] = reader["id"].ToString();
                    s_item[1] = reader["name"].ToString();
                    s_item[2] = Double.Parse(reader["balance"].ToString()).ToString("N", CultureInfo.InvariantCulture);
                    s_item[3] = "0";
                    a_items.Add(s_item);
                }
                reader.Close();
                DatabaseService.Instance.getConnection().Close();
                return Json(new
                {
                    error = false,
                    items = a_items
                });
            }
               
           
        }
    }
}