﻿using CorePayment.Base;
using ManageCorePayment.DAL;
using ManageCorePayment.Helper;
using ManageCorePayment.Models;
using ManageCorePayment.PrivateServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManageCorePayment.Controllers
{
    public class CoinController : Controller
    {
        // GET: Coin
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Listdata(String iSortCol_0, String sSortDir_0, String iDisplayStart, String iDisplayLength)
        {
            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            string[] a_sort = new string[] { "id", "name", "id"};
            ServiceBase serviceBase = new ServiceBase();
            string queryString = "SELECT" +
                " id" +
                " ,name" +
                " FROM Coin ";

            string queryCount = "SELECT COUNT(id) as 'count' FROM Coin";

            if (sSortDir_0.Equals("desc"))
            {
                sSortDir_0 = "DESC";
            }
            else
            {
                sSortDir_0 = "ASC";
            }
            queryString += " ORDER BY " + a_sort[Int32Ext.toInt(iSortCol_0)] + " " + sSortDir_0 + " OFFSET @iDisplayStart ROWS FETCH NEXT @iDisplayLength ROWS ONLY";
            // SqlDataReader reader = serviceBase.query(queryString);
            SqlCommand command;
            command = new SqlCommand(queryString, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@iDisplayStart", Int32Ext.toInt(iDisplayStart));
            command.Parameters.AddWithValue("@iDisplayLength", Int32Ext.toInt(iDisplayLength));
            SqlDataReader reader = serviceBase.querySqlParam(command);

            List<String[]> users = new List<String[]>();
            while (reader.Read())
            {
                string[] s_user = new string[3];
                s_user[0] = reader["id"].ToString();
                s_user[1] = reader["name"].ToString();
                s_user[2] = "<button onclick=\"coin.showEdit('" + reader["id"].ToString() + "')\" type=\"button\" class=\"btn btn-default btn-xs\">Sửa</button><button onclick=\"coin.showDelete('" + reader["id"].ToString() + "')\"  type=\"button\" class=\"btn btn-danger btn-xs\">Xóa</button>";
                users.Add(s_user);
            }
            reader.Close();
            DatabaseService.Instance.getConnection().Close();

            int row = 0;
            reader = serviceBase.query(queryCount);
            while (reader.Read())
            {
                row = Int32.Parse(reader["count"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();

            return Json(new
            {
                aaData = users,
                iTotalRecords = row,
                iTotalDisplayRecords = row,
                display = iDisplayLength
            });
        }
        [HttpPost]
        public ActionResult add(string title)
        {
            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            using (var db = new GamePaymentContext())
            {
                db.Coin.Add(new Coin()
                {
                    name = title
                });
                db.SaveChanges();
            }
            return Json(new
            {
                error = false
            });
        }

        private Boolean check_delete(int id)
        {
            using (var db = new GamePaymentContext())
            {
                var wallet = db.Wallet.FirstOrDefault(w => w.coin_id == id);
                if (wallet != null)
                {
                    return false;
                }
                return true;
            } 
        }


        [HttpPost]
        public ActionResult edit(string title,int id)
        {

            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            using (var db = new GamePaymentContext())
            {
                var coin = db.Coin.FirstOrDefault(c => c.id == id);
                if (coin == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Coin không tồn tại"
                    });

                }
                coin.name = title;
                db.SaveChanges();
            }
            return Json(new
            {
                error = false
            });
        }
        [HttpPost]
        public ActionResult delete(int id)
        {
            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            if (!check_delete(id))
            {
                return Json(new
                {
                    error = true,
                    msg = "Coin này đã tham chiếu với loại ví không thể xóa."
                });
            }
            using (var db = new GamePaymentContext())
            {
                var coin = db.Coin.FirstOrDefault(c => c.id == id);
                if (coin == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Coin không tồn tại"
                    });

                }
                db.Coin.Remove(coin);
            }
            return Json(new
            {
                error = false
            });


        }

        [HttpPost]
        public ActionResult detail(int id)
        {
            int userID = 0;
            try
            {
                userID = (new AuthenticationHelper(Session).ID());
                AuthMiddleware.checkRole((new Helper.AuthenticationHelper(Session)), Helper.SessionKeyMode.Admin);
            }
            catch (Exception)
            {
                return Json(new
                {
                    error = true,
                    msg = "Vui lòng đăng nhập lại"
                });
            }
            using (var db = new GamePaymentContext())
            {
                var coin = db.Coin.FirstOrDefault(c => c.id == id);
                if (coin == null)
                {
                    return Json(new
                    {
                        error = true,
                        msg = "Coin không tồn tại"
                    });
                    
                }
                return Json(new
                {
                    error = false,
                    item = coin
                });
            }

        }
    }
}