﻿define({
    data: {
        loading: true,
        toast: {
            error: false,
            msg: ""
        },
        items: [],
        form: {
            kw: "",
            filter:""
        },
        item_update: {
            id : 0
        }
    },

    // Methods
    methods: { 
        doViewOTP: function (id, status) {
            $.fancybox.showActivity();
            $.post('/AccountingAPI/sendSMS', {
                id: id,
                status: status
               
            }, function (json) {
                $.fancybox.hideActivity();
                // Show if error
                if (json.error) {
                    ns_master.toastr(json.msg, "error");
                } else {
                    $('#show-edit').modal("show");
                    item_update = { "id": id };
                    $('.phone-send').html(json.phone);
                }
            }, 'json');
        },
        doVerify: function (item, status) {
            if (item) {
                item_update = item;
            }
            $.fancybox.showActivity();
            $.post('/AccountingAPI/ResponseRequest', {
                id: item_update.id,
                status: status,
                code: $('.txt-otp').val()
            }, function (json) {
                $.fancybox.hideActivity();
                // Show if error
                if (json.error) {
                    ns_master.toastr(json.msg, "error");
                } else {
                    verify_payment.doUpdate();
                    $('#show-edit').modal("hide");
                    ns_master.toastr("Thực hiện thành công", "success");
                }
            }, 'json');
        },
        doVerifyNone: function (id, status) {
            $.fancybox.showActivity();
            $.post('/AccountingAPI/ResponseRequestNone', {
                id: id,
                status: status,
            }, function (json) {
                $.fancybox.hideActivity();
                // Show if error
                if (json.error) {
                    ns_master.toastr(json.msg, "error");
                } else {
                    verify_payment.doUpdate();
                    
                    $('#show-edit').modal("hide");
                    ns_master.toastr("Thực hiện thành công", "success");
                }
            }, 'json');
        },

        doUpdate: function (change) {
            var filter = $('#filter').val();
            var page_start = $('.paginate_button.current').html();
            //var lengh = $('select[name="datatable-labelManagement_length"]').val();
            var page = (page_start - 1) * 10;
            if (!page) {
                page = 0;
            }
            if (change) {
                page = 0;
            }
            $('#user-merchant').dataTable({
                dom: '<"top"i>rt<"bottom"flp><"clear">',
                tableTools: {
                    "aButtons": []
                },
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "bLengthChange": true,
                "bFilter": false,
                'displayStart': page,
                "bSort": true,
                "bInfo": false,
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    nRow.setAttribute('id', aData.RowOrder);

                },
                "fnServerParams": function (aoData) {
                    aoData.push(
                        {
                            "name": "filter", "value": filter
                        }
                    );
                },
                "sServerMethod": "POST",
                "sAjaxSource": "/AccountingAPI/ListRequest",
                "aaSorting": [[0, "desc"]]
            });
            return;
            comp = this;
            $.post('/AccountingAPI/ListRequest', {
                filter: comp.form.filter ? comp.form.filter : "all",
            }, function (json) {
                // Show if error
                if (json.error) {
                    ns_master.toastr(json.msg, "error");
                } else {
                    comp.items = json.items;
                }
            }, 'json');
        },
        clickChangeTag: function (id) {

            $.fancybox.showActivity();
            $.post('/AccountingAPI/changeTag', {
                "id": id
            }, function (json) {
                $.fancybox.hideActivity();
                // Show if error
                if (json.error) {
                    ns_master.toastr(json.msg, "error");
                } else {
                    ns_master.toastr("Thực hiện thành công", "success");
                    verify_payment.doUpdate();
                }
                }, 'json');
        },
        clickChangeTagRemove: function (id) {
            $.fancybox.showActivity();
            $.post('/AccountingAPI/clickChangeTagRemove', {
                "id": id
            }, function (json) {
                $.fancybox.hideActivity();
                // Show if error
                if (json.error) {
                    ns_master.toastr(json.msg, "error");
                } else {
                    ns_master.toastr("Thực hiện thành công", "success");
                    verify_payment.doUpdate();
                }
            }, 'json');

        }
    },

    // Autoload
    created: function () {
        this.doUpdate();
    }
});