﻿agency = {};
agency.agencyid = 0;
agency.showCreate = function (agencyid) {
    $('#create-rwithdrawuser').modal("show");
    agency.agencyid = agencyid;
};
agency.dataTable = function (change) {
    var page_start = $('.paginate_button.current').html();
    var page = (page_start - 1) * 10;
    if (!page) {
        page = 0;
    }
    if (change) {
        page = 0;
    }
    $('#user-agency').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "displayStart": page,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);
        },
        "fnServerParams": function (aoData) {
            aoData.push(
                
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/rwithdrawuserUserAPI/listdataAgencyy",
        "aaSorting": [[0, "desc"]]
    });
};

$(function () {
    agency.dataTable();
});

