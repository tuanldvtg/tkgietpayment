﻿define({

    data: {
        toast: {
            error: false,
            msg: ""
        },
        present: {
            title: "Tạo mới cổng",
            btn_title: "Tạo mới cổng"
        },
        items: [],
        form: {
            oldpass: "",
            password: "",
            repass:""
        },
        wallets: [],
        socials:[]
    },

    // Methods
    methods: {
        addSocial: function () {
            change_password.socials.push(
                {'id':0, 'title': '', 'link': '', 'view_user': false }
            );
        },
        removeSocial: function (i) {
            change_password.socials.splice(i, 1);
        },

        doUpdateSocial: function () {
            var items = [];
            change_password.socials.forEach(function (item) {
                var social = {};
                social['id'] = item['id'];
                social['agency_id'] = item['agency_id'];
                social['title'] = item['title'];
                social['link'] = item['link'];
                if (item['view_user']) {
                    social['view_user'] = 1;
                } else {
                    social['view_user'] = 0;
                }
                items.push(social);
            });
            $.post('/AgencyAccountAPI/doUpdateSocial', {
                'socials': items
            }, function (json) {
                // Show if error
                if (json.error) {
                    ns_master.toastr(json.msg, "error");
                } else {
                    ns_master.toastr("Cập nhật thành công.", "success");
                }
            }, 'json');
        },
        doUpdate: function () {
            if (!this.form.password) {
                this.toast = {
                    error: true,
                    msg: "Vui lòng  nhập mật khẩu"
                }
                return;
            }
            if (this.form.password != this.form.repass) {
                this.toast = {
                    error: true,
                    msg : "Mật khẩu nhập lại phải giống nhau"
                }
                return;
            }
            comp = this;
            $.post('/AgencyAccountAPI/ChangePassword', this.form, function (json) {
                // Show if error
                if (json.error) {
                    comp.toast = json;
                } else {
                    comp.toast = {
                        success: true,
                        msg: "Cập nhật thông tin thành công"
                    }
                }
            }, 'json');
            
        },
        
    },
    // Autoload
    created: function () {
        $.post('/AgencyAccountAPI/getListSocial', {}, function (json) {
            // Show if error
            if (json.error) {
                ns_master.toastr(json.msg, "error");
            } else {
                change_password.socials = json.items;
            }
        }, 'json');
    }
});