﻿manage_accounting = {};
manage_accounting.dataTable = function (change) {
    var page_start = $('.paginate_button.current > a').html();
    //var lengh = $('select[name="datatable-labelManagement_length"]').val();
    var page = (page_start - 1) * 10;
    if (!page) {
        page = 0;
    }
    if (change) {
        page = 0;
    }
    
    $('#datatable').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

        },
        "fnServerParams": function (aoData) {
            aoData.push(
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/ManageAccounting/Listdata",
        "aaSorting": [[0, "desc"]]
    });
}




manage_accounting.showDelete = function (id) {
    $('#modal-confirm').modal("show");
    $('#action-confirm').attr('onclick', 'manage_accounting.actionDelete(' + id + ')');
}
manage_accounting.actionDelete = function (id) {
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/ManageAccounting/delete",
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                manage_accounting.dataTable();
                $('#modal-confirm').modal("hide");
                ns_master.toastr("Thực hiện thành công", "success");
            }

        }
    });
}

manage_accounting.showAdd = function () {
    $('#show-edit').modal("show");
    $('#show-edit .modal-title').html("Tạo mới");
    $('#action-edit').attr('onclick', 'manage_accounting.add()');
}

manage_accounting.add = function () {
    var title = $('input[name="title"]').val();
    if (!title) {
        ns_master.toastr("Vui lòng điền tên", "error");
        $('input[name="title"]').focus();
        return;
    }
    var username = $('input[name="username"]').val();
    if (!username) {
        ns_master.toastr("Vui lòng điền tài khoản", "error");
        $('input[name="username"]').focus();
        return;
    }
    var password = $('input[name="password"]').val();
    if (!password) {
        ns_master.toastr("Vui lòng điền password", "error");
        $('input[name="password"]').focus();
        return;
    }
    var phone = $('input[name="phone"]').val();
    if (!phone) {
        ns_master.toastr("Vui lòng điền phone", "error");
        $('input[name="phone"]').focus();
        return;
    }
    var email = $('input[name="email"]').val();
    if (!email) {
        ns_master.toastr("Vui lòng điền phone", "error");
        $('input[name="email"]').focus();
        return;
    }
    var paymentgates = $('#paymentgate_id').val();
    if (!paymentgates) {
        ns_master.toastr("Vui lòng chọn cổng thanh toán", "error");
        return;
    }
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/ManageAccounting/add",
        dataType: "json",
        data: {
            title: title,
            username: username,
            password: password,
            phone: phone,
            email: email,
            paymentgate_id: $('#paymentgate_id').val()
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                manage_accounting.dataTable();
                $('#show-edit').modal("hide");
                ns_master.toastr("Thực hiện thành công", "success");
            }
            
           
        }
    });
}
manage_accounting.actionEdit = function (id) {
    var title = $('input[name="title"]').val();
    if (!title) {
        ns_master.toastr("Vui lòng điền tên", "error");
        $('input[name="title"]').focus();
        return;
    }
    var username = $('input[name="username"]').val();
    if (!username) {
        ns_master.toastr("Vui lòng điền tài khoản", "error");
        $('input[name="username"]').focus();
        return;
    }
    var password = $('input[name="password"]').val();

    var phone = $('input[name="phone"]').val();
    if (!phone) {
        ns_master.toastr("Vui lòng điền phone", "error");
        $('input[name="phone"]').focus();
        return;
    }
    var email = $('input[name="email"]').val();
    if (!email) {
        ns_master.toastr("Vui lòng điền phone", "error");
        $('input[name="email"]').focus();
        return;
    }
    var paymentgates = $('#paymentgate_id').val();
    if (!paymentgates) {
        ns_master.toastr("Vui lòng chọn cổng thanh toán", "error");
        return;
    }
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/ManageAccounting/edit",
        dataType: "json",
        data: {
            title: title,
            username: username,
            password: password,
            phone: manage_accounting.phone,
            email: manage_accounting.email,
            id: id,
            paymentgate_id: $('#paymentgate_id').val()
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                manage_accounting.dataTable();
                $('#show-edit').modal("hide");
                ns_master.toastr("Thực hiện thành công", "success");
            }     
        }
    });
}


manage_accounting.onchangEmail = function (e) {
    manage_accounting.email = $(e).val();
}

manage_accounting.onchangPhone = function (e) {
    manage_accounting.phone = $(e).val();
}


manage_accounting.email = "";
manage_accounting.phone = "";


manage_accounting.showEdit = function (id) {
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/ManageAccounting/detail",
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            $.fancybox.hideActivity();

            $('input[name="title"]').val(data.item.name);
            $('input[name="username"]').val(data.item.username);
            $('input[name="phone"]').val(data.item.phone);
            $('input[name="email"]').val(data.item.email);

            $('input[name="phone"]').val(data.item.phone_new);
            $('input[name="email"]').val(data.item.email_new);

            manage_accounting.email = data.item.email;
            manage_accounting.phone = data.item.phone;

            $('#show-edit').modal("show");
            $('#show-edit .modal-title').html("Sửa");
            $('#action-edit').attr('onclick', 'manage_accounting.actionEdit(' + id + ')');
            var a_th = [];
            data.items.forEach(function (item) {
                a_th.push(item.paymentgate_id);
            });
            console.log(a_th);
            $('#paymentgate_id').select2('val', a_th);
        }
    });

}


$(function () {
    manage_accounting.dataTable();
    $('#paymentgate_id').select2();
});