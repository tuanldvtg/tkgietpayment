﻿define({
    data: {
        loading: true,
        toast: {
            error: false,
            msg: ""
        },
        items: [],
        form: {
            kw: "",
            filter:""
        },
        item_update: {
            id : 0
        },
        agency_id: 0
    },

    // Methods
    methods: { 
        dataloadDetail: function (change) {
            var page_start = $('#datatable-history-detail_paginate .paginate_button.current').html();
            //var lengh = $('select[name="datatable-labelManagement_length"]').val();
            var page = (page_start - 1) * 10;
            if (!page) {
                page = 0;
            }
            if (change) {
                page = 0;
            }
            $('#datatable-history-detail').dataTable({
                dom: '<"top"i>rt<"bottom"flp><"clear">',
                tableTools: {
                    "aButtons": []
                },
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "bLengthChange": true,
                "bFilter": false,
                'displayStart': page,
                "bSort": true,
                "bInfo": false,
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    nRow.setAttribute('id', aData.RowOrder);

                },
                "fnServerParams": function (aoData) {
                    aoData.push(
                        {
                            "name": "agency_id", "value": debt_agency.agency_id
                        }
                    );
                },
                "sServerMethod": "POST",
                "sAjaxSource": "/AccountingDebt/ListDetail",
                "aaSorting": [[0, "desc"]]
            });
        },
        showDetail: function (agency_id, change) {
            $('#show-detail').modal("show");
            debt_agency.agency_id = agency_id;
            debt_agency.dataloadDetail(); 
        },
        clickChangeTagRemove: function (id) {
            if (!confirm("Xác nhận")) {
                return;
            }
            $.fancybox.showActivity();
            $.post('/AccountingAPI/clickChangeTagRemove', {
                "id": id
            }, function (json) {
                $.fancybox.hideActivity();
                // Show if error
                if (json.error) {
                    ns_master.toastr(json.msg, "error");
                } else {
                    ns_master.toastr("Thực hiện thành công", "success");
                    debt_agency.doUpdate();
                    debt_agency.dataloadDetail(); 
                }
            }, 'json');

        },
        doUpdate: function (change) {
            var filter = $('#agency_id').val();
            var page_start = $('.pagination .paginate_button.current > a').html();
            //var lengh = $('select[name="datatable-labelManagement_length"]').val();
            var page = (page_start - 1) * 10;
            if (!page) {
                page = 0;
            }
            if (change) {
                page = 0;
            }
            $('#user-merchant').dataTable({
                dom: '<"top"i>rt<"bottom"flp><"clear">',
                tableTools: {
                    "aButtons": []
                },
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "bLengthChange": true,
                "bFilter": false,
                'displayStart': page,
                "bSort": true,
                "bInfo": false,
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    nRow.setAttribute('id', aData.RowOrder);

                },
                "fnServerParams": function (aoData) {
                    aoData.push(
                        {
                            "name": "agency_id", "value": filter
                        }
                    );
                },
                "sServerMethod": "POST",
                "sAjaxSource": "/AccountingDebt/ListRequest",
                "aaSorting": [[0, "desc"]]
            });
            return;
            comp = this;
            $.post('/AccountingDebt/ListRequest', {
                filter: comp.form.filter ? comp.form.filter : "all",
            }, function (json) {
                // Show if error
                if (json.error) {
                    ns_master.toastr(json.msg, "error");
                } else {
                    comp.items = json.items;
                }
            }, 'json');
        },
    },

    // Autoload
    created: function () {
        this.doUpdate();
    }
});