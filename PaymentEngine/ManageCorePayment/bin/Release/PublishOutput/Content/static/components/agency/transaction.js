﻿define({
    data: {
        userid:0,
        loading: false,
        toast: {
            error: false,
            msg: ""
        },
        items: [],
        form: {
            kw: "",
            txtname: "",
            gate: 0,
            cash:0
        },
        merchants: [],
        wallets: [],
    },

    // Methods
    methods: {
        doFindUsers: function (change) {
            var page_start = $('.pagination .paginate_button.current > a').html();
            //var lengh = $('select[name="datatable-labelManagement_length"]').val();
            var page = (page_start - 1) * 10;
            if (!page) {
                page = 0;
            }
            if (change) {
                page = 0;
            }
            console.log("$('#form-merchant').val(): " + $('#form-merchant').val());
            $('#user-merchant').dataTable({
                dom: '<"top"i>rt<"bottom"flp><"clear">',
                tableTools: {
                    "aButtons": []
                },
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "bLengthChange": true,
                "bFilter": false,
                'displayStart': page,
                "bSort": true,
                "bInfo": false,
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    nRow.setAttribute('id', aData.RowOrder);

                },
                "fnServerParams": function (aoData) {
                    aoData.push(
                        {
                            "name": "merchant_id", "value": $('#form-merchant').val()
                        },
                        { "name": "s_seach", "value": $('#form-txtname').val() }
                    );
                },
                "sServerMethod": "POST",
                "sAjaxSource": "/AgencyCreatePayAPI/SearchUsers",
                "aaSorting": [[0, "desc"]]
            });

            //this.loading = true;
            // Show all agencys
            //$.fancybox.showActivity();
            //$.post('/AgencyCreatePayAPI/SearchUsers', 
            //    {
            //        kw: this.form.txtname,
            //        merchantid: this.form.merchant
            //    }, function (json) {
            //        $.fancybox.hideActivity();
            //        //this.loading = false;
            //        // Show if error
            //        if (json.error) {
            //            transaction.toast = json;
            //        } else {
            //            transaction.items = json.items;
            //        }
            //}, 'json');
        },

        showCreateTransaction: function (userid,name) {
            var e = this;
            $.post('/AgencyCreatePayAPI/GetPaymentGates',
                {
                    merchantid: this.form.merchant
                }, function (json) {
                    $('#vueAppCreateTransaction').modal('show');
                    create_transaction.wallets = json.items;
                    create_transaction.userid = userid;
                    create_transaction.selected_user = [{ id: userid, name: name }];
                    create_transaction.cashout = 0;
                    create_transaction.total = 0;
                    create_transaction.form.cash = 0;

                    create_transaction.onSelectGate();

                }, 'json');

        }
    },

    // Autoload
    created: function () {
        // Show all agencys
        $.post('/AgencyPaymentGateAPI/Merchants', this.form, function (json) {
            // Show if error
            if (json.error) {
                transaction.toast = json;
            } else {
                transaction.merchants = json.items;
                if (json.items.length == 0) {
                    ns_master.toastr("Tài khoản chưa được gán dùng merchant nào", "success");
                    return;
                }
                if (json.items) {
                    transaction.form.merchant = json.items[0].id;
                    setTimeout(function () {
                       // transaction.doFindUsers();
                    }, 500);
                }
            }
        }, 'json');
    }
});

