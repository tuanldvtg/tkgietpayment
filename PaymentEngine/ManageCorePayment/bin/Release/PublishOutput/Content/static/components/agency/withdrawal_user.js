﻿withdrawal_user = {};
withdrawal_user.showCreate = function () {
    $('#create-rwithdrawuser').modal("show");
};

withdrawal_user.dataTable = function (change) {
    var page_start = $('.paginate_button.current').html();
    //var lengh = $('select[name="datatable-labelManagement_length"]').val();
    var page = (page_start - 1) * 10;
    if (!page) {
        page = 0;
    }
    if (change) {
        page = 0;
    }
    var start_time = $('input[name="daterangepicker_start"]').val();
    var end_time = $('input[name="daterangepicker_end"]').val();
    $('#user-merchant').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "displayStart": page,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

        },
        "fnServerParams": function (aoData) { 
            aoData.push(
                { "name": "filter", "value": $('#filter').val() },
                { "name": "search_note", "value": $('#search_note').val() }
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/RwithdrawuserAPI/listdata",
        "aaSorting": [[0, "desc"]]
    });
};

$(function () {
    withdrawal_user.dataTable();
    //$('#user_id').select2({
    //    dropdownParent: $(this).parent(),
    //    ajax: {
    //        url: '/Agency/ListUserSelect2',
    //        data: function (params) {
    //            var query = {
    //                search: params.term
    //            }
    //            // Query parameters will be ?search=[term]&type=public
    //            return query;
    //        }
             
    //    }
    //});
});

