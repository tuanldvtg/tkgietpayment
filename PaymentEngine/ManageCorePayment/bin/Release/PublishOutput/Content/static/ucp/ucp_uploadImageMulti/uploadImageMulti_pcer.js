/* 
 * UPLOAD nhiều ảnh sản phẩm
 */

uploadImageMulti_Producer = {};

uploadImageMulti_Producer.productid = "";
uploadImageMulti_Producer.proid = "";
uploadImageMulti_Producer.product_image_color = 0;
uploadImageMulti_Producer.show = function (pid) {
    uploadImageMulti_Producer.productid = pid;
//    Load ảnh theo sản phẩm nếu có
    $.post('/', {
        ajax: 'uploadImageMulti_Producer/services',
        action: 'showInfo',
        id: uploadImageMulti_Producer.productid
    }, function (json) {
        if (json.error) {
            alert(json.msg);
        } else {
            $('#modal-uploadImageMulti_Producer .modal-title').html(json.title);
        }
    }, 'json');
    $('#modal-uploadImageMulti_Producer .input-icon').removeClass('state-success');
    $('#modal-uploadImageMulti_Producer .input-icon').removeClass('state-error');
    $('#modal-uploadImageMulti_Producer').modal('show');
    $('#modal-uploadImageMulti_Producer .invalid').html('');
    $('.note.note-danger').hide();
    uploadImageMulti_Producer.init();
    uploadImageMulti_Producer.upimgMulti();

};
/*
 * Upload ảnh
 * @returns {undefined}
 */


uploadImageMulti_Producer.upimgMulti = function (id) {

    var ul = $('#upload ul');
    $('#drop a').click(function () {
        $(this).parent().find('input').click();
    });
    $('#upload').fileupload({
        formData: {
            'ajax': 'uploadImageMulti_Producer/services',
            'action': 'product_img_multi',
            'productid': uploadImageMulti_Producer.productid,
            'product_modelid': uploadImageMulti_Producer.product_image_color
        },
        url: '/',
        dataType: 'json',
        done: function (e, data) {
//            console.log(data);
            if (data.result.error) {
                ns_master.toastr('Có lỗi xảy ra');
            } else {
//                data.context.addClass('success');


            }
//            $('#upload img[namefile = "' + data.result.basename + '"]').attr('src', data.result.display_image);
//            $('#upload img[namefile = "' + data.result.basename + '"]').parent().find('span').attr('photoid', data.result.product_photos_id);
//          
            uploadImageMulti_Producer.getImage(id);
        },
        dropZone: $('#drop'),
//        add: function (e, data) {
//
//            var tpl = $('<li class="working">' +
//                    '<input type="text" value="0" data-width="48" data-height="48"' +
//                    'data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" />' +
//                    '<img width="48" class="" height="48" src="">' +
//                    '<p></p>' +
//                    '<span></span></li>');
//            tpl.find('p').text(data.files[0].name)
//                    .append('<i>' + formatFileSize(data.files[0].size) + '</i>');
//            tpl.find('img').attr('namefile', data.files[0].name);
////            tpl.find('span').attr('photoid', data.jqXHR.responseJSON.product_photos_id);
//            data.context = tpl.appendTo(ul);
//            tpl.find('input').knob();
//            tpl.find('span').click(function () {
//                if (tpl.hasClass('working')) {
//                    jqXHR.abort();
////                    $.post('/', {
////                        ajax: 'uploadImageMulti_Producer/services',
////                        action: 'dropImage',
////                        product_photos_id: $(this).find('span').attr('photoid')
////                    }, function (json) {
////
////                    }, 'json');
//                }
//                tpl.fadeOut(function () {
//                    tpl.remove();
//                });
//            });
//            var jqXHR = data.submit();
//        },
//        progress: function (e, data) {
//            var progress = parseInt(data.loaded / data.total * 100, 10);
//            data.context.find('input').val(progress).change();
//            if (progress == 100) {
//                data.context.removeClass('working');
//                data.context.find('canvas').remove();
//            }
//        }
    });
    $(document).on('drop dragover', function (e) {
//        $.post('/', {
//            ajax: 'uploadImageMulti_Producer/services',
//            action: 'dropImage',
//            id: uploadImageMulti_Producer.productid
//        }, function (json) {
//            if (json.error) {
//                alert(json.msg);
//            } else {
//                $('#modal-uploadImageMulti_Producer .modal-title').html(json.title);
//            }
//        }, 'json');
        e.preventDefault();
    });
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }
        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }
        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }
        return (bytes / 1000).toFixed(2) + ' KB';
    }


};

// =============================== CÁC CHỨC NĂNG MẶC ĐỊNH ===============//
/*
 * Hiển thị modal hỏi có xác nhận thực hiện chạy tiếp
 * $param :
 */
uploadImageMulti_Producer.confirm = function (id) {
    $('#modal-confirm').modal('show');
    $('#modal-confirm .confirm-title-config').html('Xóa màu sẽ xóa ảnh sản phẩm tương ứng. Bạn có chắc chắn muốn thực hiện thao tác này ?');
    $('#modal-confirm .btn-primary').attr('onclick', 'uploadImageMulti_Producer.removeImageByCategory(' + id + ')');
};
/*
 * Hiển thị modal hỏi có xác nhận thực hiện chạy tiếp
 * $param :
 */
uploadImageMulti_Producer.confirmImage = function (id) {
    $('#modal-confirm').modal('show');
    $('#modal-confirm .btn-primary').attr('onclick', 'uploadImageMulti_Producer.removeImage(' + id + ')');
};
// ================================ CÁC CHỨC NĂNG THÊM ====================== //

uploadImageMulti_Producer.init = function () {

    // Quản lý màu sản phẩm : display, remove, edit, add
//    uploadImageMulti_Producer.getImage();
    $('#upload ul').html('');
};
/*
 * Lấy danh sách ảnh sản phẩm khi chọn màu
 * @param {type} elementid
 * @returns {undefined}
 */
uploadImageMulti_Producer.getImage = function (id) {

    element = $('.image-db');
    $.post('/', {
        ajax: 'uploadImageMulti_Producer/services',
        action: 'getImage',
        id: id,
    }, function (json) {
        html = '';
        if (!json.error) {
            $.each(json.data, function (i, e) {
                html += '<div class="col-lg-3 col-sm-4 col-xs-6">';
                html += '<a title="Image 1">';
                html += '<img class="thumbnail img-responsive" style="width:auto;height:130px" src="' + e.cover + '">';
                html += '</a>';
                if (e.title === '' || e.title === null) {
                    html += '<a style="cursor: pointer" class="editsubj-table-adt" data-id="' + e.id + '" onclick="uploadImageMulti_Producer.changetitle(this)">Click để thêm tiêu đề</a>';

                } else {
                    html += '<a style="cursor: pointer" class="editsubj-table-adt" data-id="' + e.id + '" onclick="uploadImageMulti_Producer.changetitle(this)">' + e.title + '</a>';

                }
                html += '<a class="removeImg" data-id="' + e.id + '" onclick="uploadImageMulti_Producer.removeImage(this)"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
//                html += '<input name="title" data-id="' + e.id + '" id="title" type="text" placeholder="Tiêu đề ảnh" class="form-control input-sm">';
                html += '</div>';
            });
            element.html(html);
        } else {
            html += '<div></div>';
            element.html(html);
        }
    }, 'json');
};

/*
 * Xóa màu sản phẩm
 * @param {type} product_model_id
 * @returns {json}
 */
uploadImageMulti_Producer.removeImageByCategory = function (id) {
    $.post('/', {
        ajax: 'product_model/services',
        action: 'removeCategoryColor',
        product_model_id: id,
    }, function (json) {
        if (!json.error) {
            ns_master.toastr(json.msg, 'success');
            uploadImageMulti_Producer.reloadtableColorProduct();
        } else {
            ns_master.toastr(json.msg, 'error');
        }
    }, 'json');
};

/*
 * Xóa ảnh sản phẩm
 * @param {type} product_model_id
 * @returns {json}
 */
uploadImageMulti_Producer.removeImage = function (e) {
    $.post('/', {
        ajax: 'uploadImageMulti_Producer/services',
        action: 'dropImage',
        product_photos_id: $(e).attr('data-id'),
    }, function (json) {
        if (!json.error) {
            ns_master.toastr(json.msg, 'success');
            $(e).parent().remove();
        } else {
            ns_master.toastr(json.msg, 'error');
        }
    }, 'json');
};

/*
 * Hiển thị ảnh theo danh mục ablum
 * @param {type} product_model_id
 * @returns {undefined}
 */
uploadImageMulti_Producer.showImageByCategory = function (e) {
    $('.product-image-color li').removeClass('active');
    $('.product-image-color li span').removeClass('label-success');
    $(e).parent().addClass('active');
    $(e).parent().find('span').addClass('label-success');
};


uploadImageMulti_Producer.imgpreview = function () {
    $('a.preview').fancybox({
        transitionIn: 'fade',
        onComplete: function () {
            FB.Canvas.getPageInfo(function (pageInfo) {
                //alert(pageInfo.scrollTop).
                $("#fancybox-wrap").css({top: '50px'});
                $("#fancybox-wrap").css({
                    'top': '50px',
                    'bottom': 'auto'
                });
            });
        }
    });
    return;
    /* CONFIG */
    xOffset = 10;
    yOffset = 30;
    // these 2 variable determine popup's distance from the cursor
    // you might want to adjust to get the right result

    /* END CONFIG */
    $("a.preview").hover(function (e) {
        this.t = this.title;
        this.title = "";
        var c = (this.t != "") ? "<br/>" + this.t : "";
        $("body").append("<p id='preview'><img src='" + $(this).attr('ihref') + "' alt='Image preview' />" + c + "</p>");
        $("#preview")
                .css("top", (e.pageY - xOffset) + "px")
                .css("left", (e.pageX + yOffset) + "px")
                .fadeIn("fast");
    },
            function () {
                this.title = this.t;
                $("#preview").remove();
            });
    $("a.preview").mousemove(function (e) {
        $("#preview")
                .css("top", (e.pageY - xOffset) + "px")
                .css("left", (e.pageX + yOffset) + "px");
    });
}

uploadImageMulti_Producer.changetitle = function (e) {
    var element = $(e).parent();
    var id = $(e).attr('data-id');
    var valuee = $(e).html();
    $(e).remove();
    element.append('<input value="' + valuee + '" onkeyup="uploadImageMulti_Producer.doEditEnter(event,this)"  data-id="' + id + '"  class="form-control update-edit" type="text" />');
    element.find('input').focus();
    $(document).click(function (event) {
        var target = $(event.target);
        $('.update-edit').each(function () {
            if (target.hasClass('editsubj-table-adt') || target.hasClass('update-edit')) {
                return;
            }
            var ele = $(this);
            uploadImageMulti_Producer.doEdit(ele);

        });
    });
};
uploadImageMulti_Producer.doEditEnter = function (event, e) {
    if (event.keyCode == 13) {
        element = $(e);
        uploadImageMulti_Producer.doEdit(element);
    }
}
uploadImageMulti_Producer.doEdit = function (element) {
    var val = element.val();
    var id = element.attr('data-id');
    if (val === '') {
        ns_master.toastr('Vui lòng điền tiêu đề ảnh', 'error');
        return;
    }
    $.post('/', {
        'ajax': 'uploadImageMulti_Producer/services',
        'action': 'doEditTitle',
        'title': val,
        'id': element.attr('data-id'),
    }, function (json) {
        if (json.error) {
            ns_master.toastr(json.msg, 'success');
            element.parent().append('<a class="editsubj-table-adt" onclick="uploadImageMulti_Producer.changetitle(this)" data-id="' + id + '">' + val + '</a>');
            element.remove();
        }
    }, 'json');
}


/**
 * Xử lý upload nhiều ảnh sản phẩm
 * @param {type} pid
 * @returns {undefined}
 */
uploadImageMulti_Producer.showImageAlbum = function (pid) {
    uploadImageMulti_Producer.proid = pid;
    $.post('/', {
        'ajax': 'uploadImageMulti_Producer/services',
        'action': 'showtitlePro',
        'id': pid,
    }, function (json) {
        $('#modal-uploadImageMulti_Producer .modal-title').html('Quản lý Album hình ảnh của sản phẩm <b>' + json.title + '</b>');

    }, 'json');
    $('#modal-uploadImageMulti_Producer .input-icon').removeClass('state-success');
    $('#modal-uploadImageMulti_Producer .input-icon').removeClass('state-error');
    $('#modal-uploadImageMulti_Producer').modal('show');
    $('#modal-uploadImageMulti_Producer .invalid').html('');
    $('.note.note-danger').hide();
    uploadImageMulti_Producer.getImageAlbum();
    $('#upload ul').html('');
    uploadImageMulti_Producer.upimgMultiAlbum();

};

uploadImageMulti_Producer.getImageAlbum = function () {
    element = $('.image-db');
    $.post('/', {
        ajax: 'uploadImageMulti_Producer/services',
        action: 'getImageAlbum',
        id: uploadImageMulti_Producer.proid,
    }, function (json) {
        html = '';
        if (!json.error) {
            $.each(json.data, function (i, e) {
                html += '<div class="col-lg-3 col-sm-4 col-xs-6">';
                html += '<a title="Image 1">';
                html += '<img class="thumbnail img-responsive" style="width:300px;height:130px" src="' + e.cover + '">';
                html += '</a>';
//                if (e.title === '' || e.title === null) {
//                    html += '<a style="cursor: pointer" class="editsubj-table-adt" data-id="' + e.id + '" onclick="uploadImageMulti_Producer.changetitle(this)">Click để thêm tiêu đề</a>';
//
//                } else {
//                    html += '<a style="cursor: pointer" class="editsubj-table-adt" data-id="' + e.id + '" onclick="uploadImageMulti_Producer.changetitle(this)">' + e.title + '</a>';
//
//                }
                html += '<a class="removeImg" data-id="' + e.id + '" onclick="uploadImageMulti_Producer.removeImageAlbum(this)">X</a>';
//                html += '<input name="title" data-id="' + e.id + '" id="title" type="text" placeholder="Tiêu đề ảnh" class="form-control input-sm">';
                html += '</div>';
            });
            element.html(html);
        } else {
            html += '<div></div>';
            element.html(html);
        }
    }, 'json');
};

uploadImageMulti_Producer.upimgMultiAlbum = function () {
    NProgress.start();
    var ul = $('#upload ul');
    $('#drop a').click(function () {
        $(this).parent().find('input').click();
    });
    $('#upload').fileupload({
        formData: {
            'ajax': 'uploadImageMulti_Producer/services',
            'action': 'product_img_multi_album',
            'productid': uploadImageMulti_Producer.proid,
        },
        url: '/',
        dataType: 'json',
        done: function (e, data) {
//            console.log(data);
            if (data.result.error) {
                data.context.addClass('error');
            } else {
                data.context.addClass('success');
            }
            $('#upload img[namefile = "' + data.result.basename + '"]').attr('src', data.result.display_image);
            $('#upload img[namefile = "' + data.result.basename + '"]').parent().find('span').attr('photoid', data.result.product_photos_id);
            uploadImageMulti_Producer.getImageAlbum();
        },
        dropZone: $('#drop'),
        add: function (e, data) {

            var tpl = $('<li class="working">' +
                    '<input type="text" value="0" data-width="48" data-height="48"' +
                    'data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" />' +
                    '<img width="48" class="" height="48" src="">' +
                    '<p></p>' +
                    '<span></span></li>');
            tpl.find('p').text(data.files[0].name)
                    .append('<i>' + formatFileSize(data.files[0].size) + '</i>');
            tpl.find('img').attr('namefile', data.files[0].name);
//            tpl.find('span').attr('photoid', data.jqXHR.responseJSON.product_photos_id);
            data.context = tpl.appendTo(ul);
            tpl.find('input').knob();
            tpl.find('span').click(function () {
                if (tpl.hasClass('working')) {
                    jqXHR.abort();
//                    $.post('/', {
//                        ajax: 'uploadImageMulti_Producer/services',
//                        action: 'dropImage',
//                        product_photos_id: $(this).find('span').attr('photoid')
//                    }, function (json) {
//
//                    }, 'json');
                }
                tpl.fadeOut(function () {
                    tpl.remove();
                });
            });
            var jqXHR = data.submit();
        },
        progress: function (e, data) {
            NProgress.done();
            var progress = parseInt(data.loaded / data.total * 100, 10);
            data.context.find('input').val(progress).change();
            if (progress == 100) {
                data.context.removeClass('working');
                data.context.find('canvas').remove();
            }
        }

    });
    NProgress.done();
    $(document).on('drop dragover', function (e) {
//        $.post('/', {
//            ajax: 'uploadImageMulti_Producer/services',
//            action: 'dropImage',
//            id: uploadImageMulti_Producer.productid
//        }, function (json) {
//            if (json.error) {
//                alert(json.msg);
//            } else {
//                $('#modal-uploadImageMulti_Producer .modal-title').html(json.title);
//            }
//        }, 'json');
        e.preventDefault();
    });
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }
        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }
        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }
        return (bytes / 1000).toFixed(2) + ' KB';
    }





};
/**
 * Xử lý upload nhiều link video sản phẩm
 * @param {type} pid
 * @returns {undefined}
 */
uploadImageMulti_Producer.provid = "";
uploadImageMulti_Producer.showVideoAlbum = function (pid) {

    $.post('/', {
        'ajax': 'uploadImageMulti_Producer/services',
        'action': 'showtitlePro',
        'id': pid,
    }, function (json) {
        $('#modal-uploadVideo .modal-title').html('Quản lý video của sản phẩm <b>' + json.title + '</b>');

    }, 'json');

    uploadImageMulti_Producer.provid = pid;
    $('#modal-uploadVideo').modal('show');
    uploadImageMulti_Producer.loaduploadVideo(pid);


};

uploadImageMulti_Producer.loaduploadVideo = function (provid) {
    $('#datatable-uploadVideo').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []},
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": false,
        "bInfo": false,
        "language": {
            //         "url": 'static/vendors/DataTables/Vietnamese.json',
            //"info": "Hiển thị trang _PAGE_ trên tổng số _PAGES_",
            "lengthMenu": 'Xem <select>' +
                    '<option value="10">10</option>' +
                    '<option value="20">20</option>' +
                    '<option value="30">30</option>' +
                    '<option value="40">40</option>' +
                    '<option value="50">50</option>' +
                    '<option value="100">100</option>' +
                    '<option value="200">200</option>' +
                    '</select> kết quả',
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

            // Apply iCheck to checkbox
            $('td input[type="checkbox"]', nRow).iCheck({
                checkboxClass: 'icheckbox_minimal-grey',
                radioClass: 'iradio_minimal-grey',
                increaseArea: '20%' // optional
            });//Initialize row id for every row

        },
        "fnServerParams": function (aoData) {
            aoData.push(
                    {"name": "ajax", "value": 'uploadImageMulti_Producer/services'},
                    {"name": "action", "value": 'list_data'},
                    {"name": "proid", "value": provid}
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/",
        "aaSorting": [[0, "desc"]]
    });
    $('.checkall').on('ifChecked', function (event) {
        $('.call-checkbox').iCheck('check');
    });
    $('.checkall').on('ifUnchecked', function (event) {
        $('.call-checkbox').iCheck('uncheck');
    });


    $('.checkall').on('ifChanged', function (event) {
        if (!this.changed) {
            this.changed = true;
            $('.checkall').iCheck('check');
        } else {
            this.changed = false;
            $('.checkall').iCheck('uncheck');
        }
        $('.checkall').iCheck('update');
    });

    $('#datatable-uploadVideo_length select').select2({
        minimumResultsForSearch: Infinity
    });
};

uploadImageMulti_Producer.addVideoAlbum = function () {


    var link = $('input#inputLink').val();
    if (link === '' || link === null) {
        ns_master.toastr('Link video không để trống', 'error');

        return;
    }
    $.post('/', {
        'ajax': 'uploadImageMulti_Producer/services',
        'action': 'addVideoAlbum',
        'link': link,
        'proid': uploadImageMulti_Producer.provid
    }, function (json) {
        if (json.error) {
            ns_master.toastr('Thêm link video thành công', 'success');
            $('input#inputLink').val('');
            uploadImageMulti_Producer.loaduploadVideo(uploadImageMulti_Producer.provid);
        } else {
            alert(json.msg);
        }
    }, 'json');
    return false;

}
uploadImageMulti_Producer.showeditVideoAlbum = function (id) {

    $.post('/', {
        'ajax': 'uploadImageMulti_Producer/services',
        'action': 'showeditVideoAlbum',
        'id': id,
    }, function (json) {
        $('#modal-uploadVideo input#inputLink').val(json.title);
        $('#btn-submit-link').html('<span class="glyphicon glyphicon-pencil"></span> Sửa link');
        $('#modal-uploadVideo .btn-info').attr('onclick', 'uploadImageMulti_Producer.editVideoAlbum(' + json.id + ')');


    }, 'json');

    $('input#inputLink').val('');

}
uploadImageMulti_Producer.editVideoAlbum = function (id) {
    var link = $('input#inputLink').val();
    if (link === '' || link === null) {
        ns_master.toastr('Link video không để trống', 'error');

        return;
    }
    $.post('/', {
        'ajax': 'uploadImageMulti_Producer/services',
        'action': 'editVideoAlbum',
        'link': link,
        'proid': id
    }, function (json) {
        if (json.error) {
            ns_master.toastr('Thêm link video thành công', 'success');
            $('input#inputLink').val('');
            $('#btn-submit-link').html('<span class="glyphicon glyphicon-ok"></span> Thêm mới');
            $('#modal-uploadVideo .btn-info').attr('onclick', 'uploadImageMulti_Producer.addVideoAlbum()');
            uploadImageMulti_Producer.loaduploadVideo(uploadImageMulti_Producer.provid);


        } else {
            alert(json.msg);
        }
    }, 'json');
    return false;

}
uploadImageMulti_Producer.doRemoveVideo = function (id) {
    $.post('/', {
        'ajax': 'uploadImageMulti_Producer/services',
        'action': 'doRemoveVideo',
        'ev_id': id

    }, function (json) {

        if (!json.error) {
            ns_master.toastr('Thực hiện thành công', 'success');
            uploadImageMulti_Producer.loaduploadVideo(uploadImageMulti_Producer.provid);
        } else {
            ns_master.toastr(json.msg);

            //alert(json.msg);
        }

    }, 'json');
    return false;
};

uploadImageMulti_Producer.removeImageAlbum = function (e) {
    $.post('/', {
        ajax: 'uploadImageMulti_Producer/services',
        action: 'dropImageAlbum',
        product_photos_id: $(e).attr('data-id'),
    }, function (json) {
        if (!json.error) {
            ns_master.toastr(json.msg, 'success');
            $(e).parent().remove();
        } else {
            ns_master.toastr(json.msg, 'error');
        }
    }, 'json');
};