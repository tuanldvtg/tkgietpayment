﻿merchant = {};
merchant.dataTable = function (change) {
    var page_start = $('.pagination .paginate_button.current > a').html();
    //var lengh = $('select[name="datatable-labelManagement_length"]').val();
    var page = (page_start - 1) * 10;
    if (!page) {
        page = 0;
    }
    if (change) {
        page = 0;
    }

    $('#datatable').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

        },
        "fnServerParams": function (aoData) {
            aoData.push(
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/Merchant/Listdata",
        "aaSorting": [[0, "desc"]]
    });
}

merchant.viewAddPaymentGate = function () {
    $('#modal-add-payment').modal('show');
    $('#list-type').modal('hide');
        $.fancybox.showActivity();
        $.ajax({
            type: 'POST',
            url: "/Merchant/loadWallet",
            dataType: "json",
            data: {
                id: merchant.idClick
            },
            success: function (data) {
                $.fancybox.hideActivity();
                if (data.error) {
                    ns_master.toastr(data.msg, "error");
                } else {
                    var html = '';
                    data.items_object.forEach(function (item) {
                        html += '<option value="'+item[0]+'">' + item[1] + '</option>';
                    });
                    $('#wallet_in').html(html);
                    $('#wallet_out').html(html);
                    var html = '';
                    data.list_paymentGate.forEach(function (item) {
                        html += '<option value="' + item['id'] + '">' + item['name'] + '</option>';
                    });
                    $('#paymentgate_id').html(html);
                    merchant.loadWalletPaymentGate();
                }

            }
        });
}

merchant.actionEdit = function (id) {
    var title = $('input[name="title"]').val();
    if (!title) {
        ns_master.toastr("Vui lòng điền tên mertchant", "error");
        $('input[name="title"]').focus();
        return;
    }
    var name = $('input[name="name"]').val();
    if (!name) {
        ns_master.toastr("Vui lòng điền mã mertchant", "error");
        $('input[name="name"]').focus();
        return;
    }
    var secret_key = $('input[name="secret_key"]').val();
    if (!secret_key) {
        ns_master.toastr("Vui lòng điền mã secret key", "error");
        $('input[name="secret_key"]').focus();
        return;
    }
    var username = $('input[name="username"]').val();
    if (!username) {
        ns_master.toastr("Vui lòng điền username ", "error");
        $('input[name="username"]').focus();
        return;
    }
    var phone = $('input[name="phone"]').val();
    if (!phone) {
        ns_master.toastr("Vui lòng điền phone", "error");
        $('input[name="phone"]').focus();
        return;
    }

    var password = $('input[name="password"]').val();

    var email = $('input[name="email"]').val();
    if (!email) {
        ns_master.toastr("Vui lòng điền email", "error");
        $('input[name="email"]').focus();
        return;
    }
    var a_wallet_id = $('#wallet_id').select2('val');
    var a_wallet_id = $('#wallet_id').select2('val');
    if (!a_wallet_id) {
        ns_master.toastr("Vui lòng chọn loại ví cho merchant", "error");
        return;
    }
    var s_wallet_id = a_wallet_id.join(',');
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Merchant/edit",
        dataType: "json",
        data: {
            title: title,
            name: name,
            username: username,
            secret_key: secret_key,
            phone: phone,
            password: password,
            email: email,
            id: id,
            wallet_ids: s_wallet_id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                merchant.dataTable();
                $('#show-edit').modal("hide");
                ns_master.toastr("Thực hiện thành công", "success");
            }

        }
    });
}

merchant.showEditActive = function (id, e) {
    $('#edit-active').modal("show");
    var avtice = $(e).attr('data-active');
    $('#edit-active type[type="radio"]').prop('checked', false);
    $('#' + avtice).prop('checked', true);
    $('#edit-active #action-confirm').attr("onclick", "merchant.actionEditActive(" + id + ")");
    $('#edit-active .modal-title').html("Sửa");
}

merchant.actionEditActive = function (id) {
    var status = "active";
    if ($('#active').is(":checked")) {
        status = "active";
    } else {
        status = "deactive";
    }
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Merchant/postActive",
        dataType: "json",
        data: {
            id: id,
            status: status,
            note: $('.note').val()
        },
        success: function (data) {
            $.fancybox.hideActivity();
            merchant.dataTable();
            $('#edit-active').modal("hide");
        }
    });
}


merchant.showEdit = function (id) {
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Merchant/detail",
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            $.fancybox.hideActivity();

            $('input[name="title"]').val(data.item.title);
            $('input[name="name"]').val(data.item.name);
            $('input[name="secret_key"]').val(data.item.secret_key);
            $('input[name="username"]').val(data.item.username);
            $('input[name="phone"]').val(data.item.phone);
            $('input[name="password"]').val(data.item.password);
            $('input[name="email"]').val(data.item.email);

            $('#show-edit').modal("show");
            $('#show-edit .modal-title').html("Sửa");
            $('#action-edit').attr('onclick', 'merchant.actionEdit(' + id + ')');
            $('#wallet_id').select2("val",data.wallet_id); 
        }
    });

}

merchant.showDelete = function(id){
    $('#modal-confirm').modal("show");
    $('#action-confirm').attr('onclick', 'merchant.actionDelete(' + id+')');
}
merchant.actionDelete = function (id) {
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Merchant/delete",
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
               
                merchant.dataTable();
                $('#modal-confirm').modal("hide");
                ns_master.toastr("Thực hiện thành công", "success");
            }
            
        }
    });
}

merchant.idClick = 0;

merchant.updateWallet = function () {
    var a_wallet_id = $('#wallet_id').select2('val');
    var s_wallet_id = a_wallet_id.join(',');
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Merchant/updateWallet",
        dataType: "json",
        data: {
            id: merchant.idClick,
            wallet_ids: s_wallet_id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            $('#list-wallet').modal("hide");
            ns_master.toastr("Thực hiện thành công", "success");
            merchant.loadPaymentGate();
        }
    });
}

merchant.loadWallet = function () {
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Merchant/loadWallet",
        dataType: "json",
        data: {
            id: merchant.idClick
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                $('#wallet_id').select2('val', data.items);

            }

        }
    });
}

merchant.loadPaymentGate = function () {
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Merchant/loadPaymentGate",
        dataType: "json",
        data: {
            id: merchant.idClick
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                itemapp.list_wallet = data.items;
            }

        }
    });
}

merchant.removeConfigPaymentGate = function (index,id) {
    if (!confirm("Xác nhận")) {
        return;
    }
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Merchant/removeConfigPaymentGate",
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                itemapp.list_config.splice(index, 1);
            }

        }
    });
}

merchant.showEditType = function(id){
    merchant.idClick = id;
    //Load ví
   // merchant.loadWallet();
    //Load cổng theo ví
    merchant.loadPaymentGate();
    merchant.showListType();
}

merchant.showListWallet = function () {
    $('#show-payment-type').modal("hide");
    $('#list-wallet').modal("show");
   
}

merchant.showListType = function () {
    $('#list-type').modal("show");
}

merchant.showAdd = function(){
    $('#show-edit').modal("show");
    $('#show-edit .modal-title').html("Tạo mới");
    $('#action-edit').attr('onclick', 'merchant.add()');
    $('#edit-active .modal-title').html("Thêm");
}

merchant.add = function () {
    var title = $('input[name="title"]').val();
    if (!title) {
        ns_master.toastr("Vui lòng điền tên mertchant", "error");
        $('input[name="title"]').focus();
        return;
    }
    var name = $('input[name="name"]').val();
    if (!name) {
        ns_master.toastr("Vui lòng điền mã mertchant", "error");
        $('input[name="name"]').focus();
        return;
    }
    var secret_key = $('input[name="secret_key"]').val();
    if (!secret_key) {
        ns_master.toastr("Vui lòng điền mã secret key", "error");
        $('input[name="secret_key"]').focus();
        return;
    }
    var username = $('input[name="username"]').val();
    if (!username) {
        ns_master.toastr("Vui lòng điền username ", "error");
        $('input[name="username"]').focus();
        return;
    }
    var phone = $('input[name="phone"]').val();
    if (!phone) {
        ns_master.toastr("Vui lòng điền phone", "error");
        $('input[name="phone"]').focus();
        return;
    }

    var password = $('input[name="password"]').val();
    if (!password) {
        ns_master.toastr("Vui lòng điền password", "error");
        $('input[name="password"]').focus();
        return;
    }

    var email = $('input[name="email"]').val();
    if (!email) {
        ns_master.toastr("Vui lòng điền email", "error");
        $('input[name="email"]').focus();
        return;
    }

    var a_wallet_id = $('#wallet_id').select2('val');
    if (!a_wallet_id) {
        ns_master.toastr("Vui lòng chọn loại ví cho merchant", "error");
        return;
    }
    var s_wallet_id = a_wallet_id.join(',');

    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Merchant/add",
        dataType: "json",
        data: {
            title: title,
            name: name,
            username: username,
            secret_key: secret_key,
            phone: phone,
            password: password,
            email: email,
            wallet_ids: s_wallet_id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                merchant.dataTable();
                $('#show-edit').modal("hide");
                ns_master.toastr("Thực hiện thành công", "success");
            }

        }
    });
}

merchant.addPaymentGate = function () {
    var paymentgate_id = $('#paymentgate_id').val();
    var wallet_in = $('#wallet_in').val();
    var wallet_out = $('#wallet_out').val();

    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Merchant/addPaymentGate",
        dataType: "json",
        data: {
            wallet_in: wallet_in,
            merchant_id: merchant.idClick,
            paymentgate_id: paymentgate_id,
            wallet_out: wallet_out
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                ns_master.toastr("Thực hiện thành công", "success");
                merchant.showEditType(merchant.idClick);
            }
        }
    });
}

merchant.removePaymentGate = function (id,index) {
    if (!confirm("Xác nhận")) {
        return;
    }
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Merchant/removePaymentGate",
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                ns_master.toastr("Thực hiện thành công", "success");
                itemapp.list_wallet.splice(index, 1);
            }

        }
    });
}
merchant.blockPaymentGate = function (id,index) {
    if (!confirm("Xác nhận")) {
        return;
    }
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Merchant/updateStatusPaymentGate",
        dataType: "json",
        data: {
            id: id,
            status_active: 'deactive'
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                ns_master.toastr("Thực hiện thành công", "success");
                itemapp.list_wallet[index].status_active = "deactive";
            }

        }
    });
}
merchant.unblockPaymentGate = function (id,index) {
    if (!confirm("Xác nhận")) {
        return;
    }
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Merchant/updateStatusPaymentGate",
        dataType: "json",
        data: {
            id: id,
            status_active: 'active'
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                ns_master.toastr("Thực hiện thành công", "success");
                itemapp.list_wallet[index].status_active = "active";
            }

        }
    });
}
merchant.formatMonney = function (num) {
    var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    return n;
},
merchant.loadPaymentGateConfig = function (id) {
    itemapp.payment_gate_id = id;
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Merchant/loadPaymentGateConfig",
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                itemapp.list_config = [];
                data.items.forEach(function (i) {
                    itemapp.list_config.push({
                        'id': i.id,
                        'cash_in': merchant.formatMonney(i.cash_in),
                        'cash_out': merchant.formatMonney(i.cash_out),
                    });
                });
                $('.name-paymentgate').html('Tên cổng: ' + data.name);
                $('.list-type-payment .active').each(function () {
                    $(this).removeClass('active');
                });
                $('.list-type-payment tr[data-id="' + id + '"]').addClass('active');
            }

        }
    });
},

merchant.loadWalletPaymentGate = function (e) {
    var payment_gate_id = $('#paymentgate_id').val();
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Merchant/loadWalletPaymentGate",
        dataType: "json",
        data: {
            id: payment_gate_id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                if (data.wallet_in != 0) {
                    $('#wallet_in').val(data.wallet_in);
                    $('#wallet_in').prop('disabled', 'disabled');
                } else {
                    $('#wallet_in').removeAttr('disabled');
                }
                if (data.wallet_out != 0) {
                    $('#wallet_out').val(data.wallet_out);
                    $('#wallet_out').prop('disabled', 'disabled');
                } else {
                    $('#wallet_out').removeAttr('disabled');
                }

                if (data.type == 1) {
                    $('#wallet_in').val("");
                    $('#wallet_in').prop('disabled', 'disabled');

                    if (data.wallet_out != 0) {
                        $('#wallet_out').val(data.wallet_out);
                        $('#wallet_out').prop('disabled', 'disabled');
                    } else {
                        $('#wallet_out').removeAttr('disabled');
                    }
                }


                if (data.type == 2) {
                    $('#wallet_out').val("");
                    $('#wallet_out').prop('disabled', 'disabled');
                    if (data.wallet_in != 0) {
                        $('#wallet_in').val(data.wallet_in);
                        $('#wallet_in').prop('disabled', 'disabled');
                    } else {
                        $('#wallet_in').removeAttr('disabled');
                    }
                }
                if (data.type == 3) {
                    $('#wallet_in').val("");
                    $('#wallet_in').prop('disabled', 'disabled');
                    if (data.wallet_out != 0) {
                        $('#wallet_out').val(data.wallet_out);
                        $('#wallet_out').prop('disabled', 'disabled');
                    } else {
                        $('#wallet_out').removeAttr('disabled');
                    }
                }
                if (data.type == 4) {
                    $('#wallet_out').val("");
                    $('#wallet_out').prop('disabled', 'disabled');
                    if (data.wallet_in != 0) {
                        $('#wallet_in').val(data.wallet_in);
                        $('#wallet_in').prop('disabled', 'disabled');
                    } else {
                        $('#wallet_in').removeAttr('disabled');
                    }
                }
                
            }

        }
    });
},


merchant.addConfigPaymentGate = function (id) {
    var cash_in = $('.txt-cash-in').val();
    if (!cash_in) {
        ns_master.toastr("Vui lòng điền ví vào", "error");
        $('.txt-cash-in').focus();
        return;
    }
    var cash_out = $('.txt-cash-out').val();
    if (!cash_out) {
        ns_master.toastr("Vui lòng điền ví ra", "error");
        $('.txt-cash-out').focus();
        return;
    }
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Merchant/addConfigPaymentGate",
        dataType: "json",
        data: {
            payment_gate_id: id,
            cash_in: cash_in,
            cash_out: cash_out
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                itemapp.list_config.push(data.item);
            }

        }
    });
}

$(function () {
    merchant.dataTable();
    $('#wallet_id').select2();

    itemapp = new Vue({
        el: '#list-type',
        data: {
            list_wallet: [
                {
                    id: 0,
                    wallet_name: 'hello',
                    items: [
                        {id:0,name:"Cổng thanh toán"}
                    ]
                }
            ],
            item: {
                code: '',
                name: '',
                wallet_out:0
            },
            list_config: [
            ],
            payment_gate_id: 0

        },
       
    });
    create_payment_gate = new Vue({
        el: '#vueAppPaymentGateInfo',
        data: {
            toast: {
                error: false,
                msg: ""
            },
            present: {
                title: "Tạo mới cổng",
                btn_title: "Tạo mới cổng"
            },
            items: [],
            form: {
                merchant: 0,
                wallet_in: 0
            },
            wallets: [],
            merchants: [],
            manager_wallets: []
        },
        // Methods
        methods: {

            // Btn add rule click
            addRule: function () {
                this.items.push({
                    "cash_in": this.item.cash_in,
                    "cash_out": this.item.cash_out
                });
                this.item.cash_in = "";
                this.item.cash_out = "";
            },

            // Remove rule
            removeValueCoin: function (index) {
                console.log(index);
                this.items.splice(index, 1);
            },

            // On merchant select
            onMerchantSelected: function () {
                comp = this;
                $.post("/AgencyPaymentGateAPI/WalletByMerchant", {
                    merchant_id: this.form.merchant
                }, function (json) {
                    if (!json.error) {
                        comp.wallets = json.items;
                    }
                }, 'json');
            },

            // Submit - Do submit new wallet to system
            onSubmit: function () {
                if ($('#check_sms').is(':checked')) {
                    sms = "1";
                } else {
                    sms = "0";
                }

                if ($('#check_purchase').is(':checked')) {
                    purchase = "1";
                } else {
                    purchase = "0";
                }

                var endpoint = (this.id) ? "/Paymentgate/Update" : "/Paymentgate/Create";
                $.post(endpoint, {
                    code: this.form.code,
                    name: this.form.name,
                    sms: sms,
                    purchase: purchase,
                    wallet: this.form.wallet,
                    merchantid: this.form.merchant,
                    iorate: this.items,
                    id: this.id,
                    wallet_in: this.form.wallet_in
                }, function (json) {
                    if (json.error) {
                        alert(json.msg);
                    } else {
                        // Close dialog
                        $('#vueAppPaymentGateInfo').modal('hide');
                        paymentgate.dataTable();
                    }
                }, 'json');
            }
        },

        // Created
        created: function () {
            comp = this;
            //console.log("Get merchants");
            $.post('/AgencyPaymentGateAPI/Merchants', this.form, function (json) {
                // Show if error
                if (json.error) {
                    comp.toast = json;
                } else {
                    comp.merchants = json.items;
                }
            }, 'json');
            // Gete all manager wallet manager_wallets
            $.post('/Paymentgate/WalletsIn', this.form, function (json) {
                // Show if error
                if (json.error) {
                    comp.toast = json;
                } else {
                    comp.manager_wallets = json.items;
                }
            }, 'json');
        }
    });

});