﻿account = {};
account.dataTable = function (change) {
    var page_start = $('#datatable .paginate_button.current').html();
    //var lengh = $('select[name="datatable-labelManagement_length"]').val();
    var page = (page_start - 1) * 10;
    if (!page) {
        page = 0;
    }
    if (change) {
        page = 0;
    }

    $('#datatable').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);
        },
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "merchant_id", "value": $('#merchant_id').val() },
                { "name": "s_seach", "value": $('#s_seach').val() }
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/Account/Listdata",
        "aaSorting": [[0, "desc"]]
    });
}

account.showEditActive = function (id,e) {
    $('#edit-active').modal("show");
    var avtice = $(e).attr('data-active');
    $('#edit-active type[type="radio"]').prop('checked', false);
    $('#' + avtice).prop('checked', true);
    $('#edit-active #action-confirm').attr("onclick", "account.actionEditActive(" + id+")");
}

account.actionEditActive = function (id) {
    var status = "active";
    if ($('#active').is(":checked")) {
        status = "active";
    } else {
        status = "deactive";
    }
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Account/postActive",
        dataType: "json",
        data: {
            id: id,
            status: status,
            note: $('.note').val()
        },
        success: function (data) {
            $.fancybox.hideActivity();
            account.dataTable();
            $('#edit-active').modal("hide");
        }
    });
}

account.getListWallet = function (id) {
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Account/getListWallet",
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            var html = '';
            data.items.forEach(function (item) {
                if (item[2]) {
                    balance = item[2];
                } else {
                    balance = 0;
                }
                if (item[3] == "0") {
                    lock = "Mở";
                } else {
                    lock = "Khóa";;
                }
                var s_tr = '<tr data-id="' + item[0]+'" data-text="Tổng số dư: ' + balance+' ' + item[1]+'" onclick=account.getDataHistory(' + id + ',' + item[0]+')>';
                s_tr += '<td>' + item[0] + '</td>';
                s_tr += '<td>' + item[1] + '</td>';
                s_tr += '<td>' + balance + '</td>';
                s_tr += '<td>' + lock + '</td>';
                s_tr += '</tr>';
                html += s_tr;
            });
            $('#datatable-pay tbody').html(html);
            account.getDataHistory(id, data.items[0][0]);
            $('#show-history').modal("show");
        }
    });
}

account.getDataHistory = function (id, coin_id) {
    var text = $('.body-wallet tr[data-id="' + coin_id + '"]').attr("data-text");
    $('.ttile-history-of-coin').html(text);
    $('#datatable-history').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

        },
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "userid", "value": id },
                { "name": "wallet_id", "value": coin_id }
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/Account/ListdataHistory",
        "aaSorting": [[0, "desc"]]
    });

    $('.body-wallet tr.active').each(function () {
        $(this).removeClass('active');
    });

    $('.body-wallet tr[data-id="' + coin_id + '"]').addClass('active');

}
account.userid = 0;
account.showAction = function (id) {
    account.userid = id;
    $('#show-action').modal("show");
    $('.view-change-password').attr('onclick', 'account.viewChangePassword(' + id + ')');
    $('.view-confirm-remove').attr('onclick', 'account.confirmRemoveAccount(' + id + ')');
    
}

account.viewChangePassword = function () {
    $('#view-change-password').modal("show");
    $('#show-action').modal("hide");
}

account.actionChangePassword = function () {
    var password_new = $('.pass_new').val();
    if (!password_new) {
        ns_master.toastr("Vui lòng nhập mật khẩu", "error");
        return;
    }
    var pass_re = $('.pass_re').val();
    if (password_new != pass_re) {
        ns_master.toastr("Nhập lại mật khẩu không đúng", "error");
        return;
    }

    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Account/updatePassword",
        dataType: "json",
        data: {
            id: account.userid,
            password: password_new
        },
        success: function (data) {
            $.fancybox.hideActivity();
            $('#modal-confirm').modal("hide");
            ns_master.toastr("Thực hiện thành công", "success");
            $('#view-change-password').modal('hide');
        }
    });

}

account.showHistory = function () {
    $('#show-history').modal("show");
}

account.confirmRemoveAccount = function (id) {
    $('#modal-confirm').modal("show");
    $('#action-confirm').attr('onclick', 'account.removeAccount(' + id + ')');
    $('#show-action').modal("hide");
}
account.removeAccount = function (id) {
   
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Account/removeAccount",
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            $('#modal-confirm').modal("hide");
            account.dataTable();
            ns_master.toastr("Thực hiện thành công", "success");
        }
    });
}
getMaxValue = function (arr) {
    return Math.max.apply(Math, arr);
}
formatMonney = function (num) {
    var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
    return n;
}

account.usert_statis_id = 0;
account.usert_statis_username = 0;
account.getListInputOut = function (id,username) {
    account.usert_statis_id = id;
    account.usert_statis_username = username;
    $('#show-getinputoutput .modal-title').html("Thống kê nạp tiền, rút tiền : " + username)
    account.getListInputOutPost();
}


account.getListInputOutPost = function () {
    var start_time = $('input[name="daterangepicker_start"]').val();
    var end_time = $('input[name="daterangepicker_end"]').val();
    $('#show-getinputoutput').modal('show');
    var type = $('#type').val();
    if (type == "day") {
        urlpost = "/Account/GetListInputOut";
    } else {
        urlpost = "/Account/GetListInputOutMonth";
    }
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: urlpost ,
        dataType: "json",
        data: {
            id: account.usert_statis_id,
            start_time: start_time,
            end_time: end_time
        },
        success: function (json) {

            $.fancybox.hideActivity();
            if (json.error) {
                ns_master.toastr(json.msg, "error");
                return;
            }
            var data = json.history;
            var data_out = json.history_out;
            dataDailySalesChart = {
                labels: [],
                series: [[]]
            };

            for (i = 0; i < data.length; i++) {
                var val = data[i];
                dataDailySalesChart.labels.push(val.date);
                dataDailySalesChart.series[0].push(val.value);
            }

            series_2 = [];
            for (i = 0; i < data_out.length; i++) {
                series_2.push(data_out[i].value);
            }


            $('.inputmoney b').html(json.balance);
            $('.outputmoney b').html(json.balance_out);
            account.getDataDetailTransaction(account.usert_statis_id);
            //line
            Chart.defaults.global.defaultFontColor = '#333';
            var ctxL = document.getElementById("lineChart").getContext('2d');
            var myLineChart = new Chart(ctxL, {
                type: 'line',
                data: {
                    labels: dataDailySalesChart.labels,
                    datasets: [
                        {
                            label: "Nạp",
                            backgroundColor: "rgba(255,99,132,0.2)",
                            fillColor: "rgba(255,99,132,0.2)",
                            strokeColor: "rgba(255,99,132,0.2)",
                            pointColor: "rgba(255,99,132,0.2)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(255,99,132,0.2)",
                            data: dataDailySalesChart.series[0]
                        },
                        {
                            label: "Rút",
                            backgroundColor: "rgba(54,162,235,0.2)",
                            fillColor: "rgba(151,187,205,0.2)",
                            strokeColor: "rgba(151,187,205,1)",
                            pointColor: "rgba(151,187,205,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(151,187,205,1)",
                            data: series_2
                        }
                    ]
                },
                options: {
                    responsive: true,
                    tooltips: {
                        callbacks: {
                            label: function (t, d) {
                                var xLabel = d.datasets[t.datasetIndex].label;
                                var yLabel = t.yLabel >= 1000 ? t.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : t.yLabel;
                                return xLabel + ': ' + yLabel;
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                callback: function (value, index, values) {
                                    if ((value < 0 && value > -1) || (value > 0 && value < 1)) {
                                        return value.toFixed(1);
                                    }
                                    if (parseInt(value) >= 1000) {
                                        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                    } else {
                                        return value;
                                    }
                                }
                            }
                        }]
                    }
                }
            });
        }
    });
    
}

account.getDataDetailTransaction = function (id) {
    var page_start = $('#datatable-history .pagination .paginate_button.current > a').html();
    //var lengh = $('select[name="datatable-labelManagement_length"]').val();
    var page = (page_start - 1) * 10;
    if (!page) {
        page = 0;
    }

    var start_time = $('input[name="daterangepicker_start"]').val();
    var end_time = $('input[name="daterangepicker_end"]').val();
    $('#datatable-history-detail').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

        },
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "id", "value": id },
                { "name": "start_time", "value": start_time },
                { "name": "end_time", "value": end_time }
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/Account/getDataDetailTransaction",
        "aaSorting": [[0, "desc"]]
    });
}


$(function () {
    account.dataTable();

    $('#daterange-btn').daterangepicker(
        {
            ranges: {
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(6, 'days'),
            endDate: moment()
        },
        function (start, end) {
            $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        }
    );
});