﻿dashboard = {};

dashboard.usert_statis_id = 0;
dashboard.getListInputOut = function (id) {
    dashboard.usert_statis_id = id;
    dashboard.getListInputOut();
}


dashboard.formatMonney = function (num) {
    var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    return n;
}

dashboard.getListInputOut = function () {
    var start_time = $('input[name="daterangepicker_start"]').val();
    var end_time = $('input[name="daterangepicker_end"]').val();
    $('#show-getinputoutput').modal('show');
    var type = $('#type').val();
    if (type == "day") {
        urlpost = "/dashboard/GetListInputOut";
    } else {
        urlpost = "/dashboard/GetListInputOutMonth";
    }
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: urlpost,
        dataType: "json",
        data: {
            id: dashboard.usert_statis_id,
            start_time: start_time,
            end_time: end_time
        },
        success: function (json) {
            $.fancybox.hideActivity();
            if (json.error) {
                ns_master.toastr(json.msg, "error");
                return;
            }
            

            var data = json.history;
            var data_out = json.history_out;
            dataDailySalesChart = {
                labels: [],
                series: [[]]
            };

            for (i = 0; i < data.length; i++) {
                var val = data[i];
                dataDailySalesChart.labels.push(val.date);
                dataDailySalesChart.series[0].push(val.value);
            }

            series_2 = [];
            for (i = 0; i < data_out.length; i++) {
                series_2.push(data_out[i].value);
            }


            $('.inputmoney b').html(json.balance);
            $('.outputmoney b').html(json.balance_out);
            dashboard.getDataDetailTransaction(dashboard.usert_statis_id);
            //line

            
            Chart.defaults.global.defaultFontColor = '#333';
            var ctxL = document.getElementById("lineChart").getContext('2d');
            var myLineChart = new Chart(ctxL, {
                type: 'line',
                data: {
                    labels: dataDailySalesChart.labels,
                    datasets: [
                        {
                            label: "Nạp",
                            backgroundColor: "rgba(255,99,132,0.2)",
                            fillColor: "rgba(255,99,132,0.2)",
                            strokeColor: "rgba(255,99,132,0.2)",
                            pointColor: "rgba(255,99,132,0.2)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(255,99,132,0.2)",
                            data: dataDailySalesChart.series[0]
                        }
                    ]
                },
                options: {
                    responsive: true,
                    tooltips: {
                        callbacks: {
                            label: function (t, d) {
                                var xLabel = d.datasets[t.datasetIndex].label;
                                var yLabel = t.yLabel >= 1000 ? t.yLabel.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") : t.yLabel;
                                return xLabel + ': ' + yLabel;
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                callback: function (value, index, values) {
                                    if ((value < 0 && value > -1) || (value > 0 && value < 1)) {
                                        return  value.toFixed(1);
                                    }
                                    if (parseInt(value) >= 1000) {
                                        return value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                                    } else {
                                        return value
                                    }
                                }
                            }
                        }]
                    }
                }
            });
            var ctxL = document.getElementById("lineChart2").getContext('2d');
            var myLineChart = new Chart(ctxL, {
                type: 'line',
                data: {
                    labels: dataDailySalesChart.labels,
                    datasets: [
                        {
                            label: "Rút",
                            backgroundColor: "rgba(54,162,235,0.2)",
                            fillColor: "rgba(151,187,205,0.2)",
                            strokeColor: "rgba(151,187,205,1)",
                            pointColor: "rgba(151,187,205,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(151,187,205,1)",
                            data: series_2,
                        }
                    ]
                },
                options: {
                    responsive: true,
                    tooltips: {
                        callbacks: {
                            label: function (t, d) {
                                var xLabel = d.datasets[t.datasetIndex].label;
                                var yLabel = t.yLabel >= 1000 ? t.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : t.yLabel;
                                return xLabel + ': ' + yLabel;
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                callback: function (value, index, values) {
                                    if ((value < 0 && value > -1) || (value > 0 && value < 1)) {
                                        return value.toFixed(1);
                                    }
                                    if (parseInt(value) >= 1000) {
                                        return  value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                    } else {
                                        return value;
                                    }
                                }
                            }
                        }]
                    }
                }
            });
        }
    });

}

dashboard.getDataDetailTransaction = function (id) {
    var page_start = $('#datatable-history .pagination .paginate_button.current > a').html();
    //var lengh = $('select[name="datatable-labelManagement_length"]').val();
    var page = (page_start - 1) * 10;
    if (!page) {
        page = 0;
    }

    var start_time = $('input[name="daterangepicker_start"]').val();
    var end_time = $('input[name="daterangepicker_end"]').val();
    $('#datatable-history-detail').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

        },
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "id", "value": id },
                { "name": "start_time", "value": start_time },
                { "name": "end_time", "value": end_time }
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/dashboard/getDataDetailTransaction",
        "aaSorting": [[0, "desc"]]
    });
}


$(function () {
    $('#daterange-btn').daterangepicker(
        {
            ranges: {
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(6, 'days'),
            endDate: moment()
        },
        function (start, end) {
            $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        }
    );
    dashboard.getListInputOut();
});