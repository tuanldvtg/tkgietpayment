﻿profit = {};

profit.loadStaticProfit = function () {
    var start_time = $('input[name="daterangepicker_start"]').val();
    var end_time = $('input[name="daterangepicker_end"]').val();
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Paymentgate/profit",
        dataType: "json",
        data: {
            start_time: start_time,
            end_time: end_time,
            merchant_id: $('#merchant-id').val()
        },
        success: function (data) {
            $.fancybox.hideActivity();
            var html = '';
            data.items.forEach(function (item) {
                html += '<div class="col-md-3 col-sm-6 col-xs-12">'+
                    '<div class="info-box" >'+
                        '<span class="info-box-icon bg-red">'+
                            '<i aria-hidden="true" class="fa fa-newspaper-o"></i>'+
                        '</span>'+
                        '<div class="info-box-content">'+
                    '<span class="info-box-text">' + item.name + '<br />(' + item.wallet_name+')</span>' +
                    '<span class="info-box-number">' + item.value + '</span>' +
                        '</div>' +
                '</div >'+
            '</div >';
            });
            $('.list-static').html(html);
        }
    });
};
profit.loadHistoryByTime = function () {
    profit.loadStaticProfit();
}



profit.loadDataTabelHistoryByTime = function () {

    var start_time = $('input[name="daterangepicker_start"]').val();
    var end_time = $('input[name="daterangepicker_end"]').val();
    $('#datatable').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);
        },
        "fnServerParams": function (aoData) {
            aoData.push(
                { "name": "start_time", "value": start_time },
                { "name": "end_time", "value": end_time },
                { "name": "merchant_id", "value": $('#merchant-id').val()}
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/Paymentgate/ListdataProfit",
        "aaSorting": [[0, "desc"]]
    });
};

profit.clickLoadHistory = function () {
    profit.loadStaticProfit();
    profit.loadDataTabelHistoryByTime();
}

$(function () {
    $('#daterange-btn').daterangepicker(
        {
            ranges: {
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(6, 'days'),
            endDate: moment()
        },
        function (start, end) {
            $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        }
    );
    profit.loadStaticProfit();
    profit.loadDataTabelHistoryByTime();
   
});

