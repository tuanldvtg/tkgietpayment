﻿coin = {};
coin.dataTable = function (change) {
    var page_start = $('.pagination .paginate_button.current > a').html();
    //var lengh = $('select[name="datatable-labelManagement_length"]').val();
    var page = (page_start - 1) * 10;
    if (!page) {
        page = 0;
    }
    if (change) {
        page = 0;
    }

    $('#datatable').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

        },
        "fnServerParams": function (aoData) {
            aoData.push(
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/Coin/Listdata",
        "aaSorting": [[0, "desc"]]
    });
}




coin.showDelete = function (id) {
    $('#modal-confirm').modal("show");
    $('#action-confirm').attr('onclick', 'coin.actionDelete(' + id + ')');
}
coin.actionDelete = function (id) {
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Coin/delete",
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                coin.dataTable();
                $('#modal-confirm').modal("hide");
                ns_master.toastr("Thực hiện thành công", "success");
            }

        }
    });
}

coin.showAdd = function () {
    $('#show-edit').modal("show");
    $('#show-edit .modal-title').html("Tạo mới");
    $('#action-edit').attr('onclick', 'coin.add()');
    $('#edit-active .modal-title').html("Thêm");
}

coin.add = function () {
    var title = $('input[name="title"]').val();
    if (!title) {
        ns_master.toastr("Vui lòng điền tên Coin", "error");
        $('input[name="title"]').focus();
        return;
    }

    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Coin/add",
        dataType: "json",
        data: {
            title: title
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                coin.dataTable();
                $('#show-edit').modal("hide");
                ns_master.toastr("Thực hiện thành công", "success");
            }
            
           
        }
    });
}
coin.actionEdit = function (id) {
    var title = $('input[name="title"]').val();
    if (!title) {
        ns_master.toastr("Vui lòng điền tên Coin", "error");
        $('input[name="title"]').focus();
        return;
    }


    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Coin/edit",
        dataType: "json",
        data: {
            title: title,
            id: id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                coin.dataTable();
                $('#show-edit').modal("hide");
                ns_master.toastr("Thực hiện thành công", "success");
            }     
        }
    });
}


coin.showEdit = function (id) {
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Coin/detail",
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            $.fancybox.hideActivity();

            $('input[name="title"]').val(data.item.name);

            $('#show-edit').modal("show");
            $('#show-edit .modal-title').html("Sửa");
            $('#action-edit').attr('onclick', 'coin.actionEdit(' + id + ')');
        }
    });

}




$(function () {
    coin.dataTable();
});