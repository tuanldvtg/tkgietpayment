fnewsoft = {};
$(function () {
    fnewsoft.init();
});
fnewsoft.lat = "20.9980303";
fnewsoft.lng = "105.7950015";
fnewsoft.init = function () {
    fnewsoft.cookie();
    fnewsoft.slide();
    fnewsoft.slidedetails();
//    fnewsoft.autosuggestion();
    fnewsoft.autoSuggestionPlace();
    fnewsoft.autosuggestionnew();
    fnewsoft.getKeyCode();
    if (fnewsoft.lat === '' || fnewsoft.lng === '') {
        // getLocation();
    }


};

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}
function showPosition(position) {
    fnewsoft.lat = Cookies.set('lat', position.coords.latitude, {expires: 5});
    fnewsoft.lng = Cookies.set('lng', position.coords.longitude, {expires: 5});

}
fnewsoft.getParnerByLoca = function () {
    if (fnewsoft.lat === '' || fnewsoft.lng === '') {
        getLocation();
    }


    $.post('/', {
        'ajax': 'partner/service',
        'action': 'getParnerByLoca',
        'lat': fnewsoft.lat,
        'lng': fnewsoft.lng
    }, function (json) {
        if (!json.error) {
            if (json.items) {
                $('#ul_pro_near').html(json.items)
            } else {
                $('#ul_pro_near').html('Chưa có địa điểm gần đây')
            }

        } else {
            $('#ul_pro_near').html('Chưa có địa điểm gần đây')
        }
    }, 'json');
};

fnewsoft.cookie = function () {
    var vcookie = Cookies.get('city');
    var path = $(location).attr('pathname');
    var st = '/';
    console.log(path);
    if (path === st) {
        if (!vcookie) {
            $('.popup-choose-place').popup('show');
        } else {

        }
    }



};
fnewsoft.openPopup = function () {
    $('.popup-choose-place').popup('show');
};
fnewsoft.openRegister = function () {
    $('#popup_register').popup('show');
};
fnewsoft.openForgot = function () {
    $('#popup_forgot').popup('show');
};
fnewsoft.flag = true;
fnewsoft.openLogin = function () {
    if (fnewsoft.flag == true) {
        $(".header-login").css({"backgroundColor": "white"});
        $('#popup_login').show();
        setTimeout(function () {
            fnewsoft.flag = false;
        }, 500);
    }
    if (fnewsoft.flag == false) {
        $(".header-login").css({"backgroundColor": "#dfdfdf"});
        $('#popup_login').hide();
        setTimeout(function () {
            fnewsoft.flag = true;
        }, 500);
    }

//    $(document).on("click", "body", function (event) {
//        if (event.target.id !== 'popup_login_show' ) {
//             $(".header-login").css({"backgroundColor": "#dfdfdf"});
//             $('#popup_login').hide();
//             console.log('click ra ngoai');
//        }else{
//            console.log('click trong');
//        }
//    });

};
fnewsoft.selectCity = function () {
    $('#popupbox').css('overflow', 'visible');
    if ($('#list_city:visible').length == 0) {
        $('#list_city').fadeIn('slow');
    } else {
        $('#list_city').fadeOut('slow');
    }
    $('#list_city li').click(function () {
        var cityid = $(this).attr("id");
        Cookies.set('city', cityid, {expires: 7});
        location.reload();
    });

};
fnewsoft.setItemProvince = function (id) {
    Cookies.set('city', id, {expires: 7});
    location.reload();
};


fnewsoft.register = function () {
    if (fnewsoft.checkformRegister()) {
        return;
    }
    element = $('#popup_register');
    var email = $('input#email', element).val();
    var pw = $('input#password', element).val();
    var repw = $('input#repassword', element).val();
    var mobi = $('input#mobile', element).val();
    var name = $('input#name', element).val();

    $.post('/', {
        'ajax': 'user/helper',
        'action': 'register',
        'email': email,
        'pw': pw,
        'name': name,
        'mobi': mobi
    }, function (json) {
        if (!json.error) {
            location.reload();
        } else {
            $('.box-error', element).html(json.msg);
        }
    }, 'json');
};
fnewsoft.forgot = function () {
    element = $('#popup_forgot');
    var email = $('input#email', element).val();
    if (!email) {
        $('.box-error', element).html('Vui lòng nhập email!');
        return true;
    } else {
        $('.box-error', element).html('');
    }
    var filteremail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filteremail.test(email)) {
        $('.box-error', element).html('Email không đúng định dạng!');
        return true;
    } else {
        $('.box-error', element).html('');
    }

    $("#popup_forgot .popup-close").fadeIn("slow");
    $("#popup_forgot .popup-close").css({"background-image": "url('/static/template/version1/css/images/ajax-loader.gif')", "background-repeat": "no-repeat"});


    $.post('/', {
        'ajax': 'user/helper',
        'action': 'forgot',
        'email': email,
    }, function (json) {
        if (!json.error) {
            location.reload();
        } else {
            $('.box-error', element).html(json.msg);
        }
    }, 'json');
};

fnewsoft.login = function () {
    element = $('#popup_login');
    var email = $('input#email', element).val();
    var pw = $('input#password', element).val();
    if (!email) {
        $('.box-error', element).html('Vui lòng nhập email!');
        return true;
    } else {
        $('.box-error', element).html('');
    }
    if (!pw) {
        $('.box-error', element).html('Vui lòng nhập mật khẩu!');
        return true;
    } else {
        $('.box-error', element).html('');
    }
    $("#popup_login .popup-close").fadeIn("slow");
    $("#popup_login .popup-close").css({"background-image": "url('/static/template/version1/css/images/ajax-loader.gif')", "background-repeat": "no-repeat"});
    $.post('/', {
        'ajax': 'user/helper',
        'action': 'login',
        'email': email,
        'pw': pw
    }, function (json) {
        if (!json.error) {
            location.reload();
        } else {
            $('.box-error', element).html(json.msg);
        }
    }, 'json');
};

fnewsoft.slide = function () {
    $('#slidejs').slidesjs({
        width: 1280,
        height: 350,
        navigation: {
            active: false
        },
        pagination: {
            effect: "fade"
        },
        effect: {
            fade: {
                speed: 800
            }
        },
        play: {
            effect: "fade",
            interval: 4000,
            auto: true

        }
    });
};
fnewsoft.slidedetails = function () {
    var sync1 = $("#big-carousel");
    var sync2 = $("#small-carousel");

    sync1.owlCarousel({
        singleItem: true,
        slideSpeed: 1000,
        navigation: false,
        pagination: false,
        afterAction: syncPosition,
        responsiveRefreshRate: 200,
    });

    sync2.owlCarousel({
        items: 4,
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [979, 3],
        itemsTablet: [768, 4],
        itemsMobile: [479, 2],
        pagination: false,
        responsiveRefreshRate: 100,
        afterInit: function (el) {
            el.find(".owl-item").eq(0).addClass("synced");
        }
    });

    function syncPosition(el) {
        var current = this.currentItem;
        $("#small-carousel")
                .find(".owl-item")
                .removeClass("synced")
                .eq(current)
                .addClass("synced")
        if ($("#small-carousel").data("owlCarousel") !== undefined) {
            center(current)
        }
    }

    $("#small-carousel").on("click", ".owl-item", function (e) {
        e.preventDefault();
        var number = $(this).data("owlItem");
        sync1.trigger("owl.goTo", number);
    });

    function center(number) {
        var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;
        for (var i in sync2visible) {
            if (num === sync2visible[i]) {
                var found = true;
            }
        }

        if (found === false) {
            if (num > sync2visible[sync2visible.length - 1]) {
                sync2.trigger("owl.goTo", num - sync2visible.length + 2)
            } else {
                if (num - 1 === -1) {
                    num = 0;
                }
                sync2.trigger("owl.goTo", num);
            }
        } else if (num === sync2visible[sync2visible.length - 1]) {
            sync2.trigger("owl.goTo", sync2visible[1])
        } else if (num === sync2visible[0]) {
            sync2.trigger("owl.goTo", num - 1)
        }

    }
};
fnewsoft.checkformRegister = function () {
    element = $('#popup_register');
    var filteremail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var filterphone = /^(01([0-9]{2})|09[0-9])(\d{7})$/i;
    var email = $('input#email', element);
    var pw = $('input#password', element);
    var rpw = $('input#repassword', element);
    var mobile = $('input#mobile', element);
    var name = $('input#name', element);

    if (!name.val() || !email.val() || !pw.val() || !mobile.val()) {
        $('.box-error', element).html('Vui lòng điền đầy đủ thông tin!');
        return true;
    } else {
        $('.box-error', element).html('');
    }
    if (!name.val()) {
        $('.box-error', element).html('Vui lòng điền tên hiển thị!');
        return true;
    } else {
        $('.box-error', element).html('');
    }
    if (rpw.val() !== pw.val()) {
        $('.box-error', element).html('Mật khẩu không giống vui lòng thử lại');
        return true;
    } else {
        $('.box-error', element).html('');
    }
    if (!filteremail.test(email.val())) {
        $('.box-error', element).html('Email không đúng định dạng!');
        return true;
    } else {
        $('.box-error', element).html('');
    }
    if (!filterphone.test(mobile.val())) {
        $('.box-error', element).html('Số điện thoại không đúng định dạng!');
        return true;
    } else {
        $('.box-error', element).html('');
    }
    $("#popup_register .popup-close").fadeIn("slow");
    $("#popup_register .popup-close").css({"background-image": "url('/static/template/version1/css/images/ajax-loader.gif')", "background-repeat": "no-repeat"});

    return false;
};

fnewsoft.getKeyCode = function () {
    var $currentDiv = $("#place-list-search").children().first();
    $currentDiv.css("background", "#ebebeb");
    $('.header-search form input').on('keypress', function (e) {
        return e.which !== 13;
    });
    $("html").keyup(function (e) {

        switch (e.which) {
            case 38: // up
                var $previousDiv;
                if ($currentDiv.prev().size() === 0)
                    $previousDiv = $("#place-list-search").children().last();
                else {
                    $previousDiv = $currentDiv.prev();
                }
                $currentDiv.removeClass('active-search');
                $previousDiv.addClass('active-search');
                $currentDiv = $previousDiv;
                break;

            case 40: // down
                var $nextDiv;
                if ($currentDiv.next().size() === 0) {
                    $nextDiv = $("#place-list-search").children().first();
                } else {
                    $nextDiv = $currentDiv.next();
                }
                $currentDiv.removeClass('active-search');
                $nextDiv.addClass('active-search');
                $currentDiv = $nextDiv;
                break;
            case 13: // enter
                var gettext;
                if ($('.active-search').hasClass('p-search-category')) {

                    gettext = $('.active-search .search-title').text();
                    var cid = $("#place-list-search .p-search-category").attr("data-id");
                    window.location = '/tim-kiem/danh-muc-' + cid + '/?kw=' + $.trim(gettext);
                } else {

                    gettext = $('.active-search').text();
                    $("#autosuggest").val(gettext);
                    $('#place-list-search').hide();
                }


//                var gettext;
//
//                if ($('#place-list-search li').hasClass('p-search-category')) {
//                    var keyx = $('#place-list-search li .category-title').text();
//                    var cid = $("#place-list-search .p-search-category").attr("data-id");
//                    window.location = '/tim-kiem/danh-muc-' + cid + '/?kw=' + keyx;
//                } else {
//                    gettext = $('.active-search').text();
//                    $("#autosuggest").val(gettext);
//                }
//
//                $('#place-list-search').hide();
                break;
            default:
                return;
                // exit this handler for other keys
        }
        e.preventDefault(); // prevent the default action (scroll / move caret)
    });


};
fnewsoft.autosuggestionnew = function () {

    $(document).on("click", "body", function (event) {
        if (event.target.id !== 'autosuggest') {
            $('#place-list-search').hide();
        }
    });

    $("#autosuggest").on('input', function () {
        var keyword = $('#autosuggest').val();
        if (keyword.length >= 1) {
            $.post('/', {
                'ajax': 'partner/service',
                'action': 'searchsuggest',
                'search': keyword
            }, function (json) {
                $('#place-list-search').show();
                $('#place-list-search').html(json.category);
                $('#place-list-search').append(json.partner);




            }, 'json');
        } else {
            $('#place-list-search').hide();
        }
    });
//    $("#autosuggest").keyup(function () {

//    new autocomplete('autosuggest', 'place-list-search');
//    });
};
fnewsoft.setItemSelected = function (item) {
    $('#autosuggest').val(item);
    $('#place-list-search').hide();
};
fnewsoft.setItemCategory = function (item, kw) {
    window.location = '/tim-kiem/danh-muc-' + item + '/?kw=' + kw;
};
fnewsoft.autoSuggestionPlace = function () {

    $(document).on("click", "body", function (event) {
        if (event.target.id != 'choose-place-input') {
            $('#place-list-choose').hide();
        }
    });
    $("#choose-place-input").keyup(function () {
        var keyword = $('#choose-place-input').val();
        if (keyword.length >= 2) {
            $.post('/', {
                'ajax': 'partner/service',
                'action': 'searchsuggestcity',
                'search': keyword
            }, function (json) {
                if (json) {
                    $('#place-list-choose').show();
                    $('#place-list-choose').html(json);
                } else {
                    $('#place-list-choose').html('<li>Không tìm thấy Quận / Huyện mà tìm kiếm</li>');
                }


            }, 'json');
        } else {
            $('#place-list-choose').hide();
        }
    });
};


function autocomplete(textBoxId, containerDivId) {
    var ac = this;
    this.textbox = document.getElementById(textBoxId);
    this.ul = document.getElementById(containerDivId);
    this.list = this.ul.getElementsByTagName('li');
    this.pointer = null;
    this.textbox.onkeydown = function (e) {
        e = e || window.event;
        switch (e.keyCode) {
            case 38: //up                
                ac.selectDiv(-1);
                break;
            case 40: //down                
                ac.selectDiv(1);
                break;
        }
    };

    this.selectDiv = function (inc) {

        if (this.pointer > 1) {
            scrollDiv();
        }
        if (this.pointer === 0)
            document.getElementById("place-list-search").scrollTop = 0;
        if (this.pointer !== null && this.pointer + inc >= 0 && this.pointer + inc < this.list.length) {
            this.list[this.pointer].className = '';
            this.pointer += inc;
            this.list[this.pointer].className = 'active-search';
//            var selected1 = this.list[this.pointer].innerHTML;
//            this.textbox.value = removeTags(selected1);
            //alert(this.pointer);
        }
        if (this.pointer === null) {
            this.pointer = 0;
            scrolLength = 20;
            this.list[this.pointer].className = 'active-search';
//            var selected = this.list[this.pointer].innerHTML;
//            this.textbox.value = removeTags(selected);
        }
    };
    function scrollDiv() {
        if (window.event.keyCode === 40) {
            document.getElementById("place-list-search").scrollTop = scrolLength;
            scrolLength = scrolLength + 19;
        } else if (window.event.keyCode === 38) {

            scrolLength = scrolLength - 19;
            document.getElementById("place-list-search").scrollTop = scrolLength;

        }
    }
}
function removeTags(text) {
    var rex = /(<([^>]+)>)/ig;
    return text.replace(rex, "");

}
