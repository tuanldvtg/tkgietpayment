js_ns = {};

js_ns.toastr = function (msg, type) {
    if (type === 'error') {
        toastr.error(msg, "Thông báo");
    } else if (type === 'success') {
        toastr.success(msg, "Thông báo");
    } else {
        toastr.error(msg, "Thông báo");
    }
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
};
