﻿wallet = {};
wallet.dataTable = function (change) {
    var page_start = $('.pagination .paginate_button.current > a').html();
    //var lengh = $('select[name="datatable-labelManagement_length"]').val();
    var page = (page_start - 1) * 10;
    if (!page) {
        page = 0;
    }
    if (change) {
        page = 0;
    }

    $('#datatable').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

        },
        "fnServerParams": function (aoData) {
            aoData.push(
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/Wallet/Listdata",
        "aaSorting": [[0, "desc"]]
    });
}




wallet.showDelete = function (id) {
    $('#modal-confirm').modal("show");
    $('#action-confirm').attr('onclick', 'wallet.actionDelete(' + id + ')');
}

wallet.actionDelete = function (id) {
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Wallet/delete",
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                wallet.dataTable();
                $('#modal-confirm').modal("hide");
                ns_master.toastr("Thực hiện thành công", "success");
            }

        }
    });
}


wallet.showAdd = function () {
    $('#show-edit').modal("show");
    $('#show-edit .modal-title').html("Tạo mới");
    $('#action-edit').attr('onclick', 'wallet.add()');
    $('#edit-active .modal-title').html("Thêm");
}

wallet.add = function () {
    var title = $('input[name="title"]').val();
    if (!title) {
        ns_master.toastr("Vui lòng điền tên wallet", "error");
        $('input[name="title"]').focus();
        return;
    }

    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Wallet/add",
        dataType: "json",
        data: {
            title: title,
            coin_id: $('#coin_id').val()
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "success");
            } else {
                wallet.dataTable();
                $('#show-edit').modal("hide");
                ns_master.toastr("Thực hiện thành công", "success");
            }
           
        }
    });
}
wallet.actionEdit = function (id) {
    var title = $('input[name="title"]').val();
    if (!title) {
        ns_master.toastr("Vui lòng điền tên wallet", "error");
        $('input[name="title"]').focus();
        return;
    }


    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Wallet/edit",
        dataType: "json",
        data: {
            title: title,
            id: id,
            coin_id: $('#coin_id').val()
        },
        success: function (data) {
            $.fancybox.hideActivity();
            wallet.dataTable();
            $('#show-edit').modal("hide");
            ns_master.toastr("Thực hiện thành công", "success");
        }
    });
}

wallet.showEdit = function (id) {
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Wallet/detail",
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            $('input[name="title"]').val(data.item.name);
            $('#coin_id').val(data.item.coin_id);
            $('#show-edit').modal("show");
            $('#show-edit .modal-title').html("Sửa");
            $('#action-edit').attr('onclick', 'wallet.actionEdit(' + id + ')');
        }
    });

}




$(function () {
    wallet.dataTable();
});