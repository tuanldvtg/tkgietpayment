﻿define({
    data: {
        form: {
            money:"0"
        },
        balance: 0,
        balance_out:0
    },

    // Methods
    methods: {
        formatMonney : function (num) {
            var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            return n;
        },
        onCoinPress: function () {
            var price = rwithdrawuser.form.money;
            var a_pay = price.split(',').join('');
            rwithdrawuser.form.money = rwithdrawuser.formatMonney(a_pay);
        }, 

        send: function () {
            var price = rwithdrawuser.form.money;
            var a_pay = price.split(',').join('');
            f_money = parseFloat(a_pay);
            if (f_money <= 0) {
                ns_master.toastr("Số tiền không được < 0", "error");
                return;
            }

            if (f_money == 0 || !price) {
                ns_master.toastr("Vui lòng điền số tiền muốn rút","error");
                return;
            }
            var wallet_id = $('#wallet_id').val();
            if (!wallet_id) {
                ns_master.toastr("Vui lòng chọn ví muốn rút", "error");
                return;
            }
            if (!rwithdrawuser.form.note) {
                ns_master.toastr("Vui lòng điền nội dung yêu cầu", "error");
                return;
            }

            $.fancybox.showActivity();
            $.ajax({
                type: 'POST',
                url: "/rwithdrawuserAPI/addrwithdrawuser",
                dataType: "json",
                data: {
                    wallet_id: $('#wallet_id').val(),
                    money: f_money,
                    note: rwithdrawuser.form.note
                },
                success: function (data) {
                    $.fancybox.hideActivity();
                    $('#create-rwithdrawuser').modal("hide");
                    ns_master.toastr("Thực hiện thành công", "success");
                    withdrawal_user.dataTable("begin");
                }
            });

        }

    },

});