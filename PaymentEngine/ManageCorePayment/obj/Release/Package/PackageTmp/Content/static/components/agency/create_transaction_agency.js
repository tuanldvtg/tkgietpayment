﻿define({
    data: {
        userid: 0,
        loading: false,
        toast: {
            error: false,
            msg: ""
        },
        items: [],
        form: {
            gate: 0,
            cash: 0
        },
        wallets: [],
        ioratio: [],
        wallet_name: "",
        money: "",
        present: {
            btn_title: "Nạp tiền",
            title: "Nạp tiền cho user"
        },
        cashout: 0,
        view_cashout:0,
        total: 0,
        view_total :0,
        selected_user: [],
        find_user: [],
        isLoadingSreachUser: false,
        isError: false,
        view_money: 0,
        view_cashIO: [],
        rest_view: 0
    },

    // Methods
    methods: {
        removeUserSelect: function (index) {
            create_transaction.selected_user.splice(index, 1);
            create_transaction.changeTotal();
        },
        onSelectGate: function () {
            var wallet_id = $('#wallet_id').val();
            if (wallet_id == '0') {
                return;
            }
            $.post('/AgencyCreatePayAPI/GetRateIO',
                {
                    wallet_id: wallet_id
                }, function (json) {
                    if (json.error) {
                        ns_master.toastr(json.msg, "error");
                        return;
                    }
                    iorates = json.items;
                    create_transaction.wallet_name = json.wallet_name;
                    create_transaction.money = json.money;
                    create_transaction.view_money = create_transaction.formatMonney(json.money);
                    create_transaction.rest_view = create_transaction.formatMonney(json.money);

                    create_transaction.ioratio = iorates.sort(function (a, b) {
                        if (a.cash_in < b.cash_in)
                            return -1;
                        if (a.cash_in > b.cash_in)
                            return 1;
                        return 0;
                    });

                    create_transaction.view_cashIO = [];
                    iorates.forEach(function (i) {
                        create_transaction.view_cashIO.push({
                            'id': i.id,
                            'cash_in': create_transaction.formatMonney(i.cash_in),
                            'cash_out': create_transaction.formatMonney(i.cash_out),
                        });
                    });

                    create_transaction.cashout = 0;
                    create_transaction.view_cashout = 0;
                    create_transaction.total = 0;
                    create_transaction.view_total = 0;
                    create_transaction.form.cash = 0;

                    //create_transaction.view_money = create_transaction.money;
                }, 'json');
        },
        formatMonney: function (num) {
            var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            return n;
        },
        changeTotal: function () {
            var count_user = create_transaction.selected_user.length;

            s_money = create_transaction.form.cash;
            s_money = s_money.toString();
            var a_money = s_money.split(",");
            
            s_money = a_money.join("");

            if (parseFloat(s_money) <= 0) {
                $('.txt-coin-tran').addClass('error');
            }


            create_transaction.total = create_transaction.selected_user.length * parseFloat(s_money);
            var total_cashout = create_transaction.total;

            create_transaction.view_total = create_transaction.formatMonney(total_cashout);

            var money_rest = create_transaction.money - create_transaction.total;
            create_transaction.view_money = create_transaction.formatMonney(money_rest);
            if (create_transaction.total > create_transaction.money) {
                $('.txt-coin-tran').addClass('error');
            } else {
                $('.txt-coin-tran').removeClass('error');
            }
        },

        // Realtime change
        onCoinPress: function () {
            s_money = create_transaction.form.cash;
            var a_money = s_money.split(",");
            s_money = a_money.join("");
            create_transaction.form.cash = create_transaction.formatMonney(s_money);

            s_money = create_transaction.form.cash;
            var a_money = s_money.split(",");
            s_money = a_money.join("");
            
            input = parseFloat(s_money);

            if (input <= 0) {
                $('.txt-coin-tran').addClass('error');
            }

            //create_transaction.cashout = input;
            for (i = 0; i < create_transaction.ioratio.length; i++) {
                if (parseFloat(create_transaction.ioratio[i].cash_in) >= input) {
                    var a = create_transaction.ioratio[i].cash_in;
                    var b = create_transaction.ioratio[i].cash_out;
                    break;
                }
            }

            create_transaction.cashout = input / a * b;
            create_transaction.cashout = Math.round(create_transaction.cashout * 1000) / 1000;
            var cash_out = create_transaction.cashout;
            create_transaction.view_cashout = create_transaction.formatMonney(cash_out);

            if (create_transaction.cashout > create_transaction.money || isNaN(a)) {
                $('.txt-coin-tran').addClass('error');
                return;
            } else {
                $('.txt-coin-tran').removeClass('error');
            }
            create_transaction.changeTotal();
        },
        // Submit it
        onSubmit: function () {
            if ($('#wallet_id').val() == '0') {
                ns_master.toastr("Vui lòng chọn ví muốn chuyển.", "error");
                return;
            }
            var cash_check = $('.txt-coin-tran').val();
            if (cash_check == "0" || !cash_check) {
                ns_master.toastr("Vui lòng điền số tiền chuyển", "error");
                return;
            }

            if ($('.txt-coin-tran').hasClass('error')) {
                ns_master.toastr("Số tiền giao dịch vượt quá mức cho phép.", "error");
                return;
            }

            var a_user = [];
            for (var i = 0; i < create_transaction.selected_user.length; i++) {
                a_user.push(create_transaction.selected_user[i].id);
            }
            var s_user = a_user.join();

            s_money = create_transaction.form.cash;
            var a_money = s_money.split(",");
            s_money = a_money.join("");

            $.fancybox.showActivity();
            $.post('/AgencyCreatePayAPI/sendSMSAgency',
                {
                    wallet_id: $('#wallet_id').val(),
                    cash: s_money,
                    userid: this.userid,
                    s_user: s_user,
                    content: $('#content').val()
                }, function (json) {
                    $.fancybox.hideActivity();
                    if (!json.error) {
                        $('#vueAppCreateTransaction').modal('hide');
                        if (json.sms == 0) {
                            ns_master.toastr("Thực hiện thành công", "success");
                        } else {
                            $('#show-edit').modal("show");
                        }
                       
                    } else {
                        ns_master.toastr(json.msg, "error");
                    }
                }, 'json');
            return;

        },
        onSubmitOTP: function () {
            if ($('#wallet_id').val() == '0') {
                ns_master.toastr("Vui lòng chọn ví muốn chuyển.", "error");
                return;
            }
            var a_user = [];
            for (var i = 0; i < create_transaction.selected_user.length; i++) {
                a_user.push(create_transaction.selected_user[i].id);
            }
            var s_user = a_user.join();
            $.fancybox.showActivity();
            $.post('/AgencyCreatePayAPI/TransferMoney',
                {
                    wallet_id: $('#wallet_id').val(),
                    cash: this.form.cash,
                    userid: this.userid,
                    code: $('.txt-otp').val(),
                    s_user: s_user,
                    content: $('#content').val()
                }, function (json) {
                    $.fancybox.hideActivity();
                    if (!json.error) {
                        $('#show-edit').modal("hide");
                        ns_master.toastr("Thực hiện thành công", "success");
                    } else {
                        ns_master.toastr(json.msg, "error");
                    }
                }, 'json');
        },
        onKeyUpSearchUser: function (event) {
            if (event.keyCode == 27) {
                return;
            }
            create_transaction.isLoadingSreachUser = true;
            $('.show-loading').show();
            $.post('/AgencyCreatePayAPI/SearchUsersSelect2',
                {
                    search: $('#users_selected').val()
                }, function (json) {
                    create_transaction.isLoadingSreachUser = false;
                    create_transaction.find_user = json.results;
                    for (var i = 0; i < create_transaction.find_user.length; i++) {
                        if (create_transaction.checkContainArray(create_transaction.selected_user, create_transaction.find_user[i])) {
                            create_transaction.find_user[i]['selected'] = true;
                        } else {
                            create_transaction.find_user[i]['selected'] = false;
                        }
                    }
                    $(".result-find-user-select").show();
                    $('.show-loading').hide();
                }, 'json');
        },

        selectUserFind: function (index) {
            var check_contans = create_transaction.checkContainArray(create_transaction.selected_user, create_transaction.find_user[index]);
            if (!check_contans) {
                create_transaction.selected_user.push({
                    "id": create_transaction.find_user[index].id,
                    "name": create_transaction.find_user[index].username
                });
            } else {
                
            }
            create_transaction.changeTotal();
            $(".result-find-user-select").hide();
        },

        checkContainArray: function (array1, obj) {
            for (var i = 0; i < array1.length; i++) {
                if (array1[i].id == obj.id) {
                    return true;
                }
            }
            return false;
        }

    },

    // Autoload
    created: function () {
        $(document).mouseup(function (e) {
            var container = $(".result-find-user-select");
            // if the target of the click isn't the container nor a descendant of the container
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.hide();
            }
        });

        document.addEventListener("keydown", function (event) {
            if (event.which == 27) {
                $(".result-find-user-select").hide();
            }
        });
    }
});