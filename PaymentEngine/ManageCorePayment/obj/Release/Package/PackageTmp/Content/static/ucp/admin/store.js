﻿agency = {};
agency.dataTable = function (change) {
    var page_start = $('.pagination .paginate_button.current > a').html();
    //var lengh = $('select[name="datatable-labelManagement_length"]').val();
    var page = (page_start - 1) * 10;
    if (!page) {
        page = 0;
    }
    if (change) {
        page = 0;
    }

    $('#datatable').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

        },
        "fnServerParams": function (aoData) {
            aoData.push(
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/Agency/Listdata",
        "aaSorting": [[0, "desc"]]
    });
}


agency.loadCashIORate = function () {
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Agency/CashIORate",
        dataType: "json",
        data: {
            gateid: $('#paymentgate_id').val()
        },
        success: function (data) {
            $.fancybox.hideActivity();
            var iorates = data.items;
            var view_iorates = data.items;
           iorates_list.iorates = iorates.sort(function (a, b) {
                if (a.cash_in < b.cash_in)
                    return -1;
                if (a.cash_in > b.cash_in)
                    return 1;
                return 0;
           });
           iorates_list.view_iorates = [];
           for (var i = 0; i < view_iorates.length; i++){
               //var item = view_iorates[i];
               //item['cash_in'] = agency.formatMonney(item['cash_in']);
               //item['cash_out'] = agency.formatMonney(item['cash_out']);
               iorates_list.view_iorates.push({
                   'cash_in': agency.formatMonney(view_iorates[i]['cash_in']),
                   'cash_out': agency.formatMonney(view_iorates[i]['cash_out'])
               });
           }


           agency.onChangePricePay();
        }
    });
}


agency.formatMonney = function (num) {
    var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    return n;
}

agency.onChangePricePay = function () {
    var price = $('.txt-coin-tran').val();
    var a_pay = price.split(',').join('');
    $('.txt-coin-tran').val(agency.formatMonney(a_pay));
    
    input = parseFloat(a_pay);
    for (i = 0; i < iorates_list.iorates.length; i++) {
        if (parseFloat(iorates_list.iorates[i].cash_in) >= input) {
            var a = iorates_list.iorates[i].cash_in;
            var b = iorates_list.iorates[i].cash_out;
            break;
        }
    }

    iorates_list.cashOut = input / a * b;
    iorates_list.cashOut = Math.round(iorates_list.cashOut * 1000) / 1000;
    iorates_list.view_cashOut = agency.formatMonney(iorates_list.cashOut);

    if (isNaN(a)) {
        $('.txt-coin-tran').addClass('error');
        return;
    } else {
        $('.txt-coin-tran').removeClass('error');
    }
    if (input <= 0) {
        $('.txt-coin-tran').addClass('error');
        return;
    }

}

agency.actionEdit = function (id) {
    var title = $('input[name="title"]').val();
    if (!title) {
        ns_master.toastr("Vui lòng điền tên Agency", "error");
        $('input[name="title"]').focus();
        return;
    }
    var username = $('input[name="username"]').val();
    if (!username) {
        ns_master.toastr("Vui lòng điền username ", "error");
        $('input[name="username"]').focus();
        return;
    }
    var phone = $('input[name="phone"]').val();
    if (!agency.phone) {
        ns_master.toastr("Vui lòng điền phone", "error");
        $('input[name="phone"]').focus();
        return;
    }

    var password = $('input[name="password"]').val();
    var location = $('input[name="location"]').val();

    var email = $('input[name="email"]').val();
    if (!email) {
        ns_master.toastr("Vui lòng điền email", "error");
        $('input[name="email"]').focus();
        return;
    }
    var merchant_id = $('#merchant_id').select2('val');
    if (merchant_id == null) {
        ns_master.toastr("Vui lòng chọn merchant sử dụng", "error");
        return;
    }
    var s_merchant = merchant_id.join(",");

    var appear = 1;
    var x = document.getElementById("isAppear").checked;
    if (x == false) {
        appear = 0;
    } else {
        appear = 1;
    }
    var orderId = $('input[name="orderId"]').val();
    if (!orderId || orderId == '') {
        orderId = 0;
    }
    
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Agency/edit",
        dataType: "json",
        data: {
            title: title,
            username: username,
            phone: agency.phone,
            password: password,
            email: agency.email,
            id: id,
            location: location,
            phone: agency.phone,
            merchant_id: s_merchant,
            appear: appear,
            orderId: orderId
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                agency.dataTable();
                $('#show-edit').modal("hide");
                ns_master.toastr("Thực hiện thành công", "success");
            }
        }
    });
}

agency.showEditActive = function (id, e) {
    $('#edit-active').modal("show");
    var avtice = $(e).attr('data-active');
    $('#edit-active type[type="radio"]').prop('checked', false);
    $('#' + avtice).prop('checked', true);
    $('#edit-active #action-confirm').attr("onclick", "agency.actionEditActive(" + id + ")");
    $('#edit-active .modal-title').html("Sửa");
}

agency.actionEditActive = function (id) {
    var status = "active";
    if ($('#active').is(":checked")) {
        status = "active";
    } else {
        status = "deactive";
    }
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Agency/postActive",
        dataType: "json",
        data: {
            id: id,
            status: status,
            note: $('.note').val()
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                agency.dataTable();
                $('#edit-active').modal("hide");
            }
            
        }
    });
}

agency.phone = "";
agency.email = "";

agency.showEdit = function (id) {
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Agency/detail",
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            $.fancybox.hideActivity();

            $('input[name="title"]').val(data.item.title);
            $('input[name="username"]').val(data.item.username);
            $('input[name="phone"]').val(data.item.phone_new);
            agency.phone = data.item.phone;
            $('input[name="email"]').val(data.item.email_new);
            agency.email = data.item.email;
            $('input[name="location"]').val(data.item.location);
            agency.email = data.item.email;
            $('input[name="password"]').val(data.item.password);
            $('#merchant_id').select2('val', data.merchant_id);

            $('#show-edit').modal("show");
            $('#show-edit .modal-title').html("Sửa");
            $('#action-edit').attr('onclick', 'agency.actionEdit(' + id + ')');
        }
    });

}

agency.showDelete = function (id) {
    $('#modal-confirm').modal("show");
    $('#action-confirm').attr('onclick', 'agency.actionDelete(' + id + ')');
}
agency.actionDelete = function (id) {
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Agency/delete",
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                agency.dataTable();
                $('#modal-confirm').modal("hide");
                ns_master.toastr("Thực hiện thành công", "success");
            }

        }
    });
}


agency.showEditType = function () {
    $('#show-payment-type').modal("show");
}

agency.showListCoin = function () {
    $('#list-coin').modal("show");
}

agency.showListType = function () {
    $('#list-type').modal("show");
}

agency.showAdd = function () {
    $('#show-edit').modal("show");
    $('#show-edit').find('input').val('');
    $('#show-edit .modal-title').html("Tạo mới");
    $('#action-edit').attr('onclick', 'agency.add()');
    $('#edit-active .modal-title').html("Thêm");
}

agency.onchangePhone = function (e) {
    agency.phone = $(e).val().trim();

}

agency.delelePayAgencyWalletForm = function (agency_id,id) {
    if (!confirm("Xác nhận")) {
        return;
    }

    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Agency/deleteWalletAgcency",
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (!data.error) {
                agency.dataTable();
                ns_master.toastr("Thực hiện thành công", "success");
            } else {
                ns_master.toastr(data.msg, "error");
            }
        }
    });

}

agency.shuffleAgency = function () {
    if (!confirm("Xác nhận")) {
        return;
    }
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Agency/shuffleAgency",
        dataType: "json",
        data: {
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (!data.error) {
                agency.dataTable();
                ns_master.toastr("Thực hiện thành công", "success");
            } else {
                ns_master.toastr(data.msg, "error");
            }

        }
    });

}


agency.add = function () {
    var title = $('input[name="title"]').val();
    if (!title) {
        ns_master.toastr("Vui lòng điền tên Agency", "error");
        $('input[name="title"]').focus();
        return;
    }
    var username = $('input[name="username"]').val();
    if (!username) {
        ns_master.toastr("Vui lòng điền username ", "error");
        $('input[name="username"]').focus();
        return;
    }
    var phone = $('input[name="phone"]').val();
    if (!agency.phone) {
        ns_master.toastr("Vui lòng điền phone", "error");
        $('input[name="phone"]').focus();
        return;
    }

    var password = $('input[name="password"]').val();
    if (!password) {
        ns_master.toastr("Vui lòng điền password", "error");
        $('input[name="password"]').focus();
        return;
    }

    var email = $('input[name="email"]').val();
    if (!email) {
        ns_master.toastr("Vui lòng điền email", "error");
        $('input[name="email"]').focus();
        return;
    }

    var merchant_id = $('#merchant_id').select2('val');
    if (merchant_id == null) {
        ns_master.toastr("Vui lòng chọn merchant sử dụng", "error");
        return;
    }
    var s_merchant = merchant_id.join(",");
    var location = $('input[name="location"]').val();
    var appear = 1;
    var x = document.getElementById("isAppear").checked;
    if (x == false) {
        appear = 0;
    } else {
        appear = 1;
    }
    var orderId = $('input[name="orderId"]').val();
    if (!orderId || orderId=='') {
        orderId = 0;
    }

    console.log("orderId: " + orderId + " phone: " + agency.phone);
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Agency/add",
        dataType: "json",
        data: {
            title: title,
            username: username,
            phone: agency.phone,
            password: password,
            email: email,
            merchant_id: s_merchant,
            location: location,
            appear: appear,
            orderId:orderId
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
            } else {
                agency.dataTable();
                $('#show-edit').modal("hide");
                ns_master.toastr("Thực hiện thành công", "success");
            }
           
        }
    });
}

agency.showCreateAgencyWalletForm = function (id) {
    agency.id = id;
    $('#dialogCreateAgencyWallet').modal("show");
};


showPayAgencyWalletForm = function (id,wallet) {
    agency.id = id;
    agency.wallet = wallet;
    $('#dialogPayAgencyWallet').modal("show");
    agency.onChangePricePay();
}

agency.doCreateAgencyWalletForm = function () {
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/Agency/CreateWallet",
        dataType: "json",
        data: {
            title: $('#dialogCreateAgencyWallet input[name=wallet_name]').val(),
            agencyid: agency.id
        },
        success: function (data) {
            $.fancybox.hideActivity();
            agency.dataTable();
            $('#dialogCreateAgencyWallet').modal("hide");
            ns_master.toastr("Thực hiện thành công", "success");
        }
    });
}
agency.showPayAgencyWalletForm = function (id, wallet) {
    agency.id = id;
    agency.wallet = wallet;
    agency.loadCashIORate();
    $('#dialogPayAgencyDialog').modal("show");
}
agency.doPayAgencyCash = function () {
    if ($('.txt-coin-tran').hasClass('error')) {
        ns_master.toastr("Số tiền không thể giao dịch", "error");
        return;
    }

    $.fancybox.showActivity();
    var cash = $('.txt-coin-tran').val();
    var a_cash = cash.split(',');
    var s_cash = a_cash.join("");
    $.ajax({
        type: 'POST',
        url: "/Agency/AdminTransferCash",
        dataType: "json",
        data: {
            cash: iorates_list.cashOut,
            agencyid: agency.id,
            walletid: agency.wallet,
            paymentgate_id: $('#paymentgate_id').val(),
            money_in: s_cash,
            content: $('.text-content').val()
        },
        success: function (data) {
            $.fancybox.hideActivity();
            agency.dataTable();
            $('#dialogPayAgencyDialog').modal("hide");
            ns_master.toastr("Thực hiện thành công", "success");
        }
    });
}

store=agency;

$(function () {
    iorates_list = new Vue({
        el: '#dialogPayAgencyDialog',
        data: {
            iorates: [],
            cashOut: 0,
            view_cashOut: 0,
            view_iorates:[]
        },
       
    });
    agency.dataTable();
    $('#merchant_id').select2();
});