qlkt = {};
//phân tích chuỗi email
function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}
qlkt.reloadDataTable = function () {
    // ds nhan vien
    $('#datatable-staffManagement').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bSort": false,
        "fnServerParams": function (aoData) {
            aoData.push(
                    {"name": "ajax", "value": 'staffManagement/service'},
            {"name": "action", "value": 'list_data'}
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/quanly_ketoan/"
    });
    //ds don vi
    $('#datatable-unitManagement').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bSort": false,
        "fnServerParams": function (aoData) {
            aoData.push(
                    {"name": "ajax", "value": 'staffManagement/service'},
            {"name": "action", "value": 'list_data_unit'}
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/quanly_ketoan/"
    });
    //ds bo phan
    $('#datatable-partManagement').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bSort": false,
        "fnServerParams": function (aoData) {
            aoData.push(
                    {"name": "ajax", "value": 'staffManagement/service'},
            {"name": "action", "value": 'list_data_part'}
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/quanly_ketoan/"
    });
    //ds chuc vu
    $('#datatable-positionManagement').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bSort": false,
        "fnServerParams": function (aoData) {
            aoData.push(
                    {"name": "ajax", "value": 'staffManagement/service'},
            {"name": "action", "value": 'list_data_position'}
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/quanly_ketoan/"
    });
    //ds chuc nang
    $('#datatable-functionManagement').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bSort": false,
        "fnServerParams": function (aoData) {
            aoData.push(
                    {"name": "ajax", "value": 'staffManagement/service'},
            {"name": "action", "value": 'list_data_function'}
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/quanly_ketoan/"
    });
    //ds trang thai
    $('#datatable-statusManagement').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bSort": false,
        "fnServerParams": function (aoData) {
            aoData.push(
                    {"name": "ajax", "value": 'staffManagement/service'},
            {"name": "action", "value": 'list_data_status'}
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/quanly_ketoan/"
    });

    $('#datatable-OtherMoneyManagement').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bSort": false,
        "fnServerParams": function (aoData) {
            aoData.push(
                    {"name": "ajax", "value": 'staffManagement/service'},
            {"name": "action", "value": 'list_data_other_money'}
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/quanly_ketoan/"
    });

    $('#datatable-InfoUnitManagement').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bSort": false,
        "fnServerParams": function (aoData) {
            aoData.push(
                    {"name": "ajax", "value": 'staffManagement/service'},
            {"name": "action", "value": 'list_data_info_unit'}
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/quanly_ketoan/"
    });

    $('#datatable-ListProManagement').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bSort": false,
        "fnServerParams": function (aoData) {
            aoData.push(
                    {"name": "ajax", "value": 'staffManagement/service'},
            {"name": "action", "value": 'list_data_list_product'}
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/quanly_ketoan/"
    });

    $('#datatable-CustomerManagement').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bSort": false,
        "fnServerParams": function (aoData) {
            aoData.push(
                    {"name": "ajax", "value": 'staffManagement/service'},
            {"name": "action", "value": 'list_data_list_customer'}
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/quanly_ketoan/"
    });

    $('#datatable-SupplierManagement').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bSort": false,
        "fnServerParams": function (aoData) {
            aoData.push(
                    {"name": "ajax", "value": 'staffManagement/service'},
            {"name": "action", "value": 'list_data_list_supplier'}
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/quanly_ketoan/"
    });

    $('#datatable-BuyPayManagement').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bSort": false,
        "fnServerParams": function (aoData) {
            aoData.push(
                    {"name": "ajax", "value": 'staffManagement/service'},
            {"name": "action", "value": 'list_data_list_buyandpay'}
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/quanly_ketoan/"
    });
}



//thêm thông tin đơn vị
qlkt.doAddNewUnit = function () {
    var ele = $('#tab_add_unit');
    qlkt.validateUnit = true;
    $('.ns-validate', ele).each(function () {
        if (!$(this).val()) {
            $(this).addClass('ns-validate-error');
            $('.box-body > .help-block.title', ele).html("Vui lòng điền đầy đủ thông tin").css('color', '#f72c2c');
            qlkt.validateUnit = false;
        } else {
            $(this).removeClass('ns-validate-error');
        }
    });
    if (!qlkt.validateUnit) {
        return;
    }
    if (!isValidEmailAddress($('#email', ele).val())) {
        $('.box-body > .help-block.title', ele).html("Email không đúng định dạng").css('color', '#f72c2c');
        return;
    }
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'AddUnit',
        'unit_code': $('#unit_code', ele).val(),
        'unit_name': $('#unit_name', ele).val(),
        'phone': $('#phone', ele).val(),
        'email': $('#email', ele).val(),
        'address': $('#address', ele).val(),
        'unit_description': $('#unit_description', ele).val(),
        'note': $('#note', ele).val()
    }, function (json) {
        if (json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            $('#unit_code', ele).val('');
            $('#unit_name', ele).val('');
            $('#phone', ele).val('');
            $('#email', ele).val('');
            $('#address', ele).val('');
            $('#unit_description', ele).val('');
            $('#note').val('');
            $('.box-body > .help-block.title', ele).html("Thêm mới đơn vị thành công!!!").css('color', '#5ec14d');
            setTimeout(function () {
                $('.box-body > .help-block.title', ele).html("");
            }, 3000);
        }
    });
}
qlkt.doAddInfoUnit = function () {
    var ele = $('#tab_add_info_unit');
    qlkt.validateInfoUnit = true;
    $('.ns-validate', ele).each(function () {
        if (!$(this).val()) {
            $(this).addClass('ns-validate-error');
            $('.box-body > .help-block.title', ele).html("Vui lòng điền đầy đủ thông tin").css('color', '#f72c2c');
            qlkt.validateInfoUnit = false;
        } else {
            $(this).removeClass('ns-validate-error');
        }
    });
    if (!qlkt.validateInfoUnit) {
        return;
    }
    if (!isValidEmailAddress($('#email', ele).val())) {
        $('.box-body > .help-block.title', ele).html("Email không đúng định dạng").css('color', '#f72c2c');
        return;
    }
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'AddInfoUnit',
        'creator': $('#creator', ele).val(),
        'unit_name': $('#unit_name', ele).val(),
        'address': $('#address', ele).val(),
        'phone': $('#phone', ele).val(),
        'email': $('#email', ele).val(),
        'fax': $('#fax', ele).val(),
        'web': $('#web', ele).val(),
        'director': $('#director', ele).val(),
        'start_time': $('#start_time', ele).val(),
        'end_time': $('#end_time', ele).val(),
        'status': $('#status', ele).val(),
        'note': $('#note', ele).val()
    }, function (json) {
        if (json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            $('#unit_name', ele).val('');
            $('#address', ele).val('');
            $('#phone', ele).val('');
            $('#email', ele).val('');
            $('#fax', ele).val('');
            $('#web', ele).val('');
            $('#director', ele).val('');
            $('#start_time', ele).val('');
            $('#end_time', ele).val('');
            $('#status', ele).val('');
            $('#note', ele).val('');
            $('.box-body > .help-block.title', ele).html("Thêm mới thông tin đơn vị thành công!!!").css('color', '#5ec14d');
            setTimeout(function () {
                $('.box-body > .help-block.title', ele).html("");
            }, 3000);
        }
    });

}
qlkt.doAddNewPart = function () {
    var ele = $('#tab_add_part');
    qlkt.validatePart = true;
    $('.ns-validate', ele).each(function () {
        if (!$(this).val()) {
            $(this).addClass('ns-validate-error');
            $('.box-body > .help-block.title', ele).html("Vui lòng điền đầy đủ thông tin").css('color', '#f72c2c');
            qlkt.validatePart = false;
        } else {
            $(this).removeClass('ns-validate-error');
        }
    });
    if (!qlkt.validatePart) {
        return;
    }
    if (!isValidEmailAddress($('#email', ele).val())) {
        $('.box-body > .help-block.title', ele).html("Email không đúng định dạng").css('color', '#f72c2c');
        return;
    }
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'AddPart',
        'part_code': $('#part_code', ele).val(),
        'part_name': $('#part_name', ele).val(),
        'phone': $('#phone', ele).val(),
        'email': $('#email', ele).val(),
        'address': $('#address', ele).val(),
        'part_description': $('#part_description', ele).val(),
        'note': $('#note', ele).val()
    }, function (json) {
        if (json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            $('#part_code', ele).val('');
            $('#part_name', ele).val('');
            $('#phone', ele).val('');
            $('#email', ele).val('');
            $('#address', ele).val('');
            $('#part_description', ele).val('');
            $('#note', ele).val('');
            $('.box-body > .help-block.title', ele).html("Thêm mới bộ phận thành công!!!").css('color', '#5ec14d');
            setTimeout(function () {
                $('.box-body > .help-block.title', ele).html("");
            }, 3000);
        }
    });
}
qlkt.doAddNewPosition = function () {
    var ele = $('#tab_add_position');
    qlkt.validatePosition = true;
    $('.ns-validate', ele).each(function () {
        if (!$(this).val()) {
            $(this).addClass('ns-validate-error');
            $('.box-body > .help-block.title', ele).html("Vui lòng điền đầy đủ thông tin").css('color', '#f72c2c');
            qlkt.validatePosition = false;
        } else {
            $(this).removeClass('ns-validate-error');
        }
    });
    if (!qlkt.validatePosition) {
        return;
    }
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'AddPosition',
        'position_code': $('#position_code', ele).val(),
        'position_name': $('#position_name', ele).val(),
        'position_description': $('#position_description', ele).val(),
        'note': $('#note', ele).val()
    }, function (json) {
        if (json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            $('#position_code', ele).val('');
            $('#position_name', ele).val('');
            $('#position_description', ele).val('');
            $('#note', ele).val('');
            $('.box-body > .help-block.title', ele).html("Thêm mới chức vụ thành công!!!").css('color', '#5ec14d');
            setTimeout(function () {
                $('.box-body > .help-block.title', ele).html("");
            }, 3000);
        }
    });
}
qlkt.doAddNewFunction = function () {
    var ele = $('#tab_add_function');
    qlkt.validateFunction = true;
    $('.ns-validate', ele).each(function () {
        if (!$(this).val()) {
            $(this).addClass('ns-validate-error');
            $('.box-body > .help-block.title', ele).html("Vui lòng điền đầy đủ thông tin").css('color', '#f72c2c');
            qlkt.validateFunction = false;
        } else {
            $(this).removeClass('ns-validate-error');
        }
    });
    if (!qlkt.validateFunction) {
        return;
    }
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'AddFunction',
        'function_code': $('#function_code', ele).val(),
        'function_name': $('#function_name', ele).val(),
        'function_description': $('#function_description', ele).val(),
        'note': $('#note', ele).val()
    }, function (json) {
        if (json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            $('#function_code', ele).val('');
            $('#function_name', ele).val('');
            $('#function_description', ele).val('');
            $('#note', ele).val('');
            $('.box-body > .help-block.title', ele).html("Thêm mới chức năng thành công!!!").css('color', '#5ec14d');
            setTimeout(function () {
                $('.box-body > .help-block.title', ele).html("");
            }, 3000);
        }
    });
}
qlkt.doAddNewStatus = function () {
    var ele = $('#tab_add_status');
    qlkt.validateStatus = true;
    $('.ns-validate', ele).each(function () {
        if (!$(this).val()) {
            $(this).addClass('ns-validate-error');
            $('.box-body > .help-block.title', ele).html("Vui lòng điền đầy đủ thông tin").css('color', '#f72c2c');
            qlkt.validateStatus = false;
        } else {
            $(this).removeClass('ns-validate-error');
        }
    });
    if (!qlkt.validateStatus) {
        return;
    }
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'AddStatus',
        'status_code': $('#status_code', ele).val(),
        'status_description': $('#status_description', ele).val(),
        'note': $('#note', ele).val()
    }, function (json) {
        if (json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            $('#status_code', ele).val('');
            $('#status_description', ele).val('');
            $('#note', ele).val('');
            $('.box-body > .help-block.title', ele).html("Thêm mới trạng thái thành công!!!").css('color', '#5ec14d');
            setTimeout(function () {
                $('.box-body > .help-block.title', ele).html("");
            }, 3000);
        }
    });
}
qlkt.doAddNewUser = function () {
    var ele = $('#tab_add_account');
    qlkt.validateUser = true;
    $('.ns-validate', ele).each(function () {
        if (!$(this).val()) {
            $(this).addClass('ns-validate-error');
            $('.box-body > .help-block.title', ele).html("Vui lòng điền đầy đủ thông tin").css('color', '#f72c2c');
            qlkt.validateUser = false;
        } else {
            $(this).removeClass('ns-validate-error');
        }
    });
    if (!qlkt.validateUser) {
        return;
    }
    if (!isValidEmailAddress($('#email', ele).val())) {
        $('#tab_add_user .box-body > .help-block.title').html("Email không đúng định dạng").css('color', '#f72c2c');
        return;
    }
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'AddUser',
        'fullname': $('#fullname', ele).val(),
        'username': $('#username', ele).val(),
        'password': $('#password', ele).val(),
        'email': $('#email', ele).val(),
        'phone': $('#phone', ele).val(),
        'unit': $('#choose-unit', ele).val(),
        'part': $('#choose-part', ele).val(),
        'position': $('#choose-position', ele).val(),
        'function': $('#choose-function', ele).val(),
        'status': $('#choose-status', ele).val()
    }, function (json) {
        if (json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            $('#fullname', ele).val('');
            $('#username', ele).val('');
            $('#password', ele).val('');
            $('#email', ele).val('');
            $('#phone', ele).val('');
//            $('#choose-unit', ele).val('');
//            $('#choose-part', ele).val('');
//            $('#choose-position', ele).val('');
//            $('#choose-function', ele).val('');
//            $('#choose-status', ele).val('');
            $('.box-body > .help-block.title', ele).html("Thêm mới tài khoản thành công!!!").css('color', '#5ec14d');
            setTimeout(function () {
                $('.box-body > .help-block.title', ele).html("");
            }, 3000);
        }
    });
}
qlkt.doAddOtherMoney = function () {
    var ele = $('#tab_add_other_money');
    qlkt.validateOm = true;
    $('.ns-validate', ele).each(function () {
        if (!$(this).val()) {
            $(this).addClass('ns-validate-error');
            $('.box-body > .help-block.title', ele).html("Vui lòng điền đầy đủ thông tin").css('color', '#f72c2c');
            qlkt.validateOm = false;
        } else {
            $(this).removeClass('ns-validate-error');
        }
    });
    if (!qlkt.validateOm) {
        return;
    }
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'AddOtherMoney',
        'creator': $('#creator', ele).val(),
        'money_name': $('#money_name', ele).val(),
        'total_revenue': ($('#total_revenue', ele).val()).split('.').join(''),
        'total_expenditure': ($('#total_expenditure', ele).val()).split('.').join(''),
        'explain_meaning': $('#explain_meaning', ele).val(),
        'note': $('#note', ele).val(),
        'status': $('#status', ele).val(),
        'latcher': $('#latcher', ele).val(),
        'time_latch': $('#time_latch', ele).val()
    }, function (json) {
        if (json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            $('#money_name', ele).val('');
            $('#total_revenue', ele).val('');
            $('#total_expenditure', ele).val('');
            $('#explain_meaning', ele).val('');
            $('#note', ele).val('');
            $('#status', ele).val('');
            $('#latcher', ele).val('');
            $('#time_latch', ele).val('');
            $('.box-body > .help-block.title', ele).html("Thêm tiền mới thành công!!!").css('color', '#5ec14d');
            setTimeout(function () {
                $('.box-body > .help-block.title', ele).html("");
            }, 3000);
        }
    });
}
qlkt.doAddProduct = function () {
    var ele = $('#tab_add_product');
    qlkt.validatePro = true;
    $('.ns-validate', ele).each(function () {
        if (!$(this).val()) {
            $(this).addClass('ns-validate-error');
            $('.box-body > .help-block.title', ele).html("Vui lòng điền đầy đủ thông tin").css('color', '#f72c2c');
            qlkt.validatePro = false;
        } else {
            $(this).removeClass('ns-validate-error');
        }
    });
    if (!qlkt.validatePro) {
        return;
    }
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'AddProduct',
        'creator': $('#creator', ele).val(),
        'product_name': $('#product_name', ele).val(),
        'sub_code': $('#sub_code', ele).val(),
        'dvt': ($('#dvt', ele).val()).split('.').join(''),
        'description': $('#choose-description', ele).val(),
        'cost_begin': ($('#cost_begin', ele).val()).split('.').join(''),
        'price_begin': ($('#price_begin', ele).val()).split('.').join(''),
        'number_begin': ($('#number_begin', ele).val()).split('.').join(''),
        'value_begin': ($('#value_begin', ele).val()).split('.').join(''),
        'number_inventory_begin': ($('#number_inventory_begin', ele).val()).split('.').join(''),
        'notification_begin': $('#notification_begin', ele).val(),
        'note_start': $('#note_start', ele).val(),
        'number_import_end': ($('#number_import_end', ele).val()).split('.').join(''),
        'value_import_end': ($('#value_import_end', ele).val()).split('.').join(''),
        'number_export_end': ($('#number_export_end', ele).val()).split('.').join(''),
        'value_export_end': ($('#value_export_end', ele).val()).split('.').join(''),
        'price_end': ($('#price_end', ele).val()).split('.').join(''),
        'number_inventory_end': ($('#number_inventory_end', ele).val()).split('.').join(''),
        'value_inventory_end': ($('#value_inventory_end', ele).val()).split('.').join(''),
        'revenue_end': ($('#revenue_end', ele).val()).split('.').join(''),
        'profit_end': ($('#profit_end', ele).val()).split('.').join(''),
        'note_end': $('#note_end', ele).val(),
        'status': $('#status', ele).val(),
        'latcher': $('#latcher', ele).val(),
        'time_latch': $('#time_latch', ele).val()
    }, function (json) {
        if (json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            $('#product_name', ele).val('');
            $('#sub_code', ele).val('');
            $('#dvt', ele).val('');
            $('#description', ele).val('');
            $('#cost_begin', ele).val('');
            $('#price_begin', ele).val('');
            $('#number_begin', ele).val('');
            $('#value_begin', ele).val('');
            $('#number_inventory_begin', ele).val('');
            $('#notification_begin', ele).val('');
            $('#note_start', ele).val('');
            $('#number_import_end', ele).val('');
            $('#value_import_end', ele).val('');
            $('#number_export_end', ele).val('');
            $('#value_export_end', ele).val('');
            $('#price_end', ele).val('');
            $('#number_inventory_end', ele).val('');
            $('#value_inventory_end', ele).val('');
            $('#revenue_end', ele).val('');
            $('#profit_end', ele).val('');
            $('#note_end', ele).val('');
            $('#status', ele).val('');
            $('#latcher', ele).val('');
            $('#time_latch', ele).val('');
            $('.box-body > .help-block.title', ele).html("Thêm tiền mới thành công!!!").css('color', '#5ec14d');
            setTimeout(function () {
                $('.box-body > .help-block.title', ele).html("");
            }, 3000);
        }
    });
}
qlkt.doAddCustomer = function () {
    var ele = $('#tab_list_customer');
    qlkt.validateCus = true;
    $('.ns-validate', ele).each(function () {
        if (!$(this).val()) {
            $(this).addClass('ns-validate-error');
            $('.box-body > .help-block.title', ele).html("Vui lòng điền đầy đủ thông tin").css('color', '#f72c2c');
            qlkt.validateCus = false;
        } else {
            $(this).removeClass('ns-validate-error');
        }
    });
    if (!qlkt.validateCus) {
        return;
    }
    if (!isValidEmailAddress($('#email', ele).val())) {
        $('.box-body > .help-block.title', ele).html("Email không đúng định dạng").css('color', '#f72c2c');
        return;
    }
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'AddCustomer',
        'creator': $('#creator', ele).val(),
        'store': $('#store', ele).val(),
        'customer_name': $('#customer_name', ele).val(),
        'phone_one': $('#phone_one', ele).val(),
        'phone_two': $('#phone_two', ele).val(),
        'address': $('#address', ele).val(),
        'district': $('#district', ele).val(),
        'city': $('#city', ele).val(),
        'email': $('#email', ele).val(),
        'tax_code': $('#tax_code', ele).val(),
        'debt_receivable_start': ($('#debt_receivable_start', ele).val()).split('.').join(''),
        'notifi': $('#notifi', ele).val(),
        'note_start': $('#note_start', ele).val(),
        'total_sale_money_end': ($('#total_sale_money_end', ele).val()).split('.').join(''),
        'collect_money_end': ($('#collect_money_end', ele).val()).split('.').join(''),
        'receivable_debt_end': ($('#receivable_debt_end', ele).val()).split('.').join(''),
        'revenue_end': ($('#revenue_end', ele).val()).split('.').join(''),
        'profit_end': ($('#profit_end', ele).val()).split('.').join(''),
        'note_end': $('#note_end', ele).val(),
        'status': $('#status', ele).val(),
        'latcher': $('#latcher', ele).val(),
        'time_latch': $('#time_latch', ele).val()
    }, function (json) {
        if (json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            $('#store', ele).val('');
            $('#customer_name', ele).val('');
            $('#phone_one', ele).val('');
            $('#phone_two', ele).val('');
            $('#address', ele).val('');
            $('#district', ele).val('');
            $('#city', ele).val('');
            $('#email', ele).val('');
            $('#tax_code', ele).val('');
            $('#debt_receivable_start', ele).val('');
            $('#notifi', ele).val('');
            $('#note_start', ele).val('');
            $('#total_sale_money_end', ele).val('');
            $('#collect_money_end', ele).val('');
            $('#receivable_debt_end', ele).val('');
            $('#revenue_end', ele).val('');
            $('#profit_end', ele).val('');
            $('#note_end', ele).val('');
            $('#status', ele).val('');
            $('#latcher', ele).val('');
            $('#time_latch', ele).val('');
            $('.box-body > .help-block.title', ele).html("Thêm tiền mới thành công!!!").css('color', '#5ec14d');
            setTimeout(function () {
                $('.box-body > .help-block.title', ele).html("");
            }, 3000);
        }
    });
}
qlkt.doAddSupplier = function () {
    var ele = $('#tab_add_supplier');
    qlkt.validateSup = true;
    $('.ns-validate', ele).each(function () {
        if (!$(this).val()) {
            $(this).addClass('ns-validate-error');
            $('.box-body > .help-block.title', ele).html("Vui lòng điền đầy đủ thông tin").css('color', '#f72c2c');
            qlkt.validateSup = false;
        } else {
            $(this).removeClass('ns-validate-error');
        }
    });
    if (!qlkt.validateSup) {
        return;
    }
    if (!isValidEmailAddress($('#email', ele).val())) {
        $('.box-body > .help-block.title', ele).html("Email không đúng định dạng").css('color', '#f72c2c');
        return;
    }
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'AddSupplier',
        'creator': $('#creator', ele).val(),
        'type_supply': $('#type_supply', ele).val(),
        'name_suppply': $('#name_suppply', ele).val(),
        'phone_one': $('#phone_one', ele).val(),
        'phone_two': $('#phone_two', ele).val(),
        'address': $('#address', ele).val(),
        'email': $('#email', ele).val(),
        'tax_code': $('#tax_code', ele).val(),
        'dept_pay_start': ($('#dept_pay_start', ele).val()).split('.').join(''),
        'caution': $('#caution', ele).val(),
        'note_start': $('#note_start', ele).val(),
        'total_purchase_price': ($('#total_purchase_price', ele).val()).split('.').join(''),
        'paid': ($('#paid', ele).val()).split('.').join(''),
        'dept_pay_end': ($('#dept_pay_end', ele).val()).split('.').join(''),
        'note_end': $('#note_end', ele).val(),
        'status': $('#status', ele).val(),
        'latcher': $('#latcher', ele).val(),
        'time_latch': $('#time_latch', ele).val()
    }, function (json) {
        if (json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            $('#type_supply', ele).val('');
            $('#name_suppply', ele).val('');
            $('#phone_one', ele).val('');
            $('#phone_two', ele).val('');
            $('#address', ele).val('');
            $('#email', ele).val('');
            $('#tax_code', ele).val('');
            $('#dept_pay_start', ele).val('');
            $('#caution', ele).val('');
            $('#note_start', ele).val('');
            $('#total_purchase_price', ele).val('');
            $('#paid', ele).val('');
            $('#dept_pay_end', ele).val('');
            $('#note_end', ele).val('');
            $('#status', ele).val('');
            $('#latcher', ele).val('');
            $('#time_latch', ele).val('');
            $('.box-body > .help-block.title', ele).html("Thêm tiền mới thành công!!!").css('color', '#5ec14d');
            setTimeout(function () {
                $('.box-body > .help-block.title', ele).html("");
            }, 3000);
        }
    });
}

qlkt.doAddBuyPay = function () {
    var ele = $('#tab_buy_pay');
    qlkt.validatebap = true;
    $('.ns-validate', ele).each(function () {
        if (!$(this).val()) {
            $(this).addClass('ns-validate-error');
            $('.box-body > .help-block.title', ele).html("Vui lòng điền đầy đủ thông tin").css('color', '#f72c2c');
            qlkt.validatebap = false;
        } else {
            $(this).removeClass('ns-validate-error');
        }
    });
    if (!qlkt.validatebap) {
        return;
    }

    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'AddBuyAndPay',
        'creator': $('#creator', ele).val(),
        'date': $('#date', ele).val(),
        'reciept': $('#reciept', ele).val(),
        'name_supplier': $('#choose-supplier', ele).val(),
        'type': $('#type', ele).val(),
        'name_product': $('#choose-product', ele).val(),
        'dvt': $('#dvt', ele).val(),
        'purchase_price_unit': ($('#purchase_price_unit', ele).val()).split('.').join(''),
        'number': ($('#number', ele).val()).split('.').join(''),
        'money': ($('#money', ele).val()).split('.').join(''),
        'discount': $('#discount', ele).val(),
        'discount_money': ($('#discount_money', ele).val()).split('.').join(''),
        'total_payment': ($('#total_payment', ele).val()).split('.').join(''),
        'amount_paid': ($('#amount_paid', ele).val()).split('.').join(''),
        'dept_pay': ($('#dept_pay', ele).val()).split('.').join(''),
        'note': $('#note', ele).val(),
        'notifi': $('#notifi', ele).val(),
        'status': $('#status', ele).val(),
        'latcher': $('#latcher', ele).val(),
        'time_latch': $('#time_latch', ele).val()
    }, function (json) {
        if (json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            $('#date', ele).val('');
            $('#reciept', ele).val('');
            $('#name_customer', ele).val('');
            $('#type', ele).val('');
            $('#name_product', ele).val('');
            $('#dvt', ele).val('');
            $('#purchase_price_unit', ele).val('');
            $('#number', ele).val('');
            $('#money', ele).val('');
            $('#discount', ele).val('');
            $('#discount_money', ele).val('');
            $('#total_payment', ele).val('');
            $('#amount_paid', ele).val('');
            $('#dept_pay', ele).val('');
            $('#note', ele).val('');
            $('#notifi', ele).val('');
            $('#status', ele).val('');
            $('#latcher', ele).val('');
            $('#time_latch', ele).val('');
            $('.box-body > .help-block.title', ele).html("Thêm tiền mới thành công!!!").css('color', '#5ec14d');
            setTimeout(function () {
                $('.box-body > .help-block.title', ele).html("");
            }, 3000);
        }
    });
}

qlkt.doAddSellCollect = function () {
    var ele = $('#tab_add_sell_collect');
    qlkt.validateSco = true;
    $('.ns-validate', ele).each(function () {
        if (!$(this).val()) {
            $(this).addClass('ns-validate-error');
            $('.box-body > .help-block.title', ele).html("Vui lòng điền đầy đủ thông tin").css('color', '#f72c2c');
            qlkt.validateSco = false;
        } else {
            $(this).removeClass('ns-validate-error');
        }
    });
    if (!qlkt.validateSco) {
        return;
    }

    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'AddSellAndCollect',
        'creator': $('#creator', ele).val(),
        'order_date': $('#order_date', ele).val(),
        'delivery_date': $('#delivery_date', ele).val(),
        'number_bills': $('#number_bills', ele).val(),
        'customer': $('#choose-customer', ele).val(),
        'phone': $('#choose-phone', ele).val(),
        'type': $('#type', ele).val(),
        'product': $('#choose-product', ele).val(),
        'dvt': $('#dvt', ele).val(),
        'price': ($('#price', ele).val()).split('.').join(''),
        'number': ($('#number', ele).val()).split('.').join(''),
        'money': ($('#money', ele).val()).split('.').join(''),
        'discount': $('#discount', ele).val(),
        'discount_money': ($('#discount_money', ele).val()).split('.').join(''),
        'total_payment': ($('#total_payment', ele).val()).split('.').join(''),
        'amount_collected': ($('#amount_collected', ele).val()).split('.').join(''),
        'debt_collect': ($('#debt_collect', ele).val()).split('.').join(''),
        'gross_profit': ($('#gross_profit', ele).val()).split('.').join(''),
        'note': $('#note', ele).val(),
        'notifi': $('#notifi', ele).val(),
        'status': $('#status', ele).val(),
        'latcher': $('#latcher', ele).val(),
        'time_latch': $('#time_latch', ele).val()
    }, function (json) {
        if (json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            $('#order_date', ele).val('');
            $('#delivery_date', ele).val('');
            $('#number_bills', ele).val('');
            $('#customer', ele).val('');
            $('#phone', ele).val('');
            $('#type', ele).val('');
            $('#product', ele).val('');
            $('#dvt', ele).val('');
            $('#price', ele).val('');
            $('#number', ele).val('');
            $('#money', ele).val('');
            $('#discount', ele).val('');
            $('#discount_money', ele).val('');
            $('#total_payment', ele).val('');
            $('#amount_collected', ele).val('');
            $('#debt_collect', ele).val('');
            $('#gross_profit', ele).val('');
            $('#note', ele).val('');
            $('#notifi', ele).val('');
            $('#status', ele).val('');
            $('#latcher', ele).val('');
            $('#time_latch', ele).val('');
            $('.box-body > .help-block.title', ele).html("Thêm tiền mới thành công!!!").css('color', '#5ec14d');
            setTimeout(function () {
                $('.box-body > .help-block.title', ele).html("");
            }, 3000);
        }
    });
}

//qlkt.doAddBuyPay = function () {
//    var ele = $('#modal-add-buyandpay');
//    qlkt.validatebap = true;
//    $('.ns-validate', ele).each(function () {
//        if (!$(this).val()) {
//            $(this).addClass('ns-validate-error');
//            $('.box-body > .help-block.title', ele).html("Vui lòng điền đầy đủ thông tin").css('color', '#f72c2c');
//            qlkt.validatebap = false;
//        } else {
//            $(this).removeClass('ns-validate-error');
//        }
//    });
//    if (!qlkt.validatebap) {
//        return;
//    }
//
//    $.post('/quanly_ketoan/', {
//        'ajax': 'staffManagement/service',
//        'action': 'AddBuyAndPay',
//        'creator': $('#creator', ele).val(),
//        'date': $('#date', ele).val(),
//        'reciept': $('#reciept', ele).val(),
//        'name_supplier': $('#choose-supplier', ele).val(),
//        'type': $('#type', ele).val(),
//        'name_product': $('#choose-product', ele).val(),
//        'dvt': $('#dvt', ele).val(),
//        'purchase_price_unit': ($('#purchase_price_unit', ele).val()).split('.').join(''),
//        'number': ($('#number', ele).val()).split('.').join(''),
//        'money': ($('#money', ele).val()).split('.').join(''),
//        'discount': $('#discount', ele).val(),
//        'discount_money': ($('#discount_money', ele).val()).split('.').join(''),
//        'total_payment': ($('#total_payment', ele).val()).split('.').join(''),
//        'amount_paid': ($('#amount_paid', ele).val()).split('.').join(''),
//        'dept_pay': ($('#dept_pay', ele).val()).split('.').join(''),
//        'note': $('#note', ele).val(),
//        'notifi': $('#notifi', ele).val(),
//        'status': $('#status', ele).val(),
//        'latcher': $('#latcher', ele).val(),
//        'time_latch': $('#time_latch', ele).val()
//    }, function (json) {
//        if (!json.error) {
//            alert(json.msg);
//            qlkt.reloadDataTable();
//        } else {
//            $('#date', ele).val('');
//            $('#reciept', ele).val('');
//            $('#name_customer', ele).val('');
//            $('#type', ele).val('');
//            $('#name_product', ele).val('');
//            $('#dvt', ele).val('');
//            $('#purchase_price_unit', ele).val('');
//            $('#number', ele).val('');
//            $('#money', ele).val('');
//            $('#discount', ele).val('');
//            $('#discount_money', ele).val('');
//            $('#total_payment', ele).val('');
//            $('#amount_paid', ele).val('');
//            $('#dept_pay', ele).val('');
//            $('#note', ele).val('');
//            $('#notifi', ele).val('');
//            $('#status', ele).val('');
//            $('#latcher', ele).val('');
//            $('#time_latch', ele).val('');
//            $('.box-body > .help-block.title', ele).html("Thêm tiền mới thành công!!!").css('color', '#5ec14d');
//            
//            setTimeout(function () {
//                $('.box-body > .help-block.title', ele).html("");
//            }, 3000);
//        }
//    });
//}

qlkt.showEdit = function (e) {

    $('#modal-edit-user .ns_validate').html('');
    $('#modal-edit-user .box-title').html('Sửa Tài Khoản');

    $.post('/quanly_ketoan/', {
        ajax: 'staffManagement/service',
        action: 'showEdit',
        id: e
    }, function (json) {
        if (json.error) {
            alert(json.msg);
        } else {
            $('#modal-edit-user .modal-content #fullname').val(json.fullname);
            $("#modal-edit-user .modal-content #email").val(json.email);
            $("#modal-edit-user .modal-content #phone").val(json.phone);
            $('#modal-edit').modal('show');
        }
    }, 'json');

}
qlkt.doEditUser = function (id) {
    var ele = $('#modal-edit-user');
    qlkt.validateEditUser = true;
    $('.ns-validate', ele).each(function () {
        if (!$(this).val()) {
            $(this).addClass('ns-validate-error');
            $('.box-body > .help-block.title', ele).html("Vui lòng điền đầy đủ thông tin").css('color', '#f72c2c');
            qlkt.validateEditUser = false;
        } else {
            $(this).removeClass('ns-validate-error');
        }
    });
    if (!qlkt.validateEditUser) {
        return;
    }
    if (!isValidEmailAddress($('#email', ele).val())) {
        $('.box-body > .help-block.title', ele).html("Email không đúng định dạng").css('color', '#f72c2c');
        return;
    }
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'edit_detail',
        'ev_id': id,
        'fullname': $('#fullname', ele).val(),
        'email': $('#email', ele).val(),
        'phone': $('#phone', ele).val(),
        'unit': $('#choose-unit', ele).val(),
        'part': $('#choose-part', ele).val(),
        'position': $('#choose-position', ele).val(),
        'function': $('#choose-function', ele).val(),
        'status': $('#choose-status', ele).val()
    }, function (json) {
        if (json.error) {
            alert(json.msg);
        } else {
            $('#fullname', ele).val('');
            $('#email', ele).val('');
            $('#phone', ele).val('');
            $('#choose-unit', ele).val('');
            $('#choose-part', ele).val('');
            $('#choose-position', ele).val('');
            $('#choose-function', ele).val('');
            $('#choose-status', ele).val('');
            $('.box-body > .help-block.title', ele).html("Thêm mới tài khoản thành công!!!").css('color', '#5ec14d');
            setTimeout(function () {
                $('.box-body > .help-block.title', ele).html("");
            }, 3000);
            qlkt.reloadDataTable();
        }
    });
};

qlkt.alertUser = function (id) {
    $('#modal-confirm .btn-primary').attr('onclick', 'qlkt.doRemoveUser(' + id + ')');
};
qlkt.doRemoveUser = function (id) {
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'doRemoveUser',
        'ev_id': id
    }, function (json) {
        if (!json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            alert('..');
        }
    }, 'json');
    return false;
};

qlkt.alertUnit = function (id) {
    $('#modal-confirm .btn-primary').attr('onclick', 'qlkt.doRemoveUnit(' + id + ')');
};
qlkt.doRemoveUnit = function (id) {
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'doRemoveUnit',
        'ev_id': id
    }, function (json) {
        if (!json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            alert('..');
        }
    }, 'json');
    return false;
};

qlkt.alertPart = function (id) {
    $('#modal-confirm .btn-primary').attr('onclick', 'qlkt.doRemovePart(' + id + ')');
};
qlkt.doRemovePart = function (id) {
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'doRemovePart',
        'ev_id': id
    }, function (json) {
        if (!json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            alert('..');
        }
    }, 'json');
    return false;
};

qlkt.alertPosition = function (id) {
    $('#modal-confirm .btn-primary').attr('onclick', 'qlkt.doRemovePosition(' + id + ')');
};
qlkt.doRemovePosition = function (id) {
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'doRemovePosition',
        'ev_id': id
    }, function (json) {
        if (!json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            alert('..');
        }
    }, 'json');
    return false;
};

qlkt.alertFunction = function (id) {
    $('#modal-confirm .btn-primary').attr('onclick', 'qlkt.doRemoveFunction(' + id + ')');
};
qlkt.doRemoveFunction = function (id) {
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'doRemoveFunction',
        'ev_id': id
    }, function (json) {
        if (!json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            alert('..');
        }
    }, 'json');
    return false;
};

qlkt.alertStatus = function (id) {
    $('#modal-confirm .btn-primary').attr('onclick', 'qlkt.doRemoveStatus(' + id + ')');
};
qlkt.doRemoveStatus = function (id) {
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'doRemoveStatus',
        'ev_id': id
    }, function (json) {
        if (!json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            alert('..');
        }
    }, 'json');
    return false;
};

qlkt.alertInfoUnit = function (id) {
    $('#modal-confirm .btn-primary').attr('onclick', 'qlkt.doRemoveInfoUnit(' + id + ')');
};
qlkt.doRemoveInfoUnit = function (id) {
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'doRemoveInfoUnit',
        'ev_id': id
    }, function (json) {
        if (!json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            alert('..');
        }
    }, 'json');
    return false;
};

qlkt.alertOtherMoney = function (id) {
    $('#modal-confirm .btn-primary').attr('onclick', 'qlkt.doRemoveOtherMoney(' + id + ')');
};
qlkt.doRemoveOtherMoney = function (id) {
    $.post('/quanly_ketoan/', {
        'ajax': 'staffManagement/service',
        'action': 'doRemoveOtherMoney',
        'ev_id': id
    }, function (json) {
        if (!json.error) {
            alert(json.msg);
            qlkt.reloadDataTable();
        } else {
            alert('..');
        }
    }, 'json');
    return false;
};

//----start thêm dấu . sau hàng nghìn----
qlkt.formatMonney = function (num) {
    var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
    return n;
}

qlkt.changePrice = function (e) {
    var value = $(e).val();
    var ele = value.split('.').join('');
    var money = qlkt.formatMonney(ele);
    $(e).val(money);
}
//----------------end-----------------
