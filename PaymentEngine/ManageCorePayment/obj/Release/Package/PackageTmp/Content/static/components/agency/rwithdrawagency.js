﻿define({
    data: {
        form: {
            money:"0"
        },
        balance: 0,
        balance_out: 0,
        cashIO: [],
        cashout: 0,
        view_cashout: 0,
        view_cashIO :[]
    },

    // Methods
    methods: {
        formatMonney : function (num) {
            var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            return n;
        },
        onCoinPress: function () {
            var price = rwithdrawagency.form.money;
            var a_pay = price.split(',').join('');
            rwithdrawagency.form.money = rwithdrawagency.formatMonney(a_pay);
            rwithdrawagency.resetTotal();
        },

        loadBalaceWalletid: function () {
            $.fancybox.showActivity();
            $.ajax({
                type: 'POST',
                url: "/RwithdrawagencyAPI/loadBalaceWalletid",
                dataType: "json",
                data: {
                    wallet_id: $('#wallet_id').val(),
                    
                },
                success: function (data) {
                    $.fancybox.hideActivity();
                    rwithdrawagency.balance = rwithdrawagency.formatMonney(data.balance);
                    rwithdrawagency.resetTotal();
                }
            });
        },
        loadPaymentGateId: function () {
            $.fancybox.showActivity();
            $.ajax({
                type: 'POST',
                url: "/RwithdrawagencyAPI/loadPaymentGateId",
                dataType: "json",
                data: {
                    wallet_id: $('#wallet_id').val(),
                    paymentGateid: $('#paymentgate_id').val()

                },
                success: function (data) {
                    $.fancybox.hideActivity();
                    if (data.error) {
                        ns_master.toastr(data.msg, "error");
                        rwithdrawagency.loadBalaceWalletid();
                        return;
                    }
                   
                    rwithdrawagency.balance = rwithdrawagency.formatMonney(data.money);
                    iorates = data.items;
                    rwithdrawagency.cashIO = iorates.sort(function (a, b) {
                        if (a.cash_in < b.cash_in)
                            return -1;
                        if (a.cash_in > b.cash_in)
                            return 1;
                        return 0;
                    });

                    iorates.forEach(function (i) {
                        rwithdrawagency.view_cashIO.push({
                            'id': i.id,
                            'cash_in': rwithdrawagency.formatMonney(i.cash_in),
                            'cash_out': rwithdrawagency.formatMonney(i.cash_out),
                        });
                    });

                    rwithdrawagency.resetTotal();
                }
            });
        },

        resetTotal: function () {
            var price = rwithdrawagency.form.money;
            var a_pay = price.split(',').join('');
            f_money = parseFloat(a_pay); 

            var price = rwithdrawagency.balance;
            var a_pay = price.split(',').join('');
            f_balance = parseFloat(a_pay);
            if (f_balance <= 0) {
                $('#money').addClass("error");
                return;
            }

            input = f_money;
            //create_transaction.cashout = input;
            for (i = 0; i < rwithdrawagency.cashIO.length; i++) {
                if (parseFloat(rwithdrawagency.cashIO[i].cash_in) >= input) {
                    var a = rwithdrawagency.cashIO[i].cash_in;
                    var b = rwithdrawagency.cashIO[i].cash_out;
                    break;
                }
            }

            rwithdrawagency.cashout = input / a * b;
            rwithdrawagency.cashout = Math.round(rwithdrawagency.cashout * 1000) / 1000;
            console.log(rwithdrawagency.cashout);
            if (isNaN(rwithdrawagency.cashout)) {
                $('#money').addClass("error");
                return;
            } else {
                $('#money').removeClass("error");
            }


            var balance_out = f_balance - f_money;
            if (balance_out < 0 || isNaN(balance_out)) {
                $('#money').addClass("error");
            } else {
                $('#money').removeClass("error");
            }
            rwithdrawagency.balance_out = rwithdrawagency.formatMonney(balance_out);
            rwithdrawagency.view_cashout = rwithdrawagency.formatMonney(rwithdrawagency.cashout);


        },

        send: function () {
            var price = rwithdrawagency.form.money;
            var a_pay = price.split(',').join('');
            f_money = parseFloat(a_pay);
            if (f_money <= 0) {
                ns_master.toastr("Số tiền không được < 0", "error");
                return;
            }
            if (f_money == 0 || !price) {
                ns_master.toastr("Vui lòng điền số tiền muốn rút","error");
                return;
            }
            if ($('#money').hasClass("error")) {
                ns_master.toastr("Số tiền vượt quá mức cho phép.", "error");
                return;
            }
            var wallet_id = $('#wallet_id').val();
            if (!wallet_id) {
                ns_master.toastr("Vui lòng chọn ví muốn rút", "error");
                return;
            }
            var paymengate_id = $('#paymentgate_id').val();
            if (!paymengate_id) {
                ns_master.toastr("Vui lòng chọn cổng rút", "error");
                return;
            }
            if ($('#money').hasClass('error')) {
                ns_master.toastr("Số tiền không được phép rút", "error");
                return;
            }
            if (!rwithdrawagency.form.note) {
                ns_master.toastr("Vui lòng điền nội dung yêu cầu", "error");
                return;
            }

            $.fancybox.showActivity();
            $.ajax({
                type: 'POST',
                url: "/RwithdrawagencyAPI/addRwithdrawagency",
                dataType: "json",
                data: {
                    wallet_id: $('#wallet_id').val(),
                    paymengate_id: paymengate_id,
                    money: f_money,
                    note: rwithdrawagency.form.note
                },
                success: function (data) {
                    $.fancybox.hideActivity();
                    $('#create-rwithdrawagency').modal("hide");
                    ns_master.toastr("Thực hiện thành công", "success");
                    withdrawal_agency.dataTable("begin");
                }
            });

        }

    },

});