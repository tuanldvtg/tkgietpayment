﻿withdrawal_user = {};
withdrawal_user.showCreate = function () {
    $('#create-rwithdrawuser').modal("show");
};

withdrawal_user.agreeConfirm = function (id) {
    $.fancybox.showActivity();
    $.post('/AccountingAPI/sendSMSRwith', {
        id: id

    }, function (json) {
        $.fancybox.hideActivity();
        // Show if error
        if (json.error) {
            ns_master.toastr(json.msg, "error");
        } else {
            $('#show-edit').modal("show");
            item_update = { "id": id };
            $('.phone-send').html(json.phone);
        }
    }, 'json');
    $('#action-edit').attr('onclick', 'withdrawal_user.agree(' + id + ')');
}

withdrawal_user.cancelConfirm = function (id) {
    $('#modal-confirm').modal("show");
    $('#action-confirm').attr('onclick', 'withdrawal_user.cancel(' + id + ')');
}
withdrawal_user.cancel = function (id) {
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/RwithdrawuserAccountingAPI/postActive",
        dataType: "json",
        data: {
            id: id,
            status: 2
        },
        success: function (data) {
            if (data.error) {
                ns_master.toastr(data.msg, "error");
                return;
            }
            $.fancybox.hideActivity();
            withdrawal_user.dataTable();
        }
    });
}

withdrawal_user.agree = function (id) {
    $.fancybox.showActivity();
    $.ajax({
        type: 'POST',
        url: "/RwithdrawuserAccountingAPI/postActive",
        dataType: "json",
        data: {
            id: id,
            status: 3,
            code: $('.txt-otp').val()
        },
        success: function (data) {
            $.fancybox.hideActivity();
            if (data.error) {
                ns_master.toastr(data.msg, "error");
                return;
            }
            
            withdrawal_user.dataTable();
            $('#show-edit').modal("hide");
        }
    });
}

withdrawal_user.dataTable = function (change) {
    var page_start = $('.paginate_button.current').html();
    //var lengh = $('select[name="datatable-labelManagement_length"]').val();
    var page = (page_start - 1) * 10;
    if (!page) {
        page = 0;
    }
    if (change) {
        page = 0;
    }
    var start_time = $('input[name="daterangepicker_start"]').val();
    var end_time = $('input[name="daterangepicker_end"]').val();
    $('#user-merchant').dataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
        tableTools: {
            "aButtons": []
        },
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "displayStart": page,
        "bInfo": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData.RowOrder);

        },
        "fnServerParams": function (aoData) { 
            aoData.push(
                { "name": "filter", "value": $('#filter').val() },
                { "name": "search_note", "value": $('#search_note').val() }
            );
        },
        "sServerMethod": "POST",
        "sAjaxSource": "/RwithdrawuserAccountingAPI/listdata",
        "aaSorting": [[0, "desc"]]
    });
};

$(function () {
    withdrawal_user.dataTable();

});

