﻿define({
    data: {
        toast: {
            error: false,
            msg: ""
        },
        present: {
            title: "Tạo mới cổng",
            btn_title: "Tạo mới cổng"
        },
        items: [],
        items_post: [],
        form: {
            merchant: 0,
            wallet_in: 0
        },
        wallets: [],
        merchants: [],
        manager_wallets:[]
    },
    // Methods
    methods: {

        // Btn add rule click
        addRule: function () {
            this.items.push({
                "cash_in": this.item.cash_in,
                "cash_out": this.item.cash_out
            });
            this.item.cash_in = "";
            this.item.cash_out = "";
        },
        formatMonney: function (num) {

            var n = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            return n;
        },
        onCoinPressIn: function () {
            s_money = this.item.cash_in;
            var a_money = s_money.split(",");
            s_money = a_money.join("");

            this.item.cash_in = this.formatMonney(s_money);
        },
        onCoinPressOut: function () {
            s_money = this.item.cash_out;
            var a_money = s_money.split(",");
            s_money = a_money.join("");

            this.item.cash_out = this.formatMonney(s_money);
        },

        // Remove rule
        removeValueCoin: function (index) {
            console.log(index);
            this.items.splice(index, 1);
        },

        // On merchant select
        onMerchantSelected: function () {
            comp = this;
            $.post("/AgencyPaymentGateAPI/WalletByMerchant", {
                merchant_id: this.form.merchant
            }, function (json) {
                if (!json.error) {
                    comp.wallets = json.items;
                }
            }, 'json');
        },

        // Submit - Do submit new wallet to system
        onSubmit: function () {
            if (!this.form.name) {
                ns_master.toastr("Vui lòng điền tên cổng thanh toán", "error");
                return;
            }
            if (this.form.wallet_in == 0) {
                ns_master.toastr("Vui lòng chọn ví nguồn", "error");
                return;
            }

            if (this.form.merchant == 0) {
                ns_master.toastr("Vui lòng chọn merchant", "error");
                return;
            }
            if (!this.form.wallet) {
                ns_master.toastr("Vui lòng chọn ví đích", "error");
                return;
            }


            if ($('#check_sms').is(':checked')) {
                sms = "1";
            } else {
                sms = "0";
            }
            var endpoint = (this.id) ? "/AgencyPaymentGateAPI/Update" : "/AgencyPaymentGateAPI/Create";

            create_payment_gate.items_post = [];
            var check_in_out = false;
            this.items.forEach(function (item) {
                var cash_in = item.cash_in;
                var a_cash_in = cash_in.split(",");
                s_cash_in = a_cash_in.join("");


                var cash_out = item.cash_out;
                var a_cash_out = cash_out.split(",");
                s_cash_out = a_cash_out.join("");

                if (parseFloat(s_cash_in) < parseFloat(s_cash_out)) {
                    check_in_out = true;
                }

                create_payment_gate.items_post.push({
                    "cash_in": s_cash_in,
                    "cash_out": s_cash_out
                });
            });
            if (create_payment_gate.items_post.length == 0) {
                ns_master.toastr("Vui lòng thêm tỷ giá", "error");
                return;
            }

            if (this.form.wallet != this.form.wallet_in) {
                ns_master.toastr("Không thể tạo được cổng thanh toán ví nguồn và ví đích khác nhau", "error");
                return;
            }

            if (check_in_out && (this.form.wallet == this.form.wallet_in)) {
                ns_master.toastr("Không thể cập nhật khi config đầu vào cổng thanh toán nhỏ hơn đầu ra.", "error");
                return;
            }
            $.post(endpoint, {
                name: this.form.name,
                wallet: this.form.wallet,
                merchantid:this.form.merchant,
                iorate: create_payment_gate.items_post,
                id: this.id,
                sms:sms,
                wallet_in: this.form.wallet_in,
            }, function (json) {
                if (json.error) {
                    alert(json.msg);
                } else {
                    // Close dialog
                    $('#vueAppPaymentGateInfo').modal('hide');
                    payment_gates.doUpdate();
                }
            }, 'json');
        }
    },

    // Created
    created: function () {
        comp = this;
        //console.log("Get merchants");
        $.post('/AgencyPaymentGateAPI/Merchants', this.form, function (json) {
            // Show if error
            if (json.error) {
                comp.toast = json;
            } else {
                comp.merchants = json.items;
            }
        }, 'json');
        // Gete all manager wallet manager_wallets
        $.post('/AgencyPaymentGateAPI/WalletsIn', this.form, function (json) {
            // Show if error
            if (json.error) {
                comp.toast = json;
            } else {
                comp.manager_wallets = json.items;
            }
        }, 'json');
    }
});