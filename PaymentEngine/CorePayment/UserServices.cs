﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography;
using CorePayment.Base;
using CorePayment.Response;
using System.Security;
using System.IdentityModel;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using CorePayment.Token;
using CorePayment.Model;
using RestSharp;
using RestSharp.Authenticators;
using CorePayment.Helper;
using System.Net;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace CorePayment
{


    public class UserServices : ServiceBase
    {
        public int app_id = 0;

        public UserServices(int id)
        {
            this.app_id = id;
        }
        //Query lấy cổng thanh toán
        public PaymentGate getPaymentGate(string code_gate)
        {
            PaymentGate paymentGateService = new PaymentGate();
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT TOP 1 id,name,code,wallet_in,wallet_out,status_active,sms FROM PaymentGate WHERE code ='" + code_gate + "'");

            int count_app = 0;
            while (responseQuery.reader.Read())
            {
                count_app++;
                paymentGateService.id = Int32.Parse(responseQuery.reader["id"].ToString());
                paymentGateService.name = responseQuery.reader["name"].ToString();
                paymentGateService.code = responseQuery.reader["code"].ToString();
                paymentGateService.wallet_in = Int32.Parse(responseQuery.reader["wallet_in"].ToString());
                paymentGateService.wallet_out = Int32.Parse(responseQuery.reader["wallet_out"].ToString());
                paymentGateService.status_active = responseQuery.reader["status_active"].ToString();
                paymentGateService.sms = Int32.Parse(responseQuery.reader["sms"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            return paymentGateService;
        }


        public TransactionServiceListResponse getListTransactions(UserModel user, string code_gate, string tag, int index)
        {
            List<Transactions> list_transactions = new List<Transactions>();
            ResponseQuery responseQuery = new ResponseQuery();
            PaymentGate paymentGateService = new PaymentGate();

            TransactionServiceListResponse transactionServiceListResponse = new TransactionServiceListResponse();

            //Kiểm tra cổng thanh toán có tồn tại 
            paymentGateService = this.getPaymentGate(code_gate);
            if (paymentGateService.id == 0)
            {
                this.appendError(2, "Cổng thanh toán không tồn tại ");
                transactionServiceListResponse.error = this.error;
                return transactionServiceListResponse;
            }

            responseQuery = this.query("SELECT " +
                "id,user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,(SELECT TOP 1 name FROM Wallet WHERE id =Transactions.wallet_id) as 'wallet_name' " +
                "FROM Transactions WHERE " +
                "user_id='" + user.id + "' " +
                "AND payment_gate='" + paymentGateService.id + "' " +
                "AND tag = '" + tag + "' " +
                "AND (transactions_type = '0' OR transactions_type IS NULL) " +
                "AND merchant_id ='" + this.app_id + "' " +
                "ORDER BY id DESC OFFSET " + index + " ROWS FETCH NEXT 10 ROWS ONLY");
            while (responseQuery.reader.Read())
            {
                Transactions transactions = new Transactions();
                transactions.id = Int32.Parse(responseQuery.reader["id"].ToString());
                transactions.user_id = Int32.Parse(responseQuery.reader["user_id"].ToString());
                transactions.merchant_id = Int32.Parse(responseQuery.reader["merchant_id"].ToString());
                transactions.payment_gate = Int32.Parse(responseQuery.reader["payment_gate"].ToString());
                transactions.wallet_id = Int32.Parse(responseQuery.reader["wallet_id"].ToString());
                transactions.balance = Double.Parse(responseQuery.reader["balance"].ToString());
                transactions.type = responseQuery.reader["type"].ToString();
                transactions.time_create = responseQuery.reader["time_creat"].ToString();
                transactions.value = Double.Parse(responseQuery.reader["value"].ToString());
                transactions.user_send = Int32.Parse(responseQuery.reader["user_send"].ToString());
                transactions.tag = responseQuery.reader["tag"].ToString();
                transactions.message = responseQuery.reader["message"].ToString();
                transactions.wallet_name = responseQuery.reader["wallet_name"].ToString();
                list_transactions.Add(transactions);
            }
            DatabaseService.Instance.getConnection().Close();
            transactionServiceListResponse.error.isError = false;
            transactionServiceListResponse.list_transactions = list_transactions;

            return transactionServiceListResponse;
        }

        public TransactionServiceResponse getTransactionStatisticUser(UserModel user, string code_gate, string tag)
        {
            TransactionServiceResponse transactionServiceResponse = new TransactionServiceResponse();

            ResponseQuery responseQuery = new ResponseQuery();

            PaymentGate paymentGateService = new PaymentGate();
            //Kiểm tra cổng thanh toán có tồn tại
            try
            {
                paymentGateService = this.getPaymentGate(code_gate);

            }
            catch (Exception ex)
            {
                this.appendError(1001, "Get công thanh toán xảy ra lỗi :" + ex.Message);
                transactionServiceResponse.error = this.error;
                return transactionServiceResponse;
            }
            if (paymentGateService.id == 0)
            {
                this.appendError(2, "Cổng thanh toán không tồn tại ");
                transactionServiceResponse.error = this.error;
                return transactionServiceResponse;
            }


            //Lấy dữ liệu giao dịch
            responseQuery = this.query("SELECT ISNULL(SUM(value),0) as 'value' FROM Transactions WHERE user_id='" + user.id + "' AND payment_gate='" + paymentGateService.id + "' AND tag = '" + tag + "' AND type = 'plus' AND merchant_id =" + this.app_id);
            //Xem query có lỗi ko
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                transactionServiceResponse.error = this.error;
                return transactionServiceResponse;
            }
            while (responseQuery.reader.Read())
            {
                transactionServiceResponse.plus = Double.Parse(responseQuery.reader["value"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();

            responseQuery = this.query("SELECT ISNULL(SUM(value),0) as 'value' FROM Transactions WHERE user_id='" + user.id + "' AND payment_gate='" + paymentGateService.id + "' AND tag = '" + tag + "' AND type = 'minus' AND merchant_id =" + this.app_id);
            //Xem query có lỗi ko
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                transactionServiceResponse.error = this.error;
                return transactionServiceResponse;
            }
            while (responseQuery.reader.Read())
            {
                transactionServiceResponse.minus = Double.Parse(responseQuery.reader["value"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();

            transactionServiceResponse.error.isError = false;
            return transactionServiceResponse;
        }

        private bool validateUsername(string username)
        {
            Regex regex = new Regex("^[a-z0-9_-]{3,24}$");
            return regex.IsMatch(username);
        }

        private bool validateEmail(string email)
        {
            Regex regex = new Regex("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
            return regex.IsMatch(email);
        }

        private bool validatePassword(string password)
        {
            if (password.Length > 5)
            {
                return true;
            }
            return false;
        }

        private bool validatePhone(string phone)
        {
            if (phone.Length > 11 || phone.Length < 10)
            {
                return false;
            }
            Regex regex = new Regex(@"^-*[0-9,\.?\-?\(?\)?\ ]+$");
            return regex.IsMatch(phone);
        }
        //Send verify phone
        private void _send_sms_otp(string phone, string code)
        {
            SendSMSHelper sendSMSHelper = new SendSMSHelper();
            string new_phone = phone.Substring(1, phone.Length - 1);
            string result = sendSMSHelper.send("+84" + phone, code, "Ma dang ky cua ban la: ");
        }

        public UserServiceResponse checkupdateProfile(UserModel user, string otp)
        {
            var userResponse = new UserServiceResponse();
            if (this.app_id == 0)
            {
                this.appendError(1001, "App chưa được chọn");
                userResponse.error = this.error;
                return userResponse;
            }
            ResponseQuery responseQuery = new ResponseQuery();

            if (user.phone != null)
            {
                if (!this.validatePhone(user.phone))
                {
                    this.appendError(2, "Điện thoại không hợp lệ");
                    userResponse.error = this.error;
                    return userResponse;
                }
            }

            //Kiểm tra username tồn tại
            responseQuery = this.query("SELECT TOP 1 id,username,merchant_id FROM Users WHERE username='" + user.username + "' AND id != " + user.id + " AND merchant_id =" + this.app_id);

            //Xem query có lỗi ko
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            int count_user = 0;
            int id = 0;
            while (responseQuery.reader.Read())
            {
                count_user++;
                id = Int32.Parse(responseQuery.reader["id"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            if (count_user > 0 && id != user.id)
            {
                this.appendError(1, "Username này đã được đăng ký");
                userResponse.error = this.error;
                return userResponse;
            }

            if (!user.phone.Trim().Equals(""))
            {

                //Kiểm tra phone tồn tại
                responseQuery = this.query("SELECT TOP 1 id,phone,merchant_id FROM Users WHERE phone='" + user.phone + "' AND id != " + user.id + " AND merchant_id =" + this.app_id);

                //Xem query có lỗi ko
                if (responseQuery.isError)
                {
                    this.appendError(1001, "Database error");
                    userResponse.error = this.error;
                    return userResponse;
                }
                int count_user_by_phone = 0;
                int id_by_phone = 0;
                while (responseQuery.reader.Read())
                {
                    count_user_by_phone++;
                    id_by_phone = Int32.Parse(responseQuery.reader["id"].ToString());
                }
                DatabaseService.Instance.getConnection().Close();
                if (count_user_by_phone > 0 && id_by_phone != user.id)
                {
                    this.appendError(1, "Số điện thoại này đã được đăng ký");
                    userResponse.error = this.error;
                    return userResponse;
                }

                //Xem query có lỗi ko
                if (responseQuery.isError)
                {
                    this.appendError(1001, "Database error");
                    userResponse.error = this.error;
                    return userResponse;
                }
                DatabaseService.Instance.getConnection().Close();
            }
            if (!user.password.Trim().Equals(""))
            {
                if (!validatePassword(user.password.Trim()))
                {
                    this.appendError(3, "Password không hợp lệ (>= 6 ký tự)");
                    userResponse.error = this.error;
                    return userResponse;
                }
            }
            string code = otp;
            if (code == null)
            {
                code = this.RandomString(6);
            }
            string querUpdate = "UPDATE Users SET code_sms='" + code + "' WHERE id='" + user.id + "'";
            responseQuery = this.query(querUpdate);
            DatabaseService.Instance.getConnection().Close();
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }

            if (otp == null)
            {
                string emailOrPhone = user.phone.Substring(1, user.phone.Length - 1);
                SendSMSHelper sendSMSHelper = new SendSMSHelper();
                try
                {
                    string result = sendSMSHelper.send("+84" + emailOrPhone, code, "Ma cap nhat thong tin cua ban la: ");
                }
                catch (Exception ex)
                {
                    this.appendError(1002, "Send otp lỗi " + ex.Message);
                    userResponse.error = this.error;
                    return userResponse;
                }
            }

            //Lấy info
            //responseQuery = this.query("SELECT TOP 1 * FROM Users WHERE id='" + user.id + "'");
            //if (responseQuery.isError)
            //{
            //    this.appendError(1001, "Database error");
            //    userResponse.error = this.error;
            //    return userResponse;
            //}

            //while (responseQuery.reader.Read())
            //{
            //    userResponse.error = this.error;
            //    userResponse.info.id = Int32.Parse(responseQuery.reader["id"].ToString());
            //    userResponse.info.username = responseQuery.reader["username"].ToString();
            //    userResponse.info.email = responseQuery.reader["email"].ToString();
            //    if (responseQuery.reader["status_active"].ToString() == "active")
            //    {
            //        userResponse.info.isActive = EnumUserModelActiveStatus.active;
            //    }
            //    else
            //    {
            //        userResponse.info.isActive = EnumUserModelActiveStatus.deactive;
            //    }
            //    userResponse.info.verify = Int32.Parse(responseQuery.reader["verify"].ToString());
            //    userResponse.info.phone = responseQuery.reader["phone"].ToString();
            //    userResponse.info.verify_phone = Int32.Parse(responseQuery.reader["verify_phone"].ToString());
            //    userResponse.info.code_sms = querUpdate;

            //}
            DatabaseService.Instance.getConnection().Close();
            return userResponse;
        }

        public UserServiceResponse updateProfile(UserModel user, string otp)
        {
            var userResponse = new UserServiceResponse();
            if (this.app_id == 0)
            {
                this.appendError(1001, "App chưa được chọn");
                userResponse.error = this.error;
                return userResponse;
            }
            ResponseQuery responseQuery = new ResponseQuery();

            //Check mã OTP
            responseQuery = this.query("SELECT TOP 1 id FROM Users WHERE id='" + user.id + "' AND code_sms='" + otp + "'");
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            int count = 0;
            while (responseQuery.reader.Read())
            {
                count++;
            }
            DatabaseService.Instance.getConnection().Close();
            if (count == 0)
            {
                this.appendError(1, "Mã OTP không chính xác");
                userResponse.error = this.error;
                return userResponse;
            }


            if (user.phone != null)
            {
                if (!this.validatePhone(user.phone))
                {
                    this.appendError(2, "Điện thoại không hợp lệ");
                    userResponse.error = this.error;
                    return userResponse;
                }
            }

            //Kiểm tra username tồn tại
            responseQuery = this.query("SELECT TOP 1 id,username,merchant_id FROM Users WHERE username='" + user.username + "' AND id != " + user.id + " AND merchant_id =" + this.app_id);

            //Xem query có lỗi ko
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            int count_user = 0;
            int id = 0;
            while (responseQuery.reader.Read())
            {
                count_user++;
                id = Int32.Parse(responseQuery.reader["id"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            if (count_user > 0 && id != user.id)
            {
                this.appendError(1, "Username này đã được đăng ký");
                userResponse.error = this.error;
                return userResponse;
            }
            string code = this.RandomString(6);
            if (!user.phone.Trim().Equals(""))
            {

                //Kiểm tra phone tồn tại
                responseQuery = this.query("SELECT TOP 1 id,phone,merchant_id FROM Users WHERE phone='" + user.phone + "' AND id != " + user.id + " AND merchant_id =" + this.app_id);

                //Xem query có lỗi ko
                if (responseQuery.isError)
                {
                    this.appendError(1001, "Database error");
                    userResponse.error = this.error;
                    return userResponse;
                }
                int count_user_by_phone = 0;
                int id_by_phone = 0;
                while (responseQuery.reader.Read())
                {
                    count_user_by_phone++;
                    id_by_phone = Int32.Parse(responseQuery.reader["id"].ToString());
                }
                DatabaseService.Instance.getConnection().Close();
                if (count_user_by_phone > 0 && id_by_phone != user.id)
                {
                    this.appendError(1, "Số điện thoại này đã được đăng ký");
                    userResponse.error = this.error;
                    return userResponse;
                }

                //Xem query có lỗi ko
                if (responseQuery.isError)
                {
                    this.appendError(1001, "Database error");
                    userResponse.error = this.error;
                    return userResponse;
                }
                DatabaseService.Instance.getConnection().Close();
            }
            string stringUpdatePass = "";

            if (!user.password.Equals(""))
            {
                stringUpdatePass = ",password ='" + CryptoHelper.MD5Hash(user.password.Trim(), user.id.ToString()) + "'";
            }

            string querUpdate = "UPDATE Users SET username='" + user.username + "',verify = 1,verify_phone = 1,phone='" + user.phone + "',code_sms='" + code + "'" + stringUpdatePass + "  WHERE id='" + user.id + "' AND merchant_id ='" + this.app_id + "'";
            responseQuery = this.query(querUpdate);
            DatabaseService.Instance.getConnection().Close();
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }

            //Lấy info
            responseQuery = this.query("SELECT TOP 1 id,username,email,status_active,verify,phone,verify_phone,code_sms,merchant_id,password FROM Users WHERE id=" + user.id);

            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }

            while (responseQuery.reader.Read())
            {
                userResponse.error = this.error;
                userResponse.info.id = Int32.Parse(responseQuery.reader["id"].ToString());
                userResponse.info.username = responseQuery.reader["username"].ToString();
                userResponse.info.email = responseQuery.reader["email"].ToString();
                if (responseQuery.reader["status_active"].ToString() == "active")
                {
                    userResponse.info.isActive = EnumUserModelActiveStatus.active;
                }
                else
                {
                    userResponse.info.isActive = EnumUserModelActiveStatus.deactive;
                }
                userResponse.info.verify = Int32.Parse(responseQuery.reader["verify"].ToString());
                userResponse.info.phone = responseQuery.reader["phone"].ToString();
                userResponse.info.verify_phone = Int32.Parse(responseQuery.reader["verify_phone"].ToString());
                userResponse.info.code_sms = querUpdate;
            }
            DatabaseService.Instance.getConnection().Close();
            return userResponse;
        }

        public MobileCardListResponse getListMobileCard()
        {
            MobileCardListResponse resp = new MobileCardListResponse();
            if (this.app_id == 0)
            {
                this.appendError(1001, "App chưa được chọn");
                resp.error = this.error;

                return resp;
            }
            ResponseQuery responseQuery = new ResponseQuery();

            List<MobileCardResponse> list = new List<MobileCardResponse>();
            list.Clear();
            List<MobileCardResponse> listNap = new List<MobileCardResponse>();
            listNap.Clear();
            List<MobileCardResponse> listRut = new List<MobileCardResponse>();
            listRut.Clear();
            
            string strQuery = "SELECT id,value," +
                "(SELECT TOP 1 name FROM Provider WHERE provider_id=Provider.id) as 'provider'," +
                "(SELECT TOP 1 shortline FROM Provider WHERE provider_id=Provider.id) as 'shortline'," +
                "(SELECT TOP 1 type_card FROM Provider WHERE provider_id=Provider.id) as 'type_card'," +
                "(SELECT TOP 1 type FROM Provider WHERE provider_id=Provider.id) as 'type' " +
                "FROM MobileCard";
            responseQuery = this.query(strQuery);
            //if (responseQuery.reader.HasRows)
            //{
            while (responseQuery.reader.Read())
            {
                MobileCardResponse mobileCard = new MobileCardResponse();
                mobileCard.id = Convert.ToInt32(responseQuery.reader["id"].ToString());
                mobileCard.value = Convert.ToDouble(responseQuery.reader["value"].ToString());
                mobileCard.provider = responseQuery.reader["provider"].ToString();
                mobileCard.shortLine = responseQuery.reader["shortline"].ToString();
                mobileCard.provider_typecard = Convert.ToInt32(responseQuery.reader["type_card"].ToString());
                mobileCard.type = Convert.ToInt32(responseQuery.reader["type"].ToString());

                list.Add(mobileCard);
            }
            //}
            DatabaseService.Instance.getConnection().Close();

            foreach (MobileCardResponse card in list)
            {
                if (card.type == 0)
                {
                    listNap.Add(card);
                }
                else
                {
                    listRut.Add(card);
                }
            }

            resp.lstNap = listNap;
            resp.lstRut = listRut;
            return resp;
        }

        public UserServiceResponse checkChangePassword(UserModel user, string password_old, string password_new, string otp)
        {
            var userResponse = new UserServiceResponse();
            if (this.app_id == 0)
            {
                this.appendError(1001, "App chưa được chọn");
                userResponse.error = this.error;

                return userResponse;
            }
            ResponseQuery responseQuery = new ResponseQuery();

            if (!validatePassword(password_new))
            {
                this.appendError(2, "Password không hợp lệ ( > 6 ký tự)");
                userResponse.error = this.error;
                return userResponse;
            }

            //Kiểm tra password cũ
            responseQuery = this.query("SELECT TOP 1 id FROM Users WHERE id='" + user.id + "' AND password='" + CryptoHelper.MD5Hash(password_old, user.id.ToString()) + "' AND merchant_id =" + this.app_id);
            //Xem query có lỗi ko
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            int count_user = 0;
            while (responseQuery.reader.Read())
            {
                count_user++;
            }
            DatabaseService.Instance.getConnection().Close();
            if (count_user == 0)
            {
                this.appendError(1001, "Password cũ không đúng.");
                userResponse.error = this.error;
                return userResponse;
            }


            string code = otp;
            if (code == null)
            {
                code = this.RandomString(6);
            }
            string querUpdate = "UPDATE Users SET code_sms='" + code + "' WHERE id='" + user.id + "'";
            responseQuery = this.query(querUpdate);
            DatabaseService.Instance.getConnection().Close();
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }


            if (otp == null)
            {
                string emailOrPhone = user.phone.Substring(1, user.phone.Length - 1);
                SendSMSHelper sendSMSHelper = new SendSMSHelper();
                try
                {
                    string result = sendSMSHelper.send("+84" + emailOrPhone, code, "Ma cap nhat thong tin cua ban la: ");
                }
                catch (Exception ex)
                {
                    this.appendError(1002, "Send otp lỗi " + ex.Message);
                    userResponse.error = this.error;
                    return userResponse;
                }
            }


            return userResponse;

        }

        public UserServiceResponse checkPassword(UserModel user, string password)
        {
            var userResponse = new UserServiceResponse();
            if (this.app_id == 0)
            {
                this.appendError(1001, "App chưa được chọn");
                userResponse.error = this.error;

                return userResponse;
            }
            ResponseQuery responseQuery = new ResponseQuery();


            //Kiểm tra password cũ
            responseQuery = this.query("SELECT TOP 1 id FROM Users WHERE id='" + user.id + "' AND password='" + CryptoHelper.MD5Hash(password, user.id.ToString()) + "' AND merchant_id =" + this.app_id);
            //Xem query có lỗi ko
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            int count_user = 0;
            while (responseQuery.reader.Read())
            {
                count_user++;
            }
            DatabaseService.Instance.getConnection().Close();
            if (count_user == 0)
            {
                this.appendError(1001, "Password không đúng.");
                userResponse.error = this.error;
                return userResponse;
            }

            return userResponse;

        }

        public UserServiceResponse changePassword(UserModel user, string password_old, string password_new, string otp)
        {
            var userResponse = new UserServiceResponse();
            if (this.app_id == 0)
            {
                this.appendError(1001, "App chưa được chọn");
                userResponse.error = this.error;

                return userResponse;
            }
            ResponseQuery responseQuery = new ResponseQuery();

            if (!validatePassword(password_new))
            {
                this.appendError(2, "Password không hợp lệ ( > 6 ký tự)");
                userResponse.error = this.error;
                return userResponse;
            }


            //Check mã OTP
            responseQuery = this.query("SELECT TOP 1 id FROM Users WHERE id='" + user.id + "' AND code_sms='" + otp + "'");
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            int count = 0;
            while (responseQuery.reader.Read())
            {
                count++;
            }
            DatabaseService.Instance.getConnection().Close();
            if (count == 0)
            {
                this.appendError(1, "Mã OTP không chính xác");
                userResponse.error = this.error;
                return userResponse;
            }

            //Kiểm tra password cũ
            responseQuery = this.query("SELECT TOP 1 id FROM Users WHERE id='" + user.id + "' AND password='" + CryptoHelper.MD5Hash(password_old, user.id.ToString()) + "' AND merchant_id =" + this.app_id);
            //Xem query có lỗi ko
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            int count_user = 0;
            while (responseQuery.reader.Read())
            {
                count_user++;
            }
            DatabaseService.Instance.getConnection().Close();
            if (count_user == 0)
            {
                this.appendError(1001, "Password cũ không đúng.");
                userResponse.error = this.error;
                return userResponse;
            }

            responseQuery = this.query("UPDATE Users SET password='" + CryptoHelper.MD5Hash(password_new, user.id.ToString()) + "' WHERE id = '" + user.id + "' AND merchant_id ='" + this.app_id + "'");
            DatabaseService.Instance.getConnection().Close();

            return userResponse;

        }

        public UserServiceResponse verifyPhone(UserModel user, string code)
        {
            var userResponse = new UserServiceResponse();
            ResponseQuery responseQuery = new ResponseQuery();
            //Check mã OTP
            responseQuery = this.query("SELECT TOP 1 id FROM Users WHERE id='" + user.id + "' AND code_sms='" + code + "'");
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            int count = 0;
            while (responseQuery.reader.Read())
            {
                count++;
            }
            DatabaseService.Instance.getConnection().Close();
            if (count == 0)
            {
                this.appendError(1, "Mã OTP không chính xác");
                userResponse.error = this.error;
                return userResponse;
            }

            //Update verify phone
            responseQuery = this.query("UPDATE Users SET verify_phone=1,verify=1 WHERE id='" + user.id + "'");
            //Xem query có lỗi ko
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            DatabaseService.Instance.getConnection().Close();

            //Lấy info
            responseQuery = this.query("SELECT TOP 1 id,username,email,status_active,verify,phone,verify_phone,code_sms,merchant_id,password FROM Users WHERE id=" + user.id);

            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }

            while (responseQuery.reader.Read())
            {
                userResponse.error = this.error;
                userResponse.info.id = Int32.Parse(responseQuery.reader["id"].ToString());
                userResponse.info.username = responseQuery.reader["username"].ToString();
                userResponse.info.email = responseQuery.reader["email"].ToString();
                if (responseQuery.reader["status_active"].ToString() == "active")
                {
                    userResponse.info.isActive = EnumUserModelActiveStatus.active;
                }
                else
                {
                    userResponse.info.isActive = EnumUserModelActiveStatus.deactive;
                }
                userResponse.info.verify = Int32.Parse(responseQuery.reader["verify"].ToString());
                userResponse.info.phone = responseQuery.reader["phone"].ToString().Trim();
            }
            DatabaseService.Instance.getConnection().Close();
            return userResponse;
        }

        //User login bằng username vs email
        public UserServiceResponse login(String username, String password)
        {
            var userResponse = new UserServiceResponse();
            if (this.app_id == 0)
            {
                this.appendError(1, "App chưa được chọn");
                userResponse.error = this.error;

                return userResponse;
            }

            ResponseQuery responseQuery = new ResponseQuery();
            string query_username = "SELECT TOP 1 id FROM Users WHERE username='" + username + "' AND merchant_id =" + this.app_id;

            responseQuery = this.query(query_username);
            //Xem query có lỗi ko
            if (responseQuery.isError)
            {
                this.appendError(1000, "Database error kiểm tra username,email,phone có tồn tại");
                userResponse.error = this.error;
                return userResponse;
            }
            int count_user = 0;
            UserModel userModel = new UserModel();
            while (responseQuery.reader.Read())
            {
                count_user++;
                userModel.id = Int32.Parse(responseQuery.reader["id"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            if (count_user > 0)
            {
                //Kiểm tra xem password có đúng không?
                string query_password = "SELECT TOP 1 id,username,email,status_active,verify,phone,verify_phone,code_sms,merchant_id,password FROM Users WHERE (username='" + username + "' AND password = '" + CryptoHelper.MD5Hash(password, userModel.id.ToString()) + "' AND merchant_id ='" + this.app_id + "') " +
                     "OR (email='" + username + "' AND password = '" + CryptoHelper.MD5Hash(password, userModel.id.ToString()) + "' AND merchant_id ='" + this.app_id + "') " +
                     "OR (phone='" + username + "' AND password = '" + CryptoHelper.MD5Hash(password, userModel.id.ToString()) + "' AND merchant_id ='" + this.app_id + "')";
                responseQuery = this.query(query_password);
                if (responseQuery.isError)
                {
                    this.appendError(1000, "Database error kiem tra password " + query_password);
                    userResponse.error = this.error;
                    return userResponse;
                }

                int count_user_password = 0;
                string status = "deactive";
                int verify = 0;
                while (responseQuery.reader.Read())
                {
                    count_user_password++;
                    userResponse.info.id = Int32.Parse(responseQuery.reader["id"].ToString());
                    userResponse.info.username = responseQuery.reader["username"].ToString();
                    userResponse.info.email = responseQuery.reader["email"].ToString();
                    if (responseQuery.reader["status_active"].ToString() == "active")
                    {
                        status = "active";
                        userResponse.error = this.error;
                        userResponse.info.isActive = EnumUserModelActiveStatus.active;
                    }
                    else
                    {
                        userResponse.info.isActive = EnumUserModelActiveStatus.deactive;
                    }
                    if (Int32.Parse(responseQuery.reader["verify"].ToString()) == 1)
                    {
                        verify = 1;
                    }
                    userResponse.info.verify = verify;
                    userResponse.info.verify_phone = Int32.Parse(responseQuery.reader["verify_phone"].ToString());
                    userResponse.info.phone = responseQuery.reader["phone"].ToString().Trim();
                }
                DatabaseService.Instance.getConnection().Close();

                if (count_user_password > 0)
                {
                    if (verify != 1)
                    {
                        this.appendError(2, "Tài khoản của bạn chưa verify");
                        userResponse.error = this.error;
                    }

                    if (status != "active")
                    {
                        this.appendError(3, "Tài khoản của bạn đã bị khóa");
                        userResponse.error = this.error;
                    }
                }
                else
                {
                    this.appendError(4, "Password không đúng");
                    userResponse.error = this.error;
                }
            }
            else
            {
                //Username không tồn tại
                this.appendError(5, "Username,email,phone chưa được đăng ký.");
                userResponse.error = this.error;
            }

            return userResponse;
        }


        private IRestResponse SendMailMessage(string email, string name, string code)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api",
                                            "key-e63b4cada39f62f60d9a6758226c7576");
            RestRequest request = new RestRequest();
            request.AddParameter("domain", "mail.newsoft.vn", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "XÁC NHẬN ĐĂNG KÝ <mailgun@mail.newsoft.vn>");
            request.AddParameter("to", email);
            request.AddParameter("subject", "Xác nhận đăng ký");
            EmailService emailService = new EmailService(email, name, code);
            request.AddParameter("html", emailService.email_registry);
            request.Method = Method.POST;
            return client.Execute(request);
        }

        //Get User Merchant
        public UserServiceResponse getUserByApp()
        {
            var userResponse = new UserServiceResponse();
            if (this.app_id == 0)
            {
                this.appendError(1001, "App chưa được chọn");
                userResponse.error = this.error;

                return userResponse;
            }
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT TOP 1 id,username FROM Merchant WHERE id = " + this.app_id);
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            string secret_key = "";
            while (responseQuery.reader.Read())
            {
                secret_key = responseQuery.reader["username"].ToString();
            }
            DatabaseService.Instance.getConnection().Close();


            responseQuery = this.query("SELECT TOP 1 id,username,email,status_active,verify,phone,verify_phone,code_sms,merchant_id,password FROM Users WHERE username = '" + secret_key + "'");
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            int count = 0;
            while (responseQuery.reader.Read())
            {
                count++;
                userResponse.error = this.error;
                userResponse.info.id = Int32.Parse(responseQuery.reader["id"].ToString());
                userResponse.info.username = responseQuery.reader["username"].ToString();
                userResponse.info.email = responseQuery.reader["email"].ToString();
                if (responseQuery.reader["status_active"].ToString() == "active")
                {
                    userResponse.info.isActive = EnumUserModelActiveStatus.active;
                }
                else
                {
                    userResponse.info.isActive = EnumUserModelActiveStatus.deactive;
                }
                userResponse.info.verify = Int32.Parse(responseQuery.reader["verify"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            if (count == 0)
            {
                this.appendError(1000, "User chưa được thêm");
                userResponse.error = this.error;
                return userResponse;
            }
            return userResponse;
        }

        public UserServiceResponse sendToSMSOTP(UserModel user)
        {
            var userResponse = new UserServiceResponse();
            ResponseQuery responseQuery = new ResponseQuery();
            //Kiểm tra đủ điều kiện gửi
            responseQuery = this.query("SELECT TOP 1 id,time_send_otp,count_send_otp FROM Users WHERE id = '" + user.id + "' ");
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            var now = DateTime.Now;
            int count_user = 0;
            string datetime = now.Year + "-" + now.Month + "-" + now.Day;
            int count_send = 0;
            while (responseQuery.reader.Read())
            {
                if (responseQuery.reader["time_send_otp"] != null)
                {
                    if (!responseQuery.reader["time_send_otp"].ToString().Trim().Equals(""))
                    {
                        datetime = responseQuery.reader["time_send_otp"].ToString();
                    }
                }

                if (responseQuery.reader["count_send_otp"] != null)
                {
                    if (!responseQuery.reader["count_send_otp"].ToString().Trim().Equals(""))
                    {
                        count_send = Int32.Parse(responseQuery.reader["count_send_otp"].ToString());
                    }
                }

                count_user++;
            }
            DatabaseService.Instance.getConnection().Close();
            if (count_user == 0)
            {
                this.appendError(1001, "User không tồn tại");
                userResponse.error = this.error;
                return userResponse;
            }
            string code = this.RandomString(6);

            DateTime myDate = DateTime.Parse(datetime);
            string[] date_time = datetime.Split('-');
            if (myDate.Year == now.Year && myDate.Month == now.Month && myDate.Day == now.Day)
            {
                if (count_send >= 2)
                {
                    this.appendError(1001, "Một ngày chỉ được gửi mã verify đối đa 2 lần");
                    userResponse.error = this.error;
                    return userResponse;
                }
                count_send++;
            }
            else
            {
                count_send = 1;
            }

            string format = "yyyy-MM-dd HH:mm:ss";
            responseQuery = this.query("UPDATE Users SET time_send_otp='" + now.ToString(format) + "',count_send_otp='" + count_send + "',code_sms='" + code + "' WHERE id ='" + user.id + "' ");
            DatabaseService.Instance.getConnection().Close();

            string emailOrPhone = user.phone.Substring(1, user.phone.Length - 1);
            SendSMSHelper sendSMSHelper = new SendSMSHelper();
            string result = sendSMSHelper.send("+84" + emailOrPhone, code, "Ma dang ky cua ban la: ");

            return userResponse;
        }


        //User đăng ký
        public UserServiceResponse registry(String username, String password, String emailOrPhone, string otp)
        {

            var userResponse = new UserServiceResponse();
            if (this.app_id == 0)
            {
                this.appendError(1001, "App chưa được chọn");
                userResponse.error = this.error;

                return userResponse;
            }

            if (!this.validateUsername(username))
            {
                this.appendError(1, "Username không hợp lệ (Không chứa ký tự đặc biểt > 3, < 24 ký tự)");
                userResponse.error = this.error;
                return userResponse;
            }

            if (!validatePassword(password))
            {
                this.appendError(2, "Password không hợp lệ ( > 6 ký tự)");
                userResponse.error = this.error;
                return userResponse;
            }

            Boolean is_email = this.validateEmail(emailOrPhone);
            Boolean is_phone = this.validatePhone(emailOrPhone);

            if (!is_phone && !is_email)
            {
                this.appendError(3, "Email or Phone không hợp lệ");
                userResponse.error = this.error;
                return userResponse;
            }


            //Kiểm tra username đã đăng ký chưa
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT TOP 1 id FROM Users WHERE username = '" + username + "' AND merchant_id = '" + this.app_id + "' ");
            if (responseQuery.isError)
            {
                this.appendError(1000, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            int count_user = 0;
            while (responseQuery.reader.Read())
            {
                count_user++;
            }
            DatabaseService.Instance.getConnection().Close();
            if (count_user > 0)
            {
                //User đã tồn tại
                this.appendError(4, "Username đã được đăng ký");
                userResponse.error = this.error;
                return userResponse;
            }
            string code = "";
            string result = "";
            if (!is_phone)
            {
                emailOrPhone = emailOrPhone.Trim();
                responseQuery = this.query("SELECT TOP 1 id FROM Users WHERE  (email = '" + emailOrPhone.Trim() + "' AND merchant_id = '" + this.app_id + "') ");
                if (responseQuery.isError)
                {
                    this.appendError(1000, "Database error");
                    userResponse.error = this.error;
                    return userResponse;
                }
                int count_user_email = 0;
                while (responseQuery.reader.Read())
                {
                    count_user_email++;
                }
                DatabaseService.Instance.getConnection().Close();
                if (count_user_email > 0)
                {
                    //User đã tồn tại
                    this.appendError(4, "Email đã được đăng ký");
                    userResponse.error = this.error;
                    return userResponse;
                }
                code = this.rendercode();
                responseQuery = this.queryInsert("INSERT INTO Users (username,password,email,merchant_id,status_active,code,verify) " +
                    "VALUES('" + username + "','" + password + "','" + emailOrPhone.Trim() + "'," + this.app_id + ",'active','" + code + "',0)");
                if (responseQuery.isError)
                {
                    this.appendError(1000, "Database error");
                    userResponse.error = this.error;

                    return userResponse;
                }
                DatabaseService.Instance.getConnection().Close();
                IRestResponse response_sendmail = this.SendMailMessage(emailOrPhone, username, code);

                if (response_sendmail.StatusCode != HttpStatusCode.OK)
                {
                    this.appendError(1001, response_sendmail.ErrorMessage);
                    userResponse.error = this.error;
                    return userResponse;
                }
            }
            else
            {
                code = otp;
                if (code == null)
                {
                    code = this.rendercode();
                }
                responseQuery = this.query("SELECT TOP 1 id FROM Users WHERE  (phone = '" + emailOrPhone.Trim() + "' AND merchant_id = '" + this.app_id + "') ");
                if (responseQuery.isError)
                {
                    this.appendError(1000, "Database error");
                    userResponse.error = this.error;
                    return userResponse;
                }
                int count_user_email = 0;
                while (responseQuery.reader.Read())
                {
                    count_user_email++;
                }
                DatabaseService.Instance.getConnection().Close();
                if (count_user_email > 0)
                {
                    //User đã tồn tại
                    this.appendError(5, "Số điện thoại đã được đăng ký");
                    userResponse.error = this.error;
                    return userResponse;
                }
                DateTime time = DateTime.Now;
                // Use current time
                string format = "yyyy-MM-dd HH:mm:ss";
                responseQuery = this.queryInsert("INSERT INTO Users (username,password,phone,merchant_id,status_active,code_sms,verify,time_send_otp,count_send_otp) " +
                    "VALUES('" + username + "','" + password + "','" + emailOrPhone.Trim() + "'," + this.app_id + ",'active','" + code + "',0,'" + time.ToString(format) + "',1)");
                if (responseQuery.isError)
                {
                    this.appendError(1000, "Database error");
                    userResponse.error = this.error;
                    return userResponse;
                }
                DatabaseService.Instance.getConnection().Close();

                if (otp == null)
                {
                    emailOrPhone = emailOrPhone.Substring(1, emailOrPhone.Length - 1);
                    SendSMSHelper sendSMSHelper = new SendSMSHelper();
                    try
                    {
                        result = sendSMSHelper.send("+84" + emailOrPhone, code, "Ma dang ky cua ban la: ");
                    }
                    catch (Exception ex)
                    {
                        this.appendError(1002, "Send otp lỗi " + ex.Message);
                        userResponse.error = this.error;
                        return userResponse;
                    }
                }


            }


            //Lấy info
            responseQuery = this.query("SELECT TOP 1 * FROM Users WHERE (email='" + emailOrPhone + "' OR phone = '0" + emailOrPhone + "') AND merchant_id = " + this.app_id);

            if (responseQuery.isError)
            {
                this.appendError(1000, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }

            while (responseQuery.reader.Read())
            {
                userResponse.error = this.error;
                userResponse.info.id = Int32.Parse(responseQuery.reader["id"].ToString());
                userResponse.info.username = responseQuery.reader["username"].ToString();
                userResponse.info.email = responseQuery.reader["email"].ToString();
                if (responseQuery.reader["status_active"].ToString() == "active")
                {
                    userResponse.info.isActive = EnumUserModelActiveStatus.active;
                }
                else
                {
                    userResponse.info.isActive = EnumUserModelActiveStatus.deactive;
                }
                userResponse.info.verify = Int32.Parse(responseQuery.reader["verify"].ToString());
                userResponse.info.phone = responseQuery.reader["phone"].ToString().Trim();
            }
            DatabaseService.Instance.getConnection().Close();

            responseQuery = this.query("UPDATE Users SET password='" + CryptoHelper.MD5Hash(password, userResponse.info.id.ToString()) + "' WHERE id = '" + userResponse.info.id + "' AND merchant_id ='" + this.app_id + "'");
            DatabaseService.Instance.getConnection().Close();

            userResponse.info.code_sms = result + "result SMS OTP";
            return userResponse;
        }


        public string rendercode()
        {
            string code = this.RandomString(6);
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT TOP 1 id FROM Users WHERE code ='" + code + "'");
            int id_user = 0;
            while (responseQuery.reader.Read())
            {
                id_user = Int32.Parse(responseQuery.reader["id"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();

            if (id_user != 0)
            {
                return this.rendercode();
            }
            return code;
        }

        private Random random = new Random();
        private string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        //Deactive
        public UserServiceResponse setUserDeactive(UserModel user)
        {
            var userResponse = new UserServiceResponse();
            if (this.app_id == 0)
            {
                this.appendError(1001, "App chưa được chọn");
                userResponse.error = this.error;

                return userResponse;
            }
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("UPDATE Users SET verify=0 WHERE email='" + user.email + "' AND merchant_id ='" + this.app_id + "'");
            //Xem query có lỗi ko
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            DatabaseService.Instance.getConnection().Close();
            responseQuery = this.query("SELECT TOP 1 * FROM Users WHERE (username='" + user.username + "' AND merchant_id ='" + this.app_id + "') OR (email='" + user.email + "' AND merchant_id =" + this.app_id + ")");

            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            while (responseQuery.reader.Read())
            {
                userResponse.error = this.error;
                userResponse.info.id = Int32.Parse(responseQuery.reader["id"].ToString());
                userResponse.info.username = responseQuery.reader["username"].ToString();
                userResponse.info.email = responseQuery.reader["email"].ToString();
                userResponse.info.isActive = EnumUserModelActiveStatus.deactive;
                userResponse.info.verify = Int32.Parse(responseQuery.reader["verify"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            return userResponse;
        }

        //Active
        public UserServiceResponse setUserActive(string code)
        {
            var userResponse = new UserServiceResponse();
            if (this.app_id == 0)
            {
                this.appendError(1001, "App chưa được chọn");
                userResponse.error = this.error;

                return userResponse;
            }
            ResponseQuery responseQuery = new ResponseQuery();
            //Tìm kiếm user từ code
            responseQuery = this.query("SELECT TOP 1 * FROM Users WHERE code='" + code + "'  AND merchant_id =" + this.app_id);

            UserModel user = new UserModel();
            while (responseQuery.reader.Read())
            {
                userResponse.error = this.error;
                user.id = Int32.Parse(responseQuery.reader["id"].ToString());
                user.username = responseQuery.reader["username"].ToString();
                user.email = responseQuery.reader["email"].ToString();
            }
            DatabaseService.Instance.getConnection().Close();
            if (user.id == 0)
            {
                this.appendError(1001, "Mã code không chính xác");
                userResponse.error = this.error;
                return userResponse;
            }

            responseQuery = this.query("UPDATE Users SET verify=1 WHERE email='" + user.email + "' AND merchant_id ='" + this.app_id + "'");
            //Xem query có lỗi ko
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            DatabaseService.Instance.getConnection().Close();
            responseQuery = this.query("SELECT TOP 1 * FROM Users WHERE (username='" + user.username + "' AND merchant_id =" + this.app_id + ") OR (email='" + user.email + "' AND merchant_id =" + this.app_id + ")");

            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            while (responseQuery.reader.Read())
            {
                userResponse.error = this.error;
                userResponse.info.id = Int32.Parse(responseQuery.reader["id"].ToString());
                userResponse.info.username = responseQuery.reader["username"].ToString();
                userResponse.info.email = responseQuery.reader["email"].ToString();
                userResponse.info.isActive = EnumUserModelActiveStatus.active;
                userResponse.info.verify = Int32.Parse(responseQuery.reader["verify"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            return userResponse;
        }

        //Lấy thông tin user
        public UserServiceResponse getInfo(int id)
        {
            var userResponse = new UserServiceResponse();
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT TOP 1 * FROM Users WHERE id=" + id + "");
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            while (responseQuery.reader.Read())
            {
                userResponse.error = this.error;
                userResponse.info.id = Int32.Parse(responseQuery.reader["id"].ToString());
                userResponse.info.username = responseQuery.reader["username"].ToString();
                userResponse.info.email = responseQuery.reader["email"].ToString();
                if (responseQuery.reader["status_active"].ToString() == "active")
                {
                    userResponse.info.isActive = EnumUserModelActiveStatus.active;
                }
                else
                {
                    userResponse.info.isActive = EnumUserModelActiveStatus.deactive;
                }
                userResponse.info.verify = Int32.Parse(responseQuery.reader["verify"].ToString());
                userResponse.info.verify_phone = Int32.Parse(responseQuery.reader["verify_phone"].ToString());
                userResponse.info.phone = responseQuery.reader["phone"].ToString().Trim();
            }
            DatabaseService.Instance.getConnection().Close();
            return userResponse;
        }

        public List<UserModel> getListUser(string kw)
        {
            List<UserModel> list_user = new List<UserModel>();
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT TOP 20 id,username,email,status_active,verify,phone,verify_phone,code_sms,merchant_id,password FROM Users WHERE (email='" + kw + "' OR username='" + kw + "' OR name LIKE '%" + kw + "%') AND merchant_id ='" + this.app_id + "' AND status_active = 'active'");
            while (responseQuery.reader.Read())
            {
                UserModel users = new UserModel();
                users.id = Int32.Parse(responseQuery.reader["id"].ToString());
                users.email = responseQuery.reader["email"].ToString();
                users.username = responseQuery.reader["username"].ToString();
                list_user.Add(users);
            }
            DatabaseService.Instance.getConnection().Close();
            return list_user;
        }

        public List<Agency> getListAgency(string kw)
        {
            List<Agency> list_user = new List<Agency>();
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT * FROM Agency WHERE status_active = 'active' AND is_appear=1 AND subid=0 ORDER BY orderId ASC");
            while (responseQuery.reader.Read())
            {
                Agency users = new Agency();
                users.id = Int32.Parse(responseQuery.reader["id"].ToString().Trim());
                users.email = responseQuery.reader["email"].ToString().Trim();
                users.title = responseQuery.reader["title"].ToString().Trim();
                users.phone = responseQuery.reader["phone"].ToString().Trim();
                users.location = responseQuery.reader["location"].ToString().Trim();
                users.orderId = Int32.Parse(responseQuery.reader["orderId"].ToString().Trim());
                list_user.Add(users);
            }
            DatabaseService.Instance.getConnection().Close();

            foreach (Agency users in list_user)
            {
                //Debug.WriteLine(users.id+"|"+ users.title);
                responseQuery = this.query("SELECT TOP 1 link FROM Agency_social WHERE agency_id=" + users.id + " ORDER BY ID ASC");
                if (responseQuery.reader.HasRows)
                {
                    while (responseQuery.reader.Read())
                    {
                        users.linkSocial = responseQuery.reader["link"].ToString();
                    }
                }
                DatabaseService.Instance.getConnection().Close();
            }
            return list_user;
        }

        //---get ilist agency
        public ListAgencyResponse getListAgencies()
        {
            var userResponse = new ListAgencyResponse();
            if (this.app_id == 0)
            {
                this.appendError(1001, "App chưa được chọn");
                userResponse.error = this.error;
                return userResponse;
            }

            userResponse.info = getListAgency("kw");

            return userResponse;
        }

        //Login facbook
        public UserServiceResponse loginFacebook(string facebook_id, string email)
        {
            var userResponse = new UserServiceResponse();
            if (this.app_id == 0)
            {
                this.appendError(1001, "App chưa được chọn");
                userResponse.error = this.error;
                return userResponse;
            }
            ResponseQuery responseQuery = new ResponseQuery();

            //Kiểm tra Đã có facebook_id chưa
            responseQuery = this.query("SELECT TOP 1 id,facebook,merchant_id FROM Users WHERE facebook='" + facebook_id + "' AND merchant_id =" + this.app_id);

            int count_user = 0;
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            while (responseQuery.reader.Read())
            {
                count_user++;
            }
            DatabaseService.Instance.getConnection().Close();
            //Kiểm tra email có không
            int count_user_email = 0;
            if (!email.Equals(""))
            {
                responseQuery = this.query("SELECT TOP 1 * FROM Users WHERE email='" + email + "' AND merchant_id =" + this.app_id);
                if (responseQuery.isError)
                {
                    this.appendError(1001, "Database error");
                    userResponse.error = this.error;
                    return userResponse;
                }
                while (responseQuery.reader.Read())
                {
                    count_user_email++;
                }
                DatabaseService.Instance.getConnection().Close();
            }

            if (count_user == 0 && count_user_email == 0)
            {
                responseQuery = this.queryInsert("INSERT INTO Users (username,password,email,merchant_id,status_active,code,verify,facebook) " +
                    "VALUES('','','" + email + "'," + this.app_id + ",'active','',1,'" + facebook_id + "')");
                //Xem query có lỗi ko
                if (responseQuery.isError)
                {
                    this.appendError(1001, "Database error");
                    userResponse.error = this.error;
                    return userResponse;
                }
                DatabaseService.Instance.getConnection().Close();

                responseQuery = this.query("SELECT TOP 1 id,facebook,merchant_id FROM Users WHERE facebook='" + facebook_id + "' AND merchant_id =" + this.app_id);

                int id_item = 0;
                if (responseQuery.isError)
                {
                    this.appendError(1001, "Database error");
                    userResponse.error = this.error;
                    return userResponse;
                }
                while (responseQuery.reader.Read())
                {
                    id_item = Int32.Parse(responseQuery.reader["id"].ToString());
                }

                responseQuery = this.query("UPDATE Users SET password='" + CryptoHelper.MD5Hash("", id_item.ToString()) + "' WHERE facebook = '" + facebook_id + "'");
                DatabaseService.Instance.getConnection().Close();

            }
            if (count_user > 0 && count_user_email == 0)
            {
                responseQuery = this.query("UPDATE Users SET email='" + email + "' WHERE facebook='" + facebook_id + "' AND merchant_id ='" + this.app_id + "' ");
                //Xem query có lỗi ko
                if (responseQuery.isError)
                {
                    this.appendError(1001, "Database error");
                    userResponse.error = this.error;
                    return userResponse;
                }
                DatabaseService.Instance.getConnection().Close();
            }
            if (count_user_email > 0 && count_user == 0)
            {
                responseQuery = this.query("UPDATE Users SET facebook='" + facebook_id + "' WHERE email='" + email + "' AND merchant_id ='" + this.app_id + "' ");
                //Xem query có lỗi ko
                if (responseQuery.isError)
                {
                    this.appendError(1001, "Database error");
                    userResponse.error = this.error;
                    return userResponse;
                }
                DatabaseService.Instance.getConnection().Close();
            }

            //Lấy info
            responseQuery = this.query("SELECT TOP 1 * FROM Users WHERE facebook='" + facebook_id + "' AND merchant_id =" + this.app_id);

            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            while (responseQuery.reader.Read())
            {
                userResponse.error = this.error;
                userResponse.info.id = Int32.Parse(responseQuery.reader["id"].ToString());
                userResponse.info.username = responseQuery.reader["username"].ToString();
                userResponse.info.email = responseQuery.reader["email"].ToString();
                if (responseQuery.reader["status_active"].ToString() == "active")
                {
                    userResponse.info.isActive = EnumUserModelActiveStatus.active;
                }
                else
                {
                    userResponse.info.isActive = EnumUserModelActiveStatus.deactive;
                }
                userResponse.info.verify = Int32.Parse(responseQuery.reader["verify"].ToString());
                userResponse.info.verify_phone = Int32.Parse(responseQuery.reader["verify_phone"].ToString());
                userResponse.info.phone = responseQuery.reader["phone"].ToString().Trim();

            }
            DatabaseService.Instance.getConnection().Close();

            return userResponse; ;
        }
        //Login gmail
        public UserServiceResponse loginGmail(string gmail_id, string email)
        {
            var userResponse = new UserServiceResponse();
            if (this.app_id == 0)
            {
                this.appendError(1001, "App chưa được chọn");
                userResponse.error = this.error;
                return userResponse;
            }
            ResponseQuery responseQuery = new ResponseQuery();

            //Kiểm tra Đã có gmail_id chưa
            responseQuery = this.query("SELECT TOP 1 id,gmail_id,merchant_id FROM Users WHERE gmail_id='" + gmail_id + "' AND merchant_id =" + this.app_id);

            int count_user = 0;
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            while (responseQuery.reader.Read())
            {
                count_user++;
            }
            DatabaseService.Instance.getConnection().Close();
            //Kiểm tra email có không
            int count_user_email = 0;

            responseQuery = this.query("SELECT TOP 1 id,email,merchant_id FROM Users WHERE email='" + email + "' AND merchant_id ='" + this.app_id + "'");
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            while (responseQuery.reader.Read())
            {
                count_user_email++;
            }
            DatabaseService.Instance.getConnection().Close();

            if (count_user == 0 && count_user_email == 0)
            {
                responseQuery = this.queryInsert("INSERT INTO Users (username,password,email,merchant_id,status_active,code,verify,gmail_id) " +
                    "VALUES('','','" + email + "'," + this.app_id + ",'active','',1,'" + gmail_id + "')");
                //Xem query có lỗi ko
                if (responseQuery.isError)
                {
                    this.appendError(1001, "Database error");
                    userResponse.error = this.error;
                    return userResponse;
                }
                DatabaseService.Instance.getConnection().Close();
            }
            if (count_user > 0 && count_user_email == 0)
            {
                responseQuery = this.query("UPDATE Users SET email='" + email + "' WHERE gmail_id='" + gmail_id + "' AND merchant_id ='" + this.app_id + "' ");
                //Xem query có lỗi ko
                if (responseQuery.isError)
                {
                    this.appendError(1001, "Database error");
                    userResponse.error = this.error;
                    return userResponse;
                }
                DatabaseService.Instance.getConnection().Close();
            }
            if (count_user_email > 0 && count_user == 0)
            {
                responseQuery = this.query("UPDATE Users SET gmail_id='" + gmail_id + "' WHERE email='" + email + "' AND merchant_id ='" + this.app_id + "' ");
                //Xem query có lỗi ko
                if (responseQuery.isError)
                {
                    this.appendError(1001, "Database error");
                    userResponse.error = this.error;
                    return userResponse;
                }
                DatabaseService.Instance.getConnection().Close();
            }

            //Lấy info
            responseQuery = this.query("SELECT TOP 1 * FROM Users WHERE gmail_id='" + gmail_id + "' AND merchant_id =" + this.app_id);
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                userResponse.error = this.error;
                return userResponse;
            }
            while (responseQuery.reader.Read())
            {
                userResponse.error = this.error;
                userResponse.info.id = Int32.Parse(responseQuery.reader["id"].ToString());
                userResponse.info.username = responseQuery.reader["username"].ToString();
                userResponse.info.email = responseQuery.reader["email"].ToString();
                if (responseQuery.reader["status_active"].ToString() == "active")
                {
                    userResponse.info.isActive = EnumUserModelActiveStatus.active;
                }
                else
                {
                    userResponse.info.isActive = EnumUserModelActiveStatus.deactive;
                }
                userResponse.info.verify = Int32.Parse(responseQuery.reader["verify"].ToString());
                userResponse.info.verify_phone = Int32.Parse(responseQuery.reader["verify_phone"].ToString());
                userResponse.info.phone = responseQuery.reader["phone"].ToString().Trim();
            }
            DatabaseService.Instance.getConnection().Close();
            return userResponse; ;
        }


        //Lấy số dư
        public WalletServiceResponse getWallet(UserModel user)
        {
            var response = new WalletServiceResponse();
            if (this.app_id == 0)
            {
                this.appendError(1001, "App chưa được chọn");
                response.error = this.error;

                return response;
            }
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT " +
                "TOP 2 " +
                "Merchant_Wallet.wallet_id," +
                "Wallet.name," +
                "Merchant_Wallet.merchant_id " +
                "FROM Merchant_Wallet INNER JOIN Wallet ON Merchant_Wallet.wallet_id = Wallet.id WHERE Merchant_Wallet.merchant_id =" + this.app_id + " ORDER BY Merchant_Wallet.wallet_id ASC");
            //Xem query có lỗi ko
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                response.error = this.error;
                return response;
            }
            //Lấy danh sách Merchant_coin
            List<Merchant_wallet> list_mer = new List<Merchant_wallet>();
            while (responseQuery.reader.Read())
            {
                Merchant_wallet merchant_wallet = new Merchant_wallet(this.app_id, Int32.Parse(responseQuery.reader["wallet_id"].ToString()), responseQuery.reader["name"].ToString());
                list_mer.Add(merchant_wallet);
            }
            DatabaseService.Instance.getConnection().Close();

            //Danh sách ví
            List<WalletService> list_w = new List<WalletService>();
            foreach (var item in list_mer)
            {
                //Lấy thông tin coin
                //Lấy tiền dư trong ví lưu ở transaction
                double balance = item.getBalance(user);
                WalletService walletService = new WalletService();
                walletService.id = item.wallet_id;
                walletService.name = item.wallet_name;
                walletService.balance = balance;
                walletService.name_coin = item.getNameCoin();
                list_w.Add(walletService);
            }
            response.userid = user.id;
            response.wallets = list_w;
            return response;
        }

    }
}
