﻿using CorePayment.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment.Response
{
   public class PaymentGateServiceResponse : ResponseBase
    {
       public List<PaymentGate> paymentGates = new List<PaymentGate>();
    }
}
