﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CorePayment.Base;
using CorePayment.Model;

namespace CorePayment.Response
{
    public class TransactionServiceResponse : ResponseBase
    {
        public double plus = 0;
        public double minus = 0;
        public int[] idRecieve = new int[2];
        public string msg = "";
    }
}
