﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CorePayment.Base;
using CorePayment.Model;

namespace CorePayment.Response
{
    public class MobileCardListResponse : ResponseBase
    {
        public List<MobileCardResponse> lstNap { get; set; }
        public List<MobileCardResponse> lstRut { get; set; }
    }
}
