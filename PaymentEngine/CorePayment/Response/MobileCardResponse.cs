﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CorePayment.Base;
using CorePayment.Model;

namespace CorePayment.Response
{
    public class MobileCardResponse
    {
        public int id = 0;
        public string provider = "";
        public double value=0;
        public int type = 0;//0: nap, 1: rut'
        public string shortLine = "";//de gui rut the
        public int provider_typecard = 0;//de nap the
    }
}
