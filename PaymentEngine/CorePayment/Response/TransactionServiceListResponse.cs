﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CorePayment.Base;
using CorePayment.Model;

namespace CorePayment.Response
{
    public class TransactionServiceListResponse : ResponseBase
    {
       public List<Transactions> list_transactions = new List<Transactions>();
    }
}
