﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorePayment.Response.PaymentVT
{
    public class ResponseCardVT
    {
        public string IsError;
        public string ErrorCode;
        public string Message;
    }
}