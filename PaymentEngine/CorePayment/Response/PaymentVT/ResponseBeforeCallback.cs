﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CorePayment.Base;
using CorePayment.Model;

namespace CorePayment.Response
{
    public class ResponseBeforeCallback : ResponseBase
    {
        public string msg = "";
        public string cardCode = "";
        public string cardSerial = "";
        public double cardValue = 0;
        public string provider = "";
    }
}
