﻿using CorePayment.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment.Response
{
   public class AppServiceResponse : ResponseBase
    {
        public List<App> list_app = new List<App>();
    }
}
