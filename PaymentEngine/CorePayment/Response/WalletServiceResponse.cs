﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CorePayment.Base;
using CorePayment.Model;

namespace CorePayment.Response
{
    public class WalletServiceResponse : ResponseBase
    {
        public int userid;
        public List<WalletService> wallets = new List<WalletService>();
    }
}
