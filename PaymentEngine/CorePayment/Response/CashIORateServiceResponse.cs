﻿using CorePayment.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment.Response
{
   public class CashIORateServiceResponse : ResponseBase
    {
        public List<int> list_cashIORate = new List<int>();
    }
}
