﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace BillingCard
{
    public class BillingHelper
    {
        public static string GetTelcoNameInAlego(ENSPType cardType)
        {
            switch (cardType)
            {
                case ENSPType.VT:
                    return "VTT";
                case ENSPType.VN:
                    return "VNP";
                case ENSPType.MB:
                    return "VMS";
                default:
                    return "";
            }
        }

        public static string GetTelcoAlegoBuyCardProductCode(ENSPType cardType)
        {
            switch (cardType)
            {
                case ENSPType.VT:
                    return "500";
                case ENSPType.MB:
                    return "501";
                case ENSPType.VN:
                    return "502";
                default:
                    return "";
              //Thẻ Sfone 503
              //Thẻ Gmobile 504
              //Thẻ VietnamMobile 505
            }
        }
    }
}
