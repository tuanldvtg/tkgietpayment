﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingCard
{
    //Loại thẻ của hệ thống: Viettel = 1; Mobi = 2; Vina = 3; 
    public enum ENSPType
    {
        NONE= 0,
        VT = 1,
        VN = 2, // vina
        MB = 3, // mobi
    }
}
