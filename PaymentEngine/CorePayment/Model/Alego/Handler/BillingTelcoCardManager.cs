﻿using System;
using BillingCard.Models;
using Newtonsoft.Json;

namespace BillingCard
{
    public class BillingTelcoCardManager
    {
        private static Object lockCashOutProccess = new object();
        /// <summary>
        /// Hàm lấy 1 mã thẻ online,
        /// Đối tác : ALEGO
        /// </summary>
        /// <param name="cardType"></param>
        /// <param name="cardAmount"></param>
        /// <param name="accountId"></param>
        /// <param name="responseStatus"> >= 0 : success, nho hon 0 : fail </param>
        /// <returns>CardBuy</returns>
        public static CardBuy GetOneCardOnline(ENSPType cardType, int cardAmount, out int responseStatus)
        {
            lock (lockCashOutProccess)
            {
                //xử lý đơn luồng
                var telcoName = BillingHelper.GetTelcoNameInAlego(cardType);
                var productCode = BillingHelper.GetTelcoAlegoBuyCardProductCode(cardType);
                var timeNow = DateTime.Now.Ticks;
                var orderId = "ex" + "_" + timeNow;
                var cardBuyRequest = new CardBuyRequest()
                {
                    ProductCode = productCode,
                    RefNumber = orderId,
                    Telco = telcoName,
                    Type = "TELCO_PINCODE",
                    CustMobile = "",
                    CustIP = "127.0.0.1",
                    CardPrice = cardAmount.ToString(),
                    CardQuantity = 1
                };
                var requestCard = JsonConvert.SerializeObject(cardBuyRequest);
                requestCard = requestCard.Replace(":", ": ").Replace(",", ", ");
                var encData = BillingSecurityManager.TripleDESEncrypt(BillingConfig.TripDESKey, requestCard);
                var checkSum = BillingSecurityManager.MD5(encData);
                var dataSend = new RequestToALeGo()
                {
                    Fnc = BillingConfig.Function,
                    Ver = BillingConfig.Ver,
                    AgentID = BillingConfig.AgentId,
                    AccID = BillingConfig.AccId,
                    EncData = encData,
                    data = requestCard,
                    Checksum = checkSum
                };
                int responseCode = 0;
                var responseTxt = NetworkUltis.SendPost(JsonConvert.SerializeObject(dataSend), BillingConfig.UrlBuyCard, out responseCode);
                if(responseCode == -1)
                {
                    //Fail - nhưng đối tác trả về dưới dạng lỗi web 502 -> Vẫn có thể parser ra kết quả dưới dạng Json
                    responseStatus = -1;
                    var cardBuyResponse = JsonConvert.DeserializeObject<CardBuyResponse>(responseTxt);
                    return new CardBuy()
                    {
                        CardType = cardType,
                        CardValue = 0,
                        CardSerial = string.Empty,
                        DateExpress = string.Empty,
                        CardCode = string.Empty,
                        OrderId = orderId,
                        ErrorMessage = cardBuyResponse.Description
                    };

                }
                else if(responseCode < -1)
                {
                    //Faill - từ phía mình : kết nối mạng -> không parse ra được dưới dạng kết quả Json
                    responseStatus = -1;
                    return new CardBuy()
                    {
                        CardType = cardType,
                        CardValue = 0,
                        CardSerial = string.Empty,
                        DateExpress = string.Empty,
                        CardCode = string.Empty,
                        OrderId = orderId,
                        ErrorMessage = "Loi he thong " + responseTxt.ToString()
                    };
                }
                else
                {
                    //responseCode > 0 - Thành công nhận request
                    responseStatus = 1;
                    var cardBuyResponse = JsonConvert.DeserializeObject<CardBuyResponse>(responseTxt);
                    var dataSuccessFromAgelo = BillingSecurityManager.TripleDESDecrypt(BillingConfig.TripDESKey, cardBuyResponse.EncData);
                    var cardInfo = JsonConvert.DeserializeObject<CardBuyFromAlego>(dataSuccessFromAgelo);
                    var cardBuySuccess = cardInfo.CardInfo[0];
                    return new CardBuy()
                    {
                        CardType = cardType,
                        CardValue = cardAmount,
                        CardSerial = cardBuySuccess.card_serial,
                        DateExpress = cardBuySuccess.expiration_date,
                        CardCode = cardBuySuccess.card_code,
                        OrderId = orderId,
                        ErrorMessage = string.Empty
                    };
                }
            }
        }
    }
}
