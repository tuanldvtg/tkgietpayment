﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingCard.Models
{
    public class CardInfo
    {
        public string card_code { get; set; }
        public string card_serial { get; set; }
        public string expiration_date { get; set; }
    }

    public class CardBuyFromAlego
    {
        public string ProductCode { get; set; }
        public string RefNumber { get; set; }
        public string TransID { get; set; }
        public string TransDate { get; set; }
        public string ResType { get; set; }
        public int CardQuantity { get; set; }
        public List<CardInfo> CardInfo { get; set; }
    }
}
