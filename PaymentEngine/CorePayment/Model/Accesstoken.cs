﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment.Model
{

    public class Accesstoken
    {
        public string access_token;
        public int expires_in;
        public string token_type;
    }
}
