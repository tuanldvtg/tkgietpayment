﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment.Model
{
    public class PaymentGate
    {
        public int id = 0;
        public string name;
        public string code;
        public int wallet_in;
        public int wallet_out;
        public float ratio;
        public string status_active;
        public int sms;
        public int fee;
        public PaymentGate()
        {

        }
        
        public PaymentGate(int id, string name, string code)
        {
            this.id = id;
            this.name = name;
            this.code = code;
        }
    }
}
