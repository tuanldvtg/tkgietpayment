﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment.Model
{

    public class Rwithdrawal_User
    {
        public int id = 0;
        public double value;
        public int wallet_id;
        public string wallet_name;
        public int agency_id;
        public string agency_name;
        public DateTime create_time;
        public double value_out;
    }
}
