﻿using CorePayment.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment.Model
{
    public class Merchant_wallet : ServiceBase
    {
        public int id;
        public int merchant_id;
        public int wallet_id;
        public string wallet_name;

        public Merchant_wallet()
        {
            this.merchant_id = 0;
            this.wallet_id = 0;
            this.wallet_name = "";
        }
        public Merchant_wallet(int merchant_id, int wallet_id,string wallet_name)
        {
            this.merchant_id = merchant_id;
            this.wallet_id = wallet_id;
            this.wallet_name = wallet_name;
        }

        public string getNameCoin()
        {
            Wallet wallet = new Wallet();
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT TOP 1 * FROM Wallet WHERE id=" + wallet_id);
            
            while (responseQuery.reader.Read())
            {
                wallet.id = wallet_id;
                wallet.name = responseQuery.reader["name"].ToString();
                wallet.coin_id = Int32.Parse(responseQuery.reader["coin_id"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();


            responseQuery = this.query("SELECT TOP 1 * FROM Coin WHERE id=" + wallet.coin_id);

            Coin coin = new Coin();
            while (responseQuery.reader.Read())
            {
                coin.id = wallet_id;
                coin.name = responseQuery.reader["name"].ToString();
            }
            DatabaseService.Instance.getConnection().Close();

            return coin.name;
        }

        public double getBalance(UserModel user)
        {
            double balance = 0;
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT TOP 1 * FROM Transactions WHERE wallet_id=" + wallet_id + " AND merchant_id="+ merchant_id +" AND user_id ="+user.id + " ORDER BY id DESC");
            //Xem query có lỗi ko
            if (responseQuery.isError)
            {
                return balance;
            }
            while (responseQuery.reader.Read())
            {
                balance =double.Parse(responseQuery.reader["balance"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            return balance;

        }
    }

   
}
