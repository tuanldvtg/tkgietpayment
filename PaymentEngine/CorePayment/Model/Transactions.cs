﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment.Model
{
   public class Transactions
    {
        public int id;
        public int user_id;
        public int merchant_id;
        public int payment_gate;
        public int wallet_id;
        public double balance;
        public string type;
        public string time_create;
        public double value;
        public double profit;
        public double value_out;
        public string transactions_type;
        public int user_send;
        public string tag;
        public string message;
        public string wallet_name;
    }
}
