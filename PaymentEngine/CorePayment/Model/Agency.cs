﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment.Model
{

    //public enum EnumUserModelActiveStatus { active, deactive};
    public class Agency
    {
        public int id = 0;
        public string email;
        public string title;
        public int subId;
        public string phone;
        public EnumUserModelActiveStatus isActive = EnumUserModelActiveStatus.deactive;
        public string location;
        public string linkSocial;
        public int orderId = 0;
        public int is_appear = 1;
        
    }
}
