﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CorePayment.Model
{
    public class Giftcode
    {
        public int id { get; set; }
        public int campaign_id { get; set; }
        public int id_creator { get; set; }
        public string code { get; set; }
        public int type_giftcode { get; set; }
        public float value { get; set; }
        public int is_active { get; set; }
        public string create_time { get; set; }
        public string approved_time { get; set; }
        public string expired_time { get; set; }
        public int user_id { get; set; }
    }
}