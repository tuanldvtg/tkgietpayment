﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment.Model
{
    public class CashIORate
    {
        public int payment_gate_id = 0;
        public double cash_in = 0;
        public double cash_out = 0;
    }
}
