﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment.Model
{

    public enum EnumUserModelActiveStatus { active, deactive};
    public class UserModel
    {
        public int id = 0;
        public string email;
        public string username;
        public string password = "";
        public string name;
        public string phone;
        public EnumUserModelActiveStatus isActive = EnumUserModelActiveStatus.deactive;
        public int verify;
        public int verify_phone;
        public string code_sms;
    }
}
