﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment.Model
{
    public class MobileCard
    {
        public int id { get; set; }
        public int provider_type_card { get; set; }
        public int value { get; set; }
    }
}
