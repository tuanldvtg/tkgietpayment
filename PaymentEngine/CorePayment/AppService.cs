﻿using CorePayment.Base;
using CorePayment.Model;
using CorePayment.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment
{
   public class AppService : ServiceBase
    {
        //Lấy Danh sách App
        public AppServiceResponse getListApp()
        {
            var response = new AppServiceResponse();
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT * FROM Merchant");
            //Xem query có lỗi ko
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                response.error = this.error;
                return response;
            }
            //Lấy danh sách Merchant_coin
            List<App> list_app = new List<App>();
            while (responseQuery.reader.Read())
            {
                App app = new App();
                app.id = Int32.Parse(responseQuery.reader["id"].ToString());
                app.name = responseQuery.reader["name"].ToString();
                app.secret_key = responseQuery.reader["secret_key"].ToString();
                list_app.Add(app);
            }
            DatabaseService.Instance.getConnection().Close();
            response.list_app = list_app;
            return response;
        }
    }
}
