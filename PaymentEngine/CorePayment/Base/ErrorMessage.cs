﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment.Base
{
    public class ErrorMessage
    {
        public String content;
        public int code;

        public ErrorMessage(String content)
        {
            this.code = 0;
            this.content = content;
        }

        public ErrorMessage(int code,String content)
        {
            this.code = code;
            this.content = content;
        }
    }
}
