using CorePayment.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment.Base
{
    public class ServiceBase
    {
        public UserModel activingUser = new UserModel();

        public Error error = new Error();

        public void appendError(Exception e)
        {
            error.isError = true;
            error.errors.Add(new ErrorMessage(e.ToString()));
        }

        public void appendError(int code, String s)
        {
            error.isError = true;
            error.errors.Add(new ErrorMessage(code, s));
        }

        public SqlDataReader querySqlParam(SqlCommand command)
        {
            DatabaseService.Instance.getConnection().Open();
            SqlDataReader reader = command.ExecuteReader();
            return reader;
        }
        public void querySqlParamInsert(SqlCommand command)
        {
            DatabaseService.Instance.getConnection().Open();
            command.ExecuteNonQuery();
        }

        public ResponseQuery query(string stringQuery)
        {

            ResponseQuery response = new ResponseQuery();

            var con = DatabaseService.Instance.getConnection();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            cmd.CommandText = stringQuery;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;
            Console.WriteLine("Query: " + stringQuery);
            try
            {
                con.Open();
            }
            catch (Exception )
            {
                response.isError = true;
                return response;
            }

            reader = cmd.ExecuteReader();
            response.isError = false;
            response.reader = reader;
            return response;
        }

        public ResponseQuery queryInsert(string stringQuery)
        {

            ResponseQuery response = new ResponseQuery();

            var con = DatabaseService.Instance.getConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = stringQuery;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;
            Console.WriteLine("Query: " + stringQuery);
            try
            {
                con.Open();
            }
            catch (Exception )
            {
                response.isError = true;
                return response;
            }

            cmd.ExecuteNonQuery();
            response.isError = false;
            return response;
        }

    }
}
