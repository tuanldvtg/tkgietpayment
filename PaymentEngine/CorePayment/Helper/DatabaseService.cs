﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using CorePayment.Base;

namespace CorePayment
{

    class DatabaseService:ServiceBase
    {
        private static DatabaseService instance;

        private DatabaseService() { }
        public static DatabaseService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DatabaseService();
                }
                return instance;
            }
        }

        SqlConnection conn;
        int app_id;
        internal void connect(string serverAddress, string dBName, string userName, string password)
        {
            string connetionString =
            "Data Source="+ serverAddress + ";" +
            "Initial Catalog="+ dBName + ";" +
            "User id="+ userName + ";" +
            "Password="+ password + ";";
            conn = new SqlConnection(connetionString);
        }

        public SqlConnection getConnection()
        {
            return conn;
        }

        public void setAppID(int appID)
        {
            app_id = appID;
        }

        public int getAppID()
        {
            return app_id;
        }

    }
}
