﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorePayment.Helper
{
    public class Int32Ext
    {
        public static int toInt(string content)
        {
            var e = 0;
            Int32.TryParse(content, out e);
            return e;
        }
    }
}