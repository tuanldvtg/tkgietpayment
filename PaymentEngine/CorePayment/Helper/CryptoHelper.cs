﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace CorePayment.Helper
{
    public class CryptoHelper
    {
        static string keyHash = "ROLLSROYCE";
        public static string MD5Hash(string input,string userID)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input+ Hash(keyHash+ userID)));
            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        static string Hash(string input)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);
                
                foreach (byte b in hash)
                {
                    // can be "x2" if you want lowercase
                    sb.Append(b.ToString("X2"));
                }

                return sb.ToString();
            }
        }
        public static string HmacSha256Digest(string message, string secret)
        {
            //ASCIIEncoding encoding = new ASCIIEncoding();
            //byte[] keyBytes = encoding.GetBytes(secret);
            //byte[] messageBytes = encoding.GetBytes(message);
            //HMACSHA256 cryptographer = new HMACSHA256(keyBytes);

            //byte[] bytes = cryptographer.ComputeHash(messageBytes);

            //return BitConverter.ToString(bytes).Replace("-", "").ToLower();
            Byte[] textBytes = Encoding.UTF8.GetBytes(message);
            Byte[] keyBytes = Encoding.UTF8.GetBytes(secret);

            Byte[] hashBytes;

            using (HMACSHA256 hash = new HMACSHA256(keyBytes))
                hashBytes = hash.ComputeHash(textBytes);
            return Convert.ToBase64String(hashBytes);
        }

        public static string Get(string uri)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(uri);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(uri).Result;

            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                var s = Newtonsoft.Json.JsonConvert.DeserializeObject(result);
                return result;
            }
            else
            {
                return "Fail";
            }
        }

        public static string getMethod(string url, string parsedContent)
        {
            using (var client = new WebClient())
            {
                var str = url + "?data=" + parsedContent;
                Debug.WriteLine("str: "+ str);
                var responseString = client.DownloadString(str);
                return responseString;
            }
        }

        //---encode base64
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string postMethod(string url, string parsedContent)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);

            var postData = "data=parsedContent";
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            return responseString;
        }

        //---decode base64
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string randomToken()
        {
            Guid g = Guid.NewGuid();
            string GuidString = Convert.ToBase64String(g.ToByteArray());
            GuidString = GuidString.Replace("=", "");
            GuidString = GuidString.Replace("+", "");
            return GuidString;
        }

    }
}