﻿using CorePayment.Base;
using CorePayment.Helper;
using CorePayment.Model;
using CorePayment.Response;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment
{
    public class TransactionsService : ServiceBase
    {
        public int app_id = 0;
        public TransactionsService(int id)
        {
            this.app_id = id;
        }

        private string render_code()
        {
            string code = this.RandomString(6);
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT TOP 1 id FROM UserMoneyTransferCommit WHERE code ='" + code + "'");
            int id_user = 0;
            while (responseQuery.reader.Read())
            {
                id_user = Int32.Parse(responseQuery.reader["id"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();

            if (id_user != 0)
            {
                return this.render_code();
            }
            return code;
        }

        //Lấy danh sách tiền có thể chuyển
        public CashIORateServiceResponse getListCashIORate(int paymentGate_id)
        {
            CashIORateServiceResponse response = new CashIORateServiceResponse();
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT TOP 1 id,cash_in,cash_out FROM CashIORate WHERE payment_gate_id ='" + paymentGate_id + "'");
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                response.error = this.error;
                return response;
            }
            while (responseQuery.reader.Read())
            {
                response.list_cashIORate.Add(Int32.Parse(responseQuery.reader["cash_out"].ToString()));
            }
            DatabaseService.Instance.getConnection().Close();
            return response;
        }

        //Query lấy cổng thanh toán
        private PaymentGate getPaymentGate(string code_gate)
        {
            PaymentGate paymentGateService = new PaymentGate();
            ResponseQuery responseQuery = new ResponseQuery();
            //responseQuery = this.query("SELECT TOP 1 id,name,code,wallet_in,wallet_out,status_active,sms FROM PaymentGate WHERE code ='" + code_gate + "' AND merchant_id='" + this.app_id + "'");
            string query = "SELECT TOP 1 id,name,code,wallet_in,wallet_out,status_active,sms FROM PaymentGate WHERE code =@code_gate AND merchant_id=@merchant_id";
            SqlCommand command = new SqlCommand(query, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@merchant_id", this.app_id);
            command.Parameters.AddWithValue("@code_gate", code_gate);
            responseQuery.reader = this.querySqlParam(command);
            int count_app = 0;
            while (responseQuery.reader.Read())
            {
                count_app++;
                paymentGateService.id = Int32.Parse(responseQuery.reader["id"].ToString());
                paymentGateService.name = responseQuery.reader["name"].ToString();
                paymentGateService.code = responseQuery.reader["code"].ToString();
                paymentGateService.wallet_in = Int32.Parse(responseQuery.reader["wallet_in"].ToString());
                paymentGateService.wallet_out = Int32.Parse(responseQuery.reader["wallet_out"].ToString());
                paymentGateService.status_active = responseQuery.reader["status_active"].ToString();
                paymentGateService.sms = Int32.Parse(responseQuery.reader["sms"].ToString());
            }
            responseQuery.reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return paymentGateService;
        }

        //Kiểm tra tham số trừ tiền có đúng config
        private CashIORate checkValueConfig(PaymentGate paymentGateService, double value)
        {
            CashIORate cashIORate = new CashIORate();
            ResponseQuery responseQuery = new ResponseQuery();
            // ResponseQuery responseQuery = this.query("SELECT TOP 1 id,payment_gate_id,cash_out,cash_in FROM CashIORate WHERE payment_gate_id ='" + paymentGateService.id + "' AND cash_in >= '" + value + "' ORDER BY cash_in ASC ");
            string query = "SELECT TOP 1 id,payment_gate_id,cash_out,cash_in FROM CashIORate WHERE payment_gate_id =@paymentGateService AND cash_in >= @value ORDER BY cash_in ASC";
            SqlCommand command = new SqlCommand(query, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@paymentGateService", paymentGateService.id);
            command.Parameters.AddWithValue("@value", value);
            responseQuery.reader = this.querySqlParam(command);
            while (responseQuery.reader.Read())
            {
                cashIORate.payment_gate_id = Int32.Parse(responseQuery.reader["payment_gate_id"].ToString());
                cashIORate.cash_out = Double.Parse(responseQuery.reader["cash_out"].ToString());
                cashIORate.cash_in = Double.Parse(responseQuery.reader["cash_in"].ToString());
            }
            responseQuery.reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return cashIORate;
        }
        //Lấy số tiền trong ví
        private double getBalace(UserModel user, int wallet_in)
        {
            double balance = 0;
            ResponseQuery responseQuery = new ResponseQuery();
            string query = "SELECT TOP 1 balance FROM Transactions WHERE wallet_id =@wallet_in " +
                 "AND user_id=@user_id AND (transactions_type = 0 OR transactions_type IS NULL) ORDER BY id DESC";
            SqlCommand command = new SqlCommand(query, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@wallet_in", wallet_in);
            command.Parameters.AddWithValue("@user_id", user.id);
            responseQuery.reader = this.querySqlParam(command);
            while (responseQuery.reader.Read())
            {
                balance = double.Parse(responseQuery.reader["balance"].ToString());
            }
            responseQuery.reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return balance;
        }
        //Kiểm tra thông ví có được dùng trong Merchant ko
        private Merchant_wallet getMerchantWalletByApp(int wallet_in)
        {
            Merchant_wallet merchant_Wallet = new Merchant_wallet();
            ResponseQuery responseQuery = new ResponseQuery();
            string query = "SELECT TOP 1 id,merchant_id,wallet_id FROM Merchant_wallet WHERE merchant_id=@merchant_id AND wallet_id = @wallet_in";
            SqlCommand command = new SqlCommand(query, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@merchant_id", this.app_id);
            command.Parameters.AddWithValue("@wallet_in", wallet_in);
            responseQuery.reader = this.querySqlParam(command);
            while (responseQuery.reader.Read())
            {
                merchant_Wallet.merchant_id = Int32.Parse(responseQuery.reader["merchant_id"].ToString());
                merchant_Wallet.wallet_id = Int32.Parse(responseQuery.reader["wallet_id"].ToString());
            }
            responseQuery.reader.Close();
            DatabaseService.Instance.getConnection().Close();
            return merchant_Wallet;
        }

        private Random random = new Random();
        private string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


        public TransactionServiceResponse verifyTransactions(UserModel user, string code)
        {
            TransactionServiceResponse response = new TransactionServiceResponse();
            if (this.app_id == 0)
            {
                this.appendError(1, "Chưa set app id");
                response.error = this.error;
                return response;
            }
            ResponseQuery responseQuery = new ResponseQuery();
            string query = "SELECT TOP 1 id,time_creat FROM OTP_user WHERE user_id =@user_id AND otp =@code AND status = 0 ORDER BY id DESC ";
            SqlCommand command = new SqlCommand(query, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@merchant_id", this.app_id);
            command.Parameters.AddWithValue("@code", code);
            command.Parameters.AddWithValue("@user_id", user.id);
            responseQuery.reader = this.querySqlParam(command);
            int count_verify_phone = 0;
            DateTime time_creat = DateTime.Now;
            while (responseQuery.reader.Read())
            {
                count_verify_phone++;
                time_creat = DateTime.Parse(responseQuery.reader["time_creat"].ToString());
            }
            responseQuery.reader.Close();
            DatabaseService.Instance.getConnection().Close();
            if (count_verify_phone == 0)
            {
                this.appendError(1, "Mã OTP không đúng");
                response.error = this.error;
                return response;
            }

            //Kiem tra otp quá 5 phút

            DateTime dateTime = DateTime.Now;
            long timeMsSinceNow = new DateTimeOffset(dateTime).ToUnixTimeMilliseconds();
            long timeMsSinceOTP = new DateTimeOffset(time_creat).ToUnixTimeMilliseconds();
            if ((timeMsSinceOTP - timeMsSinceNow) / 1000 > 5)
            {
                this.appendError(1, "Mã OTP hết hạn");
                response.error = this.error;
                return response;
            }

            query = "SELECT TOP 2 id,user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,profit,value_out,user_send,tag,message FROM UserMoneyTransferCommit WHERE code =@code ORDER BY id ASC ";
            command = new SqlCommand(query, DatabaseService.Instance.getConnection());
            command.Parameters.AddWithValue("@code", code);
            responseQuery.reader = this.querySqlParam(command);

            List<Transactions> list_tran = new List<Transactions>();
            while (responseQuery.reader.Read())
            {
                Transactions transactions = new Transactions();
                transactions.user_id = Int32.Parse(responseQuery.reader["user_id"].ToString());
                transactions.merchant_id = Int32.Parse(responseQuery.reader["merchant_id"].ToString());
                transactions.payment_gate = Int32.Parse(responseQuery.reader["payment_gate"].ToString());
                transactions.wallet_id = Int32.Parse(responseQuery.reader["wallet_id"].ToString());
                transactions.balance = Double.Parse(responseQuery.reader["balance"].ToString());
                transactions.type = responseQuery.reader["type"].ToString();
                transactions.time_create = responseQuery.reader["time_creat"].ToString();
                transactions.value = Double.Parse(responseQuery.reader["value"].ToString());
                transactions.profit = Double.Parse(responseQuery.reader["profit"].ToString());
                transactions.value_out = Double.Parse(responseQuery.reader["value_out"].ToString());
                transactions.user_send = Int32.Parse(responseQuery.reader["user_send"].ToString());
                transactions.tag = responseQuery.reader["tag"].ToString();
                transactions.message = responseQuery.reader["message"].ToString();
                list_tran.Add(transactions);
            }
            responseQuery.reader.Close();
            DatabaseService.Instance.getConnection().Close();
            int id_gate = 0;
            foreach (Transactions tran in list_tran)
            {
                responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) VALUES ('" +
tran.user_id + "'," + this.app_id + ",'" + tran.payment_gate + "'," +
"'" + tran.wallet_id + "','" + tran.balance + "','" + tran.type + "','" + tran.time_create + "'" +
",'" + tran.value + "','" + tran.user_send + "',N'" + tran.tag + "',N'" + tran.message + "'," + tran.profit + "," + tran.value_out + ")");
                id_gate = tran.payment_gate;

                DatabaseService.Instance.getConnection().Close();
            }

            PaymentGate paymentGateService = new PaymentGate();
            //Kiểm tra cổng thanh toán có tồn tại
            try
            {
                responseQuery = this.query("SELECT TOP 1 id,name,code,wallet_in,wallet_out,status_active,sms,fee,merchant_id FROM PaymentGate WHERE id ='" + id_gate + "' AND merchant_id='" + this.app_id + "'");
                int count_app = 0;
                while (responseQuery.reader.Read())
                {
                    count_app++;
                    paymentGateService.id = Int32Ext.toInt(responseQuery.reader["id"].ToString());
                    paymentGateService.name = responseQuery.reader["name"].ToString();
                    paymentGateService.code = responseQuery.reader["code"].ToString();
                    paymentGateService.wallet_in = Int32.Parse(responseQuery.reader["wallet_in"].ToString());
                    paymentGateService.wallet_out = Int32.Parse(responseQuery.reader["wallet_out"].ToString());
                    paymentGateService.status_active = responseQuery.reader["status_active"].ToString();
                    paymentGateService.sms = Int32Ext.toInt(responseQuery.reader["sms"].ToString());
                    paymentGateService.fee = Int32Ext.toInt(responseQuery.reader["fee"].ToString());
                }
                DatabaseService.Instance.getConnection().Close();
            }
            catch (Exception ex)
            {
                this.appendError(1000, "Get cổng thanh toán lỗi :" + "SELECT TOP 1 id,name,code,wallet_in,wallet_out,status_active,sms,fee,merchant_id FROM PaymentGate WHERE id ='" + list_tran[0].payment_gate + "' AND merchant_id='" + this.app_id + "'");
                response.error = this.error;
                return response;
            }
            //Trừ phí cố định
            if (paymentGateService.fee != 0)
            {
                double balance = 0;
                //Tính số tiền trong ví có đủ chuyển
                try
                {
                    balance = this.getBalace(user, paymentGateService.wallet_in);

                }
                catch (Exception ex)
                {
                    this.appendError(1000, "Lỗi lấy tổng tiền trong ví user" );
                    response.error = this.error;
                    return response;
                }
                DateTime time = DateTime.Now;
                // Use current time
                string format = "yyyy-MM-dd HH:mm:ss";

                balance = balance - paymentGateService.fee;
                string s_balace = balance.ToString();
                s_balace = s_balace.Replace(",", ".");

                //Lấy user merchant Cộng phí cố định vào merchant
                UserServices userServices = new UserServices(this.app_id);
                UserServiceResponse userServiceResponse = userServices.getUserByApp();
                if (userServiceResponse.error.isError)
                {
                    this.appendError(1000, "Lấy user merchant lỗi.");
                    response.error = this.error;
                    return response;
                }

                responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) VALUES (" +
user.id + "," + this.app_id + "," + paymentGateService.id + "," + paymentGateService.wallet_in + "," + s_balace + ",'minus','" + time.ToString(format) + "'," + paymentGateService.fee + "," + userServiceResponse.info.id + ",N'phecodinh',N'Trừ phí cố định',0," + paymentGateService.fee + ")");
                if (responseQuery.isError)
                {
                    this.appendError(1000, "INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) VALUES (" +
user.id + "," + this.app_id + "," + paymentGateService.id + "," + paymentGateService.wallet_in + "," + s_balace + ",'minus','" + time.ToString(format) + "'," + paymentGateService.fee + "," + userServiceResponse.info.id + ",N'phecodinh',N'Trừ phí cố định',0," + paymentGateService.fee + ")");
                    response.error = this.error;
                    return response;
                }
                DatabaseService.Instance.getConnection().Close();

                double balance_merchant = this.getBalace(userServiceResponse.info, paymentGateService.wallet_in);
                balance_merchant = balance_merchant + paymentGateService.fee;

                string s_balance_merchant = balance_merchant.ToString();
                s_balance_merchant = s_balance_merchant.Replace(",", ".");
                try
                {
                    responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) VALUES (" +
userServiceResponse.info.id + "," + this.app_id + "," + paymentGateService.id + "," + paymentGateService.wallet_in + "," + s_balance_merchant + ",'plus','" + time.ToString(format) + "','" + paymentGateService.fee + "'," + user.id + ",N'phecodinh',N'Phế cố định nhận được giao dịch',0,'" + paymentGateService.fee + "')");
                    if (responseQuery.isError)
                    {
                        this.appendError(1000, "Database Inser update phế cho merchant lỗi " + "INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) VALUES (" +
                    userServiceResponse.info.id + "," + this.app_id + "," + paymentGateService.id + "," + paymentGateService.wallet_in + "," + s_balance_merchant + ",'plus','" + time.ToString(format) + "',0," + user.id + ",N'phecodinh',N'Phế cố định nhận được giao dịch',0,'" + paymentGateService.fee + "')");
                        response.error = this.error;
                        return response;
                    }
                    DatabaseService.Instance.getConnection().Close();
                }
                catch (Exception ex)
                {
                    this.appendError(1000, "Database Inser update phế cho merchant lỗi " + "INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) VALUES (" +
                userServiceResponse.info.id + "," + this.app_id + "," + paymentGateService.id + "," + paymentGateService.wallet_in + "," + s_balance_merchant + ",'plus','" + time.ToString(format) + "',0," + user.id + ",N'phecodinh',N'Phế cố định nhận được giao dịch',0,'" + paymentGateService.fee + "')");
                    response.error = this.error;
                    return response;
                }

            }


            responseQuery = this.query("UPDATE OTP_user SET status =1  WHERE user_id =" + user.id + " AND otp = '" + code + "'");
            DatabaseService.Instance.getConnection().Close();
            responseQuery = this.query("DELETE FROM UserMoneyTransferCommit WHERE code = '" + code + "'");
            DatabaseService.Instance.getConnection().Close();

            //Cộng phế cho merchant
            Transactions transactions_item = list_tran[0];
            Transactions transactions_item_2 = list_tran[1];
            if (transactions_item.wallet_id == transactions_item_2.wallet_id && transactions_item.profit != 0)
            {
                UserServices userServices = new UserServices(this.app_id);
                UserServiceResponse userServiceResponse = userServices.getUserByApp();
                if (userServiceResponse.error.isError)
                {
                    this.appendError(7, "Lấy user merchant lỗi.");
                    response.error = this.error;
                    return response;
                }
                double balance_merchant = 0;
                balance_merchant = this.getBalace(userServiceResponse.info, transactions_item.wallet_id);
                balance_merchant = balance_merchant + transactions_item.profit;

                string s_balance_merchant = balance_merchant.ToString();
                s_balance_merchant = s_balance_merchant.Replace(",", ".");

                responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) VALUES (" +
                userServiceResponse.info.id + "," + this.app_id + "," + transactions_item.payment_gate + "," + transactions_item.wallet_id + "," + s_balance_merchant + ",'plus','" + transactions_item.time_create + "'," + transactions_item.profit + "," + transactions_item.user_id + ",N'phe',N'Phế nhận được',0," + transactions_item.profit + ")");
                if (responseQuery.isError)
                {
                    this.appendError(7, "Database Inser update phế cho merchant lỗi ");
                    response.error = this.error;
                    return response;
                }
                DatabaseService.Instance.getConnection().Close();
            }
            response.minus = transactions_item.value;
            response.plus = transactions_item_2.value;
            response.idRecieve[0] = transactions_item.user_id;
            response.idRecieve[1] = transactions_item.user_send;
            return response;
        }


        public TransactionServiceResponse transactionsWalletUserToUser(string tag, UserModel user_a, UserModel user_b, string code_gate, double value, string message,bool checkWithdrawal)
        {
            TransactionServiceResponse response = new TransactionServiceResponse();
            SqlCommand command;
            if (value <= 0)
            {
                this.appendError(1, "Số tiền chuyển không được <= 0");
                response.error = this.error;
                return response;
            }

            if (this.app_id == 0)
            {
                this.appendError(1, "Chưa set app id");
                response.error = this.error;
                return response;
            }

            ResponseQuery responseQuery = new ResponseQuery();

            PaymentGate paymentGateService = new PaymentGate();
            string query;


            //Kiểm tra cổng thanh toán có tồn tại
            try
            {
                query = "SELECT TOP 1 id,name,code,wallet_in,wallet_out,status_active,sms,fee,merchant_id FROM PaymentGate WHERE code =@code_gate AND merchant_id=@merchant_id";
                command = new SqlCommand(query, DatabaseService.Instance.getConnection());
                command.Parameters.AddWithValue("@merchant_id", this.app_id);
                command.Parameters.AddWithValue("@code_gate", code_gate);
                responseQuery.reader = this.querySqlParam(command);
                int count_app = 0;
                while (responseQuery.reader.Read())
                {
                    count_app++;
                    paymentGateService.id = Int32Ext.toInt(responseQuery.reader["id"].ToString());
                    paymentGateService.name = responseQuery.reader["name"].ToString();
                    paymentGateService.code = responseQuery.reader["code"].ToString();
                    paymentGateService.wallet_in = Int32.Parse(responseQuery.reader["wallet_in"].ToString());
                    paymentGateService.wallet_out = Int32.Parse(responseQuery.reader["wallet_out"].ToString());
                    paymentGateService.status_active = responseQuery.reader["status_active"].ToString();
                    paymentGateService.sms = Int32Ext.toInt(responseQuery.reader["sms"].ToString());
                    paymentGateService.fee = Int32Ext.toInt(responseQuery.reader["fee"].ToString());
                }
                responseQuery.reader.Close();
                DatabaseService.Instance.getConnection().Close();
            }
            catch (Exception ex)
            {
                this.appendError(1000, "Get cổng thanh toán lỗi :" + "SELECT TOP 1 id,name,code,wallet_in,wallet_out,status_active,sms,fee,merchant_id FROM PaymentGate WHERE code ='" + code_gate + "' AND merchant_id=" + this.app_id + "");
                response.error = this.error;
                return response;
            }


            if (paymentGateService.id == 0)
            {
                this.appendError(2, "Cổng thanh toán không tồn tại ");
                response.error = this.error;
                return response;
            }

            if (!paymentGateService.status_active.Trim().Equals("active"))
            {
                this.appendError(3, "Cổng thanh toán đang bị khóa ");
                response.error = this.error;
                return response;
            }
            string format = "yyyy-MM-dd HH:mm:ss";
            var now = DateTime.Now;
            int count_send = 0;

            if (paymentGateService.sms == 1)
            {
                //---check tien` chuyen nguoi choi
                if (value <= 10000)
                {
                    this.appendError(1, "Số tiền chuyển không được <= 10000");
                    response.error = this.error;
                    return response;
                }

                if (checkWithdrawal)
                {
                    ////---chống rút tiền
                    //---tien`nap
                    string queryChecknap = "SELECT SUM(value) as 'total'  FROM Transactions WHERE (tag='Agency+' or payment_gate=2066) AND user_id = " + user_a.id + " AND wallet_id = 11 AND type = 'plus'";
                    responseQuery = this.query(queryChecknap);
                    if (!responseQuery.reader.HasRows)
                    {
                        DatabaseService.Instance.getConnection().Close();
                        this.appendError(2, "Bạn chưa nạp tiền, nên không được chuyển tiền.");
                        response.error = this.error;
                        return response;
                    }
                    long ballanceNap = 0;
                    while (responseQuery.reader.Read())
                    {
                        try
                        {
                            ballanceNap = (long)Convert.ToDouble(responseQuery.reader["total"].ToString().Trim());
                        }
                        catch (Exception)
                        {
                            DatabaseService.Instance.getConnection().Close();
                            this.appendError(2, "Bạn chưa nạp tiền, nên không được chuyển tiền.");
                            response.error = this.error;
                            return response;
                        }
                    }
                    if (ballanceNap == 0)
                    {
                        DatabaseService.Instance.getConnection().Close();
                        this.appendError(2, "Bạn chưa nạp tiền, nên không được chuyển tiền.");
                        response.error = this.error;
                        return response;
                    }
                    DatabaseService.Instance.getConnection().Close();
                    //---tien` rut'
                    string queryCheckRut = "SELECT SUM(value) as 'total'  FROM Transactions WHERE user_id = " + user_a.id + " AND wallet_id = 11 AND type = 'minus' AND (payment_gate= 2061 OR payment_gate= 2072 OR payment_gate= 2082)";
                    responseQuery = this.query(queryCheckRut);
                    long ballanceRut = 0;
                    if (!responseQuery.reader.HasRows)
                    {
                        ballanceRut = 0;
                    }
                    while (responseQuery.reader.Read())
                    {
                        try
                        {
                            ballanceRut = (long)Convert.ToDouble(responseQuery.reader["total"].ToString().Trim());
                        }
                        catch (Exception)
                        {

                        }
                    }
                    DatabaseService.Instance.getConnection().Close();
                    //---tien` cho merchant
                    var queryCheckTranMerchant = "SELECT SUM(value) as 'total'  FROM Transactions WHERE user_send = " + user_a.id + " AND user_id = 155 AND wallet_id = 11 AND type = 'plus' AND payment_gate= 1054";
                    responseQuery = this.query(queryCheckTranMerchant);
                    if (!responseQuery.reader.HasRows)
                    {
                        DatabaseService.Instance.getConnection().Close();
                        this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                        response.error = this.error;
                        return response;
                    }
                    long ballanceMerchant = 0;
                    while (responseQuery.reader.Read())
                    {
                        try
                        {
                            ballanceMerchant = (long)Convert.ToDouble(responseQuery.reader["total"].ToString().Trim());
                        }
                        catch
                        {
                            DatabaseService.Instance.getConnection().Close();
                            this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                            response.error = this.error;
                            return response;
                        }
                    }
                    if (ballanceMerchant == 0)
                    {
                        DatabaseService.Instance.getConnection().Close();
                        this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                        response.error = this.error;
                        return response;
                    }
                    DatabaseService.Instance.getConnection().Close();
                    //---tien` merchant chuyen lai user
                    var queryCheckTranFromMerchant = "SELECT SUM(value) as 'total'  FROM Transactions WHERE user_send = 155 AND user_id = " + user_a.id + " AND wallet_id = 11 AND type = 'plus' AND payment_gate= 1055";
                    responseQuery = this.query(queryCheckTranFromMerchant);
                    if (!responseQuery.reader.HasRows)
                    {
                        DatabaseService.Instance.getConnection().Close();
                        this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                        response.error = this.error;
                        return response;
                    }
                    long ballanceFromMerchant = 0;
                    while (responseQuery.reader.Read())
                    {
                        try
                        {
                            ballanceFromMerchant = (long)Convert.ToDouble(responseQuery.reader["total"].ToString().Trim());
                        }
                        catch (Exception)
                        {
                            DatabaseService.Instance.getConnection().Close();
                            this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                            response.error = this.error;
                            return response;
                        }
                    }
                    if (ballanceFromMerchant == 0)
                    {
                        DatabaseService.Instance.getConnection().Close();
                        this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                        response.error = this.error;
                        return response;
                    }
                    DatabaseService.Instance.getConnection().Close();
                    //---tien` userToUser
                    var queryUToU = "SELECT SUM(value) as 'total'  FROM Transactions WHERE user_id = " + user_a.id + " AND user_send != 155 AND wallet_id = 11 AND payment_gate=1058";
                    responseQuery = this.query(queryUToU);

                    long balanceUToU = 0;
                    if (responseQuery.reader.HasRows)
                    {
                        while (responseQuery.reader.Read())
                        {
                            try
                            {
                                balanceUToU = (long)Convert.ToDouble(responseQuery.reader["total"].ToString().Trim());
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                    DatabaseService.Instance.getConnection().Close();
                    //---tinh' toan' chong'r rut' tien`
                    double sysConf = 0;
                    //if (ballanceNap < ballanceRut)
                    //{
                    //    sysConf = (double)ballanceMerchant / (balanceUToU + ballanceNap);
                    //}
                    //else
                    //{
                    sysConf = (double)ballanceMerchant / (balanceUToU + ballanceNap);
                    //}
                    Debug.WriteLine("sysConf: " + sysConf + " ballanceMerchant: " + ballanceMerchant + " balanceUToU: " + balanceUToU + " ballanceNap: " + ballanceNap + " ballanceFromMerchant: " + ballanceFromMerchant);
                    if (sysConf < 0.1)
                    {
                        this.appendError(4, "Bạn chưa đủ điều kiện để chuyển tiền. Hãy liên hệ với chăm sóc khách hàng để được giải quyết!");
                        response.error = this.error;
                        return response;
                    }
                    //---
                }

                //---kiem tra count otp>10
                var countOtp = 10;
                Debug.WriteLine("countOtp: " + countOtp);
                responseQuery = this.query("SELECT TOP 1 id,time_send_otp,count_send_otp FROM Users WHERE id =" + user_a.id);
                if (!responseQuery.reader.HasRows)
                {
                    this.appendError(1, "Không có user hợp lệ");
                    response.error = this.error;
                    return response;
                }
                if (responseQuery.isError)
                {
                    this.appendError(1001, "Database error");
                    response.error = this.error;
                    return response;
                }
                string datetime = now.Year + "-" + now.Month + "-" + now.Day;

                while (responseQuery.reader.Read())
                {
                    if (responseQuery.reader["time_send_otp"] != null)
                    {
                        if (!responseQuery.reader["time_send_otp"].ToString().Trim().Equals(""))
                        {
                            datetime = responseQuery.reader["time_send_otp"].ToString();
                        }
                    }

                    if (responseQuery.reader["count_send_otp"] != null)
                    {
                        if (!responseQuery.reader["count_send_otp"].ToString().Trim().Equals(""))
                        {
                            count_send = Int32.Parse(responseQuery.reader["count_send_otp"].ToString());
                        }
                    }

                }
                DatabaseService.Instance.getConnection().Close();
                DateTime myDate = DateTime.Parse(datetime);
                if (myDate.Year == now.Year && myDate.Month == now.Month && myDate.Day == now.Day)
                {
                    if (count_send >= countOtp)
                    {
                        this.appendError(1001, "Một ngày chỉ được gửi mã tối đa 10 lần");
                        response.error = this.error;
                        return response;
                    }
                }
                DateTime dateTime = DateTime.Now;
                long timeMsSinceNow = new DateTimeOffset(dateTime).ToUnixTimeMilliseconds();
                long timeMsSinceOTP = new DateTimeOffset(myDate).ToUnixTimeMilliseconds();
                if ((timeMsSinceOTP - timeMsSinceNow) / 1000 > 5)
                {
                    this.appendError(1, "Xin vui lòng chờ trong giây lát! (Mỗi giao dịch cách nhau 5 phút.");
                    response.error = this.error;
                    return response;
                }

                //Kiểm tra user xem đã verify phone chưa
                responseQuery = this.query("SELECT TOP 1 id FROM Users WHERE id =" + user_a.id + " AND verify_phone = 1");
                if (responseQuery.isError)
                {
                    this.appendError(1000, "Database check verify_phone lỗi ");
                    response.error = this.error;
                    return response;
                }
                if (!responseQuery.reader.HasRows)
                {
                    this.appendError(4, "User chưa verify phone");
                    response.error = this.error;
                    return response;
                }
                DatabaseService.Instance.getConnection().Close();

            }

            //Kiểm tra ví có được dùng trong merchant ko?
            Merchant_wallet merchant_Wallet = new Merchant_wallet();
            try
            {
                merchant_Wallet = this.getMerchantWalletByApp(paymentGateService.wallet_in);
            }
            catch (Exception ex)
            {
                this.appendError(1000, "Kiểm tra ví có được dùng trong Merchant ko? bị lỗi" );
                response.error = this.error;
                return response;
            }

            int wallet_in = paymentGateService.wallet_in;
            int wallet_out = paymentGateService.wallet_out;

            //Kiểm tra tham số trừ tiền có đúng config
            CashIORate cashIORate;
            try
            {
                cashIORate = this.checkValueConfig(paymentGateService, value);
            }
            catch (Exception ex)
            {
                this.appendError(1000, "Kiểm tra config lỗi " );
                response.error = this.error;
                return response;
            }

            if (cashIORate.cash_in == 0)
            {
                this.appendError(5, "Số tiền vượt quá config được chuyển ");
                response.error = this.error;
                return response;
            }
            double balance = 0;
            //Tính số tiền trong ví có đủ chuyển
            try
            {
                balance = this.getBalace(user_a, wallet_in);

            }
            catch (Exception ex)
            {
                this.appendError(1000, "Lỗi lấy tổng tiền trong ví user A" );
                response.error = this.error;
                return response;
            }

            if (balance < (value + paymentGateService.fee))
            {
                this.appendError(6, "Ví không đủ tiền giao dịch ");
                response.error = this.error;
                return response;
            }

            balance = balance - value;
            string s_balace = balance.ToString();
            s_balace = s_balace.Replace(",", ".");
            double profit = 0;
            double plus_money_profit = 0;
            plus_money_profit = double.Parse(value.ToString()) / cashIORate.cash_in * cashIORate.cash_out;
            plus_money_profit = Math.Round(plus_money_profit, 2, MidpointRounding.ToEven);
            if (paymentGateService.wallet_in == paymentGateService.wallet_out)
            {
                profit = value - plus_money_profit;
            }
            string s_profit = profit.ToString();
            s_profit = s_profit.Replace(",", ".");

            string s_plus_money_profit = plus_money_profit.ToString();
            s_plus_money_profit = s_plus_money_profit.Replace(",", ".");

            //Lưu số tiền trừ trong ví
            DateTime time = DateTime.Now;
            // Use current time
            string code = "";
            //if (paymentGateService.sms != 0)
            //{
            //    this.appendError(7, "Cổng thanh toán cần verify opt ");
            //    response.error = this.error;
            //    return response;
            //}
            //Lấy user merchant
            UserServices userServices = new UserServices(this.app_id);
            UserServiceResponse userServiceResponse = userServices.getUserByApp();
            if (userServiceResponse.error.isError)
            {
                this.appendError(1000, "Lấy user merchant lỗi.");
                response.error = this.error;
                return response;
            }

            string s_value = value.ToString();
            s_value = s_value.Replace(",", ".");

            if (paymentGateService.sms == 0)
            {
                try
                {
                    query = "INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) VALUES (@userid,@merchant_id," +
                        "@paymentGateService,@wallet_in,@s_balace,'minus','" + time.ToString(format) + "',@s_value,@user_bid,@tag,@message,@s_profit,@s_plus_money_profit)";
                    command = new SqlCommand(query, DatabaseService.Instance.getConnection());
                    command.Parameters.AddWithValue("@merchant_id", this.app_id);
                    command.Parameters.AddWithValue("@userid", user_a.id);
                    command.Parameters.AddWithValue("@paymentGateService", paymentGateService.id);
                    command.Parameters.AddWithValue("@wallet_in", wallet_in);
                    command.Parameters.AddWithValue("@s_balace", s_balace);
                    command.Parameters.AddWithValue("@s_value", s_value);
                    command.Parameters.AddWithValue("@user_bid", user_b.id);
                    command.Parameters.AddWithValue("@tag", tag);
                    command.Parameters.AddWithValue("@message", message);
                    command.Parameters.AddWithValue("@s_profit", s_profit);
                    command.Parameters.AddWithValue("@s_plus_money_profit", s_plus_money_profit);
                    this.querySqlParamInsert(command);
                    if (responseQuery.isError)
                    {
                        this.appendError(1000, "Database Inser trừ tiền lỗi " + "INSERT INTO Transactions(user_id, merchant_id, payment_gate, wallet_id, balance, type, time_creat, value, user_send, tag, message, profit, value_out) VALUES(" +
                    user_a.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_in + "," + s_balace + ",'minus','" + time.ToString(format) + "'," + s_value + "," + user_b.id + ",N'" + tag + "',N'" + message + "'," + s_profit + "," + s_plus_money_profit + ")");
                        response.error = this.error;
                        return response;
                    }
                    DatabaseService.Instance.getConnection().Close();

                    //Trừ phí cố định
                    if (paymentGateService.fee != 0)
                    {
                        balance = balance - paymentGateService.fee;
                        s_balace = balance.ToString();
                        s_balace = s_balace.Replace(",", ".");
                        
                        query = "INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) VALUES (@userid," +
                            "@merchant_id,@paymentGateService,@wallet_in,@s_balace,'minus','" + time.ToString(format) + "',@s_value,@user_bid,N'phecodinh',N'Trừ phí cố định',0,@s_value)";
                        command = new SqlCommand(query, DatabaseService.Instance.getConnection());
                        command.Parameters.AddWithValue("@merchant_id", this.app_id);
                        command.Parameters.AddWithValue("@userid", user_a.id);
                        command.Parameters.AddWithValue("@paymentGateService", paymentGateService.id);
                        command.Parameters.AddWithValue("@wallet_in", wallet_in);
                        command.Parameters.AddWithValue("@s_balace", s_balace);
                        command.Parameters.AddWithValue("@s_value", paymentGateService.fee);
                        command.Parameters.AddWithValue("@user_bid", userServiceResponse.info.id);
                        command.Parameters.AddWithValue("@tag", tag);
                        command.Parameters.AddWithValue("@message", message);
                        command.Parameters.AddWithValue("@s_profit", s_profit);
                        command.Parameters.AddWithValue("@s_plus_money_profit", s_plus_money_profit);
                        this.querySqlParamInsert(command);


                        if (responseQuery.isError)
                        {
                            this.appendError(1000, "INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) VALUES (" +
user_a.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_in + "," + s_balace + ",'minus','" + time.ToString(format) + "'," + paymentGateService.fee + "," + userServiceResponse.info.id + ",N'phecodinh',N'Trừ phí cố định',0," + paymentGateService.fee + ")");
                            response.error = this.error;
                            return response;
                        }
                        DatabaseService.Instance.getConnection().Close();
                    }
                }
                catch (Exception ex)
                {
                    this.appendError(1000, "Database Inser trừ tiền lỗi " + "INSERT INTO Transactions(user_id, merchant_id, payment_gate, wallet_id, balance, type, time_creat, value, user_send, tag, message, profit, value_out) VALUES(" +
user_a.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_in + "," + s_balace + ",'minus','" + time.ToString(format) + "'," + s_value + "," + user_b.id + ",N'" + tag + "',N'" + message + "'," + s_profit + "," + s_plus_money_profit + ")");
                    response.error = this.error;
                    return response;
                }
                double balance_merchant = 0;
                string s_balance_merchant = "";
                //Cộng phế cho merchant
                if (paymentGateService.wallet_in == paymentGateService.wallet_out && profit != 0)
                {
                    balance_merchant = this.getBalace(userServiceResponse.info, wallet_in);
                    balance_merchant = balance_merchant + profit;

                    s_balance_merchant = balance_merchant.ToString();
                    s_balance_merchant = s_balance_merchant.Replace(",", ".");

                    try
                    {
                        responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) VALUES (" +
userServiceResponse.info.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_in + "," + s_balance_merchant + ",'plus','" + time.ToString(format) + "'," + s_profit + "," + user_a.id + ",N'phe',N'Phế nhận được giao dịch',0," + profit + ")");

                        if (responseQuery.isError)
                        {
                            this.appendError(1000, "Database Inser update phế cho merchant lỗi " + "INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) VALUES (" +
                        userServiceResponse.info.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_in + "," + s_balance_merchant + ",'plus','" + time.ToString(format) + "'," + s_profit + "," + user_a.id + ",N'phe',N'Phế nhận được giao dịch',0," + s_profit + ")");
                            response.error = this.error;
                            return response;
                        }
                        DatabaseService.Instance.getConnection().Close();
                    }
                    catch (Exception ex)
                    {
                        this.appendError(1000, "Database Inser update phế cho merchant lỗi " + "INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) VALUES (" +
userServiceResponse.info.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_in + "," + s_balance_merchant + ",'plus','" + time.ToString(format) + "'," + s_profit + "," + user_a.id + ",N'phe',N'Phế nhận được giao dịch',0," + s_profit + ")");
                        response.error = this.error;
                        return response;
                    }

                }
                //Cộng phí cố định cho merchant
                if (paymentGateService.fee != 0)
                {
                    balance_merchant = this.getBalace(userServiceResponse.info, wallet_in);
                    balance_merchant = balance_merchant + paymentGateService.fee;

                    s_balance_merchant = balance_merchant.ToString();
                    s_balance_merchant = s_balance_merchant.Replace(",", ".");
                    try
                    {
                        responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) VALUES (" +
userServiceResponse.info.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_in + "," + s_balance_merchant + ",'plus','" + time.ToString(format) + "','" + paymentGateService.fee + "'," + user_a.id + ",N'phecodinh',N'Phế cố định nhận được giao dịch',0," + paymentGateService.fee + ")");
                        if (responseQuery.isError)
                        {
                            this.appendError(1000, "Database Inser update phế cho merchant lỗi " + "INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) VALUES (" +
                        userServiceResponse.info.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_in + "," + s_balance_merchant + ",'plus','" + time.ToString(format) + "','" + paymentGateService.fee + "'," + user_a.id + ",N'phecodinh',N'Phế cố định nhận được giao dịch',0," + paymentGateService.fee + ")");
                            response.error = this.error;
                            return response;
                        }
                        DatabaseService.Instance.getConnection().Close();
                    }
                    catch (Exception ex)
                    {
                        this.appendError(1000, "Database Inser update phế cho merchant lỗi " + "INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) VALUES (" +
userServiceResponse.info.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_in + "," + s_balance_merchant + ",'plus','" + time.ToString(format) + "'," + s_profit + "," + user_a.id + ",N'phecodinh',N'Phế nhận được giao dịch',0," + paymentGateService.fee + ")");
                        response.error = this.error;
                        return response;
                    }
                }
            }
            else
            {
                code = this.render_code();
                try
                {
                    responseQuery = this.queryInsert("INSERT INTO UserMoneyTransferCommit(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,code,profit,value_out) VALUES (" +
user_a.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_in + "," + s_balace + ",'minus','" + time.ToString(format) + "'," + s_value + "," + user_b.id + ",N'" + tag + "',N'" + message + "','" + code + "'," + s_profit + "," + s_plus_money_profit + ")");
                    if (responseQuery.isError)
                    {
                        this.appendError(1000, "Database Inser trừ tiền lỗi " + "INSERT INTO UserMoneyTransferCommit(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,code,profit,value_out) VALUES (" +
user_a.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_in + "," + s_balace + ",'minus','" + time.ToString(format) + "'," + s_value + "," + user_b.id + ",N'" + tag + "',N'" + message + "','" + code + "'," + s_profit + "," + s_plus_money_profit + ")");
                        response.error = this.error;
                        return response;
                    }
                    DatabaseService.Instance.getConnection().Close();
                }
                catch (Exception ex)
                {
                    this.appendError(1000, "Database Inser trừ tiền lỗi " + "INSERT INTO UserMoneyTransferCommit(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,code,profit,value_out) VALUES (" +
user_a.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_in + "," + s_balace + ",'minus','" + time.ToString(format) + "'," + s_value + "," + user_b.id + ",N'" + tag + "',N'" + message + "','" + code + "'," + s_profit + "," + s_plus_money_profit + ")");
                    response.error = this.error;
                    return response;
                }

            }


            //Tính số tiền trong ví để cộng
            double balance_out = 0;
            try
            {
                balance_out = this.getBalace(user_b, wallet_out);
            }
            catch (Exception ex)
            {
                this.appendError(1000, "Lỗi lấy tổng tiền trong ví user B " );
                response.error = this.error;
                return response;
            }
            double plus_money = 0;
            string s_balance_out = "";
            string s_plus_money = "";
            try
            {
                plus_money = double.Parse(value.ToString()) / cashIORate.cash_in * cashIORate.cash_out;
                plus_money = Math.Round(plus_money, 2, MidpointRounding.ToEven);
                balance_out = Math.Round(balance_out, 2, MidpointRounding.ToEven) + plus_money;

                s_balance_out = balance_out.ToString();
                s_balance_out = s_balance_out.Replace(",", ".");

                s_plus_money = plus_money.ToString();
                s_plus_money = s_plus_money.Replace(",", ".");
            }
            catch (Exception ex)
            {
                this.appendError(1000, "Lỗi lấy tính toán số tiền để cộng ");
                response.error = this.error;
                return response;
            }

            //Lưu số tiền cộng trong ví
            if (paymentGateService.sms == 0)
            {
                try
                {
                    responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,value_out) VALUES (" +
    user_b.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_out + "," + s_balance_out + ",'plus','" + time.ToString(format) + "'," + s_plus_money + "," + user_a.id + ",N'" + tag + "',N'" + message + "'," + s_value + ")");
                    if (responseQuery.isError)
                    {
                        this.appendError(1000, "Database Insert thêm tiền lỗi " + "INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,value_out) VALUES (" +
    user_b.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_out + "," + s_balance_out + ",'plus','" + time.ToString(format) + "'," + s_plus_money + "," + user_a.id + ",N'" + tag + "',N'" + message + "'," + s_value + ")");
                        response.error = this.error;
                        return response;
                    }
                    DatabaseService.Instance.getConnection().Close();
                }
                catch (Exception ex)
                {
                    this.appendError(1000, "Database Insert thêm tiền lỗi " + "INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,value_out) VALUES (" +
    user_b.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_out + "," + s_balance_out + ",'plus','" + time.ToString(format) + "'," + s_plus_money + "," + user_a.id + ",N'" + tag + "',N'" + message + "'," + s_value + ")");
                    response.error = this.error;
                    return response;
                }
            }
            else
            {
                try
                {
                    responseQuery = this.queryInsert("INSERT INTO UserMoneyTransferCommit(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,code,value_out) VALUES (" +
 user_b.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_out + "," + s_balance_out + ",'plus','" + time.ToString(format) + "'," + s_plus_money + "," + user_a.id + ",N'" + tag + "',N'" + message + "','" + code + "'," + s_value + ")");
                    if (responseQuery.isError)
                    {
                        this.appendError(1000, "Database Insert thêm tiền lỗi " + "INSERT INTO UserMoneyTransferCommit(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,code,value_out) VALUES (" +
 user_b.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_out + "," + s_balance_out + ",'plus','" + time.ToString(format) + "'," + s_plus_money + "," + user_a.id + ",N'" + tag + "',N'" + message + "','" + code + "'," + s_value + ")");
                        response.error = this.error;
                        return response;
                    }
                    DatabaseService.Instance.getConnection().Close();
                }
                catch (Exception ex)
                {
                    this.appendError(1000, "Database Insert thêm tiền lỗi " + "INSERT INTO UserMoneyTransferCommit(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,code,value_out) VALUES (" +
 user_b.id + "," + this.app_id + "," + paymentGateService.id + "," + wallet_out + "," + s_balance_out + ",'plus','" + time.ToString(format) + "'," + s_plus_money + "," + user_a.id + ",N'" + tag + "',N'" + message + "','" + code + "'," + s_value + ")");
                    response.error = this.error;
                    return response;

                }


                responseQuery = this.query("DELETE FROM OTP_user WHERE user_id='" + user_a.id + "' AND type = '0'");
                if (responseQuery.isError)
                {
                    this.appendError(1000, "Database delete history OTP");
                    response.error = this.error;
                    return response;
                }
                DatabaseService.Instance.getConnection().Close();
                try
                {
                    responseQuery = this.queryInsert("INSERT INTO OTP_user(user_id,type,otp,time_creat) VALUES(" + user_a.id + ",0,'" + code + "','" + time.ToString(format) + "')");
                    if (responseQuery.isError)
                    {
                        this.appendError(1000, "Database Error Insert OTP " + "INSERT INTO OTP_user(user_id,type,otp,time_creat) VALUES(" + user_a.id + ",0,'" + code + "','" + time.ToString(format) + "')");
                        response.error = this.error;
                        return response;
                    }
                    DatabaseService.Instance.getConnection().Close();
                }
                catch (Exception ex)
                {
                    this.appendError(1000, "Database Error Insert OTP " + "INSERT INTO OTP_user(user_id,type,otp,time_creat) VALUES(" + user_a.id + ",0,'" + code + "','" + time.ToString(format) + "')");
                    response.error = this.error;
                    return response;
                }


                //Lấy số phone User để gửi
                responseQuery = this.query("SELECT TOP 1 id,phone FROM Users WHERE id =" + user_a.id + " AND verify_phone = 1 ");
                if (responseQuery.isError)
                {
                    this.appendError(1000, "Database check verify_phone lỗi ");
                    response.error = this.error;
                    return response;
                }
                string phone = "";
                while (responseQuery.reader.Read())
                {
                    phone = responseQuery.reader["phone"].ToString().Trim();
                }
                DatabaseService.Instance.getConnection().Close();

                count_send++;
                responseQuery = this.query("UPDATE Users SET time_send_otp='" + now.ToString(format) + "',count_send_otp='" + count_send + "',code_sms='" + code + "' WHERE id =" + user_a.id);
                DatabaseService.Instance.getConnection().Close();

                phone = phone.Substring(1, phone.Length - 1);
                SendSMSHelper sendSMSHelper = new SendSMSHelper();
                string result = sendSMSHelper.send("+84" + phone, code, "mã xác nhận của bạn là: ");
            }
            response.minus = value;
            response.plus = plus_money;
            return response;
        }

        //---nhap giftcode
        public GiftcodeResponse checkGiftcode(UserModel user, string code, string paymentgate_code)
        {
            var userResponse = new GiftcodeResponse();
            if (this.app_id == 0)
            {
                this.appendError(1001, "App chưa được chọn");
                userResponse.error = this.error;
                return userResponse;
            }
            if (code.Equals(""))
            {
                this.appendError(-1, "Bạn chưa nhập giftcode");
                userResponse.error = this.error;
                return userResponse;
            }
            ResponseQuery responseQuery = new ResponseQuery();

            Campaign campaign = new Campaign();
            Giftcode giftcode = new Giftcode();
            //---lấy giftcode
            try
            {
                string quertFindCode = "SELECT TOP 1 * FROM Giftcode WHERE code = '" + code + "'";
                responseQuery = this.query(quertFindCode);
                if (!responseQuery.reader.HasRows)
                {
                    DatabaseService.Instance.getConnection().Close();
                    this.appendError(10, "Giftcode không tồn tại hoặc nhập sai giftcode!!!");
                    userResponse.error = this.error;
                    return userResponse;
                }
                while (responseQuery.reader.Read())
                {
                    giftcode.id = Int32.Parse(responseQuery.reader["id"].ToString());
                    giftcode.code = responseQuery.reader["code"].ToString();
                    giftcode.value = Int32.Parse(responseQuery.reader["value"].ToString());
                    giftcode.is_active = Int32.Parse(responseQuery.reader["is_active"].ToString());
                    giftcode.campaign_id = Int32.Parse(responseQuery.reader["campaign_id"].ToString());
                    giftcode.user_id = Int32.Parse(responseQuery.reader["user_id"].ToString());
                }
                DatabaseService.Instance.getConnection().Close();
            }
            catch (SqlException)
            {
                DatabaseService.Instance.getConnection().Close();
                this.appendError(9, "Sai mã giftcode.");
                userResponse.error = this.error;
                return userResponse;
            }
            //---kiểm tra tiền giftcode có âm không
            if (giftcode.value < 0)
            {
                this.appendError(8, "Không nạp được giftcode âm.");
                userResponse.error = this.error;
                return userResponse;
            }
            //---kiểm tra user đã nạp thẻ nào chưa
            string quertFindCode1 = "SELECT TOP 1 id FROM Giftcode WHERE user_id = " + user.id + " AND campaign_id=" + giftcode.campaign_id;
            responseQuery = this.query(quertFindCode1);
            if (responseQuery.reader.HasRows)
            {
                DatabaseService.Instance.getConnection().Close();
                this.appendError(8, "Bạn đã sử dụng 1 giftcode trong chiến dịch này, không thể sử dụng thêm nữa!!!");
                userResponse.error = this.error;
                return userResponse;
            }
            DatabaseService.Instance.getConnection().Close();
            //---lấy campaign
            try
            {
                string querFinCampaign = "SELECT TOP 1 * FROM Campaign WHERE id = " + giftcode.campaign_id;
                responseQuery = this.query(querFinCampaign);
                while (responseQuery.reader.Read())
                {
                    campaign.id = Int32.Parse(responseQuery.reader["id"].ToString());
                    campaign.name = responseQuery.reader["name"].ToString();
                    campaign.merchant_id = Int32.Parse(responseQuery.reader["merchant_id"].ToString());
                    campaign.start_time = responseQuery.reader["start_time"].ToString();
                    campaign.end_time = responseQuery.reader["end_time"].ToString();
                }
                DatabaseService.Instance.getConnection().Close();
            }
            catch (SqlException)
            {
                DatabaseService.Instance.getConnection().Close();
                this.appendError(7, "Chưa chạy chiến dịch này");
                userResponse.error = this.error;
                return userResponse;
            }
            //Kiểm tra thẻ thành công
            DateTime d1, d2;
            if ((DateTime.TryParse(campaign.start_time, out d1) &&
                DateTime.Now < d1) || (DateTime.TryParse(campaign.end_time, out d2) &&
                DateTime.Now > d2))
            {
                this.appendError(7, "Chưa chạy chiến dịch này");
                userResponse.error = this.error;
                return userResponse;

            }
            else
            {
                if (giftcode.is_active == 1)
                {
                    this.appendError(6, "Giftcode này đã được sử dụng");
                    userResponse.error = this.error;
                    return userResponse;
                }

            }

            //Kiểm tra thẻ thành công

            //Kieemr tra cong thanh toan
            PaymentGate paymentGateService = this.getPaymentGate(paymentgate_code);
            if (paymentGateService.id == 0)
            {
                this.appendError(5, "Cổng thanh toán không tồn tại.");
                userResponse.error = this.error;
                return userResponse;
            }

            if (!paymentGateService.status_active.Trim().Equals("active"))
            {
                this.appendError(4, "Cổng thanh toán đang bị khóa");
                userResponse.error = this.error;
                return userResponse;
            }

            //Kiểm tra tham số trừ tiền có đúng config
            CashIORate cashIORate;
            try
            {
                cashIORate = this.checkValueConfig(paymentGateService, giftcode.value);
            }
            catch (Exception ex)
            {
                this.appendError(3, "Kiểm tra config lỗi");
                userResponse.error = this.error;
                return userResponse;
            }

            if (cashIORate.cash_in == 0)
            {
                this.appendError(2, "Số tiền vượt quá config được chuyển");
                userResponse.error = this.error;
                return userResponse;
            }

            //---lay merchant
            var merchant_username = "";
            responseQuery = this.query("SELECT TOP 1 username FROM Merchant WHERE id=" + this.app_id);
            while (responseQuery.reader.Read())
            {
                merchant_username = responseQuery.reader["username"].ToString();
            }
            DatabaseService.Instance.getConnection().Close();

            var user_merchant = new UserModel();
            responseQuery = this.query("SELECT TOP 1 id FROM Users WHERE username='" + merchant_username + "'");
            while (responseQuery.reader.Read())
            {
                user_merchant.id = Convert.ToInt32(responseQuery.reader["id"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();

            // Update tiên cho merchant
            double balance = this.getBalace(user_merchant, paymentGateService.wallet_in);
            double profit = 0;
            double plus_money_profit = 0;
            plus_money_profit = double.Parse(giftcode.value.ToString()) / cashIORate.cash_in * cashIORate.cash_out;
            plus_money_profit = Math.Round(plus_money_profit, 2, MidpointRounding.ToEven);

            balance = balance - plus_money_profit;
            DateTime time = DateTime.Now;
            // Use current time
            string format = "yyyy-MM-dd HH:mm:ss";
            responseQuery = this.query("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) " +
                "VALUES (" + user_merchant.id + "," + this.app_id + "," + paymentGateService.id + ",0," + balance + ",'minus','" + time.ToString(format) + "'," + giftcode.value + "," + user.id + ",N'giftcode',N'User nạp giftcode'," + profit + "," + plus_money_profit + ")");
            DatabaseService.Instance.getConnection().Close();

            //Update tiền cho user
            double balance_user = this.getBalace(user, paymentGateService.wallet_out);
            balance_user = balance_user + plus_money_profit;

            responseQuery = this.query("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) " +
    "VALUES (" + user.id + "," + this.app_id + "," + paymentGateService.id + ",0," + balance_user + ",'plus','" + time.ToString(format) + "'," + giftcode.value + "," + user_merchant.id + ",N'giftcode',N'User nạp giftcode'," + profit + "," + plus_money_profit + ")");

            DatabaseService.Instance.getConnection().Close();

            //Tính số tiền trong ví để cộng
            double balance_out = 0;
            try
            {
                balance_out = this.getBalace(user, paymentGateService.wallet_out);
            }
            catch (Exception)
            {
                this.appendError(1, "Lỗi lấy tổng tiền trong ví user để cộng");
                userResponse.error = this.error;
                return userResponse;
            }

            balance_out = balance_out + plus_money_profit;
            responseQuery = this.query("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message) VALUES (" +
    user.id + "," + this.app_id + "," + paymentGateService.id + "," + paymentGateService.wallet_out + "," + balance_out + ",'plus','" + time.ToString(format) + "'," + plus_money_profit + "," + user_merchant.id + ",N'giftcode',N'Nạp Giftcode')");
            DatabaseService.Instance.getConnection().Close();

            //---gạch thẻ giftcode 
            string query = "UPDATE Giftcode SET is_active = 1, user_id = " + user.id + " WHERE id=" + giftcode.id;
            responseQuery = this.query(query);
            DatabaseService.Instance.getConnection().Close();

            userResponse.plusCoin = plus_money_profit;
            userResponse.msg = "Nạp Giftcode thành công, bạn nhận được " + plus_money_profit + " Coin";
            return userResponse;
        }

        public Rwithdrawal_UserResponse get_rwithdrawal_User(string code)
        {
            Rwithdrawal_UserResponse rwithdrawal_User = new Rwithdrawal_UserResponse();
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT " +
                "id," +
                "create_time," +
                "agency_id," +
                "value," +
                "wallet_id," +
                "(SELECT TOP 1 name FROM Wallet WHERE id = Rwithdrawal_User.wallet_id ) as 'wallet_name'," +
                "(SELECT TOP 1 title FROM Agency WHERE id = Rwithdrawal_User.agency_id ) as 'agency_name'" +
                " FROM Rwithdrawal_User WHERE code ='" + code.Trim() + "'");
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                rwithdrawal_User.error = this.error;
                return rwithdrawal_User;
            }
            Rwithdrawal_User rwithdrawal = new Rwithdrawal_User();
            int count = 0;
            while (responseQuery.reader.Read())
            {
                count++;
                rwithdrawal.id = Int32.Parse(responseQuery.reader["id"].ToString().Trim());
                rwithdrawal.create_time = DateTime.Parse(responseQuery.reader["create_time"].ToString().Trim());
                rwithdrawal.wallet_id = Int32.Parse(responseQuery.reader["wallet_id"].ToString().Trim());
                rwithdrawal.wallet_name = responseQuery.reader["wallet_name"].ToString().Trim();
                rwithdrawal.agency_id = Int32.Parse(responseQuery.reader["agency_id"].ToString().Trim());
                rwithdrawal.agency_name = responseQuery.reader["agency_name"].ToString().Trim();
                rwithdrawal.value = Double.Parse(responseQuery.reader["value"].ToString().Trim());
            }
            DatabaseService.Instance.getConnection().Close();
            if (count == 0)
            {
                this.appendError(1001, "Mã code không chính xác");
                rwithdrawal_User.error = this.error;
                return rwithdrawal_User;
            }
            //responseQuery = this.query("UPDATE Rwithdrawal_User SET user_id = "+ user.id + ",approved = 2 WHERE code ='" + code.Trim() + "'");

            rwithdrawal_User.rwithdrawal_user = rwithdrawal;
            return rwithdrawal_User;
        }

        public Rwithdrawal_UserResponse get_rwithdrawal_User(int id)
        {
            Rwithdrawal_UserResponse rwithdrawal_User = new Rwithdrawal_UserResponse();
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT " +
                "id," +
                "create_time," +
                "agency_id," +
                "value," +
                "wallet_id," +
                "(SELECT TOP 1 name FROM Wallet WHERE id = Rwithdrawal_User.wallet_id ) as 'wallet_name'," +
                "(SELECT TOP 1 title FROM Agency WHERE id = Rwithdrawal_User.agency_id ) as 'agency_name'" +
                " FROM Rwithdrawal_User WHERE id =" + id);
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                rwithdrawal_User.error = this.error;
                return rwithdrawal_User;
            }
            Rwithdrawal_User rwithdrawal = new Rwithdrawal_User();
            int count = 0;
            while (responseQuery.reader.Read())
            {
                count++;
                rwithdrawal.id = Int32.Parse(responseQuery.reader["id"].ToString().Trim());
                rwithdrawal.create_time = DateTime.Parse(responseQuery.reader["create_time"].ToString().Trim());
                rwithdrawal.wallet_id = Int32.Parse(responseQuery.reader["wallet_id"].ToString().Trim());
                rwithdrawal.wallet_name = responseQuery.reader["wallet_name"].ToString().Trim();
                rwithdrawal.agency_id = Int32.Parse(responseQuery.reader["agency_id"].ToString().Trim());
                rwithdrawal.agency_name = responseQuery.reader["agency_name"].ToString().Trim();
                rwithdrawal.value = Double.Parse(responseQuery.reader["value"].ToString().Trim());
            }
            DatabaseService.Instance.getConnection().Close();
            if (count == 0)
            {
                this.appendError(1001, "Không tìm được giao dịch user");
                rwithdrawal_User.error = this.error;
                return rwithdrawal_User;
            }
            //responseQuery = this.query("UPDATE Rwithdrawal_User SET user_id = "+ user.id + ",approved = 2 WHERE code ='" + code.Trim() + "'");

            rwithdrawal_User.rwithdrawal_user = rwithdrawal;
            return rwithdrawal_User;
        }

        //---user chuyển tiền agency theo flow tự động
        public TransactionServiceResponse tranferUserToAgency(UserModel user, double money, int agencyId, string reason, string password, bool checkWithdrawal)
        {

            TransactionServiceResponse transacResponse = new TransactionServiceResponse();
            if (this.app_id == 0)
            {
                this.appendError(1, "Chưa set app id");
                transacResponse.error = this.error;
                return transacResponse;
            }
            ResponseQuery responseQuery = new ResponseQuery();
            //---agency tạo yêu cầu chuyển tiền
            var walletID = 11;//hardcode --- vi' VSCoin
            if (money <= 0)
            {
                this.appendError(1, "Số tiền không được phép <= 0");
                transacResponse.error = this.error;
                return transacResponse;
            }

            //---check pass
            string queryCheckPassword = "SELECT TOP 1 id,password FROM Users WHERE id = " + user.id + " AND password = '" + CryptoHelper.MD5Hash(password, user.id.ToString()) + "'";
            responseQuery = this.query(queryCheckPassword);
            int countcheck = 0;
            while (responseQuery.reader.Read())
            {
                countcheck++;
            }
            DatabaseService.Instance.getConnection().Close();
            if (countcheck == 0)
            {
                this.appendError(2, "Mật khẩu không đúng.");
                transacResponse.error = this.error;
                return transacResponse;
            }

            if (checkWithdrawal)
            {
                ////---chống rút tiền
                //---tien`nap
                string queryChecknap = "SELECT SUM(value) as 'total'  FROM Transactions WHERE (tag='Agency+' or payment_gate=2066) AND user_id = " + user.id + " AND wallet_id = 11 AND type = 'plus'";
                responseQuery = this.query(queryChecknap);
                if (!responseQuery.reader.HasRows)
                {
                    DatabaseService.Instance.getConnection().Close();
                    this.appendError(2, "Bạn chưa nạp tiền, nên không được chuyển tiền.");
                    transacResponse.error = this.error;
                    return transacResponse;
                }
                long ballanceNap = 0;
                while (responseQuery.reader.Read())
                {
                    try
                    {
                        ballanceNap = (long)Convert.ToDouble(responseQuery.reader["total"].ToString().Trim());
                    }
                    catch (Exception)
                    {
                        DatabaseService.Instance.getConnection().Close();
                        this.appendError(2, "Bạn chưa nạp tiền, nên không được chuyển tiền.");
                        transacResponse.error = this.error;
                        return transacResponse;
                    }
                }
                if (ballanceNap == 0)
                {
                    DatabaseService.Instance.getConnection().Close();
                    this.appendError(2, "Bạn chưa nạp tiền, nên không được chuyển tiền.");
                    transacResponse.error = this.error;
                    return transacResponse;
                }
                DatabaseService.Instance.getConnection().Close();
                //---tien` rut'
                string queryCheckRut = "SELECT SUM(value) as 'total'  FROM Transactions WHERE user_id = " + user.id + " AND wallet_id = 11 AND type = 'minus' AND (payment_gate= 2061 OR payment_gate= 2072 OR payment_gate= 2082)";
                responseQuery = this.query(queryCheckRut);
                long ballanceRut = 0;
                if (!responseQuery.reader.HasRows)
                {
                    ballanceRut = 0;
                }
                while (responseQuery.reader.Read())
                {
                    try
                    {
                        ballanceRut = (long)Convert.ToDouble(responseQuery.reader["total"].ToString().Trim());
                    }
                    catch (Exception)
                    {

                    }
                }
                DatabaseService.Instance.getConnection().Close();
                //---tien` cho merchant
                var queryCheckTranMerchant = "SELECT SUM(value) as 'total'  FROM Transactions WHERE user_send = " + user.id + " AND user_id = 155 AND wallet_id = 11 AND type = 'plus' AND payment_gate= 1054";
                responseQuery = this.query(queryCheckTranMerchant);
                if (!responseQuery.reader.HasRows)
                {
                    DatabaseService.Instance.getConnection().Close();
                    this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                    transacResponse.error = this.error;
                    return transacResponse;
                }
                long ballanceMerchant = 0;
                while (responseQuery.reader.Read())
                {
                    try
                    {
                        ballanceMerchant = (long)Convert.ToDouble(responseQuery.reader["total"].ToString().Trim());
                    }
                    catch
                    {
                        DatabaseService.Instance.getConnection().Close();
                        this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                        transacResponse.error = this.error;
                        return transacResponse;
                    }
                }
                if (ballanceMerchant == 0)
                {
                    DatabaseService.Instance.getConnection().Close();
                    this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                    transacResponse.error = this.error;
                    return transacResponse;
                }
                DatabaseService.Instance.getConnection().Close();
                //---tien` merchant chuyen lai user
                var queryCheckTranFromMerchant = "SELECT SUM(value) as 'total'  FROM Transactions WHERE user_send = 155 AND user_id = " + user.id + " AND wallet_id = 11 AND type = 'plus' AND payment_gate= 1055";
                responseQuery = this.query(queryCheckTranFromMerchant);
                if (!responseQuery.reader.HasRows)
                {
                    DatabaseService.Instance.getConnection().Close();
                    this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                    transacResponse.error = this.error;
                    return transacResponse;
                }
                long ballanceFromMerchant = 0;
                while (responseQuery.reader.Read())
                {
                    try
                    {
                        ballanceFromMerchant = (long)Convert.ToDouble(responseQuery.reader["total"].ToString().Trim());
                    }
                    catch (Exception)
                    {
                        DatabaseService.Instance.getConnection().Close();
                        this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                        transacResponse.error = this.error;
                        return transacResponse;
                    }
                }
                if (ballanceFromMerchant == 0)
                {
                    DatabaseService.Instance.getConnection().Close();
                    this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                    transacResponse.error = this.error;
                    return transacResponse;
                }
                DatabaseService.Instance.getConnection().Close();
                //---tien` userToUser
                var queryUToU = "SELECT SUM(value) as 'total'  FROM Transactions WHERE user_id = " + user.id + " AND user_send != 155 AND wallet_id = 11 AND payment_gate=1058";
                responseQuery = this.query(queryUToU);

                long balanceUToU = 0;
                if (responseQuery.reader.HasRows)
                {
                    while (responseQuery.reader.Read())
                    {
                        try
                        {
                            balanceUToU = (long)Convert.ToDouble(responseQuery.reader["total"].ToString().Trim());
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
                DatabaseService.Instance.getConnection().Close();
                //---tinh' toan' chong'r rut' tien`
                double sysConf = 0;
                //if (ballanceNap < ballanceRut)
                //{
                //    sysConf = (double)ballanceMerchant / (balanceUToU + ballanceNap);
                //}
                //else
                //{
                sysConf = (double)ballanceMerchant / (balanceUToU + ballanceNap);
                //}
                Debug.WriteLine("sysConf: " + sysConf + " ballanceMerchant: " + ballanceMerchant + " balanceUToU: " + balanceUToU + " ballanceNap: " + ballanceNap + " ballanceFromMerchant: " + ballanceFromMerchant);
                if (sysConf < 0.1)
                {
                    this.appendError(4, "Bạn chưa đủ điều kiện để chuyển tiền. Hãy liên hệ với chăm sóc khách hàng để được giải quyết!");
                    transacResponse.error = this.error;
                    return transacResponse;
                }
                //---
            }
            DatabaseService.Instance.getConnection().Close();
            //---Kiêm tra thời gian rút gần nhất
            string queryStringCheckRwithdrawal = "SELECT TOP 1 id,confirm_time FROM Rwithdrawal_User WHERE user_id =" + user.id + " ORDER BY confirm_time DESC";
            responseQuery = this.query(queryStringCheckRwithdrawal);
            DateTime dateTime = DateTime.Now;
            DateTime time_creat = dateTime;
            int count_r = 0;
            if (responseQuery.reader.HasRows)
            {
                while (responseQuery.reader.Read())
                {
                    count_r++;
                    time_creat = DateTime.Parse(responseQuery.reader["confirm_time"].ToString().Trim());
                }
            }
            DatabaseService.Instance.getConnection().Close();

            long timeMsSinceNow = new DateTimeOffset(dateTime).ToUnixTimeMilliseconds();
            long timeMsSinceOTP = new DateTimeOffset(time_creat).ToUnixTimeMilliseconds();
            long count_min = (timeMsSinceNow - timeMsSinceOTP) / 1000 / 60;
            if (count_r > 0)
            {
                if (count_min < 5)
                {
                    this.appendError(5, "Xin vui lòng chờ trong giây lát! (Mỗi giao dịch cách nhau 5 phút. Thời gian còn lại của bạn là: " + (5 - count_min) + " phút)");
                    transacResponse.error = this.error;
                    return transacResponse;
                }
            }

            //Kiểm tra thời gian nạp
            string queryStringCheck = "SELECT TOP 1 " +
"id,time_creat" +
" FROM Transactions WHERE user_id =0 AND user_send = " + user.id + " AND wallet_id = 0 AND type = 'plus' ORDER BY time_creat DESC";
            responseQuery = this.query(queryStringCheck);
            int count_i = 0;
            while (responseQuery.reader.Read())
            {
                count_i++;
                var time_cr = responseQuery.reader["time_creat"].ToString().Trim();
                time_creat = DateTime.Parse(responseQuery.reader["time_creat"].ToString().Trim());
            }
            DatabaseService.Instance.getConnection().Close();

            long timeMsSinceNowIn = new DateTimeOffset(dateTime).ToUnixTimeMilliseconds();
            long timeMsSinceOTPIn = new DateTimeOffset(time_creat).ToUnixTimeMilliseconds();
            long count_min_in = (timeMsSinceNowIn - timeMsSinceOTPIn) / 1000 / 60;
            if (count_i > 0)
            {
                if (count_min_in < 5)
                {
                    this.appendError(5, "Xin vui lòng chờ trong giây lát! (Mỗi giao dịch cách nhau 5 phút. Thời gian còn lại của bạn là: " + (5 - count_min_in) + " phút)");
                    transacResponse.error = this.error;
                    return transacResponse;
                }
            }

            //tạo 1 giao dịch user-agency
            DateTime time = DateTime.Now;
            string format = "yyyy-MM-dd HH:mm:ss";

            string code = this.render_code();
            responseQuery = this.query("INSERT INTO Rwithdrawal_User(agency_id,user_id,create_time,wallet_id,paymentgate_id,approved,accounting_id,value,note,value_out,code,tag,status) VALUES (" +
    agencyId + ",0,'" + time.ToString(format) + "'," + walletID + ",0,0,0," + money + ",'" + reason + "',0,'" + code + "','',0)");
            DatabaseService.Instance.getConnection().Close();
            var rwithdrawal = get_rwithdrawal_User(code).rwithdrawal_user;

            //Tìm cổng thanh toán phù hợp
            string queryPaymentGate = "SELECT TOP 1 " +
                "PaymentGate.id as 'id'," +
                "PaymentGate.sms as 'sms'," +
                "PaymentGate.name as 'name'," +
                "CashIORate.cash_in as 'cash_in'," +
                "CashIORate.cash_out as 'cash_out' " +
                "FROM PaymentGate INNER JOIN CashIORate ON PaymentGate.id = CashIORate.payment_gate_id WHERE PaymentGate.merchant_id = " + this.app_id + " AND PaymentGate.type = 2 AND CashIORate.cash_in > " + rwithdrawal.value + " ";
            responseQuery = this.query(queryPaymentGate);
            PaymentGateCashIO paymentGateCashIO = new PaymentGateCashIO();
            while (responseQuery.reader.Read())
            {
                paymentGateCashIO.id = Int32.Parse(responseQuery.reader["id"].ToString().Trim());
                paymentGateCashIO.sms = Int32.Parse(responseQuery.reader["sms"].ToString().Trim());
                paymentGateCashIO.cash_in = Double.Parse(responseQuery.reader["cash_in"].ToString().Trim());
                paymentGateCashIO.cash_out = Double.Parse(responseQuery.reader["cash_out"].ToString().Trim());
                paymentGateCashIO.name = responseQuery.reader["name"].ToString().Trim();
            }
            DatabaseService.Instance.getConnection().Close();

            if (paymentGateCashIO.id == 0)
            {
                this.appendError(5, "Merchant không có cổng thanh toán nào phù hợp giao dịch.");
                transacResponse.error = this.error;
                return transacResponse;
            }
            double plus_money_profit = rwithdrawal.value / paymentGateCashIO.cash_in * paymentGateCashIO.cash_out;
            plus_money_profit = Math.Round(plus_money_profit, 2, MidpointRounding.ToEven);
            rwithdrawal.value_out = plus_money_profit;
            //Kiểm tra xem tiền trong ví có đủ
            double balance = this.getBalace(user, rwithdrawal.wallet_id);
            if (balance < rwithdrawal.value)
            {
                this.appendError(5, "Số tiền trong ví của bạn không đủ để thực hiện giao dịch này.");
                transacResponse.error = this.error;
                return transacResponse;
            }

            DateTime time1 = DateTime.Now;
            // Use current time
            this.query("UPDATE Rwithdrawal_User SET user_id = " + user.id + ",approved = 1,paymentgate_id=" + paymentGateCashIO.id + ",value_out=" + plus_money_profit + ",confirm_time='" + time1.ToString(format) + "' WHERE id =" + rwithdrawal.id);
            DatabaseService.Instance.getConnection().Close();

            //Trừ số tiền ví của user vào merchant
            bool is_update = updateBalaceWalletUser(rwithdrawal, agencyId, user, paymentGateCashIO, reason);
            if (!is_update)
            {
                this.appendError(6, "Xảy ra sự cố update ví");
                transacResponse.error = this.error;
                return transacResponse;
            }

            if (paymentGateCashIO.sms != 1)
            {
                bool is_update_accounting = updateBalaceWalletUserNoAccounting(rwithdrawal, agencyId, user, paymentGateCashIO, reason);

                transacResponse.minus = rwithdrawal.value_out;
                transacResponse.msg = "Bạn đã gửi tiền thành công.";
                return transacResponse;
            }
            else
            {
                transacResponse.minus = 0;
                transacResponse.msg = "Số tiền vượt quá quy định. Hãy đợi duyệt.";
                return transacResponse;
            }
        }

        private bool updateBalaceWalletUserNoAccounting(Rwithdrawal_User wi, int agencyId, UserModel user_item, PaymentGateCashIO paymentGateCashIO, string reason)
        {

            ResponseQuery responseQuery = new ResponseQuery();

            string format = "yyyy-MM-dd HH:mm:ss";
            var time = DateTime.Now;
            if (wi != null)
            {
                time = DateTime.Now;
                string queryUpdate = "UPDATE Rwithdrawal_User SET approved = 3, accounting_id=0, approved_time='" + time.ToString(format) + "' WHERE id = " + wi.id;
                responseQuery = this.query(queryUpdate);
                DatabaseService.Instance.getConnection().Close();
            }

            //Trừ tiền merchant tạm giữ
            //---get user merchant
            UserServices userServices = new UserServices(this.app_id);
            UserServiceResponse userServiceResponse = userServices.getUserByApp();
            var user_merchant = userServiceResponse.info;

            string querySelectTranMer = "SELECT TOP 1 id, balance FROM Transactions WHERE user_id = " + user_merchant.id + " AND transactions_type = 0 AND wallet_id= " + wi.wallet_id + " ORDER BY id DESC";
            responseQuery = this.query(querySelectTranMer);
            Transactions query_merchant = new Transactions();
            while (responseQuery.reader.Read())
            {
                query_merchant.id = Int32.Parse(responseQuery.reader["id"].ToString().Trim());
                query_merchant.balance = Double.Parse(responseQuery.reader["balance"].ToString().Trim());
            }
            DatabaseService.Instance.getConnection().Close();

            double userMoney_merchant = 0;
            if (query_merchant != null)
            {
                userMoney_merchant = query_merchant.balance;
            }
            userMoney_merchant = userMoney_merchant - wi.value;
            time = DateTime.Now;
            responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,value,transactions_type,user_send,tag,message,time_creat,value_out) VALUES (" + user_merchant.id + "," + this.app_id + ",0," +
wi.wallet_id + "," + userMoney_merchant + ",'minus'," + wi.value +
",0," + agencyId + ",N'ruttien',N'Trả tiền tạm giữ của user:" + user_item.username + " cho merchant ','" + time.ToString(format) + "'," + wi.value + ")");
            DatabaseService.Instance.getConnection().Close();

            //Lưu log Merchant gửi cho Agency
            time = DateTime.Now;
            responseQuery = this.queryInsert("INSERT INTO Log_Merchant_To_Agency(user_id,time,agency_id,value,message) VALUES (" +
user_merchant.id + ",'" + time.ToString(format) + "'," + agencyId + "," + wi.value + ",'Khi " + user_item.username + " chuyển tiền thành công" + "')");
            DatabaseService.Instance.getConnection().Close();

            //Cộng tiền cho agency
            string querySelectTranAgency = "SELECT TOP 1 id, balance FROM Transactions WHERE user_id = " + agencyId + " AND transactions_type = 2 AND wallet_id= " + wi.wallet_id + " ORDER BY id DESC";
            responseQuery = this.query(querySelectTranAgency);
            Transactions query_agency = new Transactions();
            while (responseQuery.reader.Read())
            {
                query_agency.id = Int32.Parse(responseQuery.reader["id"].ToString().Trim());
                query_agency.balance = Double.Parse(responseQuery.reader["balance"].ToString().Trim());
            }
            DatabaseService.Instance.getConnection().Close();

            double userMoney_agency = 0;
            if (query_agency != null)
            {
                userMoney_agency = query_agency.balance;
            }
            userMoney_agency = userMoney_agency + wi.value;
            time = DateTime.Now;
            responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,value,transactions_type,user_send,tag,message,time_creat,value_out) VALUES (" +
agencyId + ",0," + paymentGateCashIO.id + "," +
wi.wallet_id + "," + userMoney_agency + ",'plus'," + wi.value +
",2," + user_merchant.id + ",N'ruttien',N'Cộng tiền user:" + user_item.username + " chuyển tiền','" + time.ToString(format) + "'," + wi.value_out + ")");
            DatabaseService.Instance.getConnection().Close();

            //Update ví rút user
            string querySelectTranUser = "SELECT TOP 1 id, balance FROM Transactions WHERE user_id = " + user_item.id + " AND transactions_type = 0 AND type='minus' AND wallet_id= 0 ORDER BY id DESC";
            responseQuery = this.query(querySelectTranUser);
            Transactions query_user = new Transactions();
            while (responseQuery.reader.Read())
            {
                query_user.id = Int32.Parse(responseQuery.reader["id"].ToString().Trim());
                query_user.balance = Double.Parse(responseQuery.reader["balance"].ToString().Trim());
            }
            DatabaseService.Instance.getConnection().Close();
            double userMoney_user = 0;
            if (query_user != null)
            {
                userMoney_user = query_user.balance;
            }

            userMoney_user = userMoney_user + wi.value_out;
            time = DateTime.Now;
            responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,value,transactions_type,user_send,tag,message,time_creat,value_out) VALUES (" +
user_item.id + ",0," + paymentGateCashIO.id + ",0," + userMoney_user + ",'minus'," + wi.value +
",0,0,N'ruttien',N'" + reason + "', '" + time.ToString(format) + "'," + wi.value_out + ")");
            DatabaseService.Instance.getConnection().Close();

            //Update ví rút admin
            string querySelectTranAdmin = "SELECT TOP 1 id, balance FROM Transactions WHERE user_id = 0 AND type = 'minus' AND wallet_id= 0 ORDER BY id DESC";
            responseQuery = this.query(querySelectTranAdmin);
            Transactions query_admin = new Transactions();
            while (responseQuery.reader.Read())
            {
                query_admin.id = Int32.Parse(responseQuery.reader["id"].ToString().Trim());
                query_admin.balance = Double.Parse(responseQuery.reader["balance"].ToString().Trim());
            }
            DatabaseService.Instance.getConnection().Close();

            double userMoney_admin = 0;
            if (query_admin != null)
            {
                userMoney_admin = query_admin.balance;
            }

            userMoney_admin = userMoney_admin + wi.value_out;
            time = DateTime.Now;
            var query1 = "INSERT INTO Transactions(user_id, merchant_id, payment_gate, wallet_id, balance, type, value, transactions_type, user_send, tag, message, time_creat, value_out) VALUES(0,0," + paymentGateCashIO.id + ",0," + userMoney_admin + ",'minus'," + wi.value +
 ",0," + user_item.id + ",N'ruttien',N'" + reason + "', '" + time.ToString(format) + "'," + wi.value_out + ")";

            responseQuery = this.queryInsert(query1);
            DatabaseService.Instance.getConnection().Close();
            return true;
        }

        private bool updateBalaceWalletUser(Rwithdrawal_User wi, int agencyId, UserModel user_item, PaymentGateCashIO paymentGateCashIO, string reason)
        {
            ResponseQuery responseQuery = new ResponseQuery();
            string format = "yyyy-MM-dd HH:mm:ss";

            UserServices userServices = new UserServices(this.app_id);
            UserServiceResponse userServiceResponse = userServices.getUserByApp();
            var user_merchant = userServiceResponse.info;

            string query = "SELECT TOP 1 id, balance FROM Transactions WHERE user_id = " + user_item.id + " AND transactions_type = 0 AND wallet_id= " + wi.wallet_id + " ORDER BY id DESC";
            responseQuery = this.query(query);
            Transactions transaction = new Transactions();
            while (responseQuery.reader.Read())
            {
                transaction.id = Int32.Parse(responseQuery.reader["id"].ToString().Trim());
                transaction.balance = Double.Parse(responseQuery.reader["balance"].ToString().Trim());
            }
            DatabaseService.Instance.getConnection().Close();

            double userMoney = 0;
            if (query != null)
            {
                userMoney = transaction.balance;
            }
            if (userMoney < wi.value)
            {
                return false;
            }

            double moneyuserMinus = userMoney - wi.value;
            //Trừ vào ví user
            var time = DateTime.Now;
            responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,value,transactions_type,user_send,tag,message,time_creat,value_out) VALUES (" + user_item.id +
 "," + this.app_id + "," + paymentGateCashIO.id + "," + wi.wallet_id + "," + moneyuserMinus + ",'minus'," + wi.value +
",0," + user_merchant.id + ",N'ruttien',N'User gửi admin tạm giữ tiền', '" + time.ToString(format) + "'," + wi.value_out + ")");
            DatabaseService.Instance.getConnection().Close();

            //Cộng tiền merchant tạm giữ
            string querySelectTranMer = "SELECT TOP 1 id, balance FROM Transactions WHERE user_id = " + user_merchant.id + " AND transactions_type = 0 AND wallet_id= " + wi.wallet_id + " ORDER BY id DESC";
            responseQuery = this.query(querySelectTranMer);
            Transactions query_merchant = new Transactions();
            while (responseQuery.reader.Read())
            {
                query_merchant.id = Int32.Parse(responseQuery.reader["id"].ToString().Trim());
                query_merchant.balance = Double.Parse(responseQuery.reader["balance"].ToString().Trim());
            }

            double userMoney_merchant = 0;
            if (query_merchant != null)
            {
                userMoney_merchant = query_merchant.balance;
            }
            userMoney_merchant = userMoney_merchant + wi.value;

            time = DateTime.Now;
            responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,value,transactions_type,user_send,tag,message,time_creat,value_out) VALUES (" +
user_merchant.id + "," + this.app_id + "," + paymentGateCashIO.id + "," +
wi.wallet_id + "," + userMoney_merchant + ",'plus'," + wi.value +
",0," + user_item.id + ",N'ruttien',N'Tạm giữ coin bán cho agency','" + time.ToString(format) + "'," + wi.value + ")");
            DatabaseService.Instance.getConnection().Close();

            return true;
        }


        public ResponseBase confirm_rwithdrawal_User(UserModel user, string code, string paymentgate_code)
        {
            ResponseBase response = new ResponseBase();
            if (this.app_id == 0)
            {
                this.appendError(1, "Chưa set app id");
                response.error = this.error;
                return response;
            }

            ResponseQuery responseQuery = new ResponseQuery();

            PaymentGate paymentGateService = new PaymentGate();
            //Kiểm tra cổng thanh toán có tồn tại
            try
            {
                paymentGateService = this.getPaymentGate(paymentgate_code);
            }
            catch (Exception ex)
            {
                this.appendError(1001, "Get công thanh toán xảy ra lỗi ");
                response.error = this.error;
                return response;
            }

            if (paymentGateService.id == 0)
            {
                this.appendError(2, "Cổng thanh toán không tồn tại ");
                response.error = this.error;
                return response;
            }

            if (!paymentGateService.status_active.Trim().Equals("active"))
            {
                this.appendError(2, "Cổng thanh toán đang bị khóa ");
                response.error = this.error;
                return response;
            }


            responseQuery = this.query("SELECT " +
                            "id," +
                            "create_time," +
                            "agency_id," +
                            "value," +
                            "wallet_id," +
                            "(SELECT TOP 1 name FROM Wallet WHERE id = Rwithdrawal_User.wallet_id ) as 'wallet_name'," +
                            "(SELECT TOP 1 title FROM Agency WHERE id = Rwithdrawal_User.agency_id ) as 'agency_name'" +
                            " FROM Rwithdrawal_User WHERE code ='" + code.Trim() + "'");
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                response.error = this.error;
                return response;
            }
            Rwithdrawal_User rwithdrawal = new Rwithdrawal_User();
            int count = 0;
            while (responseQuery.reader.Read())
            {
                count++;
                rwithdrawal.id = Int32.Parse(responseQuery.reader["id"].ToString().Trim());
                rwithdrawal.create_time = DateTime.Parse(responseQuery.reader["create_time"].ToString().Trim());
                rwithdrawal.wallet_id = Int32.Parse(responseQuery.reader["wallet_id"].ToString().Trim());
                rwithdrawal.wallet_name = responseQuery.reader["wallet_name"].ToString().Trim();
                rwithdrawal.agency_id = Int32.Parse(responseQuery.reader["agency_id"].ToString().Trim());
                rwithdrawal.agency_name = responseQuery.reader["agency_name"].ToString().Trim();
                rwithdrawal.value = Double.Parse(responseQuery.reader["value"].ToString().Trim());
            }
            DatabaseService.Instance.getConnection().Close();
            if (count == 0)
            {
                this.appendError(1001, "Mã code không chính xác");
                response.error = this.error;
                return response;
            }

            //Kiểm tra tham số trừ tiền có đúng config
            CashIORate cashIORate;
            try
            {
                cashIORate = this.checkValueConfig(paymentGateService, Double.Parse(rwithdrawal.value.ToString()));
            }
            catch (Exception ex)
            {
                this.appendError(3, "Kiểm tra config lỗi " );
                response.error = this.error;
                return response;
            }

            if (cashIORate.cash_in == 0)
            {
                this.appendError(4, "Số tiền vượt quá config được chuyển ");
                response.error = this.error;
                return response;
            }


            double plus_money_profit = rwithdrawal.value / cashIORate.cash_in * cashIORate.cash_out;
            plus_money_profit = Math.Round(plus_money_profit, 2, MidpointRounding.ToEven);


            responseQuery = this.query("UPDATE Rwithdrawal_User SET user_id = " + user.id + ",approved = 1,paymentgate_id=" + paymentGateService.id + ",value_out=" + plus_money_profit + " WHERE code ='" + code.Trim() + "'");
            DatabaseService.Instance.getConnection().Close();
            return response;
        }
    }
}
