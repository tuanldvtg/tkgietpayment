﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment
{
   public class ConnectDatabase
    {
        //Constructor connect sql server
        public ConnectDatabase()
        {


        }

        public void connectDB(String ServerAddress, String DBName, String UserName, String Password)
        {
            if (DatabaseService.Instance.getConnection() == null)
            {
                DatabaseService.Instance.connect(ServerAddress, DBName, UserName, Password);
            }
        }

    }
}
