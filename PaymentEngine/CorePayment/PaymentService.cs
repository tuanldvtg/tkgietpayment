﻿using System;
using System.Linq;
using System.Text;
using CorePayment.Model;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net;
using System.Configuration;
using CorePayment.Base;
using CorePayment.Response;
using Newtonsoft.Json;
using CorePayment.Response.PaymentVT;
using BillingCard;
using BillingCard.Models;
using CorePayment.Helper;
using System.Diagnostics;

namespace CorePayment
{
    public class PaymentService : ServiceBase
    {
        public int app_id;

        public PaymentService(int id)
        {
            app_id = id;
        }


        private string get_accessToken()
        {
            string client_id = "487719171387-isfcshdgpfbhsurd0dbp5lbcn8rrj2qc.apps.googleusercontent.com";
            string client_secret = "LJpLH0e-XzagtEdCQnGk9xfT";
            string refresh_token = "1/JVGsbd3YUnbqLbQK-v9NwVEc_fCSLTRmbo4U8PsdZGg";

            var request = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");

            string postData = string.Format("grant_type=refresh_token&client_id={0}&client_secret={1}&refresh_token={2}", client_id, client_secret, refresh_token);

            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;


            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            Accesstoken accesstoken = new Accesstoken();
            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                accesstoken = JsonConvert.DeserializeObject<Accesstoken>(responseString);
            }
            catch (Exception)
            {
                return "";
            }

            return accesstoken.access_token;

        }
        private int getOrderAndroid(string purchaseToken, string packageName, string productID, string accesstoken)
        {
            try
            {
                WebClient n = new WebClient();
                var json = n.DownloadString("https://www.googleapis.com/androidpublisher/v2/applications/" + packageName + "/purchases/products/" + productID + "/tokens/" + purchaseToken + "?access_token=" + accesstoken);
                string valueOriginal = Convert.ToString(json);
                ResponseOrder responseOrder = JsonConvert.DeserializeObject<ResponseOrder>(valueOriginal);
                return responseOrder.purchaseState;
            }
            catch (Exception)
            {
                return 2;
            }

        }

        private double getPriceProduct(string packageName, string productID, string accesstoken)
        {
            try
            {
                WebClient n = new WebClient();
                var json = n.DownloadString("https://www.googleapis.com/androidpublisher/v2/applications/" + packageName + "/inappproducts/" + productID + "?access_token=" + accesstoken);
                string valueOriginal = Convert.ToString(json);
                ResponsePrice responseOrder = JsonConvert.DeserializeObject<ResponsePrice>(valueOriginal);
                return responseOrder.defaultPrice.priceMicros;
            }
            catch (Exception)
            {
                return 0;
            }
        }


        public string Get(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        public string PurchaseItem(string receiptData)
        {
            string returnmessage = "";
            try
            {
                // var json = "{ 'receipt-data': '" + receiptData + "'}";

                var json = new JObject(new JProperty("receipt-data", receiptData)).ToString();

                ASCIIEncoding ascii = new ASCIIEncoding();
                byte[] postBytes = Encoding.UTF8.GetBytes(json);

                //  HttpWebRequest request;
                var request = System.Net.HttpWebRequest.Create("https://sandbox.itunes.apple.com/verifyReceipt");
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = postBytes.Length;

                //Stream postStream = request.GetRequestStream();
                //postStream.Write(postBytes, 0, postBytes.Length);
                //postStream.Close();

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(postBytes, 0, postBytes.Length);
                    stream.Flush();
                }

                //  var sendresponse = (HttpWebResponse)request.GetResponse();

                var sendresponse = request.GetResponse();

                string sendresponsetext = "";
                using (var streamReader = new StreamReader(sendresponse.GetResponseStream()))
                {
                    sendresponsetext = streamReader.ReadToEnd().Trim();
                }
                returnmessage = sendresponsetext;

            }
            catch (Exception)
            {
                return "";
            }
            return returnmessage;
        }
        //Query lấy cổng thanh toán
        private PaymentGate getPaymentGate(string paymentgate_code)
        {
            PaymentGate paymentGateService = new PaymentGate();
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT TOP 1 * FROM PaymentGate WHERE code = '" + paymentgate_code + "' ORDER BY id DESC");
            int count_app = 0;
            while (responseQuery.reader.Read())
            {
                count_app++;
                paymentGateService.id = Int32.Parse(responseQuery.reader["id"].ToString());
                paymentGateService.name = responseQuery.reader["name"].ToString();
                paymentGateService.code = responseQuery.reader["code"].ToString();
                paymentGateService.wallet_in = Int32.Parse(responseQuery.reader["wallet_in"].ToString());
                paymentGateService.wallet_out = Int32.Parse(responseQuery.reader["wallet_out"].ToString());
                paymentGateService.status_active = responseQuery.reader["status_active"].ToString();
                paymentGateService.sms = Int32.Parse(responseQuery.reader["sms"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            return paymentGateService;
        }



        //Kiểm tra tham số trừ tiền có đúng config
        private CashIORate checkValueConfig(PaymentGate paymentGateService, double value)
        {
            CashIORate cashIORate = new CashIORate();
            ResponseQuery responseQuery = this.query("SELECT TOP 1 * FROM CashIORate WHERE payment_gate_id ='" + paymentGateService.id + "' AND cash_in >= '" + value + "' ORDER BY cash_in ASC ");
            while (responseQuery.reader.Read())
            {
                cashIORate.payment_gate_id = Int32.Parse(responseQuery.reader["payment_gate_id"].ToString());
                cashIORate.cash_out = Int32.Parse(responseQuery.reader["cash_out"].ToString());
                cashIORate.cash_in = Int32.Parse(responseQuery.reader["cash_in"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            return cashIORate;
        }
        //Lấy số tiền trong ví
        private double getBalace(int user_id, int wallet_in)
        {
            double balance = 0;
            ResponseQuery responseQuery = this.query("SELECT TOP 1 balance FROM Transactions WHERE wallet_id ='" + wallet_in + "' " +
                "AND user_id='" + user_id + "' AND (transactions_type = 0 OR transactions_type IS NULL) ORDER BY id DESC");
            while (responseQuery.reader.Read())
            {
                balance = double.Parse(responseQuery.reader["balance"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            return balance;
        }
        //Lấy số tiền trong ví
        private double getBalaceUser(int user_id, int wallet_in)
        {
            double balance = 0;
            ResponseQuery responseQuery = this.query("SELECT TOP 1 balance FROM Transactions WHERE wallet_id ='" + wallet_in + "' " +
                "AND user_id='" + user_id + "' AND (transactions_type = 0 OR transactions_type IS NULL) AND type= 'plus' ORDER BY id DESC");
            while (responseQuery.reader.Read())
            {
                balance = double.Parse(responseQuery.reader["balance"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            return balance;
        }

        //Lấy số tiền trong ví nạp của admin
        private double getBalaceInput()
        {
            double balance = 0;
            ResponseQuery responseQuery = this.query("SELECT TOP 1 balance FROM Transactions WHERE wallet_id =0 " +
                "AND user_id=0 AND type ='plus' ORDER BY id DESC");
            while (responseQuery.reader.Read())
            {
                balance = double.Parse(responseQuery.reader["balance"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            return balance;
        }

        public ResponseBase InAppPurchaseAndroid(string purchaseToken, UserModel user, string packageName, string productID, string paymentgate_code)
        {
            ResponseBase response = new ResponseBase();
            ResponseQuery responseQuery = new ResponseQuery();

            //Lấy access token
            string access_token = this.get_accessToken();
            if (access_token.Equals(""))
            {
                this.appendError(4, "Kiểm tra thông tin order lỗi.");
                response.error = this.error;
                return response;
            }
            //Kiểm tra đơn hàng thanh toán
            int state = this.getOrderAndroid(purchaseToken, packageName, productID, access_token);
            if (state != 0)
            {
                this.appendError(4, "Đơn hàng chưa được thanh toán.");
                response.error = this.error;
                return response;
            }
            //Lấy giá tiền sản phẩm
            double price = this.getPriceProduct(packageName, productID, access_token);
            if (price == 0)
            {
                this.appendError(4, "Lấy giá tiền nạp gặp lỗi.");
                response.error = this.error;
                return response;
            }
            price = price / 1000000;

            if (this.app_id == 0)
            {
                this.appendError(1, "Chưa set app id");
                response.error = this.error;
                return response;
            }

            //Kieemr tra cong thanh toan
            PaymentGate paymentGateService = this.getPaymentGate(paymentgate_code.Trim());
            if (paymentGateService.id == 0)
            {
                this.appendError(1, "Cổng thanh toán không tồn tại");
                response.error = this.error;
                return response;
            }


            if (!paymentGateService.status_active.Trim().Equals("active"))
            {
                this.appendError(2, "Cổng thanh toán đang bị khóa ");
                response.error = this.error;
                return response;
            }

            //Kiểm tra tham số trừ tiền có đúng config
            CashIORate cashIORate;
            try
            {
                cashIORate = this.checkValueConfig(paymentGateService, price);
            }
            catch (Exception ex)
            {
                this.appendError(3, "Kiểm tra config lỗi: " + ex.Message);
                response.error = this.error;
                return response;
            }

            if (cashIORate.cash_in == 0)
            {
                this.appendError(4, "Số tiền vượt quá config được chuyển ");
                response.error = this.error;
                return response;
            }


            //Update tiên cho admin
            double balance = this.getBalaceInput();
            double profit = 0;
            double plus_money_profit = 0;
            plus_money_profit = double.Parse(price.ToString()) / cashIORate.cash_in * cashIORate.cash_out;
            plus_money_profit = Math.Round(plus_money_profit, 2, MidpointRounding.ToEven);


            balance = balance + price;
            DateTime time = DateTime.Now;
            // Use current time
            string format = "yyyy-MM-dd HH:mm:ss";
            responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) " +
                "VALUES (0," + this.app_id + "," + paymentGateService.id + ",0," + balance + ",'plus','" + time.ToString(format) + "'," + price + "," + user.id + ",N'naptien',N'User nạp tiền'," + profit + "," + plus_money_profit + ")");
            if (responseQuery.isError)
            {
                this.appendError(7, "Database Inser trừ tiền lỗi ");
                response.error = this.error;
                return response;
            }
            DatabaseService.Instance.getConnection().Close();

            //Update tiền nạp cho user thống kê
            double balance_user = this.getBalace(user.id, 0);
            balance_user = balance_user + price;

            responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) " +
    "VALUES (" + user.id + "," + this.app_id + "," + paymentGateService.id + ",0," + balance_user + ",'plus','" + time.ToString(format) + "'," + price + ",0,N'naptien',N'User nạp tiền'," + profit + "," + plus_money_profit + ")");
            if (responseQuery.isError)
            {
                this.appendError(7, "Database Inser trừ tiền lỗi ");
                response.error = this.error;
                return response;
            }
            DatabaseService.Instance.getConnection().Close();


            //Tính số tiền trong ví để cộng
            double balance_out = 0;
            try
            {
                balance_out = this.getBalace(user.id, paymentGateService.wallet_out);
            }
            catch (Exception ex)
            {
                this.appendError(8, "Lỗi lấy tổng tiền trong ví user để cộng: " + ex.Message);
                response.error = this.error;
                return response;
            }

            balance_out = balance_out + plus_money_profit;
            responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message) VALUES (" +
    user.id + "," + this.app_id + "," + paymentGateService.id + "," + paymentGateService.wallet_out + "," + balance_out + ",'plus','" + time.ToString(format) + "'," + plus_money_profit + ",0,N'naptien',N'Nạp tiền')");
            if (responseQuery.isError)
            {
                this.appendError(10, "Database Insert thêm tiền lỗi ");
                response.error = this.error;
                return response;
            }
            DatabaseService.Instance.getConnection().Close();

            response.error.isError = false;
            return response;

            //using (WebClient webClient = new System.Net.WebClient())
            //{
            //    try
            //    {
            //        WebClient n = new WebClient();
            //        string url = "https://www.googleapis.com/androidpublisher/v2/applications/org.cocos2d.thebigbossclub/purchases/products/basic_1/tokens/ngmpdeeomdjjehdnhefmdbgb.AO-J1OyiG57xJSpQmwh-OcT5uh0zl3X20DRzLWnW2LXelLRp1Fm79d-JSYN3lRU3CXIj7ozaXSCJKVZgin9F1HOxuuVfTv2SP-RhIVcLR60LHQ-lqYFCQCFWNSyi2B1f97CscOjS5T89";
            //        var json = n.DownloadString(url);
            //        string valueOriginal = Convert.ToString(json);
            //        Console.WriteLine(json);
            //    }
            //    catch(Exception ex)
            //    {

            //    }

            //}
        }
        public ResponseBase InAppPurchaseIOS(string receiptData, UserModel user, int price, string paymentgate_code)
        {
            ResponseBase response = new ResponseBase();
            ResponseQuery responseQuery = new ResponseQuery();
            if (this.app_id == 0)
            {
                this.appendError(1, "Chưa set app id");
                response.error = this.error;
                return response;
            }
            //Kieemr tra cong thanh toan
            PaymentGate paymentGateService = this.getPaymentGate(paymentgate_code.Trim());
            if (paymentGateService.id == 0)
            {
                this.appendError(1, "Merchant chưa có cổng thanh toán nạp thẻ");
                response.error = this.error;
                return response;
            }


            if (!paymentGateService.status_active.Trim().Equals("active"))
            {
                this.appendError(2, "Cổng thanh toán đang bị khóa ");
                response.error = this.error;
                return response;
            }

            //Kiểm tra tham số trừ tiền có đúng config
            CashIORate cashIORate;
            try
            {
                cashIORate = this.checkValueConfig(paymentGateService, price);
            }
            catch (Exception ex)
            {
                this.appendError(3, "Kiểm tra config lỗi: " + ex.Message);
                response.error = this.error;
                return response;
            }

            if (cashIORate.cash_in == 0)
            {
                this.appendError(4, "Số tiền vượt quá config được chuyển ");
                response.error = this.error;
                return response;
            }


            //Update tiên cho admin
            double balance = this.getBalaceInput();
            double profit = 0;
            double plus_money_profit = 0;
            plus_money_profit = double.Parse(price.ToString()) / cashIORate.cash_in * cashIORate.cash_out;
            plus_money_profit = Math.Round(plus_money_profit, 2, MidpointRounding.ToEven);


            balance = balance + price;
            DateTime time = DateTime.Now;
            // Use current time
            string format = "yyyy-MM-dd HH:mm:ss";
            responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) " +
                "VALUES (0," + this.app_id + "," + paymentGateService.id + ",0," + balance + ",'plus','" + time.ToString(format) + "'," + price + "," + user.id + ",N'naptien',N'User nạp tiền'," + profit + "," + plus_money_profit + ")");
            if (responseQuery.isError)
            {
                this.appendError(7, "Database Inser trừ tiền lỗi ");
                response.error = this.error;
                return response;
            }
            DatabaseService.Instance.getConnection().Close();

            //Update tiền cho user
            double balance_user = this.getBalaceUser(user.id, 0);
            balance_user = balance_user + price;

            responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) " +
    "VALUES (" + user.id + "," + this.app_id + "," + paymentGateService.id + ",0," + balance_user + ",'plus','" + time.ToString(format) + "'," + price + ",0,N'naptien',N'User nạp tiền'," + profit + "," + plus_money_profit + ")");
            if (responseQuery.isError)
            {
                this.appendError(7, "Database Inser trừ tiền lỗi ");
                response.error = this.error;
                return response;
            }
            DatabaseService.Instance.getConnection().Close();


            //Tính số tiền trong ví để cộng
            double balance_out = 0;
            try
            {
                balance_out = this.getBalace(user.id, paymentGateService.wallet_out);
            }
            catch (Exception ex)
            {
                this.appendError(8, "Lỗi lấy tổng tiền trong ví user để cộng: " + ex.Message);
                response.error = this.error;
                return response;
            }

            balance_out = balance_out + plus_money_profit;
            responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message) VALUES (" +
    user.id + "," + this.app_id + "," + paymentGateService.id + "," + paymentGateService.wallet_out + "," + balance_out + ",'plus','" + time.ToString(format) + "'," + plus_money_profit + ",0,N'naptien',N'Nạp tiền')");
            if (responseQuery.isError)
            {
                this.appendError(10, "Database Insert thêm tiền lỗi ");
                response.error = this.error;
                return response;
            }
            DatabaseService.Instance.getConnection().Close();

            response.error.isError = false;
            return response;

            //using (WebClient webClient = new System.Net.WebClient())
            //{
            //    try
            //    {
            //        WebClient n = new WebClient();
            //        string url = "https://www.googleapis.com/androidpublisher/v2/applications/org.cocos2d.thebigbossclub/purchases/products/basic_1/tokens/ngmpdeeomdjjehdnhefmdbgb.AO-J1OyiG57xJSpQmwh-OcT5uh0zl3X20DRzLWnW2LXelLRp1Fm79d-JSYN3lRU3CXIj7ozaXSCJKVZgin9F1HOxuuVfTv2SP-RhIVcLR60LHQ-lqYFCQCFWNSyi2B1f97CscOjS5T89";
            //        var json = n.DownloadString(url);
            //        string valueOriginal = Convert.ToString(json);
            //        Console.WriteLine(json);
            //    }
            //    catch(Exception ex)
            //    {

            //    }

            //}
        }

        public bool inAppPurchaseIOS(string purchaseToken, int userID)
        {
            //throw new AndroidIAPurchaseException();
            throw new Exception("Not implement");

        }

        //---rut the cao
        public ResponseBeforeCallback callURLmobileCard(UserModel user, string typeCard, string cardValue, string password,bool checkWithdrawal)
        {
            var userResponse = new ResponseBeforeCallback();
            if (this.app_id == 0)
            {
                this.appendError(1001, "App chưa được chọn");
                userResponse.error = this.error;
                return userResponse;
            }
            ResponseQuery responseQuery = new ResponseQuery();

            //---xac thuc tai khoan dung pass
            string queryCheckPassword = "SELECT id FROM Users WHERE password = '" + CryptoHelper.MD5Hash(password, user.id.ToString()) + "'";
            responseQuery = this.query(queryCheckPassword);
            if (!responseQuery.reader.HasRows)
            {
                DatabaseService.Instance.getConnection().Close();
                this.appendError(1, "Mật khẩu không đúng.");
                userResponse.error = this.error;
                return userResponse;
            }
            DatabaseService.Instance.getConnection().Close();

            if (checkWithdrawal)
            {
                ////---chống rút tiền
                //---tien`nap
                string queryChecknap = "SELECT SUM(value) as 'total'  FROM Transactions WHERE (tag='Agency+' or payment_gate=2066) AND user_id = " + user.id + " AND wallet_id = 11 AND type = 'plus'";
                responseQuery = this.query(queryChecknap);
                if (!responseQuery.reader.HasRows)
                {
                    DatabaseService.Instance.getConnection().Close();
                    this.appendError(2, "Bạn chưa nạp tiền, nên không được chuyển tiền.");
                    userResponse.error = this.error;
                    return userResponse;
                }
                long ballanceNap = 0;
                while (responseQuery.reader.Read())
                {
                    try
                    {
                        ballanceNap = (long)Convert.ToDouble(responseQuery.reader["total"].ToString().Trim());
                    }
                    catch (Exception)
                    {
                        DatabaseService.Instance.getConnection().Close();
                        this.appendError(2, "Bạn chưa nạp tiền, nên không được chuyển tiền.");
                        userResponse.error = this.error;
                        return userResponse;
                    }
                }
                if (ballanceNap == 0)
                {
                    DatabaseService.Instance.getConnection().Close();
                    this.appendError(2, "Bạn chưa nạp tiền, nên không được chuyển tiền.");
                    userResponse.error = this.error;
                    return userResponse;
                }
                DatabaseService.Instance.getConnection().Close();
                //---tien` rut'
                string queryCheckRut = "SELECT SUM(value) as 'total'  FROM Transactions WHERE user_id = " + user.id + " AND wallet_id = 11 AND type = 'minus' AND (payment_gate= 2061 OR payment_gate= 2072 OR payment_gate= 2082)";
                responseQuery = this.query(queryCheckRut);
                long ballanceRut = 0;
                if (!responseQuery.reader.HasRows)
                {
                    ballanceRut = 0;
                }
                while (responseQuery.reader.Read())
                {
                    try
                    {
                        ballanceRut = (long)Convert.ToDouble(responseQuery.reader["total"].ToString().Trim());
                    }
                    catch (Exception)
                    {

                    }
                }
                DatabaseService.Instance.getConnection().Close();
                //---tien` cho merchant
                var queryCheckTranMerchant = "SELECT SUM(value) as 'total'  FROM Transactions WHERE user_send = " + user.id + " AND user_id = 155 AND wallet_id = 11 AND type = 'plus' AND payment_gate= 1054";
                responseQuery = this.query(queryCheckTranMerchant);
                if (!responseQuery.reader.HasRows)
                {
                    DatabaseService.Instance.getConnection().Close();
                    this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                    userResponse.error = this.error;
                    return userResponse;
                }
                long ballanceMerchant = 0;
                while (responseQuery.reader.Read())
                {
                    try
                    {
                        ballanceMerchant = (long)Convert.ToDouble(responseQuery.reader["total"].ToString().Trim());
                    }
                    catch
                    {
                        DatabaseService.Instance.getConnection().Close();
                        this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                        userResponse.error = this.error;
                        return userResponse;
                    }
                }
                if (ballanceMerchant == 0)
                {
                    DatabaseService.Instance.getConnection().Close();
                    this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                    userResponse.error = this.error;
                    return userResponse;
                }
                DatabaseService.Instance.getConnection().Close();
                //---tien` merchant chuyen lai user
                var queryCheckTranFromMerchant = "SELECT SUM(value) as 'total'  FROM Transactions WHERE user_send = 155 AND user_id = " + user.id + " AND wallet_id = 11 AND type = 'plus' AND payment_gate= 1055";
                responseQuery = this.query(queryCheckTranFromMerchant);
                if (!responseQuery.reader.HasRows)
                {
                    DatabaseService.Instance.getConnection().Close();
                    this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                    userResponse.error = this.error;
                    return userResponse;
                }
                long ballanceFromMerchant = 0;
                while (responseQuery.reader.Read())
                {
                    try
                    {
                        ballanceFromMerchant = (long)Convert.ToDouble(responseQuery.reader["total"].ToString().Trim());
                    }
                    catch (Exception)
                    {
                        DatabaseService.Instance.getConnection().Close();
                        this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                        userResponse.error = this.error;
                        return userResponse;
                    }
                }
                if (ballanceFromMerchant == 0)
                {
                    DatabaseService.Instance.getConnection().Close();
                    this.appendError(3, "Bạn chưa chơi game, nên không được chuyển tiền.");
                    userResponse.error = this.error;
                    return userResponse;
                }
                DatabaseService.Instance.getConnection().Close();
                //---tien` userToUser
                var queryUToU = "SELECT SUM(value) as 'total'  FROM Transactions WHERE user_id = " + user.id + " AND user_send != 155 AND wallet_id = 11 AND payment_gate=1058";
                responseQuery = this.query(queryUToU);

                long balanceUToU = 0;
                if (responseQuery.reader.HasRows)
                {
                    while (responseQuery.reader.Read())
                    {
                        try
                        {
                            balanceUToU = (long)Convert.ToDouble(responseQuery.reader["total"].ToString().Trim());
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
                DatabaseService.Instance.getConnection().Close();
                //---tinh' toan' chong'r rut' tien`
                double sysConf = 0;
                if (ballanceNap > ballanceRut)
                {
                    sysConf = (double)ballanceMerchant / (balanceUToU + ballanceRut);
                }
                else
                {
                    sysConf = (double)ballanceMerchant / (balanceUToU + ballanceNap);
                }
                Debug.WriteLine("sysConf: " + sysConf + " ballanceMerchant: " + ballanceMerchant + " balanceUToU: " + balanceUToU + " ballanceNap: " + ballanceNap + " ballanceFromMerchant: " + ballanceFromMerchant);
                if (sysConf < 0.1)
                {
                    this.appendError(4, "Bạn chưa đủ điều kiện để chuyển tiền. Hãy liên hệ với chăm sóc khách hàng để được giải quyết!");
                    userResponse.error = this.error;
                    return userResponse;
                }
                //---
            }
            ////---kiem tra 5 lan rut

            double intCardVal = 0;
            try
            {
                intCardVal = Convert.ToDouble(cardValue.ToString().Trim());
                Console.WriteLine("intCardVal: " + intCardVal);
            }
            catch (Exception e)
            {
                this.appendError(6, "Lỗi định dạng tiền!");
                userResponse.error = this.error;
                return userResponse;
            }
            //---kiểm tra tiền còn đủ ko
            long ubalance = 0;
            string queryUserBalane = "SELECT TOP 1 balance FROM Transactions WHERE user_id =" + user.id + " ORDER BY id DESC";
            responseQuery = this.query(queryUserBalane);
            if (!responseQuery.reader.HasRows)
            {
                DatabaseService.Instance.getConnection().Close();
                this.appendError(2, "Bạn chưa có tiền, nên không được rút tiền.");
                userResponse.error = this.error;
                return userResponse;
            }
            while (responseQuery.reader.Read())
            {
                ubalance = (long)Convert.ToDouble(responseQuery.reader["balance"].ToString().Trim());
            }
            DatabaseService.Instance.getConnection().Close();

            PaymentGate paymentGateService = getPaymentGate("THECAO");
            if (paymentGateService.id == 0)
            {
                this.appendError(3, "Cổng thanh toán không tồn tại.");
                userResponse.error = this.error;
                return userResponse;
            }


            if (!paymentGateService.status_active.Trim().Equals("active"))
            {
                this.appendError(3, "Cổng thanh toán đang bị khóa .");
                userResponse.error = this.error;
                return userResponse;
            }

            //Kiểm tra tham số trừ tiền có đúng config
            CashIORate cashIORate;
            try
            {
                cashIORate = this.checkValueConfig(paymentGateService, intCardVal);
            }
            catch (Exception ex)
            {
                this.appendError(4, "Kiểm tra config lỗi: " + ex.Message);
                userResponse.error = this.error;
                return userResponse;
            }

            if (cashIORate.cash_in == 0)
            {
                this.appendError(4, "Số tiền vượt quá config được chuyển ");
                userResponse.error = this.error;
                return userResponse;
            }

            if (ubalance < (intCardVal / cashIORate.cash_in * cashIORate.cash_out))
            {
                this.appendError(4, "Bạn không đủ coin để rút được tiền mệnh giá này.");
                userResponse.error = this.error;
                return userResponse;
            }
            //Kiêm tra thời gian rút gần nhất
            string queryStringCheckRwithdrawal = "SELECT TOP 1 " +
    "id,confirm_time" +
    " FROM CardBuyAlegoResponse WHERE user_id =" + user.id + " ORDER BY confirm_time DESC";
            responseQuery = this.query(queryStringCheckRwithdrawal);
            DateTime dateTime = DateTime.Now;
            DateTime time_creat = dateTime;
            int count_r = 0;
            while (responseQuery.reader.Read())
            {
                count_r++;
                time_creat = DateTime.Parse(responseQuery.reader["confirm_time"].ToString().Trim());
            }
            DatabaseService.Instance.getConnection().Close();

            long timeMsSinceNow = new DateTimeOffset(dateTime).ToUnixTimeMilliseconds();
            long timeMsSinceOTP = new DateTimeOffset(time_creat).ToUnixTimeMilliseconds();
            long count_min = (timeMsSinceNow - timeMsSinceOTP) / 1000 / 60;
            if (count_r > 0)
            {
                if (count_min < 5)
                {
                    this.appendError(5, "Xin vui lòng chờ trong giây lát! (Mỗi giao dịch cách nhau 5 phút. Thời gian còn lại của bạn là: " + (5 - count_min) + " phút)");
                    userResponse.error = this.error;
                    return userResponse;
                }
            }

            //Kiểm tra thời gian nạp
            string queryStringCheck = "SELECT TOP 1 " +
"id,time_creat" +
" FROM Transactions WHERE user_id =0 AND user_send = " + user.id + " AND wallet_id = 0 AND type = 'plus' ORDER BY time_creat DESC";
            responseQuery = this.query(queryStringCheck);
            int count_i = 0;
            while (responseQuery.reader.Read())
            {
                count_i++;
                var time_cr = responseQuery.reader["time_creat"].ToString().Trim();
                time_creat = DateTime.Parse(responseQuery.reader["time_creat"].ToString().Trim());
            }
            DatabaseService.Instance.getConnection().Close();

            long timeMsSinceNowIn = new DateTimeOffset(dateTime).ToUnixTimeMilliseconds();
            long timeMsSinceOTPIn = new DateTimeOffset(time_creat).ToUnixTimeMilliseconds();
            long count_min_in = (timeMsSinceNowIn - timeMsSinceOTPIn) / 1000 / 60;
            if (count_i > 0)
            {
                if (count_min_in < 5)
                {
                    this.appendError(5, "Xin vui lòng chờ trong giây lát! (Mỗi giao dịch cách nhau 5 phút. Thời gian còn lại của bạn là: " + (5 - count_min_in) + " phút)");
                    userResponse.error = this.error;
                    return userResponse;
                }
            }

            //---gọi API sang ben Alego
            int responseStatus;
            CardBuy response;
            try
            {
                response = BillingTelcoCardManager.GetOneCardOnline((ENSPType)Convert.ToInt32(typeCard), Convert.ToInt32(intCardVal), out responseStatus);

                //---lay the thanh cong
                if (responseStatus > 0)
                {
                    double plus_money_profit = response.CardValue / cashIORate.cash_in * cashIORate.cash_out;
                    plus_money_profit = Math.Round(plus_money_profit, 2, MidpointRounding.ToEven);

                    //Kiểm tra xem tiền trong ví có đủ
                    double balance = this.getBalace(user.id, 11);
                    if (balance < plus_money_profit)
                    {
                        this.appendError(6, "Số tiền trong ví của bạn không đủ để thực hiện giao dịch này.");
                        userResponse.error = this.error;
                        return userResponse;
                    }

                    //---get user merchant
                    UserServices userServices = new UserServices(this.app_id);
                    UserServiceResponse userServiceResponse = userServices.getUserByApp();

                    //Trừ số tiền ví của user vào merchant
                    double userMoney = 0;
                    string queryTrans = "SELECT TOP 1 balance " +
    "FROM Transactions WHERE user_id = " + user.id + " AND wallet_id = 11 ORDER BY id DESC";
                    responseQuery = this.query(queryTrans);
                    while (responseQuery.reader.Read())
                    {
                        userMoney = Convert.ToDouble(responseQuery.reader["balance"].ToString().Trim());
                    }
                    DatabaseService.Instance.getConnection().Close();
                    double moneyuserMinus = userMoney - plus_money_profit;

                    //Trừ vào ví user
                    DateTime time = DateTime.Now;
                    // Use current time
                    string format = "yyyy-MM-dd HH:mm:ss";
                    responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) " +
                        "VALUES (" + user.id + "," + this.app_id + "," + paymentGateService.id + ",11," + moneyuserMinus + ",'minus','" + time.ToString(format) + "'," + intCardVal + "," + userServiceResponse.info.id + ",N'ruttien',N'Rút thẻ cào',0," + plus_money_profit + ")");
                    DatabaseService.Instance.getConnection().Close();
                    //Trừ vào ví rút user
                    time = DateTime.Now;
                    // Use current time
                    responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) " +
                        "VALUES (" + user.id + ",0," + paymentGateService.id + ",11," + moneyuserMinus + ",'minus','" + time.ToString(format) + "'," + intCardVal + ",0,N'ruttien',N'Rút thẻ cào',0," + plus_money_profit + ")");
                    DatabaseService.Instance.getConnection().Close();

                    //Cộng tiền merchant tạm giữ
                    double userMoney_merchant = 0;
                    string queryTransa = "SELECT TOP 1 balance " +
    "FROM Transactions WHERE user_id =" + userServiceResponse.info.id + " AND transactions_type = 0 AND wallet_id = 0 ORDER BY id DESC";
                    responseQuery = this.query(queryTrans);
                    while (responseQuery.reader.Read())
                    {
                        userMoney = Convert.ToDouble(responseQuery.reader["balance"].ToString().Trim());
                    }
                    DatabaseService.Instance.getConnection().Close();
                    userMoney_merchant = userMoney + plus_money_profit;

                    time = DateTime.Now;
                    // Use current time
                    responseQuery = this.queryInsert("INSERT INTO Transactions(user_id,merchant_id,payment_gate,wallet_id,balance,type,time_creat,value,user_send,tag,message,profit,value_out) " +
                        "VALUES (" + userServiceResponse.info.id + ",0," + paymentGateService.id + ",11," + userMoney_merchant + ",'minus','" + time.ToString(format) + "'," + intCardVal + "," + user.id + ",N'ruttien',N'User nạp thẻ',0," + plus_money_profit + ")");
                    DatabaseService.Instance.getConnection().Close();

                    //---luu the
                    var strTypeCard = "";
                    var intTypeCard = (int)response.CardType;
                    switch (intTypeCard)
                    {
                        case 1:
                            strTypeCard = "Viettel";
                            break;
                        case 2:
                            strTypeCard = "Vinaphone";
                            break;
                        case 3:
                            strTypeCard = "Mobifone";
                            break;
                    }
                    time = DateTime.Now;

                    responseQuery = this.queryInsert("INSERT INTO CardBuyAlegoResponse(card_code,card_serial,card_value,type , time_creat ,status ,user_id ,value_out ,approved_time ,confirm_time,isSeen) " +
                        "VALUES ('" + response.CardCode + "','" + response.CardSerial + "'," + (int)response.CardValue + ",'" + strTypeCard + "','" + time.ToString(format) + "',0," + user.id + "," + (float)plus_money_profit + ",'" + time.ToString(format) + "','" + time.ToString(format) + "',0)");
                    DatabaseService.Instance.getConnection().Close();

                    userResponse.msg = "Bạn đã nhận được thẻ cào, vào mục thẻ cào để lấy thẻ!";
                    userResponse.cardCode = response.CardCode;
                    userResponse.cardSerial = response.CardSerial;
                    userResponse.cardValue = response.CardValue;
                    userResponse.provider = strTypeCard;
                    return userResponse;

                }
            }
            catch (Exception)
            {
                this.appendError(7, "Chưa nhận được thẻ!");
                userResponse.error = this.error;
                return userResponse;
            }

            this.appendError(7, "Chưa nhận được thẻ!");
            userResponse.error = this.error;
            return userResponse;
            //}
        }


        //---nap tien ben Viettel
        public ResponseBeforeCallback addCard(UserModel user, string cardNumber, string serialNumber, int cardValue, string typecard)
        {

            var userResponse = new ResponseBeforeCallback();
            if (this.app_id == 0)
            {
                this.appendError(1001, "App chưa được chọn");
                userResponse.error = this.error;
                return userResponse;
            }
            try
            {
                ResponseQuery responseQuery = new ResponseQuery();

                string provider = "";
                switch (Convert.ToInt32(typecard.Trim()))
                {
                    case 1:
                        provider = "VTT";
                        break;
                    case 2:
                        provider = "VNP";
                        break;
                    case 3:
                        provider = "VMS";
                        break;
                }

                string urlCreateCard = "https://vtt05-api.easypay.live/api/CardCallBack/CreateCardRequest/";
                string gameCode = "5b7e22407a4d4b68a3c76759";
                string nccCode = "5b7e22407a4d4b68a3c76759";
                string accessKey = "gzlmloo8n13eywkmu9vnr02s";
                string sercretKey = "1jhc7in0klte9s45y8f0fyfd";

                string requestId = CryptoHelper.randomToken();

                string text = requestId + "|" + nccCode + "|" + gameCode + "|" + user.username + "|" + cardNumber + "|" + serialNumber + "|" + cardValue + "|" + provider + "|1|" + accessKey;
                //string plantext = CryptoHelper.Base64Encode(text);
                var signature = CryptoHelper.HmacSha256Digest(text, sercretKey);

                text += "|" + signature;
                var data = CryptoHelper.Base64Encode(text);

                //string postCreate = CryptoHelper.postMethod(urlCreateCard, data);
                string getCreate = CryptoHelper.getMethod(urlCreateCard, data);
                //var str = urlCreateCard + "?data=" + data;
                //string getCreate = CryptoHelper.Get(str);

                ResponseCardVT jsonCreateCard = JsonConvert.DeserializeObject<ResponseCardVT>(getCreate);
                if (jsonCreateCard.IsError.Equals("true"))
                {
                    //return Json(new
                    //{
                    //    error = true,
                    //    msg = jsonCreateCard.Message,
                    //    code = jsonCreateCard.ErrorCode
                    //});

                    this.appendError(1, jsonCreateCard.Message);
                    userResponse.error = this.error;
                    return userResponse;

                }

                string sid = requestId;
                ServiceBase serviceBase = new ServiceBase();
                string query = "INSERT INTO Transactions_LogVT(user_id,time,requestId,amout,status,url,cardCode,cardSerial) " + "VALUES (" + user.id + ", convert(datetime,'" + DateTime.Now + "'),'" + sid + "',0,0,'" + ConfigurationManager.AppSettings["domainCallBack"] + "/UsersCreatePayAPI/callbackURLInput','" + cardNumber + "','" + serialNumber + "')";
                responseQuery = this.query(query);
                DatabaseService.Instance.getConnection().Close();
                //db.Transactions_LogVT.Add(new Models.Transactions_LogVT()
                //{
                //    user_id = userID,
                //    time = DateTime.Now,
                //    requestId = sid,
                //    amout = (float)0,
                //    status = 0,
                //    url = ConfigurationManager.AppSettings["domainCallBack"] + "/UsersCreatePayAPI/callbackURLInput"
                //});
                //db.SaveChanges();

                userResponse.msg = "Yêu cầu gửi thẻ dã được gửi. Xin vui lòng đợi hệ thống xử lý";
                return userResponse;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Lỗi nạp thẻ: " + e.Message);
                this.appendError(1, "Lỗi nạp thẻ");
                userResponse.error = this.error;
                return userResponse;
            }
        }
    }

    internal class AuthToken
    {
    }
}
