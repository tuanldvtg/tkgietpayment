﻿using CorePayment.Base;
using CorePayment.Model;
using CorePayment.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorePayment
{
    public class PaymentGateService : ServiceBase
    {
        //Lấy Danh sách App
        public PaymentGateServiceResponse getListPaymentGate()
        {
            var response = new PaymentGateServiceResponse();
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT * FROM PaymentGate");
            //Xem query có lỗi ko
            if (responseQuery.isError)
            {
                this.appendError(1001, "Database error");
                response.error = this.error;
                return response;
            }
            //Lấy danh sách Merchant_coin
            List<PaymentGate> list_paymentGate = new List<PaymentGate>();
            while (responseQuery.reader.Read())
            {
                PaymentGate app = new PaymentGate(Int32.Parse(responseQuery.reader["id"].ToString()), responseQuery.reader["name"].ToString(), responseQuery.reader["code"].ToString());
                list_paymentGate.Add(app);
            }
            DatabaseService.Instance.getConnection().Close();
            response.paymentGates = list_paymentGate;
            return response;
        }

        //Query lấy cổng thanh toán
        private PaymentGate getPaymentGate(string code_gate)
        {
            PaymentGate paymentGateService = new PaymentGate();
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT TOP 1 * FROM PaymentGate WHERE code ='" + code_gate + "'");
            int count_app = 0;
            while (responseQuery.reader.Read())
            {
                count_app++;
                paymentGateService.id = Int32.Parse(responseQuery.reader["id"].ToString());
                paymentGateService.name = responseQuery.reader["name"].ToString();
                paymentGateService.code = responseQuery.reader["code"].ToString();
                paymentGateService.wallet_in = Int32.Parse(responseQuery.reader["wallet_in"].ToString());
                paymentGateService.wallet_out = Int32.Parse(responseQuery.reader["wallet_out"].ToString());
                paymentGateService.status_active = responseQuery.reader["status_active"].ToString();
                paymentGateService.sms = Int32.Parse(responseQuery.reader["sms"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            return paymentGateService;
        }

        //Kiểm tra tham số trừ tiền có đúng config
        private List<CashIORate> getValueConfig(PaymentGate paymentGateService)
        {
            
            ResponseQuery responseQuery = this.query("SELECT * FROM CashIORate WHERE payment_gate_id ='" + paymentGateService.id + "' ORDER BY cash_in ASC");
            List<CashIORate> list_cash = new List<CashIORate>();
            while (responseQuery.reader.Read())
            {
                CashIORate cashIORate = new CashIORate();
                cashIORate.payment_gate_id = Int32.Parse(responseQuery.reader["payment_gate_id"].ToString());
                cashIORate.cash_out = Double.Parse(responseQuery.reader["cash_out"].ToString());
                cashIORate.cash_in = Double.Parse(responseQuery.reader["cash_in"].ToString());
                list_cash.Add(cashIORate);
            }
            DatabaseService.Instance.getConnection().Close();
            return list_cash;
        }

        //Kiểm tra tham số trừ tiền có đúng config
        private CashIORate checkValueConfig(PaymentGate paymentGateService, double value)
        {
            CashIORate cashIORate = new CashIORate();
            ResponseQuery responseQuery = this.query("SELECT TOP 1 * FROM CashIORate WHERE payment_gate_id ='" + paymentGateService.id + "' AND cash_in >= '" + value + "' ORDER BY cash_in ASC ");
            while (responseQuery.reader.Read())
            {
                cashIORate.payment_gate_id = Int32.Parse(responseQuery.reader["payment_gate_id"].ToString());
                cashIORate.cash_out = Double.Parse(responseQuery.reader["cash_out"].ToString());
                cashIORate.cash_in = Double.Parse(responseQuery.reader["cash_in"].ToString());
            }
            DatabaseService.Instance.getConnection().Close();
            return cashIORate;
        }


        public PaymentGateCodeServiceResponse getPaymentGateByCode(string code,double value)
        {
            PaymentGateCodeServiceResponse paymentGateCodeServiceResponse = new PaymentGateCodeServiceResponse();
            PaymentGate paymentGate = this.getPaymentGate(code.Trim());
            List<CashIORate> list_cash = new List<CashIORate>();
            CashIORate cashIORate = this.checkValueConfig(paymentGate, value);
            if (cashIORate.payment_gate_id == 0)
            {
                this.appendError(1000, "Giá trị không thể giao dịch trong cổng này.");
                paymentGateCodeServiceResponse.error = this.error;
                return paymentGateCodeServiceResponse;
            }
            //list_cash = this.getValueConfig(paymentGate);
            double ratio = (cashIORate.cash_in - cashIORate.cash_out) / cashIORate.cash_in;
            paymentGateCodeServiceResponse.ratio = ratio;
            return paymentGateCodeServiceResponse;
        }
    }
}
