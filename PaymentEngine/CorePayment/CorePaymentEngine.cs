﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using CorePayment.Response;
using CorePayment.Base;
using CorePayment.Model;

namespace CorePayment
{
    public class CorePaymentEngine : ServiceBase
    { 
        public string ApppID;

        public SqlConnection connection;
        public int app_id;
        
        public CorePaymentEngine()
        {
            
        } 

        //Constructor connect sql server
        public CorePaymentEngine(String ServerAddress,String DBName,String UserName,String Password)
        {
            if (DatabaseService.Instance.getConnection() == null)
            {
                DatabaseService.Instance.connect(ServerAddress, DBName, UserName, Password);
            }
            
        }


        //Get userService call login registry
        public UserServices userServiceInstance()
        {
            UserServices u = new UserServices(app_id);
            return u;
        }
        //Get transactions call login registry
        public TransactionsService transactionsServiceInstance()
        {
            TransactionsService u = new TransactionsService(app_id);
            return u;
        }
        //Get call paymentGate
        public PaymentGateService paymentGateService()
        {
            PaymentGateService u = new PaymentGateService();
            return u;
        }
        //Get call app
        public AppService appService()
        {
            AppService appService = new AppService();
            return appService;
        }


        //Get userService call login registry
        public PaymentService paymentServiceInstance()
        {
            PaymentService u = new PaymentService(app_id);
            u.activingUser = this.activingUser;
            return u;
        }

        public object paymentServiceInstance(UserModel user)
        {
            this.activingUser = user;
            return user;
        }


        //Set app id
        public bool SetAppID(string name, string secret_key)
        {
            SqlCommand cmd = new SqlCommand();
            ResponseQuery responseQuery = new ResponseQuery();
            responseQuery = this.query("SELECT TOP 1 percent * FROM Merchant WHERE name='" + name + "' AND secret_key='" + secret_key + "' AND status_active = 'active'");
            int count_merchant = 0;
            if (responseQuery.isError)
            {
                return false;
            }
            
            while (responseQuery.reader.Read())
            {
                count_merchant++;
                setApp_id((int)responseQuery.reader[0]);
            }
            DatabaseService.Instance.getConnection().Close();
            if (count_merchant > 0)
            {
                return true;
            }
            return false;
        }

        public void setApp_id(int app_id_set)
        {
            app_id = app_id_set;
            //DatabaseService.Instance.setAppID(app_id_set);
        }

        public int getApp_id()
        {
            return this.app_id;
        }

        public SqlConnection getConnect()
        {
            return DatabaseService.Instance.getConnection();
        }
    }
}
